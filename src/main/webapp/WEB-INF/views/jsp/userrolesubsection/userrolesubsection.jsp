<%-- 
    Document   : userrolesubsection
    Created on : Aug 21, 2017, 1:42:03 AM
    Author     : chandima
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.avn.affiniti.hibernate.model.Userrolesubsection"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>

            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-12">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            User Role Subsection Add
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>

                <form:form id="useRoleSection" novalidate="novalidate" commandName="userRoleSubsectionAddForm" class="smart-form" action="/AFFINITI-V1.98/subsections/searched" method="post">
                    <input id="oldmultisubsectionarray" name="oldmultisubsectionarray" type="hidden" value=""/>
                    <input id="multisubsectionarray" name="multisubsectionarray" type="hidden" value=""/>
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value=""/>
                    <input id="uNmultisectionarray" name="uNmultisectionarray" type="hidden" value=""/>
                    <input id="command" name="command" type="hidden" value=""/>
                    <input id="usubrolid" name="usubrolid" type="hidden" value=""/>
                    <input id="rolid" name="rolid" type="hidden" value=""/>
                    <input id="secid" name="secid" type="hidden" value=""/>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">User Role <samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="userroleid" path="userrole" name="userroleid" items="${userRoleList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>

                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <select id="sectionid" name="sectionid">--Select--</select>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row" id="multibranchselect" style="display: ">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3" id="sectionids">
                            <section>
                                <label class="label">Sub Section</label>
                                <label class="select">
                                    <select id="subsectionid" name="subsectionid" style="height:100px;" multiple="multiple"></select><input type="hidden" name="_subsectionid" value="1"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2">
                            <section>
                                <label class="label" style="height:19px;"></label>
                            </section>
                            <div class="row">
                                <div class="col-xs-5"></div>
                                <div class="col-xs-2">
                                    <section>
                                        <button id="pull_right" type="button" class="btn btn-xs">>></button>
                                    </section>
                                    <section>
                                        <button id="pull_left" type="button" class="btn btn-xs"><<</button>
                                    </section>
                                </div>
                                <div class="col-xs-5"></div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label" style="height:19px;"></label>
                                <label class="select">
                                    <select id="multisection" name="multisection" style="height:100px;" multiple="multiple"></select><input type="hidden" name="_multisection" value="1"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="pagesubmit" onclick="pageSubmit();" type="button" class="btn btn-primary btn-spinner" type="submit" value="Submit">Save</button>  
                                <button id="clearfield"  type="reset" class="btn btn-default" value="Clear">Clear</button>

                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <input id="saveaction" name="saveaction" type="text" value="create" hidden/>
                </form:form>

                <!-- widget grid -->
                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>User Role Subsection Details</h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone"><i class="fa fa-fw fa-user hidden-md hidden-sm hidden-xs"></i>User Role</th>
                                                    <th data-hide="phone"><i class="fa fa-fw fa-user hidden-md hidden-sm hidden-xs"></i>Section</th>
                                                    <th data-hide="phone"><i class="fa fa-fw fa-user hidden-md hidden-sm hidden-xs"></i>Subsection</th> 
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o hidden-md hidden-sm hidden-xs"></i> Last Updated Time</th>
                                                    <th data-hide="phone,tablet">Last Updated User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->

                    </div>								
                </section>
                <!-- end widget grid -->

            </div>
            <!-- END MAIN CONTENT -->

        </div>
        <!-- END MAIN PANEL -->
        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>


        <script type="text/javascript">

            var search = false;
            var search_table;
            $(document).ready(function () {
//                var id = "1";
//                var value = "nimal";
//                pageSetUp();
//                var table = $('#dt_basic').DataTable({
//                    columns: [
//                        {data: 'userroleDes'},
//                        {data: 'sectionDes'},
//                        {data: 'subsectionDes'},
//                        {data: 'createddatetime'},
//                        {data: 'lastupdateddatetime'},
//                        {data: 'createduser'},
//                        {data: 'action'}
//                    ]
//                });
//                $.ajax({
//                    type: "get",
//                    url: "${pageContext.servletContext.contextPath}/userrolesubsection/loadtable",
//                    cache: false,
//                    data: {max: 30},
//                    success: function (response) {
//                        response = JSON.parse(response);
//                        var tableData = response.data;
//                        table.rows.add(tableData).draw();
//                        if (response.status === true) {
//                            $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
//                        } else {
//                            // $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.message + '</div> <br/>');
//                            window.scrollTo(0, 0);
//                        }
//                    },
//                    error: function () {
//                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
//                        window.scrollTo(0, 0);
//                    }
//                });




                /***************************** Load Data Table **********************/

                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                search_table = $('#dt_basic').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/userrolesubsection/loadTable",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "searchOption", "value": $('#searchoption').val()});
                        aoData.push({"name": "searchValue", "value": $.trim($('#input').val())});

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": "${pageContext.servletContext.contextPath}/userrolesubsection/loadTable",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "userroleDes", "bSortable": false},
                        {"mDataProp": "sectionDes", "bSortable": false},
                        {"mDataProp": "subsectionDes", "bSortable": false},
                        {"mDataProp": "lastupdateddatetime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });


                /***************************** END Load Data Table **********************/

            });


            //search_table.fnDraw();

            function clearTable() {
                search = false;
                $('#dt_basic').dataTable().fnClearTable();
                // $("#msg_dev").fadeOut("slow");

            }

            var form = $('#useRoleSection').validate({
                onkeyup: function (element) {
                    $(element).valid();
                },
                onfocusout: function (element) {
                    $(element).valid();
                },
                rules: {
                    sectionid: {
                        required: true
                    },
                    statusid: {
                        required: true
                    },
                    userrole: {
                        required: true
                    }
                }, errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });



            /*******************************----******************************/

            $('#pull_right').click(function () {
                var selectedItem = $("#subsectionid option:selected");
                $("#multisection").append(selectedItem);
                selectedItem.prop("selected", false);
                generateBranchArray();
            });

            $('#pull_left').click(function () {
                var selectedItem = $("#multisection option:selected");
                $("#subsectionid").append(selectedItem);
                selectedItem.prop("selected", false);
                generateBranchArray();
            });

            //            $('#sectionid').dblclick(function () {
            //                var selectedItem = $("#sectionid option:selected");
            //                $("#multisection").append(selectedItem);
            //                selectedItem.prop("selected", false);
            //                generateBranchArray();
            //            });
            //            $('#multisection').dblclick(function () {
            //                var selectedItem = $("#multisection option:selected");
            //                $("#sectionid").append(selectedItem);
            //                selectedItem.prop("selected", false);
            //                generateBranchArray();
            //            });

            function resetMultiBranchSelect() {

                $('#multisectionarray').val();
                $('#multisection option').prop('selected', true);
                var selectedItem = $('#multisection option:selected');
                $('#sectionid').append(selectedItem);
                selectedItem.prop('selected', false);
                sortSelect('#sectionid', 'text', 'asc');
            }

            function generateBranchArray() {
                var section = [];
                $("#multisection option").each(function () {
                    section.push($(this).val());
                });
                $('#multisectionarray').val(JSON.stringify(section));

            }

            function generateUnAssignedBranchArray() {
                var unAssignedsection = [];
                $("#subsectionid option").each(function () {
                    unAssignedsection.push($(this).val());
                });
                $('#uNmultisectionarray').val(JSON.stringify(unAssignedsection));

            }

            jQuery.validator.addMethod("multisectionvalidate", function () {
                $('#multisection option').prop('selected', true);
                var selectedItem = $('#multisection option:selected');
                console.log(selectedItem.length);
                if (selectedItem.length > 0) {
                    return true;
                } else {
                    return false;
                }
            }, jQuery.validator.format("Please select atleast one section."));



            /*******************************--Save or Update function--************************************/
            function pageSubmit() {

                var validTask = true;
                generateBranchArray();
                generateUnAssignedBranchArray();

                var userroleid = $('#userroleid').val();
                var sectionid = $('#sectionid').val();
                if (userroleid === '' || sectionid === '') {
                    validTask = false;
                }

                if ($('#useRoleSection').valid()) {

                    var objArr = [];
                    var unObjArr = [];
                    var sectionIdArr = JSON.parse($('#multisectionarray').val());
                    var uNsectionIdArr = JSON.parse($('#uNmultisectionarray').val());

                    var rol = $('#userroleid').val();
                    var sec = $('#sectionid').val();
                    for (var i = 0; i < sectionIdArr.length; i++) {
                        var obj = {
                            "userrolid": $('#userroleid').val(),
                            "sectionId": $('#sectionid').val(),
                            "subsectionId": sectionIdArr[i]
                        };
                        objArr.push(obj);
                    }

                    for (var i = 0; i < uNsectionIdArr.length; i++) {
                        var obj = {
                            "userroleid": $('#userroleid').val(),
                            "sectionid": $('#sectionid').val(),
                            "subsectionid": uNsectionIdArr[i]
                        };
                        unObjArr.push(obj);
                    }
                    var selectedFunction = $('#saveaction').val();
                    var content = JSON.stringify(objArr);
                    var Uncontent = JSON.stringify(unObjArr);
                    var option = $('#userroleid').val();
                    $.ajax({
                        type: "post",
                        url: "${pageContext.servletContext.contextPath}/userrolesubsection/insert?action=" + selectedFunction,
                        cache: false,
                        data: {subinfo: content, Unsubinfo: Uncontent, command: $('#command').val(), id: $('#usubrolid').val(), section: sec, role: rol},
                        success: function (response) {
                            response = JSON.parse(response);
                            console.log(response);
                            if (response) {

                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
                                search_table.fnDraw();
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.statuscode + '</div> <br/>');
                                window.scrollTo(0, 0);
                            }
                            $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            clearAll();
                            
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            window.scrollTo(0, 0);
                        }
//                        ,
//                        beforeSend: function () {
//                            $('#pagesubmit').button('loading');
//                        },
//                        complete: function () {
//                            $('#pagesubmit').button('reset');
//                        }
                    });
                } else {
                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong>Please select required fields</div><br/>');
                     $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                }
                $('#command').val("");
            }


            /*******************************--Load Section List to the Section Combo Box--******************************/

            function SectionList() {
                var roleid = $('#userroleid').val();
                var roleindex = $('#userroleid').prop('selectedIndex');
                $('#sectionid').empty();
                $('#multisection').empty();
                $('#sectionid').append($("<option></option>").attr("value", '').text('--Select--'));
                generateBranchArray();
                if (roleindex !== 0) {
                    $.ajax({
                        type: "GET",
                        url: "${pageContext.servletContext.contextPath}/userrolesubsection/loadsections",
                        async: false,
                        cache: false,
                        data: {id: roleid},
                        success: function (data) {
                            console.log("Section list " + data);
                            if (data !== "") {
                                res = JSON.parse(data);
                                response = res.data;
                                console.log("POPOPOPOPOPO", response);
                                for (var i = 0; i < response.length; i++) {
                                    $("#sectionid").append($("<option></option>")
                                            .attr("value", response[i].SECTIONID)
                                            .text(response[i].DESCRIPTION));
                                    $("#sectionid").trigger("change");
                                }
                                if (response.status === true) {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                } else {
                                    //   $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                            }
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }

            }

            /*******************************--End of Load Section List to the Section Combo Box--******************************/

            $('#sectionid').on('change', function () {
                var roleid = $('#userroleid').val();
                var roleindex = $('#sectionid').val();

                if (roleindex !== "") {
                    //SubSectionList(roleid, roleindex);
                    listSubsections(roleid, roleindex);
                }

            });

            $('#userroleid').on('change', function () {
                var roleid = $('#userroleid').val();
                SectionList();
            });

            $('#userroleid').on('click', function () {
                $('#sectionid').prop('selectedIndex', 0);
                $('#subsectionid').empty();
                $('#multisection').empty();
            });

            $('#clearfield').on('click', function () {
                $("#pagesubmit").fadeIn();
                $('#subsectionid').empty();
                $('#multisection').empty();
                $('#userroleid').prop('selectedIndex', 0);
                $('#sectionid').prop('selectedIndex', 0);
                $("#msg_dev").fadeOut("slow");


                clearValidations();

                $("#sectionid").prop('disabled', false);
                $("#userroleid").prop('disabled', false);
                $("#multisection").prop('disabled', false);
                $("#subsectionid").prop('disabled', false);
            });

            function clearAll() {
                $("#pagesubmit").fadeIn();
                $('#subsectionid').empty();
                $('#multisection').empty();
                $('#userroleid').prop('selectedIndex', 0);
                $('#sectionid').prop('selectedIndex', 0);

                clearValidations();

                $("#sectionid").prop('disabled', false);
                $("#userroleid").prop('disabled', false);
                $("#multisection").prop('disabled', false);
                $("#subsectionid").prop('disabled', false);
            }

            /************** Function to clear valiodations *************/

            function clearValidations() {

                $("#useRoleSection").validate().resetForm();
                $("#useRoleSection label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });
            }

            /************** End of Function to clear valiodations *************/

            /******************************-- View And Update --***********************************/

            $('#dt_basic').on('click', 'tr a', function (e) {
                clearValidations();
                var id = $(this).attr('id');
                $('#usubrolid').val(id);
                var value = $(this).attr('value');
                var value2 = $(this).attr('value1');
                var value1 = $(this).attr('value2');
                $('#rolid').val(value1);
                $('#secid').val(value2);

                if (value == "view") {
                    $("#pagesubmit").fadeOut("slow");
                    $('#command').val(value);
                    listSubsections(value1, value2);
                    $('#userroleid').find('option[value="' + value1 + '"]').prop('selected', true);
                    SectionList();
                    $('#sectionid').find('option[value="' + value2 + '"]').prop('selected', true);
                    $("#sectionid").prop('disabled', true);
                    $("#userroleid").prop('disabled', true);
                    $("#multisection").prop('disabled', true);
                    $("#subsectionid").prop('disabled', true);

                    $("#useRoleSection label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass('state-success');
                    });
                }

                else if (value == "edit") {
                    $("#pagesubmit").fadeIn();
                    listSubsections(value1, value2);
                    $('#userroleid').find('option[value="' + value1 + '"]').prop('selected', true);
                    SectionList();
                    $('#sectionid').find('option[value="' + value2 + '"]').prop('selected', true);
                    $("#sectionid").prop('disabled', false);
                    $("#userroleid").prop('disabled', false);
                    $("#multisection").prop('disabled', false);
                    $("#subsectionid").prop('disabled', false);
                }
                // SubSectionList(value1, value2);
                $("#useRoleSection label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });

            });

            /******************************-- End of View And Update --***************************/



            /**************************************--Load both Subsection list--*********************/

            function  listSubsections(roleid, roleindex) {

                console.log(roleid);
                $('#subsectionid').empty();
                $('#multisection').empty();
                generateBranchArray();
                if (roleindex !== 0) {
                    var dataObject = new Object();
                    dataObject.userrolid = roleid;
                    dataObject.sectionid = roleindex;
                    var content = JSON.stringify(dataObject);
                    $.ajax({
                        type: "GET",
                        url: "${pageContext.servletContext.contextPath}/userrolesubsection/loadSubsectionDropdownlists",
                        //async: false,
                        cache: false,
                        data: {subsectiondataobject: content},
                        success: function (data) {
                            if (response !== "") {
                                var response = JSON.parse(data);
                                var data1 = response.subsection;
                                var data2 = response.assignedsubsection;
                                console.log(data1);
                                console.log(data2);

                                for (var i = 0; i < data1.length; i++) {
                                    $('#subsectionid').append($("<option></option>").attr("value", data1[i].id).text(data1[i].value));
                                }

                                for (var i = 0; i < data2.length; i++) {
                                    $('#multisection').append($("<option></option>").attr("value", data2[i].id).text(data2[i].value));
                                }
                            }
                        },
                        error: function () {
                            console.log('Error while request..');
                        }
                    });
                }
            }


            /***************************************--END--*******************************************/


        </script>
        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </body>
</html>

