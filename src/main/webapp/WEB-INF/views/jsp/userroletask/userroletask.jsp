<%-- 
    Document   : userroletask
    Created on : Aug 12, 2017, 9:16:07 AM
    Author     : Kaushan Fernando
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            User Role Subsection Task Add   
                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  id="userRoleTaskAddFormId" commandName="userRoleTaskAddForm" novalidate="novalidate" class="smart-form">
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value="0"/>
                    <input id="selectfunctionid" name="selectfunction" type="hidden" value="save"/>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">User Role<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="userroleid" path="userrole" cssClass="input-sm" items="${userRoleList}" onchange="SectionList()"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="sectionid" path="section" cssClass="input-sm" onchange="SubSectionList()"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Subsection<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="subsectionid" path="subsection" cssClass="input-sm" onchange="TaskList()"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>                

                    </div>

                    <div class="row" id="multibranchselect" >
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Task<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="tasklistid" path="section" cssClass="input-sm" multiple="multiple" readonly="true" style="height:100px" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2">

                            <section>
                                <label class="label" style="height:19px;"></label>
                            </section>
                            <div class="row">
                                <div class="col-xs-5"></div>
                                <div class="col-xs-2">
                                    <section>
                                        <button id="pull_right" readonly="true" type="button" class="btn btn-xs">&gt;&gt;</button>
                                    </section>
                                    <section>
                                        <button id="pull_left" readonly="true" type="button" class="btn btn-xs">&lt;&lt;</button>
                                    </section>
                                </div>
                                <div class="col-xs-5"></div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label" style="height:19px;"></label>
                                <label class="select">
                                    <%--<form:select id="multisection" path="section" cssClass="input-sm" multiple="multiple" readonly="true" style="height:100px"/>--%>
                                    <select id="multisection" name="multisection" style="height:100px;" multiple="multiple"></select>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <form:button id="btnAddUserRoleTask" type="button" class="btn btn-primary btn-spinner">Assign</form:button>
                                </footer>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                </form:form>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            User Role Subsection Task Search   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev1" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>




                <!-- SEARCH BOX -->

                <div class="row ">
                    <!-- NEW COL START -->
                    <div class="smart-form" style="margin-left: 14px">
                        <label class="label " >User Role</label>  
                    </div>
                    <form>

                        <form:form commandName="userRoleTaskSearchForm" novalidate="novalidate" class="smart-form" >

                            <article class="col-sm-12 col-md-12 col-lg-6">

                                <div class="input-group input-group-sm ">                              
                                    <form:select id="userrole" path="userrole" class="form-control" items="${userRoleList}"/>                                
                                    <div class="input-group-btn">
                                        <form:button id="search_btn" type="button" class = "btn btn-primary" value = "Search">Search</form:button>
                                        </div>
                                    </div>
                                </article>
                        </form:form>
                    </form>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p style="color: white">|</p>
                    </div>
                </div>
                <!-- END OF SEARCH BOX -->


                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="true">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>User Role Subsection Task Details</h2>

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="userRoleTaskTableId" class="table table-bordered">
                                            <thead>			                
                                                <tr>
                                                    <th data-class="expand">Section</th>
                                                    <th data-class="expand">Subsection</th>
                                                    <th data-class="expand">User Role</th>
                                                    <th data-class="expand">Task</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Date Time</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Date Time</th>
                                                    <th data-class="expand">Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>
        <!-- Style for edit and find buttons in table -->

        <!-- Start Javascript functions -->
        <script type="text/javascript">

            /************** Page Load Functions *************/
            $(document).ready(function () {
                var permissions = JSON.parse($('#permissions').val());
                refreshPermissions(permissions);

                $('#sectionid').append($("<option></option>").attr("value", '').text('-- Select --'));
                $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));


            });

            /************** End of Page Load Functions *************/

            /************** Load Dropdown list of sections *************/
            function SectionList() {
                var userRoleId = $('#userroleid').val();
                resetDropdowns();
                enableEditFields(true);
                $('#btnAddUserRoleTask').removeClass('disabled');
                $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));
                $('#msg_dev').empty();
                if (userRoleId !== '') {
                    $.ajax({
                        type: "GET",
                        url: '${pageContext.servletContext.contextPath}/userroletask/getuserrolesections',
                        cache: false,
                        data: 'userroleid=' + userRoleId,
                        success: function (response) {
                            result = JSON.parse(response).data;


                            for (var i = 0; i < result.length; i++) {

                                $('#sectionid').append($("<option></option>").attr("value", result[i].id).text(result[i].value));
                            }

                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading sections Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }
            }

            /************** End of Load Dropdown list of sections *************/

            /************** Load Dropdown list of sub sections *************/
            function SubSectionList() {

                var userRoleId = $('#userroleid').val();
                var sectionId = $('#sectionid').val();

                $('#subsectionid').empty();
                $("#multisection").empty();
                $("#tasklistid").empty();
                $('#msg_dev').empty();
                if (sectionId !== '') {
                    $.ajax({
                        type: "GET",
                        url: '${pageContext.servletContext.contextPath}/userroletask/getuserrolesubsections',
                        cache: false,
                        dataType: 'json',
                        data: {'userroleId': userRoleId, 'sectionId': sectionId},
                        success: function (response) {

                            result = response.data;
                            for (var i = 0; i < result.length; i++) {

                                $('#subsectionid').append($("<option></option>").attr("value", result[i].id).text(result[i].value));
                            }
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading Sub sections Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }
            }

            /************** End of Load Dropdown list of sub sections *************/

            /************** Load Dropdown list of Tasks *************/
            function TaskList() {
                $('#btnAddUserRoleTask').removeClass('disabled');
                $("#multisection").empty();
                $("#tasklistid").empty();
                var userRoleId = $('#userroleid').val();
                var sectionId = $('#sectionid').val();
                var subSectionId = $('#subsectionid').val();
                var buttonSelector = 1;
                if (!(subSectionId === '-- Select --' || subSectionId === '')) {

                    getTasksOnEditAndView(userRoleId, sectionId, subSectionId, buttonSelector);

                }

            }

            /************** End of Load Dropdown list of Tasks *************/

            /***************************** Load Table **********************/
            var responsiveHelper_dt_basic = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            var search_table = $('#userRoleTaskTableId').dataTable({
                searching: false, //to disable default search function in data table
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#userRoleTaskTableId'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();

                    var permissions2 = JSON.parse($('#permissions').val());

                    if (!permissions2.assign) {

                        $('.fa-pencil').parent().parent().remove();
                    }
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/userroletask/getuserrroletasktotable",
                "fnServerData": function (sSource, aoData, fnCallback) {

                    aoData.push({"name": "userroleid", "value": $('#userrole').val()});



                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data) {

                            fnCallback(data);
                            $('#msg_dev1').empty();
                            $('#search_btn').removeClass('disabled');
                            if (data.CODE === 'ERROR') {
                                $('#msg_dev1').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + data.MESSAGE + ' </div> <br/>');
                                $("#msg_dev1").fadeIn().delay(3000).fadeOut('slow');
                                $('#search_btn').addClass('disabled');
                                $('#userrole').append($("<option></option>").attr("value", '').text('--Select--'));
                            }
                        }
                    });
                },
                "order": [[0, "asc"]],
                "aoColumns": [
                    {"mDataProp": "section", "bSortable": false},
                    {"mDataProp": "subsection", "bSortable": false},
                    {"mDataProp": "userrole", "bSortable": false},
                    {"mDataProp": "task", "bSortable": false},
                    {"mDataProp": "lastupdatedtime", "bSortable": false},
                    {"mDataProp": "createdtime", "bSortable": false},
                    {"mDataProp": "createduser", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}

                ]
            });
            /* END BASIC */

            $('#search_btn').click(function () {
                $('#msg_dev').empty(); //remove messages
                search_table.fnDraw();

            });
            /*************************** End Load Table **********************/
            /************** Function to clear data table *************/
            function clearTable() {
                search_table.clear().draw();
            }
            /************** End of Function to clear data table *************/

            /************** >> pull right button on click Function *************/
            $('#pull_right').click(function () {
                $('#msg_dev').empty();
                var selectedItem = $("#tasklistid option:selected");
                $("#multisection").append(selectedItem);
                selectedItem.prop("selected", false);
                generateBranchArray();
            });

            /************** >> End of pull right button on click Function *************/

            /************** << pull left button on click Function *************/

            $('#pull_left').click(function () {
                $('#msg_dev').empty();
                var selectedItem = $("#multisection option:selected");
                $("#tasklistid").append(selectedItem);
                selectedItem.prop("selected", false);
                generateBranchArray();
            });

            /************** << End of pull left button on click Function *************/

            /************** Function to hold add or edit vlue *************/

            function setFunctionAddOrEdit(selectedFunction) {
                $('#selectfunctionid').val(selectedFunction);
            }

            /************** End of Function to hold add or edit vlue *************/

            /************** Jquery validation Function *************/

            var form = $('#userRoleTaskAddFormId').validate({
                onkeyup: function (element) {
                    $(element).valid();
                },
                onfocusout: function (element) {
                    $(element).valid();
                },
                rules: {
                    userrole: {
                        required: true
                    },
                    section: {
                        required: true
                    },
                    subsection: {
                        required: true
                    }
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });

            $.validator.methods.count_children = function (value, element) {
                var is_empty = $(element).is(":empty");
                return !is_empty;
            }
            /************** End of Jquery validation Function *************/

            /************** Function to add or edit task *************/
            $('#btnAddUserRoleTask').click(function () {

                if ($("#userRoleTaskAddFormId").valid()) {
                    var selectedFunction = $('#selectfunctionid').val();

                    var objArr = [];
                    var taskIdArr = JSON.parse($('#multisectionarray').val());
                    if (taskIdArr.length === 0 || $('#multisectionarray').val() === "0") {
                        var obj = {
                            "userroleId": $('#userroleid').val(),
                            "sectionId": $('#sectionid').val(),
                            "subsectionId": $('#subsectionid').val(),
                            "taskId": "0"

                        };

                        objArr.push(obj);

                    } else {
                        for (var i = 0; i < taskIdArr.length; i++) {
                            var obj = {
                                "userroleId": $('#userroleid').val(),
                                "sectionId": $('#sectionid').val(),
                                "subsectionId": $('#subsectionid').val(),
                                "taskId": taskIdArr[i]

                            };

                            objArr.push(obj);
                        }
                    }

                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: '${pageContext.servletContext.contextPath}/userroletask/adduserroletask?selectfunction=' + selectedFunction + '&userroleid=' + $('#userroleid').val() + '&sectionid=' + $('#sectionid').val() + '&subsectionid=' + $('#subsectionid').val(),
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(objArr),
                        success: function (result) {

                            if (result.success) {
                                window.scrollTo(0, 0);
                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong>  ' + result.message + '</div> <br/>');
                                $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + result.message + '</div> <br/>');
                                window.scrollTo(0, 0);
                                $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            }
                            clearValidations();
                            resetDropdowns();
                            setFunctionAddOrEdit('save');
                            search_table.fnDraw();
                            $('#userroleid').val('');
                            $('#sectionid').append($("<option></option>").attr("value", '').text('-- Select --'));
                            $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));
                        },
//                        beforeSend: function () {
//                            $('#btnAddUserRoleTask').button('loading');
//                        },
//                        complete: function () {
//                            $('#btnAddUserRoleTask').button('reset');
//                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                            $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                        }
                    });

                }

            });

            /************** End of Function to add or edit task *************/


            /************** Function of table edit adn view icon click *************/
            $('#userRoleTaskTableId').on('click', 'tr a', function () {
                clearValidations();
                resetDropdowns();
                var id = $(this).attr('id');

                var splitData = id.split(':');

                var userRoleId = splitData[0];
                var sectionId = splitData[1];
                var subSectionId = splitData[2];


                var buttonSelector = 0;

                var value = $(this).attr('value');



                if (value === "view") {
                    $('#btnAddUserRoleTask').addClass('disabled');
                    enableEditFields(false);

                } else if (value === "edit") {

                    setFunctionAddOrEdit('edit');
                    $('#btnAddUserRoleTask').removeClass('disabled');
                    enableEditFields(true);

                }
                getTasksOnEditAndView(userRoleId, sectionId, subSectionId, buttonSelector);



            });

            /************** End of Function of table edit and view icon click *************/

            /************** Function to set tasks on talbe view and edit buttons clicked *************/

            function getTasksOnEditAndView(userRoleId, sectionId, subSectionId, buttonSelector) {
                $('#msg_dev').empty();
                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    url: '${pageContext.servletContext.contextPath}/userroletask/gettasklist',
                    type: 'GET',
                    dataType: 'json',
                    data: {'userRoleId': userRoleId, 'sectionId': sectionId, 'subSectionId': subSectionId},
                    success: function (result) {
                        var data = result.data;
                        var data2 = result.data2;
                        var data3 = result.data3;
                        var data4 = result.data4;

                        if (buttonSelector === 0) {
                            $('#userroleid').val(userRoleId);
                            $('#sectionid').append($("<option></option>").attr("value", data3[1].id).text(data3[1].value));
                            $('#subsectionid').append($("<option></option>").attr("value", data4[1].id).text(data4[1].value));

                        }
                        if (data.length === 0) {
                            if (data2.length === 0) {
                                $('#btnAddUserRoleTask').addClass('disabled');
                            }

                        }
                        for (var i = 0; i < data.length; i++) {
                            var option = "<option value='" + data[i].id + "'>" + data[i].value + "</option>";
                            $('#tasklistid').append(option);

                        }
                        if (data2.length !== 0) {
                            for (var i = 0; i < data2.length; i++) {
                                var option = "<option value='" + data2[i].id + "'>" + data2[i].value + "</option>";
                                $('#multisection').append(option);
                                generateBranchArray();

                            }
                        }


                    }, error: function () {
                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading Tasks Please Re-Try!</div> <br/>');
                        window.scrollTo(0, 0);
                    }
                });
            }


            /************** Function to Enable of disable buttons or selectors *************/
            function enableEditFields(status) {
                if (status) {
                    //enable

                    $('#pull_right').removeClass('disabled');
                    $('#pull_left').removeClass('disabled');
                } else {
                    // disable

                    $('#pull_left').addClass('disabled');
                    $('#pull_right').addClass('disabled');
                }
            }

            /************** End of Function to Enable of disable buttons or selectors *************/

//            /************** Function to clear data table *************/
//            function clearTable() {
//                table.clear().draw();
//            }
//            /************** End of Function to clear data table *************/

            /************** Function to clear selectors *************/

            function resetDropdowns() {
                $('#sectionid').empty();
                $('#subsectionid').empty();
                $('#tasklistid').empty();
                $('#multisection').empty();
            }

            /************** End of Function to clear selectors *************/

            /************** Function to hold task details *************/

            function generateBranchArray() {
                var section = [];
                $("#multisection option").each(function () {
                    section.push($(this).val());
                });
                $('#multisectionarray').val(JSON.stringify(section));

                sortSelectOptions('#tasklistid');
                sortSelectOptions('#multisection');

            }

            /************** End of Function to hold task details *************/

            /************** Function to sort values in selectors *************/
            function sortSelectOptions(elemId) {
                $(elemId).html($(elemId + ' option').sort(function (a, b) {
                    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                }));
            }

            /************** End of Function to sort values in selectors *************/
            /************** Function to clear valiodations *************/

            function clearValidations() {

                $("#userRoleTaskAddFormId").validate().resetForm();
                $("#userRoleTaskAddFormId label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });
            }

            /************** End of Function to clear valiodations *************/
            /************** Function to disable all fields *************/

            function disableForm() {

                $('#userroleid').prop('disabled', true);
                $('#sectionid').prop('disabled', true);
                $('#subsectionid').prop('disabled', true);
                $('#tasklistid').prop('disabled', true);
                $('#multisection').prop('disabled', true);
                $('#pull_right').attr('disabled', 'disabled');
                $('#pull_left').attr('disabled', 'disabled');

            }

            /************** End of Function to disable all fields *************/

            /**************** Function to handle page permissions *****************/

            function refreshPermissions(permissions) {
                if (!permissions.search) {
                    $('#search_btn').attr('disabled', 'disabled');
                    $('#userrole').attr('disabled', 'disabled');

                }
                if (!permissions.assign) {
                    disableForm();
                    $('#btnAddUserRoleTask').attr('disabled', 'disabled');


                }
            }
            /**************** End of Function to handle page permissions *****************/

        </script>
        <!-- End Javascript functions -->
    </body>
</html>