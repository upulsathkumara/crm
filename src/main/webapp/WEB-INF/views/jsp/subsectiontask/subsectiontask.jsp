<%-- 
    Document   : subsectiontask
    Created on : Sep 25, 2017, 11:15:35 AM
    Author     : Kaushan Fernando
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">

                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Subsection Task Add   
                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  id="subSectionTaskAddFormId" commandName="subSectionTaskAddForm" novalidate="novalidate" class="smart-form">
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value="0"/>
                    <input id="selectfunctionid" name="selectfunction" type="hidden" value="save"/>
                    <input id="sectionId" name="section" type="hidden" value=""/>
                    <input id="subSectionId" name="subsection" type="hidden" value=""/>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="sectionid" path="section" cssClass="input-sm" items="${sectionList}" onchange="GetSubSectionAndTaskList()"/>
                                    <i></i>
                                </label>
                            </section>                           
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Sub Section<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="subsectionid" path="subsection" cssClass="input-sm" onchange="GetTaskList()"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Tasks<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="tasklistid" path="task" cssStyle="width:100%" Class="select2-container select2-container-multi select2" multiple="true" tabindex="1" onchange=""></form:select>
                                    </label>

                                </section>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label"></label>
                                    <label class="input" id="dynamicLbl">

                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div> 
                            <div class="col-xs-3">
                                <section>
                                    <label class="label"></label>
                                    <label class="input" id="dynamic">

                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>   
                        </div>

                        <div class="row">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <footer style="background-color: #ffffff">
                                <form:button id="btnAddSubSectionTask" type="button" class="btn btn-primary btn-spinner">Assign</form:button>

                                <form:button id="resetBtnId" type="button" class="btn btn-default">Clear</form:button>
                                </footer>
                            </div>

                            <div class="col-xs-1"></div>
                        </div>

                </form:form>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Subsection Task Search   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev1" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>


                <form:form commandName="subSectionTaskSearchForm" novalidate="novalidate" class="smart-form" >

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section</label>
                                <label class="select">
                                    <form:select id="sectionsearchid" path="section" cssClass="input-sm" items="${sectionList}" onchange="SubSectionList()"/>
                                    <i></i>
                                </label>
                            </section>                           
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Sub Section</label>
                                <label class="select">
                                    <form:select id="subsectionsearchid" path="subsection" items="${subSectionList}" cssClass="input-sm" onchange="TaskList()"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Tasks</label>
                                <label class="select">
                                    <form:select id="tasklistsearchid" path="task" items="${taskList}" cssClass="input-sm" />
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>              
                    </div>


                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <form:button id="search_btn" type="button" class="btn btn-primary" value="Submit">Search</form:button>  
                                </footer>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                </form:form>
                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="true">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Subsection Tasks Details </h2>

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="subSectionTaskTableId" class="table table-bordered">
                                            <thead>			                
                                                <tr>
                                                    <th data-class="expand">Section</th>
                                                    <th data-class="expand">Subsection</th>                                                    
                                                    <th data-class="expand">Task</th>
                                                    <th data-class="expand">URL</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Date Time</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Date Time</th>
                                                    <th data-class="expand">Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>
        <!-- Style for edit and find buttons in table -->

        <!-- Start Javascript functions -->
        <script type="text/javascript">

            /************** Page Load Functions *************/
            var dynamicFields = [];

            $(document).ready(function () {

                var permissions = JSON.parse($('#permissions').val());

                refreshPermissions(permissions);

                $("#tasklistid").select2();
                $('#tasklistid').append($("<option></option>").attr("value", '').text('-- Select --'));
                $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));

                /************** Function to generate or remove dynamic text fields *************/
                function GenerateAndRemoveDynamicFields(selectedFunction) {
                    if (selectedFunction === 'save') {
                        var sectionId = $('#sectionid option:selected').text();
                        var subsectionId = $('#subsectionid option:selected').text();
                        var selections = [];
                        if ($('#tasklistid').val()) {
                            selections = $('#tasklistid').val();
                        }

                        for (var i = 0; i < dynamicFields.length; i++) {

                            if (jQuery.inArray(dynamicFields[i], selections) == -1) {

                                $('[data-task-id="' + dynamicFields[i] + '"]').remove();
                                $('[data-url-id="' + dynamicFields[i] + '"]').remove();

                                dynamicFields.splice(i, 1);
                            }
                        }

                        for (var i = 0; i < selections.length; i++) {

                            var optionDescriprion = $('#tasklistid option[value="' + selections[i] + '"]').html();

                            if (optionDescriprion != '-- Select --') {


                                if (jQuery.inArray(selections[i], dynamicFields) == -1) {
                                    dynamicFields.push(selections[i]);
                                    var elem1 = '<label class="label" data-task-id="' + selections[i] + '" style="margin-top:15px">' + optionDescriprion + ' URL <samp style="color: red">*</samp></label>';
                                    $('#dynamicLbl').append(elem1);
                                    var elem = '<input class="input-sm" id = "url" name = "url" type="text" data-url-id="' + selections[i] + '" placeholder="' + sectionId + '/' + subsectionId + '/' + optionDescriprion + '" style="margin-bottom:5px"/>';
                                    $('#dynamic').append(elem);

                                }
                            }
                        }

                    } else if (selectedFunction === 'edit') {
                        var sectionId = $('#sectionid option:selected').text();
                        var subsectionId = $('#subsectionid option:selected').text();
                        var selections = [];
                        if ($('#tasklistid').val()) {

                            selections = $('#tasklistid').val();
                        }

                        for (var i = 0; i < dynamicFields.length; i++) {
                            var deleteOrNot = "delete";
                            for (var x = 0; x < selections.length; x++) {
                                if (dynamicFields[i].toString() === selections[x]) {

                                    deleteOrNot = "keep";

                                }
                            }
                            if (deleteOrNot === "delete") {

                                $('[data-task-id="' + dynamicFields[i] + '"]').remove();
                                $('[data-url-id="' + dynamicFields[i] + '"]').remove();
                                dynamicFields.splice(i, 1);
                            }
                        }
                        for (var i = 0; i < selections.length; i++) {
                            var insertOrNot = "insert";
                            var optionDescriprion = $('#tasklistid option[value="' + selections[i] + '"]').html();

                            if (optionDescriprion != '-- Select --') {
                                for (var x = 0; x < dynamicFields.length; x++) {
                                    if (selections[i] === dynamicFields[x].toString()) {
                                        insertOrNot = "dont";
                                    }
                                }
                                if (insertOrNot === "insert") {
                                    dynamicFields.push(selections[i]);
                                    var elem1 = '<label class="label" data-task-id="' + selections[i] + '" style="margin-top:15px">' + optionDescriprion + ' URL <samp style="color: red">*</samp></label>';
                                    $('#dynamicLbl').append(elem1);
                                    var elem = '<input class="input-sm" id = "url" name = "url" type="text" data-url-id="' + selections[i] + '" placeholder="' + sectionId + '/' + subsectionId + '/' + optionDescriprion + '" style="margin-bottom:5px"/>';
                                    $('#dynamic').append(elem);
                                }





                            }
                        }

                    }


                }
                /************** Enf of Function to generate or remove dynamic text fields *************/

                /************** Function on change of task selector *************/

                $('#tasklistid').change(function () {
                    clearValidations();
                    var selectedFunction = $('#selectfunctionid').val();
                    GenerateAndRemoveDynamicFields(selectedFunction);

                });
                /************** End of Function on change of task selector *************/


            });
            /************** End of Page Load Functions *************/

            /************** Function to clear valiodations *************/

            function clearValidations() {

                $("#subSectionTaskAddFormId").validate().resetForm();
                $("#subSectionTaskAddFormId label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });

             //   $('.invalid').remove();

            }


            /************** End of Function to clear valiodations *************/

            /************** Function on click of clear button *************/

            $('#resetBtnId').click(function () {
                DisableSaveButton(false);

                $('#msg_dev').empty();
                $('#dynamic').empty();
                $('#dynamicLbl').empty();
                dynamicFields = [];
                clearValidations();
                $('#sectionid').val('');
                $('#subsectionid').empty();
                $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));

                $("#tasklistid").prop('disabled', false);
                $('#tasklistid').empty();
                $('#tasklistid').select2("val", 0);




            });

            /************** End of Function on click of clear button *************/

            /************** Load Dropdown list of sub sections *************/
            function GetSubSectionAndTaskList() {


                var sectionId = $('#sectionid').val();

                $('#dynamic').empty();
                $('#dynamicLbl').empty();
                $("#tasklistid").prop('disabled', false);
                $('#tasklistid').empty();
                $('#tasklistid').select2("val", 0);
                DisableSaveButton(false);
                $('#subsectionid').empty();
                $('#msg_dev').empty();
                if (sectionId !== '') {
                    $.ajax({
                        type: "GET",
                        url: '${pageContext.servletContext.contextPath}/subsectiontask/getsubsectionsandtasksbysectionid',
                        cache: false,
                        dataType: 'json',
                        data: {'sectionId': sectionId},
                        success: function (response) {

                            result = response.data;

                            for (var i = 0; i < result.length; i++) {

                                $('#subsectionid').append($("<option></option>").attr("value", result[i].id).text(result[i].value));
                            }

                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading.Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }
            }

            /************** End of Load Dropdown list of sub sections *************/


            /************** Load Dropdown list of sub sections *************/
            function GetTaskList() {


                var sectionId = $('#sectionid').val();
                var subsectionId = $('#subsectionid').val();
                $('#tasklistid').empty();
                $('#tasklistid').select2("val", 0);
                $('#msg_dev').empty();
                if (sectionId !== '' && subsectionId !== '') {
                    $.ajax({
                        type: "GET",
                        url: '${pageContext.servletContext.contextPath}/subsectiontask/gettasksbysectionidandsubsectionid',
                        cache: false,
                        dataType: 'json',
                        data: {'sectionId': sectionId, 'subsectionId': subsectionId},
                        success: function (response) {

                            result = response.data;


                            for (var i = 0; i < result.length; i++) {

                                $('#tasklistid').append($("<option></option>").attr("value", result[i].id).text(result[i].value));


                            }
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading.Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }
            }

            /************** End of Load Dropdown list of sub sections *************/

            /***************************** Load Table **********************/
            var responsiveHelper_dt_basic = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            var search_table = $('#subSectionTaskTableId').dataTable({
                searching: false, //to disable default search function in data table
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#subSectionTaskTableId'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();

                    var permissions2 = JSON.parse($('#permissions').val());

                    if (!permissions2.update) {

                        $('.fa-pencil').parent().parent().remove();
                    }
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/subsectiontask/getsubsectionsandtasksbysectionid",
                "fnServerData": function (sSource, aoData, fnCallback) {

                    aoData.push({"name": "sectionsearchid", "value": $('#sectionsearchid').val()});
                    aoData.push({"name": "subsectionsearchid", "value": $('#subsectionsearchid').val()});
                    aoData.push({"name": "tasklistsearchid", "value": $('#tasklistsearchid').val()});



                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data) {

                            fnCallback(data);
                            $('#msg_dev1').empty();
                            $('#search_btn').removeClass('disabled');
                            if (data.CODE === 'ERROR') {
                                $('#msg_dev1').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + data.MESSAGE + ' </div> <br/>');
                                $('#search_btn').addClass('disabled');
                                $('#sectionsearchid').append($("<option></option>").attr("value", '').text('--Select--'));
                                $('#subsectionsearchid').append($("<option></option>").attr("value", '').text('--Select--'));
                                $('#tasklistsearchid').append($("<option></option>").attr("value", '').text('--Select--'));
                                $("#msg_dev1").fadeIn().delay(3000).fadeOut('slow');
                            }
                        }

                    });
                },
                "order": [[0, "asc"]],
                "aoColumns": [
                    {"mDataProp": "section", "bSortable": false},
                    {"mDataProp": "subsection", "bSortable": false},
                    {"mDataProp": "task", "bSortable": false},
                    {"mDataProp": "url", "bSortable": false},
                    {"mDataProp": "lastupdatedtime", "bSortable": false},
                    {"mDataProp": "createdtime", "bSortable": false},
                    {"mDataProp": "createduser", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}

                ]
            });
            /* END BASIC */

            $('#search_btn').click(function () {
                $('#tasklistid').empty();
                $('#sectionid').val('');
                $('#subsectionid').empty();
                $('#tasklistid').select2("val", "");
                $('#dynamic').empty();
                $('#dynamicLbl').empty();
                dynamicFields = [];
                $('#msg_dev').empty();
                search_table.fnDraw();

            });
            /*************************** End Load Table **********************/
            /************** Function to clear data table *************/
            function clearTable() {
                search_table.clear().draw();
            }
            /************** End of Function to clear data table *************/



            /************** Function to hold add or edit vlue *************/

            function setFunctionAddOrEdit(selectedFunction) {
                $('#selectfunctionid').val(selectedFunction);
            }


            /************** End of Function to hold add or edit vlue *************/


            /************** Jquery validation Function *************/

            var form = $('#subSectionTaskAddFormId').validate({
                onkeyup: function (element) {
                    $(element).valid();
                },
                onfocusout: function (element) {
                    $(element).valid();
                },
                rules: {
                    task: {
                        required: true
                    },
                    section: {
                        required: true
                    },
                    subsection: {
                        required: true
                    }, url: {
                        required: true
                    }
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });

            $.validator.methods.count_children = function (value, element) {
                var is_empty = $(element).is(":empty");
                return !is_empty;
            }
            /************** End of Jquery validation Function *************/

            /************** Function to add or edit task *************/
            $('#btnAddSubSectionTask').click(function () {

                if ($("#subSectionTaskAddFormId").valid()) {
                    var selectedFunction = $('#selectfunctionid').val();

                    var sectionId = $('#sectionid').val();
                    var subSectionId = $('#subsectionid').val();

                    var objArr = [];

                    $('[data-url-id]').each(function () {
                        var obj = new Object();
                        obj.sectionId = sectionId;
                        obj.subSectionId = subSectionId;
                        obj.taskId = $(this).attr('data-url-id');
                        obj.url = $(this).val();
                        objArr.push(obj);
                    });


                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: '${pageContext.servletContext.contextPath}/subsectiontask/addsubsectiontask?action=' + selectedFunction + '&sectionId=' + sectionId + '&subSectionId=' + subSectionId,
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(objArr),
                        success: function (result) {

                            if (result.success) {
                                 window.scrollTo(0, 0);
                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong>  ' + result.message + '</div> <br/>');
                                $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + result.message + '</div> <br/>');
                                window.scrollTo(0, 0);
                                $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            }
                            $('#dynamic').empty();
                            $('#dynamicLbl').empty();


                            $("#tasklistid").prop('disabled', false);
                            dynamicFields = [];
                            clearValidations();
                            resetDropdowns();
                            setFunctionAddOrEdit('save');
                            search_table.fnDraw();
                            $('#sectionid').val('');
                            $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));

                        },
//                        beforeSend: function () {
//                            $('#btnAddSubSectionTask').button('loading');
//                        },
//                        complete: function () {
//                            $('#btnAddSubSectionTask').button('reset');
//                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                            $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                        }
                    });

                }

            });

            /************** End of Function to add or edit task *************/


            /************** Function of table edit adn view icon click *************/
            $('#subSectionTaskTableId').on('click', 'tr a', function () {

                resetDropdowns();
                $('#dynamic').empty();
                $('#dynamicLbl').empty();
                $('#msg_dev').empty();
                $('#tasklistid').select2("val", "");
                var id = $(this).attr('id');

                var splitData = id.split(':');

                var sectionId = splitData[0];
                var subSectionId = splitData[1];

                var value = $(this).attr('value');
                if (value === "view") {
                    DisableSaveButton(true);

                    $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));
                } else if (value === "edit") {

                    setFunctionAddOrEdit('edit');
                    DisableSaveButton(false);

                    $('#subsectionid').append($("<option></option>").attr("value", '').text('-- Select --'));
                }
                getSubSectionTasksOnEditAndView(sectionId, subSectionId, value);
            });
            /************** End of Function of table edit and view icon click *************/

            /********** Load Data To Input Fields ***********/
            function loadDataToField(response) {
                result2 = response.data;

                var selectedTasks = [];
                for (var i = 0; i < result2.length; i++) {

                    $('#tasklistid').append($("<option></option>").attr("value", result2[i].taskid).text(result2[i].taskdescription));
                    var obj = {id: result2[i].taskid, text: result2[i].taskdescription};
                    selectedTasks.push(obj);

                    if (jQuery.inArray(result2[i], dynamicFields) == -1) {
                        dynamicFields.push(result2[i].taskid);
                        var elem1 = '<label class="label clearFields" data-task-id="' + result2[i].taskid + '" style="margin-top:15px">' + result2[i].taskdescription + ' URL <samp style="color: red">*</samp> </label>';
                        $('#dynamicLbl').append(elem1);
                        var elem = '<input class="input-sm clearFields" id = "url" name = "url" type="text" data-url-id="' + result2[i].taskid + '" value="' + result2[i].url + '" style="margin-bottom:5px"/>';
                        $('#dynamic').append(elem);
                    }
                }


                $('#tasklistid').data().select2.updateSelection(selectedTasks);

            }
            /********* End Load Data To Input Fields **********/

            /************** Function to set tasks on talbe view and edit buttons clicked *************/

            function getSubSectionTasksOnEditAndView(sectionId, subSectionId, value) {

                $('#msg_dev').empty();
                $("#sectionid").val(sectionId);
                $('#subsectionid').empty();

                if (sectionId !== '') {
                    $.ajax({
                        type: "GET",
                        url: '${pageContext.servletContext.contextPath}/subsectiontask/gettasksbysectionidandsubsectionidoneditandview',
                        cache: false,
                        dataType: 'json',
                        data: {'sectionId': sectionId, 'subsectionId': subSectionId},
                        success: function (response) {


                            result = response.data2;



                            for (var i = 0; i < result.length; i++) {

                                $('#subsectionid').append($("<option></option>").attr("value", result[i].id).text(result[i].value));

                            }
                            loadDataToField(response);

                            if (value === "view") {

                                $("#sectionid").prop('disabled', true);
                                $("#subsectionid").prop('disabled', true);
                                $("#tasklistid").prop('disabled', true);                               
                                $(".clearFields").prop('disabled', true);


                            } else if (value === "edit") {

                                $("#sectionid").prop('disabled', true);
                                $("#subsectionid").prop('disabled', true);
                                $("#tasklistid").prop('disabled', false);
                                $(".clearFields").prop('disabled', false);

                            }


                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading.Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }
            }



            /************** End of Function to set tasks on talbe view and edit buttons clicked *************/

            /************** Function to disable save button *************/
            function DisableSaveButton(active) {
                if (active) {
                    $('#btnAddSubSectionTask').addClass('disabled');

                } else {
                    $('#btnAddSubSectionTask').removeClass('disabled');

                }
            }

            /************** End Function to disable save button *************/

            /************** Function to clear selectors *************/

            function resetDropdowns() {

                $('#sectionid').val('');
                $('#subsectionid').empty();
                $('#tasklistid').select2("val", "");
                $('#sectionsearchid').val('');
                $('#subsectionsearchid').val('');
                $('#tasklistsearchid').val('');
            }

            /************** End of Function to clear fields *************/

            /************** Function to disable all fields *************/

            function disableForm() {

                $('#sectionid').prop('disabled', true);
                $('#subsectionid').prop('disabled', true);
                $('#tasklistid').prop('disabled', true);

            }

            /************** End of Function to disable all fields *************/
            /**************** Function to handle page permissions *****************/

            function refreshPermissions(permissions) {

                if (!permissions.search) {

                    $('#search_btn').attr('disabled', 'disabled');
                    $('#sectionsearchid').attr('disabled', 'disabled');
                    $('#subsectionsearchid').attr('disabled', 'disabled');
                    $('#tasklistsearchid').attr('disabled', 'disabled');

                }
                if (!permissions.create) {
                    disableForm();
                    $('#btnAddSubSectionTask').attr('disabled', 'disabled');
                    $('#resetBtnId').attr('disabled', 'disabled');

                }
            }
            /**************** End of Function to handle page permissions *****************/
        </script>
        <!-- End Javascript functions -->
    </body>
</html>
