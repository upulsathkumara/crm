<%-- 
    Document   : audittrace
    Created on : Sep 19, 2017, 3:07:41 AM
    Author     : ASHOK
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>


            <!-- MAIN CONTENT -->
            <div id="content">

                <!-- START OF VIEW SECTION TAG -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Audit Trace Search
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <!-- END OF VIEW SECTION TAG -->

                <!-- SEARCH BOX -->
                <form:form class="smart-form" id="callCenterForm" commandName="audittraceSearch">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">From Date</label>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input id="from_date" class="input-sm form-control" path="from_date" data-mask="2099-99-99" onchange="clearTable()" />
                                </label>
                                <i></i>
                            </section>

                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">To Date</label>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input id="to_date" class="input-sm form-control" path="to_date" data-mask="2099-99-99" onchange="clearTable()" />
                                </label>
                                <i></i>
                            </section>  
                        </div>
                    </div>        

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff"></footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Affected Page</label>
                                <label class="select">
                                    <form:select id="caseTypeId1" name="caseTypeId1" path="affectedpage" class="select2 input-sm" style="width:100%" items="${affectedPageList}"/>
                                </label>
                                <i></i>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Task</label>
                                <label class="select">
                                    <form:select id="caseTypeId2" name="caseTypeId2" path="task" class="select2 input-sm" style="width:100%" items="${taskList}"/>
                                </label>
                                <i></i>
                            </section>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff"></footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Affected User</label>
                                <label class="input">
                                    <form:select id="caseTypeId3" name="caseTypeId3" path="createduser" class="select2 input-sm" style="width:100%" items="${createdUserList}"/>
                                </label>
                                <i></i>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="search_btn" type="button" class="btn btn-primary" value="Search">Search</button>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </form:form>
                <!-- END OF SEARCH BOX -->

                <!-- START OF TABLE -->
                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Audit Trace Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="audittrace_table" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone"> Audit Trace Id</th>
                                                    <th data-class="expand">   Affected Id</th>
                                                    <th data-class="expand">   Page</th>
                                                    <th data-hide="phone"> Task </th>
                                                    <th data-class="expand"> Description  </th>
                                                    <th data-hide="phone">   </i> Responsible user</th>
                                                    <th data-hide="phone,tablet"> Created DateTime</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->										
                        </article>
                        <!-- WIDGET END -->
                    </div>								
                </section>
                <!-- END OF TABLE -->
            </div>
            <!-- END MAIN PANEL -->

            <!-- PAGE FOOTER -->
            <div class="page-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <span class="txt-color-white">All rights reserved Commercial Credit PLC | Powered by Avonet Technologies</span>
                    </div>
                </div>
            </div>
            <!-- END PAGE FOOTER -->


        </div>
        <!-- END MAIN CONTENT -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>


        <script type='text/javascript'>

            $(document).ready(function () {
                console.log("ready!");
                $("#caseTypeId1").select2();
                $("#caseTypeId2").select2();
                $("#caseTypeId3").select2();
                
                $('#from_date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                $('#to_date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });


                /***************************** Load Table **********************/
                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var search_table = $('#audittrace_table').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#audittrace_table'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/audittrace/tableLoad",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "from_date", "value": $.trim($('#from_date').val())});
                        aoData.push({"name": "to_date", "value": $.trim($('#to_date').val())});
                        aoData.push({"name": "affected_page", "value": $.trim($('#caseTypeId1').val())});
                        aoData.push({"name": "task", "value": $.trim($('#caseTypeId2').val())});
                        aoData.push({"name": "updated_user", "value": $.trim($('#caseTypeId3').val())});

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": "${pageContext.servletContext.contextPath}/audittrace/tableLoad",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "audittraceid", "bSortable": false},
                        {"mDataProp": "afectedid", "bSortable": false},
                        {"mDataProp": "afectedpage", "bSortable": false},
                        {"mDataProp": "task", "bSortable": false},
                        {"mDataProp": "description", "bSortable": false},
                        {"mDataProp": "responsibleuser", "bSortable": false},
                        {"mDataProp": "createddatetime", "bSortable": false}
                    ]
                });
                /* END BASIC */

                $('#search_btn').click(function () {
                    search_table.fnDraw();
                });
                /*************************** End Load Table **********************/
            });

        </script>
    </body>
</html>