<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default welcome_page_panal ">
                            <div class="panel-body open_ticket">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-flag-o fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="useropencount">${myOpenTicketCount}</h1>
                                            <h3 class="count_w_2">My Open Tickets </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer open_ticket_footer"><a href="#wid-id-0">View</a><div class="pull-right">
                                    <i class="fa fa-flag-o  icons_1 icons_size "></i></div></div>
                        </div>  
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default welcome_page_panal ">

                            <div class="panel-body inprogress_ticket">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-hourglass-half fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="userinprogresscount">${myInProgressTicketCount}</h1>
                                            <h3 class="count_w_2" > My In-Progress Tickets</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer inprogress_ticket_footer"><a href="#wid-id-0">View</a><div class="pull-right">
                                    <i class="fa fa-hourglass-half  icons_2 icons_size "></i></div> </div>
                        </div>  
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default welcome_page_panal ">

                            <div class="panel-body total_outstanding">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-flag-o fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="orgopencount">${IdlingTicketCount}</h1>
                                            <h3 class="count_w_2">Idling Tickets</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer total_outstanding_footer"><a href="#${MyCallsID}">View</a><div class="pull-right">
                                    <i class="fa fa-flag-o  icons_1 icons_size "></i></div></div>
                        </div>  
                    </div>
                    <!--                    <div class="col-md-3">
                                            <div class="panel panel-default welcome_page_panal ">
                    
                                                <div class="panel-body new_enterns">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-hourglass-half fa-4x icons_size"></i>
                                                        </div> 
                                                        <div class="col-xs-9">
                                                            <div class="pull-right">
                                                                <h1 class="count_w text-right" id="orginprogresscount">${myOrgInProgressTicketCount}</h1>
                                                                <h3 class="count_w_2" > Org. In-Progress Tickets</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer new_enterns_footer"><a href="#">View</a><div class="pull-right">
                                                        <i class="fa fa-hourglass-half  icons_2 icons_size "></i></div> </div>
                                            </div>  
                                        </div>-->
                </div>
                <!--                <div class="row">
                                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                         Widget ID (each widget will need unique ID)
                                        <div class="jarviswidget" id="1" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                                             widget options:
                                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                            data-widget-colorbutton="false"
                                            data-widget-editbutton="false"
                                            data-widget-togglebutton="false"
                                            data-widget-deletebutton="false"
                                            data-widget-fullscreenbutton="false"
                                            data-widget-custombutton="false"
                                            data-widget-collapsed="true"
                                            data-widget-sortable="false"
                
                                            
                                            <header>
                                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                                <h2>Calls Vs Tickets</h2>
                                            </header>
                                             widget div
                                            <div>
                                                 widget edit box 
                                                <div class="jarviswidget-editbox">
                                                     This area used as dropdown edit box 
                                                </div>
                                                 end widget edit box 
                
                                                 widget content 
                                                <div class="widget-body no-padding">
                                                    <div class="row">
                                                        <div class="col-xs-8"></div>
                                                        <div class="col-xs-2"></div>
                                                    </div>
                                                    <div id="nogrid-graph" class="chart no-padding"></div>
                                                    <div id="legend"></div>
                                                </div>
                                                 end widget content 
                                            </div>
                                             end widget div 
                                        </div>
                                         end widget 
                                    </article>
                                     WIDGET END 
                                </div>-->
                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0"
                                 data-widget-colorbutton="false"	
                                 data-widget-editbutton="false"
                                 data-widget-togglebutton="false"
                                 data-widget-deletebutton="false"
                                 data-widget-fullscreenbutton="false"
                                 data-widget-custombutton="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>My Tickets</h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="ticketSections" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th>TICKET ID</th>
                                                    <th>ASSIGNEE</th>
                                                    <th>STATUS</th>
                                                    <th>PRODUCT</th>
                                                    <th>TICKET CATEGORY</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> LAST UPDATED TIME</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> CREATED TIME</th>
                                                    <th>LAST UPDATED USER</th>
                                                    <th><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> ACTION</th>

                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>   
                                    </div>
                                    <!-- end widget content --> 

                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
                <!-- END MAIN CONTENT -->


                <!-- Idiel table 2nd table -->
                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0"
                                 data-widget-colorbutton="false"	
                                 data-widget-editbutton="false"
                                 data-widget-togglebutton="false"
                                 data-widget-deletebutton="false"
                                 data-widget-fullscreenbutton="false"
                                 data-widget-custombutton="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Idling Table</h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="idlingSections" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th>TICKET ID</th>
                                                    <th>TICKET DATE</th>
                                                    <th>ASSIGNEE</th>
                                                    <th>TICKET CATEGORY</th>
                                                    <th>PRODUCT</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> LAST UPDATED TIME</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> CREATED TIME</th>
                                                    <th>CREATED USER</th>
                                                    <th><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> ACTION</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>   
                                    </div>
                                    <!-- end widget content --> 

                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
                <!-- END MAIN CONTENT -->




            </div>

            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="template/jsinclide.jsp"/>

        <script type="text/javascript">


            pageSetUp();
                    $(document).ready(function () {
            console.log("ready!");
                    /***************************** Load Table **********************/
                    var responsiveHelper_dt_basic = undefined;
                    var breakpointDefinition = {
                    tablet: 1024,
                            phone: 480
                    };
                    var search_table = $('#ticketSections').dataTable({
            "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    //"deferRender": true,
                     //"deferLoading": 5,
                    "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#ticketSections'), breakpointDefinition);
                    }
                    },
                    "ro wCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/tableLoad",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({"name": "searchoptionID", "value": $.trim($('#searchId').val())});
                            aoData.push({"name": "searchoptionDES", "value": $.trim($('#searchDes').val())});
                            $.ajax({
                            "dataType": 'json',
                                    "type": "POST",
                                    "url": "${pageContext.servletContext.contextPath}/tableLoad",
                                    "data": aoData,
                                    "success": fnCallback
                            });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                    {"mDataProp": "ticketid", "bSortable": false},
                    {"mDataProp": "assignee", "bSortable": false},
                    {"mDataProp": "status", "bSortable": false},
                    {"mDataProp": "product", "bSortable": false},
                    {"mDataProp": "ticketcategory", "bSortable": false},
                    {"mDataProp": "lastupdatedatetime", "bSortable": false},
                    {"mDataProp": "createddatetime", "bSortable": false},
                    {"mDataProp": "lastupdateduser", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}
                    ]
            });
                    /* END BASIC */

                    $('#sectionSearch').click(function () {
            search_table.fnDraw();
            });
                    /*************************** End Load Table **********************/


                    /**************************Idling table************************/
                    /***************************** Load Table **********************/
                    var responsiveHelper_dt_basic = undefined;
                    var breakpointDefinition = {
                    tablet: 1024,
                            phone: 480
                    };
                    var search_table = $('#idlingSections').dataTable({
            "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#idlingSections'), breakpointDefinition);
                    }
                    },
                    "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/tableLoadidling",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({"name": "searchoptionID", "value": $.trim($('#searchId').val())});
                            aoData.push({"name": "searchoptionDES", "value": $.trim($('#searchDes').val())});
                            $.ajax({
                            "dataType": 'json',
                                    "type": "POST",
                                    "url": "${pageContext.servletContext.contextPath}/tableLoadidling",
                                    "data": aoData,
                                    "success": fnCallback


                            });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                    {"mDataProp": "ticketid", "bSortable": false},
                    {"mDataProp": "ticketdate", "bSortable": false},
                    {"mDataProp": "assignee", "bSortable": false},
                    {"mDataProp": "product", "bSortable": false},
                    {"mDataProp": "ticketcategory", "bSortable": false},
                    {"mDataProp": "lastupdatedatetime", "bSortable": false},
                    {"mDataProp": "createddatetime", "bSortable": false},
                    {"mDataProp": "createduser", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}
                    ]
            });
                    /* END BASIC */

                    $('#sectionSearch').click(function () {
            search_table.fnDraw();
            });
                    /*************************** End Load Table **********************/




//                $('#nogrid-graph2').length 
////                var day_data = response;
//                    var chart = Morris.Line({
//                        element: 'nogrid-graph2',
//                        grid: true,
//                        data: {},
//                        xkey: 'x',
//                        ykeys: ['y'],
//                        labels: ['Assigned Ticket Count'],
//                        hideHover: 'auto',
//                        resize: false,
//                        parseTime: false
//                    });
//                    chart.options.labels.forEach(function (label, i) {
//                        var legendItem = $('<span></span>').text(label).css('color', chart.options.lineColors[i])
//                        $('#legend2').append(legendItem);
//                    });


            });
        </script>
    </body>
</html>
