<%-- 
    Document   : organization
    Created on : Sep 19, 2017, 8:45:45 PM
    Author     : Kaushan Fernando
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Organization Add

                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  id="organizationAddFormId" commandName="organizationAddForm" novalidate="novalidate" class="smart-form">
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value="0"/>
                    <input id="selectfunctionid" name="selectfunction" type="hidden" value="save"/>
                    <input id="organizationId" name="organization" type="hidden" value=""/>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Organization Code<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="organizationcodeid" name="organizationcode" placeholder="" type = text value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Organization Name<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="organizationnameid" name="organizationname" placeholder="" type = text value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Email<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="emailid" name="email" placeholder="" type = email value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Contact No<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="contactnoid" name="contactno" placeholder="" type = "tel" value=""/>
                                    <i></i>
                                </label>
                            </section>                            
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Activation Date<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input id="activationdateid" name="activationdate" class="input-sm"/>
                                    <i></i>
                                </label>
                            </section>                          
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Date Joined<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input id="joineddateid" name="joineddate" class="input-sm"/>

                                    <i></i>
                                </label>
                            </section>                              
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">License<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="licenseid" path = "license" items="${licenselist}" cssClass="input-sm" />
                                    <i></i>
                                </label>                                 
                            </section>
                        </div>
                        <div class="col-xs-2"></div>


                        <div class="col-xs-3">
                            <section>
                                <label class="label">Status<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="statusid" path = "status" items="${statuslist}" cssClass="input-sm" />
                                    <i></i>
                                </label>
                            </section>                          
                        </div>
                        <div class="col-xs-2"></div>                      

                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">

                                <form:button id="btnAddOrganization" type="button" class="btn btn-primary btn-spinner">Save</form:button>
                                <form:button id="resetBtnId" type="button" class="btn btn-default">Clear</form:button>    
                                </footer>
                            </div>

                            <div class="col-xs-1"></div>
                        </div>
                </form:form>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Organization Search   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev1" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>




                <!-- SEARCH BOX -->
                <div class="row">
                    <!-- NEW COL START -->
                    <form>
                        <form:form commandName="organizationSearchForm" novalidate="novalidate" class="smart-form" >
                            <article class="col-sm-12 col-md-12 col-lg-6">

                                <div class="input-group input-group-sm ">
                                    <input type="text" id="organizationSearchId" name="organizationSearch" class="form-control" placeholder="Organization Name" value="" >
                                    <div class="input-group-btn">
                                        <form:button id="search_btn" type="button" class = "btn btn-primary" value = "Search">Search</form:button>
                                        </div>
                                    </div>
                                </article>
                        </form:form>
                    </form>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p style="color: white">|</p>
                    </div>
                </div>
                <!-- END OF SEARCH BOX -->



                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="true">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Organization Details</h2>

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="userRoleTaskTableId" class="table table-bordered">
                                            <thead>			                
                                                <tr>

                                                    <th data-class="expand">Organization Code</th>
                                                    <th data-class="expand">Organization Name</th>
                                                    <th data-class="expand">Email</th>
                                                    <th data-class="expand">Contact No</th>
                                                    <th data-class="expand">License</th>
                                                    <th data-class="expand">Date Joined</th>
                                                    <th data-class="expand">Activation Date</th>
                                                    <th data-class="expand">Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Date Time</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Date Time</th>
                                                    <th data-class="expand">Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>
        <!-- Style for edit and find buttons in table -->

        <!-- Start Javascript functions -->
        <script type="text/javascript">

            /************** Page Load Functions *************/
            $(document).ready(function () {
                $('#statusid').val("1");
                $('#statusid').attr("disabled", "disabled");
                var permissions = JSON.parse($('#permissions').val());
                refreshPermissions(permissions);
                //getLocation();


//                function getLocation() {
//
//                    $.ajax({
//                        headers: {
//                            'Accept': 'application/json',
//                            'Content-Type': 'application/json'
//                        },
//                        url: "${pageContext.servletContext.contextPath}/organization/addlocation",
//                        type: 'GET',
//                        dataType: 'json',
//                        cache: false,
//                        //data: {'organizationId': id},
//                        success: function (result) {
//
//                            var response = result;
//                            console.log("Response : " + response);
//                        }
////                        error: function () {
////                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading Tasks Please Re-Try!</div> <br/>');
////                            window.scrollTo(0, 0);
////                        }
//                    });
//                }
            });
            /************** End of Page Load Functions *************/

            /************** Function to set date picker *************/

            $('#activationdateid').datetimepicker({
                format: "YYYY-MM-DD",
                maxDate: new Date()
            });

            $('#joineddateid').datetimepicker({
                format: "YYYY-MM-DD",
                maxDate: new Date()
            });


            /************** End of Function to set date picker *************/

            /************** Reset Button on click Function *************/

            $('#resetBtnId').on('click', function () {
                $('#msg_dev').empty();
                clearValidations();
                resetDropdowns();
            });
            /************** End of Reset Button on click Function *************/

            /***************************** Load Table **********************/
            var responsiveHelper_dt_basic = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            var search_table = $('#userRoleTaskTableId').dataTable({
                searching: false, //to disable default search function in data table
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#userRoleTaskTableId'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();

                    var permissions2 = JSON.parse($('#permissions').val());

                    if (!permissions2.update) {

                        $('.fa-pencil').parent().parent().remove();
                    }
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/organization/getorganizationdetailstotable",
                "fnServerData": function (sSource, aoData, fnCallback) {

                    aoData.push({"name": "organizationid", "value": $.trim($('#organizationSearchId').val())});
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function (data) {

                            fnCallback(data);
                            $('#msg_dev1').empty();
                            $('#search_btn').removeClass('disabled');
                            $('#organizationSearchId').prop('disabled', false);
                            if (data.CODE === 'ERROR') {
                                $('#msg_dev1').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + data.MESSAGE + ' </div> <br/>');
                                $("#msg_dev1").fadeIn().delay(3000).fadeOut('slow');
                                $('#search_btn').addClass('disabled');
                                $('#organizationSearchId').prop('disabled', true);

                            }
                        }
                    });
                },
                "order": [[0, "asc"]],
                "aoColumns": [
                    {"mDataProp": "organizationcode", "bSortable": false},
                    {"mDataProp": "organizationname", "bSortable": false},
                    {"mDataProp": "email", "bSortable": false},
                    {"mDataProp": "contactno", "bSortable": false},
                    {"mDataProp": "license", "bSortable": false},
                    {"mDataProp": "datejoined", "bSortable": false},
                    {"mDataProp": "activationdate", "bSortable": false},
                    {"mDataProp": "status", "bSortable": false},
                    {"mDataProp": "lastupdatedtime", "bSortable": false},
                    {"mDataProp": "createdtime", "bSortable": false},
                    {"mDataProp": "createduser", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}

                ]
            });
            /*************************** End Load Table **********************/

            /*************************** Search button on click function **********************/

            $('#search_btn').click(function () {
                $('#msg_dev').empty();
                search_table.fnDraw();
                $('#organizationSearchId').empty();
            });
            /*************************** Search button on click function **********************/

            /************** Function to clear data table *************/
            function clearTable() {
                search_table.clear().draw();
            }
            /************** End of Function to clear data table *************/





            /************** Function to hold add or edit value *************/

            function setFunctionAddOrEdit(selectedFunction) {
                $('#selectfunctionid').val(selectedFunction);
            }

            /************** End of Function to hold add or edit value *************/

            /************** Function to hold organization id *************/

            function setOrganizationId(orgId) {
                $('#organizationId').val(orgId);
            }

            /************** End of Function to hold organization id **************/


            /************** Jquery validation Function *************/

            var form = $('#organizationAddFormId').validate({
                onkeyup: function (element) {
                    $(element).valid();
                },
                onfocusout: function (element) {
                    $(element).valid();
                },
                rules: {
                    status: {
                        required: true
                    },
                    license: {
                        required: true
                    },
                    organizationcode: {
                        required: true,
                        maxlength: 16
                    },
                    organizationname: {
                        required: true,
                        maxlength: 128
                    },
                    email: {
                        required: true,
                        maxlength: 32
                    },
                    contactno: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 14
                    },
                    activationdate: {
                        required: true
                    },
                    joineddate: {
                        required: true
                    }


                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });
            $.validator.methods.count_children = function (value, element) {
                var is_empty = $(element).is(":empty");
                return !is_empty;
            }
            /************** End of Jquery validation Function *************/

            /************** Function to add or edit task *************/
            $('#btnAddOrganization').click(function () {

                if ($("#organizationAddFormId").valid()) {
                    var selectedFunction = $('#selectfunctionid').val();
                    var organizationId;
                    if (selectedFunction === 'edit') {
                        organizationId = $('#organizationId').val();
                    } else {
                        organizationId = null;
                    }

                    var organizationObj = {
                        "organizationid": organizationId,
                        "organizationcode": $('#organizationcodeid').val(),
                        "description": $('#organizationnameid').val(),
                        "email": $('#emailid').val(),
                        "contactno": $('#contactnoid').val(),
                        "activationdate": $('#activationdateid').val(),
                        "datejoined": $('#joineddateid').val()
                    };
                    var licenseObj = {
                        "licenseid": $('#licenseid').val()
                    };
                    var statusObj = {
                        "statusid": $('#statusid').val()
                    };
                    organizationObj = JSON.stringify(organizationObj);
                    licenseObj = JSON.stringify(licenseObj);
                    statusObj = JSON.stringify(statusObj);

                    $.ajax({
                        method: "POST",
                        cache: false,
                        url: "${pageContext.servletContext.contextPath}/organization/addorganization?action=" + selectedFunction,
                        data: {organizationInfo: organizationObj, licenseInfo: licenseObj, statusInfo: statusObj},
                        success: function (result) {
                            $('#msg_dev').empty();

                            if (result.CODE === 'ERROR') {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + result.MESSAGE + ' </div> <br/>');
                                $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                            } else {
                                result = JSON.parse(result);

                                if (result.success) {
                                    window.scrollTo(0, 0);
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong>  ' + result.message + '</div> <br/>');
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                    // $('#statusid').val("1");
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + result.message + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                    //$('#statusid').val("1");
                                }

                            }

                            clearValidations();
                            search_table.fnDraw();
                            setFunctionAddOrEdit('save');
                            //$('#statusid').val("1");
                            $('#licenseid').val("");
                            resetDropdowns();
                        },
//                        beforeSend: function () {
//                            $('#btnAddOrganization').button('loading');
//                        },
//                        complete: function () {
//                            $('#btnAddOrganization').button('reset');
//                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                            $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                        }
                    });
                }

            });
            /************** End of Function to add or edit task *************/


            /************** Function of table edit adn view icon click *************/
            $('#userRoleTaskTableId').on('click', 'tr a', function () {
                clearValidations();
                resetDropdowns();
                var id = $(this).attr('id');
                var value = $(this).attr('value');
                if (value === "view") {
                    DisableSaveButton(true);
                    $('#statusid').val("");
                    $('#licenseid').val("");
                } else if (value === "edit") {

                    setFunctionAddOrEdit('edit');
                    setOrganizationId(id);
                    DisableSaveButton(false);
                    //$('#statusid').val("");
                    $('#licenseid').val("");
                }
                getOrganizationsOnEditAndView(id);
            });
            /************** End of Function of table edit and view icon click *************/

            /************** Function to set tasks on talbe view and edit buttons clicked *************/

            function getOrganizationsOnEditAndView(id) {
                $('#msg_dev').empty();
                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    url: '${pageContext.servletContext.contextPath}/organization/getOrganiztion',
                    type: 'GET',
                    dataType: 'json',
                    data: {'organizationId': id},
                    success: function (result) {

                        var response = result;
                        $("#organizationcodeid").val(response.data[0].organizationCode);
                        $("#organizationnameid").val(response.data[0].organizationDescription);
                        $("#emailid").val(response.data[0].email);
                        $("#contactnoid").val(response.data[0].contactNo);
                        $("#activationdateid").val(response.data[0].activationDate);
                        $("#joineddateid").val(response.data[0].dateJoined);
                        $('#statusid').val(response.data[0].statusId);
                        $('#licenseid').val(response.data[0].licenseId);
                    }, error: function () {
                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading Tasks Please Re-Try!</div> <br/>');
                        window.scrollTo(0, 0);
                    }
                });
            }

            /************** End of Function to set tasks on talbe view and edit buttons clicked *************/

            /************** Function to disable save button *************/
            function DisableSaveButton(active) {
                if (active) {
                    $('#btnAddOrganization').addClass('disabled');
                    $('#organizationcodeid').prop('disabled', true);
                    $('#organizationnameid').prop('disabled', true);
                    $('#emailid').prop('disabled', true);
                    $('#contactnoid').prop('disabled', true);
                    $('#activationdateid').prop('disabled', true);
                    $('#joineddateid').prop('disabled', true);
                    $('#statusid').prop('disabled', true);
                    $('#licenseid').prop('disabled', true);
                } else {
                    $('#statusid').prop('disabled', false);
                    $('#licenseid').prop('disabled', false);
                    $('#btnAddOrganization').removeClass('disabled');
                    $('#organizationcodeid').prop('disabled', true);
                    $('#organizationnameid').prop('disabled', true);
                    $('#activationdateid').prop('disabled', true);
                    $('#joineddateid').prop('disabled', true);
                }
            }

            /************** End Function to disable save button *************/
            /************** Function to disable all fields *************/

            function disableForm() {

                $('#organizationcodeid').prop('disabled', true);
                $('#organizationnameid').prop('disabled', true);
                $('#emailid').prop('disabled', true);
                $('#contactnoid').prop('disabled', true);
                $('#activationdateid').prop('disabled', true);
                $('#joineddateid').prop('disabled', true);
                $('#licenseid').prop('disabled', true);
                $('#statusid').prop('disabled', true);

            }

            /************** End of Function to disable all fields *************/

            /************** Function to clear selectors *************/

            function resetDropdowns() {


                $('#btnAddOrganization').removeClass('disabled');
                $('#organizationcodeid').prop('disabled', false);
                $('#organizationnameid').prop('disabled', false);
                $('#emailid').prop('disabled', false);
                $('#contactnoid').prop('disabled', false);
                $('#activationdateid').prop('disabled', false);
                $('#joineddateid').prop('disabled', false);
                $('#organizationcodeid').val("");
                $('#organizationnameid').val("");
                $('#emailid').val("");
                $('#contactnoid').val("");
                //$('#licenseid').val("");
                $('#licenseid').attr("disabled", false);
                $('#statusid').val("1");
                $('#statusid').attr("disabled", "disabled");

                var date = new Date();
                var dateStr = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
                $('#joineddateid').val(dateStr);
                $('#activationdateid').val(dateStr);
                //$('#statusid').val("");
                $('#licenseid').val("");
            }

            /************** End of Function to clear fields *************/

            /************** Function to clear valiodations *************/

            function clearValidations() {

                $("#organizationAddFormId").validate().resetForm();
                $("#organizationAddFormId label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });
            }

            /************** End of Function to clear valiodations *************/

            /**************** Function to handle page permissions *****************/

            function refreshPermissions(permissions) {
                if (!permissions.search) {
                    $('#search_btn').attr('disabled', 'disabled');
                    $('#organizationSearchId').prop('disabled', true);


                }
                if (!permissions.create) {
                    disableForm();
                    $('#btnAddOrganization').attr('disabled', 'disabled');
                    $('#resetBtnId').attr('disabled', 'disabled');

                }
            }
            /**************** End of Function to handle page permissions *****************/

        </script>
        <!-- End Javascript functions -->
    </body>
</html>
