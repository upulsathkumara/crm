<%-- 
    Document   : client
    Created on : 17/08/2017, 2:38:18 PM
    Author     : Nadun Chamikara
--%>

<%@page import="java.util.HashMap"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>


    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'/>
                <input id="action" value="create" hidden/>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 id="add_update_view_title" class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Client <span id="client-dynamic-title" class="dynamic_title">Add</span>
                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  commandName="clientAddForm" novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div id="msg_dev" class="col-xs-8">

                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <h3 class="sub-title">Client Details</h3>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>


                    <input id="clientid" val="" hidden>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <section>
                                <label class="label">Client Name<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <form:input path="name" type="text" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>

                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <section>
                                <label class="label">Address<samp style="color: red">*</samp></label>
                                <label class="textarea">
                                    <form:textarea path="address" type="text" class="input-sm" rows="4"></form:textarea>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Organization<samp style="color: red">*</samp></label>
                                    <label class="select">
                                    <%--<form:select path="organization" class="input-sm" onChange="changeOrganization('organization')" items="${organizationList}"/>--%>
                                    <form:select path="organization" class="input-sm" items="${organizationList}"/>

                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Client Type<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <!--                                    <select id="clientcategory" name="clientcategory" class="input-sm" disabled="disabled">
                                                                            <option value="">-- Please Select an Organization --</option>
                                                                        </select>-->
                                    <form:select  id="clientcategory" path="clientcategory" class="input-sm select2-container select2-container-multi select2" multiple="true" items="${clientCategoryList}"  onchange="getProductCategory()"/>

                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Primary Contact Person<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <form:input path="primarycontactperson" type="text" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Email<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <form:input path="email" type="email" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Contact Number 1<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <form:input path="contactno01" type="tel" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Contact Number 2</label>
                                <label class="input">
                                    <form:input path="contactno02" type="tel" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>


                    <!--                    <div class="row">
                                            <div class="col-xs-2"></div>
                                            <div class="col-xs-3">
                                                <section>
                                                    <label class="label">Username<samp style="color: red">*</samp></label>
                                                    <label class="input">
                                                        <input type="text" class="input-sm" disabled="" value="DISABLED"/>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-xs-2"></div>
                                            <div class="col-xs-3">
                                                <section>
                                                    <label class="label">Password<samp style="color: red">*</samp></label>
                                                    <label class="input">
                                                        <input type="password" class="input-sm" disabled="" value="DISABLED"/>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-xs-2"></div>
                                        </div>-->

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Status<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select path="status" class="input-sm" items="${statusList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Username<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="username" name="username" type="text" class="input-sm" />
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </form:form>
                <form:form  commandName="clientProductAddForm" novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <h3 class="sub-title">Product Details</h3>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div stylea="border:1px solid black">
                        <div class="row">
                            <input id="clientproductelem" type="text" hidden value="[]"/>

                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Product Category<samp style="color: red">*</samp></label>
                                    <label class="select">
                                        <form:select id="productcategory" name="productcategory" path="" class="input-sm" />

                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Purchased Date<samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-calendar"></i>
                                        <input id="purchaseddate" name="purchaseddate" class="input-sm"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>

                        </div>

                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Product<samp style="color: red">*</samp></label>
                                    <label class="select">
                                        <!--<select id="product" class="input-sm"></select>-->
                                        <select id="product" name="product" class="input-sm" disabled="disabled">
                                            <option value="">-- Please Select a Product Category --</option>                                    
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>

                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Current Status<samp style="color: red">*</samp></label>
                                    <label class="select">
                                        <select id="clientproductstatus" name="clientproductstatus" class="input-sm"></select>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3"></div>
                            <div class="col-xs-2"></div>

                            <div class="col-xs-3">
                                <section>
                                    <label class="label"><br/></label>
                                    <label class="input text-right">
                                        <button id="productAddBtn" type="button" onclick="productAdd()" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Add</button>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>

                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-8">
                                <table id="client-product-table" class="table table-bordered table-responsivea table-hover">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Purchased Date</th>
                                            <th>Current Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    </tbody>
                                </table>
                                <br>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                    </div>
                </form:form>
                <form novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2" style="margin-left:14px"></div>
                        <div class="col-xs-8">
                            <footer style="background-color: #ffffff">
                                <!--<button onclick="pageSubmit()" id="btnAddClient" type="button" class="btn btn-primary" data-loading-text-client="<i class='fa fa-spinner fa-spin '></i> Saving">Save</button>-->
                                <button onclick="pageSubmit()" id="btnAddClient" type="button" class="btn btn-primary btn-spinner">Save</button>
                                <button onclick="clearAll()" id="btnClear" type="button" class="btn btn-default">Clear</button>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-1"></div>
                    </div>
                </form>



                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Client Search   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <!--                <div class="row">
                                    <div class="col-xs-1"></div>
                                    <div id="msg_dev" class="col-xs-10" tabindex="0"></div>
                                    <div class="col-xs-1"></div>
                                </div>-->


                <!--                <div class="row">
                                    <div class="col-xs-offset-1 col-xs-10">
                                        <div class="smart-form">
                                            <div class="row" style="padding-bottom: 30px;">
                                                <div class="col-xs-12">
                                                    <header><h4>User Role Section Search</h4></header>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->




                <!--
                                <form commandName="userRoleSectionSearchForm" novalidate="novalidate" class="smart-form">
                
                                    <div class="row">
                                        <div class="col-xs-2"></div>
                                        <div class="col-xs-3">
                                            <section>
                                                <label class="label">User Role</label>
                                                <label class="select">
                                                    <select path="userrole" cssClass="input-sm" items="${userRoleList}"></select>
                                                    <i></i>
                                                </label>
                                            </section>
                
                                        </div>
                                        <div class="col-xs-2"></div>
                                        <div class="col-xs-3">
                                            <section>
                                                <label class="label">Section</label>
                                                <label class="select">
                                                    <select path="section" cssClass="input-sm" items="${sectionList}"></select>
                                                    <i></i>
                                                </label>
                                            </section>  
                                        </div>
                                    </div>
                
                                    <div class="row">
                                        <div class="col-xs-1"></div>
                                        <div class="col-xs-10">
                                            <footer style="background-color: #ffffff">
                                                <button id="search_btn" type="button" class="btn btn-primary" value="Submit">Search</button>  
                                            </footer>
                                        </div>
                                        <div class="col-xs-1"></div>
                                    </div>
                                </form>
                -->                

                <form:form commandName="clientSearchForm" novalidate="novalidate" class="smart-form">

                    <div class="row">                        
                        <div class="col-xs-2"></div>
                        <div class="col-xs-2">
                            <section>
                                <label class="label">Name</label>
                                <label class="input">
                                    <form:input id="search_name" path="name"  class="input-sm"/>
                                </label>
                            </section>

                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                            <section>
                                <label class="label">Organization</label>
                                <label class="select">
                                    <%--<form:select id="search_organization" path="organization" onChange="changeOrganization('search_organization')" class="input-sm" items="${organizationList}"/>--%>
                                    <form:select id="search_organization" path="organization" class="input-sm" items="${organizationList}"/>

                                    <i></i>
                                </label>
                            </section>

                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                            <section>
                                <label class="label">Client Type</label>
                                <label class="select">
                                    <!--                                    <select id="search_clientcategory" class="input-sm" disabled="disabled">
                                                                            <option value="">-- Please Select an Organization --</option>
                                                                        </select>-->
                                    <form:select id="search_clientcategory" path="clientcategory" class="input-sm" items="${clientCategoryListSearch}"/>

                                    <i></i>
                                </label>
                            </section>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2" style="margin-left:14px"></div>
                        <div class="col-xs-8">
                            <footer style="background-color: #ffffff">
                                <button id="search_btn" type="button" class="btn btn-primary">Search</button>  
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-1"></div>
                    </div>
                </form:form>             


                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" 
                                 data-widget-colorbutton="false"	
                                 data-widget-editbutton="false"
                                 data-widget-togglebutton="false"
                                 data-widget-deletebutton="false"
                                 data-widget-fullscreenbutton="false"
                                 data-widget-custombutton="false"
                                 data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Client results</h2>

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <span id="delete-client-id" class="hidden"></span>
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Organization</th>
                                                    <th>Client Type</th>
                                                    <th>Location</th>
                                                    <th>Status</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Time</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Time</th>
                                                    <th>Created User</th>
                                                    <th><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>

                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>


        <!-- Modal -->
        <!--        <div id="clientDeleteModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
        
                         Modal content
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Client Delete Confirm</h4>
                            </div>
                            <div class="modal-body">
                                <div id="msg_dev_delete_modal"></div>
                                <p>Are you sure want to delete this client?</p>
                            </div>
                            <div class="modal-footer">
                                <span id="modal-client-delete-id" class="hidden"></span>
                                <button id="modal-client-delete-btn" type="button" class="btn btn-danger">Delete</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
        
                    </div>
                </div>-->


        <style>
            .btn-table-action{
                color: #3276b1;
                cursor: pointer
            }
            .btn-table-action.disabled{
                color: #93adc4;
                cursor: default
            }
            .dynamic_title{
                font-size: 24px !important;    
            }
            .sub-title{
                margin-top: 30px;
                margin-bottom: 20px;
                padding-bottom: 5px;
                border-bottom: 1px solid #ccc;
            }
        </style>

        <script type="text/javascript">
            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            var search = false;
            var search_table;
            $(document).ready(function () {

                var permissions = JSON.parse($('#permissions').val());
                refreshPermissions(permissions);
                $('#productcategory').append($("<option></option>").attr("value", '').text('-- Select --'));
                pageSetUp();
                /* // DOM Position key index //
                 
                 l - Length changing (dropdown)
                 f - Filtering input (search)
                 t - The Table! (datatable)
                 i - Information (records)
                 p - Pagination (paging)
                 r - pRocessing 
                 < and > - div elements
                 <"#id" and > - div with an id
                 <"class" and > - div with a class
                 <"#id.class" and > - div with an id and class
                 
                 Also see: http://legacy.datatables.net/usage/features
                 */

                /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                search_table = $('#dt_basic').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();

                        if (!permissions.delete) {
                            $('.client-delete-icon').parent().remove();
                        }
                        if (!permissions.update) {
                            $('.client-edit-icon').parent().remove();
                        }
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/client/getclientstotable",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "name", "value": $('#search_name').val()});
                        aoData.push({"name": "organizationid", "value": $('#search_organization').val()});
                        aoData.push({"name": "clientcategoryid", "value": $('#search_clientcategory').val()});
                        $.ajax({
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "name", "bSortable": false},
                        {"mDataProp": "organization", "bSortable": false},
                        {"mDataProp": "clientcategory", "bSortable": false},
                        {"mDataProp": "location", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "lastupdateddatetime", "bSortable": false},
                        {"mDataProp": "createddatetime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });
                /* END BASIC */

                $('#purchaseddate').datetimepicker({
                    format: "YYYY-MM-DD",
                    maxDate: new Date()
                });


                $('#search_btn').click(function () {
                    $('#msg_dev').empty();
                    clearValidations();
                    clearTable();
                    enableFormFields(true);
                    search = true;
                    search_table.fnDraw();
                    clearForm();
                    setClientProducts("[]");
                });
                $('#status').val("1");
                $('#status').attr("disabled", "disabled");
                $('#dt_basic').on('click', '.client-view-icon', function () {
                    $('#msg_dev').empty();
                    clearValidations();
                    clearForm();
                    var clientId = $(this).attr('data-client-id');
                    getDataAndfillFields(clientId, "view", function () {
                        enableFormFields(false);
//                        $('#clientcategory').attr("disabled", "disabled");
                    });
                    changeDynamicTitle('View');
                    $('#action').val('create');

                }); // client view


                $('#dt_basic').on('click', '.client-edit-icon', function () {
                    $('#msg_dev').empty();
                    clearValidations();
                    clearForm();
                    var clientId = $(this).attr('data-client-id');
                    getDataAndfillFields(clientId, "edit", enableFormFields(true));
                    $('#status').removeAttr("disabled");
                    changeDynamicTitle('Edit');
                    $('#action').val('update');
                    $('#username').attr('disabled', 'disabled');
                }); // client edit


                $('#dt_basic').on('click', '.client-delete-icon', function () {
                    $('#msg_dev').empty();
                    clearValidations();
                    var clientId = $(this).attr('data-client-id');
////                    alert(clientId);
//                    $('#clientDeleteModal').modal('show');
//                    $('#modal-client-delete-id').html(clientId);
                    $('#delete-client-id').html(clientId);
                    var clientName = $(this).closest('tr').find("td:first").html();

                    $.SmartMessageBox({
                        title: "Delete Client : " + clientName,
                        content: "Are you sure want to delete this client?",
                        buttons: '[Delete][Cancel]'
                    }, function (ButtonPress, Value) {
                        if (ButtonPress == "Delete") {
                            deleteClient();
                            return 0;
                        }
                    });




                }); // search table client delete button

//                $('#clientDeleteModal').on('show.bs.modal', function () {
//                    $('#msg_dev_delete_modal').html();
//                });
//                $('#clientDeleteModal').on('click', '#modal-client-delete-btn', function () {
//                    var clientId = $('#modal-client-delete-id').html();
//                    $.ajax({
//                        type: "POST",
//                        url: "${pageContext.servletContext.contextPath}/client/deleteclient?clientid=" + clientId,
//                        cache: false,
//                        success: function (response) {
//                            console.log(response);
//                            response = JSON.parse(response);
//                            if (response.CODE === "SUCCESS") {
//                                $('#clientDeleteModal .close').click();
//                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Deleted</div> <br/>');
//                                window.scrollTo(0, 0);
//                            } else {
//                                $('#msg_dev_delete_modal').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
//                                window.scrollTo(0, 0);
//                            }
//                        },
//                        error: function () {
//                            $('#msg_dev_delete_modal').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
//                            window.scrollTo(0, 0);
//                        }
//                    });
//                }); // modal client delete button


            });

            function deleteClient() {
                var clientId = $('#delete-client-id').html();
                $.ajax({
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/client/deleteclient?clientid=" + clientId,
                    cache: false,
                    success: function (response) {
                        console.log(response);
                        response = JSON.parse(response);
                        if (response.CODE === "SUCCESS") {
                            $('#delete-client-id').html('');
                            $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Deleted</div>');
                            window.scrollTo(0, 0);
                            clearTable();
                        } else {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> ');
                            window.scrollTo(0, 0);
                        }
                    },
                    error: function () {
                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div>');
                        window.scrollTo(0, 0);
                    }
                });
            }

            function clearTable() {
                search = false;
//                $('#dt_basic').dataTable().fnClearTable();
                search_table.fnClearTable();
            }

            function enableFormFields(enable) {
                if (enable) {
                    $('#name').removeAttr('disabled');
                    $('#organization').removeAttr('disabled');
                    $('#clientcategory').removeAttr('disabled');
                    $('#primarycontactperson').removeAttr('disabled');
                    $('#email').removeAttr('disabled');
                    $('#contactno01').removeAttr('disabled');
                    $('#contactno02').removeAttr('disabled');
                    $('#address').removeAttr('disabled');
                    $('#username').removeAttr('disabled');
//                    $('#status').removeAttr('disabled');
                    $('#productAddBtn').removeAttr('disabled');
                    $('#btnAddClient').removeAttr('disabled');
                    $('#productcategory').removeAttr('disabled');
                    $('#product').removeAttr('disabled');
                    $('#purchaseddate').removeAttr('disabled');
                    $('#clientproductstatus').removeAttr('disabled');
//                    $('#btnAddClient').show();
                    $('#btnAddClient').removeAttr('disabled');

                } else {
                    $('#name').attr('disabled', 'disabled');
                    $('#organization').attr('disabled', 'disabled');
                    $('#clientcategory').attr('disabled', 'disabled');
                    $('#primarycontactperson').attr('disabled', 'disabled');
                    $('#email').attr('disabled', 'disabled');
                    $('#contactno01').attr('disabled', 'disabled');
                    $('#contactno02').attr('disabled', 'disabled');

                    $('#address').attr('disabled', 'disabled');
                    $('#username').attr('disabled', 'disabled');
                    $('#status').attr('disabled', 'disabled');
                    $('#productAddBtn').attr('disabled', 'disabled');
                    $('#btnAddClient').attr('disabled', 'disabled');
                    $('#productcategory').attr('disabled', 'disabled');
                    $('#product').attr('disabled', 'disabled');
                    $('#purchaseddate').attr('disabled', 'disabled');
                    $('#clientproductstatus').attr('disabled', 'disabled');
//                    $('#btnAddClient').hide();
                    $('#btnAddClient').attr('disabled', 'disabled');

                }
            }



        </script>

        <script type="text/javascript">
//            var clientCategoryInitialOption = '<option value="">-- Please Select an Organization --</option>';
            /************** Load product category by client type *************/
            function getProductCategory() {
//                alert("Action removed");
//                return;



                var clientType = $('#clientcategory').val();

                $('#productcategory').empty();
                $('#msg_dev').empty();
                if (clientType !== '') {
                    $.ajax({
                        type: "POST",
                        url: '${pageContext.servletContext.contextPath}/client/getproductcategory',
                        cache: false,
                        dataType: 'json',
                        data: {clientType: JSON.stringify(clientType)},
                        success: function (response) {
                            if (response.CODE == "SUCCESS") {
                                result = response.DATA;
                                for (var i = 0; i < result.length; i++) {

                                    $('#productcategory').append($("<option></option>").attr("value", result[i].id).text(result[i].value));
                                }
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                            }


                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error While loading Sub sections Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }
            }


            $('#clientcategory').change(function () {
                $("#productcategory").trigger("change");
            });

            /************** End of Load product category by client type *************/
            function pageSubmit() {
                clearTopMessage();

                if ($('#clientAddForm').valid()) {


                    var dataObject = new Object();
                    if ($('#clientid').val() != "") {
                        dataObject.clientid = $('#clientid').val();
                    } else {
                        dataObject.clientid = "";
                    }

                    dataObject.name = $('#name').val();
//                    dataObject.clientcategory = parseInt($('#clientcategory').val());
                    dataObject.clientcategory = $('#clientcategory').val();

                    dataObject.organization = parseInt($('#organization').val());
                    dataObject.primarycontactperson = $('#primarycontactperson').val();
                    dataObject.email = $('#email').val();
                    dataObject.contactno01 = $('#contactno01').val();
                    dataObject.contactno02 = $('#contactno02').val();

                    dataObject.address = $('#address').val();
                    dataObject.username = $('#username').val();
                    dataObject.status = parseInt($('#status').val());
                    dataObject.clientproducts = getClientProducts();
//                    console.log(dataObject);

                    var content = JSON.stringify(dataObject);
                    // console.log(content);
                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/client/createorupdateclient?action=" + $('#action').val(),
                        cache: false,
                        data: content,
                        dataType: 'json',
                        success: function (response) {
                            //response = JSON.parse(response);
                            // console.log(response);
                            if (response.CODE === "SUCCESS") {
                                $('#status').attr("disabled", "disabled");
                                clearForm();
//                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> ');
                                search_table.fnDraw();
                                clearValidations();
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> ');
                                window.scrollTo(0, 0);
                            }
                        },
                        error: function (e) {
//                            console.log(e);
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div>');
                            window.scrollTo(0, 0);
                        }
//                        ,
//                        beforeSend: function () {
//                            $('#btnAddClient').html($('#btnAddClient').attr('data-loading-text-client'));
//                        },
//                        complete: function () {
//                            $('#btnAddClient').html('Save');
//                        }
                    });
                }
//                else {
//                    $('#msg_dev').html('<div class="alert alert-danger"><strong>Error!</strong> All fields are required!</div> <br/>');
//                    window.scrollTo(0, 0);
//                }
            }

            function productAdd() {
//                    if (isValidProductsAddForm()) {

                if ($('#clientProductAddForm').valid()) {
                    var obj = new Object();
                    obj.product = $('#product').val();
                    obj.clientproductstatus = $('#clientproductstatus').val();
                    obj.purchaseddate = $('#purchaseddate').val();
                    console.log(obj);

                    var clientProducts = getClientProducts();
                    clientProducts.push(obj);
                    setClientProducts(JSON.stringify(clientProducts));
//                        console.log("ClientProducts: ", clientProducts);
                    var deleteBtn = "<div class='col-md-4'>"
                            + "<li class='fa fa-lg fa-fw fa-trash btn-table-action product-delete-icon' data-product-id='" + obj.product + "' title='Delete'></li>"
                            + "</div>";
                    clientProductTable.row.add([$("#product option[value='" + obj.product + "']").text(), obj.purchaseddate, $("#clientproductstatus option[value='" + obj.clientproductstatus + "']").text(), deleteBtn]).draw();
                    showHideSelectedOption(obj.product, 'hide');
                    clearProductAddForm();
                }
            }


            $(document).ready(function () {

                $('#clientAddForm').validate({
                    // TODO : check disabled inputs
                    rules: {
                        name: {
                            required: true,
                            maxlength: 64
                        },
                        organization: {
                            required: true
                        },
                        clientcategory: {
                            required: true
                        },
                        primarycontactperson: {
                            required: true,
                            maxlength: 64
                        },
                        email: {
                            required: true,
                            maxlength: 32,
                            email: true,
                            remote: {
                                url: "${pageContext.servletContext.contextPath}/client/isvalidemail",
                                data: {'clientid': function () {
                                        return $('#clientid').val();
                                    }
                                },
                                async: false
                            }
                        },
                        contactno01: {
                            required: true,
                            number: true,
                            minlength: 10,
                            maxlength: 14
                        },
                        contactno02: {
                            number: true,
                            minlength: 10,
                            maxlength: 14
                        },
                        address: {
                            required: true,
                            maxlength: 1024
                        },
                        username: {
                            required: true,
                            maxlength: 32,
                            remote: "${pageContext.servletContext.contextPath}/user/view/isvaliduserid"
                        },
                        status: {
                            required: true
                        }
                    },
                    messages: {
                        email: {
                            remote: "Email duplicate"
                        },
                        username: {
                            remote: "Username duplicate"
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }

                });

                $('#clientProductAddForm').validate({
                    rules: {
                        productcategory: {
                            required: true
                        },
                        purchaseddate: {
                            required: true
                        },
                        product: {
                            required: true
                        },
                        clientproductstatus: {
                            required: true
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }

                });
//                $('#search_organization, #search_clientcategory').on('change', function () {
//                    clearTable();
//                });
//                $('#search_name').on('input', function () {
//                    var delay = (function () {
//                        var timer = 0;
//                        return function (callback, ms) {
//                            clearTimeout(timer);
//                            timer = setTimeout(callback, ms);
//                        };
//                    })();
//
//                    $('input').keyup(function () {
//                        delay(function () {
//                            clearTable();
//                        }, 500);
//                    });
//                });




            }); // $(document).ready() end




            /***************************************************/
            /*
             function changeOrganization(elem, callback) {
             var elemId;
             var organizationid;
             if (elem == "organization") {
             elemId = "organization";
             } else {
             elemId = "search_organization";
             }
             
             if (elemId == "organization") {
             $('#clientcategory').empty();
             $('#clientcategory').append(clientCategoryInitialOption);
             organizationid = $('#organization').val();
             } else {
             $('#search_clientcategory').empty();
             $('#search_clientcategory').append(clientCategoryInitialOption);
             organizationid = $('#search_organization').val();
             }
             
             
             if (organizationid) {
             //var organizationid = $(this).val();
             $.ajax({
             type: "POST",
             url: "${pageContext.servletContext.contextPath}/client/clientcategoriesbyorganization?organizationid=" + organizationid,
             cache: false,
             success: function (response) {
             response = JSON.parse(response);
             // console.log(response);
             if (response.CODE === "SUCCESS") {
             clientCategories = response.DATA;
             if (clientCategories.length > 0) {
             
             if (elemId == "organization") {
             $('#clientcategory').removeAttr("disabled");
             $('#clientcategory').empty();
             } else {
             $('#search_clientcategory').removeAttr("disabled");
             $('#search_clientcategory').empty();
             }
             
             }
             for (var i = 0; i < clientCategories.length; i++) {
             var option = "<option value='" + clientCategories[i].id + "'>" + clientCategories[i].description + "</option>";
             if (elemId == "organization") {
             $('#clientcategory').append(option);
             } else {
             $('#search_clientcategory').append(option);
             }
             }
             if (callback) {
             callback();
             }
             //                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
             } else {
             $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
             window.scrollTo(0, 0);
             }
             },
             error: function () {
             $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
             window.scrollTo(0, 0);
             }
             });
             } else {
             // $('#clientcategory').empty();
             if (elemId == "organization") {
             $('#clientcategory').attr("disabled", "disabled");
             } else {
             $('#search_clientcategory').attr("disabled", "disabled");
             }
             }
             } // organization on change
             */
            /***************************************************/



            function clearForm() {
                // TODO: clear disabled inputs

                $('#clientid').val('');
                $('#name').val('');
                $('#organization').val('');

                $('#clientcategory').select2("val", null);
//                $('#clientcategory').append(clientCategoryInitialOption);
                $('#primarycontactperson').val('');
                $('#email').val('');
                $('#contactno01').val('');
                $('#contactno02').val('');

                $('#address').val('');
                $('#username').val('');

//                $('#status').val('');
                $('#modal-client-delete-id').html('');
                clearProductAddForm();
                clearProductTable();
                var clientProducts = getClientProducts();
//                console.log("CLIENT_PRODUCTS: ", clientProducts);

                clearValidations();
            }

            /************** Function to clear valiodations *************/

            function clearValidations() {

                $("#clientAddForm").validate().resetForm();
                $("#clientAddForm label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });


                $("#clientProductAddForm").validate().resetForm();
                $("#clientProductAddForm label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });


            }

            /************** End of Function to clear valiodations *************/

            function changeDynamicTitle(title) {
                if (title != 'Add') {
                    $('html, body').animate({
                        scrollTop: $('#add_update_view_title').offset().top
                    }, 400);
                }
                $('#client-dynamic-title').html(title);
            }

            function getClientProducts() {
                return JSON.parse($('#clientproductelem').val());
            }
            function setClientProducts(val) {
                $('#clientproductelem').val(val);
//                console.log("CLIENT_PRODUCTS: ", getClientProducts());
            }

        </script>

        <script type="text/javascript">
            var clientProductTable;
            $(document).ready(function () {

                clientProductTable = $('#client-product-table').DataTable({
                    searching: false,
                    lengthChange: false
                });

                $.ajax({
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/client/getclientproductstatuslist",
                    cache: false,
                    success: function (response) {
                        response = JSON.parse(response);
                        // console.log(response);
                        if (response.CODE === "SUCCESS") {
                            var products = response.DATA;
                            for (var i = 0; i < products.length; i++) {
                                var option = "<option value='" + products[i].id + "'>" + products[i].description + "</option>";
                                $('#clientproductstatus').append(option);
                            }
//                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                        } else {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div>');
                            window.scrollTo(0, 0);
                        }
                    },
                    error: function () {
                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> ');
                        window.scrollTo(0, 0);
                    }
                });
                $('#productcategory').change(function () {

                    var productCategoryId = $(this).val();
                    $('#product').empty();
                    if (productCategoryId != "") {
                        $.ajax({
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/client/getproductlistbycategory?productcategoryid=" + productCategoryId,
                            cache: false,
                            success: function (response) {
                                response = JSON.parse(response);
                                // console.log(response);
                                if (response.CODE === "SUCCESS") {
                                    var products = response.DATA;
                                    if (products.length > 0) {
                                        $('#product').removeAttr("disabled");
                                        $('#product').empty();
                                    }
                                    var clientProducts = getClientProducts();
                                    for (var i = 0; i < products.length; i++) {
                                        var option = "<option value='" + products[i].id + "'>" + products[i].description + "</option>";
                                        $('#product').append(option);
                                        if (($.grep(clientProducts, function (e) {
                                            return e.product == products[i].id;
                                        })).length > 0) {
                                            showHideSelectedOption(products[i].id, 'hide');
                                        }
                                    }
//                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> ');
                                    window.scrollTo(0, 0);
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> ');
                                window.scrollTo(0, 0);
                            }
                        });
                    } else {
                        var option = "<option value=''>-- Please Select a Product Category --</option>";
                        $('#product').append(option);
                        $('#product').attr("disabled", "disabled");
                    }
                });

//                $('#productAddBtn').click(function (e) {
//                    e.preventDefault();
////                    if (isValidProductsAddForm()) {
//                    if ($('#clientProductAddForm').valid()) {
//                        alert("valid")
//                    } else {
//                        alert("invalid")
//                    }
//                    if ($('#clientProductAddForm').valid()) {
//                        var obj = new Object();
//                        obj.product = $('#product').val();
//                        obj.clientproductstatus = $('#clientproductstatus').val();
//                        obj.purchaseddate = $('#purchaseddate').val();
//                        console.log(obj);
//
//                        var clientProducts = getClientProducts();
//                        clientProducts.push(obj);
//                        setClientProducts(JSON.stringify(clientProducts));
////                        console.log("ClientProducts: ", clientProducts);
//                        var deleteBtn = "<div class='col-md-4'>"
//                                + "<li class='fa fa-lg fa-fw fa-trash btn-table-action product-delete-icon' data-product-id='" + obj.product + "' title='Delete'></li>"
//                                + "</div>";
//                        clientProductTable.row.add([$("#product option[value='" + obj.product + "']").text(), obj.purchaseddate, $("#clientproductstatus option[value='" + obj.clientproductstatus + "']").text(), deleteBtn]).draw();
//                        showHideSelectedOption(obj.product, 'hide');
//                        clearProductAddForm();
//                    }
//                });

                $('#client-product-table').on('click', '.product-delete-icon', function () {
                    var productId = $(this).attr('data-product-id');
                    var elem = $(this);
                    var productName = $(this).closest('tr').find("td:first").html();

                    $.SmartMessageBox({
                        title: "Remove Product : " + productName,
                        content: "Are you sure want to remove this product?",
                        buttons: '[Remove][Cancel]'
                    }, function (ButtonPress, Value) {
                        if (ButtonPress == "Remove") {
                            $('#msg_dev').empty();
                            clearValidations();
                            showHideSelectedOption(productId, 'show');
                            var clientProducts = getClientProducts();

                            for (var i = 0; i < clientProducts.length; i++) {
                                if (clientProducts[i].product == productId) {
                                    clientProducts.splice(i, 1);
                                }
                            }

                            setClientProducts(JSON.stringify(clientProducts));
                            clientProductTable.row($(elem).parents('tr')).remove().draw();

                            return 0;
                        }
                    });



                });
            });


            function clearProductAddForm() {
                $('#productcategory').val('');
//                $('#product').val('');
                $('#clientproductstatus').val('');
                var date = new Date();
                $('#purchaseddate').val(date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate());
                $('#product').empty();
                var option = "<option value=''>-- Please Select a Product Category --</option>";
                $('#product').append(option);
                $('#product').attr("disabled", "disabled");

                $("#clientProductAddForm").validate().resetForm();
                $("#clientProductAddForm label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-sucess');

                });
            }


            function clearProductTable() {
                setClientProducts('[]');
                clientProductTable.clear().draw();
            }

            function showHideSelectedOption(val, display) {
                if (display === "show") {
                    $("#product option[value='" + val + "']").removeAttr('hidden');
                } else {
                    $("#product option[value='" + val + "']").attr('hidden', 'hidden');
                }
            }

            function isValidProductsAddForm() {
                var isValid = true;
                if ($('#product').val() === "") {
                    isValid = false;
                    $('#product').addClass('invalid');
                }
                if ($('#purchaseddate').val() === "") {
                    isValid = false;
                    $('#purchaseddate').addClass('invalid');
                }
                if ($('#clientproductstatus').val() === "") {
                    isValid = false;
                    $('#clientproductstatus').addClass('invalid');
                }

                return isValid;
            }

            function getDataAndfillFields(clientId, action, callback) {


                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/client/getclientbyid?clientid=" + clientId,
                    cache: false,
                    data: content,
                    dataType: 'json',
                    success: function (response) {
                        //response = JSON.parse(response);
                        // console.log(response);
                        if (response.CODE === "SUCCESS") {
//                            console.log("CLIENTBYID: ", response);
                            var data = response.DATA[0];
                            $('#action').val('update');

                            $('#clientid').val(data.clientid);
                            $('#name').val(data.name);
                            $('#organization').val(data.organization);

                            //$('#organization').change();

//            changeOrganization('organization', function () {
//                                $('#clientcategory').val(data.clientcategory);
//                                if (callback) {
//                                    callback();
//                                }
//
//                            });

                            $('#clientcategory').val(data.clientcategory).trigger('change');

                            $('#primarycontactperson').val(data.primarycontactperson);
                            $('#email').val(data.email);
                            $('#contactno01').val(data.contactno01);
                            $('#contactno02').val(data.contactno02);

                            $('#address').val(data.address);
                            $('#username').val(data.username);

                            $('#status').val(data.status);
                            var products = data.clientproducts;
                            var clientProducts = new Array();
                            for (var i = 0; i < products.length; i++) {
                                var deleteBtn;
                                if (action == "view") {
                                    deleteBtn = "<div class='col-md-4'>"
                                            + "<li class='fa fa-lg fa-fw fa-trash btn-table-action disabled' title='Delete'></li>"
                                            + "</div>";
                                } else {
                                    deleteBtn = "<div class='col-md-4'>"
                                            + "<li class='fa fa-lg fa-fw fa-trash btn-table-action product-delete-icon' data-product-id='" + products[i].product.id + "' title='Delete'></li>"
                                            + "</div>";
                                }

                                clientProductTable.row.add([products[i].product.description, products[i].purchaseddate, products[i].status.description, deleteBtn]).draw();
                                var obj = new Object();
                                obj.product = products[i].product.id;
                                obj.clientproductstatus = products[i].status.id;
                                obj.purchaseddate = products[i].purchaseddate;
//                                console.log(obj);
                                clientProducts.push(obj);
                                showHideSelectedOption(obj.product, 'hide');
                            }
                            setClientProducts(JSON.stringify(clientProducts));

                            if (callback) {
                                callback();
                            }
                        } else {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> ');
                            window.scrollTo(0, 0);
                        }
                    },
                    error: function (e) {
//                        console.log(e);
                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> ');
                        window.scrollTo(0, 0);
                    }
                });
            }

            function clearAll() {

                $('#msg_dev').empty();
                clearValidations();
                clearForm();
                enableFormFields(true);
                $('#action').val('create');

            }

            function clearTopMessage() {
                $('#msg_dev').empty();
            }

            function refreshPermissions(permissions) {
                if (!permissions.search) {
                    $('#search_btn').attr('disabled', 'disabled');
                    $('#search_name').attr('disabled', 'disabled');
                    $('#search_organization').attr('disabled', 'disabled');
                    $('#search_clientcategory').attr('disabled', 'disabled');
                }
                if (!permissions.create) {
                    enableFormFields(false);
                    $('#btnAddClient').attr('disabled', 'disabled');
                    $('#productAddBtn').attr('disabled', 'disabled');
                    $('#btnClear').attr('disabled', 'disabled');
                }

            }

        </script>
    </body>
</html>