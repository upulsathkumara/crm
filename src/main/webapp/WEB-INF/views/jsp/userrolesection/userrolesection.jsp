<%-- 
    Document   : userrolesection
    Created on : 15/08/2017, 12:12:06 PM
    Author     : Nadun Chamikara
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>


    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 id="add_update_view_title" class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            User Role Section <span id="user-role-section-dynamic-title" class="dynamic_title">Add</span>
                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  commandName="userRoleSectionAddForm" novalidate="novalidate" class="smart-form">
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value=""/>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">

                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">User Role<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="userroleid" path="userrole" cssClass="input-sm" items="${userRoleList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row" id="multibranchselect" >
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section</label>
                                <label class="select">
                                    <%--<form:select id="sectionid" path="section" cssClass="input-sm" items="${sectionList}" multiple="multiple" readonly="true" style="height:100px"/>--%>
                                    <select id="sectionid"  class="input-sm" multiple="multiple" readonly="true" style="height:100px"></select>

                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2">
                            <section>
                                <label class="label" style="height:19px;"></label>
                            </section>
                            <div class="row">
                                <div class="col-xs-5"></div>
                                <div class="col-xs-2">
                                    <section>
                                        <button id="pull_right" readonly="true" type="button" class="btn btn-xs">&gt;&gt;</button>
                                    </section>
                                    <section>
                                        <button id="pull_left" readonly="true" type="button" class="btn btn-xs">&lt;&lt;</button>
                                    </section>
                                </div>
                                <div class="col-xs-5"></div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label" style="height:19px;"></label>
                                <label class="select">
                                    <%--<form:select id="multisection" path="section" cssClass="input-sm" multiple="multiple" readonly="true" style="height:100px"/>--%>
                                    <select id="multisection" name="multisection" style="height:100px;" multiple="multiple"></select><input type="hidden" name="_multisection" value="1"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <%--<form:button id="btnAddUserRoleSection" type="button" class="btn btn-primary btn-spinner" data-loading-text-userrolesection="<i class='fa fa-spinner fa-spin '></i> Assigning">Assign</form:button>--%>
                                <form:button id="btnAddUserRoleSection" type="button" class="btn btn-primary btn-spinner">Assign</form:button>
                                <form:button id="btnClear" type="button" class="btn btn-default">Clear</form:button>
                                </footer>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                </form:form>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            User Role Section Search   
                        </h1>
                        <span></span>
                    </div>
                </div>



                <form:form commandName="userRoleSectionSearchForm" novalidate="novalidate" class="smart-form">

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">User Role</label>
                                <label class="select">
                                    <form:select path="userrole" cssClass="input-sm" items="${userRoleList}"/>
                                    <i></i>
                                </label>
                            </section>

                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section</label>
                                <label class="select">
                                    <form:select path="section" cssClass="input-sm" items="${sectionList}"/>
                                    <i></i>
                                </label>
                            </section>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="search_btn" type="button" class="btn btn-primary" value="Submit">Search</button>  
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </form:form>



                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" 
                                 data-widget-colorbutton="false"	
                                 data-widget-editbutton="false"
                                 data-widget-togglebutton="false"
                                 data-widget-deletebutton="false"
                                 data-widget-fullscreenbutton="false"
                                 data-widget-custombutton="false"
                                 data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>User Role Section results</h2>

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th>User Role</th>
                                                    <th>Section</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Time</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Time</th>
                                                    <th>Created User</th>
                                                    <th><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </article>

                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>
        <style>
            .btn-table-action{
                color: #3276b1;
                cursor: pointer
            }
            .dynamic_title{
                font-size: 24px !important;    
            }
        </style>
        <script type='text/javascript'>

            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            var search = false;
            var search_table;

            $(document).ready(function () {
                var permissions = JSON.parse($('#permissions').val());
                refreshPermissions(permissions);
                pageSetUp();
                /* // DOM Position key index //
                 
                 l - Length changing (dropdown)
                 f - Filtering input (search)
                 t - The Table! (datatable)
                 i - Information (records)
                 p - Pagination (paging)
                 r - pRocessing 
                 < and > - div elements
                 <"#id" and > - div with an id
                 <"class" and > - div with a class
                 <"#id.class" and > - div with an id and class
                 
                 Also see: http://legacy.datatables.net/usage/features
                 */

                /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                search_table = $('#dt_basic').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        if (!permissions.assign) {
                            $('.userrolesection-edit-icon').parent().remove();
                        }
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/userrolesection/getuserrolesectionstotable",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "userRoleId", "value": $('#userrole').val()});
                        aoData.push({"name": "sectionId", "value": $('#section').val()});
                        $.ajax({
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "userrole", "bSortable": false},
                        {"mDataProp": "section", "bSortable": false},
                        {"mDataProp": "lastupdatedtime", "bSortable": false},
                        {"mDataProp": "createdtime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });

                $('#search_btn').click(function () {
                    clearTable();
                    resetValidations();
                    resetAddForm(true, true, false);
                    hideMessage();
                    enableEditFields(true);
                    changeDynamicTitle('Add');

                    $('#msg_dev').empty();
                    search = true;
                    search_table.fnDraw();
                });

                $('#btnClear').click(function () {
                    resetValidations();
                    resetAddForm(true, true, false);
                });

                function resetValidations() {
                    $("#userRoleSectionAddForm").validate().resetForm();
                    $("#userRoleSectionAddForm label").each(function () {
                        $(this).removeClass('state-error');
                    });
                }






                // clear table when dropdown values changed
                $('#userrole, #section').on('change', function () {
                    clearTable();
                    hideMessage();
                });
                $('#userroleid').on('change', function () {
                    resetAddForm(false, true, false);
                    hideMessage();
                    if ($(this).val()) {
                        //$('#sectionid').
                        //console.log("SECTION_LIST", ${sectionList});
                        getSections(getUserRoleSectionByUserRoleId($(this).val()));
                        //getUserRoleSectionByUserRoleId($(this).val());

                    } else {
                        $('#sectionid').html('');
                    }
                });

                $('#userRoleSectionAddForm').validate({
                    rules: {
                        userrole: {
                            required: true
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }

                });

                $('#pull_right').click(function () {
                    var selectedItem = $("#sectionid option:selected");
                    $("#multisection").append(selectedItem);
                    selectedItem.prop("selected", false);
                    generateBranchArray();
                });
                $('#pull_left').click(function () {
                    var selectedItem = $("#multisection option:selected");
                    $("#sectionid").append(selectedItem);
                    selectedItem.prop("selected", false);
                    generateBranchArray();
                });
                $('#sectionid').dblclick(function () {
                    var selectedItem = $("#sectionid option:selected");
                    $("#multisection").append(selectedItem);
                    selectedItem.prop("selected", false);
                    generateBranchArray();
                });

                $('#multisection').dblclick(function () {
                    var selectedItem = $("#multisection option:selected");
                    $("#sectionid").append(selectedItem);
                    selectedItem.prop("selected", false);
                    generateBranchArray();
                });

                // add user role section
                $('#btnAddUserRoleSection').click(function () {
                    hideMessage();
                    if ($('#userRoleSectionAddForm').valid()) {
//                        var objArr = [];
                        var sectionIdArr = JSON.parse($('#multisectionarray').val());
                        var obj = new Object();
                        obj.userRoleId = $('#userroleid').val();
                        obj.sectionIds = sectionIdArr;

//                        for (var i = 0; i < sectionIdArr.length; i++) {
//                            var obj = {
//                                "userRoleId": $('#userroleid').val(),
//                                "sectionId": sectionIdArr[i]
//                            };
//                            objArr.push(obj);
//                        }

                        $.ajax({
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            url: '${pageContext.servletContext.contextPath}/userrolesection/addorupdateuserrolesection',
                            type: 'POST',
                            dataType: 'json',
                            //data: JSON.stringify(objArr),
                            data: JSON.stringify(obj),
                            success: function (response) {
//                                console.log(response);

                                if (response.CODE === "SUCCESS") {

                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                    //$('#search_btn').click(); // refresh table
                                    enableEditFields(true);
                                    resetAddForm(true, true, false);
                                    resetValidations();
                                    search_table.fnDraw();
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                window.scrollTo(0, 0);
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#btnAddUserRoleSection').html($('#btnAddUserRoleSection').attr('data-loading-text-userrolesection'));
//                            },
//                            complete: function () {
//                                $('#btnAddUserRoleSection').html('Assign');
//                            }
                        });
                    }
//                    else {
//                        $('#msg_dev').html('<div class="alert alert-danger"><strong>Error!</strong> All fields are required!</div> <br/>');
//                        window.scrollTo(0, 0);
//                    }
                });
                // table edit icon click
                $('#dt_basic').on('click', '.userrolesection-edit-icon', function () {
                    hideMessage();
                    resetValidations();
                    resetAddForm(true, true, false);
                    enableEditFields(true);
                    $('#userroleid').attr('disabled', 'disabled');
                    changeDynamicTitle('Edit');
                    var userRoleId = $(this).attr('data-userrolesection-id').split(':')[0];
                    getSections(getUserRoleSectionByUserRoleId(userRoleId));
                });
                // table view icon click
                $('#dt_basic').on('click', '.userrolesection-view-icon', function () {
                    hideMessage();
                    resetValidations();
                    resetAddForm(true, true, true);
                    enableEditFields(false);
                    changeDynamicTitle('View');
                    var userRoleId = $(this).attr('data-userrolesection-id').split(':')[0];
                    getSections(getUserRoleSectionByUserRoleId(userRoleId));
                });
                function validateFields() {
                    var isValid = true;
                    if (isEmpty($('#userroleid').val()) || isEmpty($('#sectionid').val()) || isEmpty($('#multisectionarray').val())) {
                        isValid = false;
                    }
                    return isValid;
                }
                function isEmpty(val) {
                    if (val === "") {
                        return true;
                    }
                    return false;
                }

                function changeDynamicTitle(title) {
                    if (title != 'Add') {
                        $('html, body').animate({
                            scrollTop: $('#add_update_view_title').offset().top
                        }, 400);
                    }
                    $('#user-role-section-dynamic-title').html(title);
                }

                function getSections(callback) {
                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: '${pageContext.servletContext.contextPath}/userrolesection/getsectionsfordropdown',
                        type: 'POST',
                        dataType: 'json',
                        success: function (response) {
                            var data = response.DATA;

                            if (response.CODE === "SUCCESS") {
                                //$('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                for (var i = 0; i < data.length; i++) {
                                    var option = "<option value='" + data[i].id + "'>" + data[i].description + "</option>";
                                    $('#sectionid').append(option);
                                }
                                callback;

                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                window.scrollTo(0, 0);
                            }
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }

                function getUserRoleSectionByUserRoleId(userRoleId) {
                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: '${pageContext.servletContext.contextPath}/userrolesection/getuserrolesections?userroleid=' + userRoleId,
                        type: 'POST',
                        dataType: 'json',
                        success: function (response) {
                            var data = response.DATA;
                            $('#userroleid').val(userRoleId);
                            if (response.CODE === "SUCCESS") {
                                //$('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                for (var i = 0; i < data.length; i++) {
                                    var option = "<option value='" + data[i].section.id + "'>" + data[i].section.description + "</option>";
                                    $("#multisection").append(option);
                                    $("#sectionid option[value='" + data[i].section.id + "']").remove();
                                    generateBranchArray();
                                }

                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                window.scrollTo(0, 0);
                            }
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                }

                function enableEditFields(status) {
                    if (status) {
                        //enable
                        $('#userroleid').removeAttr('disabled')
                        $('#pull_right').removeClass('disabled');
                        $('#pull_left').removeClass('disabled');
                    } else {
                        // disable
                        $('#userroleid').attr('disabled', 'disabled');
                        $('#pull_right').addClass('disabled');
                        $('#pull_left').addClass('disabled');
                    }
                }

                function hideMessage() {
                    $('#msg_dev').empty();
                }
                function resetValidations() {
                    $("#userRoleSectionAddForm").validate().resetForm();
                    $("#userRoleSectionAddForm label").each(function () {
                        $(this).removeClass('state-error');
                    });
                }
                function resetAddForm(resetUserRole, resetSections, disableSaveBtn) {
                    $('#userroleid').removeAttr('disabled');

                    if (resetSections) {
//                        $("#multisection option").each(function (i, elem) {
//                            $("#sectionid").append(elem);
//                        });
                        $("#sectionid").html('');
                        $("#multisection").html('');
                    }
                    if (resetUserRole) {
                        $('#userroleid').val('');
                    }
                    if (disableSaveBtn) {
                        $('#btnAddUserRoleSection').addClass('disabled');
                    } else {
                        $('#btnAddUserRoleSection').removeClass('disabled');
                    }
                    generateBranchArray();
                }


                function clearTable() {
                    search_table.fnClearTable();
//                    search_table.fnDraw();

                }

                function generateBranchArray() {
                    var section = [];
                    $("#multisection option").each(function () {
                        section.push($(this).val());
                    });
                    $('#multisectionarray').val(JSON.stringify(section));

                    sortSelectOptions('#sectionid');
                    sortSelectOptions('#multisection');

                }

                function sortSelectOptions(elemId) {
                    $(elemId).html($(elemId + ' option').sort(function (a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                    }));
                }

                function refreshPermissions(permissions) {
                    console.log(permissions);
                    if (!permissions.search) {
                        $('#search_btn').attr('disabled', 'disabled');
                        $('#userrole').attr('disabled', 'disabled');
                        $('#section').attr('disabled', 'disabled');
                    }

                    if (!permissions.assign) {
                        enableEditFields(false);
                        $('#btnAddUserRoleSection').attr('disabled', 'disabled');
                        $('#btnClear').attr('disabled', 'disabled');
                    }

                }
            });


        </script>
    </body>
</html>