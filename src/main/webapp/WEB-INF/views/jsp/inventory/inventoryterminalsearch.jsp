<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Search Terminal
                        </h1>
                        <span></span>
                    </div>
                </div>

              

                <div class="col-xs-offset-2 col-xs-8">
                    <form:form class="smart-form" id="terminalSearchForm" commandName="inventoryTerminalCreate">
                        <div class="row">



                            <div class="col-xs-5">

                                <section>
                                    <label class="label">Client</label>
                                    <label class="select">
                                        <form:select path="client" id="client" class="select2" items="${dropdownListClient}" style="width:100%;"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>

                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Brand </label>
                                    <label class="select">
                                        <form:select path="" id="brand" class="select2" items="${dropdownListBrand}" style="width:100%;"/>
                                    </label>
                                </section>
                            </div>

                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Model </label>
                                    <label class="select">
                                        <form:select path="" id="model" class="select2" items="${dropdownListModel}" style="width:100%;"/>
                                    </label>
                                </section>
                            </div>

                        </div>
                                    <div class="row" style="margin-bottom: 10px">
                            
                            <div class="col-xs-12">
                                <section>
                                    <button type="button" id="btnsearch" class="pull-right btn btn-primary btn-sm">Search</button>
                                </section>
                            </div>
                        </div>
                                   
                    </form:form>
                </div>
                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Terminal search results</h2>

                                </header>

                                <!-- widget div-->
                                <div>


                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>  
                                                    <th data-hide="phone">Brand</th>
                                                    <th data-hide="phone,tablet">Model</th>
                                                    <th data-hide="phone">Serial Number</th>
                                                    <th data-hide="phone">Client</th>
                                                    <th data-hide="phone,tablet">Category</th>
                                                    <th data-hide="phone,tablet">Owner</th>
                                                    <th data-hide="phone,tablet">Created Date</th>
                                                    <th data-hide="phone,tablet">Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->

                    </div>								
                </section>
            </div>    
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">
            pageSetUp();

            var search = false;

            $(document).ready(function () {

                /* // DOM Position key index //
                 
                 l - Length changing (dropdown)
                 f - Filtering input (search)
                 t - The Table! (datatable)
                 i - Information (records)
                 p - Pagination (paging)
                 r - pRocessing 
                 < and > - div elements
                 <"#id" and > - div with an id
                 <"class" and > - div with a class
                 <"#id.class" and > - div with an id and class
                 
                 Also see: http://legacy.datatables.net/usage/features
                 */

                /* BASIC ;*/


                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };


                /***********************--Load Table--***************************/
                var search_table = $('#dt_basic').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory/loadtable/terminal",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "search", "value": search});
                        aoData.push({"name": "brandsearch", "value": $.trim($('#brand').val())});
                        aoData.push({"name": "clientsearch", "value": $.trim($('#client').val())});
                        aoData.push({"name": "modelsearch", "value": $.trim($('#model').val())});

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": "${pageContext.servletContext.contextPath}/inventory/loadtable/terminal",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "desc"]],
                    "aoColumns": [
                        {"mDataProp": "brand", "bSortable": false},
                        {"mDataProp": "model", "bSortable": false},
                        {"mDataProp": "serialNo", "bSortable": false},
                        {"mDataProp": "client", "bSortable": false},
                        {"mDataProp": "category", "bSortable": false},
                        {"mDataProp": "owner", "bSortable": false},
                        {"mDataProp": "createddatetime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });
                /***********************--End of load Table--***********************/

                $('#btnsearch').click(function () {
                    search = true;
                    search_table.fnDraw();
                });


//                var search_table = $('#dt_terminal').dataTable({
//                    "paging": true,
//                    "bProcessing": true,
//                    "bServerSide": true,
//                    "bFilter": false,
//                    "sPaginationType": "full_numbers",
//                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
//                            "t" +
//                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
//                    "autoWidth": true,
//                    "preDrawCallback": function () {
//                        // Initialize the responsive datatables helper once.
//                        if (!responsiveHelper_dt_terminal) {
//                            responsiveHelper_dt_terminal = new ResponsiveDatatablesHelper($('#dt_terminal'), breakpointDefinition);
//                        }
//                    },
//                    "rowCallback": function (nRow) {
//                        responsiveHelper_dt_terminal.createExpandIcon(nRow);
//                    },
//                    "drawCallback": function (oSettings) {
//                        responsiveHelper_dt_terminal.respond();
//                    },
//                    "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory/searched/terminal",
//                    "fnServerData": function (sSource, aoData, fnCallback) {
//                        aoData.push({"name": "search", "value": search});
//                        aoData.push({"name": "brandsearch", "value": $('#brandsearch').val()});
//
//                        $.ajax({
//                            "dataType": 'json',
//                            "type": "GET",
//                            "url": "${pageContext.servletContext.contextPath}/inventory/searched/terminal",
//                            "data": aoData,
//                            "success": fnCallback
//                        });
//                    },
//                    "order": [[0, "desc"]],
//                    "aoColumns": [
//                        {"mDataProp": "brand", "bSortable": false},
//                        {"mDataProp": "model", "bSortable": false},
//                        {"mDataProp": "serialNo", "bSortable": false},
//                        {"mDataProp": "client", "bSortable": false},
//                        {"mDataProp": "category", "bSortable": false},
//                        {"mDataProp": "owner", "bSortable": false},
//                        {"mDataProp": "createddatetime", "bSortable": false},
//                        {"mDataProp": "createduser", "bSortable": false},
//                        {"mDataProp": "action", "bSortable": false}
//
//                    ]
//
//                });
                /* END BASIC */
            });

            function clearTable() {
                search = false;
                $('#dt_terminal').dataTable().fnClearTable();
            }

        </script>
    </body>
</html>