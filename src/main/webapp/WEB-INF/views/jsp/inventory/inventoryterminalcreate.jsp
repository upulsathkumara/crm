<%-- 
    Document   : inventorycreate
    Created on : Jun 1, 2017, 12:46:22 PM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Insert Terminal
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10">


                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Terminal Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <form:form id="terminalInsertForm" commandName="inventoryTerminalCreate">
                    <div class="col-xs-offset-2 col-xs-8">

                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">

                                    <section>
                                        <label class="label">Client (Hardware)</label>
                                        <label class="select">
                                            <form:select path="client" id="client" class="select2" items="${dropdownListClient}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Category <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="category" id="category" class="select2" items="${dropdownListCategory}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Brand <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="brand" id="brand" class="select2" items="${dropdownListBrand}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Model</label>
                                        <label class="select">
                                            <form:select path="model" id="model" class="select2" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">TID<samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:input path="tid" id="tid" type="text"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">MID<samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:input path="mid" id="mid" type="text"/>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Serial Number<samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:input path="serialnumber" id="serialnumber" type="text"/>

                                        </label>
                                    </section>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-10">
                            <button  type="button" style="padding-right: 20px;padding-left: 20px" id="btninsert" class="pull-right btn btn-primary btn-spinner btn-sm">Save</button>
                            <button style="padding-right: 20px;padding-left: 20px;margin-right: 20px" id="clearform" type="reset" class="pull-right btn btn-default btn-sm">Clear</button>
                        </div>
                    </div>
                </div>
                <input id="saveaction" name="saveaction" type="text" value="create" hidden/>
            </form:form> 

        </div>

        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer">
        <jsp:include page="../template/footer.jsp"/>
    </div>
    <!-- END PAGE FOOTER -->

    <!-- js file include -->
    <jsp:include page="../template/jsinclide.jsp"/>

    <script type="text/javascript">

        $(document).ready(function () {
            var permissions = JSON.parse($('#permissions').val());
            refreshPermissions(permissions);
            $('#brand').select2();
            $('#category').select2();
            $('#model').select2();
            $('#client').select2();
            $("#serialnumber").mask('999-999-999');

//                $('#addeddate').datetimepicker({
//                    weekStart: 1,
//                    todayBtn: 1,
//                    autoclose: 1,
//                    todayHighlight: 1,
//                    startView: 2,
//                    forceParse: 0,
//                    showMeridian: 1,
//                    format: "yyyy-mm-dd"    // yyyy-mm-dd
//                });
//                $('#addeddate').datetimepicker('setStartDate', new Date());
//                $('#addeddate').datetimepicker('setDate', new Date());
//
//                var date = new Date();
//                date.setHours(23, 59, 59, 999);
//
//                $('#addeddate').datetimepicker('setEndDate', date);

//                var x = $('#terminalInsertForm').validate({
//                    rules: {
//                        brand: {
//                            required: true
//                        },
//                        category: {
//                            required: true
//                        },
//                        tid: {
//                            required: true
//                        },
//                        mid: {
//                            required: true
//                        },
//                        category: {
//                            required: true
//                        }
//
//                    },
//                    messages: {
//                        brand: {
//                            required: "Select the Terminal Brand"
//                        },
//                        category: {
//                            required: "Select the Terminal Categroy"
//                        }
//                    },
//                    onfocusout: false,
//                    invalidHandler: function (form, validator) {
//                        var errors = validator.numberOfInvalids();
//                        if (errors) {
//                            validator.errorList[0].element.focus();
//                        }
//                    }
//                });

            /*******************************************/
            var form = $('#terminalInsertForm').validate({
                onkeyup: function (element) {
                    $(element).valid();
                },
                onfocusout: function (element) {
                    $(element).valid();
                },
                rules: {
                    brand: {
                        required: true
                    },
                    category: {
                        required: true
                    },
                    tid: {
                        required: true
                    },
                    mid: {
                        required: true
                    },
                    client: {
                        required: true
                    }

                },
                messages: {
                    serialnumber: {
                        remote: "Terminal Serial number is duplicated."
                    }
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });
            /********************************************/


            $('#serialnumber').rules('add', {
                required: true,
                maxlength: 32,
                minlength: 11,
                onkeyup: false,
                remote: {
                    url: "${pageContext.servletContext.contextPath}/inventory/validateSerialNumber",
                    type: 'GET',
                    data: {serialnumber: function () {
                            return $('#serialnumber').val();
                        }
                    },
                    async: true,
                }
            });

            $("#btninsert").click(function () {
                // $('#terminalInsertForm').valid();

//                    var dataObject = new Object();
//
//                    var inventorybrand = new Object();
//                    var inventorycat = new Object();
//                    var inventoryclient = new Object();
//                    var inventorymodel = new Object();
//
//                    inventorybrand.inventorybrandid = $("#brand").val();
//                    dataObject.terminalbrand = inventorybrand;  //inventoryproductcategory
//
//                    dataObject.tid = $("#tid").val();
//                    dataObject.mid = $("#mid").val();
//                    dataObject.serialno = $("#serialnumber").val();
//
//                    inventorymodel.inventorymodelid = $("#model").val();
//                    dataObject.terminalmodel = inventorymodel;
//
////                    inventoryowner.employeeid = $("#owner").val();
////                    dataObject.employee = inventoryowner;
//
//                    inventorycat.inventorycategoryid = $("#category").val();
//                    dataObject.inventorycategory = inventorycat;  //inventorycategory
//
//                    inventoryclient.clientid = $("#client").val();
//                    dataObject.client = inventoryclient;  


                /************************--Create Data Object--***************************/
                if ($("#terminalInsertForm").valid()) {
                    var dataObject = new Object();
                    dataObject.terminalbrand = $('#brand').val();
                    dataObject.tid = $('#tid').val();
                    dataObject.mid = $('#mid').val();
                    dataObject.serialno = $('#serialnumber').val();
                    dataObject.terminalmodel = $('#model').val();
                    dataObject.inventorycategory = $('#category').val();
                    dataObject.client = $('#client').val();
                    var selectedFunction = $('#saveaction').val();
                    console.log(dataObject);
                    var content = JSON.stringify(dataObject);
                    console.log(content);


                    $.ajax({
                        type: "post",
                        url: "${pageContext.servletContext.contextPath}/inventory/insert_terminal/done?action=" + selectedFunction,
                        cache: false,
                        data: {terminal_info: content},
                        success: function (response) {
                            response = JSON.parse(response);
                            if (true) {
                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');

                                clearValidations();
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Error in saving record </div> <br/>');
                                window.scrollTo(0, 0);
                            }
                            clear();
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
//                        ,
//                        beforeSend: function () {
//                            $('#btninsert').button('loading');
//                        },
//                        complete: function () {
//                            $('#btninsert').button('reset');
//                        }
                    });


                    /************************--End of Data Object--***************************/

                }
            });

            function clear() {
                $("#serialnumber").val("");
                $("#tid").val("");
                $('#client').val("").change();
                $('#category').val("").change();
                $('#brand').val("").change();
                $('#model').val("").change();
                $("#mid").val("");
                clearValidations();
                $('#pagesubmit').prop('disabled', false);
                $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
            }

            $('#clearform').click(function () {
                $("#serialnumber").val("");
                $("#tid").val("");
                $('#client').val("").change();
                $('#category').val("").change();
                $('#brand').val("").change();
                $('#model').val("").change();
                $("#mid").val("");
                clearValidations();
                $('#pagesubmit').prop('disabled', false);
            });

            function clearValidations() {

                $("#terminalInsertForm").validate().resetForm();
                $("#terminalInsertForm label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });
            }

            $('#brand').change(function () {
                $('#model').empty();
                if ($('#brand').val()) {
                    $.get("${pageContext.servletContext.contextPath}/inventory/getterminalmodels", {
                        terminalbrand: $('#brand').val()
                    }, "json").done(function (data) {
                        $.each(jQuery.parseJSON(data), function (key, value) {
                            $('#model').append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.value));
                            $('#model').trigger('change');
                        });
                    });
                } else {
                    $('#model').append($("<option></option>")
                            .attr("value", "")
                            .text("-- Select --"));
                }
            });


            /**************** Function to handle page permissions *****************/

            function refreshPermissions(permissions) {
                if (!permissions.create) {
                    $("#serialnumber").val(response[0].sortid).prop('disabled', true);
                    $("#mid").val(response[0].icon).prop('disabled', true);
                    $("#tid").val(response[0].subsectionId).prop('disabled', true);
                    $("#model").prop('disabled', true);
                    $("#brand").prop('disabled', true);
                    $("#category").prop('disabled', true);
                    $("#client").prop('disabled', true);
                    $('#btninsert').attr('disabled', 'disabled');
                    // $('#clearform').attr('disabled', 'disabled');
                }
            }
            /**************** End of Function to handle page permissions *****************/


        });


    </script>
</body>
</html>
