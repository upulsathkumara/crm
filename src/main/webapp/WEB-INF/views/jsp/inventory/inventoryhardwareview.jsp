<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            View Hardware Item
                        </h1>
                        <span></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Hardware Item Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form:form id="hardwareItemViewForm" commandName="itemview">
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Added Date <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <form:input path="createddatetime" id="createddatetime" type="date" class="input-sm" readonly="readonly"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Category <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="inventorycategory.inventorycategoryid" id="inventorycategory" class="select2" items="${categoryList}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Part name</label>
                                        <label class="input">
                                            <form:input path="partname" id="partname" type="text"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Part Code</label>
                                        <label class="input">
                                            <form:input path="partcode" id="partcode" type="text"/>
                                        </label>
                                    </section>
                                </div>
                            </div>     

                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Location<samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:select path="location.locationid" id="location" cssClass="input-sm" items="${locationList}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div id="dynamiccontent" style="display: none">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">Revision</label>
                                            <label class="input">
                                                <form:input path="posrevision" id="posrevision" type="text"/>
                                            </label>
                                        </section>
                                    </div>                                
                                </div>
                                <div class="row">
                                    <div class="col-xs-5 pull-right">
                                        <section>
                                            <label class="label">Part ID</label>
                                            <label class="input">
                                                <form:input path="pospartid" id="pospartid" type="text"/>
                                            </label>
                                        </section>
                                    </div>        
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">MAC</label>
                                            <label class="input">
                                                <form:input path="posmac" id="posmac" type="text"/>
                                            </label>
                                        </section>
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-xs-5 pull-right">
                                        <section>
                                            <label class="label">PTID</label>
                                            <label class="input">
                                                <form:input path="posptid" id="posptid" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">Serial No</label>
                                            <label class="input">
                                                <form:input path="posserialnumber" id="posserialnumber" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-1">

                            <%--<form:button style="float:right" id="btnedit" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</form:button>--%>
                            <input id="saveaction" name="saveaction" type="text" value="update" hidden/>

                            <!--<button type="button" id="btnedit" class="btn btn-default btn-sm">Edit</button>-->

                        </div>
                        <div class="col-xs-2"></div>
                    </div>


                </form:form>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">

            $(document).ready(function () {
                disableForm();
                $('#inventorycategory').select2();
                $('#location').select2();

                $('#createddatetime').datetimepicker({
                    format: "YYYY-MM-DD",
                    defaultDate: new Date(),
                    minDate: new Date(),
                    maxDate: new Date()
                });

//            $('#createddatetime').datetimepicker({
//                weekStart: 1,
//                todayBtn: 1,
//                autoclose: 1,
//                todayHighlight: 1,
//                startView: 2,
//                forceParse: 0,
//                showMeridian: 1,
//                format: "yyyy-mm-dd"    // yyyy-mm-dd
//            });

//            $('#createddatetime').datetimepicker('setStartDate', new Date());
//            $('#createddatetime').datetimepicker('setDate', new Date());

                var date = new Date();
                date.setHours(23, 59, 59, 999);

//            $('#createddatetime').datetimepicker('setEndDate', date);

                $("#inventorycategory").find("option:selected").text();

                /************** Disable Section Form *************/
                function disableForm() {
                    $("#createddatetime").prop('disabled', true);
                    $("#inventorycategory").prop('disabled', true);
                    $("#partname").prop('disabled', true);
                    $("#partcode").prop('disabled', true);
                    $("#location").prop('disabled', true);
                    $("#posrevision").prop('disabled', true);
                    $("#pospartid").prop('disabled', true);
                    $("#posmac").prop('disabled', true);
                    $("#posptid").prop('disabled', true);
                    $("#posserialnumber").prop('disabled', true);

                }
                /************* END Disable Section Form ***********/

                if ($(inventorycategory).val() === '3') {
                    $("#dynamiccontent").show();
                } else {
                    $("#dynamiccontent").hide();
                }



            });



        </script>
    </body>
</html>
