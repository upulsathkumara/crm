<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Search Item
                        </h1>
                        <span></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">

                            </div>
                        </div>
                    </div>
                </div>

                <!-- SEARCH BOX -->
                <form:form class="smart-form" id="terminalSearchForm" commandName="hardwareItemSearch">


                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Category</label>
                                <label class="select">
                                    <form:select path="category" id="category" class="select2" items="${categoryList}" style="width:100%;"/>
                                </label>
                                <i></i>
                            </section>

                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Part Name</label>
                                <label class="select">
                                    <input type="text" id="searchDes" class="form-control" >
                                    <i></i>
                                </label>
                            </section>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="btnsearch" type="button" class="btn btn-primary" value="Search">Search</button>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    </form>
                    <!-- END OF SEARCH BOX -->


                </form:form>
            </div>
            <section id="widget-grid" class="case-datatable">

                <!-- row -->
                <div class="row">

                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                <h2>Item search results</h2>

                            </header>

                            <!-- widget div-->
                            <div>


                                <!-- widget content -->
                                <div class="widget-body no-padding">

                                    <table id="dt_item" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>			                
                                            <tr>  
                                                <th data-hide="phone">Category</th>
                                                <th data-hide="phone,tablet">Part Name</th>
                                                <th data-hide="phone">Part Code</th>
                                                <th data-hide="phone">Revision(POS)</th>
                                                <th data-hide="phone,tablet">Part ID(POS)</th>
                                                <th data-hide="phone,tablet">MAC(POS)</th>
                                                <th data-hide="phone,tablet">PTID(POS)</th>
                                                <th data-hide="phone,tablet">Created User</th>
                                                <th data-hide="phone">Created Date</th>
                                                <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->											
                    </article>
                    <!-- WIDGET END -->

                </div>								
            </section>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer">
        <jsp:include page="../template/footer.jsp"/>
    </div>
    <!-- END PAGE FOOTER -->

    <!-- js file include -->
    <jsp:include page="../template/jsinclide.jsp"/>

    <script type="text/javascript">

        pageSetUp();

        var search = false;

        $(document).ready(function () {
            var permissions = JSON.parse($('#permissions').val());
            refreshPermissions(permissions);
            $('#category').select2();

            var responsiveHelper_dt_item = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };


            /***************************** Load Table **********************/
            var responsiveHelper_dt_basic = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            var search_table = $('#dt_item').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_item'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();

                    if (!permissions.delete) {
                        $('.fa-trash').parent().parent().remove();
                    }
                    if (!permissions.update) {
                        $('.fa-pencil').parent().parent().remove();
                    }
                    if (!permissions.view) {
                        $('.fa-eye').parent().parent().remove();
                    }
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory_hardware/searched/item",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({"name": "search", "value": $.trim($('#search').val())});
                    aoData.push({"name": "partname", "value": $.trim($('#searchDes').val())});
                    aoData.push({"name": "inventorycategory", "value": $('#category').val()});
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": "${pageContext.servletContext.contextPath}/inventory_hardware/searched/item",
                        "data": aoData,
                        "success": fnCallback
                    });
                },
                "order": [[0, "desc"]],
                "aoColumns": [
                    {"mDataProp": "category", "bSortable": false},
                    {"mDataProp": "partname", "bSortable": false},
                    {"mDataProp": "partcode", "bSortable": false},
                    {"mDataProp": "posrevision", "bSortable": false},
                    {"mDataProp": "pospartid", "bSortable": false},
                    {"mDataProp": "posmac", "bSortable": false},
                    {"mDataProp": "posptid", "bSortable": false},
                    {"mDataProp": "createduser", "bSortable": false},
                    {"mDataProp": "createdtime", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}

                ]
            });

            /************************* END Table Load *******************/



//                var search_table = $('#dt_item').dataTable({
//                    "paging": true,
//                    "bProcessing": true,
//                    "bServerSide": true,
//                    "bFilter": false,
//                    "sPaginationType": "full_numbers",
//                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
//                            "t" +
//                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
//                    "autoWidth": true,
//                    "preDrawCallback": function () {
//                        // Initialize the responsive datatables helper once.
//                        if (!responsiveHelper_dt_item) {
//                            responsiveHelper_dt_item = new ResponsiveDatatablesHelper($('#dt_item'), breakpointDefinition);
//                        }
//                    },
//                    "rowCallback": function (nRow) {
//                        responsiveHelper_dt_item.createExpandIcon(nRow);
//                    },
//                    "drawCallback": function (oSettings) {
//                        responsiveHelper_dt_item.respond();
//                    },
//                    "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory_hardware/searched/item",
//                    "fnServerData": function (sSource, aoData, fnCallback) {
//                        aoData.push({"name": "search", "value": search});
//                        aoData.push({"name": "category", "value": $('#category').val()});
//
//                        $.ajax({
//                            "dataType": 'json',
//                            "type": "GET",
//                            "url": "${pageContext.servletContext.contextPath}/inventory_hardware/searched/item",
//                            "data": aoData,
//                            "success": fnCallback
//                        });
//                    },
//                    "order": [[0, "desc"]],
//                    "aoColumns": [
//                        {"mDataProp": "category", "bSortable": false},
//                        {"mDataProp": "partname", "bSortable": false},
//                        {"mDataProp": "partcode", "bSortable": false},
//                        {"mDataProp": "posrevision", "bSortable": false},
//                        {"mDataProp": "pospartid", "bSortable": false},
//                        {"mDataProp": "posmac", "bSortable": false},
//                        {"mDataProp": "posptid", "bSortable": false},
//                        {"mDataProp": "createduser", "bSortable": false},
//                        {"mDataProp": "createddate", "bSortable": false},
//                        {"mDataProp": "action", "bSortable": false}
//
//                    ]
//
//                });

            $('#btnsearch').click(function () {
                search = true;
                search_table.fnDraw();
            });





            function refreshPermissions(permissions) {
                if (!permissions.search) {
                    $('#search_btn').attr('disabled', 'disabled');
                    $('#search_statusid').attr('disabled', 'disabled');

                }
                if (!permissions.create) {
                    // disableForm();
                    $('#statusClear').attr('disabled', 'disabled');
                    $('#btnAddStatusSection').attr('disabled', 'disabled');

                }
            }


        });
    </script>
</body>
</html>