<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
            <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>

        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Insert Accessory
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Hardware Item Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form:form id="hardwareItemInsertForm" commandName="hardwareItemCreate">
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Added Date <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <form:input path="addeddate" id="addeddate" class="input-sm" readonly="readonly"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Category <samp style="color: red">*</samp></label>
                                        <label class="select">

                                            <form:select path="category" id="category" cssClass="select2" items="${categoryList}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Part Code</label>
                                        <label class="input">
                                            <form:input path="partcode" id="partcode" type="text"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Part name</label>
                                        <label class="input">
                                            <form:input path="partname" id="partname" type="text"/>
                                        </label>
                                    </section>
                                </div>
                            </div>                                        
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Location<samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:select path="location" id="location" cssClass="select2" items="${locationList}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>

                            </div>

                            <div id="dynamiccontent" style="display: none">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">Revision</label>
                                            <label class="input">
                                                <form:input path="posrevision" id="posrevision" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-5 pull-right">
                                        <section>
                                            <label class="label">Part ID</label>
                                            <label class="input">
                                                <form:input path="pospartid" id="pospartid" type="text"/>
                                            </label>
                                        </section>
                                    </div>        
                                </div>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">MAC</label>
                                            <label class="input">
                                                <form:input path="posmac" id="posmac" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-5 pull-right">
                                        <section>
                                            <label class="label">PTID</label>
                                            <label class="input">
                                                <form:input path="posptid" id="posptid" type="text"/>
                                            </label>
                                        </section>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">Serial No</label>
                                            <label class="input">
                                                <form:input path="posserialnumber" id="posserialnumber" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-1">


                            <%--<form:button style="float:right" id="btninsert" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</form:button>--%>
                            <form:button style="float:right" id="btninsert" type="button" class="btn btn-primary btn-spinner" >Save</form:button>

                                <input id="saveaction" name="saveaction" type="text" value="create" hidden/>


                            </div>
                            <div class="col-xs-2"></div>
                        </div>


                </form:form>
                <!--</div>-->

                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN PANEL -->

            <!-- PAGE FOOTER -->
            <div class="page-footer">
                <jsp:include page="../template/footer.jsp"/>
            </div>
            <!-- END PAGE FOOTER -->

            <!-- js file include -->
            <jsp:include page="../template/jsinclide.jsp"/>

            <script type="text/javascript">

                $(document).ready(function () {

                    var permissions = JSON.parse($('#permissions').val());
                    refreshPermissions(permissions);
//                    $("#dynamiccontent").hide();
                    $('#category').select2();
                    $('#location').select2();


                    $('#addeddate').datetimepicker({
                        format: "YYYY-MM-DD",
                        defaultDate: new Date(),
                        minDate: new Date(),
                        maxDate: new Date()
                    });
                    //                $('#addeddate').datetimepicker({
                    //                    weekStart: 1,
                    //                    todayBtn: 1,
                    //                    autoclose: 1,
                    //                    todayHighlight: 1,
                    //                    startView: 2,
                    //                    forceParse: 0,
                    //                    showMeridian: 1,
                    //                    format: "yyyy-mm-dd"    // yyyy-mm-dd
                    //                });
                    //                $('#addeddate').datetimepicker('setStartDate', new Date());
                    //                $('#addeddate').datetimepicker('setDate', new Date());

                    // var date = new Date();
                    // date.setHours(23, 59, 59, 999);

                    //                $('#addeddate').datetimepicker('setEndDate', date);

                    $('#hardwareItemInsertForm').validate({
                        rules: {
                            addeddate: {
                                required: true
                            },
                            category: {
                                required: true
                            },
                            location: {
                                required: true
                            },
                            posserialnumber: {
                                number: true,
                                max: 2147483648,
                                maxlength: 10
                            }
                        },
                        messages: {
                            addeddate: {
                                required: "Please select the Date"
                            },
                            location: {
                                required: "Please select the Location"
                            },
                            category: {
                                required: "Select the terminal Categroy"
                            }
                        },
                        onfocusout: false,
                        invalidHandler: function (form, validator) {
                            var errors = validator.numberOfInvalids();
                            if (errors) {
                                validator.errorList[0].element.focus();
                            }
                        }
                    });

                    $('#btninsert').click(function () {
                        var dataObject = new Object();
                        var inventorycategory = new Object();
                        var location = new Object();
                        dataObject.inventoryhardwareitemid = $("#inventoryhardwareitemid").val();
                        inventorycategory.inventorycategoryid = $("#category").val();
                        location.locationid = $("#location").val();
                        dataObject.createddatetime = $("#addeddate").val();
                        dataObject.lastupdateddatetime = $("#addeddate").val();
                        dataObject.inventorycategory = inventorycategory;
                        dataObject.partname = $("#partname").val();
                        dataObject.partcode = $("#partcode").val();
                        dataObject.location = location;

                        if ($("#category").val() === '3') {
                            dataObject.posrevision = $("#posrevision").val();
                            dataObject.pospartid = $("#pospartid").val();
                            dataObject.posmac = $("#posmac").val();
                            dataObject.posptid = $("#posptid").val();
                            dataObject.posserialnumber = $("#posserialnumber").val();
                        }

                        var content = JSON.stringify(dataObject);
                        var selectedFunction = $('#saveaction').val();
                        if ($("#hardwareItemInsertForm").valid()) {

                            $.ajax({
                                type: 'POST',
                                contentType: 'application/json',
                                url: "${pageContext.servletContext.contextPath}/inventory_hardware/insert_item/done?action=" + selectedFunction,
                                data: content,
                                async: false,
                                cache: false,
                                dataType: 'json',
                                success: function (data, textStatus, jqXHR) {
                                    console.log(data);
                                    if (data) {
                                        clearSectionForm();
                                        $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + data.message + '</div> <br/>');
                                    } else {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + data.message + '</div> <br/>');
                                        window.scrollTo(0, 0);
                                    }
                                    $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                                    resetDropdowns();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log("In Error", jqXHR);
                                }
//                                ,
//                                beforeSend: function () {
//                                    $('#btninsert').button('loading');
//                                },
//                                complete: function () {
//                                    $('#btninsert').button('reset');
//                                }
                            });
                        } else {

                        }

                    });

//                    $('#hardwareItemInsertForm').validate({
//                        rules: {
//                            addeddate: {
//                                required: true
//                            },
//                            category: {
//                                required: true
//                            }
//                        },
//                        messages: {
//                            addeddate: {
//                                required: "Please select the Date"
//                            },
//                            category: {
//                                required: "Select the Item Categroy"
//                            }
//                        },
//                        onfocusout: false,
//                        invalidHandler: function (form, validator) {
//                            var errors = validator.numberOfInvalids();
//                            if (errors) {
//                                validator.errorList[0].element.focus();
//                            }
//                        }
//                    });

                    $('#category').on('change', function () {


                        if ($(this).val() === '3') {
                            $("#dynamiccontent").show();
                        } else {
                            $("#dynamiccontent").hide();
                        }

                    });

                    /**************** Reset Dropdown *****************/
                    function resetDropdowns() {
                        $('#inventoryhardwareitemid').val('');
                        $('#category').val('').trigger('change');
                        $('#posrevision').val('');
                        $('#location').val('').trigger('change');
                        // $('#select2-chosen-1').val('');
                        $('#partcode').val('');
                        $('#partname').val('');
                        $('#posmac').val('');
                        $('#pospartid').val('');
                        $('#posserialnumber').val('');
                        $('#posptid').val('');

                    }
                    /**************** END Reset Dropdown *****************/

                    function clearTable() {
                        table.clear().draw();
                    }

                    /*************** Clear Section Form **************/
                    function clearSectionForm() {

                        /**************** Removing Color of the boxes *****************/
                        $("#hardwareItemInsertForm").validate().resetForm();
                        $("#hardwareItemInsertForm label").each(function () {
                            $(this).removeClass('state-error');
                            $(this).removeClass('state-success');
                        });

                        /**************** End Removing Color of the boxes *****************/

                        SECTION_RECORD_ID = null;
                        $('#posserialnumber').val("");
                        $("#statusCode").val("");
                        $("#statusDes").val("");
                        $("#sortId").val("");
                        $("#isfinalstatus").val("");
                        $("#isinitialstatus").val("");
                    }
                    /************** END Clear Section Form ************/

                    /************** Disable Section Form *************/
                    function disableForm() {
                        $("#btninsert").prop('disabled', true);

                    }
                    /************* END Disable Section Form ***********/

                    function refreshPermissions(permissions) {
                        if (!permissions.search) {
                            $('#search_btn').attr('disabled', 'disabled');
                            $('#search_statusid').attr('disabled', 'disabled');
                        }
                        if (!permissions.create) {
                            disableForm();

                            //$('#btninsert').attr('disabled', 'disabled');
                            $('#productAddBtn').attr('disabled', 'disabled');
                            $('#btnClear').attr('disabled', 'disabled');
                        }
                    }
                });

            </script>
    </body>
</html>
