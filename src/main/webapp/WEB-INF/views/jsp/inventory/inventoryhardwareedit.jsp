<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Edit Hardware Item
                        </h1>
                        <span></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div id="msg_dev"></div>
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Hardware Item Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form:form id="hardwareItemEditForm" commandName="itemedit">
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Last Updated Date <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <form:input path="lastupdateddatetime" id="lastupdateddatetime" type="text" class="input-sm" readonly="readonly"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Category <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="inventorycategory.inventorycategoryid" id="inventorycategory" cssClass="select2" items="${categoryList}" style="width:100%;"/>

                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Part name</label>
                                        <label class="input">
                                            <form:input path="partname" id="partname" type="text"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Part Code</label>
                                        <label class="input">
                                            <form:input path="partcode" id="partcode" type="text"/>
                                        </label>
                                    </section>
                                </div>

                            </div>   


                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Location<samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:select path="location.locationid" id="location" cssClass="input-sm" items="${locationList}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>

                            <div id="dynamiccontent" style="display: none">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">Revision</label>
                                            <label class="input">
                                                <form:input path="posrevision" id="posrevision" type="text"/>
                                            </label>
                                        </section>
                                    </div>                                
                                </div>   



                                <div class="row">                                
                                    <div class="col-xs-5 pull-right">
                                        <section>
                                            <label class="label">Part ID</label>
                                            <label class="input">
                                                <form:input path="pospartid" id="pospartid" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">MAC</label>
                                            <label class="input">
                                                <form:input path="posmac" id="posmac" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-5 pull-right">
                                        <section>
                                            <label class="label">PTID</label>
                                            <label class="input">
                                                <form:input path="posptid" id="posptid" type="text"/>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-5">
                                        <section>
                                            <label class="label">Serial No</label>
                                            <label class="input">
                                                <form:input path="posserialnumber" id="posserialnumber" type="text"/>
                                                <form:hidden path="inventoryhardwareitemid" id="inventoryhardwareitemid"/>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-1">

                            <form:button style="float:right" id="btnedit" type="button" class="btn btn-primary btn-spinner">Save</form:button>
                                <input id="saveaction" name="saveaction" type="text" value="update" hidden/>

                                <!--<button type="button" id="btnedit" class="btn btn-default btn-sm">Edit</button>-->

                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                </form:form>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#inventorycategory').select2();
                $('#location').select2();
                $('#lastupdateddatetime').datetimepicker({
                    format: "YYYY-MM-DD",
                    defaultDate: new Date(),
                    minDate: new Date(),
                    maxDate: new Date()
                });
                $("#btnedit").click(function () {

                    var dataObject = new Object();
                    var inventorycat = new Object();
                    var location = new Object();
                    dataObject.inventoryhardwareitemid = $("#inventoryhardwareitemid").val();
                    inventorycat.inventorycategoryid = $("#inventorycategory").val();
                    dataObject.inventorycategory = inventorycat;
                    location.locationid = $("#location").val();
                    dataObject.createddatetime = $("#addeddate").val();
                    dataObject.lastupdateddatetime = $("#addeddate").val();
                    dataObject.inventorycategory = inventorycat;
                    dataObject.partname = $("#partname").val();
                    dataObject.partcode = $("#partcode").val();
                    dataObject.location = location;

                    if ($("#inventorycategory").val() === '3') {
                        dataObject.posrevision = $("#posrevision").val();
                        dataObject.pospartid = $("#pospartid").val();
                        dataObject.posmac = $("#posmac").val();
                        dataObject.posptid = $("#posptid").val();
                        dataObject.posserialnumber = $("#posserialnumber").val();
                    }





                    var content = JSON.stringify(dataObject);
                    var selectedFunction = $('#saveaction').val();
                    if ($("#hardwareItemEditForm").valid()) {




                        $.ajax({
                            type: 'POST',
                            contentType: 'application/json',
                            url: "${pageContext.servletContext.contextPath}/inventory_hardware/edit_item/done?action=" + selectedFunction,
                            data: content,
                            async: false,
                            cache: false,
                            dataType: 'json',
                            success: function (data, textStatus, jqXHR) {
                                console.log(data);
                                if (data.SUCCESS) {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + data.message + '</div> <br/>');

                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + data.message + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log("In Error", jqXHR);
                            }

                        });
                    } else {
                    }
                });

                $("#inventorycategory").change(function () {
                    if ($(this).val() === '3') {
                        $("#dynamiccontent").show();
                    } else {
                        $("#dynamiccontent").hide();
                    }
                });


                if ($(inventorycategory).val() === '3') {
                    $("#dynamiccontent").show();
                } else {
                    $("#dynamiccontent").hide();
                }


                $('#hardwareItemEditForm').validate({
                    rules: {
                        lastupdateddatetime: {
                            required: true
                        },
                        inventorycategory: {
                            required: true
                        },
                        posserialnumber: {
                            number: true,
                            max: 2147483648,
                            maxlength: 10
                        },
                        location: {
                            required: true
                        }
                    },
                    messages: {
                        lastupdateddatetime: {
                            required: "Please select the Date"
                        },
                        inventorycategory: {
                            required: "Select the Item Categroy"
                        }
                    },
                    onfocusout: false,
                    invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });

            });
        </script>
    </body>
</html>