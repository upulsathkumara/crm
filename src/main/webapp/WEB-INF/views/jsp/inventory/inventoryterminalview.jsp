<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            View Terminal
                        </h1>
                        <span></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Terminal Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <form:form id="terminalViewForm" commandName="terminalview">
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Client</label>
                                        <label class="select">
                                            <form:select path="client.clientid" id="client" class="select2" items="${dropdownListClient}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Category</label>
                                        <label class="select">
                                            <form:select path="inventorycategory.inventorycategoryid" id="inventorycategory" class="select2" items="${dropdownListCategory}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Brand</label>
                                        <label class="select">
                                            <form:select path="terminalbrand.terminalbrandid" id="terminalbrand" class="select2" items="${dropdownListBrand}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Model</label>
                                        <label class="select">
                                            <form:select path="terminalmodel.terminalmodelid"  id="terminalmodel" class="select2" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">TID</label>
                                        <label class="input">
                                            <form:input path="tid" id="tid" type="text"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">MID</label>
                                        <label class="input">
                                            <form:input path="mid" id="mid" type="text"/>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Serial Number</label>
                                        <label class="input">
                                            <form:input path="serialno" id="serialno" type="text"/>
                                            <form:hidden path="inventoryterminalitemid" id="inventoryterminalitemid"/>
                                        </label>
                                    </section>
                                </div>

                            </div>
                        </div>
                    </div>

                </form:form>

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">
            $(document).ready(function () {
               
                $('#inventorycategory').select2();
                $('#terminalbrand').select2();
                $('#terminalmodel').select2();
                $('#client').select2();

                if ($('#terminalbrand').val() != '') {
                    console.log("test");
                    $('#terminalbrand').trigger('change');
                }

                $('#inventorycategory').prop('disabled', true);
                $('#client').prop('disabled', true);
                $('#terminalbrand').prop('disabled', true);
                $('#terminalmodel').prop('disabled', true);
                $('#tid').prop('disabled', true);
                $('#mid').prop('disabled', true);
                $('#serialno').prop('disabled', true);

             

                $('#terminalbrand').change(function () {
                    $('#terminalmodel').empty();
                    if ($('#terminalbrand').val()) {
                        $.get("${pageContext.servletContext.contextPath}/inventory/getterminalmodels", {
                            terminalbrand: $('#terminalbrand').val()
                        }, "json").done(function (data) {
                            $.each(jQuery.parseJSON(data), function (key, value) {
                                $('#terminalmodel').append($("<option></option>")
                                        .attr("value", value.id)
                                        .text(value.value));
                                $('#terminalmodel').trigger('change');
                            });
                        });
                    } else {
                        $('#terminalmodel').append($("<option></option>")
                                .attr("value", "")
                                .text("-- Select --"));
                    }
                });


                /********************--terminal model dropdown load--************************/
                $('#terminalmodel').empty();
                if ($('#terminalbrand').val()) {
                    $.get("${pageContext.servletContext.contextPath}/inventory/getterminalmodels", {
                        terminalbrand: $('#terminalbrand').val()
                    }, "json").done(function (data) {
                        $.each(jQuery.parseJSON(data), function (key, value) {

                            $('#terminalmodel').append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.value));
                            //    $('#terminalmodel').trigger('change');
                            $('#terminalmodel').val("${terminalModel}").change();
                        });

                    });

                } else {
                    $('#terminalmodel').append($("<option></option>")
                            .attr("value", "")
                            .text("-- Select --"));
                }
                /*******************--End ternimal model dropdown--*************************/

            });

        </script>
    </body>
</html>