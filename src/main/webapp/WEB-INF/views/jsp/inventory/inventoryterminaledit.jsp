<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Edit Terminal
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10">


                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Terminal Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <form:form id="terminalEditForm" commandName="terminaledit">
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Client</label>
                                        <label class="select">
                                            <form:select path="client.clientid" id="client" class="select2" items="${dropdownListClient}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Category <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="inventorycategory.inventorycategoryid" id="inventorycategory" class="select2" items="${dropdownListCategory}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Brand <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="terminalbrand.terminalbrandid" id="terminalbrand" class="select2" items="${dropdownListBrand}" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Model</label>
                                        <label class="select">
                                            <form:select path="terminalmodel.terminalmodelid" id="terminalmodel"  class="select2" style="width:100%;"/>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">TID</label>
                                        <label class="input">
                                            <form:input path="tid" id="tid" type="text"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">MID</label>
                                        <label class="input">
                                            <form:input path="mid" id="mid" type="text"/>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Serial Number</label>
                                        <label class="input">
                                            <input id="inventoryterminalitemid" name="inventoryterminalitemid" type="hidden" value="${terminalId}"/>
                                            <form:input path="serialno" id="serialno" type="text"/>
                                            <form:hidden value="${terminalId}" path="inventoryterminalitemid" id="inventoryterminalitemid"/>
                                        </label>
                                    </section>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-offset-2 col-xs-8" style="padding-bottom: 100px;">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" id="btnedit" class="pull-right btn btn-primary btn-sm  btn-spinner">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input id="saveaction" name="saveaction" type="text" value="update" hidden/>
                </form:form>

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#inventorycategory').select2();
                $('#terminalbrand').select2();
                $('#terminalmodel').select2();
                $('#client').select2();
                $("#serialno").mask('999-999-999');
                if ($('#terminalbrand').val() != '') {
                    console.log("test");
                    $('#terminalbrand').trigger('change');
                }

                /**********************--Validate Form--**********************/
                var form = $('#terminalEditForm').validate({
                    onkeyup: function (element) {
                        $(element).valid();
                    },
                    onfocusout: function (element) {
                        $(element).valid();
                    },
                    rules: {
                        brand: {
                            required: true
                        },
                        category: {
                            required: true
                        },
                        tid: {
                            required: true
                        },
                        mid: {
                            required: true
                        },
                        client: {
                            required: true
                        }

                    }, errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    },
                    invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });
                /************************************************/


                /********************--terminal model dropdown load--************************/
                $('#terminalmodel').empty();
                if ($('#terminalbrand').val()) {
                    $.get("${pageContext.servletContext.contextPath}/inventory/getterminalmodels", {
                        terminalbrand: $('#terminalbrand').val()
                    }, "json").done(function (data) {
                        $.each(jQuery.parseJSON(data), function (key, value) {

                            $('#terminalmodel').append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.value));
                            //    $('#terminalmodel').trigger('change');
                            $('#terminalmodel').val("${terminalModel}").change();
                        });

                    });

                } else {
                    $('#terminalmodel').append($("<option></option>")
                            .attr("value", "")
                            .text("-- Select --"));
                }
                /*******************--End ternimal model dropdown--*************************/

                $('#serialno').rules('add', {
                    required: true,
                    maxlength: 32,
                    onkeyup: false,
                    remote: {
                        url: "${pageContext.servletContext.contextPath}/inventory/validateSerialNumber",
                        type: 'get',
                        data: {serialnumber: function () {
                                return $('#serialno').val();
                            }
                        },
                        async: true,
                    }
                });




                /********************--Edit--*********************************/
                $("#btnedit").click(function () {
                    //alert($('#inventoryterminalitemid').val());

                    /************************--Create Data Object--***************************/
                    var dataObject = new Object();

                    dataObject.terminalbrand = $('#terminalbrand').val();
                    dataObject.tid = $('#tid').val();
                    dataObject.mid = $('#mid').val();
                    dataObject.serialno = $('#serialno').val();
                    dataObject.terminalmodel = $('#terminalmodel').val();
                    dataObject.inventorycategory = $('#inventorycategory').val();
                    dataObject.client = $('#client').val();
                    dataObject.inventoryterminalitemid = $('#inventoryterminalitemid').val();
                    var selectedFunction = $('#saveaction').val();

                    console.log(dataObject);
                    var content = JSON.stringify(dataObject);
                    console.log("********************" + content);



                    $.ajax({
                        type: "post",
                        url: "${pageContext.servletContext.contextPath}/inventory/insert_terminal/done?action=" + selectedFunction,
                        cache: false,
                        data: {terminal_info: content},
                        success: function () {

                            // response = JSON.parse(response);
                            if (true) {
                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Terminal Sucessfully Updated!</div> <br/>');

                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Error in saving record </div> <br/>');
                                window.scrollTo(0, 0);
                            }
                            $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });


                    /************************--End of Data Object--***************************/

                });
                /***********************--End Edit--*****************************/



//            $("#btnedit").click(function () {
//                alert("aaaaaaaaaaaaaaaaaaaa");
//                var dataObject = new Object();
//
//                var inventorybrand = new Object();
//                var inventorycat = new Object();
//                var inventoryclient = new Object();
//                var inventorymodel = new Object();
//
//                console.log("INVENTORY ID "+ $("inventoryterminalitemid").val());
//
//                inventorybrand.inventorybrandid = $("#terminalbrand").val();
//                dataObject.terminalbrand = inventorybrand;  //inventoryproductcategory
//
//                dataObject.tid = $("#tid").val();
//                dataObject.mid = $("#mid").val();
//                dataObject.serialno = $("#serialno").val();
//
//                inventorymodel.inventorymodelid = $("#terminalmodel").val();
//                dataObject.terminalmodel = inventorymodel;
//
//                inventorycat.inventorycategoryid = $("#inventorycategory").val();
//                dataObject.inventorycategory = inventorycat;  //inventorycategory
//
//                inventoryclient.clientid = $("#client").val();
//                dataObject.client = inventoryclient;
//
//                var content = JSON.stringify(dataObject);
//                console.log("Data in the object " + content);
//                $.ajax({
//                    type: 'POST',
//                    contentType: 'application/json',
//                    url: "${pageContext.servletContext.contextPath}/inventory/edit_terminal/done",
//                    data: content,
//                    async: false,
//                    cache: false,
//                    dataType: 'json',
//                    success: function (data, textStatus, jqXHR) {
//                        console.log(data);
//                    },
//                    error: function (jqXHR, textStatus, errorThrown) {
//                        console.log("In Error" + jqXHR)
//                    }
//
//                });
//            });

                $('#terminalbrand').change(function () {
                    $('#terminalmodel').empty();
                    if ($('#terminalbrand').val()) {
                        $.get("${pageContext.servletContext.contextPath}/inventory/getterminalmodels", {
                            terminalbrand: $('#terminalbrand').val()
                        }, "json").done(function (data) {
                            $.each(jQuery.parseJSON(data), function (key, value) {
                                $('#terminalmodel').append($("<option></option>")
                                        .attr("value", value.id)
                                        .text(value.value));
                                $('#terminalmodel').trigger('change');
                            });
                        });
                    } else {
                        $('#terminalmodel').append($("<option></option>")
                                .attr("value", "")
                                .text("-- Select --"));
                    }
                });


            });




        </script>
    </body>
</html>