<%-- 
    Document   : uitemplate
    Created on : Apr 26, 2017, 11:20:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->           

            <div id="content">


                <!-- START OF ADD TERMINAL TAG -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Terminal Assign / Accept
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <!-- END OF ADD TERMINAL TAG -->


                <section id="widget-grid" class="case-datatable">

                    <!-- STATUS MESSAGE DISPLAY -->
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">

                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <!-- END STATUS MESSAGE DISPLAY -->

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Ticket Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_ticket" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>  
                                                    <th data-hide="phone"> Ticket ID</th>
                                                    <th data-hide="phone"> Ticket Date</th>
                                                    <th data-hide="phone"> Ticket Priority</th>
                                                    <th data-hide="phone"> Merchant Name</th>
                                                    <th data-class="expand"> Merchant Contact No</th>
                                                    <th data-hide="phone"> Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>

                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Terminal Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_terminal" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone">Brand</th>
                                                    <th data-hide="phone,tablet">Model</th>
                                                    <th data-hide="phone">Serial Number</th>
                                                    <th data-hide="phone">Client</th>
                                                    <th data-hide="phone,tablet">Category</th>
                                                    <th data-hide="phone,tablet">Owner</th>
                                                    <th data-hide="phone,tablet">Created Date</th>
                                                    <th data-hide="phone,tablet">Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>
                </section>


                <section id="widget-grid" class="case-datatable">

                    <!-- STATUS MESSAGE DISPLAY -->
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">

                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <!-- END STATUS MESSAGE DISPLAY -->

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Accept Ticket Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_ticket_accept" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>  
                                                    <th data-hide="phone"> Ticket ID</th>
                                                    <th data-hide="phone"> Ticket Date</th>
                                                    <th data-hide="phone"> Ticket Priority</th>
                                                    <th data-hide="phone"> Merchant Name</th>
                                                    <th data-class="expand"> Merchant Contact No</th>
                                                    <th data-hide="phone"> Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>
                </section>

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->


        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">

            $(document).ready(function () {

                var ticketId = null;
                var terminalSearch = false;

                /**************************************** Load Ticket Table *****************************************/
                var responsiveHelper_dt_ticketassigndata = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var ticket_table = $('#dt_ticket').dataTable({
                    "paging": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_ticketassigndata) {
                            responsiveHelper_dt_ticketassigndata = new ResponsiveDatatablesHelper($('#dt_ticket'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_ticketassigndata.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_ticketassigndata.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory/assign/ticket_table",
                    "fnServerData": function (sSource, aoData, fnCallback) {

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "desc"]],
                    "aoColumns": [
                        {"mDataProp": "ticketid", "bSortable": false},
                        {"mDataProp": "ticketdate", "bSortable": false},
                        {"mDataProp": "ticketpriority", "bSortable": false},
                        {"mDataProp": "merchantname", "bSortable": false},
                        {"mDataProp": "merchantcontactno", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });
                /**************************************** END Load Ticket Table *****************************************/


                /******************** Ticket Table Action Button Click *******************/
                var responsiveHelper_dt_ticketassigndat = undefined;
                $('#dt_ticket').on('click', 'tr a', function (e) {

//                    if ($(this).hasClass('selected')) {
//                        $(this).removeClass('selected');
//                    }
//                    else {
//                        ticket_table.$('tr.selected').removeClass('selected');
//                        $(this).addClass('selected');
//                    }


                    ticketId = $(this).attr('id');
                    terminalSearch = true;
                    loadODsTransferToNPA(ticketId);
                    terminalSearch = false;
                });
                /******************** END Ticket Table Action Button Click *******************/


                /********************************* Terminal Table Load *********************************/
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var terminal_table = $('#dt_terminal').dataTable({
                    "paging": true,
                    "bProcessing": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                });
                /***************************************************************************************/

                function loadODsTransferToNPA(id) {
                    if (id == null) {
                        id = "";
                    }
                    var responsiveHelper_dt_basic = undefined;
                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };

                    $('#dt_terminal').dataTable().fnClearTable();
                    $('#dt_terminal').dataTable().fnDestroy();

                    terminal_table = $('#dt_terminal').dataTable({
                        "paging": true,
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "sPaginationType": "full_numbers",
                        "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                                "t" +
                                "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                        "autoWidth": true,
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_ticketassigndat) {
                                responsiveHelper_dt_ticketassigndat = new ResponsiveDatatablesHelper($('#dt_terminal'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_ticketassigndat.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_ticketassigndat.respond();
                        },
                        "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory/assign/terminal_table",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            aoData.push({"name": "ticketId", "value": id});
                            aoData.push({"name": "search", "value": terminalSearch});

                            $.ajax({
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                            });
                        },
                        "order": [[0, "desc"]],
                        "aoColumns": [
                            {"mDataProp": "brand", "bSortable": false},
                            {"mDataProp": "model", "bSortable": false},
                            {"mDataProp": "serialNo", "bSortable": false},
                            {"mDataProp": "client", "bSortable": false},
                            {"mDataProp": "category", "bSortable": false},
                            {"mDataProp": "owner", "bSortable": false},
                            {"mDataProp": "createddatetime", "bSortable": false},
                            {"mDataProp": "createduser", "bSortable": false},
                            {"mDataProp": "action", "bSortable": false}
                        ]
                    });
                }
                /********************************* END Terminal Table Load *********************************/


                /*********************************** Confirmation Of Terminal Assing *************************************/
                $('#dt_terminal').on('click', 'tr a', function (e) {
                    var inventoryTerminalId = $(this).attr('id');

                    $.SmartMessageBox({
                        title: "<i class='fa fa-times' style='color:#D50000 '></i> Are you sure you want to assign this terminal ?",
                        content: "Warning: This action cannot be undone!",
                        buttons: '[No][Yes]'
                    }, function (ButtonPressed) {

                        if (ButtonPressed === "Yes") {
                            $.ajax({
                                type: "POST",
                                url: "${pageContext.servletContext.contextPath}/inventory/assign/terminalAssign",
                                data: {ticketId: ticketId, inventoryTerminalId: inventoryTerminalId},
                                async: true,
                                cache: false,
                                success: function (response, textStatus, jqXHR) {
                                    response = JSON.parse(response);
                                    if (response.CODE === "SUCCESS") {
                                        $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    } else {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    }
                                    ticketId = null;
                                    ticket_table.fnDraw();
                                    terminal_table.fnDraw();
                                    window.scrollTo(0, 0);
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                }
                            });
                        }
                        if (ButtonPressed === "No") {
                        }

                    });
                    e.preventDefault();
                });
                /*********************************** END Confirmation Of Terminal Assing *************************************/


                /**************************************** Table Load Accept Ticket *****************************************/
                var responsiveHelper_dt_ticketassigndata = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var dt_ticket_accept = $('#dt_ticket_accept').dataTable({
                    "paging": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_ticketassigndata) {
                            responsiveHelper_dt_ticketassigndata = new ResponsiveDatatablesHelper($('#dt_ticket_accept'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_ticketassigndata.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_ticketassigndata.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/inventory/accept/accept_ticket_table",
                    "fnServerData": function (sSource, aoData, fnCallback) {

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "desc"]],
                    "aoColumns": [
                        {"mDataProp": "ticketid", "bSortable": false},
                        {"mDataProp": "ticketdate", "bSortable": false},
                        {"mDataProp": "ticketpriority", "bSortable": false},
                        {"mDataProp": "merchantname", "bSortable": false},
                        {"mDataProp": "merchantcontactno", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]

                });
                /**************************************** END Table Load Accept Ticket *****************************************/



                /*********************************** Confirmation Of Terminal Assing *************************************/
                $('#dt_ticket_accept').on('click', 'tr a', function (e) {
                    var acceptTicketId = $(this).attr('id');
                    var action = $(this).attr('value');

                    if (action === "accept") {

                        $.SmartMessageBox({
                            title: "<i class='fa fa-times' style='color:#D50000 '></i> Are you sure you want to accept this terminal ?",
                            buttons: '[No][Yes]'
                        }, function (ButtonPressed) {

                            if (ButtonPressed === "Yes") {
                                $.ajax({
                                    type: "POST",
                                    url: "${pageContext.servletContext.contextPath}/inventory/accept/acceptTicket",
                                    data: {acceptTicketId: acceptTicketId},
                                    async: true,
                                    cache: false,
                                    success: function (response, textStatus, jqXHR) {
                                        response = JSON.parse(response);
                                        if (response.CODE === "SUCCESS") {
                                            $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
                                        } else {
                                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                        }
                                        ticketId = null;
                                        dt_ticket_accept.fnDraw();                                        
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                    }
                                });
                                window.scrollTo(0, 0);
                            }
                            if (ButtonPressed === "No") {
                            }
                        });
                    }
                    e.preventDefault();
                });
                /*********************************** END Confirmation Of Terminal Assing *************************************/
            });

        </script>
    </body>
</html>