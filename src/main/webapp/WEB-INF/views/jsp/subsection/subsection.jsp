<%-- 
    Document   : subsection
    Created on : Aug 26, 2017, 7:35:24 PM
    Author     : chandima
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.avn.affiniti.hibernate.model.Subsection"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en-us">
    <!-- Basic Styles -->

    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>

            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Subsection Add 
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <form:form id="subSectionsCreateForm" novalidate="novalidate" class="smart-form" commandName="SubsectionAddform" action="/addnew" method="post">
                    <input id="subsectionId" name="subsectionId" type="hidden" value=""/>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section <samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="sectionid" path="section" items="${sectionList}" name="sectionid"></form:select>
                                        <i></i>
                                    </label>
                                </section>

                            </div>

                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Subsection Description<samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <input id="subsectionDes" name="subsectionDes" placeholder="Sub Section" type="text" value=""/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>

                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Status <samp style="color: red">*</samp></label>
                                    <label class="select">
                                    <form:select id="statusid" path="status" items="${statuslist}" name="statusname"></form:select>
                                        <i></i>
                                    </label>
                                </section>
                            </div>


                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Icon</label>
                                    <label class="input">
                                        <input id="icon" name="icon" placeholder="Section" type="text" value=""/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>   
                        </div>



                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">URL <samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <input id="url" name="url" placeholder="URL" type="text" value=""/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>

                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Clickable URL</label>
                                    <label class="afinity_checkbox">
                                        <input id="checkbox" name="clickableurl" checked="checked" type="checkbox" value="true"/><input type="hidden" name="_clickableurl" value="on"/>
                                        <input id="subsectionIdd" name="subsectionIdd" type="text" value="" hidden/>
                                        <input id="saveaction" name="saveaction" type="text" value="create" hidden/>
                                    </label>
                                </section>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Sort Id <samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <input id="sortid" name="sortid" placeholder="Sort Id" type="text" value=""/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <footer style="background-color: #ffffff">
                                    <button id="pagesubmit" type="button" class="btn btn-primary btn-spinner" type="submit" value="Submit">Save</button>
                                    <button id="clearform" type="reset" class="btn btn-default">Clear</button> 
                                </footer>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>

                </form:form>
                <!-- END OF FORM -->
                <!-- START MODAL -->

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Subsection Search   
                        </h1>
                        <span></span>
                    </div>
                </div>

                <div class="row">

                    <!-- NEW COL START -->
                    <form:form id="subsectionsearch" class="" novalidate="novalidate" commandName="SubsectionAddform" action="/AFFINITI_EPIC/subsection/search" method="get">
                        <article class="col-sm-12 col-md-6 col-lg-6">

                            <div class="input-group input-group-sm smart-form">
                                <div class="input-group-btn">
                                    <select id="searchoption" name="searchoption" data-toggle="dropdown" name="searchoption" class="btn btn-default dropdown-toggle" onchange="clearTable()">
                                        <option value="">--Select--</option>
                                        <!--<option value="section">Section</option>-->
                                        <option value="subsectionDes"> Description</option>
                                    </select>
                                </div>
                                <input id="input1" name="input"  placeholder="Search" class="form-control input-xs" type="text"/>
                                <div class="input-group-btn">
                                    <button id="search_btn" type="button" class="btn btn-default" value="Submit" style="z-index: +999" >Search</button>
                                </div>
                            </div>

                        </article>

                        <article class="col-sm-12 col-md-6 col-lg-6 smart-form" >
                            <!--<h5 style="display: inline-block">section </h5>-->
                            <label class="label" style="display: inline-block">Section</label>
                            <div style="display: inline-block" class="input-group input-group-lg">    
                                <form:select id="sectionidsearch" path="section" items="${sectionList}" name="sectionidsearch" style="height:27px; width:220px   ;padding: 0px 5px 0px 10px;font-size: 12px;border-color: #ccc;border-radius: 2px; margin-left:10px"></form:select>
                                </div>  
                            </article>
                    </form:form>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p style="color: white">|</p>
                    </div>
                </div>
                <!-- widget grid -->
                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Subsection search results</h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone">Subsection ID</th>
                                                    <th data-class="expand"><i class=""></i>Description</th>
                                                    <th data-hide="phone"><i class=""></i> Section</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"> </i>Status</th>
                                                    <th data-hide="phone,tablet">URL</th>
                                                    <th data-hide="phone,tablet">Clickabel URL</th>
                                                    <th data-hide="phone,tablet">Icon</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->

                    </div>								
                </section>

                <!-- END MODAL -->                                        


            </div>

            <!-- END PAGE FOOTER -->
            <div class="page-footer">
                <jsp:include page="../template/footer.jsp"/>
            </div>
            <!-- END PAGE FOOTER -->

            <!-- js file include -->
            <jsp:include page="../template/jsinclide.jsp"/>


            <script type="text/javascript">
                $(document).ready(function () {
                    var permissions = JSON.parse($('#permissions').val());
                    refreshPermissions(permissions);

                    $("#searchoption").change(function () {
                        $("#input").val("");
                        if ($('#searchoption').val() == "subsectionId") {

                            $("#input").keydown(function (e) {
                                // Allow: backspace, delete, tab, escape, enter and .
                                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                                        // Allow: Ctrl+A, Command+A
                                                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                                                // Allow: home, end, left, right, down, up
                                                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                                            // let it happen, don't do anything
                                            return;
                                        }
                                        // Ensure that it is a number and stop the keypress
                                        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                            e.preventDefault();
                                        }
                                    });
                        }
                    });


                    /***************************** Multi Select **********************/
                    $('#pull_right').click(function () {
                        var selectedItem = $("#sectionid option:selected");
                        $("#multisection").append(selectedItem);
                        selectedItem.prop("selected", false);
                        generateBranchArray();
                    });

                    $('#pull_left').click(function () {
                        var selectedItem = $("#multisection option:selected");
                        $("#sectionid").append(selectedItem);
                        selectedItem.prop("selected", false);
                        generateBranchArray();
                    });
                    /***************************** END Multi Select **********************/



                    /***************************** Sub Section to Array **********************/
                    function generateBranchArray() {
                        var section = [];
                        $("#multisection option").each(function () {
                            section.push($(this).val());
                        });
                        $('#multisectionarray').val(JSON.stringify(section));
                    }
                    /***************************** END Sub Section to Array **********************/



                    /***************************** Form Validation **********************/
                    var form = $('#subSectionsCreateForm').validate({
                        onkeyup: function (element) {
                            $(element).valid();
                        },
                        onfocusout: function (element) {
                            $(element).valid();
                        },
                        rules: {
                            subsectionDes: {
                                required: true
                            },
                            section: {
                                required: true
                            },
                            status: {
                                required: true
                            },
                            url: {
                                required: true
                            },
                            sortid: {
                                required: true,
                                number: true

                            }
                        }, errorPlacement: function (error, element) {
                            error.insertAfter(element.parent());
                        },
                        invalidHandler: function (form, validator) {
                            var errors = validator.numberOfInvalids();
                            if (errors) {
                                validator.errorList[0].element.focus();
                            }
                        }
                    });
                    /***************************** END Form Validation **********************/



                    /***************************** Load Data Table **********************/

                    var responsiveHelper_dt_basic = undefined;

                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };

                    var search_table = $('#dt_basic').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "sPaginationType": "full_numbers",
                        "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                                "t" +
                                "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                        "autoWidth": true,
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_basic) {
                                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_basic.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_basic.respond();

                            /******************--Permission--********************/
                            var permissions2 = JSON.parse($('#permissions').val());

                            if (!permissions2.update) {

                                $('.fa-pencil').parent().parent().remove();
                            }
                            /******************--Permission--**********************/
                        },
                        "sAjaxSource": "${pageContext.servletContext.contextPath}/subsection/loadTable",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            aoData.push({"name": "searchOption", "value": $('#searchoption').val()});
                            aoData.push({"name": "searchValue", "value": $.trim($('#input1').val())});
                            aoData.push({"name": "sectionValue", "value": $.trim($('#sectionidsearch').val())});
                            $.ajax({
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                            });
                        },
                        "order": [[0, "asc"]],
                        "aoColumns": [
                            {"mDataProp": "subsectionId", "bSortable": false},
                            {"mDataProp": "subsectionDes", "bSortable": false},
                            {"mDataProp": "sectionDes", "bSortable": false},
                            {"mDataProp": "statusDes", "bSortable": false},
                            {"mDataProp": "url", "bSortable": false},
                            {"mDataProp": "clickable", "bSortable": false},
                            {"mDataProp": "icon", "bSortable": false},
                            {"mDataProp": "action", "bSortable": false}
                        ]
                    });


                    $('#search_btn').click(function () {
                        search_table.fnDraw();
                    });

                    $('#sectionidsearch').on('change', function () {
                        search_table.fnDraw();
                    });
                    /***************************** END Load Data Table **********************/



                    /***************************** Clear Form **********************/

                    function clearSectionForm() {
                        $("#pagesubmit").fadeIn();
                        $("#subsectionIdd").val("");
                        $("#subsectionDes").val("");
                        $('#statusid').prop('selectedIndex', 0);
                        $('#sectionid').prop('selectedIndex', 0);
                        $("#checkbox").prop('checked', false);
                        $("#url").val("");
                        $("#icon").val("");
                        $("#sortid").val("");
                    }

                    $('#clearform').click(function () {
                        $("#pagesubmit").fadeIn();
                        $("#subsectionIdd").val("");
                        $("#subsectionDes").val("");
                        $("#subsectionDes").prop('disabled', false);
                        $('#statusid').prop('selectedIndex', 0);
                        $('#sectionid').prop('selectedIndex', 0);
                        $("#checkbox").prop('checked', false);
                        $("#checkbox").prop('disabled', false);
                        $("#url").prop('disabled', false);
                        $("#url").val("");
                        $("#icon").prop('disabled', false);
                        $("#icon").val("");
                        $("#sortid").prop('disabled', false);
                        $("#sortid").val("");
                        $("#msg_dev").fadeOut("slow");
                        $("#sectionid").prop('disabled', false);
                        $("#statusid").prop('disabled', false);
                        $("#subsectionIdd").prop('disabled', false);
                        clearValidations();
                        $('#pagesubmit').prop('disabled', false);
                    });



                    /***************************** END Clear Form **********************/
                    /************** Function to clear valiodations *************/

                    function clearValidations() {

                        $("#subSectionsCreateForm").validate().resetForm();
                        $("#subSectionsCreateForm label").each(function () {
                            $(this).removeClass('state-error');
                            $(this).removeClass('state-success');
                        });
                    }


                    /************** End of Function to clear valiodations *************/


                    /***************************** Add Sub Section **********************/
                    $('#pagesubmit').click(function () {
                        //$('#msg_dev').empty();
                        if ($("#subSectionsCreateForm").valid()) {
                            var dataObject = new Object();
                            dataObject.subsectionId = $('#subsectionIdd').val();
                            dataObject.subsectionDes = $('#subsectionDes').val();
                            dataObject.sectionid = $('#sectionid').val();
                            dataObject.statusid = $('#statusid').val();
                            dataObject.url = $('#url').val();
                            dataObject.icon = $('#icon').val();
                            dataObject.sortid = $('#sortid').val();
                            dataObject.clickableurl = $("#checkbox").is(':checked') ? true : false;
                            var selectedFunction = $('#saveaction').val();
                            console.log(dataObject);
                            var content = JSON.stringify(dataObject);
                            console.log(content);
                            $.ajax({
                                type: "post",
                                url: "${pageContext.servletContext.contextPath}/subsection/insert?action=" + selectedFunction,
                                cache: false,
                                data: {subsection_info: content},
                                success: function (response) {
                                    clearSectionForm();
                                    response = JSON.parse(response);
                                    if (response) {
                                        $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
                                        search_table.fnDraw();
                                    } else {
                                        // $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                        window.scrollTo(0, 0);
                                    }
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                    clearValidations();
                                },
                                error: function () {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
//                                ,
//                                beforeSend: function () {
//                                    $('#pagesubmit').button('loading');
//                                },
//                                complete: function () {
//                                    $('#pagesubmit').button('reset');
//                                }
                            });
                        }
                    });


                    /***************************** END Add Sub Section **********************/

//                    $.ajax({
//                        method: "POST",
//                        cache: false,
//                        url: "${pageContext.servletContext.contextPath}/subsection/loadstatus",
//                        success: function (response) {
//                            response = JSON.parse(response);
//                            for (var i = 0; i < response.data.length; i++) {
//                                $("#taskid").append($("<option></option>")
//                                        .attr("value", response.data[i].id)
//                                        .text(response.data[i].value));
//                            }
//                        },
//                        error: function () {
//                        }
//                    });

                    /***************************** View Sub Section **********************/
                    $('#dt_basic').on('click', 'tr a', function (e) {
                        clearValidations();
                        var id = $(this).attr('id');
                        var value = $(this).attr('value');

                        $.ajax({
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/subsection/view",
                            data: {subsectionId: id},
                            async: true,
                            cache: false,
                            success: function (response) {
                                response = JSON.parse(response);
                                if (response.CODE === "SUCCESS") {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                } else {
                                    //  $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                                if (value == "view") {
                                    $('#pagesubmit').attr('disabled', 'disabled');
                                    // $("#pagesubmit").fadeOut("slow");

                                    viewIncludeToInputBox(response.data);

                                } else if (value == "edit") {
                                    //$('#pagesubmit').removeClass('disabled');
                                    $('#pagesubmit').prop('disabled', false);
                                    //$("#pagesubmit").fadeIn();
                                    $("#saveaction").val('update');
                                    editIncludeToInputBox(response.data);
                                }
                            },
                            error: function () {
                                console.log("In Error");
                            }
                        });

                        function viewIncludeToInputBox(response) {
                            $("#subsectionDes").val(response[0].subsectionDes).prop('disabled', true);
                            $("#url").val(response[0].url).prop('disabled', true);
                            $("#sortid").val(response[0].sortid).prop('disabled', true);
                            $("#icon").val(response[0].icon).prop('disabled', true);
                            $("#subsectionIdd").val(response[0].subsectionId).prop('disabled', true);

                            $('#sectionid').find('option[value="' + response[0].sectionId + '"]').prop('selected', true);
                            $("#sectionid").prop('disabled', true);

                            //$('#statusid').empty();
                            $('#statusid').find('option[value="' + response[0].statusId + '"]').prop('selected', true);
                            $('#statusid').append($("<option></option>").attr("value", response[0].statusId).text(response[0].statusDes));

                            $("#statusid").prop('disabled', true);

                            if (response[0].clickable === true) {

                                $("#checkbox").prop('checked', true);
                                $("#checkbox").prop('disabled', true);
                            } else if (response[0].clickable === false) {
                                $("#checkbox").prop('checked', false);
                                $("#checkbox").prop('disabled', true);
                            }

                        }

                        function editIncludeToInputBox(response) {
                            $("#checkbox").prop('disabled', false);
                            $("#subsectionDes").val(response[0].subsectionDes).prop('disabled', false);
                            $("#url").val(response[0].url).prop('disabled', false);
                            $("#sortid").val(response[0].sortid).prop('disabled', false);
                            $("#icon").val(response[0].icon).prop('disabled', false);
                            $("#subsectionIdd").val(response[0].subsectionId).prop('disabled', false);

                            $('#sectionid').find('option[value="' + response[0].sectionId + '"]').prop('selected', true);
                            $("#sectionid").prop('disabled', false);
                            $('#statusid').find('option[value="' + response[0].statusId + '"]').prop('selected', true);
                            $("#statusid").prop('disabled', false);
                            // alert($('#statusid option:last-child').val());
                            var chk = response[0].clickableurl;
                            if (response[0].clickable === true) {
                                $("#checkbox").prop('checked', true);
                            } else if (response[0].clickable === false) {
                                $("#checkbox").prop('checked', false);
                                $("#checkbox").prop('disabled', false);
                            }
                        }
                    });
                    /***************************** END View Sub Section **********************/



                    /**************** Function to handle page permissions *****************/

                    function refreshPermissions(permissions) {
                        if (!permissions.search) {
                            $('#search_btn').attr('disabled', 'disabled');
                            $('#searchoption').attr('disabled', 'disabled');
                            $('#input1').attr('disabled', 'disabled');

                        }
                        if (!permissions.create) {
                            $("#subsectionDes").val(response[0].subsectionDes).prop('disabled', true);
                            $("#url").val(response[0].url).prop('disabled', true);
                            $("#sortid").val(response[0].sortid).prop('disabled', true);
                            $("#icon").val(response[0].icon).prop('disabled', true);
                            $("#subsectionIdd").val(response[0].subsectionId).prop('disabled', true);
                            $("#sectionid").prop('disabled', true);
                            $("#statusid").prop('disabled', true);
                            $('#pagesubmit').attr('disabled', 'disabled');
                            $('#clearform').attr('disabled', 'disabled');

                        }
                    }
                    /**************** End of Function to handle page permissions *****************/
                });




            </script>  

            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
    </body>

</html>



