<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <jsp:include page="template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <div class="row">
                    <div class="col-xs-6">
                        <ol class="breadcrumb">
                            <li>Home</li><li>Dashboard</li>
                        </ol>
                    </div>
                    <div class="col-xs-6">
                        <p style="color: #ffffff; padding-top: 11px" class="text-right"><c:out value="${sessionScope.lastlogin}" /></p>
                    </div>
                </div>
                <!-- breadcrumb -->

                <!-- end breadcrumb -->

                <!-- You can also add more buttons to the
                ribbon for further usability

                Example below:

                <span class="ribbon-button-alignment pull-right">
                <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
                <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
                <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
                </span> -->

            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default welcome_page_panal ">

                            <div class="panel-body open_ticket">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-flag-o fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="useropencount">0</h1>
                                            <h3 class="count_w_2">My Open Tickets </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer open_ticket_footer"><a href="#wid-id-0">View</a><div class="pull-right">
                                    <i class="fa fa-flag-o  icons_1 icons_size "></i></div></div>
                        </div>  
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default welcome_page_panal ">

                            <div class="panel-body inprogress_ticket">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-hourglass-half fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="userinprogresscount">0</h1>
                                            <h3 class="count_w_2" > My In-Progress Tickets</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer inprogress_ticket_footer"><a href="#wid-id-0">View</a><div class="pull-right">
                                    <i class="fa fa-hourglass-half  icons_2 icons_size "></i></div> </div>
                        </div>  
                    </div>





                    <div class="col-md-3">
                        <div class="panel panel-default welcome_page_panal ">

                            <div class="panel-body total_outstanding">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-flag-o fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="orgopencount">0</h1>
                                            <h3 class="count_w_2">Org. Open Tickets </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer total_outstanding_footer"><a href="#wid-id-0">View</a><div class="pull-right">
                                    <i class="fa fa-flag-o  icons_1 icons_size "></i></div></div>
                        </div>  
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default welcome_page_panal ">

                            <div class="panel-body new_enterns">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-hourglass-half fa-4x icons_size"></i>
                                    </div> 
                                    <div class="col-xs-9">
                                        <div class="pull-right">
                                            <h1 class="count_w text-right" id="orginprogresscount">0</h1>
                                            <h3 class="count_w_2" > Org. In-Progress Tickets</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer new_enterns_footer"><a href="#wid-id-0">View</a><div class="pull-right">
                                    <i class="fa fa-hourglass-half  icons_2 icons_size "></i></div> </div>
                        </div>  
                    </div>
                </div>



                <c:if test="${MyCase}">
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                    <h2>My Tickets</h2>
                                    <div class="widget-toolbar">

                                        <div class="btn-group">
                                            <button class="btn dropdown-toggle btn-xs btn-warning" data-toggle="dropdown" id="Select_drpdwn">
                                                <span class="cptitle">
                                                    Me
                                                </span>  <i class="fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:void(0);" onclick="myFunctionSubordinateticket(1)">Me</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" onclick="myFunctionSubordinateticket(2)">Subordinate</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <!--<div class="widget-body no-padding">-->

                                    <!--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">-->
                                    <div>
                                        <!-- widget content -->
                                        <!--<div class="widget-body no-padding">-->
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th data-hide="phone">Ticket id</th>
                                                    <th data-class="expand">Ticket type</th>
                                                    <th data-hide="phone,tablet">Product</th>
                                                    <th data-hide="phone">Priority</th>
                                                    <th data-hide="phone,tablet">Assignee</th>
                                                    <th data-hide="phone,tablet">Territory</th>
                                                    <th data-hide="phone,tablet">Contact Name</th>
                                                    <th data-hide="phone,tablet">Telephone</th>
                                                    <th data-hide="phone,tablet">Status</th>
                                                    <th data-hide="phone,tablet">Create Date</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i>Action</th>


                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                        <!--</div>-->
                                        <!-- end widget content -->
                                    </div>
                                    <!--</article>-->
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                </c:if>
                <div class="row">
                    <c:if test="${CallVsCaseChart}">
                        <!-- NEW WIDGET START -->
                        <article class="${CallVsCaseChartCSS}">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="${CallVsCaseChartID}" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                    <h2>Created Tickets</h2>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-2"></div>
                                        </div>
                                        <div id="nogrid-graph" class="chart no-padding"></div>
                                        <div id="legend"></div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </c:if>
                    <c:if test="${AssignCase}">
                        <!-- NEW WIDGET START -->
                        <article class="${AssignCaseCSS}">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="${AssignCaseID}" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                    <h2>Assigned Tickets</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <div class="row">
                                            <div class="col-xs-8"></div>
                                            <div class="col-xs-2"></div>
                                        </div>
                                        <div id="nogrid-graph2" class="chart no-padding"></div>
                                        <div id="legend2"></div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </c:if>
                </div>

                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                <h2><span class="wtitel">Deals Lost and Close over time</span></h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="nogrid-graph3" class="chart no-padding" style="height: 250px;"></div>
                                            <!--<div id="legend3"></div>-->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="row">
                    <!-- NEW WIDGET START -->
                    <%--<c:if test="${MyCalls}">--%>
<!--                        <article class="${MyCallsCSS}">
                             Widget ID (each widget will need unique ID)
                            <div class="jarviswidget" id="${MyCallsID}" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                    <h2>My Calls </h2>
                                </header>
                                 widget div
                                <div>
                                     widget edit box 
                                    <div class="jarviswidget-editbox">
                                         This area used as dropdown edit box 
                                    </div>
                                     end widget edit box 

                                     widget content 
                                    <div class="widget-body no-padding">
                                    <div>
                                        <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th data-hide="phone">Call log Id</th>
                                                    <th data-class="phone, expand">Name in full</th>
                                                    <th data-class="phone, expand">Module</th>
                                                    <th data-class="phone, tablet"> Telephone</th>
                                                    <th data-class="phone,tablet"> <i class="glyphicon glyphicon-sort">  </i>   Call direction</th>
                                                    <th data-class="phone,tablet">Follow up Action</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody> 
                                        </table>
                                        <div id="bar-graph" class="chart no-padding"></div>
                                    </div>
                                </div>
                                 end widget content 
                            </div>
                             end widget div 
                        </article>-->
                    <%--</c:if>--%>
                    <!-- end widget content -->
                </div>

                <div class="row">
                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                <h2><span class="wtitel">Product Wise Inquiry : Bar</span></h2>
                            </header>

                            <!-- widget div-->
                            <div>
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <!--<input type="button" value="chart" id="chart" />-->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <!--                                            <div id="normal-bar-graph2" class="chart no-padding" style="height: 250px;"></div>
                                                                                        <div id="legend3"></div>-->

                                            <div id="barchart"  class="padding-10">

                                            </div>

                                        </div>
                                        <!--                                        <div class="col-xs-6">
                                                                                    <div id="pie-chart2" class="chart"></div>
                                        
                                                                                </div>-->
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>
                    <!-- WIDGET END -->
                </div>

                <div class="row has-eqc">
                    <!-- NEW WIDGET START -->
                    <article class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                <h2><span class="wtitel">Ticket Type wise Tickets : Donut</span></h2>
                            </header>

                            <!-- widget div-->
                            <div>
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <!--<input type="button" value="chart" id="chart" />-->
                                    <div class="row">
                                        <!--                                        <div class="col-xs-12">
                                                                                    <div id="normal-bar-graph2" class="chart no-padding" style="height: 400px;"></div>
                                                                                    <div id="legend3"></div>
                                        
                                                                                </div>-->
                                        <div class="col-xs-12">
                                            <div id="pie-chart2" class="chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>

                    <article class="col-xs-6 col-sm-6 col-md-6 col-lg-6 eqc">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                                <h2><span class="wtitel">Average Deal Closure time and ratio</span></h2>
                            </header>

                            <!-- widget div-->
                            <div>
                                <div>
                                    <div class="row">
                                        <div class="col-md-6 line_breaker">
                                            <div class="text-center">
                                                <h1 id="useravgclosedtime" class="text_count color_orange">0</h1>
                                                <p class="text_text">
                                                    Average Deal Closure Time (Days)
                                                </p>
                                            </div>
                                        </div>
                                        <span class="brk_area"></span>
                                        <div class="col-md-6 line_breaker">
                                            <div class="text-center">
                                                <h1 id="useravgcloseration" class="text_count color_red">0</h1>
                                                <p class="text_text">
                                                    Average Deal Closure Ratio
                                                </p>

                                                <!--                                                <div class="easy-pie-chart" data-percent="91" data-pie-size="100"  style="color: #5cb85c">
                                                                                                    <span class="percent percent-sign"  style="color: #5cb85c;">23 </span>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                        <span class="brk_area"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>



                    <!-- WIDGET END -->
                </div>

                <div class="row">
                    <!-- NEW WIDGET START -->
                    <%--<c:if test="${MyCalls}">--%>
                    <article class="${MyCallsCSS}">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="${MyCallsID}" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                            <header>
                                <h2><span class="wtitel">Idling Tickets</span></h2>
                                <div class="widget-toolbar">

                                    <div class="btn-group">
                                        <button class="btn dropdown-toggle btn-xs btn-warning" data-toggle="dropdown" id="Select_drpdwn">
                                            <span class="ctitle">
                                                Created by me
                                            </span>  <i class="fa fa-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:void(0);" onclick="myFunctionIdelticket(3)">Assigned ticket</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="myFunctionIdelticket(1)">Created by me</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="myFunctionIdelticket(4)">Subordinate</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" onclick="myFunctionIdelticket(2)">All</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </header>
                            <!-- widget div-->
                            <div>
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <!--<div class="widget-body no-padding">-->
                                <div>
                                    <table id="dt_basic3" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th data-hide="phone">Ticket id</th>
                                                <th data-hide="phone,tablet">Ticket type</th>
                                                <th data-hide="phone,tablet">Product</th>
                                                <th data-hide="phone,tablet">Priority </th>
                                                <th data-class="expand">Assignee</th>
                                                <th data-hide="phone">Territory</th>
                                                <th data-class="expand">Customer name</th>
                                                <th data-class="expand">Telephone</th>
                                                <th data-class="expand">Status</th>
                                                <th data-class="expand">Create Date</th>
                                                <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody> 
                                    </table>
                                    <!--<div id="bar-graph" class="chart no-padding"></div>-->
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </article>
                    <%--</c:if>--%>
                    <!-- end widget content -->
                </div>

            </div>
            <!-- end widget div -->
        </div>
        <!-- end widget -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="template/footer.jsp"/>
        </div>

        <!-- END PAGE FOOTER -->

        <jsp:include page="template/jsinclide.jsp"/>

    </body>

    <script type="text/javascript">
        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        var parameeter;
        function myFunctionIdelticket(x) {
            switch (x) {
                case 1:
                    parameeter = 1;
                    document.getElementsByClassName("ctitle")[0].innerHTML = "Created By me";
                    idelticket();
                    break;
                case 2:
                    parameeter = 2;
                    document.getElementsByClassName("ctitle")[0].innerHTML = "All";
                    idelticket();
                    break;
                case 4:
                    parameeter = 4;
                    document.getElementsByClassName("ctitle")[0].innerHTML = "Subordinate";
                    idelticket();
                    break;
                default:
                    parameeter = 3;
                    document.getElementsByClassName("ctitle")[0].innerHTML = "My tickets";
                    idelticket();

            }


//            if (x === 2) {
            //                parameeter = 2;
            //                document.getElementsByClassName("ctitle")[0].innerHTML = "All";
//                idelticket();
//
            //            } else {
//                parameeter = 1;
            //                document.getElementsByClassName("ctitle")[0].innerHTML = "Created By me";
//                idelticket();
// //            }
        }
        var parameetervalue = 2;

        function myFunctionSubordinateticket(x) {
            if (x === 2) {
                parameetervalue = 2;
                document.getElementsByClassName("cptitle")[0].innerHTML = "Subordinate";
                loadtickeTable();

            } else {
                parameetervalue = 1;
                document.getElementsByClassName("cptitle")[0].innerHTML = "Me";
                loadtickeTable();
            }
        }

        function idelticket() {
            var responsiveHelper_dt_basic = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            $('#dt_basic3').dataTable().fnDestroy();
            $('#dt_basic3').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic3'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/idelticket", "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({"name": "parameeter", "value": parameeter});

                    $.ajax({
                        "dataType": 'json',
                        "type": "GET",
                        "url": "${pageContext.servletContext.contextPath}/idelticket",
                        "data": aoData,
                        "global": false,
                        "success": fnCallback});
                },
                "order": [[0, "desc"]],
                "aoColumns": [
                    {"mDataProp": "caseId", "bSortable": true},
                    {"mDataProp": "caseTypeDes", "bSortable": true},
                    {"mDataProp": "productDes", "bSortable": true},
                    {"mDataProp": "priorityDes", "bSortable": true},
                    {"mDataProp": "assignee", "bSortable": true},
                    {"mDataProp": "branchDesc", "bSortable": true},
                    {"mDataProp": "customer", "bSortable": false},
                    {"mDataProp": "phone", "bSortable": true},
                    {"mDataProp": "statusDes", "bSortable": true},
                    {"mDataProp": "caseDate", "bSortable": true},
                    //                    {"mDataProp": "caseCallLogId", "bSortable": true},
                    {"mDataProp": "action", "bSortable": true}

                ],
                "columnDefs": [{
                        "defaultContent": "--",
                        "targets": "_all"
                    }]
            });

        }

        function loadtickeTable() {
            var responsiveHelper_dt_basic = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            $('#dt_basic').dataTable().fnDestroy();
            $('#dt_basic').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "searching": true,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<f>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/casetable",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({"name": "parameeter", "value": parameetervalue});
                    $.ajax({
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData, "global": false,
                        "success": fnCallback
                    });
                },
                "order": [[0, "desc"]],
                "aoColumns": [
                    {"mDataProp": "caseid", "bSortable": true},
                    {"mDataProp": "casetype", "bSortable": true},
                    {"mDataProp": "module", "bSortable": true},
                    {"mDataProp": "priority", "bSortable": true},
                    {"mDataProp": "assignee", "bSortable": true},
                    {"mDataProp": "branchDesc", "bSortable": true},
                    //                    {"mDataProp": "nic", "bSortable": true},
                    {"mDataProp": "customer", "bSortable": false},
                    {"mDataProp": "phone", "bSortable": true},
                    {"mDataProp": "status", "bSortable": true},
                    {"mDataProp": "createddate", "bSortable": false},
                    {"mDataProp": "action", "bSortable": false}


                ],
                "columnDefs": [{
                        "defaultContent": "--",
                        "targets": "_all"
                    }]
            });
            $('#dt_basic_wrapper .form-control').attr("placeholder", "Assignee / Customer");
        }

        $(document).ready(function () {
            myFunctionIdelticket();
            myFunctionSubordinateticket();
            loadOrganizationWisePieChart();
            pageSetUp();
//            loadUseWiseBarChart();
            //            loadUseWisePieChart();
            loadCaseVsCaseChart();
            loadAssigneCaseChart();
            loadOrganizationWiseBarChart();

            loadLeadLostAndClosChart();

            //                            setInterval("ajaxd('${pageContext.servletContext.contextPath}')", 30000);
            setInterval(function () {

            }, 3000);
            /* // DOM Position key index //
             
             l - Length changing (dropdown)
             f - Filtering input (search)
             t - The Table! (datatable)
             i - Information (records)
             p - Pagination (paging)
             r - pRocessing 
             < and > - div elements
             <"#id" and > - div with an id
             <"class" and > - div with a class
             <"#id.class" and > - div with an id and class
             
             Also see: http://legacy.datatables.net/usage/features
             */

            /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            $('#dt_basic1').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                "autoWidth": true,
                "preDrawCallback": function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic1'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                },
                "sAjaxSource": "${pageContext.servletContext.contextPath}/calltable",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "GET",
                        "url": "${pageContext.servletContext.contextPath}/calltable",
                        "data": aoData,
                        "global": false,
                        "success": fnCallback
                    });
                },
                "order": [[0, "desc"]],
                "aoColumns": [
                    {"mDataProp": "callLogId", "bSortable": true},
                    {"mDataProp": "name_in_full", "bSortable": true}, {"mDataProp": "productIdDes", "bSortable": true},
                    {"mDataProp": "telephone", "bSortable": true},
                    {"mDataProp": "callDirectionDes", "bSortable": true},
                    {"mDataProp": "followUpActionDes", "bSortable": true},
                    {"mDataProp": "action", "bSortable": false}

                ]
            });
            function loadCaseVsCaseChart() {
                $.ajax({
                    type: "post",
                    url: "${pageContext.servletContext.contextPath}/loadCaseVsCaseChart",
                    cache: false,
                    global: false,
                    dataType: 'json',
                    success: function (response) {
                        if ($('#nogrid-graph').length) {
                            var day_data = response;
                            var chart = Morris.Line({
                                element: 'nogrid-graph',
                                grid: true,
                                data: day_data,
                                xkey: 'x',
                                ykeys: ['y'],
                                labels: ['Tickets Count'],
                                hideHover: 'auto',
                                resize: false,
                                parseTime: false,
                                lineColors: ['#93b5ea']

                            });
                            chart.options.labels.forEach(function (label, i) {
                                var legendItem = $('<span></span>').text(label).css('color', chart.options.lineColors[i])
                                $('#legend').append(legendItem);
                            });
                        }


                    },
                    error: function () {
                        console.log("Error loading case vs call graph.");
                    }
                });
            }

            function loadAssigneCaseChart() {
                $.ajax({
                    type: "post",
                    url: "${pageContext.servletContext.contextPath}/loadAssigneCaseChart",
                    cache: false,
                    global: false,
                    dataType: 'json',
                    success: function (response) {
                        if ($('#nogrid-graph2').length) {
                            var day_data = response;
                            var chart = Morris.Line({
                                element: 'nogrid-graph2',
                                grid: true,
                                data: day_data,
                                xkey: 'x',
                                ykeys: ['y'],
                                labels: ['Assign Tickets Count'],
                                hideHover: 'auto',
                                resize: false,
                                parseTime: false,
                                lineColors: ['#82E0AA']

                            });
                            chart.options.labels.forEach(function (label, i) {
                                var legendItem = $('<span></span>').text(label).css('color', chart.options.lineColors[i])
                                $('#legend2').append(legendItem);
                            });
                        }
                    }, error: function () {
                        console.log("Error loading assignee case chart.");
                    }
                });
            }

            function loadLeadLostAndClosChart() {
                $.ajax({
                    type: "post",
                    url: "${pageContext.servletContext.contextPath}/loadLeadLostAndClosChart",
                    cache: false,
                    global: false,
                    dataType: 'json',
                    success: function (response) {
                        if ($('#nogrid-graph3').length) {
                            console.log(response);
                            var day_data = response;
                            var chart = Morris.Line({
                                element: 'nogrid-graph3',
                                grid: true,
                                data: day_data,
                                xkey: 'x',
                                ykeys: ['y', 'z'],
                                labels: ['Lead Closed', 'Lead Lost'],
                                hideHover: 'auto',
                                resize: false,
                                parseTime: false,
                                lineColors: ['#428bca', '#d9534f']
                            });
                            chart.options.labels.forEach(function (label, i) {
//                                var legendItem = $('<span></span>').text(label).css('color', chart.options.lineColors[i])
//                                $('#legend4').append(legendItem);
                            });
                        }
                    }, error: function () {
                        console.log("Error loading assignee case chart.");
                    }
                });
            }



            /* ASSIGNEE CASE CHART */

            $.ajax({
                type: "post",
                url: "${pageContext.servletContext.contextPath}/dashboard/userticketstatus",
                cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
                    $('#useropencount').empty();
                    $('#useropencount').text(response.opencount);
                    $('#userinprogresscount').empty();
                    $('#userinprogresscount').text(response.inprogress);
                },
                error: function () {
                    console.log("Error loading user ticket status.");
                }
            });

            $.ajax({
                type: "post",
                url: "${pageContext.servletContext.contextPath}/dashboard/orgticketstatus",
                cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
                    $('#orgopencount').empty();
                    $('#orgopencount').text(response.opencount);
                    $('#orginprogresscount').empty();
                    $('#orginprogresscount').text(response.inprogress);
                },
                error: function () {
                    console.log("Error loading user ticket status.");
                }
            });

            $.ajax({
                type: "post",
                url: "${pageContext.servletContext.contextPath}/dashboard/averagedealclosuretime", cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
                    $('#useravgclosedtime').empty();
                    $('#useravgclosedtime').text(response.avgclosedtime);
                    $('#useravgcloseration').empty();
                    $('#useravgcloseration').text(response.avgclosedRatio + '%');


                },
                error: function () {
                    console.log("Error loading user ticket status.");
                }
            });



        });
        //add the jQuery click/show/hide behaviours (or native JS if you prefer):
        $(document).ready(function () {
//            if ($("#normal-bar-graph").is(":visible")) {
//                $("#pie-chart").show();
//                $("#pie-chart2").hide();
//                $("#normal-bar-graph2").hide();
            //                //                                 document.getElementsByClassName("wtitel")[0].innerHTML = "Reset";
//            }
            return false;
        });

        function loadOrganizationWiseBarChart() {
            $.ajax({
                type: "post",
                url: "${pageContext.servletContext.contextPath}/loadOrganizationWiseBarchart",
                cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
//                    if ($('#normal-bar-graph2').length) {
//                        var chart = Morris.Bar({
//                            element: 'normal-bar-graph2',
//                            data: response.DpcontentOrg,
//                            xkey: 'x',
//                            ykeys: response.DepartmentOrg,
//                            labels: response.DepartmentOrg,
//                            horizontal: true,
//                            hideHover: 'auto'
//                        });
//                    }

                    var arry = response.DpcontentOrg;
                    console.log(response)
                    console.log(response.DpcontentOrg[0].ACount)
//                    console.log("tets data" + response.DpcontentOrg[0].details.Count);
//                    if ($('#normal-bar-graph2').length) {
//                        var chart = Morris.Bar({
//                            element: 'normal-bar-graph2',
//                            data: response.DpcontentOrg,
//                            xkey: 'x',
//                            ykeys: response.DepartmentOrg,
//                            labels: response.DepartmentOrg,
//                            horizontal: true,
//                            hideHover: 'auto'
//                        });
//                    }

                    $("#barchart").empty();

                    for (var i = 0; i < response.DpcontentOrg.length; i++) {
                        var li = '<div style="position: relative;"><h5 style="size: 8px;">' + response.DpcontentOrg[i].Dep + '</h5>'
//                                + '<div style="flex-direction: row; display: flex;">'
                                + '<span class="resolutionsticktypeonecount">' + response.DpcontentOrg[i].ACount + '</span>'
//                                + '</div>'
                                + '<div class="progress progress-sm">'
                                + '<div class="progress-bar" role="progressbar" style="width: ' + response.DpcontentOrg[i].Count + '%" id="resolutionstyleone"></div>'
                                + '</div></div>';
                        $("#barchart").prepend(li);
                    }

                },
                error: function () {
                    console.log("Error loading organization wise barchart.");
                }
            });
        }

        function loadOrganizationWisePieChart() {
            $.ajax({
                type: "post", url: "${pageContext.servletContext.contextPath}/loadOrganizationWisePieChart",
                cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
                    if ($('#nogrid-graph').length) {
                        if ($('#pie-chart2').length) {
                            var data_pie = response;
                            $.plot($("#pie-chart2"), data_pie, {
                                series: {
                                    pie: {
                                        show: true,
                                        innerRadius: 0.5,
                                        radius: 1,
                                        label: {
                                            show: true, //                                            radius: 3 / 3,
                                            formatter: function (label, series) {
                                                return '<div style="font-size:13px; text-align:center; padding:4px; color:black;">' + label + '<br/>' + series.data[0][1] + '%</div>';
                                            },
                                            threshold: 0.1
                                        }
                                    }
                                },
                                legend: {
                                    show: true,
                                    noColumns: 1, // number of colums in legend table
                                    labelFormatter: null, // fn: string -> string
                                    labelBoxBorderColor: "#000", // border color for the little label boxes
                                    container: null, // container (as jQuery object) to put legend in, null means default on top of graph
                                    position: "ne", // position of default legend container within plot
                                    margin: [5, 10], // distance from grid edge to default legend container within plot
                                    //                                    backgroundColor: "#efefef", // null means auto-detect
                                    backgroundOpacity: 1 // set to 0 to avoid background
                                },
                                grid: {
                                    hoverable: true
                                }
                            });
                        }
                    }
                },
                error: function () {
                    console.log("Error loading organization pie chart.");
                }

            });
        }

        function loadUseWiseBarChart() {
            $.ajax({
                type: "post",
                url: "${pageContext.servletContext.contextPath}/loadUserWiseBarchart",
                cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
                    if ($('#normal-bar-graph').length) {
                        Morris.Bar({
                            element: 'normal-bar-graph',
                            data: response.Dcontent,
                            xkey: 'x',
                            ykeys: response.Department,
                            labels: response.Department,
                            hideHover: 'auto'
                        });
                    }
                },
                error: function () {
                    console.log("Error loading user wise barchart.");
                }
            });
        }

        function loadUseWisePieChart() {
            $.ajax({
                type: "post",
                url: "${pageContext.servletContext.contextPath}/loadUserWisePieChart",
                cache: false,
                global: false,
                dataType: 'json',
                success: function (response) {
                    if ($('#nogrid-graph').length) {
                        if ($('#pie-chart').length) {
                            var data_pie = response;
                            $.plot($("#pie-chart"), data_pie, {
                                series: {
                                    pie: {
                                        show: true,
                                        innerRadius: 0.5,
                                        radius: 1,
                                        label: {
                                            show: true,
                                            radius: 2 / 3,
                                            formatter: function (label, series) {
                                                return '<div style="font-size:13px;text-align:center;font-weight: bold; padding:4px;color:black;">' + label + '<br/>' + series.data[0][1] + '%</div>';
                                            },
                                            threshold: 0.1
                                        }
                                    }
                                },
                                legend: {
                                    show: true,
                                    noColumns: 1, // number of colums in legend table
                                    labelFormatter: null, // fn: string -> string
                                    labelBoxBorderColor: "#000", // border color for the little label boxes
                                    container: null, // container (as jQuery object) to put legend in, null means default on top of graph
                                    position: "ne", // position of default legend container within plot
                                    margin: [5, 10], // distance from grid edge to default legend container within plot
                                    backgroundColor: "#efefef", // null means auto-detect
                                    backgroundOpacity: 1 // set to 0 to avoid background
                                },
                                grid: {
                                    hoverable: true
                                }
                            });
                        }
                    }
                },
                error: function () {
                    console.log("Error loading user piechart.");
                }
            });
        }
    </script>
</html>
