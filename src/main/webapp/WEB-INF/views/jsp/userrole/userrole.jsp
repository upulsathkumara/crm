<%-- 
    Document   : userrole
    Created on : Sep 19, 2017, 12:36:07 PM
    Author     : chandima
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.avn.affiniti.hibernate.model.Userrole"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en-us">
    <!-- Basic Styles -->

    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>

            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            User Role Add 
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <form:form id="userroleCreateForm" novalidate="novalidate" class="smart-form" commandName="UserroleAddform"  method="post">
                    <input id="userroleId" name="userroleId" type="hidden" value=""/>
                    <input id="saveaction" name="saveaction" type="text" value="create" hidden/>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">User Role Type <samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="userroletypeid" path="" items="${userroletypelist}" name="userroletypeid"></form:select>
                                        <i></i>
                                    </label>
                                </section>

                            </div>

                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">User Role Description<samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <input id="userroleDes" name="userroleDes" placeholder="Userrole" type="text" value=""/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>

                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">User Role Code <samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <input id="userroleCode" name="userroleCode" placeholder="Userrole Code" type="text" value=""/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>


                            <div class="col-xs-2"></div>
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Status <samp style="color: red">*</samp></label>
                                    <label class="select">
                                    <form:select id="statusid" path="status" items="${statuslist}" name="statusid"></form:select>
                                        <i></i>
                                    </label>
                                </section>
                            </div>   
                        </div>



                        <div class="row">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <footer style="background-color: #ffffff">
                                    <button id="pagesubmit"  type="button" class="btn btn-primary btn-spinner">Save</button>
                                    <button id="clearformbtn"  type="button" class="btn btn-default">Clear</button> 
                                </footer>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>

                </form:form>
                <!-- END OF FORM -->
                <!-- START MODAL -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            User Role Search   
                        </h1>
                        <span></span>
                    </div>
                </div>

                <div class="row">

                    <!-- NEW COL START -->
                    <form id="subsectionsearch" novalidate="novalidate"  method="get">
                        <article class="col-sm-12 col-md-12 col-lg-6">

                            <div class="input-group input-group-sm ">
                                <div class="input-group-btn">
                                    <select id="searchoption" name="searchoption" data-toggle="dropdown" name="searchoption" class="btn btn-default dropdown-toggle" onchange="clearTable()">
                                        <option value="">--Select--</option>
                                        <option value="subsectionId">User Role ID</option>
                                        <option value="subsectionDes">Description</option>
                                    </select>
                                </div>
                                <input id="input" name="input"  placeholder="Search" class="form-control input-lg" type="text" value=""/>
                                <div class="input-group-btn">
                                    <button id="search_btn" type="button" class="btn btn-default" value="Submit">Search</button>
                                </div>
                            </div>
                        </article>
                    </form>
                </div>
                <!-- widget grid -->
                <section id="widget-grid" class="case-datatable">

                    <p style="color: white">
                        |
                    </p>
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>User Role Details</h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone">User Role ID</th>
                                                    <th data-class="expand"><i class=""></i>Description</th>
                                                    <th data-hide="phone"><i class=""></i> User Role Type</th>
                                                    <th data-hide="phone,tablet"><i class=""> </i>User Role Code</th>
                                                    <th data-hide="phone,tablet">Status</th>
                                                    <th data-hide="phone,tablet">Created Date</th>
                                                    <th data-hide="phone,tablet">Last Updated Date</th>
                                                    <th data-hide="phone,tablet">Last Updated User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->

                    </div>								
                </section>

                <!-- END MODAL -->                                        


            </div>

            <!-- END PAGE FOOTER -->
            <div class="page-footer">
                <jsp:include page="../template/footer.jsp"/>
            </div>
            <!-- END PAGE FOOTER -->

            <!-- js file include -->
            <jsp:include page="../template/jsinclide.jsp"/>


            <script type="text/javascript">


                $(document).ready(function () {

                    var permissions = JSON.parse($('#permissions').val());
                    refreshPermissions(permissions);


                    /***************************** Form Validation **********************/
                    var form = $('#userroleCreateForm').validate({
                        onkeyup: function (element) {
                            $(element).valid();
                        },
                        onfocusout: function (element) {
                            $(element).valid();
                        },
                        rules: {
                            userroletypeid: {
                                required: true
                            },
                            userroleDes: {
                                required: true
                            },
                            status: {
                                required: true
                            },
                            userroleCode: {
                                required: true
                            }
                        }, errorPlacement: function (error, element) {
                            error.insertAfter(element.parent());
                        },
                        invalidHandler: function (form, validator) {
                            var errors = validator.numberOfInvalids();
                            if (errors) {
                                validator.errorList[0].element.focus();
                            }
                        }
                    });
                    /***************************** END Form Validation **********************/



                    /***************************** Load Data Table **********************/

                    var responsiveHelper_dt_basic = undefined;
                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };
                    var search_table = $('#dt_basic').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "sPaginationType": "full_numbers",
                        "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                                "t" +
                                "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                        "autoWidth": true,
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_basic) {
                                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_basic.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_basic.respond();
                        },
                        "sAjaxSource": "${pageContext.servletContext.contextPath}/userrole/loadTable",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            aoData.push({"name": "searchOption", "value": $('#searchoption').val()});
                            aoData.push({"name": "searchValue", "value": $.trim($('#input').val())});
                            $.ajax({
                                "dataType": 'json',
                                "type": "POST",
                                "url": "${pageContext.servletContext.contextPath}/userrole/loadTable",
                                "data": aoData,
                                "success": fnCallback
                            });
                        },
                        "order": [[0, "asc"]],
                        "aoColumns": [
                            {"mDataProp": "userroleId", "bSortable": false},
                            {"mDataProp": "desctiption", "bSortable": false},
                            {"mDataProp": "userroletype", "bSortable": false},
                            {"mDataProp": "userrolecode", "bSortable": false},
                            {"mDataProp": "status", "bSortable": false},
                            {"mDataProp": "createddate", "bSortable": false},
                            {"mDataProp": "lastupdateddate", "bSortable": false},
                            {"mDataProp": "createduser", "bSortable": false},
                            {"mDataProp": "action", "bSortable": false}
                        ]
                    });
                    $('#search_btn').click(function () {
                        var searchoption = $('#searchoption').val();
                        var input = $('#input').val();
                        if (searchoption == "subsectionId" && input == "") {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + 'Please insert a search parameter' + '</div> <br/>');
                            $('#msg_dev').fadeIn().delay(3000).fadeOut('slow');
                        } else if (searchoption == "subsectionDes" && input == "") {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + 'Please insert a search parameter' + '</div> <br/>');
                            $('#msg_dev').fadeIn().delay(3000).fadeOut('slow');
                        } else {
                            search_table.fnDraw();
                        }
                    });
                    /***************************** END Load Data Table **********************/



                    /***************************** Clear Form   *****************************/

                    function clearForm() {
                        $("#userroleDes").val("");
                        $("#userroleCode").val("");
                        $('#statusid').prop('selectedIndex', 0);
                        $('#userroletypeid').prop('selectedIndex', 0);
                        clearValidations();
                        $("#msg_dev").fadeOut("slow");
                    }

                    $('#clearformbtn').click(function () {
                        $("#userroleDes").val("");
                        $("#userroleCode").val("");
                        $('#statusid').prop('selectedIndex', 0);
                        $('#userroletypeid').prop('selectedIndex', 0);
                        clearValidations();
                        $("#msg_dev").fadeOut("slow");
                    });
                    /***************************** END Clear Form **********************/
                    /************** Function to clear valiodations *************/

                    function clearValidations() {

                        $("#userroleCreateForm").validate().resetForm();
                        $("#userroleCreateForm label").each(function () {
                            $(this).removeClass('state-error');
                            $(this).removeClass('state-success');
                        });
                    }

                    /************** End of Function to clear valiodations *************/


                    /***************************** Add Userrole **********************/

                    $('#pagesubmit').click(function () {
                        $('#msg_dev').empty();
                        if ($("#userroleCreateForm").valid()) {
                            var dataObject = new Object();
                            dataObject.userroleid = $('#userroleId').val();
                            dataObject.userroletype = $('#userroletypeid').val();
                            dataObject.description = $('#userroleDes').val();
                            dataObject.userrolecode = $('#userroleCode').val();
                            dataObject.status = $('#statusid').val();
                            console.log(dataObject);
                            var content = JSON.stringify(dataObject);
                            console.log(content);
                            var selectedFunction = $('#saveaction').val();
                            $.ajax({
                                type: "post",
                                url: "${pageContext.servletContext.contextPath}/userrole/insert?action=" + selectedFunction,
                                cache: false,
                                data: {userrole_info: content, userrole_id: $('#userroleId').val()},
                                success: function (response) {
                                    clearForm();
                                    response = JSON.parse(response);
                                    search_table.fnDraw();
                                    if (response) {
                                        $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong>'+response.message+'</div> <br/>');
                                    } else {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.message + '</div> <br/>');
                                        window.scrollTo(0, 0);
                                    }
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                    $("#saveaction").val('create');
                                    clearValidations();
                                },
                                error: function () {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                    window.scrollTo(0, 0);
                                    $("#saveaction").val('create');
                                }
//                                ,
//                                beforeSend: function () {
//                                    $('#pagesubmit').button('loading');
//                                },
//                                complete: function () {
//                                    $('#pagesubmit').button('reset');
//                                }
                            });
                        }
                    });
                    /***************************** END Add Userrole **********************/



                    /***************************** View Userrole **********************/

                    $('#dt_basic').on('click', 'tr a', function (e) {
                        clearValidations();
                        var id = $(this).attr('id');
                        var value = $(this).attr('value');
                        $('#userroleId').val(id);
                        $.ajax({
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/userrole/view",
                            data: {userroleId: id},
                            async: true,
                            cache: false,
                            success: function (response) {

                                var response1 = JSON.parse(response);
                                if (response.CODE === "SUCCESS") {

                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                } else {
                                    //  $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                                if (value == "view") {
                                    $('#pagesubmit').attr('disabled', 'disabled');
                                    //$("#pagesubmit").fadeOut("slow");
                                    viewIncludeToInputBox(response1);
                                } else if (value == "edit") {
                                    $("#saveaction").val('update');
                                    $('#pagesubmit').prop('disabled', false);
                                    //$("#pagesubmit").fadeIn();
                                    editIncludeToInputBox(response1);
                                }
                            },
                            error: function () {
                                console.log("In Error");
                            }
                        });
                        function viewIncludeToInputBox(response) {
                            $("#userroleDes").val(response.data[0].desctiption).prop('disabled', true);
                            $("#userroleCode").val(response.data[0].userrolecode).prop('disabled', true);
                            $('#statusid').find('option[value="' + response.data[0].statusid + '"]').prop('selected', true);
                            $("#statusid").prop('disabled', true);
                            $('#userroletypeid').find('option[value="' + response.data[0].userroletype + '"]').prop('selected', true);
                            $("#userroletypeid").prop('disabled', true);
                            $("#msg_dev").fadeOut("slow");
                            $("#userroleCreateForm label").each(function () {
                                $(this).removeClass('state-error');
                                $(this).removeClass('state-success');
                            });
                        }

                        function editIncludeToInputBox(response) {
                            $("#userroleDes").val(response.data[0].desctiption).prop('disabled', false);
                            $("#userroleCode").val(response.data[0].userrolecode).prop('disabled', false);
                            $("#statusid").prop('disabled', false);
                            $('#statusid').find('option[value="' + response.data[0].statusid + '"]').prop('selected', true);
                            $("#userroletypeid").prop('disabled', false);
                            $('#userroletypeid').find('option[value="' + response.data[0].userroletype + '"]').prop('selected', true);
                            $("#msg_dev").fadeOut("slow");
                            $("#userroleCreateForm label").each(function () {
                                $(this).removeClass('state-error');
                                $(this).removeClass('state-success');
                            });
                        }
                    });
                    /***************************** END View Sub Section **********************/


                    /***************************** END View Userrole **********************/
                    /**************** Function to handle page permissions *****************/

                    function refreshPermissions(permissions) {
                        if (!permissions.search) {
                            $('#search_btn').attr('disabled', 'disabled');
                            $('#searchoption').attr('disabled', 'disabled');
                            $('#input').attr('disabled', 'disabled');

                        }
                        if (!permissions.create) {
                            //$("#subsectionDes").val(response[0].subsectionDes).prop('disabled', true);
                            // $("#url").val(response[0].url).prop('disabled', true);
                            $("#sortid").val(response[0].sortid).prop('disabled', true);
                            $("#userroleCode").prop('disabled', true);
                            $("#userroleDes").prop('disabled', true);
                            $("#userroletypeid").prop('disabled', true);
                            $("#statusid").prop('disabled', true);
                            $('#pagesubmit').attr('disabled', 'disabled');
                            $('#clearform').attr('disabled', 'disabled');

                        }
                    }
                    /**************** End of Function to handle page permissions *****************/

                });

            </script>  

            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
    </body>

</html>