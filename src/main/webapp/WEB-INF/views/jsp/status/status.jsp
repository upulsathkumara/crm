<%-- 
    Document   : status
    Created on : Aug 16, 2017, 8:35:35 AM
    Author     : acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>


    <!-- TITLE IS HERE PLZ REMOVE IT BUDDY-->
    <title>JSP Page</title>



    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>

                <!--<input id="permissions" type="hidden" value='{"view":true,"search":true,"create":true,"update":true,"delete":true,"assign":true}'>-->
                <%--= session.getAttribute("permissions")--%>

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Status Add
                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  id = "statusAddFormId" commandName="statusAddForm" novalidate="novalidate" class="smart-form">
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value="" />
                    <input id="saveaction" name="saveaction" type="text" value="create" hidden/>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <input id="id" type="text" value="" hidden />
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Status Category<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="statusid" path="statusid" cssClass="input-sm" items="${statusList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Status Code<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <form:input id="statusCode" path="statuscode" placeholder="Status Code" type="statusCode" value=""/>
                                    <i></i>
                                </label>

                            </section>
                        </div>
                        <div class="col-xs-2"></div>

                    </div>



                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Sort ID<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <form:input id="sortId" path="sortid" placeholder="Sort ID" type="sortId" value=""/>
                                    <i></i>
                                </label>

                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Is Initial Status<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <form:input id="isinitialstatus" path="isinitialstatus" placeholder="Is Initial Status" type="isinitialstatus" value=""/>
                                    <i></i>
                                </label>

                            </section>
                        </div>
                        <div class="col-xs-2"></div>

                    </div>


                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Is Final Status<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <form:input id="isfinalstatus" path="isfinalstatus" placeholder="Is Final Status" type="isfinalstatus" value=""/>
                                    <i></i>
                                </label>

                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Description<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <form:input id="statusDes" path="description" placeholder="Description" type="statusDes" value=""/>
                                    <i></i>
                                </label>

                            </section>
                        </div>

                        <div class="col-xs-2"></div>
                    </div>



                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <h1 id="add_update_view_title" class="page-title txt-color-blueDark">
                                <footer style="background-color: #ffffff">
                                    <%--<form:button id="btnAddStatusSection" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</form:button>--%>
                                    
                         <form:button id="btnAddStatusSection" type="button" class="btn btn-primary btn-spinner">Save</form:button>

                                        <button type="button" id="statusClear" class="btn">Clear</button>
                                    </footer>
                                </h1>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>


                </form:form> 

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Status Search   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>

                <form:form commandName="statusSearchForm" novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Status Category<samp style="color: red"></samp></label>
                                <label class="select">
                                    <form:select id="search_statusid" path="statusid" cssClass="input-sm" items="${statusList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="search_btn" type="button" class="btn btn-primary" value="Submit">Search</button>  
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>




                </form:form>

                <!-- widget grid -->
                <section id="widget-grid" class="">

                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Status Details </h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="statusSections" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th>Status ID</th>
                                                    <th>Description</th>
                                                    <th>Status Code</th>
                                                    <th>Status Category</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Time</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Time</th>
                                                    <th>Created User</th>
                                                    <th><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>   
                                    </div>
                                    <!-- end widget content --> 
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN PANEL -->

            <!-- PAGE FOOTER -->
            <div class="page-footer">
                <jsp:include page="../template/footer.jsp"/>
            </div>
            <!-- END PAGE FOOTER -->

            <!-- js file include -->
            <jsp:include page="../template/jsinclide.jsp"/>
            <style>
                .btn-table-action{
                    color: #3276b1;
                    cursor: pointer
                }
            </style>
        </div>

        <script type='text/javascript'>
            $(document).ready(function () {

                var permissions = JSON.parse($('#permissions').val());
                refreshPermissions(permissions);


                var SECTION_RECORD_ID = null;



                $('#search_btn').click(function () {
                    clearValidations();
                    search_table.fnDraw();
                });

                $("#statusClear").click(function () {
                    clearSectionForm();//Clear Section Form
                    EnableForm();//Enable Input Fields
                    $("#statusid").prop('disabled', false);
                    $("#statusCode").prop('disabled', false);

                    $("#btnAddStatusSection").fadeIn();
                    $("#msg_dev").fadeOut("slow");
                });


                /***************************** Load Table **********************/
                var responsiveHelper_dt_basic = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                var search_table = $('#statusSections').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#statusSections'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        if (!permissions.delete) {
                            $('.fa-trash').parent().parent().remove();
                        }
                        if (!permissions.update) {
                            $('.fa-pencil').parent().parent().remove();
                        }
                        if (!permissions.view) {
                            $('.fa-eye').parent().parent().remove();
                        }
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/status/getstatus",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "searchoptionID", "value": $.trim($('#search_statusid').val())});
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": "${pageContext.servletContext.contextPath}/status/getstatus",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "statusid", "bSortable": false},
                        {"mDataProp": "statusdescription", "bSortable": false},
                        {"mDataProp": "statuscode", "bSortable": false},
                        {"mDataProp": "statuscategory", "bSortable": false},
                        {"mDataProp": "lastupdatedtime", "bSortable": false},
                        {"mDataProp": "createdtime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}

                    ]
                });
                /************************* END Table Load *******************/



/////////////////// <save>////////////////////////////////////////////////////////////////////////////////


                $('#btnAddStatusSection').click(function () {



                    $('#msg_dev').empty();

                    var obj = {
                        "statusid": $('#id').val(),
                        "statuscategory": {statuscategoryid: $('#statusid').val()},
                        "statuscode": $('#statusCode').val(),
                        "description": $('#statusDes').val(),
                        "sortid": $('#sortId').val(),
                        "isfinalstatus": $('#isfinalstatus').val(),
                        "isinitialstatus": $('#isinitialstatus').val()
                    };


                    var selectedFunction = $('#saveaction').val();

                    if ($('#id').val() != '') {
                        console.log("hidden status id: ", $('#id').val());
                        obj.statusid = $('#id').val();
                    }
                    console.log("request: ", obj);
                    if ($("#statusAddFormId").valid()) {
                        $.ajax({
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            url: "${pageContext.servletContext.contextPath}/status/addorupdatestatus?action=" + selectedFunction,
                            type: 'POST',
                            dataType: 'json',
                            data: JSON.stringify(obj),
                            success: function (response) {

                                if (response) {

                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
                                    clearSectionForm();
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.message + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }

                                $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                                console.log("result: ", response);

                                enableEditFields(true);
                                resetDropdowns();

                                $('#statusid').removeAttr('disabled');
                                $('#statusCode').removeAttr('disabled');

                                $('#btnAddStatusSection').removeClass('disabled');
                                search_table.fnDraw();
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#btnAddStatusSection').button('loading');
//                            },
//                            complete: function () {
//                                $('#btnAddStatusSection').button('reset');
//                            }
                        });
                    } else {
                        //$('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong>' +errorMessage + '</div><br/>');
                        // $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong>Fill All Boxes</div><br/>');
                    }
                });
                /************************ Edit And View Records ************************/
                $('#statusSections').on('click', 'tr a', function (e) {
                    var id = $(this).attr('id');
                    var value = $(this).attr('value');

                    $.ajax({
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/status/getustatuss",
                        data: {statusid: id},
                        async: true,
                        cache: false,
                        success: function (response, textStatus, jqXHR) {
                            response = JSON.parse(response);

                            console.log(response);

                            SECTION_RECORD_ID = response.data.id;
                            if (value === "view") {
                                loadDataToField(response);
                                disableForm();
                                clearValidations();
                                $("#btnAddStatusSection").fadeOut("slow");
                            } else if (value === "edit") {
                                //console.log("id", id);
                                $("#saveaction").val('UPDATE');
                                $('#id').val(id);
                                loadDataToField(response);
                                EnableForm();
                                clearValidations();
                                $("#btnAddStatusSection").fadeIn();
                            }


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("In Error" + jqXHR);
                        }
                    });

                    /********** Load Data To Input Fields ***********/
                    function loadDataToField(response) {//|| 'defult'

                        //$('#id').find('option[value="' + response.data.id + '"]').prop('selected', true);
                        $('#statusid').val(response.data.statuscategory);
                        $('#statusCode').val(response.data.statuscode);
                        $("#statusDes").val(response.data.description);
                        $('#sortId').val(response.data.sortId);
                        $('#isfinalstatus').val(response.data.isfinalstatus);
                        $('#isinitialstatus').val(response.data.isinitialstatus);
                    }
                });
                /********* End Load Data To Input Fields **********/


                /************************ Delete Records ************************/
                $('#statusSections').on('click', 'tr a', function (e) {
                    var id = $(this).attr('id');
                    var value = $(this).attr('value');

                    if (value == "delete") {
                        $.SmartMessageBox({
                            title: "<i class='fa fa-times' style='color:#D50000 '></i> Are you sure you want to delete this status ?",
                            content: "Warning: This action cannot be undone!",
                            buttons: '[No][Yes]'
                        }, function (ButtonPressed) {



                            // if (value === "delete") {
                            if (ButtonPressed === "Yes") {
                                $.ajax({
                                    type: "POST",
                                    url: "${pageContext.servletContext.contextPath}/status/deletestatus",
                                    data: {statusid: id},
                                    async: true,
                                    cache: false,
                                    success: function (response, textStatus, jqXHR) {
                                        response = JSON.parse(response);

                                        //console.log("gaje",response);
                                        if (response.CODE === "SUCCESS") {
                                            $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
                                        } else {
                                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.message + '</div> <br/>');
                                        }
                                        $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                                        search_table.fnDraw();
                                        clearValidations();
                                        clearSectionForm();
                                        EnableForm();
                                        $("#statusCode").prop('disabled', false);
                                        $("#statusid").prop('disabled', false);
                                        $("#btnAddStatusSection").fadeIn();





                                        // SECTION_RECORD_ID = response.data.id;
                                        // if (value === "delete") {

                                        // search_table.fnDraw();
                                        // clearValidations();

//                                }
//
//
//                            },
//                            error: function (jqXHR, textStatus, errorThrown) {
//                                console.log("In Error" + jqXHR);
//                            }
//                        });
//                    }
//                });
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                    }
                                });
                            }
                            if (ButtonPressed === "No") {
                            }

                        });
                    }
                    e.preventDefault();
                });

                /***********Delete Conformation message********************/

//           $.SmartMessageBox({
//                            title: "<i class='fa fa-times' style='color:#D50000 '></i> Are you sure you want to accept this terminal ?",
//                            buttons: '[No][Yes]'
//                        }, function (ButtonPressed) {
//
//                           if (ButtonPressed === "Yes") {
//                                $.ajax({
//                                    type: "POST",
//                                    url: "${pageContext.servletContext.contextPath}/inventory/accept/acceptTicket",
//                                    data: {acceptTicketId: acceptTicketId},
//                                    async: true,
//                                    cache: false,
//                                    success: function (response, textStatus, jqXHR) {
//                                        response = JSON.parse(response);
//                                        if (response.CODE === "SUCCESS") {
//                                            $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
//                                        } else {
//                                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
//                                        }
//                                        ticketId = null;
//                                        dt_ticket_accept.fnDraw();
//                                    },
//                                    error: function (jqXHR, textStatus, errorThrown) {
//                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
//                                    }
//                                });
//                            }
//                            if (ButtonPressed === "No") {
//                            }

//                       });



                /*************** Clear Section Form **************/
                function clearSectionForm() {

                    /**************** Removing Color of the boxes *****************/
                    $("#statusAddFormId").validate().resetForm();
                    $("#statusAddFormId label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass('state-success');
                    });

                    /**************** End Removing Color of the boxes *****************/



                    SECTION_RECORD_ID = null;
                    $("#statusid").val("");
                    $("#statusCode").val("");
                    $("#statusDes").val("");
                    $("#sortId").val("");
                    $("#isfinalstatus").val("");
                    $("#isinitialstatus").val("");
                }
                /************** END Clear Section Form ************/


                function clearValidations() {

                    $("#statusAddFormId").validate().resetForm();
                    $("#statusAddFormId label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass('state-success');
                    });
                }


                /************** Jquery validation Function *************/


                $('#statusAddFormId').validate({
                    onkeyup: function (element) {
                        $(element).valid();
                    }, onfocusout: function (element) {
                        $(element).valid();
                    },
                    rules: {
                        statusid: {
                            required: true
                        },
                        description: {
                            required: true,
                            maxlength: 64
                        },
                        isfinalstatus: {
                            required: true,
                            number: true,
                            maxlength: 1
                        },
                        isinitialstatus: {
                            required: true,
                            number: true,
                            maxlength: 1
                        },
                        sortid: {
                            required: true,
                            number: true,
                            maxlength: 11
                        },
                        statuscode: {
                            required: true,
                            maxlength: 16
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    },
                    invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });
//            $.validator.methods.count_children = function (value, element) {
//                var is_empty = $(element).is(":empty");
//                return !is_empty;
//            }
                /************** End of Jquery validation Function *************/




///////////////////////////////////////////////////validation///////////////////////////////////////////////////////////// 

//                $('#statusAddFormId').validate({
//                    onkeyup: function (element) {
//                    $(element).valid();
//                },
//                onfocusout: function (element) {
//                    $(element).valid();
//                },
//                    rules: {
//                        statusid: {
//                            required: true
//                        },
//                        description: {
//                            required: true,
//                            maxlength: 64
//                        },
//                        isfinalstatus: {
//                            required: true,
//                            number: true,
//                            maxlength: 1
//                        },
//                        isinitialstatus: {
//                            required: true,
//                            number: true,
//                            maxlength: 1
//                        },
//                        sortid: {
//                            required: true,
//                            number: true,
//                            maxlength: 11
//                        },
//                        statuscode: {
//                            required: true,
//                            maxlength: 16
//                        }
//                    },
//                errorPlacement: function (error, element) {
//                    error.insertAfter(element.parent());
//                },
//                invalidHandler: function (form, validator) {
//                    var errors = validator.numberOfInvalids();
//                    if (errors) {
//                        validator.errorList[0].element.focus();
//                    }
//                }
//            });
//            $.validator.methods.count_children = function (value, element) {
//                var is_empty = $(element).is(":empty");
//                return !is_empty;
//            }



                /************** Disable Section Form *************/
                function disableForm() {
                    $("#statusid").prop('disabled', true);
                    $("#statusCode").prop('disabled', true);
                    $("#sortId").prop('disabled', true);
                    $("#isinitialstatus").prop('disabled', true);
                    $("#isfinalstatus").prop('disabled', true);
                    $("#statusDes").prop('disabled', true);
                }
                /************* END Disable Section Form ***********/

                /************* Enable Section Form ***************/
                function EnableForm() {
                    $("#statusid").prop('disabled', true);
                    $("#statusCode").prop('disabled', true);
                    $("#sortId").prop('disabled', false);
                    $("#isinitialstatus").prop('disabled', false);
                    $("#isfinalstatus").prop('disabled', false);
                    $("#statusDes").prop('disabled', false);
                }
                /************ END Enable Section Form ************/

                ///////////////////////search validation/////////////////////////////////////////////////////////////////////////////////////


                $('#statusSearchForm').validate({
                    rules: {
                        statusid: {
                            required: true
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }

                });




                function enableEditFields(status) {
                    if (status) {
                        //enable
                        $('#userroleid').removeAttr('disabled');
                    } else {
                        // disable
                        $('#statusid').attr('disabled', 'disabled');
                        $('#statusCode').attr('disabled', 'disabled');
                    }
                }

                function resetDropdowns() {
                    $('#id').val('');
                    $('#statusid').val('');
                    $('#statusDes').val('');
                    $('#statusCode').val('');
                    $('#sortId').val('');
                    $('#isfinalstatus').val('');
                    $('#isinitialstatus').val('');
                }

                function clearTable() {
                    table.clear().draw();
                }

                /************** Disable Section Form *************/
               // function disableForm() {
                   // $("#btninsert").prop('disabled', true);

                //}
                /************* END Disable Section Form ***********/

                function refreshPermissions(permissions) {
                    if (!permissions.search) {
                        $('#search_btn').attr('disabled', 'disabled');
                        $('#search_statusid').attr('disabled', 'disabled');

                    }
                    if (!permissions.create) {
                        disableForm();
                        $('#statusClear').attr('disabled', 'disabled');
                        $('#btnAddStatusSection').attr('disabled', 'disabled');

                    }
                }



            });

        </script>
    </body>
</html>
