<%-- 
    Document   : accessoryticketassign
    Created on : Oct 18, 2017, 12:00:34 PM
    Author     : ASHOK
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->           

            <div id="content">


                <!-- START OF ADD TERMINAL TAG -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Accessory Assign
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <!-- END OF ADD TERMINAL TAG -->


                <section id="widget-grid" class="case-datatable">

                    <!-- STATUS MESSAGE DISPLAY -->
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">

                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <!-- END STATUS MESSAGE DISPLAY -->

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Ticket Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_ticket" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>  
                                                    <th data-hide="phone"> Ticket ID</th>
                                                    <th data-hide="phone"> Ticket Date</th>
                                                    <th data-hide="phone"> Ticket Priority</th>
                                                    <th data-hide="phone"> Merchant Name</th>
                                                    <th data-hide="phone"> Part Name</th>
                                                    <th data-hide="phone"> Quantity</th>
                                                    <th data-class="expand"> Merchant Contact No</th>
                                                    <th data-hide="phone"> Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>

                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Accessory Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_accessory" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone">CATEGORY</th>
                                                    <th data-hide="phone,tablet">PART CODE</th>
                                                    <th data-hide="phone">PART NAME</th>
                                                    <th data-hide="phone">POSREVISION</th>
                                                    <th data-hide="phone,tablet">POSPTID</th>
                                                    <th data-hide="phone,tablet">POSSERIAL NUMBER</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>


                    <!-- ASSIGN BUTTON -->
                    <form id="sectionSearchForm" novalidate="novalidate" class="smart-form">
                        <div class="row">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <footer style="background-color: #ffffff">
                                    <button id="assignAccessories" type="button" class="btn btn-primary" value="Search">Assign</button>
                                </footer>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                    </form>
                    <!-- END OF ASSIGN BUTTON -->
                </section>      

            </div>
            <!-- END MAIN CONTENT -->            
        </div>
        <!-- END MAIN PANEL -->        

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">

            $(document).ready(function () {

                var ticketId = null;
                var accessorySearch = false;

                /**************************************** Load Ticket Table *****************************************/
                var responsiveHelper_dt_ticketassigndata = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var ticket_table = $('#dt_ticket').dataTable({
                    "paging": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_ticketassigndata) {
                            responsiveHelper_dt_ticketassigndata = new ResponsiveDatatablesHelper($('#dt_ticket'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_ticketassigndata.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_ticketassigndata.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/accessory/ticket_table",
                    "fnServerData": function (sSource, aoData, fnCallback) {

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "desc"]],
                    "aoColumns": [
                        {"mDataProp": "ticketid", "bSortable": false},
                        {"mDataProp": "ticketdate", "bSortable": false},
                        {"mDataProp": "ticketpriority", "bSortable": false},
                        {"mDataProp": "merchantname", "bSortable": false},
                        {"mDataProp": "partname", "bSortable": false},
                        {"mDataProp": "quantity", "bSortable": false},
                        {"mDataProp": "merchantcontactno", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]

                });
                /**************************************** END Load Ticket Table *****************************************/


                /***************************************** Accessory Table Load *********************************/
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var dt_accessory = $('#dt_accessory').dataTable({
                    "paging": true,
                    "bProcessing": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                });
                /***************************************************************************************/
                function loadODsTransferToNPA(id) {
                    if (id == null) {
                        id = "";
                    }
                    var responsiveHelper_dt_accesoryassigndata = undefined;
                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };

                    $('#dt_accessory').dataTable().fnClearTable();
                    $('#dt_accessory').dataTable().fnDestroy();

                    dt_accessory = $('#dt_accessory').dataTable({
                        "paging": true,
                        "bProcessing": true,
                        "bServerSide": true,
                        "bFilter": false,
                        "sPaginationType": "full_numbers",
                        "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                                "t" +
                                "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                        "autoWidth": true,
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_accesoryassigndata) {
                                responsiveHelper_dt_accesoryassigndata = new ResponsiveDatatablesHelper($('#dt_accessory'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_accesoryassigndata.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_accesoryassigndata.respond();
                        },
                        "sAjaxSource": "${pageContext.servletContext.contextPath}/accessory/accessory_table",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            aoData.push({"name": "ticketId", "value": id});
                            aoData.push({"name": "search", "value": accessorySearch});

                            $.ajax({
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": aoData,
                                "success": fnCallback
                            });
                        },
                        "order": [[0, "desc"]],
                        "aoColumns": [
                            {"mDataProp": "inventorycategory", "bSortable": false},
                            {"mDataProp": "partcode", "bSortable": false},
                            {"mDataProp": "partname", "bSortable": false},
                            {"mDataProp": "posrevision", "bSortable": false},
                            {"mDataProp": "posptid", "bSortable": false}, {"mDataProp": "posserialnumber", "bSortable": false},
                            {"mDataProp": "action", "bSortable": false}
                        ]
                    });
                }
                /***************************************** END Accessory Table Load *********************************/


                /******************** Ticket Table Action Button Click *******************/
                $('#dt_ticket').on('click', 'tr a', function (e) {
                    ticketId = $(this).attr('id');
                    accessorySearch = true;
                    loadODsTransferToNPA(ticketId);
                    accessorySearch = false;

                });
                /******************** END Ticket Table Action Button Click *******************/


                /*********************************** Confirmation Of Accessory Assing *************************************/
                $("#assignAccessories").click(function (e) {

                    if ($('input[name="accessory"]:checked').length > 0) {
                        var inventoryHardwareItemId = [];
                        $.each($("input[name='accessory']:checked"), function () {
                            inventoryHardwareItemId.push($(this).val());
                        });
                        inventoryHardwareItemId = JSON.stringify(inventoryHardwareItemId);
                        console.log(inventoryHardwareItemId);

                        $.SmartMessageBox({
                            title: "<i class='fa fa-times' style='color:#D50000 '></i> Are you sure you want to assign this Accessory ?",
                            content: "Warning: This action cannot be undone!",
                            buttons: '[No][Yes]'
                        }, function (ButtonPressed) {

                            if (ButtonPressed === "Yes") {
                                $.ajax({
                                    type: "POST",
                                    url: "${pageContext.servletContext.contextPath}/accessory/accessoryAssign",
                                    data: {ticketId: ticketId, inventoryHardwareItemId: inventoryHardwareItemId},
                                    async: true,
                                    cache: false,
                                    success: function (response, textStatus, jqXHR) {
                                        response = JSON.parse(response);
                                        if (response.CODE === "SUCCESS") {
                                            $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
                                            $("#excelfileUploadform").trigger('reset');
                                            $("#pagesubmit").show();
                                        } else {
                                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                        }
                                        ticketId = null;
                                        ticket_table.fnDraw();
                                        dt_accessory.fnDraw();
                                        window.scrollTo(0, 0);
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                    }
                                });
                            }
                            if (ButtonPressed === "No") {
                            }
                        });
                        e.preventDefault();
                    } else {
                        $.SmartMessageBox({
                            title: "<i class='fa fa-times' style='color:#D50000 '></i> Please select one or more Accessory before Assign...!",
                            buttons: '[Cancel]'
                        });
                    }
                });
                /*********************************** END Confirmation Of Accessory Assing *************************************/

            });

        </script>
    </body>
</html>
