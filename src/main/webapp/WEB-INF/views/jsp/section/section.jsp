<%-- 
Document   : tickecreate
Created on : Apr 27, 2017, 9:16:07 AM
Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>


            <!-- MAIN CONTENT -->
            <div id="content">

                <!-- START OF CREATE SECTION TAG -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Section Add / Edit
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <!-- END OF CREATE SECTION TAG -->

                <!-- START OF FORM -->
                <form:form id="sectionsCreateForm" novalidate="novalidate" class="smart-form" commandName="sectionAdd">
                    <input id="sectionid" name="sectionid" type="hidden" value=""/>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">


                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section Description<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="sectionDes" name="sectionDes" placeholder="Section" type="sectionDes" value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>

                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Status <samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="statusid" name="caseTypeId1" path="statusDes" class="select2 input-sm" style="width:250px" items="${statusList}"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Level <samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="sectionlevel" name="caseTypeId2" path="sectionlevel" class="select2 input-sm" style="width:250px" items="${levelList}"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>

                        <div id="parent">
                            <div class="col-xs-3">
                                <section>
                                    <label class="label">Parent Section<samp style="color: red">*</samp></label>
                                    <label class="select">
                                        <select id="parentsection" name="caseTypeId1" class="select2 input-sm" style="width:250px">
                                            <option value="">-- Select --</option>
                                        </select>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Only Parent Section</label>
                                <label class="afinity_checkbox">
                                    <input id="checkbox" name="onlyparentsection" type="checkbox" value="true"/><input type="hidden" name="_onlyparentsection" value="on"/>
                                </label>
                            </section>
                        </div>

                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">URL <samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="url" name="url" placeholder="URL" type="text" value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Icon</label>
                                <label class="input">
                                    <input id="icon" name="icon" placeholder="Section" type="icon" value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>

                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Sort Id <samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="sortid" name="sortid" placeholder="Sort Id" type="sortid" value=""/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button type="button" id="sectionSave" class="btn btn-primary btn-spinner">Save</button>
                                <button type="button" id="sectionClear" class="btn">Clear</button>                                
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>                                                                                                    
                </form:form>
                <!-- END OF FORM -->

                <!-- START OF VIEW SECTION TAG -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw "></i>
                            Search Sections
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <!-- END OF VIEW SECTION TAG -->

                <!-- SEARCH BOX -->
                <form id="sectionSearchForm" novalidate="novalidate" class="smart-form">

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section ID</label>
                                <label class="input">
                                    <input type="text" id="searchId" class="form-control" >
                                    <i></i>
                                </label>
                            </section>

                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Description</label>
                                <label class="input">
                                    <input type="text" id="searchDes" class="form-control" >
                                    <i></i>
                                </label>
                            </section>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="sectionSearch" type="button" class="btn btn-primary" value="Search">Search</button>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </form>
                <!-- END OF SEARCH BOX -->

                <!-- START OF TABLE -->
                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Section Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="section_table" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone">Section ID</th>
                                                    <th data-class="expand"><i class=""></i>Description</th>
                                                    <th data-hide="phone"><i class=""></i> Level</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"> </i>Parent</th>
                                                    <th data-hide="phone,tablet">Icon</th>
                                                    <th data-hide="phone,tablet">Url</th>
                                                    <th data-hide="phone,tablet">Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>								
                </section>
                <!-- END OF TABLE -->
            </div>
            <!-- END MAIN PANEL -->

            <!-- PAGE FOOTER -->
            <div class="page-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <span class="txt-color-white">All rights reserved Commercial Credit PLC | Powered by Avonet Technologies</span>
                    </div>
                </div>
            </div>
            <!-- END PAGE FOOTER -->

        </div>
        <!-- END MAIN CONTENT -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type='text/javascript'>

            pageSetUp();
            var status;
            $(document).ready(function () {
                console.log("ready!");

                var SECTION_RECORD_ID = null;
                var actionStatus = false;
                var parentSectionValue;

                $("#parentsection").prop('disabled', true);//Parent Section Disable
                $("#sectionClear").click(function () {

                    $("#sectionsCreateForm").validate().resetForm();
                    $("#sectionsCreateForm label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass("state-success");
                    });
                    clearSectionForm();//Clear Section Form
                    EnableForm();//Enable Input Fields
                    $("#sectionSave").fadeIn();
                    $("#msg_dev").fadeOut("slow");
                });


                /***************************** Validate SortId to print only Numbers **********************/
                $("#searchId").keydown(function (e) {
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                        return;
                    }
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
                /***************************** End Validate SortId to print only Numbers **********************/


                /***************************** #chechbox On Change **********************/
                $("#checkbox").change(function () {
                    if ($("#checkbox").is(':checked')) {
                        $("#url").val("");
                        $("#url").prop('disabled', true);
                    } else {
                        $("#url").prop('disabled', false);
                    }
                });
                /***************************** End #chechbox On Change **********************/



                /***************************** Validate Form **********************/
                $('#sectionsCreateForm').validate({
                    rules: {
                        sectionDes: {
                            required: true
                        },
                        statusDes: {
                            required: true
                        },
                        sectionlevel: {
                            required: true
                        },
                        parentsection: {
                            required: true
                        },
                        url: {
                            required: function () {
                                return (!$("#checkbox").is(':checked'));
                            }
                        },
                        sortid: {
                            required: true
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                        return false;
                    }
                });
                /***************************** End Validate Form **********************/


                /***************************** Load Table **********************/
                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var search_table = $('#section_table').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "aaSorting": [],
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#section_table'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/section/tableLoad",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "searchoptionID", "value": $.trim($('#searchId').val())});
                        aoData.push({"name": "searchoptionDES", "value": $.trim($('#searchDes').val())});

                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": "${pageContext.servletContext.contextPath}/section/tableLoad",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "sectionId", "bSortable": false},
                        {"mDataProp": "sectionDes", "bSortable": false},
                        {"mDataProp": "sectionlevel", "bSortable": false},
                        {"mDataProp": "parentSection", "bSortable": false},
                        {"mDataProp": "sectionIcon", "bSortable": false},
                        {"mDataProp": "url", "bSortable": false},
                        {"mDataProp": "statusDes", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });
                /* END BASIC */

                $('#sectionSearch').click(function () {
                    search_table.fnDraw();
                });
                /*************************** End Load Table **********************/



                /***************************** Validate SortId to print only Numbers **********************/
                $("#sortid").keydown(function (e) {
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                        return;
                    }
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
                /***************************** End Validate SortId to print only Numbers **********************/


                /***************************** Get parent section Dropdown values **********************/
                $("#sectionlevel").change(function () {
                    var sectionLevel = $('#sectionlevel').val();
                    status = false;

                    if ($(this).val() === '1') {

                        $("#parentsection").prop('disabled', false);
                        $("#parentsection").rules("add", {
                            required: true
                        });

                        $.ajax({
                            method: "POST",
                            cache: false,
                            url: "${pageContext.servletContext.contextPath}/section/loadParentSection",
                            data: {section_level: sectionLevel},
                            success: function (response) {
                                response = JSON.parse(response);

                                $("#parentsection").children(".parentItemOption").remove();

                                for (var i = 0; i < response.data.length; i++) {
                                    $("#parentsection").append($("<option class='parentItemOption'></option>")
                                            .attr("value", response.data[i].id)
                                            .text(response.data[i].value));
                                }
                                if (actionStatus) {
                                    $('#parentsection').select2("val", parentSectionValue);
                                    actionStatus = false;
                                }
                            },
                            error: function () {
                            }
                        });
                    } else {
                        $("#parentsection").rules("remove");
                        $("#parentsection").children(".parentItemOption").remove();
                        $('#parentsection').select2("val", null);
                        $("#parentsection").prop('disabled', true);
                    }

                    status = true;
                });
                /***************************** END parent section Dropdown values **********************/



                /***************************** Edit And View Records **********************************/
                /********** Action Button Click ***********/
                $('#section_table').on('click', 'tr a', function (e) {
                    var id = $(this).attr('id');
                    var value = $(this).attr('value');

                    $.ajax({
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/section/view",
                        data: {sectionId: id},
                        async: true,
                        cache: false,
                        success: function (response, textStatus, jqXHR) {
                            response = JSON.parse(response);
                            SECTION_RECORD_ID = response.data[0].sectionId;

                            if (value === "view") {
                                loadDataToField(response.data);
                                disableForm();
                                $("#sectionSave").fadeOut("slow");

                            } else if (value === "edit") {
                                loadDataToField(response.data);
                                EnableForm();
                                $("#sectionSave").fadeIn();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("In Error" + jqXHR);
                        }
                    });
                    /********** End Action Button Click ***********/


                    /********** Load Data To Input Fields ***********/
                    function loadDataToField(response) {//|| 'defult'

                        $("#sectionDes").val(response[0].sectionDes);
                        $('#statusid').select2("val", response[0].statusid);
                        $('#sectionlevel').select2("val", response[0].sectionlevel);
                        parentSectionValue = response[0].parentSection;
                        actionStatus = true;
                        $('#sectionlevel').trigger("change");

                        if (response[0].parentOnly === true) {
                            $("#checkbox").prop('checked', true);
                        } else if (response[0].parentOnly === false) {
                            $("#checkbox").prop('checked', false);
                        }
                        $("#url").val(response[0].url);
                        $("#icon").val(response[0].sectionIcon);
                        $("#sortid").val(response[0].sortId);
                    }
                });
                /********* End Load Data To Input Fields **********/

                /************** Disable Section Form *************/
                function disableForm() {
                    $("#sectionDes").prop('disabled', true);
                    $("#statusid").prop('disabled', true);
                    $("#sectionlevel").prop('disabled', true);
                    $("#parentsection").prop('disabled', true);
                    $("#checkbox").prop('disabled', true);
                    $("#url").prop('disabled', true);
                    $("#icon").prop('disabled', true);
                    $("#sortid").prop('disabled', true);
                }
                /************* END Disable Section Form ***********/

                /************* Enable Section Form ***************/
                function EnableForm() {
                    $("#sectionDes").prop('disabled', false);
                    $("#statusid").prop('disabled', false);
                    $("#sectionlevel").prop('disabled', false);
                    $("#checkbox").prop('disabled', false);
                    $("#url").prop('disabled', false);
                    $("#icon").prop('disabled', false);
                    $("#sortid").prop('disabled', false);
                }
                /************ END Enable Section Form ************/

                /*************** Clear Section Form **************/
                function clearSectionForm() {
                    SECTION_RECORD_ID = null;
                    $("#sectionDes").val("");
                    $('#statusid').select2("val", null);
                    $('#sectionlevel').select2("val", null);
                    $('#sectionlevel').trigger("change");
                    $('#parentsection').select2("val", null);
                    $("#checkbox").prop('checked', false);
                    $("#url").val("");
                    $("#icon").val("");
                    $("#sortid").val("");
                }
                /************** END Clear Section Form ************/

                /***************************** End Edit And View Records **********************************/


                /******* Set form values to JSON Object **********/
                function sectionInfoToJSON() {
                    var request = new Object();
                    var action;

                    if (SECTION_RECORD_ID === null) {
                        action = "create";
                    } else {
                        action = "update";
                    }
                    request.sectionid = SECTION_RECORD_ID;
                    SECTION_RECORD_ID = null;

                    request.sectionDes = $('#sectionDes').val();
                    request.statusid = $('#statusid').prop('value');
                    request.sectionlevel = $('#sectionlevel').prop('value');
                    request.parentsection = $('#parentsection').prop('value');
                    request.icon = $('#icon').val();

                    if ($("#checkbox").is(':checked')) {
                        request.onlyparent = true;
                        request.url = "";
                    } else {
                        request.onlyparent = false;
                        request.url = $('#url').val();
                    }
                    request.sortid = $('#sortid').val();
                    request.action = action;
                    return request;
                }
                /******* END Setting form values to JSON Object *****/


                /************* Save Section Form Data **************/
                $("#sectionSave").click(function () {
                    if ($("#sectionsCreateForm").valid()) {
                        var content = sectionInfoToJSON();
                        var action = content.action;
                        content = JSON.stringify(content);
                        if (content !== null) {
                            $.ajax({
                                method: "POST",
                                cache: false,
                                url: "${pageContext.servletContext.contextPath}/section/insert",
                                data: {section_info: content, action: action},
                                success: function (response) {
                                    response = JSON.parse(response);

                                    if (response.success === true) {
                                        $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> Record Sucessfully Created</div> <br/>');
                                        clearSectionForm();
                                        console.log("true");
                                    } else if (response.success === false) {
                                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '/div> <br/>');
                                        window.scrollTo(0, 0);
                                        console.log("false");
                                    }
                                    $("#sectionsCreateForm").validate().resetForm();
                                    $("#sectionsCreateForm label").each(function () {
                                        $(this).removeClass('state-error');
                                        $(this).removeClass("state-success");
                                    });                                    
                                    clearSectionForm();
                                    search_table.fnDraw();
                                    $("#msg_dev").fadeIn();
                                },
                                error: function () {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                            });
                        } else {
                            $('#sectionSave').button('reset');
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Fill All Information </div> <br/>');
                        }
//                        $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                    } else {
                        $('#sectionSave').button('reset');
                    }
                });
                /************ END Save Section Form Data ***********/

            });
        </script>
    </body>
</html>