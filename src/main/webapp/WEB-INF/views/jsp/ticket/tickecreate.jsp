<%-- 
    Document   : tickecreate
    Created on : Apr 27, 2017, 9:16:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Create   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Ticket Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form:form commandName="ticketForm">
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Ticket Date <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <form:input path="ticketdate" cssClass="input-sm" readonly="readonly"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Ticket Priority <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="ticketpriority" cssClass="input-sm" items="${ticketPriorityList}"/>
                                            <i></i>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Product</label>
                                        <label class="select">
                                            <form:select path="product" class="input-sm" items="${productList}"/>
                                            <i></i>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Product Category</label>
                                        <label class="select">
                                            <form:select path="productcategory" class="input-sm"/>
                                            <i></i>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <!--                                <div class="col-xs-5">
                                                                    <section>
                                                                        <label class="label">Territory</label>
                                                                        <label class="select">
                                <%--<form:select path="territory" class="input-sm"/>--%>
                                <i></i>
                            </label>
                        </section>
                    </div>-->
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Ticket Category <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select path="ticketcategory" class="input-sm" items="${ticketCategoryList}"/>
                                            <i></i>
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dynamiccontent"></div>

                    <div class="col-xs-offset-1 col-xs-10" style="padding-bottom: 100px;">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-12">
                                    <footer style=" background-color: #ffffff; margin-bottom: 10px;  margin-top: 30px;">
                                        <button id="save" type="button" class="btn btn-primary pull-rignt btn-spinner">Save</button>
                                    </footer>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="col-xs-offset-2 col-xs-8" style="padding-bottom: 100px;">
                                            <div class="smart-form">
                                                <div class="row">
                                                    <div class="col-xs-1"></div>
                                                    <div class="col-xs-12">
                                                        <footer style=" background-color: #ffffff; margin-bottom: 10px;  margin-top: 10px;">
                                                            <button id="save" type="button" class="btn btn-primary pull-rignt">Save</button>
                                                        </footer>
                                                    </div>
                                                    <div class="col-xs-1"></div>
                                                </div>
                                            </div>
                                        </div>-->
                </form:form>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type='text/javascript'>
            $(document).ready(function () {
                $("#terminalserialno").mask('999-999-999');
                $("#bterminalserialnumber").mask('999-999-999');
                $("#startserialno").mask('999-999-999');
                $("#endserialno").mask('999-999-999');

                $('#ticketdate').datetimepicker({
                    format: "YYYY-MM-DD",
                    defaultDate: new Date(),
                    minDate: new Date(),
                    maxDate: new Date()
                });
                $('#ticketForm').validate({
                    onkeyup: function (element) {
                        var element_id = $(element).attr('id');
                        if (this.settings.rules[element_id].onkeyup !== false) {
                            $.validator.defaults.onkeyup.apply(this, arguments);
                        }
                    },
                    rules: {
                        ticketdate: {
                            required: true
                        },
                        ticketpriority: {
                            required: true
                        },
//                        terminalserialno: {
//                            required: true,
//                            minlength: 11,
//                            maxlength: 11,
//                            onkeyup: false,
//                            remote: {
//                                url: ':app/ticket/validateSerialNumber',
//                                type: 'get',
//                                data: {serialnumber: function () {
//                                        return $('#terminalserialno').val();
//                                    }
//                                },
//                                async: true
//                            }
//                        },
                        ticketcategory: {
                            required: true
                        }
                    },
                    messages: {
                        terminalserialno: {
                            remote: "Terminal Serial number is duplicated."
                        },
                        startserialno:{
                            remote: "Serial Number already in use"
                        },
                        endserialno:{
                            remote: "Serial Number already in use"
                        }                         
                    },
                    invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });

                $("#product").change(function () {
                    $('#productcategory').empty();
                    if ($("#product").val()) {
                        $.get("${pageContext.servletContext.contextPath}/ticket/getproductcategory", {
                            product: $("#product").val()
                        }, "json").done(function (data) {
                            $.each(jQuery.parseJSON(data), function (key, value) {
                                $("#productcategory").append($("<option></option>")
                                        .attr("value", value.id)
                                        .text(value.value));
                                $("#productcategory").trigger("change");

                            });
                        });
                    } else {
                        $("#productcategory").append($("<option></option>")
                                .attr("value", "")
                                .text("-- Select --"));

                    }
                });

                $('#ticketcategory').change(function () {
                    $('#save').unbind();
                    $('#dynamiccontent input, #dynamiccontent select, #dynamiccontent textarea').each(
                            function (index) {
                                $(this).rules('remove');
                                clearValidations();
                                resetDropdowns();
                            });
                    $('#dynamiccontent').empty();
                    if ($('#ticketcategory').val()) {
                        $.get('${pageContext.servletContext.contextPath}/ticket/getdynamiccontent',
                                {
                                    ticketcategory: $('#ticketcategory').val(),
                                    state: 'C'

                                },
                                "json").done(function (data) {
                            $('#dynamiccontent').html(data);
                        });
                    }

                });


                /**************clear validation*****************/
                function clearValidations() {

                    $("#ticketForm").validate().resetForm();
                    $("#ticketForm label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass('state-success');
                    });
                }

                /*****************Reset Dropdown*************/
                function resetDropdowns() {
                    $('#ticketpriority').val('');
                    $('#product').val('');
                    $('#productcategory').val('');

                }

            });

        </script>
    </body>
</html>