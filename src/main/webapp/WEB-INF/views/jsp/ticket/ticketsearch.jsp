<%-- 
    Document   : ticketsearch
    Created on : Apr 27, 2017, 9:16:36 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%> 
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">

        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">

            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="${pageContext.servletContext.contextPath}/ticket/search"> Ticket Management</a></li>
                </ol>
                <!-- end breadcrumb -->

            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search"></i>
                            Search         
                        </h1>
                        <span>

                        </span>
                    </div>
                </div>

                <!-- SEARCH BOX -->
                <div class="row">
                    <!-- NEW COL START -->
                    <form>
                        <article class="col-sm-12 col-md-12 col-lg-6">

                            <div class="input-group input-group-sm ">
                                <input type="text" id="searchId" class="form-control" placeholder="Ticket ID / TID" >
                                <div class="input-group-btn">
                                    <button id="search_btn" type="button" class="btn btn-primary" value="Search">Search</button>
                                </div>
                            </div>
                        </article>
                    </form>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p style="color: white">|</p>
                    </div>
                </div>
                <!-- END OF SEARCH BOX -->

                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2> Ticket search results</h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone"> Ticket ID</th>
                                                    <th data-hide="phone"> Ticket Date</th>
                                                    <th data-hide="phone"> Ticket Priority</th>
                                                    <th data-hide="phone"> Merchant Name</th>
                                                    <th data-class="expand"> Merchant Contact No</th>
                                                    <th data-hide="phone"> Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->

                    </div>								
                </section>

            </div>

            <!-- END MAIN CONTENT -->

        </div>

        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="/WEB-INF/views/jsp/template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">

        </script>

        <script type="text/javascript">


            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            var search = false;

            $(document).ready(function () {
                pageSetUp();
                /* // DOM Position key index //
                 
                 l - Length changing (dropdown)
                 f - Filtering input (search)
                 t - The Table! (datatable)
                 i - Information (records)
                 p - Pagination (paging)
                 r - pRocessing 
                 < and > - div elements
                 <"#id" and > - div with an id
                 <"class" and > - div with a class
                 <"#id.class" and > - div with an id and class
                 
                 Also see: http://legacy.datatables.net/usage/features
                 */

                /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                var search_table = $('#dt_basic').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/ticket/searched",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "searchoptionID", "value": $.trim($('#searchId').val())});
                        var content = JSON.stringify($('#searchId').val());
                        $.ajax({
                            "dataType": 'json',
                            "type": "GET",
                            "url": "${pageContext.servletContext.contextPath}/ticket/searched",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "desc"]],
                    "aoColumns": [
                        {"mDataProp": "ticketid", "bSortable": false},
                        {"mDataProp": "ticketdate", "bSortable": false},
                        {"mDataProp": "ticketpriority", "bSortable": false},
                        {"mDataProp": "merchantname", "bSortable": false},
                        {"mDataProp": "merchantcontactno", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}

                    ]
                });

                /* END BASIC */

                $('#search_btn').click(function () {
                    search_table.fnDraw();
                });

                $('#dt_basic').on('click', 'tr a', function (e) {
                   
                    var value = $(this).attr('value');
                    var ticketId = $(this).data("id");
                    if (value === "switch") {
                        $.SmartMessageBox({
                            title: "<i class='fa fa-question-circle' ></i> Are you sure you want to assign this ticket to yourself ?",
                            buttons: '[No][Yes]'
                        }, function (ButtonPressed) {

                            if (ButtonPressed === "Yes") {
                                console.log("yes or no", ButtonPressed);
                                 console.log("ID : " + ticketId);
                                $.ajax({
                                    type: "POST",
                                    url: "${pageContext.servletContext.contextPath}/ticket/switchAssignee",
                                    data: {ticketid: ticketId},
                                    async: true,
                                    cache: false,
                                    success: function (data) {
                                        if (data.code == "SUCCESS") {
                                            $("#msg_dev").html("<div class=''alert alert-success''><strong>Success!</strong> " + data.message + "</div> <br/>");
                                        } else {
                                            $("#msg_dev").html("<div class=''alert alert-warning''><strong>Warning!</strong> " + data.message + "</div> <br/>");
                                        }
                                        $("#msg_dev").focus();
                                    },
                                    error: function (e) {
                                        console.log(e);
                                    },
                                });
                            }
                            if (ButtonPressed === "No") {
                            }

                        });
                    }
                });

//                $('#dt_basic').on('click', 'tr a', function (e) {
//                    console.log("ID : " + $(this).data("id"));
//                    var value = $(this).attr('value');
//                    if (value === "switch") {
//
//                        $.ajax({
//                            type: "POST",
//                            url: "${pageContext.servletContext.contextPath}/ticket/switchAssignee",
//                            data: {ticketid: $(this).data("id")},
//                            async: true,
//                            cache: false,
//                            success: function (data) {
//                                if (data.code == "SUCCESS") {
//                                    $("#msg_dev").html("<div class=''alert alert-success''><strong>Success!</strong> " + data.message + "</div> <br/>");
//                                } else {
//                                    $("#msg_dev").html("<div class=''alert alert-warning''><strong>Warning!</strong> " + data.message + "</div> <br/>");
//                                }
//                                $("#msg_dev").focus();
//                            },
//                            error: function (e) {
//                                console.log(e);
//                            },
//                        });
//                    }
//
//
//                });


                /***************************** Search Id only Numbers **********************/
                $("#searchId").keydown(function (e) {
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                        return;
                    }
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
                /***************************** End Search Id only Numbers **********************/

            });

//            function clearTable() {
//                search = false;
//                $('#download').val('');
//                $('#dt_basic').dataTable().fnClearTable();
//            }

            $('#searchoption').on('change', function () {
                $('#input').val('');
            });


            $('#input').keyup(function () {
                var searchoption = $('#searchoption').val();
                if (searchoption === "nameInFull") {
                    $('#input').val($('#input').val().toUpperCase());
                }
            });

            $('#input').bind('keyup', function (e) {
                if (e.keyCode === 13) {
                    search = true;
                    search_table.fnDraw();
                }

            });


        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </body>
</html>