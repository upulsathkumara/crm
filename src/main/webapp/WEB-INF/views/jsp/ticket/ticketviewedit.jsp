<%-- 
    Document   : ticketview
    Created on : Apr 27, 2017, 9:16:19 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            View / Edit   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="smart-form">
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-xs-12">
                                    <header><h4>Ticket Info</h4></header>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form:form commandName="ticketForm">
                    <form:hidden path="ticketcategoryid"/>
                    <div class="col-xs-offset-2 col-xs-8">
                        <div class="smart-form">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Ticket ID</label>
                                        <label class="input">
                                            <form:input id="ticketid" path="ticketid" cssClass="input-sm" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Ticket Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <form:input path="ticketdatetime" cssClass="input-sm" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Ticket Priority</label>
                                        <label class="input">
                                            <form:input path="ticketpriority" cssClass="input-sm" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Product</label>
                                        <label class="input">
                                            <form:input path="product" cssClass="input-sm" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-5 pull-right">
                                    <section>
                                        <label class="label">Product Category</label>
                                        <label class="input">
                                            <form:input path="productcategory" cssClass="input-sm" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <!--                                                                <div class="col-xs-5">
                                                                                                    <section>
                                                                                                        <label class="label">Territory</label>
                                                                                                        <label class="input">
                                <%--<form:input path="territorymap" cssClass="input-sm" readonly="true"/>--%>
</label>
</section>
</div>                                            -->
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Ticket Category</label>
                                        <label class="input">
                                            <form:input path="ticketcategory" cssClass="input-sm" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dynamicview">
                        <div id="dynamiccontent"></div>
                        <div id="recontent"></div>
                        <div id="viewtickethistorytable"></div>
                    </div>

                    <!--                    <div class="col-xs-offset-2 col-xs-8" style="padding-bottom: 100px;">
                                            <div class="smart-form">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <a id="save" class="pull-right btn btn-success btn-sm" href="javascript:void(0);"><i class="fa fa-save" style="padding-right: 10px;"></i>Save</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                </form:form>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type='text/javascript'>
            $(document).ready(function () {
                $('#ticketForm').validate({
                    onfocusout: false,
                    invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });

                $('#dynamiccontent').empty();
                $.get('${pageContext.servletContext.contextPath}/ticket/getdynamiccontent',
                        {
                            ticketcategory: $('#ticketcategoryid').val(),
                            state: 'VE'
                        },
                "json").done(function (data) {
                    $('#dynamiccontent').html(data);
                });

            });
        </script>
    </body>
</html>