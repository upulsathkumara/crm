<%-- 
    Document   : tickecreate
    Created on : Apr 27, 2017, 9:16:07 AM
    Author     : Roshen Dilshan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>

            <!-- MAIN CONTENT -->
            <div id="content">


                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Section Task Add
                            <span>

                            </span>
                        </h1>
                    </div>
                </div>
                <form:form id="sectionTaskCreateForm" novalidate="novalidate" class="smart-form" commandName="sectionTaskAdd">
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">

                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row" >
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section<samp style="color: red">*</samp></label>
                                <label class="select">                                    
                                    <!--<select id="sectionid" name="sectionid" class="select2 input-sm"></select>-->

                                    <%--<form:select id="sectionlevel" name="caseTypeId2" class="select2 input-sm" path="sectionlevel" style="width:250px" items="${levelList}"/>--%>
                                    <form:select  name="sectionid" class="select2 input-sm" path="sectionid" items="${selectIdList}" />
                                </label>
                            </section>
                        </div>

                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section class="clearfix">
                                <label class="label">Tasks<samp style="color: red">*</samp></label>                                
                                <select id="taskid" name="taskid" cssStyle="width:100%" Class="select2-container select2-container-multi select2 " multiple="true"></select>
                            </section>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label"></label>
                                <label class="input" id="dynamicLbl">
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div> 
                        <div class="col-xs-3">
                            <section>
                                <label class="label"></label>
                                <label class="input" id="dynamic">

                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>   
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="SaveSectionTask" type="button" class="btn btn-primary btn-spinner" value="Submit">Save</button>
                                <button id="ClearSectionTask" type="button" class="btn btn-default">Clear</button>                                
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                </form:form>
                <!-- END OF FORM -->    

                <!-- START OF VIEW SECTION TAG -->
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!--<i class="fa fa-edit fa-fw "></i>--> 
                            <i class="fa fa-search fa-fw "></i>
                            Search Section Tasks
                            <span>

                            </span>
                        </h1>
                    </div>                    
                </div>
                <!-- END OF VIEW SECTION TAG -->


                <!-- SEARCH BOX -->
                <form id="sectionTaskSearchForm" novalidate="novalidate" class="smart-form">

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Section / Task ID</label>

                                <label class="input">
                                    <input id="searchId" class="form-control" >
                                    <i></i>
                                </label>
                                <i></i>
                            </section>

                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Description</label>
                                <label class="input">
                                    <input id="searchDes" class="form-control" value="" >
                                    <i></i>
                                </label>                                
                                <i></i>
                            </section>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="sectionTaskSearch" type="button" class="btn btn-primary" value="Search">Search</button>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </form>
                <!-- END OF SEARCH BOX -->


                <!-- START OF TABLE -->
                <section id="widget-grid" class="case-datatable">

                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" role="widget">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Section Task Details</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <table id="sectionTask_table" class="table table-bordered">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone">Section Id</th>
                                                    <th data-hide="phone">Task Id</th>
                                                    <th data-hide="phone">Section</th>
                                                    <th data-class="expand"><i class=""></i>Task</th> 
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Date Time</th>
                                                    <th data-class="expand"><i class=""></i>Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                            </tbody>
                                        </table>
                                        </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->	                            
                        </article>
                        <!-- WIDGET END -->
                    </div>								
                </section>
                <!-- END OF TABLE -->

            </div>
            <!-- END MAIN PANEL -->


            <!-- PAGE FOOTER -->
            <div class="page-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <span class="txt-color-white">All rights reserved Commercial Credit PLC | Powered by Avonet Technologies</span>
                    </div>
                </div>
            </div>
            <!-- END PAGE FOOTER -->

        </div>
        <!-- END MAIN CONTENT -->


        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>


        <script type="text/javascript">

            pageSetUp();
            $(document).ready(function () {
                console.log("ready!");
                var SELECTED_SECTION_ID = null; //Selected Section for edit or view
                var ACTION_CLICKED = false; //User Clicked Edit or view button
                var DINAMIC_FIELD = []; //Required Dinamic Field Items
                var ASSIGNED_TASKS = []; //Assigned tasked from DB
                var RESPONSE = null; //Response data for update and view


                /***************************** Validate SortId to print only Numbers **********************/
                $("#searchId").keydown(function (e) {
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                            (e.keyCode >= 35 && e.keyCode <= 40)) {
                        return;
                    }
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
                /***************************** End Validate SortId to print only Numbers **********************/


                /***************************** Save Section Task **********************/
                $('#SaveSectionTask').click(function () {
                    var action;
                    var saveOrEditUrl = "";
                    if (ACTION_CLICKED) {
                        saveOrEditUrl = "${pageContext.servletContext.contextPath}/sectiontask/update";
                        action = "update";
                        console.log(action);
                    } else {
                        saveOrEditUrl = "${pageContext.servletContext.contextPath}/sectiontask/insert";
                        action = "create";
                        console.log(action);
                    }

                    if ($("#sectionTaskCreateForm").valid()) {
                        var sectionTaskInfo = [];
                        var sectionId = $("#sectionid :selected").val();
                        $('[data-url-id]').each(function () {
                            var obj = new Object();
                            obj.sectionid = sectionId;
                            obj.taskid = $(this).attr('data-url-id');
                            obj.url = $(this).val();
                            sectionTaskInfo.push(obj);
                        });
                        var content = JSON.stringify(sectionTaskInfo);
                        $.ajax({
                            method: "POST",
                            cache: false,
                            url: saveOrEditUrl,
                            data: {requestInfo: content, action: action},
                            success: function (response) {
                                response = JSON.parse(response);
                                $("#ClearSectionTask").trigger("click");
                                if (response.success === true) {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.messageService + '</div> <br/>');
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.messageService + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                                $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                                $("#sectionTaskCreateForm").validate().resetForm();
                                $("#sectionTaskCreateForm label").each(function () {
                                    $(this).removeClass('state-error');
                                    $(this).removeClass("state-success");
                                });
                                ACTION_CLICKED = false;
                                search_table.fnDraw();
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                                window.scrollTo(0, 0);
                            }
                        });
                    } else {
//                        console.log("this is validate fail area");
//                        $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Fill All Information </div> <br/>');
//                        $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
//                        SELECTED_SECTION_ID = null;
                        console.log(SELECTED_SECTION_ID);
//                        EnableFormForEdit();
//                        $("#sectionid").prop('disabled', false);
//                        $('#sectionid').select2("val", null);
//                        $("#taskid").empty();
//                        $('#taskid').select2("val", null);
//                        $(".clearFields").remove();
//                        DINAMIC_FIELD = [];
                        console.log(DINAMIC_FIELD);
                    }
                });
                /***************************** END Save Section Task **********************/


                /***************************** Clear Form **********************/
                $("#ClearSectionTask").click(function () {
                    SELECTED_SECTION_ID = null; //Selected Section for edit or view
                    ACTION_CLICKED = false; //User Clicked Edit or view button
                    DINAMIC_FIELD = []; //Required Dinamic Field Items
                    ASSIGNED_TASKS = []; //Assigned tasked from DB
                    clearSectionForm();
                    $("#SaveSectionTask").fadeIn();
                });
                /***************************** End Clear Form **********************/


                /***************************** Edit And View Records **********************************/
                $('#sectionTask_table').on('click', 'tr a', function (e) {
                    clearSectionForm(); //Clear Section Form
                    var id = $(this).attr('id');
                    var value = $(this).attr('value');
                    $.ajax({
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/sectiontask/view",
                        data: {sectionId: id},
                        async: true,
                        cache: false,
                        success: function (response, textStatus, jqXHR) {
                            response = JSON.parse(response);
                            SELECTED_SECTION_ID = response.sectionId;
                            if (value === "view") {
                                ACTION_CLICKED = false;
                                window.checkExist = setInterval(function () {
                                    if ($("#dynamic input.clearFields").length) {
                                        console.log("Exists!");
                                        disableForm();
                                        clearInterval(window.checkExist);
                                    }
                                }, 100);
                                loadDataToField(response);
                                console.log('disabled Form');
                                $("#SaveSectionTask").fadeOut("slow");
                            } else if (value === "edit") {
                                ACTION_CLICKED = true;
                                loadDataToField(response);
                                EnableFormForEdit();
                                window.checkExist = setInterval(function () {
                                    if ($("#dynamic input.clearFields").length) {
//                                        $("#sectionTaskCreateForm #dynamic input").rules("add", {
//                                            required: true
//                                        });
                                        clearInterval(window.checkExist);
                                    }
                                }, 100);
                                $("#SaveSectionTask").fadeIn();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("In Error" + jqXHR);
                        }
                    });
                });
                /***************************** End Edit And View Records **********************************/


                /***************************** Load #taskid Dropdown **********************/
                $("#sectionid").change(function () {
                    $("#taskid").empty();
                    $('#taskid').select2("val", null);
                    $(".clearFields").remove();
                    if ($("#sectionid").val() !== "") {
                        $.ajax({
                            method: "POST",
                            cache: false,
                            url: "${pageContext.servletContext.contextPath}/sectiontask/loadTask",
                            success: function (response) {
                                response = JSON.parse(response);
                                for (var i = 0; i < response.data.length; i++) {

                                    $("#taskid").append($("<option></option>")
                                            .attr("value", response.data[i].id)
                                            .text(response.data[i].value));
                                }
                                $("#taskid").val(ASSIGNED_TASKS).trigger('change');
                                RESPONSE = null;
                                ASSIGNED_TASKS = [];
                            },
                            error: function () {
                            }
                        });
                    }
                });
                /**************************** End Loading #taskid Dropdown **********************/


                /************** Function on change of task selector *************/
                $('#taskid').change(function () {
                    GenerateDynamicFields();
                });
                /************** End of Function on change of task selector *************/


                /************* Enable Section Form ***************/
                function EnableFormForEdit() {
                    $("#taskid").prop('disabled', false);
                    $(".clearFields").prop('disabled', false);
                }
                /************ END Enable Section Form ************/


                /************** Disable Section Form *************/
                function disableForm() {
                    $("#sectionid").prop('disabled', true);
                    $("#taskid").prop('disabled', true);
                    console.log("Before");
                    $("#dynamic input.clearFields").prop('disabled', true);
                    console.log("After");
                }
                /************* END Disable Section Form ***********/


                /*************** Clear Section Form **************/
                function clearSectionForm() {
                    DINAMIC_FIELD = [];
                    EnableFormForEdit();
                    $("#sectionid").prop('disabled', false);
                    $('#sectionid').select2("val", null);
                    $("#taskid").empty();
                    $('#taskid').select2("val", null); //Remove selected Tasks
                    $(".clearFields").remove();
                    $("#sectionTaskCreateForm").validate().resetForm();
                    $("#sectionTaskCreateForm label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass("state-success");
                    });
                }
                /************** END Clear Section Form ************/


                /************** Function to generate dynamic text fields *************/
                function GenerateDynamicFields() {
                    var sectionId = $('#sectionid option:selected').text();
                    var selections = [];
                    if ($('#taskid').val()) {
                        selections = $('#taskid').val();
                    }

                    for (var i = 0; DINAMIC_FIELD.length > i; i++) {
                        if (jQuery.inArray(DINAMIC_FIELD[i], selections) === -1) {
                            $('[data-task-id="' + DINAMIC_FIELD[i] + '"]').remove();
                            $('[data-url-id="' + DINAMIC_FIELD[i] + '"]').remove();
                            DINAMIC_FIELD.splice(i, 1);
                        }
                    }

                    if (RESPONSE === null) {

                        for (var i = 0; i < selections.length; i++) {
                            var optionDescriprion = $('#taskid option[value="' + selections[i] + '"]').html();
                            if (jQuery.inArray(selections[i], DINAMIC_FIELD) === -1) {
                                DINAMIC_FIELD.push(selections[i]);
                                var elem1 = '<label class="label clearFields" data-task-id="' + selections[i] + '" style="margin-top:15px">' + optionDescriprion + ' URL  <samp style="color: red">*</samp></label>';
                                $('#dynamicLbl').append(elem1);
                                var elem = '<input class="input-sm clearFields" id = "' + selections[i] + '" name = "' + selections[i] + '" type="text" data-url-id="' + selections[i] + '" placeholder="' + sectionId + "/" + optionDescriprion + '" style="margin-bottom:5px"/>';
                                $('#dynamic').append(elem);
//                                $("'#" + selections[i] + "'").rules("add", "required");
                                $("#" + selections[i] + "").rules("add", {required: true});
                            }
                        }
                    } else {
                        DINAMIC_FIELD = selections;
                        for (var i = 0; i < RESPONSE.urlList.length; i++) {
                            if (jQuery.inArray(RESPONSE.urlList[i], DINAMIC_FIELD) === -1) {
                                DINAMIC_FIELD.push(RESPONSE.urlList[i]);
                                var elem1 = '<label class="label clearFields" data-task-id="' + RESPONSE.urlList[i].taskId + '" style="margin-top:15px">' + RESPONSE.urlList[i].taskDes + ' URL <samp style="color: red">*</samp></label>';
                                $('#dynamicLbl').append(elem1);
                                var elem = '<input class="input-sm clearFields" id = "' + RESPONSE.urlList[i].taskId + '" name = "' + RESPONSE.urlList[i].taskId + '" type="text" data-url-id="' + RESPONSE.urlList[i].taskId + '" value="' + RESPONSE.urlList[i].url + '" style="margin-bottom:5px"/>';
                                $('#dynamic').append(elem);
//                                $("'#" + RESPONSE.urlList[i].taskId + "'").rules("add", "required");
                                $("#" + RESPONSE.urlList[i].taskId + "").rules("add", {required: true});
                            }
                        }
                    }

                }
                /************** End of Function to generate dynamic text fields *************/


                /************************** Load Data To Input Fields **************************/
                function loadDataToField(response) {
//                    return new Promise((resolve, reject) => {
                    for (var i = 0; i < response.sectionInfo.length; i++) {
                        $("#sectionid").append($("<option></option>")
                                .attr("value", response.sectionInfo[i].id)
                                .text(response.sectionInfo[i].value));
                    }
                    $('#sectionid').select2("val", response.selectedSectionId);
                    for (var i = 0; i < response.urlList.length; i++) {
                        ASSIGNED_TASKS.push(response.urlList[i].taskId);
                    }
                    RESPONSE = response;
                    $("#sectionid").trigger("change"); //Load data to Task Drop Down
                    $("#sectionid").prop('disabled', true); //Disable Section Dropdown  
//                    });

                }
                /************************** END Load Data To Input Fields **************************/


                /***************************** Validate Form **********************/
                window.loadValidations = () => {
                    $('#sectionTaskCreateForm').validate({
                        rules: {
                            sectionid: {
                                required: true
                            },
                            taskid: {
                                required: true
                            }
                        },
                        messages: {
                            sectionid: {
                                required: "Please select section."
                            },
                            taskid: {
                                required: "Please select at least one task."
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element.parent());
                        }
                    });
                    $.validator.methods.count_children = function (value, element) {
                        var is_empty = false;
                        if ($(element).val()) {
                            is_empty = true;
                        }
                        return !is_empty;
                    };
                }
                window.loadValidations();
                /***************************** End Validate Form **********************/


                /***************************** Load Table **********************/
                var responsiveHelper_dt_basic = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                var search_table = $('#sectionTask_table').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#sectionTask_table'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/sectiontask/loadTable",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "searchoptionID", "value": $.trim($('#searchId').val())});
                        aoData.push({"name": "searchoptionDES", "value": $.trim($('#searchDes').val())});
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "sectionId", "bSortable": false},
                        {"mDataProp": "taskId", "bSortable": false},
                        {"mDataProp": "sectionDes", "bSortable": false},
                        {"mDataProp": "taskDes", "bSortable": false},
                        {"mDataProp": "lastupdateddatetime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });
                /* END BASIC */

                $('#sectionTaskSearch').click(function () {
                    search_table.fnDraw();
                });
                /*************************** End Load Table **********************/
            });
        </script>

    </body>
</html>