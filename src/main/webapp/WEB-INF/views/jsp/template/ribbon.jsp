<%-- 
    Document   : ribbon
    Created on : Apr 26, 2017, 11:16:19 AM
    Author     : Roshen Dilshan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- RIBBON -->
<div id="ribbon">
    <div class="row">
        <div class="col-xs-6">
            <ol class="breadcrumb">
                <li>${root}</li><li>${page}</li>
            </ol>
        </div>
        <div class="col-xs-6">
            <p style="color: #ffffff; padding-top: 11px" class="text-right"><c:out value="${sessionScope.lastlogin}" /></p>
        </div>
    </div>
</div>
<!-- END RIBBON -->
