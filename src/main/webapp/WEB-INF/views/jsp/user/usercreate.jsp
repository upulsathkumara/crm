<%-- 
    Author     : Roshen Dilshan
    Document   : usercreate
    Created on : Sep 14, 2015, 2:26:09 PM
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>

        <style>
            .smart-form *, .smart-form:after, .smart-form:before {    
                box-sizing: border-box;
            }
            .smart-form select.input-sm{
                height: 31px;
            }
        </style>
    </head>

    <body class="">

        <!-- HEADER -->
        <header id="header">
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="${pageContext.servletContext.contextPath}/user">User Management</a></li><li>Create User</li>
                </ol>

            </div>
            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            Create User 
                            <span>
                            </span>
                        </h1>
                    </div>
                </div>

                <form:form id="userCreate" novalidate="novalidate" class="smart-form" commandName="user" action="${pageContext.servletContext.contextPath}/user/create" autocomplete="off">
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10" tabindex="0">
                            <c:if test="${not empty successMsg}">
                                <div class="alert alert-success">
                                    <strong>Success!</strong> ${successMsg}
                                </div>
                            </c:if> 
                            <c:if test="${not empty errorMsg}">
                                <div class="alert alert-warning">
                                    <strong>Warning!</strong> ${errorMsg}
                                </div>
                                <br/>
                            </c:if> 
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row" style="padding-bottom: 30px;">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <header><h4>Employee Information</h4></header>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <section>
                                <label class="label">Name in Full <samp style="color: red">*</samp></label>
                                <label class="input">
                                    <form:input id="nameinfull" path="nameinfull" cssClass="input-sm" placeholder="Name in Full"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-4">
                                <div class="col-xs-10">
                                    <section>
                                        <label class="label">Initials <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:input id="initials" path="initials" cssClass="input-sm" placeholder="Initials"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <div class="col-xs-4">
                                <div class="col-xs-12">
                                    <section>
                                        <label class="label">Preferred Name <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:input id="preferredname" path="preferredname" cssClass="input-sm" placeholder="Preferred Name"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-10">
                                    <section>
                                        <label class="label">Surname <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <form:input id="surname" path="surname" cssClass="input-sm" placeholder="Surname"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <!--                    <div class="row">
                                            <div class="col-xs-2"></div>
                                            <div class="col-xs-8">
                                                <div class="col-xs-5">
                                                    <section>
                                                        <label class="label">EPF </label>
                                                        <label class="input">
                    <%--<form:input id="epf" path="epf" cssClass="input-sm" placeholder="EPF" maxlength="8"/>--%>
                </label>
            </section>
        </div>
    </div>
    <div class="col-xs-2"></div>
</div>-->
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">NIC </label>
                                    <label class="input">
                                        <form:input id="nic" path="nic" cssClass="input-sm" placeholder="NIC" maxlength="12"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">E-mail <samp style="color: red">*</samp></label>
                                    <label class="input">
                                        <form:input id="email" path="email" cssClass="input-sm" placeholder="E-mail"/>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Contact # 01 <!--<samp style="color: red">*</samp>--></label>
                                    <label class="input">
                                        <form:input id="contactno01" path="contactno01" cssClass="input-sm" placeholder="Contact # 02"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Contact # 02 </label>
                                    <label class="input">
                                        <form:input id="contactno02" path="contactno02" cssClass="input-sm" placeholder="Contact # 02"/>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Hierarchy <samp style="color: red">*</samp></label>
                                    <label class="select">
                                        <form:select id="hierarchyid" path="selhierarchyid" cssClass="input-sm" items="${hierarchylist}"/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Status <samp style="color: red">*</samp></label>
                                    <label class="select">
                                        <form:select id="status" path="selstatus" cssClass="input-sm" items="${statuslist}"/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Product </label>
                                        <label class="select">
                                            <form:select id="products" path="selproducts" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4" items="${productlist}"/>
                                        </label>
                                    </section>                                
                                </div>
                                <div class="col-xs-2"></div>
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Product Subcategory </label>
                                        <label class="select">
                                            <form:select id="productscategorytype" path="selproductscategorytype" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>

                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row clearfix" >
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <!--                            <div class="col-xs-5">
                                                            <section>
                                                                <label class="label">Territory <samp style="color: red">*</samp></label>
                                                                <label class="select">
                            <%--<form:select id="territories" path="selterritories" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4"/>--%>
                        </label>
                    </section>
                </div>
                <div class="col-xs-2"></div>-->
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Supervisor </label>
                                    <label class="select">
                                        <form:select id="supervisors" path="selsupervisors" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row" style="padding-bottom: 30px;">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <header><h4>User Account</h4></header>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="row">
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">Username <samp style="color: red">*</samp></label>
                                        <label class="input">
                                            <%--<form:input id="employeeid" path="employeeid" cssClass="input-sm" placeholder="Username"/>--%>
                                            <form:input id="username" path="username" cssClass="input-sm" placeholder="Username"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-2"></div>
                                <div class="col-xs-5">
                                    <section>
                                        <label class="label">User Role <samp style="color: red">*</samp></label>
                                        <label class="select">
                                            <form:select id="userrole" path="seluserrole" cssClass="input-sm" items="${userrolelist}"/>
                                            <i></i>
                                        </label>
                                    </section>
                                </div>
                            </div>            
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <!--<button id="save" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</button>-->
                                <button id="save" type="button" class="btn btn-primary btn-spinner">Save</button>
                                <a id="back_btn" class="btn btn-primary" href="${pageContext.servletContext.contextPath}/user">Back to Search</a>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </form:form>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#products").select2();
//                $("#territories").select2();
                $("#productscategorytype").select2();
                $("#supervisors").select2();

                $('#hierarchyid').change(function () {
//                    loadTeritories();
                    loadSupervisor();
                });

                $('#products').change(function () {
//                    loadTeritories();
                    loadProductCategoryTypes();
                });

                $('#nameinfull').keyup(function () {
                    var fullname = $('#nameinfull').val().split(' ');
                    $('#nameinfull').val($('#nameinfull').val().toUpperCase());
                    $('#initials').val('');
                    $('#surname').val('');
                    if (fullname.length > 1) {
                        var initials = "";
                        for (var i = 0; i < fullname.length - 1; i++) {
                            initials += fullname[i].substring(0, 1) + ".";
                        }
                        $('#initials').val(initials.toUpperCase());
                        $('#surname').val(fullname[fullname.length - 1].toUpperCase());
                    }
                });

                $('#preferredname').keyup(function () {
                    $('#preferredname').val($('#preferredname').val().toUpperCase());
                });

//                $('#employeeid').keyup(function () {
//                    $('#employeeid').val($('#employeeid').val().toLowerCase());
//                });
                $('#username').keyup(function () {
                    $('#username').val($('#username').val().toLowerCase());
                });

                jQuery.validator.addMethod("nic", function (value) {
                    if (value.length === 10) {
                        return (/^[0-9]{9}[V]$/.test(value) || /^[0-9]{9}[v]$/.test(value) || /^[0-9]{9}[X]$/.test(value) || /^[0-9]{9}[x]$/.test(value) || '' === value);
                    } else {
                        return (/^[0-9]{12}$/.test(value) || '' === value);
                    }
                }, jQuery.validator.format("Please input valid nic number"));

                jQuery.validator.addMethod("lettersonly", function (value, element) {
                    return this.optional(element) || /^[a-z\s]+$/i.test(value);
                }, "Only alphabetical characters");

                $('#userCreate').validate({
                    onfocusout: function (element) {
                        $(element).valid();
                    },
                    rules: {
                        nameinfull: {
                            required: true,
                            maxlength: 64
//                            lettersonly: true
                        }, initials: {
                            required: true,
                            maxlength: 16
                        }, preferredname: {
                            required: true,
                            maxlength: 32
//                            ,lettersonly: true
                        }, surname: {
                            required: true,
                            maxlength: 16
//                            ,lettersonly: true
                        },
//                        epf: {
//                            remote: "${pageContext.servletContext.contextPath}/user/view/isvalideepf"
//                        }, 
                        email: {
                            required: true,
                            email: true,
                            maxlength: 64,
                            remote: "${pageContext.servletContext.contextPath}/user/view/isvalidemail"
                        }, nic: {
                            nic: true,
                            minlength: 10,
                            maxlength: 12,
                            remote: "${pageContext.servletContext.contextPath}/user/view/isvalidnic"
                        }, contactno01: {
//                            required: true,
                            number: true,
                            minlength: 10,
                            maxlength: 10
                        }, contactno02: {
                            number: true,
                            minlength: 10,
                            maxlength: 10
                        }, selhierarchyid: {
                            required: true
                        }, selstatus: {
                            required: true
                        },
//                                selterritories: {
//                            required: true
//                        }, 
                        username: {
                            required: true,
                            maxlength: 32,
                            remote: "${pageContext.servletContext.contextPath}/user/view/isvaliduserid"
                        },
//                        employeeid: {
//                            required: true,
//                            remote: "${pageContext.servletContext.contextPath}/user/view/isvaliduserid"
//                        },
                        seluserrole: {
                            required: true
                        }
                    }, messages: {
                        nic: {
                            remote: "Nic duplicate"
                        }, email: {
                            remote: "Email duplicate"
                        }, username: {
                            remote: "Username duplicate"
                        }
//                        employeeid: {
//                            remote: "Username duplicate"
//                        }
                    }, invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });

                $('#save').click(function () {
                    $('#msg_dev').empty();
                    if ($('#userCreate').valid()) {
                        var dataObject = new Object();
                        dataObject.nameinfull = $('#nameinfull').val();
                        dataObject.initials = $('#initials').val();
                        dataObject.preferredname = $('#preferredname').val();
                        dataObject.surname = $('#surname').val();
//                        dataObject.epf = $('#epf').val();
                        dataObject.nic = $('#nic').val();
                        dataObject.email = $('#email').val();
                        dataObject.contactno01 = $('#contactno01').val();
                        dataObject.contactno02 = $('#contactno02').val();
                        dataObject.hierarchyid = $('#hierarchyid').val();
                        dataObject.status = $('#status').val();
                        dataObject.products = $('#products').val();
//                        dataObject.territories = $('#territories').val();
                        dataObject.productscategorytype = $('#productscategorytype').val();
                        dataObject.supervisors = $('#supervisors').val();
//                        dataObject.employeeid = $('#employeeid').val();
                        dataObject.username = $('#username').val();
                        dataObject.userrole = $('#userrole').val();
                        $.ajax({
                            type: "POST",
                            contentType: "application/json",
                            url: "${pageContext.servletContext.contextPath}/user/create?action=create",
                            data: JSON.stringify(dataObject),
                            dataType: 'json',
                            success: function (data) {
                                if (data.code === "SUCCESS") {
                                    $(':input', '#userCreate')
                                            .not(':button, :submit, :reset, :hidden')
                                            .val('')
                                            .removeAttr('checked')
                                            .removeAttr('selected');
//                                    $("#territories").empty().trigger('change');
                                    $("#productscategorytype").empty().trigger('change');
                                    $("#supervisors").empty().trigger('change');
                                    $("#products").trigger('change');
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + data.message + '</div> <br/>');
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + data.message + '</div> <br/>');
                                    $("#msg_dev").fadeIn().delay(3000).fadeOut('slow');
                                }
                                $('#msg_dev').focus();
                            },
                            error: function (e) {
                                console.log(e);
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#save').button('loading');
//                            },
//                            complete: function () {
//                                $('#save').button('reset');
//                            }
                        });
                    }
                });

//                if (${isclient}) {
//                    $('#userrole').val("10").attr("disabled", "disabled");
//                }
            });

//            function loadTeritories() {
//                var hierarchy = $('#hierarchyid').val();
//                var product = $('#products').val();
//                $('#territories').empty();
//                if (product !== null && product.length > 0 && hierarchy) {
//                    $.post("${pageContext.servletContext.contextPath}/user/getterritorieshp", {hierarichy: hierarchy, product: product}, "json")
//                            .done(function (data) {
//                                $.each(data, function (key, value) {
//                                    $('#territories').append($("<option></option>")
//                                            .attr("value", key)
//                                            .text(value));
//                                    $('#territories').trigger('change');
//                                });
//                            });
//                } else if (hierarchy) {
//                    $.post("${pageContext.servletContext.contextPath}/user/getterritoriesh", {hierarichy: hierarchy}, "json")
//                            .done(function (data) {
//                                $.each(data, function (key, value) {
//                                    $('#territories').append($("<option></option>")
//                                            .attr("value", key)
//                                            .text(value));
//                                    $('#territories').trigger('change');
//                                });
//                            });
//                }
//                $('#territories').trigger('change');
//            }

            function loadProductCategoryTypes() {
                var product = $('#products').val();
                $('#productscategorytype').empty();
                if (product !== null && product.length) {
                    $.post("${pageContext.servletContext.contextPath}/user/getproductcategorytypes", {product: product}, "json")
                            .done(function (data) {
                                $.each(data, function (key, value) {
                                    $('#productscategorytype').append($("<option></option>")
                                            .attr("value", key)
                                            .text(value));
                                    $('#productscategorytype').trigger('change');
                                });
                            });
                }
                $('#productscategorytype').trigger('change');
            }

            function loadSupervisor() {
                var hierarchy = $('#hierarchyid').val();
                $('#supervisors').empty();
                if (hierarchy) {
                    $.post("${pageContext.servletContext.contextPath}/user/getsupervisor", {hierarichy: hierarchy}, "json")
                            .done(function (data) {
                                $.each(data, function (key, value) {
                                    $('#supervisors').append($("<option></option>")
                                            .attr("value", key)
                                            .text(value));
                                    $('#supervisors').trigger('change');
                                });
                            });
                }
                $('#supervisors').trigger('change');
            }
        </script>
    </body>
</html>
