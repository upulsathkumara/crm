<%-- 
    Author     : Roshen Dilshan
    Document   : userview
    Created on : Sep 17, 2015, 3:28:38 PM
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        
        <style>
            .smart-form *, .smart-form:after, .smart-form:before {    
            box-sizing: border-box;
            }
            .smart-form select.input-sm{
                height: 31px;
            }
        </style>
    </head>
    <body class="">

        <!-- HEADER -->
        <header id="header">
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="${pageContext.servletContext.contextPath}/user">User Management</a></li><li>View User</li>
                </ol>

            </div>
            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i> 
                            View User 
                            <span>
                            </span>
                        </h1>
                    </div>
                </div>

                <form:form id="userView" novalidate="novalidate" class="smart-form" commandName="user" action="${pageContext.servletContext.contextPath}/user/create" autocomplete="off">
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">
                            <c:if test="${not empty successMsg}">
                                <div class="alert alert-success">
                                    <strong>Success!</strong> ${successMsg}
                                </div>
                            </c:if> 
                            <c:if test="${not empty errorMsg}">
                                <div class="alert alert-warning">
                                    <strong>Warning!</strong> ${errorMsg}
                                </div>
                                <br/>
                            </c:if> 
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row" style="padding-bottom: 30px;">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <header><h4>Employee Information</h4></header>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <section>
                                <label class="label">Name in Full </label>
                                <label class="input">
                                    <form:input id="nameinfull" path="nameinfull" cssClass="input-sm" placeholder="Name in Full" readonly="true"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-4">
                                <div class="col-xs-10">
                                    <section>
                                        <label class="label">Initials </label>
                                        <label class="input">
                                            <form:input id="initials" path="initials" cssClass="input-sm" placeholder="Initials" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <div class="col-xs-4">
                                <div class="col-xs-12">
                                    <section>
                                        <label class="label">Preferred Name </label>
                                        <label class="input">
                                            <form:input id="preferredname" path="preferredname" cssClass="input-sm" placeholder="Preferred Name" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-10">
                                    <section>
                                        <label class="label">Surname </label>
                                        <label class="input">
                                            <form:input id="surname" path="surname" cssClass="input-sm" placeholder="Surname" readonly="true"/>
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
<!--                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">EPF </label>
                                    <label class="input">
                                        <%--<form:input id="epf" path="epf" cssClass="input-sm" placeholder="EPF" maxlength="8" readonly="true"/>--%>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>-->
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">NIC </label>
                                    <label class="input">
                                        <form:input id="nic" path="nic" cssClass="input-sm" placeholder="NIC" readonly="true"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">E-mail </label>
                                    <label class="input">
                                        <form:input id="email" path="email" cssClass="input-sm" placeholder="E-mail" readonly="true"/>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Contact # 01 </label>
                                    <label class="input">
                                        <form:input id="contactno01" path="contactno01" cssClass="input-sm" placeholder="Contact # 02" readonly="true"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Contact # 02 </label>
                                    <label class="input">
                                        <form:input id="contactno02" path="contactno02" cssClass="input-sm" placeholder="Contact # 02" readonly="true"/>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Hierarchy </label>
                                    <label class="select">
                                        <form:select id="hierarchyid" path="selhierarchyid" cssClass="input-sm disabled" items="${hierarchylist}" readonly="true" disabled="true"/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Status </label>
                                    <label class="select">
                                        <form:select id="status" path="selstatus" cssClass="input-sm disabled" items="${statuslist}" readonly="true" disabled="true"/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Product </label>
                                    <label class="select">
                                        <form:select id="products" path="selproducts" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4" items="${productlist}"/>
                                    </label>
                                </section>                                
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Product Subcategory </label>
                                    <label class="select">
                                        <form:select id="productscategorytype" path="selproductscategorytype" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4" items="${productcategorytypelist}"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
<!--                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Territory </label>
                                    <label class="select">
                                        <%--<form:select id="territories" path="selterritories" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4" items="${territorylist}"/>--%>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>-->
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Supervisor </label>
                                    <label class="select">
                                        <form:select id="supervisors" path="selsupervisors" cssStyle="width:100%" cssClass="select2-container select2-container-multi select2" multiple="true" tabindex="4" items="${supervisorlist}"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row" style="padding-bottom: 30px;">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <header><h4>User Account</h4></header>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">Username </label>
                                    <label class="input">
                                        <%--<form:input id="userid" path="userid" cssClass="input-sm" placeholder="Username" readonly="true"/>--%>
                                        <form:input id="username" path="username" cssClass="input-sm" placeholder="Username" readonly="true"/>
                                    </label>
                                </section>
                            </div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-5">
                                <section>
                                    <label class="label">User Role </label>
                                    <label class="select">
                                        <form:select id="userrole" path="seluserrole" cssClass="input-sm disabled" items="${userrolelist}" disabled="true"/>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <a id="back_btn" class="btn btn-primary" href="${pageContext.servletContext.contextPath}/user">Back to Search</a>
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </form:form>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#products").select2();
                $("#territories").select2();
                $("#productscategorytype").select2();
                $("#supervisors").select2();
                
                $('#products option').prop('selected', true);
                $('#territories option').prop('selected', true);
                $('#productscategorytype option').prop('selected', true);
                $('#supervisors option').prop('selected', true);
                
                $("#products").attr('disabled', true);
                $("#territories").attr('disabled', true);
                $("#productscategorytype").attr('disabled', true);
                $("#supervisors").attr('disabled', true);
                
                $('#products').trigger("change");
                $('#territories').trigger("change");
                $('#productscategorytype').trigger("change");
                $('#supervisors').trigger("change");
            });
        </script>
    </body>
</html>
