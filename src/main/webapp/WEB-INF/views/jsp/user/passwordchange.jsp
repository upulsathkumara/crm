<%-- 
    Document   : passwordchange
    Created on : 02/11/2017, 11:03:18 PM
    Author     : stephen.silver
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>


    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 id="add_update_view_title" class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Password Change
                        </h1>
                        <span></span>
                    </div>
                </div>


                <form id="userPasswordChange" novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div id="msg_dev" class="col-xs-8">
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <section>
                                <label class="label">Current Password<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="c_password" name="c_password" type="password" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <section>
                                <label class="label">New Password<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="new_password" name="new_password" type="password" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <section>
                                <label class="label">Retype New Password<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="re_password" name="re_password" type="password" class="input-sm" />
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <footer style="background-color: #ffffff">
                                <!--<button id="btnChangePassword" type="button" class="btn btn-primary btn-spinner" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing">Change Password</button>-->
                                <button id="btnChangePassword" type="button" class="btn btn-primary btn-spinner">Change Password</button>
                                <button id="clear" onclick="clearForm()" type="button" class="btn btn-default">Clear</button>
                            </footer>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>
                </form>

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">


            $(document).ready(function () {
                jQuery.validator.addMethod("notEqual", function (value, element, param) {
                    return this.optional(element) || value != $(param).val();
                }, "Old password and new password cannot be same.");


                $('#userPasswordChange').validate({
                    onfocusout: function (element) {
                        $(element).valid();
                    },
                    rules: {
                        c_password: {
                            required: true
                        },
                        new_password: {
                            required: true,
                            notEqual: "#c_password"
                        },
                        re_password: {
                            required: true,
                            equalTo: "#new_password",
                            notEqual: "#c_password"
                        }
                    },
                    invalidHandler: function (form, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            validator.errorList[0].element.focus();
                        }
                    }
                });




                $('#btnChangePassword').click(function () {
                    if ($('#userPasswordChange').valid()) {
                        var content = new Object();
                        content.cpassword = $('#c_password').val();
                        content.newpassword = $('#new_password').val();
                        console.log("Content", content);

                        $.ajax({
                            method: "POST",
                            cache: false,
                            url: "${pageContext.servletContext.contextPath}/user/passwordchange?action=create",
                            data: content,
                            success: function (response) {
//                            console.log(response);
                                response = JSON.parse(response);
                                if (response.CODE === "SUCCESS") {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                    clearForm();
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                window.scrollTo(0, 0);
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#btnChangePassword').button('loading');
//                            },
//                            complete: function () {
//                                $('#btnChangePassword').button('reset');
//                            }
                        });
                    }
                });



            });

            function clearForm() {
                $('#c_password').val('');
                $('#new_password').val('');
                $('#re_password').val('');


                $("#userPasswordChange").validate().resetForm();
                $("#userPasswordChange label").each(function () {
                    $(this).removeClass('state-error');
                    $(this).removeClass('state-success');
                });
            }

        </script>
    </body>
</html>