<%-- 
    Author     : Roshen Dilshan
    Document   : usersearch
    Created on : Sep 15, 2015, 10:40:50 AM
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <jsp:include page="../template/cssinclude.jsp"/> 
    </head>
    <body class="">

        <!-- HEADER -->
        <header id="header">
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="${pageContext.servletContext.contextPath}/user">User Management</a></li><li>Search User</li>
                </ol>

            </div>
            <!-- END RIBBON -->
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>

                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-12 col-lg-12"  style="height: 25px;">
                        <!--                        <ul id="sparks" class="">
                                                    <a href="${pageContext.servletContext.contextPath}/user/create/view" class="btn btn-primary">Add New User</a>
                                                </ul>-->
                    </div>
                </div>
                <form method="post" autocomplete="off">
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10"></div>
                        <div class="col-xs-1"></div>
                    </div>
                    <div class="row">
                        <!-- NEW COL START -->
                        <article class="col-sm-12 col-md-12 col-lg-6" style="margin-bottom:40px">
                            <div class="input-group input-group-sm">
                                <!--<div class="input-group-btn">
                                    <form :select id="search_type" path="search_type" items="${customerCodeTypeList}" class="btn btn-default dropdown-toggle" data-toggle="dropdown"/>
                                </div>-->
                                <input id="searchKeyword" class="form-control input-lg" type="text" placeholder="Search Username, Preferred Name, NIC, Contact # 01"/>
                                <div class="input-group-btn">
                                    <button type="button" id="search_btn" class="btn btn-default">
                                        &nbsp;&nbsp;&nbsp;<i class="fa fa-fw fa-search fa-lg"></i>&nbsp;&nbsp;&nbsp;
                                    </button>
                                </div>
                            </div>
                        </article>
                    </div>
                </form>

                <!-- widget grid -->
                <section id="widget-grid" class="case-datatable">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" role="widget">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>User search results</h2>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th data-hide="phone"><i class="fa fa-fw fa-user hidden-md hidden-sm hidden-xs"></i> Username</th>
                                                    <th data-class="expand"> <i class="fa fa-fw fa-users hidden-md hidden-sm hidden-xs">  </i>  User Role</th>
                                                    <th data-hide="phone,tablet"><i class="hidden-md hidden-sm hidden-xs"></i> Preferred Name</th>
                                                    <th data-hide="phone,tablet"><i class="hidden-md hidden-sm hidden-xs"></i> Surname</th>
                                                    <th data-hide="phone,tablet"> Status</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o hidden-md hidden-sm hidden-xs"></i> Created Time</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o hidden-md hidden-sm hidden-xs"></i> Last Updated Time</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o hidden-md hidden-sm hidden-xs"></i>Created User</th>
                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-clock-o hidden-md hidden-sm hidden-xs"></i> Last Updated User</th>

                                                    <th data-hide="phone,tablet"><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->											
                        </article>
                        <!-- WIDGET END -->
                    </div>								
                </section>
                <!-- end widget grid -->

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">

            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            var search = false;
            var search_table;

            $(document).ready(function () {
                var permissions = JSON.parse($('#permissions').val());

                pageSetUp();
                /* // DOM Position key index //
                 
                 l - Length changing (dropdown)
                 f - Filtering input (search)
                 t - The Table! (datatable)
                 i - Information (records)
                 p - Pagination (paging)
                 r - pRocessing 
                 < and > - div elements
                 <"#id" and > - div with an id
                 <"class" and > - div with a class
                 <"#id.class" and > - div with an id and class
                 
                 Also see: http://legacy.datatables.net/usage/features
                 */

                /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                search_table = $('#dt_basic').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4 col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        if(!permissions.update){
                            $('.fa-pencil').parent().parent().remove();
                        }
                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/user/search/tabledata",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "search", "value": search});
                        aoData.push({"name": "searchKeyword", "value": $('#searchKeyword').val()});
                        $.ajax({
                            "dataType": 'json',
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "aoColumns": [
                        {"mDataProp": "username", "bSortable": false},
                        {"mDataProp": "user_role", "bSortable": false},
                        {"mDataProp": "preferred_name", "bSortable": false},
                        {"mDataProp": "surname", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "created_time", "bSortable": false},
                        {"mDataProp": "lastupdated_time", "bSortable": false},
                        {"mDataProp": "created_user", "bSortable": false},
                        {"mDataProp": "lastupdated_user", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}
                    ]
                });

                $('#search_btn').click(function () {
                    $('#msg_dev').empty();
                    search = true;
                    search_table.fnDraw();
                });
            });

            $(document).ajaxSuccess(function () {
                var rowCount = 0;
                $('#dt_basic >tbody >tr').each(function () {
                    if (!$(this).hasClass('odd')) {
                        rowCount++;
                    }
                });
            });

            function clearTable() {
                $('#msg_dev').empty();
                search = false;
                $('#dt_basic').dataTable().fnClearTable();
            }

            function resetAttempts(username) {
                $('#msg_dev').empty();
                $.SmartMessageBox({
                    title: "Alert!",
                    content: "<i class='fa fa-undo fa-3x'></i> Are you sure you want to Re-Set invalid login attempts for user : " + username,
                    buttons: '[No][Yes]'
                }, function (ButtonPressed) {
                    if (ButtonPressed === "Yes") {
                        $.ajax({
                            async: true,
                            type: "post",
                            url: "${pageContext.servletContext.contextPath}/user/userreset",
                            cache: false,
                            data: {username: username},
                            success: function (response) {
                                response = JSON.parse(response);
                                if (response.CODE === "SUCCESS") {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    search = true;
                                    search_table.fnDraw();
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            }
                        });
                    }
                });
            }

            function resetPassword(username) {
                $('#msg_dev').empty();
                $.SmartMessageBox({
                    title: "Alert!",
                    content: "<i class='fa fa-key fa-3x'></i> Are you sure you want to Re-Set password for user : " + username,
                    buttons: '[No][Yes]'
                }, function (ButtonPressed) {
                    if (ButtonPressed === "Yes") {
                        $.ajax({
                            async: true,
                            type: "post",
                            url: "${pageContext.servletContext.contextPath}/user/userresetpassword",
                            cache: false,
                            data: {username: username},
                            success: function (response) {
                                response = JSON.parse(response);
                                if (response.code === "SUCCESS") {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.message + '</div> <br/>');
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            }
                        });
                    }
                });
            }

            
        </script>
    </body>
</html>
