
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
    <head>
        <meta charset="utf-8">
        <title>AFFINITI</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
        <link href="${bootstrapCss}" rel="stylesheet" />

        <spring:url value="/resources/core/css/demo.min.css" var="demo" />
        <link href="${demo}" rel="stylesheet" />

        <spring:url value="/resources/core/css/font-awesome.min.css" var="font" />
        <link href="${font}" rel="stylesheet" />

        <spring:url value="/resources/core/css/invoice.min.css" var="invoice" />
        <link href="${invoice}" rel="stylesheet" />

        <spring:url value="/resources/core/css/lockscreen.min.css" var="lockscreen" />
        <link href="${lockscreen}" rel="stylesheet" />

        <spring:url value="/resources/core/css/smartadmin-production.min.css" var="production" />
        <link href="${production}" rel="stylesheet" />

        <spring:url value="/resources/core/css/smartadmin-rtl.min.css" var="rtl" />
        <link href="${rtl}" rel="stylesheet" />

        <spring:url value="/resources/core/css/smartadmin-skins.min.css" var="skins" />
        <link href="${skins}" rel="stylesheet" />

        <spring:url value="/resources/core/css/your_style.css" var="your_style" />
        <link href="${your_style}" rel="stylesheet" />

        <spring:url value="/resources/core/css/style.css" var="style" />
        <link href="${style}" rel="stylesheet" />

        <spring:url value="resources/img/favicon.ico" var="icon" />
        <link href="${icon}" rel="stylesheet" />

        <link rel="icon" href="${icon}" type="image/x-icon"/>
        <link rel="shortcut icon" href="${icon}" type="image/x-icon"/>

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

    </head>

    <body>


        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <div id="content" class="container">
                <div class="row">
                    <div class="col-xs-2"></div>
                    <div id="msg_dev" class="col-xs-8">
                    </div>
                    <div class="col-xs-2"></div>
                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">


                        <c:choose>
                            <c:when test="${tokenExpired}">
                                <h3 class="text-center">Registration Link Expired <a href="#" id="resendLink">Request another link</a></h3>
                            </c:when>
                            <c:otherwise>
                                <div class="well no-padding">
                                    <form id="registration-form" novalidate="novalidate" class="smart-form client-form">                              
                                        <header>
                                            Complete Registration
                                        </header>

                                        <fieldset>
                                            <input id="token" type="text" value="${userToken}" hidden/>
                                            <section>
                                                <label class="label">Username</label>
                                                <label class="input"> 
                                                    <i class="icon-append fa fa-user"></i>
                                                    <input id="username" name="username" class="myWish" placeholder="username" disabled="true" value="${username}"/>
                                                    <b class="tooltip tooltip-top-right">
                                                        <i class="fa fa-user txt-color-teal"></i> Please enter username
                                                    </b>
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Password</label>
                                                <label class="input"> 
                                                    <i class="icon-append fa fa-lock"></i>
                                                    <input type="password" id="password" name="password" class="myWish" placeholder="password"/>
                                                    <b class="tooltip tooltip-top-right">
                                                        <i class="fa fa-lock txt-color-teal"></i> Enter your password
                                                    </b> 
                                                </label>
                                            </section>
                                            <section>
                                                <label class="label">Retype Password</label>
                                                <label class="input"> 
                                                    <i class="icon-append fa fa-lock"></i>
                                                    <input type="password" id="repassword" name="repassword" class="myWish" placeholder="retype password"/>
                                                    <b class="tooltip tooltip-top-right">
                                                        <i class="fa fa-lock txt-color-teal"></i> Retype password
                                                    </b> 
                                                </label>
                                            </section>
                                        </fieldset>
                                        <footer>                                   
                                            <!--<button id="registerBtn" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing">Complete Registration</button>-->
                                            <button id="registerBtn" type="button" class="btn btn-primary btn-spinner">Complete Registration</button>

                                        </footer>
                                    </form>
                                </div>
                            </c:otherwise>
                        </c:choose>



                        <p class="pull-right">${version}</p>
                    </div>
                </div>

            </div>
        </div>

        <spring:url value="/resources/core/js/jquery-2.0.2.min.js" var="jquery2" />
        <script src="${jquery2}"></script>

        <spring:url value="/resources/core/js/jquery-ui-1.10.3.min.js" var="jqueryui" />
        <script src="${jqueryui}"></script>

        <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrap" />
        <script src="${bootstrap}"></script>

        <spring:url value="/resources/core/js/jquery.validate.min.js" var="validate" />
        <script src="${validate}"></script>

        <spring:url value="/resources/core/js/jquery.maskedinput.min.js" var="masked" />
        <script src="${masked}"></script>


        <script type="text/javascript">

            $(document).ready(function () {
//                $('#password, #repassword').on('keyup', function () {
//                    var password = $('#password').val();
//                    var repassword = $('#repassword').val();
//
//                    if (password != repassword) {
//                        if(repassword != ""){
//                            alert("Mismatch");
//                        }
//                    }else{
//                        alert("Match");
//                    }
//                });

                $('#registration-form').validate({
                    rules: {
                        username: {
                            required: true
                        },
                        password: {
                            required: true
                        },
                        repassword: {
                            required: true,
                            equalTo: "#password"
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }
                });


//                jQuery.validator.addMethod("passwordvalidate", function () {
//                    var password = $('#password').val();
//                    var repassword = $('#repassword').val();
//
//                    if (password.length < 1 || repassword.length < 1) {
//                        return false;
//                    }
//                    if (password != repassword) {
//                        if (repassword != "") {
//                            return false;
//                        }
//                        return true;
//                    } else {
//                        return true;
//                    }
//                }, jQuery.validator.format("Password doesn't match."));


                $('#registerBtn').click(function (e) {
                    if ($('#registration-form').valid()) {

                        var object = new Object();
                        object.username = $('#username').val();
                        object.token = $('#token').val();
                        object.password = $('#password').val();

                        var content = JSON.stringify(object);

                        $.ajax({
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/reset/completeregistration",
                            cache: false,
                            data: content,
                            dataType: 'json',
                            success: function (response) {
                                //response = JSON.parse(response);
                                console.log(response);
                                if (response.CODE === "SUCCESS") {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + ' <br>You will be redirected to login page withing few seconds.</div> <br/>');
                                    setTimeout(function () {
                                        window.location.href = "${pageContext.servletContext.contextPath}";
                                    }, 3000);
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                window.scrollTo(0, 0);
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#registerBtn').button('loading');
//                            },
//                            complete: function () {
//                                $('#registerBtn').button('reset');
//                            }
                        });
                    }
                });


                $('#resendLink').click(function () {
                    var username = getURLParam('username');
                    var token = getURLParam('token');

                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/reset/resendlink?username=" + username + "&token=" + token,
                        cache: false,
                        dataType: 'json',
                        success: function (response) {
                            //response = JSON.parse(response);
                            console.log(response);
                            if (response.CODE === "SUCCESS") {
                                $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + ' </div> <br/>');
                            } else {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                window.scrollTo(0, 0);
                            }
                        },
                        error: function () {
                            $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                            window.scrollTo(0, 0);
                        }
                    });
                });

            });

            function getURLParam(name) {
                return (location.search.split(name + '=')[1] || '').split('&')[0];
            }
        </script>

        <spring:url value="/resources/core/js/app.min.js" var="app" />
        <script src="${app}"></script>

    </body>
</html>
