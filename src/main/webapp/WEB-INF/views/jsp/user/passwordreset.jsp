<%-- 
    Document   : passwordreset
    Created on : 02/11/2017, 11:03:18 PM
    Author     : stephen.silver
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>


    </head>
    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 id="add_update_view_title" class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Password Reset
                        </h1>
                        <span></span>
                    </div>
                </div>


                <!--               <form id="resetForm" novalidate="novalidate" class="smart-form client-form">                              
                                                <header>
                                                    Password Reset
                                                </header>
                
                                                <fieldset>
                                                    <section>
                                                        <label class="label">Username</label>
                                                        <label class="input"> 
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input id="username" name="username" class="myWish" placeholder="username"/>
                                                            <b class="tooltip tooltip-top-right">
                                                                <i class="fa fa-user txt-color-teal"></i> Please enter username
                                                            </b>
                                                        </label>
                                                    </section>
                
                                                </fieldset>
                                                <footer>                                   
                                                    <button id="reset" type="button" class="btn btn-primary">Reset Password</button>
                                                </footer>
                                            </form>-->





                <form id="resetForm" novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div id="msg_dev" class="col-xs-8">
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <section>
                                <label class="label">Username<samp style="color: red">*</samp></label>
                                <label class="input">
                                    <input id="username" name="username" type="text" class="input-sm" placeholder="Username"/>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>


                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <footer style="background-color: #ffffff">
                                <!--<button id="reset" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing">Reset Password</button>-->
                                <button id="reset" type="button" class="btn btn-primary btn-spinner">Reset Password</button>
                            </footer>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>
                </form>

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <jsp:include page="../template/footer.jsp"/>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- js file include -->
        <jsp:include page="../template/jsinclide.jsp"/>

        <script type="text/javascript">

            $(document).ready(function () {

                $('#resetForm').validate({
                    rules: {
                        username: {
                            required: true,
                            maxlength: 32
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }
                });



                $('#reset').click(function (e) {
                    if ($('#resetForm').valid()) {

                        var content = new Object();
                        content.username = $('#username').val();
                        console.log("Content", content);

                        $.ajax({
                            method: "POST",
                            cache: false,
                            url: "${pageContext.servletContext.contextPath}/user/passwordreset?action=create",
                            data: content,
                            success: function (response) {
//        console.log(response);                        
                                response = JSON.parse(response);
                                console.log(response);
                                if (response.CODE === "SUCCESS") {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    $('#username').val('');
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.MESSAGE + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                            },
                            error: function () {
                                $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> Communication Error Please Re-Try!</div> <br/>');
                                window.scrollTo(0, 0);
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#reset').button('loading');
//                            },
//                            complete: function () {
//                                $('#reset').button('reset');
//                            }
                        });
                    }
                });
            });
        </script>

    </body>
</html>