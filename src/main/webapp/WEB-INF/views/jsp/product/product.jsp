 <%-- 
    Document   : product
    Created on : Sep 4, 2017, 8:07:38 AM
    Author     : acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <!-- css include -->
        <jsp:include page="../template/cssinclude.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>


    <!-- TITLE IS HERE PLZ REMOVE IT BUDDY-->
    <title>JSP Page</title>



    <body class="">
        <!-- HEADER -->
        <header id="header">			
            <jsp:include page="../template/header.jsp"/>
        </header>
        <!-- END HEADER -->

        <aside id="left-panel">
            <jsp:include page="../template/menu.jsp"/>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <jsp:include page="../template/ribbon.jsp"/>
            <!-- MAIN CONTENT -->
            <div id="content">
                <input id="permissions" type="hidden" value='<%= session.getAttribute("permissions")%>'>


                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-edit fa-fw "></i>
                            Product Add
                        </h1>
                        <span></span>
                    </div>
                </div>

                <form:form  commandName="productAddForm" novalidate="novalidate" class="smart-form">
                    <input id="multisectionarray" name="multisectionarray" type="hidden" value="" />
                    <input id="selectfunctionid" name="selectfunction" type="hidden" value="save"/>
                    <input id="saveaction" name="saveaction" type="text" value="create" hidden/>


                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div id="msg_dev" class="col-xs-10">

                        </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <input id="id" type="text" value="" hidden />
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Product Category<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="productid" path="productid" cssClass="input-sm" items="${productList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Product Name<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <form:input id="description" path="description" placeholder="Product Name" type="description" value=""/>
                                    <i></i>
                                </label>

                            </section>
                        </div>
                        <div class="col-xs-2"></div>


                    </div>
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Product Status<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select path="status" cssClass="input-sm" items="${productStatusList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
<!--                        <div class="col-xs-3">
                            <section>
                                <label class="label">Sort ID<samp style="color: red">*</samp></label>

                                <label class="input">
                                    <%--<form:input id="sortid" path="sortid" placeholder="SortID" type="sortid" value=""/>--%>
                                    <i></i>
                                </label>

                            </section>
                        </div>-->
                        <div class="col-xs-2"></div>

                    </div> 
                    <div class="row">

                    </div>

                    <div class="col-xs-2"></div>
                    <div class="col-xs-3">

                    </div>
                    <div class="col-xs-2"></div>

                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-10">
                            <!--                            <h1 id="add_update_view_title" class="page-title txt-color-blueDark">-->
                            <footer style="background-color: #ffffff">
                                <%--<form:button id="btnAddStatusSection" type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving">Save</form:button>--%>
                                <form:button id="btnAddStatusSection" type="button" class="btn btn-primary btn-spinner">Save</form:button>

                                    <button type="button" id="productClear" class="btn">Clear</button>  
                                </footer>
                                <!--</h1>-->
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                </form:form>
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa fa-search fa-fw"></i>
                            Product Search   
                        </h1>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div id="msg_dev" class="col-xs-10" tabindex="0"></div>
                    <div class="col-xs-1"></div>
                </div>
                <form:form commandName="productSearchForm" novalidate="novalidate" class="smart-form">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3">
                            <section>
                                <label class="label">Product Category<samp style="color: red">*</samp></label>
                                <label class="select">
                                    <form:select id="search_productid" path="productid" cssClass="input-sm" items="${productList}"/>
                                    <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <footer style="background-color: #ffffff">
                                <button id="search_btn" type="button" class="btn btn-primary" value="Submit">Search</button>  
                            </footer>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>


                </form:form>

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                                    <h2>Product Details </h2>

                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="productSections" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>			                
                                                <tr>
                                                    <th>Product ID</th>
                                                    <th>Description</th>
                                                    <th>Product Category</th>
                                                    <th>Status</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Last Updated Time</th>
                                                    <th><i class="fa fa-fw fa-clock-o txt-color-blue hidden-md hidden-sm hidden-xs"></i> Created Time</th>
                                                    <th>Created User</th>
                                                    <th><i class="fa fa-fw fa-hand-o-up hidden-md hidden-sm hidden-xs"></i> Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>   
                                    </div>
                                    <!-- end widget content --> 
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </section>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN PANEL -->

            <!-- PAGE FOOTER -->
            <div class="page-footer">
                <jsp:include page="../template/footer.jsp"/>
            </div>
            <!-- END PAGE FOOTER -->

            <!-- js file include -->
            <jsp:include page="../template/jsinclide.jsp"/>
            <style>
                .btn-table-action{
                    color: #3276b1;
                    cursor: pointer
                }
            </style>
        </div>
        <script type='text/javascript'>

            $(document).ready(function () {

                var permissions = JSON.parse($('#permissions').val());
                refreshPermissions(permissions);





                var SECTION_RECORD_ID = null;



                $('#search_btn').click(function () {
                    search_table.fnDraw();
                    clearValidations();
                });

                $("#productClear").click(function () {
                    clearSectionForm();//Clear Section Form
                    EnableForm();//Enable Input Fields
                    $("#productid").prop('disabled', false);
                    $("#btnAddStatusSection").fadeIn();
                    $("#msg_dev").fadeOut("slow");
                });


                /***************************** Load Table **********************/
                var responsiveHelper_dt_basic = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                var search_table = $('#productSections').dataTable({
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "sDom": "<'dt-toolbar'<'col-sm-4col-xs-6 hidden-xs'i><'col-xs-12 col-sm-6'r>>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-4 col-xs-6 hidden-xs'l><'col-xs-12 col-sm-8'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#productSections'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();

                        if (!permissions.update) {
                            $('.fa-pencil').parent().parent().remove();
                        }

                    },
                    "sAjaxSource": "${pageContext.servletContext.contextPath}/product/getproduct",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData.push({"name": "searchoptionID", "value": $.trim($('#search_productid').val())});
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": "${pageContext.servletContext.contextPath}/product/getproduct",
                            "data": aoData,
                            "success": fnCallback
                        });
                    },
                    "order": [[0, "asc"]],
                    "aoColumns": [
                        {"mDataProp": "productid", "bSortable": false},
                        {"mDataProp": "productname", "bSortable": false},
                        {"mDataProp": "productcategory", "bSortable": false},
                        {"mDataProp": "status", "bSortable": false},
                        {"mDataProp": "lastupdatedtime", "bSortable": false},
                        {"mDataProp": "createdtime", "bSortable": false},
                        {"mDataProp": "createduser", "bSortable": false},
                        {"mDataProp": "action", "bSortable": false}

                    ]
                });
                /************************* END Table Load *******************/


                /********************** Save Product ************************/
                $('#btnAddStatusSection').click(function () {
                    $("#productAddForm").valid();
                    $('#msg_dev').empty();

                    var status = {"statusid": $('#status').val()};
                    var productcategory = {"productcategoryid": $('#productid').val()};
                    var obj = {
                        "productcategory": productcategory,
                        "description": $('#description').val(),
                        "sortid": $('#sortid').val(),
                        "status": status
                    };

                    var selectedFunction = $('#saveaction').val();

                    if ($('#id').val() != '') {
                        console.log("hidden product id: ", $('#id').val());
                        obj.productid = $('#id').val();
                    }

                    console.log("request: ", obj);
                    if ($("#productAddForm").valid()) {

                        $.ajax({
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            url: "${pageContext.servletContext.contextPath}/product/addorupdateproduct?action=" + selectedFunction,
                            type: 'POST',
                            dataType: 'json',
                            data: JSON.stringify(obj),
                            success: function (response) {
                                if (response) {
                                    $('#msg_dev').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.message + '</div> <br/>');
                                    clearSectionForm();
                                } else {
                                    $('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong> ' + response.message + '</div> <br/>');
                                    window.scrollTo(0, 0);
                                }
                                $("#msg_dev").fadeIn().delay(2000).fadeOut('slow');
                                enableEditFields(true);
                                resetDropdowns();
                                $('#productid').removeAttr('disabled');
                                $('#status').removeAttr('disabled');
                                search_table.fnDraw();
                            }
//                            ,
//                            beforeSend: function () {
//                                $('#btnAddStatusSection').button('loading');
//                            },
//                            complete: function () {
//                                $('#btnAddStatusSection').button('reset');
//                            }
                        });
                    } else {
                        //$('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong>' +errorMessage + '</div><br/>');
                        //$('#msg_dev').html('<div class="alert alert-warning"><strong>Warning!</strong></div><br/>');
                    }
                });
                /*********************** END Save Product **************************/




                /************************ Edit And View Records ************************/
                $('#productSections').on('click', 'tr a', function (e) {
                    var id = $(this).attr('id');
                    var value = $(this).attr('value');

                    $.ajax({
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/product/getuproductt",
                        data: {productid: id},
                        async: true,
                        cache: false,
                        success: function (response, textStatus, jqXHR) {
                            response = JSON.parse(response);

                            console.log(response);

                            SECTION_RECORD_ID = response.data.id;
                            if (value === "view") {
                                loadDataToField(response);
                                disableForm();
                                $("#btnAddStatusSection").fadeOut("slow");
                                clearValidations();
                            } else if (value === "edit") {

                                $("#saveaction").val('UPDATE');
                                $('#id').val(id);
                                loadDataToField(response);
                                EnableForm();
                                $("#btnAddStatusSection").fadeIn();
                                clearValidations();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("In Error" + jqXHR);
                        }
                    });

                    /********** Load Data To Input Fields ***********/
                    function loadDataToField(response) {//|| 'defult'

                        //$('#id').find('option[value="' + response.data.id + '"]').prop('selected', true);
                        $('#productid').val(response.data.productcategory);
                        $('#description').val(response.data.description);
                        $("#sortid").val(response.data.sortid);
                        $('#status').val(response.data.statusid);
                    }
                });
                /********* End Load Data To Input Fields **********/

                /************** Disable Section Form *************/
                function disableForm() {
                    $("#productid").prop('disabled', true);
                    $("#description").prop('disabled', true);
                    $("#sortid").prop('disabled', true);
                    $("#status").prop('disabled', true);
                }
                /************* END Disable Section Form ***********/

                /************* Enable Section Form ***************/
                function EnableForm() {
                    $("#productid").prop('disabled', true);
                    $("#description").prop('disabled', false);
                    $("#sortid").prop('disabled', false);
                    $("#status").prop('disabled', false);
                }
                /************ END Enable Section Form ************/

                /*************** Clear Section Form **************/
                function clearSectionForm() {
                    $("#productAddForm").validate().resetForm();
                    $("#productAddForm label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass('state-success');

                    });

                    SECTION_RECORD_ID = null;
                    $("#productid").val("");
                    $("#description").val("");
                    $("#status").val("");
                    $("#sortid").val("");



                }
                /************** END Clear Section Form ************/

                function clearValidations() {

                    $("#productAddForm").validate().resetForm();
                    $("#productAddForm label").each(function () {
                        $(this).removeClass('state-error');
                        $(this).removeClass('state-success');
                    });
                }






                /**************** Form Validation *****************/
                $('#productAddForm').validate({
                    rules: {
                        productid: {
                            required: true
                        },
                        description: {
                            required: true,
                            maxlength: 64
                        },
                        sortid: {
                            //required: true,
                            number: true,
                            maxlength: 11

                        },
                        status: {
                            required: true,
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }
                });
                /**************** END Form Validation *****************/


                /**************** Search Validation *****************/
                $('#productSearchForm').validate({
                    rules: {
                        productid: {
                            required: true
                        }
                    },
                    errorPlacement: function (error, element) {
                        error.insertAfter(element.parent());
                    }
                });
                /**************** END Search Validation *****************/


                /**************** Enable Text Fields *****************/
                function enableEditFields(status) {
                    if (status) {
                        //enable
                        $('#userroleid').removeAttr('disabled');
                    } else {
                        // disable
                        $('#productid').attr('disabled', 'disabled');
                    }
                }
                /**************** END Enable Text Fields *****************/


                /**************** Reset Dropdown *****************/
                function resetDropdowns() {
                    $('#id').val('');
                    $('#productid').val('');
                    $('#description').val('');
                    $('#status').val('');
                    $('#sortid').val('');
                }
                /**************** END Reset Dropdown *****************/

                function refreshPermissions(permissions) {
                    if (!permissions.search) {
                        $('#search_btn').attr('disabled', 'disabled');
                        $('#search_productid').attr('disabled', 'disabled');

                    }
                    if (!permissions.create) {
                        disableForm();
                        $('#btnAddStatusSection').attr('disabled', 'disabled');
                        $('#productClear').attr('disabled', 'disabled');
                        ;
                    }
                }





            });
        </script>



    </body>
</html>
