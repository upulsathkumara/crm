package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Clientclientcategory generated by hbm2java
 */
@Entity
@Table(name="clientclientcategory"

)
public class Clientclientcategory  implements java.io.Serializable {


     private ClientclientcategoryId id;
     private Client client;
     private Clientcategory clientcategory;
     private Date lastupdateddatetime;
     private Date createdatetime;
     private String createduser;

    public Clientclientcategory() {
    }

	
    public Clientclientcategory(ClientclientcategoryId id, Client client, Clientcategory clientcategory, Date lastupdateddatetime, Date createdatetime) {
        this.id = id;
        this.client = client;
        this.clientcategory = clientcategory;
        this.lastupdateddatetime = lastupdateddatetime;
        this.createdatetime = createdatetime;
    }
    public Clientclientcategory(ClientclientcategoryId id, Client client, Clientcategory clientcategory, Date lastupdateddatetime, Date createdatetime, String createduser) {
       this.id = id;
       this.client = client;
       this.clientcategory = clientcategory;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createdatetime = createdatetime;
       this.createduser = createduser;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="client", column=@Column(name="CLIENT", nullable=false) ), 
        @AttributeOverride(name="clientcategory", column=@Column(name="CLIENTCATEGORY", nullable=false) ) } )
    public ClientclientcategoryId getId() {
        return this.id;
    }
    
    public void setId(ClientclientcategoryId id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="CLIENT", nullable=false, insertable=false, updatable=false)
    public Client getClient() {
        return this.client;
    }
    
    public void setClient(Client client) {
        this.client = client;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="CLIENTCATEGORY", nullable=false, insertable=false, updatable=false)
    public Clientcategory getClientcategory() {
        return this.clientcategory;
    }
    
    public void setClientcategory(Clientcategory clientcategory) {
        this.clientcategory = clientcategory;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDATETIME", nullable=false, length=19)
    public Date getCreatedatetime() {
        return this.createdatetime;
    }
    
    public void setCreatedatetime(Date createdatetime) {
        this.createdatetime = createdatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }




}


