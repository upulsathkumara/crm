package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Widget generated by hbm2java
 */
@Entity
@Table(name="widget"

)
public class Widget  implements java.io.Serializable {


     private Integer widgetid;
     private Status status;
     private String description;
     private Date createddatetime;
     private Date lastupdateddatetime;
     private String createduser;
     private Set<Userroledashboardwidget> userroledashboardwidgets = new HashSet<Userroledashboardwidget>(0);

    public Widget() {
    }

	
    public Widget(Date createddatetime, Date lastupdateddatetime) {
        this.createddatetime = createddatetime;
        this.lastupdateddatetime = lastupdateddatetime;
    }
    public Widget(Status status, String description, Date createddatetime, Date lastupdateddatetime, String createduser, Set<Userroledashboardwidget> userroledashboardwidgets) {
       this.status = status;
       this.description = description;
       this.createddatetime = createddatetime;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createduser = createduser;
       this.userroledashboardwidgets = userroledashboardwidgets;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="WIDGETID", unique=true, nullable=false)
    public Integer getWidgetid() {
        return this.widgetid;
    }
    
    public void setWidgetid(Integer widgetid) {
        this.widgetid = widgetid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=32)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="widget")
    public Set<Userroledashboardwidget> getUserroledashboardwidgets() {
        return this.userroledashboardwidgets;
    }
    
    public void setUserroledashboardwidgets(Set<Userroledashboardwidget> userroledashboardwidgets) {
        this.userroledashboardwidgets = userroledashboardwidgets;
    }




}


