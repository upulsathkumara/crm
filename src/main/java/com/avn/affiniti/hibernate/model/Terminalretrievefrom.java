package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Terminalretrievefrom generated by hbm2java
 */
@Entity
@Table(name="terminalretrievefrom"

)
public class Terminalretrievefrom  implements java.io.Serializable {


     private Integer terminalretrievefromid;
     private Status status;
     private String description;
     private Integer sortid;
     private Date lastupdateddatetime;
     private Date createddatetime;
     private String createduser;

    public Terminalretrievefrom() {
    }

	
    public Terminalretrievefrom(Date lastupdateddatetime, Date createddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
        this.createddatetime = createddatetime;
    }
    public Terminalretrievefrom(Status status, String description, Integer sortid, Date lastupdateddatetime, Date createddatetime, String createduser) {
       this.status = status;
       this.description = description;
       this.sortid = sortid;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createddatetime = createddatetime;
       this.createduser = createduser;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="TERMINALRETRIEVEFROMID", unique=true, nullable=false)
    public Integer getTerminalretrievefromid() {
        return this.terminalretrievefromid;
    }
    
    public void setTerminalretrievefromid(Integer terminalretrievefromid) {
        this.terminalretrievefromid = terminalretrievefromid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=32)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="SORTID")
    public Integer getSortid() {
        return this.sortid;
    }
    
    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }




}


