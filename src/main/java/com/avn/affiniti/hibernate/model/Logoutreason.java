package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Logoutreason generated by hbm2java
 */
@Entity
@Table(name="logoutreason"

)
public class Logoutreason  implements java.io.Serializable {


     private Integer reasonid;
     private Status status;
     private String reasondescription;
     private Integer sortid;
     private Date createddatetime;
     private Date lastupdateddatetime;
     private String createduser;

    public Logoutreason() {
    }

	
    public Logoutreason(Date createddatetime, Date lastupdateddatetime) {
        this.createddatetime = createddatetime;
        this.lastupdateddatetime = lastupdateddatetime;
    }
    public Logoutreason(Status status, String reasondescription, Integer sortid, Date createddatetime, Date lastupdateddatetime, String createduser) {
       this.status = status;
       this.reasondescription = reasondescription;
       this.sortid = sortid;
       this.createddatetime = createddatetime;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createduser = createduser;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="REASONID", unique=true, nullable=false)
    public Integer getReasonid() {
        return this.reasonid;
    }
    
    public void setReasonid(Integer reasonid) {
        this.reasonid = reasonid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="REASONDESCRIPTION", length=32)
    public String getReasondescription() {
        return this.reasondescription;
    }
    
    public void setReasondescription(String reasondescription) {
        this.reasondescription = reasondescription;
    }

    
    @Column(name="SORTID")
    public Integer getSortid() {
        return this.sortid;
    }
    
    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }




}


