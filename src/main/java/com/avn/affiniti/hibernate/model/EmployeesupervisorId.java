package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * EmployeesupervisorId generated by hbm2java
 */
@Embeddable
public class EmployeesupervisorId  implements java.io.Serializable {


     private int employeeid;
     private int supervisor;

    public EmployeesupervisorId() {
    }

    public EmployeesupervisorId(int employeeid, int supervisor) {
       this.employeeid = employeeid;
       this.supervisor = supervisor;
    }
   


    @Column(name="EMPLOYEEID", nullable=false)
    public int getEmployeeid() {
        return this.employeeid;
    }
    
    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }


    @Column(name="SUPERVISOR", nullable=false)
    public int getSupervisor() {
        return this.supervisor;
    }
    
    public void setSupervisor(int supervisor) {
        this.supervisor = supervisor;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof EmployeesupervisorId) ) return false;
		 EmployeesupervisorId castOther = ( EmployeesupervisorId ) other; 
         
		 return (this.getEmployeeid()==castOther.getEmployeeid())
 && (this.getSupervisor()==castOther.getSupervisor());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getEmployeeid();
         result = 37 * result + this.getSupervisor();
         return result;
   }   


}


