package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SectiontaskId generated by hbm2java
 */
@Embeddable
public class SectiontaskId  implements java.io.Serializable {


     private int sectionid;
     private int taskid;

    public SectiontaskId() {
    }

    public SectiontaskId(int sectionid, int taskid) {
       this.sectionid = sectionid;
       this.taskid = taskid;
    }
   


    @Column(name="SECTIONID", nullable=false)
    public int getSectionid() {
        return this.sectionid;
    }
    
    public void setSectionid(int sectionid) {
        this.sectionid = sectionid;
    }


    @Column(name="TASKID", nullable=false)
    public int getTaskid() {
        return this.taskid;
    }
    
    public void setTaskid(int taskid) {
        this.taskid = taskid;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SectiontaskId) ) return false;
		 SectiontaskId castOther = ( SectiontaskId ) other; 
         
		 return (this.getSectionid()==castOther.getSectionid())
 && (this.getTaskid()==castOther.getTaskid());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getSectionid();
         result = 37 * result + this.getTaskid();
         return result;
   }   


}


