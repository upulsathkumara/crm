package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Location generated by hbm2java
 */
@Entity
@Table(name="location"

)
public class Location  implements java.io.Serializable {


     private Integer locationid;
     private String locationdescription;
     private String locationcode;
     private Date lastupdateddatetime;
     private Date createddatetime;
     private String createduser;
     private Set<District> districts = new HashSet<District>(0);
     private Set<Inventoryterminalitem> inventoryterminalitems = new HashSet<Inventoryterminalitem>(0);
     private Set<Inventoryhardwareitem> inventoryhardwareitems = new HashSet<Inventoryhardwareitem>(0);
     private Set<Employee> employees = new HashSet<Employee>(0);
     private Set<Organization> organizations = new HashSet<Organization>(0);
     private Set<Client> clients = new HashSet<Client>(0);
     private Set<Terminalbrand> terminalbrands = new HashSet<Terminalbrand>(0);

    public Location() {
    }

	
    public Location(Date lastupdateddatetime, Date createddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
        this.createddatetime = createddatetime;
    }
    public Location(String locationdescription, String locationcode, Date lastupdateddatetime, Date createddatetime, String createduser, Set<District> districts, Set<Inventoryterminalitem> inventoryterminalitems, Set<Inventoryhardwareitem> inventoryhardwareitems, Set<Employee> employees, Set<Organization> organizations, Set<Client> clients, Set<Terminalbrand> terminalbrands) {
       this.locationdescription = locationdescription;
       this.locationcode = locationcode;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createddatetime = createddatetime;
       this.createduser = createduser;
       this.districts = districts;
       this.inventoryterminalitems = inventoryterminalitems;
       this.inventoryhardwareitems = inventoryhardwareitems;
       this.employees = employees;
       this.organizations = organizations;
       this.clients = clients;
       this.terminalbrands = terminalbrands;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="LOCATIONID", unique=true, nullable=false)
    public Integer getLocationid() {
        return this.locationid;
    }
    
    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    
    @Column(name="LOCATIONDESCRIPTION", length=64)
    public String getLocationdescription() {
        return this.locationdescription;
    }
    
    public void setLocationdescription(String locationdescription) {
        this.locationdescription = locationdescription;
    }

    
    @Column(name="LOCATIONCODE", length=16)
    public String getLocationcode() {
        return this.locationcode;
    }
    
    public void setLocationcode(String locationcode) {
        this.locationcode = locationcode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    
    @Column(name="CREATEDUSER", length=16)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<District> getDistricts() {
        return this.districts;
    }
    
    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Inventoryterminalitem> getInventoryterminalitems() {
        return this.inventoryterminalitems;
    }
    
    public void setInventoryterminalitems(Set<Inventoryterminalitem> inventoryterminalitems) {
        this.inventoryterminalitems = inventoryterminalitems;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Inventoryhardwareitem> getInventoryhardwareitems() {
        return this.inventoryhardwareitems;
    }
    
    public void setInventoryhardwareitems(Set<Inventoryhardwareitem> inventoryhardwareitems) {
        this.inventoryhardwareitems = inventoryhardwareitems;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Employee> getEmployees() {
        return this.employees;
    }
    
    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Organization> getOrganizations() {
        return this.organizations;
    }
    
    public void setOrganizations(Set<Organization> organizations) {
        this.organizations = organizations;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Client> getClients() {
        return this.clients;
    }
    
    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Terminalbrand> getTerminalbrands() {
        return this.terminalbrands;
    }
    
    public void setTerminalbrands(Set<Terminalbrand> terminalbrands) {
        this.terminalbrands = terminalbrands;
    }




}


