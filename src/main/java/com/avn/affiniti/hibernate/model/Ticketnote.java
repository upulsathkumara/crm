package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ticketnote generated by hbm2java
 */
@Entity
@Table(name="ticketnote"

)
public class Ticketnote  implements java.io.Serializable {


     private Integer ticketnoteid;
     private Ticket ticket;
     private String description;
     private Date lastupdateddatetime;
     private Date createdatetime;
     private String createduser;

    public Ticketnote() {
    }

	
    public Ticketnote(Date lastupdateddatetime, Date createdatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
        this.createdatetime = createdatetime;
    }
    public Ticketnote(Ticket ticket, String description, Date lastupdateddatetime, Date createdatetime, String createduser) {
       this.ticket = ticket;
       this.description = description;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createdatetime = createdatetime;
       this.createduser = createduser;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="TICKETNOTEID", unique=true, nullable=false)
    public Integer getTicketnoteid() {
        return this.ticketnoteid;
    }
    
    public void setTicketnoteid(Integer ticketnoteid) {
        this.ticketnoteid = ticketnoteid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TICKET")
    public Ticket getTicket() {
        return this.ticket;
    }
    
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    
    @Column(name="DESCRIPTION", length=256)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDATETIME", nullable=false, length=19)
    public Date getCreatedatetime() {
        return this.createdatetime;
    }
    
    public void setCreatedatetime(Date createdatetime) {
        this.createdatetime = createdatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }




}


