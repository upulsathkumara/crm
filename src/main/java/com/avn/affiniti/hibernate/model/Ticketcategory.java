package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ticketcategory generated by hbm2java
 */
@Entity
@Table(name="ticketcategory"

)
public class Ticketcategory  implements java.io.Serializable {


     private Integer ticketcategoryid;
     private Organization organization;
     private Status status;
     private String description;
     private Integer sla;
     private Integer sortid;
     private Date lastupdateddatetime;
     private Date createddatetime;
     private String createduser;
     private Set<Ticketassignment> ticketassignments = new HashSet<Ticketassignment>(0);
     private Set<Ticketcatgorycontent> ticketcatgorycontents = new HashSet<Ticketcatgorycontent>(0);
     private Set<Ticketcategoryuserrole> ticketcategoryuserroles = new HashSet<Ticketcategoryuserrole>(0);
     private Set<Ticket> tickets = new HashSet<Ticket>(0);
     private Set<Bulkterminallot> bulkterminallots = new HashSet<Bulkterminallot>(0);
     private Set<Ticketcategorystatus> ticketcategorystatuses = new HashSet<Ticketcategorystatus>(0);
     private Set<Userroleticketcategory> userroleticketcategories = new HashSet<Userroleticketcategory>(0);
     private Set<Organizationproductticketcategory> organizationproductticketcategories = new HashSet<Organizationproductticketcategory>(0);

    public Ticketcategory() {
    }

	
    public Ticketcategory(Date lastupdateddatetime, Date createddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
        this.createddatetime = createddatetime;
    }
    public Ticketcategory(Organization organization, Status status, String description, Integer sla, Integer sortid, Date lastupdateddatetime, Date createddatetime, String createduser, Set<Ticketassignment> ticketassignments, Set<Ticketcatgorycontent> ticketcatgorycontents, Set<Ticketcategoryuserrole> ticketcategoryuserroles, Set<Ticket> tickets, Set<Bulkterminallot> bulkterminallots, Set<Ticketcategorystatus> ticketcategorystatuses, Set<Userroleticketcategory> userroleticketcategories, Set<Organizationproductticketcategory> organizationproductticketcategories) {
       this.organization = organization;
       this.status = status;
       this.description = description;
       this.sla = sla;
       this.sortid = sortid;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createddatetime = createddatetime;
       this.createduser = createduser;
       this.ticketassignments = ticketassignments;
       this.ticketcatgorycontents = ticketcatgorycontents;
       this.ticketcategoryuserroles = ticketcategoryuserroles;
       this.tickets = tickets;
       this.bulkterminallots = bulkterminallots;
       this.ticketcategorystatuses = ticketcategorystatuses;
       this.userroleticketcategories = userroleticketcategories;
       this.organizationproductticketcategories = organizationproductticketcategories;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="TICKETCATEGORYID", unique=true, nullable=false)
    public Integer getTicketcategoryid() {
        return this.ticketcategoryid;
    }
    
    public void setTicketcategoryid(Integer ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ORGANIZATION")
    public Organization getOrganization() {
        return this.organization;
    }
    
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=64)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="SLA")
    public Integer getSla() {
        return this.sla;
    }
    
    public void setSla(Integer sla) {
        this.sla = sla;
    }

    
    @Column(name="SORTID")
    public Integer getSortid() {
        return this.sortid;
    }
    
    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Ticketassignment> getTicketassignments() {
        return this.ticketassignments;
    }
    
    public void setTicketassignments(Set<Ticketassignment> ticketassignments) {
        this.ticketassignments = ticketassignments;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Ticketcatgorycontent> getTicketcatgorycontents() {
        return this.ticketcatgorycontents;
    }
    
    public void setTicketcatgorycontents(Set<Ticketcatgorycontent> ticketcatgorycontents) {
        this.ticketcatgorycontents = ticketcatgorycontents;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Ticketcategoryuserrole> getTicketcategoryuserroles() {
        return this.ticketcategoryuserroles;
    }
    
    public void setTicketcategoryuserroles(Set<Ticketcategoryuserrole> ticketcategoryuserroles) {
        this.ticketcategoryuserroles = ticketcategoryuserroles;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Ticket> getTickets() {
        return this.tickets;
    }
    
    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Bulkterminallot> getBulkterminallots() {
        return this.bulkterminallots;
    }
    
    public void setBulkterminallots(Set<Bulkterminallot> bulkterminallots) {
        this.bulkterminallots = bulkterminallots;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Ticketcategorystatus> getTicketcategorystatuses() {
        return this.ticketcategorystatuses;
    }
    
    public void setTicketcategorystatuses(Set<Ticketcategorystatus> ticketcategorystatuses) {
        this.ticketcategorystatuses = ticketcategorystatuses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Userroleticketcategory> getUserroleticketcategories() {
        return this.userroleticketcategories;
    }
    
    public void setUserroleticketcategories(Set<Userroleticketcategory> userroleticketcategories) {
        this.userroleticketcategories = userroleticketcategories;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ticketcategory")
    public Set<Organizationproductticketcategory> getOrganizationproductticketcategories() {
        return this.organizationproductticketcategories;
    }
    
    public void setOrganizationproductticketcategories(Set<Organizationproductticketcategory> organizationproductticketcategories) {
        this.organizationproductticketcategories = organizationproductticketcategories;
    }




}


