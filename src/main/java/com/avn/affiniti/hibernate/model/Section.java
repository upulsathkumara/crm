package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Section generated by hbm2java
 */
@Entity
@Table(name="section"

)
public class Section  implements java.io.Serializable {


     private Integer sectionid;
     private Section section;
     private Status status;
     private String description;
     private int sectionlevel;
     private String icon;
     private boolean onlyparent;
     private String url;
     private Integer sortid;
     private Date createddatetime;
     private Date lastupdateddatetime;
     private String createduser;
     private Set<Userrolesection> userrolesections = new HashSet<Userrolesection>(0);
     private Set<Subsection> subsections = new HashSet<Subsection>(0);
     private Set<Userrolesubsection> userrolesubsections = new HashSet<Userrolesubsection>(0);
     private Set<Subsectiontask> subsectiontasks = new HashSet<Subsectiontask>(0);
     private Set<Section> sections = new HashSet<Section>(0);
     private Set<Sectiontask> sectiontasks = new HashSet<Sectiontask>(0);

    public Section() {
    }

	
    public Section(int sectionlevel, boolean onlyparent, Date createddatetime, Date lastupdateddatetime) {
        this.sectionlevel = sectionlevel;
        this.onlyparent = onlyparent;
        this.createddatetime = createddatetime;
        this.lastupdateddatetime = lastupdateddatetime;
    }
    public Section(Section section, Status status, String description, int sectionlevel, String icon, boolean onlyparent, String url, Integer sortid, Date createddatetime, Date lastupdateddatetime, String createduser, Set<Userrolesection> userrolesections, Set<Subsection> subsections, Set<Userrolesubsection> userrolesubsections, Set<Subsectiontask> subsectiontasks, Set<Section> sections, Set<Sectiontask> sectiontasks) {
       this.section = section;
       this.status = status;
       this.description = description;
       this.sectionlevel = sectionlevel;
       this.icon = icon;
       this.onlyparent = onlyparent;
       this.url = url;
       this.sortid = sortid;
       this.createddatetime = createddatetime;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createduser = createduser;
       this.userrolesections = userrolesections;
       this.subsections = subsections;
       this.userrolesubsections = userrolesubsections;
       this.subsectiontasks = subsectiontasks;
       this.sections = sections;
       this.sectiontasks = sectiontasks;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="SECTIONID", unique=true, nullable=false)
    public Integer getSectionid() {
        return this.sectionid;
    }
    
    public void setSectionid(Integer sectionid) {
        this.sectionid = sectionid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="PARENTSECTION")
    public Section getSection() {
        return this.section;
    }
    
    public void setSection(Section section) {
        this.section = section;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=32)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="SECTIONLEVEL", nullable=false)
    public int getSectionlevel() {
        return this.sectionlevel;
    }
    
    public void setSectionlevel(int sectionlevel) {
        this.sectionlevel = sectionlevel;
    }

    
    @Column(name="ICON", length=64)
    public String getIcon() {
        return this.icon;
    }
    
    public void setIcon(String icon) {
        this.icon = icon;
    }

    
    @Column(name="ONLYPARENT", nullable=false)
    public boolean isOnlyparent() {
        return this.onlyparent;
    }
    
    public void setOnlyparent(boolean onlyparent) {
        this.onlyparent = onlyparent;
    }

    
    @Column(name="URL", length=215)
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    
    @Column(name="SORTID")
    public Integer getSortid() {
        return this.sortid;
    }
    
    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    
    @Column(name="CREATEDUSER", length=32)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="section")
    public Set<Userrolesection> getUserrolesections() {
        return this.userrolesections;
    }
    
    public void setUserrolesections(Set<Userrolesection> userrolesections) {
        this.userrolesections = userrolesections;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="section")
    public Set<Subsection> getSubsections() {
        return this.subsections;
    }
    
    public void setSubsections(Set<Subsection> subsections) {
        this.subsections = subsections;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="section")
    public Set<Userrolesubsection> getUserrolesubsections() {
        return this.userrolesubsections;
    }
    
    public void setUserrolesubsections(Set<Userrolesubsection> userrolesubsections) {
        this.userrolesubsections = userrolesubsections;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="section")
    public Set<Subsectiontask> getSubsectiontasks() {
        return this.subsectiontasks;
    }
    
    public void setSubsectiontasks(Set<Subsectiontask> subsectiontasks) {
        this.subsectiontasks = subsectiontasks;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="section")
    public Set<Section> getSections() {
        return this.sections;
    }
    
    public void setSections(Set<Section> sections) {
        this.sections = sections;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="section")
    public Set<Sectiontask> getSectiontasks() {
        return this.sectiontasks;
    }
    
    public void setSectiontasks(Set<Sectiontask> sectiontasks) {
        this.sectiontasks = sectiontasks;
    }




}


