package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * OrganizationproductticketcategoryId generated by hbm2java
 */
@Embeddable
public class OrganizationproductticketcategoryId  implements java.io.Serializable {


     private int organization;
     private int product;
     private int ticketcategory;

    public OrganizationproductticketcategoryId() {
    }

    public OrganizationproductticketcategoryId(int organization, int product, int ticketcategory) {
       this.organization = organization;
       this.product = product;
       this.ticketcategory = ticketcategory;
    }
   


    @Column(name="ORGANIZATION", nullable=false)
    public int getOrganization() {
        return this.organization;
    }
    
    public void setOrganization(int organization) {
        this.organization = organization;
    }


    @Column(name="PRODUCT", nullable=false)
    public int getProduct() {
        return this.product;
    }
    
    public void setProduct(int product) {
        this.product = product;
    }


    @Column(name="TICKETCATEGORY", nullable=false)
    public int getTicketcategory() {
        return this.ticketcategory;
    }
    
    public void setTicketcategory(int ticketcategory) {
        this.ticketcategory = ticketcategory;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof OrganizationproductticketcategoryId) ) return false;
		 OrganizationproductticketcategoryId castOther = ( OrganizationproductticketcategoryId ) other; 
         
		 return (this.getOrganization()==castOther.getOrganization())
 && (this.getProduct()==castOther.getProduct())
 && (this.getTicketcategory()==castOther.getTicketcategory());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getOrganization();
         result = 37 * result + this.getProduct();
         result = 37 * result + this.getTicketcategory();
         return result;
   }   


}


