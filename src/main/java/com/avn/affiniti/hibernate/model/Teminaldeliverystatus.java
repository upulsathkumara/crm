package com.avn.affiniti.hibernate.model;
// Generated Jan 29, 2018 12:02:34 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Teminaldeliverystatus generated by hbm2java
 */
@Entity
@Table(name="teminaldeliverystatus"

)
public class Teminaldeliverystatus  implements java.io.Serializable {


     private Integer terminaldeliverystatusid;
     private Status status;
     private String description;
     private Integer sortid;
     private Date lastupdateddatetime;
     private Date createddatetime;
     private String createduser;
     private Set<Tickethistory> tickethistories = new HashSet<Tickethistory>(0);

    public Teminaldeliverystatus() {
    }

	
    public Teminaldeliverystatus(Date lastupdateddatetime, Date createddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
        this.createddatetime = createddatetime;
    }
    public Teminaldeliverystatus(Status status, String description, Integer sortid, Date lastupdateddatetime, Date createddatetime, String createduser, Set<Tickethistory> tickethistories) {
       this.status = status;
       this.description = description;
       this.sortid = sortid;
       this.lastupdateddatetime = lastupdateddatetime;
       this.createddatetime = createddatetime;
       this.createduser = createduser;
       this.tickethistories = tickethistories;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="TERMINALDELIVERYSTATUSID", unique=true, nullable=false)
    public Integer getTerminaldeliverystatusid() {
        return this.terminaldeliverystatusid;
    }
    
    public void setTerminaldeliverystatusid(Integer terminaldeliverystatusid) {
        this.terminaldeliverystatusid = terminaldeliverystatusid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=8)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="SORTID")
    public Integer getSortid() {
        return this.sortid;
    }
    
    public void setSortid(Integer sortid) {
        this.sortid = sortid;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDDATETIME", nullable=false, length=19)
    public Date getLastupdateddatetime() {
        return this.lastupdateddatetime;
    }
    
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDDATETIME", nullable=false, length=19)
    public Date getCreateddatetime() {
        return this.createddatetime;
    }
    
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    
    @Column(name="CREATEDUSER", length=16)
    public String getCreateduser() {
        return this.createduser;
    }
    
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="teminaldeliverystatus")
    public Set<Tickethistory> getTickethistories() {
        return this.tickethistories;
    }
    
    public void setTickethistories(Set<Tickethistory> tickethistories) {
        this.tickethistories = tickethistories;
    }




}


