package com.avn.affiniti.model.subsectiontask;

/**
 *
 * @author Kaushan Fernando
 * @Document subsectiontask
 * @Created on: Sep 26, 2017, 11:22:48 PM
 */
public class subsectiontask {

    String sectionId;
    String subSectionId;
    String taskId;
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
   

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSubSectionId() {
        return subSectionId;
    }

    public void setSubSectionId(String subSectionId) {
        this.subSectionId = subSectionId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

   
    

}
