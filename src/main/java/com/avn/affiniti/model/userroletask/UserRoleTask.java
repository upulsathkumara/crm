package com.avn.affiniti.model.userroletask;

/**
 *
 * @author Kaushan Fernando
   @Document UserRoleTask
   @Created on: Aug 17, 2017, 10:35:06 AM
 */
public class UserRoleTask {
    private String sectionId;
    private String subsectionId;
    private String userroleId;
    private String taskId;
    private String createddatetime;
    private String createduser;

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSubsectionId() {
        return subsectionId;
    }

    public void setSubsectionId(String subsectionId) {
        this.subsectionId = subsectionId;
    }

    public String getUserroleId() {
        return userroleId;
    }

    public void setUserroleId(String userroleId) {
        this.userroleId = userroleId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCreateddatetime() {
        return createddatetime;
    }

    public void setCreateddatetime(String createddatetime) {
        this.createddatetime = createddatetime;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    
}
