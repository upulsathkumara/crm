/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.userrolsubsection;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userroletask;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author chandima
 */
public class Userrolesubsection {

    private Integer userrolesubsectionid;
    private Section section;
    private Subsection subsection;
    private Userrole userrole;
    private Date createddatetime;
    private Date lastupdateddatetime;
    private String createduser;
    private String userrolid;
    private String sectionId;
    private String subsectionId;
    private String command;
    private int removeStatus;
    private Set<Userroletask> userroletasks = new HashSet<Userroletask>(0);
    private List<Integer> subsectionids;

    public String getUserrolid() {
        return userrolid;
    }

    public String getCommand() {
        return command;
    }

    public int getRemoveStatus() {
        return removeStatus;
    }

    public void setRemoveStatus(int removeStatus) {
        this.removeStatus = removeStatus;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setUserrolid(String userrolid) {
        this.userrolid = userrolid;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getSubsectionId() {
        return subsectionId;
    }

    public void setSubsectionId(String subsectionId) {
        this.subsectionId = subsectionId;
    }

    public List<Integer> getSubsectionids() {
        return subsectionids;
    }

    public void setSubsectionids(List<Integer> subsectionids) {
        this.subsectionids = subsectionids;
    }

    public Integer getUserrolesubsectionid() {
        return userrolesubsectionid;
    }

    public void setUserrolesubsectionid(Integer userrolesubsectionid) {
        this.userrolesubsectionid = userrolesubsectionid;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Subsection getSubsection() {
        return subsection;
    }

    public void setSubsection(Subsection subsection) {
        this.subsection = subsection;
    }

    public Userrole getUserrole() {
        return userrole;
    }

    public void setUserrole(Userrole userrole) {
        this.userrole = userrole;
    }

    public Date getCreateddatetime() {
        return createddatetime;
    }

    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    public Date getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public Set<Userroletask> getUserroletasks() {
        return userroletasks;
    }

    public void setUserroletasks(Set<Userroletask> userroletasks) {
        this.userroletasks = userroletasks;
    }

}
