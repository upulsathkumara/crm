/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

/**
 * @Author : KAsun Udayanaga
 * @Document : TerminalRepairMysTicket
 * @Created on : Nov 22, 2017, 11:56:41 PM
 */
public class TerminalRepairMysTicket {

    private int ticketid;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String ticketcategory;
    private int ticketcategoryid;
    private String damagepartdescription;
    private String causeoffault;
    private String actiontaken;
    private String replacedparts;
    private String parttype;
    private String revision;
    private String mac;
    private String ptid;
    private String completiondatetime;
    private String totaltimespent;
    private String specialremark;
    private String deliveryorderreference;
    private String rmareferance;
    private String dispatchdate;
    private String courierserviceprovider;
    private String tackingnumber;
    private String destination;
    private String contactperson;
    private String contactnumber;
    private String district;
    private String locationaddress;
    private String deliveryrequirement;
    private String deliverydestination;
    private String invoicenumber;
    private String purpose;
    private String invoicevalue;
    private String invoicedate;
    private String statusid;
    private String bkplocationaddress;
    private String description;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the damagepartdescription
     */
    public String getDamagepartdescription() {
        return damagepartdescription;
    }

    /**
     * @param damagepartdescription the damagepartdescription to set
     */
    public void setDamagepartdescription(String damagepartdescription) {
        this.damagepartdescription = damagepartdescription;
    }

    /**
     * @return the causeoffault
     */
    public String getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(String causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the actiontaken
     */
    public String getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the replacedparts
     */
    public String getReplacedparts() {
        return replacedparts;
    }

    /**
     * @param replacedparts the replacedparts to set
     */
    public void setReplacedparts(String replacedparts) {
        this.replacedparts = replacedparts;
    }

    /**
     * @return the parttype
     */
    public String getParttype() {
        return parttype;
    }

    /**
     * @param parttype the parttype to set
     */
    public void setParttype(String parttype) {
        this.parttype = parttype;
    }

    /**
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * @return the ptid
     */
    public String getPtid() {
        return ptid;
    }

    /**
     * @param ptid the ptid to set
     */
    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    /**
     * @return the completiondatetime
     */
    public String getCompletiondatetime() {
        return completiondatetime;
    }

    /**
     * @param completiondatetime the completiondatetime to set
     */
    public void setCompletiondatetime(String completiondatetime) {
        this.completiondatetime = completiondatetime;
    }

    /**
     * @return the totaltimespent
     */
    public String getTotaltimespent() {
        return totaltimespent;
    }

    /**
     * @param totaltimespent the totaltimespent to set
     */
    public void setTotaltimespent(String totaltimespent) {
        this.totaltimespent = totaltimespent;
    }

    /**
     * @return the specialremark
     */
    public String getSpecialremark() {
        return specialremark;
    }

    /**
     * @param specialremark the specialremark to set
     */
    public void setSpecialremark(String specialremark) {
        this.specialremark = specialremark;
    }

    /**
     * @return the deliveryorderreference
     */
    public String getDeliveryorderreference() {
        return deliveryorderreference;
    }

    /**
     * @param deliveryorderreference the deliveryorderreference to set
     */
    public void setDeliveryorderreference(String deliveryorderreference) {
        this.deliveryorderreference = deliveryorderreference;
    }

    /**
     * @return the rmareferance
     */
    public String getRmareferance() {
        return rmareferance;
    }

    /**
     * @param rmareferance the rmareferance to set
     */
    public void setRmareferance(String rmareferance) {
        this.rmareferance = rmareferance;
    }

    /**
     * @return the dispatchdate
     */
    public String getDispatchdate() {
        return dispatchdate;
    }

    /**
     * @param dispatchdate the dispatchdate to set
     */
    public void setDispatchdate(String dispatchdate) {
        this.dispatchdate = dispatchdate;
    }

    /**
     * @return the courierserviceprovider
     */
    public String getCourierserviceprovider() {
        return courierserviceprovider;
    }

    /**
     * @param courierserviceprovider the courierserviceprovider to set
     */
    public void setCourierserviceprovider(String courierserviceprovider) {
        this.courierserviceprovider = courierserviceprovider;
    }

    /**
     * @return the tackingnumber
     */
    public String getTackingnumber() {
        return tackingnumber;
    }

    /**
     * @param tackingnumber the tackingnumber to set
     */
    public void setTackingnumber(String tackingnumber) {
        this.tackingnumber = tackingnumber;
    }

    /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactnumber
     */
    public String getContactnumber() {
        return contactnumber;
    }

    /**
     * @param contactnumber the contactnumber to set
     */
    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the deliveryrequirement
     */
    public String getDeliveryrequirement() {
        return deliveryrequirement;
    }

    /**
     * @param deliveryrequirement the deliveryrequirement to set
     */
    public void setDeliveryrequirement(String deliveryrequirement) {
        this.deliveryrequirement = deliveryrequirement;
    }

    /**
     * @return the deliverydestination
     */
    public String getDeliverydestination() {
        return deliverydestination;
    }

    /**
     * @param deliverydestination the deliverydestination to set
     */
    public void setDeliverydestination(String deliverydestination) {
        this.deliverydestination = deliverydestination;
    }

    /**
     * @return the invoicenumber
     */
    public String getInvoicenumber() {
        return invoicenumber;
    }

    /**
     * @param invoicenumber the invoicenumber to set
     */
    public void setInvoicenumber(String invoicenumber) {
        this.invoicenumber = invoicenumber;
    }

    /**
     * @return the purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * @param purpose the purpose to set
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * @return the invoicevalue
     */
    public String getInvoicevalue() {
        return invoicevalue;
    }

    /**
     * @param invoicevalue the invoicevalue to set
     */
    public void setInvoicevalue(String invoicevalue) {
        this.invoicevalue = invoicevalue;
    }

    /**
     * @return the invoicedate
     */
    public String getInvoicedate() {
        return invoicedate;
    }

    /**
     * @param invoicedate the invoicedate to set
     */
    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate;
    }

    /**
     * @return the statusid
     */
    public String getStatusid() {
        return statusid;
    }

    /**
     * @param statusid the statusid to set
     */
    public void setStatusid(String statusid) {
        this.statusid = statusid;
    }

    /**
     * @return the bkplocationaddress
     */
    public String getBkplocationaddress() {
        return bkplocationaddress;
    }

    /**
     * @param bkplocationaddress the bkplocationaddress to set
     */
    public void setBkplocationaddress(String bkplocationaddress) {
        this.bkplocationaddress = bkplocationaddress;
    }
    
    /**
     * @return the bkplocationaddress
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}