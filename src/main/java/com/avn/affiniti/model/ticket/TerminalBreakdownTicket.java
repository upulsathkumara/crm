/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

/**
 * @Author : Roshen Dilshan
 * @Document : TerminalBreakdownTicket
 * @Created on : Jun 26, 2017, 8:02:31 PM
 */
public class TerminalBreakdownTicket {

    private int ticketid;
    private String client;
    private String clienttype;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String territorymap;
    private int ticketcategoryid;
    private String ticketcategory;
    private String tid;
    private String mid;
    private String terminalserial;
    private String terminalbrand;
    private String terminalmodel;
    private String warrantyexpirystatus;
    private String amcaexpirystatus;
    private String lotno;
    private String partno;
    private String revision;
    private String mac;
    private String ptid;
    private String merchantname;
    private String contactperson;
    private String contactno;
    private String district;
    private String locationaddress;
    private String deleveryrequirement;
    private String deleverydestination;
    private String natureoffault;
    private String actiontobetaken;
    private String causeoffault;
    private String actiontaken;
    private String technicalofficerincharge;
    private String reportedby;
    private String reportedmerchant;
    private String status;
    private String btid;
    private String bmid;
    private String bterminalserial;
    private String bterminalbrand;
    private String bterminalmodel;
    private String deploymentdate;
    private String statusid;
    private String createdby;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the clienttype
     */
    public String getClienttype() {
        return clienttype;
    }

    /**
     * @param clienttype the clienttype to set
     */
    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territorymap
     */
    public String getTerritorymap() {
        return territorymap;
    }

    /**
     * @param territorymap the territorymap to set
     */
    public void setTerritorymap(String territorymap) {
        this.territorymap = territorymap;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the terminalserial
     */
    public String getTerminalserial() {
        return terminalserial;
    }

    /**
     * @param terminalserial the terminalserial to set
     */
    public void setTerminalserial(String terminalserial) {
        this.terminalserial = terminalserial;
    }

    /**
     * @return the terminalbrand
     */
    public String getTerminalbrand() {
        return terminalbrand;
    }

    /**
     * @param terminalbrand the terminalbrand to set
     */
    public void setTerminalbrand(String terminalbrand) {
        this.terminalbrand = terminalbrand;
    }

    /**
     * @return the terminalmodel
     */
    public String getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(String terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the warrantyexpirystatus
     */
    public String getWarrantyexpirystatus() {
        return warrantyexpirystatus;
    }

    /**
     * @param warrantyexpirystatus the warrantyexpirystatus to set
     */
    public void setWarrantyexpirystatus(String warrantyexpirystatus) {
        this.warrantyexpirystatus = warrantyexpirystatus;
    }

    /**
     * @return the amcaexpirystatus
     */
    public String getAmcaexpirystatus() {
        return amcaexpirystatus;
    }

    /**
     * @param amcaexpirystatus the amcaexpirystatus to set
     */
    public void setAmcaexpirystatus(String amcaexpirystatus) {
        this.amcaexpirystatus = amcaexpirystatus;
    }

    /**
     * @return the lotno
     */
    public String getLotno() {
        return lotno;
    }

    /**
     * @param lotno the lotno to set
     */
    public void setLotno(String lotno) {
        this.lotno = lotno;
    }

    /**
     * @return the partno
     */
    public String getPartno() {
        return partno;
    }

    /**
     * @param partno the partno to set
     */
    public void setPartno(String partno) {
        this.partno = partno;
    }

    /**
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * @return the ptid
     */
    public String getPtid() {
        return ptid;
    }

    /**
     * @param ptid the ptid to set
     */
    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactno
     */
    public String getContactno() {
        return contactno;
    }

    /**
     * @param contactno the contactno to set
     */
    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the deleveryrequirement
     */
    public String getDeleveryrequirement() {
        return deleveryrequirement;
    }

    /**
     * @param deleveryrequirement the deleveryrequirement to set
     */
    public void setDeleveryrequirement(String deleveryrequirement) {
        this.deleveryrequirement = deleveryrequirement;
    }

    /**
     * @return the deleverydestination
     */
    public String getDeleverydestination() {
        return deleverydestination;
    }

    /**
     * @param deleverydestination the deleverydestination to set
     */
    public void setDeleverydestination(String deleverydestination) {
        this.deleverydestination = deleverydestination;
    }

    /**
     * @return the natureoffault
     */
    public String getNatureoffault() {
        return natureoffault;
    }

    /**
     * @param natureoffault the natureoffault to set
     */
    public void setNatureoffault(String natureoffault) {
        this.natureoffault = natureoffault;
    }

    /**
     * @return the actiontobetaken
     */
    public String getActiontobetaken() {
        return actiontobetaken;
    }

    /**
     * @param actiontobetaken the actiontobetaken to set
     */
    public void setActiontobetaken(String actiontobetaken) {
        this.actiontobetaken = actiontobetaken;
    }

    /**
     * @return the causeoffault
     */
    public String getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(String causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the actiontaken
     */
    public String getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the technicalofficerincharge
     */
    public String getTechnicalofficerincharge() {
        return technicalofficerincharge;
    }

    /**
     * @param technicalofficerincharge the technicalofficerincharge to set
     */
    public void setTechnicalofficerincharge(String technicalofficerincharge) {
        this.technicalofficerincharge = technicalofficerincharge;
    }

    /**
     * @return the reportedby
     */
    public String getReportedby() {
        return reportedby;
    }

    /**
     * @param reportedby the reportedby to set
     */
    public void setReportedby(String reportedby) {
        this.reportedby = reportedby;
    }

    /**
     * @return the reportedmerchant
     */
    public String getReportedmerchant() {
        return reportedmerchant;
    }

    /**
     * @param reportedmerchant the reportedmerchant to set
     */
    public void setReportedmerchant(String reportedmerchant) {
        this.reportedmerchant = reportedmerchant;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the btid
     */
    public String getBtid() {
        return btid;
    }

    /**
     * @param btid the btid to set
     */
    public void setBtid(String btid) {
        this.btid = btid;
    }

    /**
     * @return the bmid
     */
    public String getBmid() {
        return bmid;
    }

    /**
     * @param bmid the bmid to set
     */
    public void setBmid(String bmid) {
        this.bmid = bmid;
    }

    /**
     * @return the bterminalserial
     */
    public String getBterminalserial() {
        return bterminalserial;
    }

    /**
     * @param bterminalserial the bterminalserial to set
     */
    public void setBterminalserial(String bterminalserial) {
        this.bterminalserial = bterminalserial;
    }

    /**
     * @return the bterminalbrand
     */
    public String getBterminalbrand() {
        return bterminalbrand;
    }

    /**
     * @param bterminalbrand the bterminalbrand to set
     */
    public void setBterminalbrand(String bterminalbrand) {
        this.bterminalbrand = bterminalbrand;
    }

    /**
     * @return the bterminalmodel
     */
    public String getBterminalmodel() {
        return bterminalmodel;
    }

    /**
     * @param bterminalmodel the bterminalmodel to set
     */
    public void setBterminalmodel(String bterminalmodel) {
        this.bterminalmodel = bterminalmodel;
    }

    /**
     * @return the deploymentdate
     */
    public String getDeploymentdate() {
        return deploymentdate;
    }

    /**
     * @param deploymentdate the deploymentdate to set
     */
    public void setDeploymentdate(String deploymentdate) {
        this.deploymentdate = deploymentdate;
    }

    /**
     * @return the statusid
     */
    public String getStatusid() {
        return statusid;
    }

    /**
     * @param statusid the statusid to set
     */
    public void setStatusid(String statusid) {
        this.statusid = statusid;
    }

    /**
     * @return the createdby
     */
    public String getCreatedby() {
        return createdby;
    }

    /**
     * @param createdby the createdby to set
     */
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

}
