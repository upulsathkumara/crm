/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

/**
 * @Author : Roshen Dilshan
 * @Document : TerminalRepairTicket
 * @Created on : Jul 11, 2017, 4:45:11 AM
 */
public class TerminalRepairTicket {

    private int ticketid;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String territorymap;
    private int ticketcategoryid;
    private String ticketcategory;
    private String reportedby;
    private String status;
    private Integer statusid;
    private Integer isusernegligence;
    private Integer islightening;
    private Integer iscoveredunderwarranty;
    private Integer isunderamc;
    private Integer parentticket;
    private String damagepartdescription;
    private String warrantyexpirystatus;
    private Integer causeoffault;
    private Integer actiontaken;
    private String revision;
    private String mac;
    private String ptid;
    private String replacedpart;
    private String parttype;
    private String partnumber;
    private String partnumberofterminal;
    private String completiondatetime;
    private String totletimespent;
    private String specialremark;
    private String dispatchdate;
    private String courierserviceprovider;
    private String trackingnumber;
    private String deleverydestination;
    private String isfinalstatus;
    private String thstatus;
    private String ticketstage;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territorymap
     */
    public String getTerritorymap() {
        return territorymap;
    }

    /**
     * @param territorymap the territorymap to set
     */
    public void setTerritorymap(String territorymap) {
        this.territorymap = territorymap;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the reportedby
     */
    public String getReportedby() {
        return reportedby;
    }

    /**
     * @param reportedby the reportedby to set
     */
    public void setReportedby(String reportedby) {
        this.reportedby = reportedby;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the statusid
     */
    public Integer getStatusid() {
        return statusid;
    }

    /**
     * @param statusid the statusid to set
     */
    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    /**
     * @return the isusernegligence
     */
    public Integer getIsusernegligence() {
        return isusernegligence;
    }

    /**
     * @param isusernegligence the isusernegligence to set
     */
    public void setIsusernegligence(Integer isusernegligence) {
        this.isusernegligence = isusernegligence;
    }

    /**
     * @return the islightening
     */
    public Integer getIslightening() {
        return islightening;
    }

    /**
     * @param islightening the islightening to set
     */
    public void setIslightening(Integer islightening) {
        this.islightening = islightening;
    }

    /**
     * @return the iscoveredunderwarranty
     */
    public Integer getIscoveredunderwarranty() {
        return iscoveredunderwarranty;
    }

    /**
     * @param iscoveredunderwarranty the iscoveredunderwarranty to set
     */
    public void setIscoveredunderwarranty(Integer iscoveredunderwarranty) {
        this.iscoveredunderwarranty = iscoveredunderwarranty;
    }

    /**
     * @return the isunderamc
     */
    public Integer getIsunderamc() {
        return isunderamc;
    }

    /**
     * @param isunderamc the isunderamc to set
     */
    public void setIsunderamc(Integer isunderamc) {
        this.isunderamc = isunderamc;
    }

    /**
     * @return the parentticket
     */
    public Integer getParentticket() {
        return parentticket;
    }

    /**
     * @param parentticket the parentticket to set
     */
    public void setParentticket(Integer parentticket) {
        this.parentticket = parentticket;
    }

    /**
     * @return the damagepartdescription
     */
    public String getDamagepartdescription() {
        return damagepartdescription;
    }

    /**
     * @param damagepartdescription the damagepartdescription to set
     */
    public void setDamagepartdescription(String damagepartdescription) {
        this.damagepartdescription = damagepartdescription;
    }

    /**
     * @return the warrantyexpirystatus
     */
    public String getWarrantyexpirystatus() {
        return warrantyexpirystatus;
    }

    /**
     * @param warrantyexpirystatus the warrantyexpirystatus to set
     */
    public void setWarrantyexpirystatus(String warrantyexpirystatus) {
        this.warrantyexpirystatus = warrantyexpirystatus;
    }

    /**
     * @return the causeoffault
     */
    public Integer getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(Integer causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the actiontaken
     */
    public Integer getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(Integer actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * @return the ptid
     */
    public String getPtid() {
        return ptid;
    }

    /**
     * @param ptid the ptid to set
     */
    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    /**
     * @return the replacedpart
     */
    public String getReplacedpart() {
        return replacedpart;
    }

    /**
     * @param replacedpart the replacedpart to set
     */
    public void setReplacedpart(String replacedpart) {
        this.replacedpart = replacedpart;
    }

    /**
     * @return the parttype
     */
    public String getParttype() {
        return parttype;
    }

    /**
     * @param parttype the parttype to set
     */
    public void setParttype(String parttype) {
        this.parttype = parttype;
    }

    /**
     * @return the partnumber
     */
    public String getPartnumber() {
        return partnumber;
    }

    /**
     * @param partnumber the partnumber to set
     */
    public void setPartnumber(String partnumber) {
        this.partnumber = partnumber;
    }

    /**
     * @return the partnumberofterminal
     */
    public String getPartnumberofterminal() {
        return partnumberofterminal;
    }

    /**
     * @param partnumberofterminal the partnumberofterminal to set
     */
    public void setPartnumberofterminal(String partnumberofterminal) {
        this.partnumberofterminal = partnumberofterminal;
    }

    /**
     * @return the completiondatetime
     */
    public String getCompletiondatetime() {
        return completiondatetime;
    }

    /**
     * @param completiondatetime the completiondatetime to set
     */
    public void setCompletiondatetime(String completiondatetime) {
        this.completiondatetime = completiondatetime;
    }

    /**
     * @return the totletimespent
     */
    public String getTotletimespent() {
        return totletimespent;
    }

    /**
     * @param totletimespent the totletimespent to set
     */
    public void setTotletimespent(String totletimespent) {
        this.totletimespent = totletimespent;
    }

    /**
     * @return the specialremark
     */
    public String getSpecialremark() {
        return specialremark;
    }

    /**
     * @param specialremark the specialremark to set
     */
    public void setSpecialremark(String specialremark) {
        this.specialremark = specialremark;
    }

    /**
     * @return the dispatchdate
     */
    public String getDispatchdate() {
        return dispatchdate;
    }

    /**
     * @param dispatchdate the dispatchdate to set
     */
    public void setDispatchdate(String dispatchdate) {
        this.dispatchdate = dispatchdate;
    }

    /**
     * @return the courierserviceprovider
     */
    public String getCourierserviceprovider() {
        return courierserviceprovider;
    }

    /**
     * @param courierserviceprovider the courierserviceprovider to set
     */
    public void setCourierserviceprovider(String courierserviceprovider) {
        this.courierserviceprovider = courierserviceprovider;
    }

    /**
     * @return the trackingnumber
     */
    public String getTrackingnumber() {
        return trackingnumber;
    }

    /**
     * @param trackingnumber the trackingnumber to set
     */
    public void setTrackingnumber(String trackingnumber) {
        this.trackingnumber = trackingnumber;
    }

    /**
     * @return the deleverydestination
     */
    public String getDeleverydestination() {
        return deleverydestination;
    }

    /**
     * @param deleverydestination the deleverydestination to set
     */
    public void setDeleverydestination(String deleverydestination) {
        this.deleverydestination = deleverydestination;
    }

    /**
     * @return the idfinalstatus
     */
    public String getIsfinalstatus() {
        return isfinalstatus;
    }

    /**
     * @param idfinalstatus the idfinalstatus to set
     */
    public void setIsfinalstatus(String isfinalstatus) {
        this.isfinalstatus = isfinalstatus;
    }

    /**
     * @return the thstatus
     */
    public String getThstatus() {
        return thstatus;
    }

    /**
     * @param thstatus the thstatus to set
     */
    public void setThstatus(String thstatus) {
        this.thstatus = thstatus;
    }

    /**
     * @return the ticketstage
     */
    public String getTicketstage() {
        return ticketstage;
    }

    /**
     * @param ticketstage the ticketstage to set
     */
    public void setTicketstage(String ticketstage) {
        this.ticketstage = ticketstage;
    }

}
