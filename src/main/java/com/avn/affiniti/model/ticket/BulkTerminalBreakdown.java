/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Author : KAsun Udayanaga
 * @Document : BulkTerminalBreakdown
 * @Created on : Oct 3, 2017, 8:52:43 AM
 */
public class BulkTerminalBreakdown {

    private int ticketid;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String territorymap;
    private int ticketcategoryid;
    private String ticketcategory;
    private String filepath;
    private String action;
    private MultipartFile file;
    private String createduser;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territorymap
     */
    public String getTerritorymap() {
        return territorymap;
    }

    /**
     * @param territorymap the territorymap to set
     */
    public void setTerritorymap(String territorymap) {
        this.territorymap = territorymap;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the filepath
     */
    public String getFilepath() {
        return filepath;
    }

    /**
     * @param filepath the filepath to set
     */
    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the file
     */
    public MultipartFile getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(MultipartFile file) {
        this.file = file;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

}
