/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

/**
 *
 * @author ASHOK
 */
public class BackupRemovalTicket {

    private int ticketid;
    private String client;
    private String clienttype;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;    
    private String ticketcategory;
    private int ticketcategoryid;
    private String productcategory;
    private String tid;
    private String mid;
    private String terminalbrand;
    private String terminalmodel;
    private String bank;
    private String locationaddress;
    private String merchantname;
    private String manualassigneestatus;

    private String description;
    private String currency;
    private String remark;
    private String lotno;
    private String contactperson;
    private String contactno;
    private String district;
    private String statusid;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the clienttype
     */
    public String getClienttype() {
        return clienttype;
    }

    /**
     * @param clienttype the clienttype to set
     */
    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the terminalbrand
     */
    public String getTerminalbrand() {
        return terminalbrand;
    }

    /**
     * @param terminalbrand the terminalbrand to set
     */
    public void setTerminalbrand(String terminalbrand) {
        this.terminalbrand = terminalbrand;
    }

    /**
     * @return the terminalmodel
     */
    public String getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(String terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the manualassigneestatus
     */
    public String getManualassigneestatus() {
        return manualassigneestatus;
    }

    /**
     * @param manualassigneestatus the manualassigneestatus to set
     */
    public void setManualassigneestatus(String manualassigneestatus) {
        this.manualassigneestatus = manualassigneestatus;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the lotno
     */
    public String getLotno() {
        return lotno;
    }

    /**
     * @param lotno the lotno to set
     */
    public void setLotno(String lotno) {
        this.lotno = lotno;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactno
     */
    public String getContactno() {
        return contactno;
    }

    /**
     * @param contactno the contactno to set
     */
    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the statusid
     */
    public String getStatusid() {
        return statusid;
    }

    /**
     * @param statusid the statusid to set
     */
    public void setStatusid(String statusid) {
        this.statusid = statusid;
    }

}
