/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Author : Roshen Dilshan
 * @Document : NewInstallationTicke
 * @Created on : Jul 25, 2017, 10:48:15 AM
 */
public class NewInstallationTicke {

    private String client;
    private String clienttype;
    private int ticketid;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String territorymap;
    private int ticketcategoryid;
    private String ticketcategory;
    private String tid;
    private String mid;
    private String terminalserial;
    private String terminalbrand;
    private String terminalmodel;
    private String warrantyexpirystatus;
    private String amcaexpirystatus;
    private String lotno;
    private String partno;
    private String revision;
    private String mac;
    private String ptid;
    private String bank;
    private String merchantname;
    private String contactperson;
    private String contactno;
    private String district;
    private String locationaddress;
    private String deleveryrequirement;
    private String deleverydestination;
    private String natureoffault;
    private String actiontobetaken;
    private String causeoffault;
    private String actiontaken;
    private String technicalofficerincharge;
    private String reportedby;
    private String reportedmerchant;
    private String status;
    private String btid;
    private String bmid;
    private String bterminalserial;
    private String bterminalbrand;
    private String bterminalmodel;
    private String createduser;
    /*Action Taken by officer*/
    private int deployablestatus;
    private int terminaldeliverystatus;
    /*Merchant Acceptance*/
    private String deploymentdate;
    private int merchantacceptancestatus;
    private String statusid;

    private String sharingtid;
    private String sharingmid;
    private String nacno;
    private String sharingnacno;
//    private int espid;
    private String esp;
    private String profiletype;

    private String manualkey;
    private String offline;
    private String preauth;
    private String tip;
    private String cashadvance;
    private String panmask;
    private String l4;
    private String emv;
    private String fourdbc;
    private String void_;
    private String settlement;
    private String tobeinstalledbyepic;
    private String password;
    private String autosettlement;
    private String simno;
    private String deadlinedate;
    private String remarks;
    private String currency;

    private String user1;
    private String userpass1;
    private String user2;
    private String userpass2;
    private String user3;
    private String userpass3;
    private String user4;
    private String userpass4;
    private String user5;
    private String userpass5;
    private String user6;
    private String userpass6;
    private String user7;
    private String userpass7;
    private String user8;
    private String userpass8;
    private String user9;
    private String userpass9;
    private String user10;
    private String userpass10;
    private String user11;
    private String userpass11;
    private String user12;
    private String userpass12;
    private String user13;
    private String userpass13;
    private String user14;
    private String userpass14;
    private String user15;
    private String userpass15;
    private String user16;
    private String userpass16;
    private String user17;
    private String userpass17;
    private String user18;
    private String userpass18;
    private String user19;
    private String userpass19;
    private String user20;
    private String userpass20;

    private String receipttext;
    private String applicationversion;
    private String visamasteramexconfig;
    private String dcc;
    private String clientcontactperson;
    private String clientemail;
    private String clientcontactno;
    private String merchantcontactperson;
    private String merchantcontactno;
    private String terminalpassword;

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the clienttype
     */
    public String getClienttype() {
        return clienttype;
    }

    /**
     * @param clienttype the clienttype to set
     */
    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territorymap
     */
    public String getTerritorymap() {
        return territorymap;
    }

    /**
     * @param territorymap the territorymap to set
     */
    public void setTerritorymap(String territorymap) {
        this.territorymap = territorymap;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the terminalserial
     */
    public String getTerminalserial() {
        return terminalserial;
    }

    /**
     * @param terminalserial the terminalserial to set
     */
    public void setTerminalserial(String terminalserial) {
        this.terminalserial = terminalserial;
    }

    /**
     * @return the terminalbrand
     */
    public String getTerminalbrand() {
        return terminalbrand;
    }

    /**
     * @param terminalbrand the terminalbrand to set
     */
    public void setTerminalbrand(String terminalbrand) {
        this.terminalbrand = terminalbrand;
    }

    /**
     * @return the terminalmodel
     */
    public String getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(String terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the warrantyexpirystatus
     */
    public String getWarrantyexpirystatus() {
        return warrantyexpirystatus;
    }

    /**
     * @param warrantyexpirystatus the warrantyexpirystatus to set
     */
    public void setWarrantyexpirystatus(String warrantyexpirystatus) {
        this.warrantyexpirystatus = warrantyexpirystatus;
    }

    /**
     * @return the amcaexpirystatus
     */
    public String getAmcaexpirystatus() {
        return amcaexpirystatus;
    }

    /**
     * @param amcaexpirystatus the amcaexpirystatus to set
     */
    public void setAmcaexpirystatus(String amcaexpirystatus) {
        this.amcaexpirystatus = amcaexpirystatus;
    }

    /**
     * @return the lotno
     */
    public String getLotno() {
        return lotno;
    }

    /**
     * @param lotno the lotno to set
     */
    public void setLotno(String lotno) {
        this.lotno = lotno;
    }

    /**
     * @return the partno
     */
    public String getPartno() {
        return partno;
    }

    /**
     * @param partno the partno to set
     */
    public void setPartno(String partno) {
        this.partno = partno;
    }

    /**
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * @return the ptid
     */
    public String getPtid() {
        return ptid;
    }

    /**
     * @param ptid the ptid to set
     */
    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactno
     */
    public String getContactno() {
        return contactno;
    }

    /**
     * @param contactno the contactno to set
     */
    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the deleveryrequirement
     */
    public String getDeleveryrequirement() {
        return deleveryrequirement;
    }

    /**
     * @param deleveryrequirement the deleveryrequirement to set
     */
    public void setDeleveryrequirement(String deleveryrequirement) {
        this.deleveryrequirement = deleveryrequirement;
    }

    /**
     * @return the deleverydestination
     */
    public String getDeleverydestination() {
        return deleverydestination;
    }

    /**
     * @param deleverydestination the deleverydestination to set
     */
    public void setDeleverydestination(String deleverydestination) {
        this.deleverydestination = deleverydestination;
    }

    /**
     * @return the natureoffault
     */
    public String getNatureoffault() {
        return natureoffault;
    }

    /**
     * @param natureoffault the natureoffault to set
     */
    public void setNatureoffault(String natureoffault) {
        this.natureoffault = natureoffault;
    }

    /**
     * @return the actiontobetaken
     */
    public String getActiontobetaken() {
        return actiontobetaken;
    }

    /**
     * @param actiontobetaken the actiontobetaken to set
     */
    public void setActiontobetaken(String actiontobetaken) {
        this.actiontobetaken = actiontobetaken;
    }

    /**
     * @return the causeoffault
     */
    public String getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(String causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the actiontaken
     */
    public String getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the technicalofficerincharge
     */
    public String getTechnicalofficerincharge() {
        return technicalofficerincharge;
    }

    /**
     * @param technicalofficerincharge the technicalofficerincharge to set
     */
    public void setTechnicalofficerincharge(String technicalofficerincharge) {
        this.technicalofficerincharge = technicalofficerincharge;
    }

    /**
     * @return the reportedby
     */
    public String getReportedby() {
        return reportedby;
    }

    /**
     * @param reportedby the reportedby to set
     */
    public void setReportedby(String reportedby) {
        this.reportedby = reportedby;
    }

    /**
     * @return the reportedmerchant
     */
    public String getReportedmerchant() {
        return reportedmerchant;
    }

    /**
     * @param reportedmerchant the reportedmerchant to set
     */
    public void setReportedmerchant(String reportedmerchant) {
        this.reportedmerchant = reportedmerchant;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the btid
     */
    public String getBtid() {
        return btid;
    }

    /**
     * @param btid the btid to set
     */
    public void setBtid(String btid) {
        this.btid = btid;
    }

    /**
     * @return the bmid
     */
    public String getBmid() {
        return bmid;
    }

    /**
     * @param bmid the bmid to set
     */
    public void setBmid(String bmid) {
        this.bmid = bmid;
    }

    /**
     * @return the bterminalserial
     */
    public String getBterminalserial() {
        return bterminalserial;
    }

    /**
     * @param bterminalserial the bterminalserial to set
     */
    public void setBterminalserial(String bterminalserial) {
        this.bterminalserial = bterminalserial;
    }

    /**
     * @return the bterminalbrand
     */
    public String getBterminalbrand() {
        return bterminalbrand;
    }

    /**
     * @param bterminalbrand the bterminalbrand to set
     */
    public void setBterminalbrand(String bterminalbrand) {
        this.bterminalbrand = bterminalbrand;
    }

    /**
     * @return the bterminalmodel
     */
    public String getBterminalmodel() {
        return bterminalmodel;
    }

    /**
     * @param bterminalmodel the bterminalmodel to set
     */
    public void setBterminalmodel(String bterminalmodel) {
        this.bterminalmodel = bterminalmodel;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the deployablestatus
     */
    public int getDeployablestatus() {
        return deployablestatus;
    }

    /**
     * @param deployablestatus the deployablestatus to set
     */
    public void setDeployablestatus(int deployablestatus) {
        this.deployablestatus = deployablestatus;
    }

    /**
     * @return the terminaldeliverystatus
     */
    public int getTerminaldeliverystatus() {
        return terminaldeliverystatus;
    }

    /**
     * @param terminaldeliverystatus the terminaldeliverystatus to set
     */
    public void setTerminaldeliverystatus(int terminaldeliverystatus) {
        this.terminaldeliverystatus = terminaldeliverystatus;
    }

    /**
     * @return the deploymentdate
     */
    public String getDeploymentdate() {
        return deploymentdate;
    }

    /**
     * @param deploymentdate the deploymentdate to set
     */
    public void setDeploymentdate(String deploymentdate) {
        this.deploymentdate = deploymentdate;
    }

    /**
     * @return the merchantacceptancestatus
     */
    public int getMerchantacceptancestatus() {
        return merchantacceptancestatus;
    }

    /**
     * @param merchantacceptancestatus the merchantacceptancestatus to set
     */
    public void setMerchantacceptancestatus(int merchantacceptancestatus) {
        this.merchantacceptancestatus = merchantacceptancestatus;
    }

    /**
     * @return the statusid
     */
    public String getStatusid() {
        return statusid;
    }

    /**
     * @param statusid the statusid to set
     */
    public void setStatusid(String statusid) {
        this.statusid = statusid;
    }

    /**
     * @return the sharingtid
     */
    public String getSharingtid() {
        return sharingtid;
    }

    /**
     * @param sharingtid the sharingtid to set
     */
    public void setSharingtid(String sharingtid) {
        this.sharingtid = sharingtid;
    }

    /**
     * @return the sharingmid
     */
    public String getSharingmid() {
        return sharingmid;
    }

    /**
     * @param sharingmid the sharingmid to set
     */
    public void setSharingmid(String sharingmid) {
        this.sharingmid = sharingmid;
    }

    /**
     * @return the nacno
     */
    public String getNacno() {
        return nacno;
    }

    /**
     * @param nacno the nacno to set
     */
    public void setNacno(String nacno) {
        this.nacno = nacno;
    }

    /**
     * @return the sharingnacno
     */
    public String getSharingnacno() {
        return sharingnacno;
    }

    /**
     * @param sharingnacno the sharingnacno to set
     */
    public void setSharingnacno(String sharingnacno) {
        this.sharingnacno = sharingnacno;
    }

    /**
     * @return the esp
     */
    public String getEsp() {
        return esp;
    }

    /**
     * @param esp the esp to set
     */
    public void setEsp(String esp) {
        this.esp = esp;
    }

    /**
     * @return the profiletype
     */
    public String getProfiletype() {
        return profiletype;
    }

    /**
     * @param profiletype the profiletype to set
     */
    public void setProfiletype(String profiletype) {
        this.profiletype = profiletype;
    }

    /**
     * @return the manualkey
     */
    public String getManualkey() {
        return manualkey;
    }

    /**
     * @param manualkey the manualkey to set
     */
    public void setManualkey(String manualkey) {
        this.manualkey = manualkey;
    }

    /**
     * @return the offline
     */
    public String getOffline() {
        return offline;
    }

    /**
     * @param offline the offline to set
     */
    public void setOffline(String offline) {
        this.offline = offline;
    }

    /**
     * @return the preauth
     */
    public String getPreauth() {
        return preauth;
    }

    /**
     * @param preauth the preauth to set
     */
    public void setPreauth(String preauth) {
        this.preauth = preauth;
    }

    /**
     * @return the tip
     */
    public String getTip() {
        return tip;
    }

    /**
     * @param tip the tip to set
     */
    public void setTip(String tip) {
        this.tip = tip;
    }

    /**
     * @return the cashadvance
     */
    public String getCashadvance() {
        return cashadvance;
    }

    /**
     * @param cashadvance the cashadvance to set
     */
    public void setCashadvance(String cashadvance) {
        this.cashadvance = cashadvance;
    }

    /**
     * @return the panmask
     */
    public String getPanmask() {
        return panmask;
    }

    /**
     * @param panmask the panmask to set
     */
    public void setPanmask(String panmask) {
        this.panmask = panmask;
    }

    /**
     * @return the l4
     */
    public String getL4() {
        return l4;
    }

    /**
     * @param l4 the l4 to set
     */
    public void setL4(String l4) {
        this.l4 = l4;
    }

    /**
     * @return the emv
     */
    public String getEmv() {
        return emv;
    }

    /**
     * @param emv the emv to set
     */
    public void setEmv(String emv) {
        this.emv = emv;
    }

    /**
     * @return the fourdbc
     */
    public String getFourdbc() {
        return fourdbc;
    }

    /**
     * @param fourdbc the fourdbc to set
     */
    public void setFourdbc(String fourdbc) {
        this.fourdbc = fourdbc;
    }

    /**
     * @return the void_
     */
    public String getVoid_() {
        return void_;
    }

    /**
     * @param void_ the void_ to set
     */
    public void setVoid_(String void_) {
        this.void_ = void_;
    }

    /**
     * @return the settlement
     */
    public String getSettlement() {
        return settlement;
    }

    /**
     * @param settlement the settlement to set
     */
    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    /**
     * @return the tobeinstalledbyepic
     */
    public String getTobeinstalledbyepic() {
        return tobeinstalledbyepic;
    }

    /**
     * @param tobeinstalledbyepic the tobeinstalledbyepic to set
     */
    public void setTobeinstalledbyepic(String tobeinstalledbyepic) {
        this.tobeinstalledbyepic = tobeinstalledbyepic;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the autosettlement
     */
    public String getAutosettlement() {
        return autosettlement;
    }

    /**
     * @param autosettlement the autosettlement to set
     */
    public void setAutosettlement(String autosettlement) {
        this.autosettlement = autosettlement;
    }

    /**
     * @return the simno
     */
    public String getSimno() {
        return simno;
    }

    /**
     * @param simno the simno to set
     */
    public void setSimno(String simno) {
        this.simno = simno;
    }

    /**
     * @return the deadlinedate
     */
    public String getDeadlinedate() {
        return deadlinedate;
    }

    /**
     * @param deadlinedate the deadlinedate to set
     */
    public void setDeadlinedate(String deadlinedate) {
        this.deadlinedate = deadlinedate;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the user1
     */
    public String getUser1() {
        return user1;
    }

    /**
     * @param user1 the user1 to set
     */
    public void setUser1(String user1) {
        this.user1 = user1;
    }

    /**
     * @return the userpass1
     */
    public String getUserpass1() {
        return userpass1;
    }

    /**
     * @param userpass1 the userpass1 to set
     */
    public void setUserpass1(String userpass1) {
        this.userpass1 = userpass1;
    }

    /**
     * @return the user2
     */
    public String getUser2() {
        return user2;
    }

    /**
     * @param user2 the user2 to set
     */
    public void setUser2(String user2) {
        this.user2 = user2;
    }

    /**
     * @return the userpass2
     */
    public String getUserpass2() {
        return userpass2;
    }

    /**
     * @param userpass2 the userpass2 to set
     */
    public void setUserpass2(String userpass2) {
        this.userpass2 = userpass2;
    }

    /**
     * @return the user3
     */
    public String getUser3() {
        return user3;
    }

    /**
     * @param user3 the user3 to set
     */
    public void setUser3(String user3) {
        this.user3 = user3;
    }

    /**
     * @return the userpass3
     */
    public String getUserpass3() {
        return userpass3;
    }

    /**
     * @param userpass3 the userpass3 to set
     */
    public void setUserpass3(String userpass3) {
        this.userpass3 = userpass3;
    }

    /**
     * @return the user4
     */
    public String getUser4() {
        return user4;
    }

    /**
     * @param user4 the user4 to set
     */
    public void setUser4(String user4) {
        this.user4 = user4;
    }

    /**
     * @return the userpass4
     */
    public String getUserpass4() {
        return userpass4;
    }

    /**
     * @param userpass4 the userpass4 to set
     */
    public void setUserpass4(String userpass4) {
        this.userpass4 = userpass4;
    }

    /**
     * @return the user5
     */
    public String getUser5() {
        return user5;
    }

    /**
     * @param user5 the user5 to set
     */
    public void setUser5(String user5) {
        this.user5 = user5;
    }

    /**
     * @return the userpass5
     */
    public String getUserpass5() {
        return userpass5;
    }

    /**
     * @param userpass5 the userpass5 to set
     */
    public void setUserpass5(String userpass5) {
        this.userpass5 = userpass5;
    }

    /**
     * @return the user6
     */
    public String getUser6() {
        return user6;
    }

    /**
     * @param user6 the user6 to set
     */
    public void setUser6(String user6) {
        this.user6 = user6;
    }

    /**
     * @return the userpass6
     */
    public String getUserpass6() {
        return userpass6;
    }

    /**
     * @param userpass6 the userpass6 to set
     */
    public void setUserpass6(String userpass6) {
        this.userpass6 = userpass6;
    }

    /**
     * @return the user7
     */
    public String getUser7() {
        return user7;
    }

    /**
     * @param user7 the user7 to set
     */
    public void setUser7(String user7) {
        this.user7 = user7;
    }

    /**
     * @return the userpass7
     */
    public String getUserpass7() {
        return userpass7;
    }

    /**
     * @param userpass7 the userpass7 to set
     */
    public void setUserpass7(String userpass7) {
        this.userpass7 = userpass7;
    }

    /**
     * @return the user8
     */
    public String getUser8() {
        return user8;
    }

    /**
     * @param user8 the user8 to set
     */
    public void setUser8(String user8) {
        this.user8 = user8;
    }

    /**
     * @return the userpass8
     */
    public String getUserpass8() {
        return userpass8;
    }

    /**
     * @param userpass8 the userpass8 to set
     */
    public void setUserpass8(String userpass8) {
        this.userpass8 = userpass8;
    }

    /**
     * @return the user9
     */
    public String getUser9() {
        return user9;
    }

    /**
     * @param user9 the user9 to set
     */
    public void setUser9(String user9) {
        this.user9 = user9;
    }

    /**
     * @return the userpass9
     */
    public String getUserpass9() {
        return userpass9;
    }

    /**
     * @param userpass9 the userpass9 to set
     */
    public void setUserpass9(String userpass9) {
        this.userpass9 = userpass9;
    }

    /**
     * @return the user10
     */
    public String getUser10() {
        return user10;
    }

    /**
     * @param user10 the user10 to set
     */
    public void setUser10(String user10) {
        this.user10 = user10;
    }

    /**
     * @return the userpass10
     */
    public String getUserpass10() {
        return userpass10;
    }

    /**
     * @param userpass10 the userpass10 to set
     */
    public void setUserpass10(String userpass10) {
        this.userpass10 = userpass10;
    }

    /**
     * @return the user11
     */
    public String getUser11() {
        return user11;
    }

    /**
     * @param user11 the user11 to set
     */
    public void setUser11(String user11) {
        this.user11 = user11;
    }

    /**
     * @return the userpass11
     */
    public String getUserpass11() {
        return userpass11;
    }

    /**
     * @param userpass11 the userpass11 to set
     */
    public void setUserpass11(String userpass11) {
        this.userpass11 = userpass11;
    }

    /**
     * @return the user12
     */
    public String getUser12() {
        return user12;
    }

    /**
     * @param user12 the user12 to set
     */
    public void setUser12(String user12) {
        this.user12 = user12;
    }

    /**
     * @return the userpass12
     */
    public String getUserpass12() {
        return userpass12;
    }

    /**
     * @param userpass12 the userpass12 to set
     */
    public void setUserpass12(String userpass12) {
        this.userpass12 = userpass12;
    }

    /**
     * @return the user13
     */
    public String getUser13() {
        return user13;
    }

    /**
     * @param user13 the user13 to set
     */
    public void setUser13(String user13) {
        this.user13 = user13;
    }

    /**
     * @return the userpass13
     */
    public String getUserpass13() {
        return userpass13;
    }

    /**
     * @param userpass13 the userpass13 to set
     */
    public void setUserpass13(String userpass13) {
        this.userpass13 = userpass13;
    }

    /**
     * @return the user14
     */
    public String getUser14() {
        return user14;
    }

    /**
     * @param user14 the user14 to set
     */
    public void setUser14(String user14) {
        this.user14 = user14;
    }

    /**
     * @return the userpass14
     */
    public String getUserpass14() {
        return userpass14;
    }

    /**
     * @param userpass14 the userpass14 to set
     */
    public void setUserpass14(String userpass14) {
        this.userpass14 = userpass14;
    }

    /**
     * @return the user15
     */
    public String getUser15() {
        return user15;
    }

    /**
     * @param user15 the user15 to set
     */
    public void setUser15(String user15) {
        this.user15 = user15;
    }

    /**
     * @return the userpass15
     */
    public String getUserpass15() {
        return userpass15;
    }

    /**
     * @param userpass15 the userpass15 to set
     */
    public void setUserpass15(String userpass15) {
        this.userpass15 = userpass15;
    }

    /**
     * @return the user16
     */
    public String getUser16() {
        return user16;
    }

    /**
     * @param user16 the user16 to set
     */
    public void setUser16(String user16) {
        this.user16 = user16;
    }

    /**
     * @return the userpass16
     */
    public String getUserpass16() {
        return userpass16;
    }

    /**
     * @param userpass16 the userpass16 to set
     */
    public void setUserpass16(String userpass16) {
        this.userpass16 = userpass16;
    }

    /**
     * @return the user17
     */
    public String getUser17() {
        return user17;
    }

    /**
     * @param user17 the user17 to set
     */
    public void setUser17(String user17) {
        this.user17 = user17;
    }

    /**
     * @return the userpass17
     */
    public String getUserpass17() {
        return userpass17;
    }

    /**
     * @param userpass17 the userpass17 to set
     */
    public void setUserpass17(String userpass17) {
        this.userpass17 = userpass17;
    }

    /**
     * @return the user18
     */
    public String getUser18() {
        return user18;
    }

    /**
     * @param user18 the user18 to set
     */
    public void setUser18(String user18) {
        this.user18 = user18;
    }

    /**
     * @return the userpass18
     */
    public String getUserpass18() {
        return userpass18;
    }

    /**
     * @param userpass18 the userpass18 to set
     */
    public void setUserpass18(String userpass18) {
        this.userpass18 = userpass18;
    }

    /**
     * @return the user19
     */
    public String getUser19() {
        return user19;
    }

    /**
     * @param user19 the user19 to set
     */
    public void setUser19(String user19) {
        this.user19 = user19;
    }

    /**
     * @return the userpass19
     */
    public String getUserpass19() {
        return userpass19;
    }

    /**
     * @param userpass19 the userpass19 to set
     */
    public void setUserpass19(String userpass19) {
        this.userpass19 = userpass19;
    }

    /**
     * @return the user20
     */
    public String getUser20() {
        return user20;
    }

    /**
     * @param user20 the user20 to set
     */
    public void setUser20(String user20) {
        this.user20 = user20;
    }

    /**
     * @return the userpass20
     */
    public String getUserpass20() {
        return userpass20;
    }

    /**
     * @param userpass20 the userpass20 to set
     */
    public void setUserpass20(String userpass20) {
        this.userpass20 = userpass20;
    }

    /**
     * @return the receipttext
     */
    public String getReceipttext() {
        return receipttext;
    }

    /**
     * @param receipttext the receipttext to set
     */
    public void setReceipttext(String receipttext) {
        this.receipttext = receipttext;
    }

    /**
     * @return the applicationversion
     */
    public String getApplicationversion() {
        return applicationversion;
    }

    /**
     * @param applicationversion the applicationversion to set
     */
    public void setApplicationversion(String applicationversion) {
        this.applicationversion = applicationversion;
    }

    /**
     * @return the visamasteramexconfig
     */
    public String getVisamasteramexconfig() {
        return visamasteramexconfig;
    }

    /**
     * @param visamasteramexconfig the visamasteramexconfig to set
     */
    public void setVisamasteramexconfig(String visamasteramexconfig) {
        this.visamasteramexconfig = visamasteramexconfig;
    }

    /**
     * @return the dcc
     */
    public String getDcc() {
        return dcc;
    }

    /**
     * @param dcc the dcc to set
     */
    public void setDcc(String dcc) {
        this.dcc = dcc;
    }

    /**
     * @return the clientcontactperson
     */
    public String getClientcontactperson() {
        return clientcontactperson;
    }

    /**
     * @param clientcontactperson the clientcontactperson to set
     */
    public void setClientcontactperson(String clientcontactperson) {
        this.clientcontactperson = clientcontactperson;
    }

    /**
     * @return the clientemail
     */
    public String getClientemail() {
        return clientemail;
    }

    /**
     * @param clientemail the clientemail to set
     */
    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }

    /**
     * @return the clientcontactno
     */
    public String getClientcontactno() {
        return clientcontactno;
    }

    /**
     * @param clientcontactno the clientcontactno to set
     */
    public void setClientcontactno(String clientcontactno) {
        this.clientcontactno = clientcontactno;
    }

    /**
     * @return the merchantcontactperson
     */
    public String getMerchantcontactperson() {
        return merchantcontactperson;
    }

    /**
     * @param merchantcontactperson the merchantcontactperson to set
     */
    public void setMerchantcontactperson(String merchantcontactperson) {
        this.merchantcontactperson = merchantcontactperson;
    }

    /**
     * @return the merchantcontactno
     */
    public String getMerchantcontactno() {
        return merchantcontactno;
    }

    /**
     * @param merchantcontactno the merchantcontactno to set
     */
    public void setMerchantcontactno(String merchantcontactno) {
        this.merchantcontactno = merchantcontactno;
    }

    /**
     * @return the terminalpassword
     */
    public String getTerminalpassword() {
        return terminalpassword;
    }

    /**
     * @param terminalpassword the terminalpassword to set
     */
    public void setTerminalpassword(String terminalpassword) {
        this.terminalpassword = terminalpassword;
    }

}
