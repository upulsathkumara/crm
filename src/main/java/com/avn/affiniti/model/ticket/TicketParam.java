/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Author : KAsun Udayanaga
 * @Document : TicketParam
 * @Created on : Sep 13, 2017, 6:05:21 PM
 */
public class TicketParam {

    private int ticketid;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ticketdate;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String territory;
    private String ticketcategory;
    private String tid;
    private String mid;
    private String terminalserialno;
    private String terminalbrnad;
    private String terminalmodel;
    private String warrantyexpirystatus;
    private String amcexpirystatus;
    private String lotno;
    private String partno;
    private String revision;
    private String mac;
    private String ptid;
    private String bank;
    private String merchantname;
    private String contactperson;
    private String contactno;
    private String district;
    private String locationaddress;
    private String deliveryrequirement;
    private String deliverydestination;
    private String natureoffault;
    private String actiontobetaken;
    private String causeoffault;
    private String actiontaken;
    private String technicalofficerincharge;
    private String reportedby;
    private String reportedmerchant;
    private String createdby;
    private String status;
    private String statuscode;
    private String createduser;
    /*Terminal Brakdown Ticket*/
    private String isusernegligence;
    private String islightening;
    private String iscoveredunderwarranty;
    private String isunderamc;
    /*Terminal Brakdown Ticket :: Courier*/
    private String damagepartdescription;
    private String replacedpart;
    private String parttype;
    private String partnumber;
    private String partnumberofterminal;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date completiondatetime;
    private String totletimespent;
    private String specialremark;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dispatchdate;
    private String courierserviceprovider;
    private String trackingnumber;
    /*Terminal Brakdown Ticket :: Quotation*/
    private String terminalquotationid;
    private String recipientbank;
    private String qterminalserialno;
    private String qterminalmodel;
    private String merchantlocation;
    private String replacementpartno;
    private String replacementdescription;
    private String unitprice;
    private String quantity;
    private String total;
    private String qcauseoffault;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date quatationvalidityperiod;
    private boolean quotation;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date deploymentdate;
    private String input;

    private long assignee;
    private int usreRoleId;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the ticketdate
     */
    public Date getTicketdate() {
        return ticketdate;
    }

    /**
     * @param ticketdate the ticketdate to set
     */
    public void setTicketdate(Date ticketdate) {
        this.ticketdate = ticketdate;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territory
     */
    public String getTerritory() {
        return territory;
    }

    /**
     * @param territory the territory to set
     */
    public void setTerritory(String territory) {
        this.territory = territory;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the terminalserialno
     */
    public String getTerminalserialno() {
        return terminalserialno;
    }

    /**
     * @param terminalserialno the terminalserialno to set
     */
    public void setTerminalserialno(String terminalserialno) {
        this.terminalserialno = terminalserialno;
    }

    /**
     * @return the terminalbrnad
     */
    public String getTerminalbrnad() {
        return terminalbrnad;
    }

    /**
     * @param terminalbrnad the terminalbrnad to set
     */
    public void setTerminalbrnad(String terminalbrnad) {
        this.terminalbrnad = terminalbrnad;
    }

    /**
     * @return the terminalmodel
     */
    public String getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(String terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the warrantyexpirystatus
     */
    public String getWarrantyexpirystatus() {
        return warrantyexpirystatus;
    }

    /**
     * @param warrantyexpirystatus the warrantyexpirystatus to set
     */
    public void setWarrantyexpirystatus(String warrantyexpirystatus) {
        this.warrantyexpirystatus = warrantyexpirystatus;
    }

    /**
     * @return the amcexpirystatus
     */
    public String getAmcexpirystatus() {
        return amcexpirystatus;
    }

    /**
     * @param amcexpirystatus the amcexpirystatus to set
     */
    public void setAmcexpirystatus(String amcexpirystatus) {
        this.amcexpirystatus = amcexpirystatus;
    }

    /**
     * @return the lotno
     */
    public String getLotno() {
        return lotno;
    }

    /**
     * @param lotno the lotno to set
     */
    public void setLotno(String lotno) {
        this.lotno = lotno;
    }

    /**
     * @return the partno
     */
    public String getPartno() {
        return partno;
    }

    /**
     * @param partno the partno to set
     */
    public void setPartno(String partno) {
        this.partno = partno;
    }

    /**
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * @return the ptid
     */
    public String getPtid() {
        return ptid;
    }

    /**
     * @param ptid the ptid to set
     */
    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactno
     */
    public String getContactno() {
        return contactno;
    }

    /**
     * @param contactno the contactno to set
     */
    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the deliveryrequirement
     */
    public String getDeliveryrequirement() {
        return deliveryrequirement;
    }

    /**
     * @param deliveryrequirement the deliveryrequirement to set
     */
    public void setDeliveryrequirement(String deliveryrequirement) {
        this.deliveryrequirement = deliveryrequirement;
    }

    /**
     * @return the deliverydestination
     */
    public String getDeliverydestination() {
        return deliverydestination;
    }

    /**
     * @param deliverydestination the deliverydestination to set
     */
    public void setDeliverydestination(String deliverydestination) {
        this.deliverydestination = deliverydestination;
    }

    /**
     * @return the natureoffault
     */
    public String getNatureoffault() {
        return natureoffault;
    }

    /**
     * @param natureoffault the natureoffault to set
     */
    public void setNatureoffault(String natureoffault) {
        this.natureoffault = natureoffault;
    }

    /**
     * @return the actiontobetaken
     */
    public String getActiontobetaken() {
        return actiontobetaken;
    }

    /**
     * @param actiontobetaken the actiontobetaken to set
     */
    public void setActiontobetaken(String actiontobetaken) {
        this.actiontobetaken = actiontobetaken;
    }

    /**
     * @return the causeoffault
     */
    public String getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(String causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the actiontaken
     */
    public String getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the technicalofficerincharge
     */
    public String getTechnicalofficerincharge() {
        return technicalofficerincharge;
    }

    /**
     * @param technicalofficerincharge the technicalofficerincharge to set
     */
    public void setTechnicalofficerincharge(String technicalofficerincharge) {
        this.technicalofficerincharge = technicalofficerincharge;
    }

    /**
     * @return the reportedby
     */
    public String getReportedby() {
        return reportedby;
    }

    /**
     * @param reportedby the reportedby to set
     */
    public void setReportedby(String reportedby) {
        this.reportedby = reportedby;
    }

    /**
     * @return the reportedmerchant
     */
    public String getReportedmerchant() {
        return reportedmerchant;
    }

    /**
     * @param reportedmerchant the reportedmerchant to set
     */
    public void setReportedmerchant(String reportedmerchant) {
        this.reportedmerchant = reportedmerchant;
    }

    /**
     * @return the createdby
     */
    public String getCreatedby() {
        return createdby;
    }

    /**
     * @param createdby the createdby to set
     */
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the statuscode
     */
    public String getStatuscode() {
        return statuscode;
    }

    /**
     * @param statuscode the statuscode to set
     */
    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the isusernegligence
     */
    public String getIsusernegligence() {
        return isusernegligence;
    }

    /**
     * @param isusernegligence the isusernegligence to set
     */
    public void setIsusernegligence(String isusernegligence) {
        this.isusernegligence = isusernegligence;
    }

    /**
     * @return the islightening
     */
    public String getIslightening() {
        return islightening;
    }

    /**
     * @param islightening the islightening to set
     */
    public void setIslightening(String islightening) {
        this.islightening = islightening;
    }

    /**
     * @return the iscoveredunderwarranty
     */
    public String getIscoveredunderwarranty() {
        return iscoveredunderwarranty;
    }

    /**
     * @param iscoveredunderwarranty the iscoveredunderwarranty to set
     */
    public void setIscoveredunderwarranty(String iscoveredunderwarranty) {
        this.iscoveredunderwarranty = iscoveredunderwarranty;
    }

    /**
     * @return the isunderamc
     */
    public String getIsunderamc() {
        return isunderamc;
    }

    /**
     * @param isunderamc the isunderamc to set
     */
    public void setIsunderamc(String isunderamc) {
        this.isunderamc = isunderamc;
    }

    /**
     * @return the damagepartdescription
     */
    public String getDamagepartdescription() {
        return damagepartdescription;
    }

    /**
     * @param damagepartdescription the damagepartdescription to set
     */
    public void setDamagepartdescription(String damagepartdescription) {
        this.damagepartdescription = damagepartdescription;
    }

    /**
     * @return the replacedpart
     */
    public String getReplacedpart() {
        return replacedpart;
    }

    /**
     * @param replacedpart the replacedpart to set
     */
    public void setReplacedpart(String replacedpart) {
        this.replacedpart = replacedpart;
    }

    /**
     * @return the parttype
     */
    public String getParttype() {
        return parttype;
    }

    /**
     * @param parttype the parttype to set
     */
    public void setParttype(String parttype) {
        this.parttype = parttype;
    }

    /**
     * @return the partnumber
     */
    public String getPartnumber() {
        return partnumber;
    }

    /**
     * @param partnumber the partnumber to set
     */
    public void setPartnumber(String partnumber) {
        this.partnumber = partnumber;
    }

    /**
     * @return the partnumberofterminal
     */
    public String getPartnumberofterminal() {
        return partnumberofterminal;
    }

    /**
     * @param partnumberofterminal the partnumberofterminal to set
     */
    public void setPartnumberofterminal(String partnumberofterminal) {
        this.partnumberofterminal = partnumberofterminal;
    }

    /**
     * @return the completiondatetime
     */
    public Date getCompletiondatetime() {
        return completiondatetime;
    }

    /**
     * @param completiondatetime the completiondatetime to set
     */
    public void setCompletiondatetime(Date completiondatetime) {
        this.completiondatetime = completiondatetime;
    }

    /**
     * @return the totletimespent
     */
    public String getTotletimespent() {
        return totletimespent;
    }

    /**
     * @param totletimespent the totletimespent to set
     */
    public void setTotletimespent(String totletimespent) {
        this.totletimespent = totletimespent;
    }

    /**
     * @return the specialremark
     */
    public String getSpecialremark() {
        return specialremark;
    }

    /**
     * @param specialremark the specialremark to set
     */
    public void setSpecialremark(String specialremark) {
        this.specialremark = specialremark;
    }

    /**
     * @return the dispatchdate
     */
    public Date getDispatchdate() {
        return dispatchdate;
    }

    /**
     * @param dispatchdate the dispatchdate to set
     */
    public void setDispatchdate(Date dispatchdate) {
        this.dispatchdate = dispatchdate;
    }

    /**
     * @return the courierserviceprovider
     */
    public String getCourierserviceprovider() {
        return courierserviceprovider;
    }

    /**
     * @param courierserviceprovider the courierserviceprovider to set
     */
    public void setCourierserviceprovider(String courierserviceprovider) {
        this.courierserviceprovider = courierserviceprovider;
    }

    /**
     * @return the trackingnumber
     */
    public String getTrackingnumber() {
        return trackingnumber;
    }

    /**
     * @param trackingnumber the trackingnumber to set
     */
    public void setTrackingnumber(String trackingnumber) {
        this.trackingnumber = trackingnumber;
    }

    /**
     * @return the terminalquotationid
     */
    public String getTerminalquotationid() {
        return terminalquotationid;
    }

    /**
     * @param terminalquotationid the terminalquotationid to set
     */
    public void setTerminalquotationid(String terminalquotationid) {
        this.terminalquotationid = terminalquotationid;
    }

    /**
     * @return the recipientbank
     */
    public String getRecipientbank() {
        return recipientbank;
    }

    /**
     * @param recipientbank the recipientbank to set
     */
    public void setRecipientbank(String recipientbank) {
        this.recipientbank = recipientbank;
    }

    /**
     * @return the qterminalserialno
     */
    public String getQterminalserialno() {
        return qterminalserialno;
    }

    /**
     * @param qterminalserialno the qterminalserialno to set
     */
    public void setQterminalserialno(String qterminalserialno) {
        this.qterminalserialno = qterminalserialno;
    }

    /**
     * @return the qterminalmodel
     */
    public String getQterminalmodel() {
        return qterminalmodel;
    }

    /**
     * @param qterminalmodel the qterminalmodel to set
     */
    public void setQterminalmodel(String qterminalmodel) {
        this.qterminalmodel = qterminalmodel;
    }

    /**
     * @return the merchantlocation
     */
    public String getMerchantlocation() {
        return merchantlocation;
    }

    /**
     * @param merchantlocation the merchantlocation to set
     */
    public void setMerchantlocation(String merchantlocation) {
        this.merchantlocation = merchantlocation;
    }

    /**
     * @return the replacementpartno
     */
    public String getReplacementpartno() {
        return replacementpartno;
    }

    /**
     * @param replacementpartno the replacementpartno to set
     */
    public void setReplacementpartno(String replacementpartno) {
        this.replacementpartno = replacementpartno;
    }

    /**
     * @return the replacementdescription
     */
    public String getReplacementdescription() {
        return replacementdescription;
    }

    /**
     * @param replacementdescription the replacementdescription to set
     */
    public void setReplacementdescription(String replacementdescription) {
        this.replacementdescription = replacementdescription;
    }

    /**
     * @return the unitprice
     */
    public String getUnitprice() {
        return unitprice;
    }

    /**
     * @param unitprice the unitprice to set
     */
    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * @return the qcauseoffault
     */
    public String getQcauseoffault() {
        return qcauseoffault;
    }

    /**
     * @param qcauseoffault the qcauseoffault to set
     */
    public void setQcauseoffault(String qcauseoffault) {
        this.qcauseoffault = qcauseoffault;
    }

    /**
     * @return the quatationvalidityperiod
     */
    public Date getQuatationvalidityperiod() {
        return quatationvalidityperiod;
    }

    /**
     * @param quatationvalidityperiod the quatationvalidityperiod to set
     */
    public void setQuatationvalidityperiod(Date quatationvalidityperiod) {
        this.quatationvalidityperiod = quatationvalidityperiod;
    }

    /**
     * @return the quotation
     */
    public boolean isQuotation() {
        return quotation;
    }

    /**
     * @param quotation the quotation to set
     */
    public void setQuotation(boolean quotation) {
        this.quotation = quotation;
    }

    /**
     * @return the deploymentdate
     */
    public Date getDeploymentdate() {
        return deploymentdate;
    }

    /**
     * @param deploymentdate the deploymentdate to set
     */
    public void setDeploymentdate(Date deploymentdate) {
        this.deploymentdate = deploymentdate;
    }

    /**
     * @return the input
     */
    public String getInput() {
        return input;
    }

    /**
     * @param input the input to set
     */
    public void setInput(String input) {
        this.input = input;
    }

    /**
     * @return the assignee
     */
    public long getAssignee() {
        return assignee;
    }

    /**
     * @param assignee the assignee to set
     */
    public void setAssignee(long assignee) {
        this.assignee = assignee;
    }

    /**
     * @return the usreRoleId
     */
    public int getUsreRoleId() {
        return usreRoleId;
    }

    /**
     * @param usreRoleId the usreRoleId to set
     */
    public void setUsreRoleId(int usreRoleId) {
        this.usreRoleId = usreRoleId;
    }

}
