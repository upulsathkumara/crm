/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

/**
 * @Author : KAsun Udayanaga
 * @Document : SoftwareHardwareBreakdownTicket
 * @Created on : Sep 21, 2017, 2:14:19 PM
 */
public class SoftwareHardwareBreakdownTicket {

    private int ticketid;
    private String client;
    private String clienttype;
    private String district;
    private String ticketdatetime;
    private String ticketpriority;
    private String product;
    private String productcategory;
    private String territorymap;
    private int ticketcategoryid;
    private String ticketcategory;
    private String natureoffault;
    private String causeoffault;
    private String statusid;

    private String terminalserialno;
    private String terminalbrnad;
    private String terminalmodel;
    private String warrantyexpirystatus;
    private String amcexpirystatus;
    private String merchantname;
    private String contactperson;
    private String contactno;
    private String locationaddress;
    private String actiontobetaken;
    private String technicalofficerincharge;
    private String reportedby;
    private String createdby;
    private String aditionalcosts;
    private String backupdeviceserial;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the clienttype
     */
    public String getClienttype() {
        return clienttype;
    }

    /**
     * @param clienttype the clienttype to set
     */
    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    /**
     * @return the district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return the ticketdatetime
     */
    public String getTicketdatetime() {
        return ticketdatetime;
    }

    /**
     * @param ticketdatetime the ticketdatetime to set
     */
    public void setTicketdatetime(String ticketdatetime) {
        this.ticketdatetime = ticketdatetime;
    }

    /**
     * @return the ticketpriority
     */
    public String getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(String ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public String getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(String productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territorymap
     */
    public String getTerritorymap() {
        return territorymap;
    }

    /**
     * @param territorymap the territorymap to set
     */
    public void setTerritorymap(String territorymap) {
        this.territorymap = territorymap;
    }

    /**
     * @return the ticketcategoryid
     */
    public int getTicketcategoryid() {
        return ticketcategoryid;
    }

    /**
     * @param ticketcategoryid the ticketcategoryid to set
     */
    public void setTicketcategoryid(int ticketcategoryid) {
        this.ticketcategoryid = ticketcategoryid;
    }

    /**
     * @return the ticketcategory
     */
    public String getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(String ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the natureoffault
     */
    public String getNatureoffault() {
        return natureoffault;
    }

    /**
     * @param natureoffault the natureoffault to set
     */
    public void setNatureoffault(String natureoffault) {
        this.natureoffault = natureoffault;
    }

    /**
     * @return the statusid
     */
    public String getStatusid() {
        return statusid;
    }

    /**
     * @param statusid the statusid to set
     */
    public void setStatusid(String statusid) {
        this.statusid = statusid;
    }

    /**
     * @return the terminalserialno
     */
    public String getTerminalserialno() {
        return terminalserialno;
    }

    /**
     * @param terminalserialno the terminalserialno to set
     */
    public void setTerminalserialno(String terminalserialno) {
        this.terminalserialno = terminalserialno;
    }

    /**
     * @return the terminalbrnad
     */
    public String getTerminalbrnad() {
        return terminalbrnad;
    }

    /**
     * @param terminalbrnad the terminalbrnad to set
     */
    public void setTerminalbrnad(String terminalbrnad) {
        this.terminalbrnad = terminalbrnad;
    }

    /**
     * @return the terminalmodel
     */
    public String getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(String terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the warrantyexpirystatus
     */
    public String getWarrantyexpirystatus() {
        return warrantyexpirystatus;
    }

    /**
     * @param warrantyexpirystatus the warrantyexpirystatus to set
     */
    public void setWarrantyexpirystatus(String warrantyexpirystatus) {
        this.warrantyexpirystatus = warrantyexpirystatus;
    }

    /**
     * @return the amcexpirystatus
     */
    public String getAmcexpirystatus() {
        return amcexpirystatus;
    }

    /**
     * @param amcexpirystatus the amcexpirystatus to set
     */
    public void setAmcexpirystatus(String amcexpirystatus) {
        this.amcexpirystatus = amcexpirystatus;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactno
     */
    public String getContactno() {
        return contactno;
    }

    /**
     * @param contactno the contactno to set
     */
    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the actiontobetaken
     */
    public String getActiontobetaken() {
        return actiontobetaken;
    }

    /**
     * @param actiontobetaken the actiontobetaken to set
     */
    public void setActiontobetaken(String actiontobetaken) {
        this.actiontobetaken = actiontobetaken;
    }

    /**
     * @return the technicalofficerincharge
     */
    public String getTechnicalofficerincharge() {
        return technicalofficerincharge;
    }

    /**
     * @param technicalofficerincharge the technicalofficerincharge to set
     */
    public void setTechnicalofficerincharge(String technicalofficerincharge) {
        this.technicalofficerincharge = technicalofficerincharge;
    }

    /**
     * @return the reportedby
     */
    public String getReportedby() {
        return reportedby;
    }

    /**
     * @param reportedby the reportedby to set
     */
    public void setReportedby(String reportedby) {
        this.reportedby = reportedby;
    }

    /**
     * @return the createdby
     */
    public String getCreatedby() {
        return createdby;
    }

    /**
     * @param createdby the createdby to set
     */
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    /**
     * @return the aditionalcosts
     */
    public String getAditionalcosts() {
        return aditionalcosts;
    }

    /**
     * @param aditionalcosts the aditionalcosts to set
     */
    public void setAditionalcosts(String aditionalcosts) {
        this.aditionalcosts = aditionalcosts;
    }

    /**
     * @return the backupdeviceserial
     */
    public String getBackupdeviceserial() {
        return backupdeviceserial;
    }

    /**
     * @param backupdeviceserial the backupdeviceserial to set
     */
    public void setBackupdeviceserial(String backupdeviceserial) {
        this.backupdeviceserial = backupdeviceserial;
    }
    
    /**
     * @return the causeoffault
     */
    public String getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(String causeoffault) {
        this.causeoffault = causeoffault;
    }

}
