/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.ticket;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author : Roshen Dilshan
 * @Document : Ticket
 * @Created on : Jun 12, 2017, 11:52:36 PM
 */
public class Ticket {

    private int ticketid;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ticketdate;
    private Integer ticketpriority;
    private Integer product;
    private Integer productcategory;
    private Integer territory;
    private Integer ticketcategory;
    private String tid;
    private String mid;
    private String boxnumber;
    private String startserialno;
    private String endserialno;
    private String terminalserialno;
    private Integer terminalbrnad;
    private Integer terminalmodel;
    private String warrantyexpirystatus;
    private String amcexpirystatus;
    private String lotno;
    private String partno;
    private String revision;
    private String mac;
    private String ptid;
    private Integer bank;
    private Integer client;
    private Integer clientcategory;
    private String merchantname;
    private String contactperson;
    private String contactno;
    private Integer district;
    private String locationaddress;
    private Integer deliveryrequirement;
    private String deliverydestination;
    private Integer natureoffault;
    private Integer actiontobetaken;
    private Integer causeoffault;
    private Integer actiontaken;
    private String technicalofficerincharge;
    private Integer reportedby;
    private String reportedmerchant;
    private String createdby;
    private Integer status;
    private Integer inventoryhardwareitem;
    private String createduser;
    private String assignee;
    private String manualassigneestatus;
    /*Terminal Brakdown Ticket*/
    private Integer isusernegligence;
    private Integer islightening;
    private Integer iscoveredunderwarranty;
    private Integer isunderamc;
    /*Terminal Brakdown Ticket :: Courier*/
    private String damagepartdescription;
    private String replacedpart;
    private String parttype;
    private String partnumber;
    private String partnumberofterminal;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date completiondatetime;
    private String totletimespent;
    private String specialremark;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dispatchdate;
    private String courierserviceprovider;
    private String trackingnumber;
    /*Terminal Brakdown Ticket :: Quotation*/
    private Integer terminalquotationid;
    private String recipientbank;
    private String qterminalserialno;
    private String qterminalmodel;
    private String merchantlocation;
    private String replacementpartno;
    private String replacementdescription;
    private String unitprice;
    private String quantity;
    private String total;
    private Integer qcauseoffault;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date quatationvalidityperiod;
    private boolean quotation;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date deploymentdate;
    private String input;
    /*Bulk terminal breakdown ticket*/
    private String filepath;
    private String action;
    private MultipartFile file;
    /*Merchant Removal Ticket*/
    private Integer terminaldeliverystatus;
    /*Terminal Sharing*/
    private String currency;
    private String description;
    /*Terminal Conversion*/
    private String newsoftwareversion;
    /*Terminal Repair Malayasia*/
    private String deliveryorderref;
    private String rmareferance;
    private String destination;
    private String invoicenumber;
    private Double invoicevalue;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date invoicedate;
    private String bkplocationaddress;
    private Integer purposetype;
    private int assigneetype;
    private int notificationlevel;
    private String resolutiondescription;
    private String amcperiod;
    private String amcpaymentplan;
    private String agreementstatus;
    private String vmtid;
    private String vmmid;
    private String amextid;
    private String amexmid;
    private String sharingtid;
    private String sharingmid;
    private Integer nacno;
    private Integer sharingnacno;
    private Integer esp;
    private Integer profiletype;
    private Integer manualkey;
    private Integer offline;
    private Integer preauth;
    private Integer tip;
    private Integer cashadvance;
    private Integer panmask;
    private Integer l4;
    private Integer emv;
    private Integer fourdbc;
    private Integer void_;
    private Integer settlement;
    private Integer tobeinstalledbyepic;
    private Integer password;
    private Integer autosettlement;
    private String simno;
    private Date deadlinedate;
    private String aditionalcosts;
    private String backupdeviceserial;
    private String remarks;

    private String user1;
    private String userpass1;
    private String user2;
    private String userpass2;
    private String user3;
    private String userpass3;
    private String user4;
    private String userpass4;
    private String user5;
    private String userpass5;
    private String user6;
    private String userpass6;
    private String user7;
    private String userpass7;
    private String user8;
    private String userpass8;
    private String user9;
    private String userpass9;
    private String user10;
    private String userpass10;
    private String user11;
    private String userpass11;
    private String user12;
    private String userpass12;
    private String user13;
    private String userpass13;
    private String user14;
    private String userpass14;
    private String user15;
    private String userpass15;
    private String user16;
    private String userpass16;
    private String user17;
    private String userpass17;
    private String user18;
    private String userpass18;
    private String user19;
    private String userpass19;
    private String user20;
    private String userpass20;

    private String receipttext;
    private String applicationversion;
    private String visamasteramexconfig;
    private String dcc;
    private String clientcontactperson;
    private String clientemail;
    private String clientcontactno;
    private String merchantcontactperson;
    private String merchantcontactno;
    private String terminalpassword;

    /**
     * @return the ticketid
     */
    public int getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @return the ticketdate
     */
    public Date getTicketdate() {
        return ticketdate;
    }

    /**
     * @param ticketdate the ticketdate to set
     */
    public void setTicketdate(Date ticketdate) {
        this.ticketdate = ticketdate;
    }

    /**
     * @return the ticketpriority
     */
    public Integer getTicketpriority() {
        return ticketpriority;
    }

    /**
     * @param ticketpriority the ticketpriority to set
     */
    public void setTicketpriority(Integer ticketpriority) {
        this.ticketpriority = ticketpriority;
    }

    /**
     * @return the product
     */
    public Integer getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Integer product) {
        this.product = product;
    }

    /**
     * @return the productcategory
     */
    public Integer getProductcategory() {
        return productcategory;
    }

    /**
     * @param productcategory the productcategory to set
     */
    public void setProductcategory(Integer productcategory) {
        this.productcategory = productcategory;
    }

    /**
     * @return the territory
     */
    public Integer getTerritory() {
        return territory;
    }

    /**
     * @param territory the territory to set
     */
    public void setTerritory(Integer territory) {
        this.territory = territory;
    }

    /**
     * @return the ticketcategory
     */
    public Integer getTicketcategory() {
        return ticketcategory;
    }

    /**
     * @param ticketcategory the ticketcategory to set
     */
    public void setTicketcategory(Integer ticketcategory) {
        this.ticketcategory = ticketcategory;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the boxnumber
     */
    public String getBoxnumber() {
        return boxnumber;
    }

    /**
     * @param boxnumber the boxnumber to set
     */
    public void setBoxnumber(String boxnumber) {
        this.boxnumber = boxnumber;
    }

    /**
     * @return the startserialno
     */
    public String getStartserialno() {
        return startserialno;
    }

    /**
     * @param startserialno the startserialno to set
     */
    public void setStartserialno(String startserialno) {
        this.startserialno = startserialno;
    }

    /**
     * @return the endserialno
     */
    public String getEndserialno() {
        return endserialno;
    }

    /**
     * @param endserialno the endserialno to set
     */
    public void setEndserialno(String endserialno) {
        this.endserialno = endserialno;
    }

    /**
     * @return the terminalserialno
     */
    public String getTerminalserialno() {
        return terminalserialno;
    }

    /**
     * @param terminalserialno the terminalserialno to set
     */
    public void setTerminalserialno(String terminalserialno) {
        this.terminalserialno = terminalserialno;
    }

    /**
     * @return the terminalbrnad
     */
    public Integer getTerminalbrnad() {
        return terminalbrnad;
    }

    /**
     * @param terminalbrnad the terminalbrnad to set
     */
    public void setTerminalbrnad(Integer terminalbrnad) {
        this.terminalbrnad = terminalbrnad;
    }

    /**
     * @return the terminalmodel
     */
    public Integer getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(Integer terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the warrantyexpirystatus
     */
    public String getWarrantyexpirystatus() {
        return warrantyexpirystatus;
    }

    /**
     * @param warrantyexpirystatus the warrantyexpirystatus to set
     */
    public void setWarrantyexpirystatus(String warrantyexpirystatus) {
        this.warrantyexpirystatus = warrantyexpirystatus;
    }

    /**
     * @return the amcexpirystatus
     */
    public String getAmcexpirystatus() {
        return amcexpirystatus;
    }

    /**
     * @param amcexpirystatus the amcexpirystatus to set
     */
    public void setAmcexpirystatus(String amcexpirystatus) {
        this.amcexpirystatus = amcexpirystatus;
    }

    /**
     * @return the lotno
     */
    public String getLotno() {
        return lotno;
    }

    /**
     * @param lotno the lotno to set
     */
    public void setLotno(String lotno) {
        this.lotno = lotno;
    }

    /**
     * @return the partno
     */
    public String getPartno() {
        return partno;
    }

    /**
     * @param partno the partno to set
     */
    public void setPartno(String partno) {
        this.partno = partno;
    }

    /**
     * @return the revision
     */
    public String getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * @param mac the mac to set
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * @return the ptid
     */
    public String getPtid() {
        return ptid;
    }

    /**
     * @param ptid the ptid to set
     */
    public void setPtid(String ptid) {
        this.ptid = ptid;
    }

    /**
     * @return the bank
     */
    public Integer getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Integer bank) {
        this.bank = bank;
    }

    /**
     * @return the client
     */
    public Integer getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Integer client) {
        this.client = client;
    }

    /**
     * @return the clientcategory
     */
    public Integer getClientcategory() {
        return clientcategory;
    }

    /**
     * @param clientcategory the clientcategory to set
     */
    public void setClientcategory(Integer clientcategory) {
        this.clientcategory = clientcategory;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the contactperson
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * @param contactperson the contactperson to set
     */
    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    /**
     * @return the contactno
     */
    public String getContactno() {
        return contactno;
    }

    /**
     * @param contactno the contactno to set
     */
    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    /**
     * @return the district
     */
    public Integer getDistrict() {
        return district;
    }

    /**
     * @param district the district to set
     */
    public void setDistrict(Integer district) {
        this.district = district;
    }

    /**
     * @return the locationaddress
     */
    public String getLocationaddress() {
        return locationaddress;
    }

    /**
     * @param locationaddress the locationaddress to set
     */
    public void setLocationaddress(String locationaddress) {
        this.locationaddress = locationaddress;
    }

    /**
     * @return the deliveryrequirement
     */
    public Integer getDeliveryrequirement() {
        return deliveryrequirement;
    }

    /**
     * @param deliveryrequirement the deliveryrequirement to set
     */
    public void setDeliveryrequirement(Integer deliveryrequirement) {
        this.deliveryrequirement = deliveryrequirement;
    }

    /**
     * @return the deliverydestination
     */
    public String getDeliverydestination() {
        return deliverydestination;
    }

    /**
     * @param deliverydestination the deliverydestination to set
     */
    public void setDeliverydestination(String deliverydestination) {
        this.deliverydestination = deliverydestination;
    }

    /**
     * @return the natureoffault
     */
    public Integer getNatureoffault() {
        return natureoffault;
    }

    /**
     * @param natureoffault the natureoffault to set
     */
    public void setNatureoffault(Integer natureoffault) {
        this.natureoffault = natureoffault;
    }

    /**
     * @return the actiontobetaken
     */
    public Integer getActiontobetaken() {
        return actiontobetaken;
    }

    /**
     * @param actiontobetaken the actiontobetaken to set
     */
    public void setActiontobetaken(Integer actiontobetaken) {
        this.actiontobetaken = actiontobetaken;
    }

    /**
     * @return the causeoffault
     */
    public Integer getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(Integer causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the actiontaken
     */
    public Integer getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(Integer actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the technicalofficerincharge
     */
    public String getTechnicalofficerincharge() {
        return technicalofficerincharge;
    }

    /**
     * @param technicalofficerincharge the technicalofficerincharge to set
     */
    public void setTechnicalofficerincharge(String technicalofficerincharge) {
        this.technicalofficerincharge = technicalofficerincharge;
    }

    /**
     * @return the reportedby
     */
    public Integer getReportedby() {
        return reportedby;
    }

    /**
     * @param reportedby the reportedby to set
     */
    public void setReportedby(Integer reportedby) {
        this.reportedby = reportedby;
    }

    /**
     * @return the reportedmerchant
     */
    public String getReportedmerchant() {
        return reportedmerchant;
    }

    /**
     * @param reportedmerchant the reportedmerchant to set
     */
    public void setReportedmerchant(String reportedmerchant) {
        this.reportedmerchant = reportedmerchant;
    }

    /**
     * @return the createdby
     */
    public String getCreatedby() {
        return createdby;
    }

    /**
     * @param createdby the createdby to set
     */
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the inventoryhardwareitem
     */
    public Integer getInventoryhardwareitem() {
        return inventoryhardwareitem;
    }

    /**
     * @param inventoryhardwareitem the inventoryhardwareitem to set
     */
    public void setInventoryhardwareitem(Integer inventoryhardwareitem) {
        this.inventoryhardwareitem = inventoryhardwareitem;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the assignee
     */
    public String getAssignee() {
        return assignee;
    }

    /**
     * @param assignee the assignee to set
     */
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    /**
     * @return the manualassigneestatus
     */
    public String getManualassigneestatus() {
        return manualassigneestatus;
    }

    /**
     * @param manualassigneestatus the manualassigneestatus to set
     */
    public void setManualassigneestatus(String manualassigneestatus) {
        this.manualassigneestatus = manualassigneestatus;
    }

    /**
     * @return the isusernegligence
     */
    public Integer getIsusernegligence() {
        return isusernegligence;
    }

    /**
     * @param isusernegligence the isusernegligence to set
     */
    public void setIsusernegligence(Integer isusernegligence) {
        this.isusernegligence = isusernegligence;
    }

    /**
     * @return the islightening
     */
    public Integer getIslightening() {
        return islightening;
    }

    /**
     * @param islightening the islightening to set
     */
    public void setIslightening(Integer islightening) {
        this.islightening = islightening;
    }

    /**
     * @return the iscoveredunderwarranty
     */
    public Integer getIscoveredunderwarranty() {
        return iscoveredunderwarranty;
    }

    /**
     * @param iscoveredunderwarranty the iscoveredunderwarranty to set
     */
    public void setIscoveredunderwarranty(Integer iscoveredunderwarranty) {
        this.iscoveredunderwarranty = iscoveredunderwarranty;
    }

    /**
     * @return the isunderamc
     */
    public Integer getIsunderamc() {
        return isunderamc;
    }

    /**
     * @param isunderamc the isunderamc to set
     */
    public void setIsunderamc(Integer isunderamc) {
        this.isunderamc = isunderamc;
    }

    /**
     * @return the damagepartdescription
     */
    public String getDamagepartdescription() {
        return damagepartdescription;
    }

    /**
     * @param damagepartdescription the damagepartdescription to set
     */
    public void setDamagepartdescription(String damagepartdescription) {
        this.damagepartdescription = damagepartdescription;
    }

    /**
     * @return the replacedpart
     */
    public String getReplacedpart() {
        return replacedpart;
    }

    /**
     * @param replacedpart the replacedpart to set
     */
    public void setReplacedpart(String replacedpart) {
        this.replacedpart = replacedpart;
    }

    /**
     * @return the parttype
     */
    public String getParttype() {
        return parttype;
    }

    /**
     * @param parttype the parttype to set
     */
    public void setParttype(String parttype) {
        this.parttype = parttype;
    }

    /**
     * @return the partnumber
     */
    public String getPartnumber() {
        return partnumber;
    }

    /**
     * @param partnumber the partnumber to set
     */
    public void setPartnumber(String partnumber) {
        this.partnumber = partnumber;
    }

    /**
     * @return the partnumberofterminal
     */
    public String getPartnumberofterminal() {
        return partnumberofterminal;
    }

    /**
     * @param partnumberofterminal the partnumberofterminal to set
     */
    public void setPartnumberofterminal(String partnumberofterminal) {
        this.partnumberofterminal = partnumberofterminal;
    }

    /**
     * @return the completiondatetime
     */
    public Date getCompletiondatetime() {
        return completiondatetime;
    }

    /**
     * @param completiondatetime the completiondatetime to set
     */
    public void setCompletiondatetime(Date completiondatetime) {
        this.completiondatetime = completiondatetime;
    }

    /**
     * @return the totletimespent
     */
    public String getTotletimespent() {
        return totletimespent;
    }

    /**
     * @param totletimespent the totletimespent to set
     */
    public void setTotletimespent(String totletimespent) {
        this.totletimespent = totletimespent;
    }

    /**
     * @return the specialremark
     */
    public String getSpecialremark() {
        return specialremark;
    }

    /**
     * @param specialremark the specialremark to set
     */
    public void setSpecialremark(String specialremark) {
        this.specialremark = specialremark;
    }

    /**
     * @return the dispatchdate
     */
    public Date getDispatchdate() {
        return dispatchdate;
    }

    /**
     * @param dispatchdate the dispatchdate to set
     */
    public void setDispatchdate(Date dispatchdate) {
        this.dispatchdate = dispatchdate;
    }

    /**
     * @return the courierserviceprovider
     */
    public String getCourierserviceprovider() {
        return courierserviceprovider;
    }

    /**
     * @param courierserviceprovider the courierserviceprovider to set
     */
    public void setCourierserviceprovider(String courierserviceprovider) {
        this.courierserviceprovider = courierserviceprovider;
    }

    /**
     * @return the trackingnumber
     */
    public String getTrackingnumber() {
        return trackingnumber;
    }

    /**
     * @param trackingnumber the trackingnumber to set
     */
    public void setTrackingnumber(String trackingnumber) {
        this.trackingnumber = trackingnumber;
    }

    /**
     * @return the terminalquotationid
     */
    public Integer getTerminalquotationid() {
        return terminalquotationid;
    }

    /**
     * @param terminalquotationid the terminalquotationid to set
     */
    public void setTerminalquotationid(Integer terminalquotationid) {
        this.terminalquotationid = terminalquotationid;
    }

    /**
     * @return the recipientbank
     */
    public String getRecipientbank() {
        return recipientbank;
    }

    /**
     * @param recipientbank the recipientbank to set
     */
    public void setRecipientbank(String recipientbank) {
        this.recipientbank = recipientbank;
    }

    /**
     * @return the qterminalserialno
     */
    public String getQterminalserialno() {
        return qterminalserialno;
    }

    /**
     * @param qterminalserialno the qterminalserialno to set
     */
    public void setQterminalserialno(String qterminalserialno) {
        this.qterminalserialno = qterminalserialno;
    }

    /**
     * @return the qterminalmodel
     */
    public String getQterminalmodel() {
        return qterminalmodel;
    }

    /**
     * @param qterminalmodel the qterminalmodel to set
     */
    public void setQterminalmodel(String qterminalmodel) {
        this.qterminalmodel = qterminalmodel;
    }

    /**
     * @return the merchantlocation
     */
    public String getMerchantlocation() {
        return merchantlocation;
    }

    /**
     * @param merchantlocation the merchantlocation to set
     */
    public void setMerchantlocation(String merchantlocation) {
        this.merchantlocation = merchantlocation;
    }

    /**
     * @return the replacementpartno
     */
    public String getReplacementpartno() {
        return replacementpartno;
    }

    /**
     * @param replacementpartno the replacementpartno to set
     */
    public void setReplacementpartno(String replacementpartno) {
        this.replacementpartno = replacementpartno;
    }

    /**
     * @return the replacementdescription
     */
    public String getReplacementdescription() {
        return replacementdescription;
    }

    /**
     * @param replacementdescription the replacementdescription to set
     */
    public void setReplacementdescription(String replacementdescription) {
        this.replacementdescription = replacementdescription;
    }

    /**
     * @return the unitprice
     */
    public String getUnitprice() {
        return unitprice;
    }

    /**
     * @param unitprice the unitprice to set
     */
    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * @return the qcauseoffault
     */
    public Integer getQcauseoffault() {
        return qcauseoffault;
    }

    /**
     * @param qcauseoffault the qcauseoffault to set
     */
    public void setQcauseoffault(Integer qcauseoffault) {
        this.qcauseoffault = qcauseoffault;
    }

    /**
     * @return the quatationvalidityperiod
     */
    public Date getQuatationvalidityperiod() {
        return quatationvalidityperiod;
    }

    /**
     * @param quatationvalidityperiod the quatationvalidityperiod to set
     */
    public void setQuatationvalidityperiod(Date quatationvalidityperiod) {
        this.quatationvalidityperiod = quatationvalidityperiod;
    }

    /**
     * @return the quotation
     */
    public boolean isQuotation() {
        return quotation;
    }

    /**
     * @param quotation the quotation to set
     */
    public void setQuotation(boolean quotation) {
        this.quotation = quotation;
    }

    /**
     * @return the deploymentdate
     */
    public Date getDeploymentdate() {
        return deploymentdate;
    }

    /**
     * @param deploymentdate the deploymentdate to set
     */
    public void setDeploymentdate(Date deploymentdate) {
        this.deploymentdate = deploymentdate;
    }

    /**
     * @return the input
     */
    public String getInput() {
        return input;
    }

    /**
     * @param input the input to set
     */
    public void setInput(String input) {
        this.input = input;
    }

    /**
     * @return the filepath
     */
    public String getFilepath() {
        return filepath;
    }

    /**
     * @param filepath the filepath to set
     */
    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the file
     */
    public MultipartFile getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(MultipartFile file) {
        this.file = file;
    }

    /**
     * @return the terminaldeliverystatus
     */
    public Integer getTerminaldeliverystatus() {
        return terminaldeliverystatus;
    }

    /**
     * @param terminaldeliverystatus the terminaldeliverystatus to set
     */
    public void setTerminaldeliverystatus(Integer terminaldeliverystatus) {
        this.terminaldeliverystatus = terminaldeliverystatus;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the newsoftwareversion
     */
    public String getNewsoftwareversion() {
        return newsoftwareversion;
    }

    /**
     * @param newsoftwareversion the newsoftwareversion to set
     */
    public void setNewsoftwareversion(String newsoftwareversion) {
        this.newsoftwareversion = newsoftwareversion;
    }

    /**
     * @return the deliveryorderref
     */
    public String getDeliveryorderref() {
        return deliveryorderref;
    }

    /**
     * @param deliveryorderref the deliveryorderref to set
     */
    public void setDeliveryorderref(String deliveryorderref) {
        this.deliveryorderref = deliveryorderref;
    }

    /**
     * @return the rmareferance
     */
    public String getRmareferance() {
        return rmareferance;
    }

    /**
     * @param rmareferance the rmareferance to set
     */
    public void setRmareferance(String rmareferance) {
        this.rmareferance = rmareferance;
    }

    /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * @return the invoicenumber
     */
    public String getInvoicenumber() {
        return invoicenumber;
    }

    /**
     * @param invoicenumber the invoicenumber to set
     */
    public void setInvoicenumber(String invoicenumber) {
        this.invoicenumber = invoicenumber;
    }

    /**
     * @return the invoicevalue
     */
    public Double getInvoicevalue() {
        return invoicevalue;
    }

    /**
     * @param invoicevalue the invoicevalue to set
     */
    public void setInvoicevalue(Double invoicevalue) {
        this.invoicevalue = invoicevalue;
    }

    /**
     * @return the invoicedate
     */
    public Date getInvoicedate() {
        return invoicedate;
    }

    /**
     * @param invoicedate the invoicedate to set
     */
    public void setInvoicedate(Date invoicedate) {
        this.invoicedate = invoicedate;
    }

    /**
     * @return the bkplocationaddress
     */
    public String getBkplocationaddress() {
        return bkplocationaddress;
    }

    /**
     * @param bkplocationaddress the bkplocationaddress to set
     */
    public void setBkplocationaddress(String bkplocationaddress) {
        this.bkplocationaddress = bkplocationaddress;
    }

    /**
     * @return the purposetype
     */
    public Integer getPurposetype() {
        return purposetype;
    }

    /**
     * @param purposetype the purposetype to set
     */
    public void setPurposetype(Integer purposetype) {
        this.purposetype = purposetype;
    }

    /**
     * @return the assigneetype
     */
    public int getAssigneetype() {
        return assigneetype;
    }

    /**
     * @param assigneetype the assigneetype to set
     */
    public void setAssigneetype(int assigneetype) {
        this.assigneetype = assigneetype;
    }

    /**
     * @return the notificationlevel
     */
    public int getNotificationlevel() {
        return notificationlevel;
    }

    /**
     * @param notificationlevel the notificationlevel to set
     */
    public void setNotificationlevel(int notificationlevel) {
        this.notificationlevel = notificationlevel;
    }

    /**
     * @return the resolutiondescription
     */
    public String getResolutiondescription() {
        return resolutiondescription;
    }

    /**
     * @param resolutiondescription the resolutiondescription to set
     */
    public void setResolutiondescription(String resolutiondescription) {
        this.resolutiondescription = resolutiondescription;
    }

    /**
     * @return the amcperiod
     */
    public String getAmcperiod() {
        return amcperiod;
    }

    /**
     * @param amcperiod the amcperiod to set
     */
    public void setAmcperiod(String amcperiod) {
        this.amcperiod = amcperiod;
    }

    /**
     * @return the amcpaymentplan
     */
    public String getAmcpaymentplan() {
        return amcpaymentplan;
    }

    /**
     * @param amcpaymentplan the amcpaymentplan to set
     */
    public void setAmcpaymentplan(String amcpaymentplan) {
        this.amcpaymentplan = amcpaymentplan;
    }

    /**
     * @return the agreementstatus
     */
    public String getAgreementstatus() {
        return agreementstatus;
    }

    /**
     * @param agreementstatus the agreementstatus to set
     */
    public void setAgreementstatus(String agreementstatus) {
        this.agreementstatus = agreementstatus;
    }

    /**
     * @return the vmtid
     */
    public String getVmtid() {
        return vmtid;
    }

    /**
     * @param vmtid the vmtid to set
     */
    public void setVmtid(String vmtid) {
        this.vmtid = vmtid;
    }

    /**
     * @return the vmmid
     */
    public String getVmmid() {
        return vmmid;
    }

    /**
     * @param vmmid the vmmid to set
     */
    public void setVmmid(String vmmid) {
        this.vmmid = vmmid;
    }

    /**
     * @return the amextid
     */
    public String getAmextid() {
        return amextid;
    }

    /**
     * @param amextid the amextid to set
     */
    public void setAmextid(String amextid) {
        this.amextid = amextid;
    }

    /**
     * @return the amexmid
     */
    public String getAmexmid() {
        return amexmid;
    }

    /**
     * @param amexmid the amexmid to set
     */
    public void setAmexmid(String amexmid) {
        this.amexmid = amexmid;
    }

    /**
     * @return the sharingtid
     */
    public String getSharingtid() {
        return sharingtid;
    }

    /**
     * @param sharingtid the sharingtid to set
     */
    public void setSharingtid(String sharingtid) {
        this.sharingtid = sharingtid;
    }

    /**
     * @return the sharingmid
     */
    public String getSharingmid() {
        return sharingmid;
    }

    /**
     * @param sharingmid the sharingmid to set
     */
    public void setSharingmid(String sharingmid) {
        this.sharingmid = sharingmid;
    }

    /**
     * @return the nacno
     */
    public Integer getNacno() {
        return nacno;
    }

    /**
     * @param nacno the nacno to set
     */
    public void setNacno(Integer nacno) {
        this.nacno = nacno;
    }

    /**
     * @return the sharingnacno
     */
    public Integer getSharingnacno() {
        return sharingnacno;
    }

    /**
     * @param sharingnacno the sharingnacno to set
     */
    public void setSharingnacno(Integer sharingnacno) {
        this.sharingnacno = sharingnacno;
    }

    /**
     * @return the esp
     */
    public Integer getEsp() {
        return esp;
    }

    /**
     * @param esp the esp to set
     */
    public void setEsp(Integer esp) {
        this.esp = esp;
    }

    /**
     * @return the profiletype
     */
    public Integer getProfiletype() {
        return profiletype;
    }

    /**
     * @param profiletype the profiletype to set
     */
    public void setProfiletype(Integer profiletype) {
        this.profiletype = profiletype;
    }

    /**
     * @return the manualkey
     */
    public Integer getManualkey() {
        return manualkey;
    }

    /**
     * @param manualkey the manualkey to set
     */
    public void setManualkey(Integer manualkey) {
        this.manualkey = manualkey;
    }

    /**
     * @return the offline
     */
    public Integer getOffline() {
        return offline;
    }

    /**
     * @param offline the offline to set
     */
    public void setOffline(Integer offline) {
        this.offline = offline;
    }

    /**
     * @return the preauth
     */
    public Integer getPreauth() {
        return preauth;
    }

    /**
     * @param preauth the preauth to set
     */
    public void setPreauth(Integer preauth) {
        this.preauth = preauth;
    }

    /**
     * @return the tip
     */
    public Integer getTip() {
        return tip;
    }

    /**
     * @param tip the tip to set
     */
    public void setTip(Integer tip) {
        this.tip = tip;
    }

    /**
     * @return the cashadvance
     */
    public Integer getCashadvance() {
        return cashadvance;
    }

    /**
     * @param cashadvance the cashadvance to set
     */
    public void setCashadvance(Integer cashadvance) {
        this.cashadvance = cashadvance;
    }

    /**
     * @return the panmask
     */
    public Integer getPanmask() {
        return panmask;
    }

    /**
     * @param panmask the panmask to set
     */
    public void setPanmask(Integer panmask) {
        this.panmask = panmask;
    }

    /**
     * @return the l4
     */
    public Integer getL4() {
        return l4;
    }

    /**
     * @param l4 the l4 to set
     */
    public void setL4(Integer l4) {
        this.l4 = l4;
    }

    /**
     * @return the emv
     */
    public Integer getEmv() {
        return emv;
    }

    /**
     * @param emv the emv to set
     */
    public void setEmv(Integer emv) {
        this.emv = emv;
    }

    /**
     * @return the fourdbc
     */
    public Integer getFourdbc() {
        return fourdbc;
    }

    /**
     * @param fourdbc the fourdbc to set
     */
    public void setFourdbc(Integer fourdbc) {
        this.fourdbc = fourdbc;
    }

    /**
     * @return the void_
     */
    public Integer getVoid_() {
        return void_;
    }

    /**
     * @param void_ the void_ to set
     */
    public void setVoid_(Integer void_) {
        this.void_ = void_;
    }

    /**
     * @return the settlement
     */
    public Integer getSettlement() {
        return settlement;
    }

    /**
     * @param settlement the settlement to set
     */
    public void setSettlement(Integer settlement) {
        this.settlement = settlement;
    }

    /**
     * @return the tobeinstalledbyepic
     */
    public Integer getTobeinstalledbyepic() {
        return tobeinstalledbyepic;
    }

    /**
     * @param tobeinstalledbyepic the tobeinstalledbyepic to set
     */
    public void setTobeinstalledbyepic(Integer tobeinstalledbyepic) {
        this.tobeinstalledbyepic = tobeinstalledbyepic;
    }

    /**
     * @return the password
     */
    public Integer getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(Integer password) {
        this.password = password;
    }

    /**
     * @return the autosettlement
     */
    public Integer getAutosettlement() {
        return autosettlement;
    }

    /**
     * @param autosettlement the autosettlement to set
     */
    public void setAutosettlement(Integer autosettlement) {
        this.autosettlement = autosettlement;
    }

    /**
     * @return the simno
     */
    public String getSimno() {
        return simno;
    }

    /**
     * @param simno the simno to set
     */
    public void setSimno(String simno) {
        this.simno = simno;
    }

    /**
     * @return the deadlinedate
     */
    public Date getDeadlinedate() {
        return deadlinedate;
    }

    /**
     * @param deadlinedate the deadlinedate to set
     */
    public void setDeadlinedate(Date deadlinedate) {
        this.deadlinedate = deadlinedate;
    }

    /**
     * @return the aditionalcosts
     */
    public String getAditionalcosts() {
        return aditionalcosts;
    }

    /**
     * @param aditionalcosts the aditionalcosts to set
     */
    public void setAditionalcosts(String aditionalcosts) {
        this.aditionalcosts = aditionalcosts;
    }

    /**
     * @return the backupdeviceserial
     */
    public String getBackupdeviceserial() {
        return backupdeviceserial;
    }

    /**
     * @param backupdeviceserial the backupdeviceserial to set
     */
    public void setBackupdeviceserial(String backupdeviceserial) {
        this.backupdeviceserial = backupdeviceserial;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the user1
     */
    public String getUser1() {
        return user1;
    }

    /**
     * @param user1 the user1 to set
     */
    public void setUser1(String user1) {
        this.user1 = user1;
    }

    /**
     * @return the userpass1
     */
    public String getUserpass1() {
        return userpass1;
    }

    /**
     * @param userpass1 the userpass1 to set
     */
    public void setUserpass1(String userpass1) {
        this.userpass1 = userpass1;
    }

    /**
     * @return the user2
     */
    public String getUser2() {
        return user2;
    }

    /**
     * @param user2 the user2 to set
     */
    public void setUser2(String user2) {
        this.user2 = user2;
    }

    /**
     * @return the userpass2
     */
    public String getUserpass2() {
        return userpass2;
    }

    /**
     * @param userpass2 the userpass2 to set
     */
    public void setUserpass2(String userpass2) {
        this.userpass2 = userpass2;
    }

    /**
     * @return the user3
     */
    public String getUser3() {
        return user3;
    }

    /**
     * @param user3 the user3 to set
     */
    public void setUser3(String user3) {
        this.user3 = user3;
    }

    /**
     * @return the userpass3
     */
    public String getUserpass3() {
        return userpass3;
    }

    /**
     * @param userpass3 the userpass3 to set
     */
    public void setUserpass3(String userpass3) {
        this.userpass3 = userpass3;
    }

    /**
     * @return the user4
     */
    public String getUser4() {
        return user4;
    }

    /**
     * @param user4 the user4 to set
     */
    public void setUser4(String user4) {
        this.user4 = user4;
    }

    /**
     * @return the userpass4
     */
    public String getUserpass4() {
        return userpass4;
    }

    /**
     * @param userpass4 the userpass4 to set
     */
    public void setUserpass4(String userpass4) {
        this.userpass4 = userpass4;
    }

    /**
     * @return the user5
     */
    public String getUser5() {
        return user5;
    }

    /**
     * @param user5 the user5 to set
     */
    public void setUser5(String user5) {
        this.user5 = user5;
    }

    /**
     * @return the userpass5
     */
    public String getUserpass5() {
        return userpass5;
    }

    /**
     * @param userpass5 the userpass5 to set
     */
    public void setUserpass5(String userpass5) {
        this.userpass5 = userpass5;
    }

    /**
     * @return the user6
     */
    public String getUser6() {
        return user6;
    }

    /**
     * @param user6 the user6 to set
     */
    public void setUser6(String user6) {
        this.user6 = user6;
    }

    /**
     * @return the userpass6
     */
    public String getUserpass6() {
        return userpass6;
    }

    /**
     * @param userpass6 the userpass6 to set
     */
    public void setUserpass6(String userpass6) {
        this.userpass6 = userpass6;
    }

    /**
     * @return the user7
     */
    public String getUser7() {
        return user7;
    }

    /**
     * @param user7 the user7 to set
     */
    public void setUser7(String user7) {
        this.user7 = user7;
    }

    /**
     * @return the userpass7
     */
    public String getUserpass7() {
        return userpass7;
    }

    /**
     * @param userpass7 the userpass7 to set
     */
    public void setUserpass7(String userpass7) {
        this.userpass7 = userpass7;
    }

    /**
     * @return the user8
     */
    public String getUser8() {
        return user8;
    }

    /**
     * @param user8 the user8 to set
     */
    public void setUser8(String user8) {
        this.user8 = user8;
    }

    /**
     * @return the userpass8
     */
    public String getUserpass8() {
        return userpass8;
    }

    /**
     * @param userpass8 the userpass8 to set
     */
    public void setUserpass8(String userpass8) {
        this.userpass8 = userpass8;
    }

    /**
     * @return the user9
     */
    public String getUser9() {
        return user9;
    }

    /**
     * @param user9 the user9 to set
     */
    public void setUser9(String user9) {
        this.user9 = user9;
    }

    /**
     * @return the userpass9
     */
    public String getUserpass9() {
        return userpass9;
    }

    /**
     * @param userpass9 the userpass9 to set
     */
    public void setUserpass9(String userpass9) {
        this.userpass9 = userpass9;
    }

    /**
     * @return the user10
     */
    public String getUser10() {
        return user10;
    }

    /**
     * @param user10 the user10 to set
     */
    public void setUser10(String user10) {
        this.user10 = user10;
    }

    /**
     * @return the userpass10
     */
    public String getUserpass10() {
        return userpass10;
    }

    /**
     * @param userpass10 the userpass10 to set
     */
    public void setUserpass10(String userpass10) {
        this.userpass10 = userpass10;
    }

    /**
     * @return the user11
     */
    public String getUser11() {
        return user11;
    }

    /**
     * @param user11 the user11 to set
     */
    public void setUser11(String user11) {
        this.user11 = user11;
    }

    /**
     * @return the userpass11
     */
    public String getUserpass11() {
        return userpass11;
    }

    /**
     * @param userpass11 the userpass11 to set
     */
    public void setUserpass11(String userpass11) {
        this.userpass11 = userpass11;
    }

    /**
     * @return the user12
     */
    public String getUser12() {
        return user12;
    }

    /**
     * @param user12 the user12 to set
     */
    public void setUser12(String user12) {
        this.user12 = user12;
    }

    /**
     * @return the userpass12
     */
    public String getUserpass12() {
        return userpass12;
    }

    /**
     * @param userpass12 the userpass12 to set
     */
    public void setUserpass12(String userpass12) {
        this.userpass12 = userpass12;
    }

    /**
     * @return the user13
     */
    public String getUser13() {
        return user13;
    }

    /**
     * @param user13 the user13 to set
     */
    public void setUser13(String user13) {
        this.user13 = user13;
    }

    /**
     * @return the userpass13
     */
    public String getUserpass13() {
        return userpass13;
    }

    /**
     * @param userpass13 the userpass13 to set
     */
    public void setUserpass13(String userpass13) {
        this.userpass13 = userpass13;
    }

    /**
     * @return the user14
     */
    public String getUser14() {
        return user14;
    }

    /**
     * @param user14 the user14 to set
     */
    public void setUser14(String user14) {
        this.user14 = user14;
    }

    /**
     * @return the userpass14
     */
    public String getUserpass14() {
        return userpass14;
    }

    /**
     * @param userpass14 the userpass14 to set
     */
    public void setUserpass14(String userpass14) {
        this.userpass14 = userpass14;
    }

    /**
     * @return the user15
     */
    public String getUser15() {
        return user15;
    }

    /**
     * @param user15 the user15 to set
     */
    public void setUser15(String user15) {
        this.user15 = user15;
    }

    /**
     * @return the userpass15
     */
    public String getUserpass15() {
        return userpass15;
    }

    /**
     * @param userpass15 the userpass15 to set
     */
    public void setUserpass15(String userpass15) {
        this.userpass15 = userpass15;
    }

    /**
     * @return the user16
     */
    public String getUser16() {
        return user16;
    }

    /**
     * @param user16 the user16 to set
     */
    public void setUser16(String user16) {
        this.user16 = user16;
    }

    /**
     * @return the userpass16
     */
    public String getUserpass16() {
        return userpass16;
    }

    /**
     * @param userpass16 the userpass16 to set
     */
    public void setUserpass16(String userpass16) {
        this.userpass16 = userpass16;
    }

    /**
     * @return the user17
     */
    public String getUser17() {
        return user17;
    }

    /**
     * @param user17 the user17 to set
     */
    public void setUser17(String user17) {
        this.user17 = user17;
    }

    /**
     * @return the userpass17
     */
    public String getUserpass17() {
        return userpass17;
    }

    /**
     * @param userpass17 the userpass17 to set
     */
    public void setUserpass17(String userpass17) {
        this.userpass17 = userpass17;
    }

    /**
     * @return the user18
     */
    public String getUser18() {
        return user18;
    }

    /**
     * @param user18 the user18 to set
     */
    public void setUser18(String user18) {
        this.user18 = user18;
    }

    /**
     * @return the userpass18
     */
    public String getUserpass18() {
        return userpass18;
    }

    /**
     * @param userpass18 the userpass18 to set
     */
    public void setUserpass18(String userpass18) {
        this.userpass18 = userpass18;
    }

    /**
     * @return the user19
     */
    public String getUser19() {
        return user19;
    }

    /**
     * @param user19 the user19 to set
     */
    public void setUser19(String user19) {
        this.user19 = user19;
    }

    /**
     * @return the userpass19
     */
    public String getUserpass19() {
        return userpass19;
    }

    /**
     * @param userpass19 the userpass19 to set
     */
    public void setUserpass19(String userpass19) {
        this.userpass19 = userpass19;
    }

    /**
     * @return the user20
     */
    public String getUser20() {
        return user20;
    }

    /**
     * @param user20 the user20 to set
     */
    public void setUser20(String user20) {
        this.user20 = user20;
    }

    /**
     * @return the userpass20
     */
    public String getUserpass20() {
        return userpass20;
    }

    /**
     * @param userpass20 the userpass20 to set
     */
    public void setUserpass20(String userpass20) {
        this.userpass20 = userpass20;
    }

    /**
     * @return the receipttext
     */
    public String getReceipttext() {
        return receipttext;
    }

    /**
     * @param receipttext the receipttext to set
     */
    public void setReceipttext(String receipttext) {
        this.receipttext = receipttext;
    }

    /**
     * @return the applicationversion
     */
    public String getApplicationversion() {
        return applicationversion;
    }

    /**
     * @param applicationversion the applicationversion to set
     */
    public void setApplicationversion(String applicationversion) {
        this.applicationversion = applicationversion;
    }

    /**
     * @return the visamasteramexconfig
     */
    public String getVisamasteramexconfig() {
        return visamasteramexconfig;
    }

    /**
     * @param visamasteramexconfig the visamasteramexconfig to set
     */
    public void setVisamasteramexconfig(String visamasteramexconfig) {
        this.visamasteramexconfig = visamasteramexconfig;
    }

    /**
     * @return the dcc
     */
    public String getDcc() {
        return dcc;
    }

    /**
     * @param dcc the dcc to set
     */
    public void setDcc(String dcc) {
        this.dcc = dcc;
    }

    /**
     * @return the clientcontactperson
     */
    public String getClientcontactperson() {
        return clientcontactperson;
    }

    /**
     * @param clientcontactperson the clientcontactperson to set
     */
    public void setClientcontactperson(String clientcontactperson) {
        this.clientcontactperson = clientcontactperson;
    }

    /**
     * @return the clientemail
     */
    public String getClientemail() {
        return clientemail;
    }

    /**
     * @param clientemail the clientemail to set
     */
    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }

    /**
     * @return the clientcontactno
     */
    public String getClientcontactno() {
        return clientcontactno;
    }

    /**
     * @param clientcontactno the clientcontactno to set
     */
    public void setClientcontactno(String clientcontactno) {
        this.clientcontactno = clientcontactno;
    }

    /**
     * @return the merchantcontactperson
     */
    public String getMerchantcontactperson() {
        return merchantcontactperson;
    }

    /**
     * @param merchantcontactperson the merchantcontactperson to set
     */
    public void setMerchantcontactperson(String merchantcontactperson) {
        this.merchantcontactperson = merchantcontactperson;
    }

    /**
     * @return the merchantcontactno
     */
    public String getMerchantcontactno() {
        return merchantcontactno;
    }

    /**
     * @param merchantcontactno the merchantcontactno to set
     */
    public void setMerchantcontactno(String merchantcontactno) {
        this.merchantcontactno = merchantcontactno;
    }

    /**
     * @return the terminalpassword
     */
    public String getTerminalpassword() {
        return terminalpassword;
    }

    /**
     * @param terminalpassword the terminalpassword to set
     */
    public void setTerminalpassword(String terminalpassword) {
        this.terminalpassword = terminalpassword;
    }

}
