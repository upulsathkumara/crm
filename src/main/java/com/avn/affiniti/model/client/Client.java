/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.client;

import com.avn.affiniti.model.clientproduct.ClientProduct;

/**
 * @Author Nadun Chamikara
 * @Document Client
 * @Created on 18/08/2017, 3:04:48 PM
 */
public class Client {

    private String clientid;
    private String name;
    private String[] clientcategory;
    private int organization;
    private String primarycontactperson;
    private String email;
    private String contactno01;
    private String contactno02;
    private int location;
    private String address;
    private int status;
    private String username;

    private ClientProduct[] clientproducts;

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getClientcategory() {
        return clientcategory;
    }

    public void setClientcategory(String[] clientcategory) {
        this.clientcategory = clientcategory;
    }

    public int getOrganization() {
        return organization;
    }

    public void setOrganization(int organization) {
        this.organization = organization;
    }

    public String getPrimarycontactperson() {
        return primarycontactperson;
    }

    public void setPrimarycontactperson(String primarycontactperson) {
        this.primarycontactperson = primarycontactperson;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactno01() {
        return contactno01;
    }

    public void setContactno01(String contactno01) {
        this.contactno01 = contactno01;
    }

    public String getContactno02() {
        return contactno02;
    }

    public void setContactno02(String contactno02) {
        this.contactno02 = contactno02;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ClientProduct[] getClientproducts() {
        return clientproducts;
    }

    public void setClientproducts(ClientProduct[] clientproducts) {
        this.clientproducts = clientproducts;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
