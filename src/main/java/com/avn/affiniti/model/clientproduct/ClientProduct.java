/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.clientproduct;

/**
 * @Author Nadun Chamikara
 * @Document ClientProduct
 * @Created on 24/08/2017, 10:16:49 AM
 */
public class ClientProduct {

    private String product;
    private String purchaseddate;
    private String clientproductstatus;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPurchaseddate() {
        return purchaseddate;
    }

    public void setPurchaseddate(String purchaseddate) {
        this.purchaseddate = purchaseddate;
    }

    public String getClientproductstatus() {
        return clientproductstatus;
    }

    public void setClientproductstatus(String clientproductstatus) {
        this.clientproductstatus = clientproductstatus;
    }

    
}
