/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.tickethistory;

import java.util.Date;

/**
 *
 * @author Kaushan Fernando
 * @Document TicketHistory
 * @Created on: Jan 3, 2018, 11:42:14 PM
 */
public class TicketHistory {

    private String previousticketid;
    private Date createddatetime;
    private String causeoffault;
    private String status;
    private String statusresionid;
    private String resolutiondescription;
    private String actiontaken;
    private String assignee;

    /**
     * @return the previousticketid
     */
    public String getPreviousticketid() {
        return previousticketid;
    }

    /**
     * @param previousticketid the previousticketid to set
     */
    public void setPreviousticketid(String previousticketid) {
        this.previousticketid = previousticketid;
    }

    /**
     * @return the createddatetime
     */
    public Date getCreateddatetime() {
        return createddatetime;
    }

    /**
     * @param createddatetime the createddatetime to set
     */
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    /**
     * @return the causeoffault
     */
    public String getCauseoffault() {
        return causeoffault;
    }

    /**
     * @param causeoffault the causeoffault to set
     */
    public void setCauseoffault(String causeoffault) {
        this.causeoffault = causeoffault;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the statusresionid
     */
    public String getStatusresionid() {
        return statusresionid;
    }

    /**
     * @param statusresionid the statusresionid to set
     */
    public void setStatusresionid(String statusresionid) {
        this.statusresionid = statusresionid;
    }

    /**
     * @return the resolutiondescription
     */
    public String getResolutiondescription() {
        return resolutiondescription;
    }

    /**
     * @param resolutiondescription the resolutiondescription to set
     */
    public void setResolutiondescription(String resolutiondescription) {
        this.resolutiondescription = resolutiondescription;
    }

    /**
     * @return the actiontaken
     */
    public String getActiontaken() {
        return actiontaken;
    }

    /**
     * @param actiontaken the actiontaken to set
     */
    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }

    /**
     * @return the assignee
     */
    public String getAssignee() {
        return assignee;
    }

    /**
     * @param assignee the assignee to set
     */
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

}
