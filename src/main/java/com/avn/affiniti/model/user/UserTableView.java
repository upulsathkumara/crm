/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.model.user;

/**
 * @Author : Roshen Dilshan
 * @Document : UserTableView
 * @Created on : Jul 20, 2016, 9:11:20 AM
 */
public class UserTableView {
    
    private String userid;
    private String userrole;
    private String status;
    private int userattempts;
    private String createddatetime;
    private String lastupdateddatetime;

    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return the userrole
     */
    public String getUserrole() {
        return userrole;
    }

    /**
     * @param userrole the userrole to set
     */
    public void setUserrole(String userrole) {
        this.userrole = userrole;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the userattempts
     */
    public int getUserattempts() {
        return userattempts;
    }

    /**
     * @param userattempts the userattempts to set
     */
    public void setUserattempts(int userattempts) {
        this.userattempts = userattempts;
    }

    /**
     * @return the createddatetime
     */
    public String getCreateddatetime() {
        return createddatetime;
    }

    /**
     * @param createddatetime the createddatetime to set
     */
    public void setCreateddatetime(String createddatetime) {
        this.createddatetime = createddatetime;
    }

    /**
     * @return the lastupdateddatetime
     */
    public String getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    /**
     * @param lastupdateddatetime the lastupdateddatetime to set
     */
    public void setLastupdateddatetime(String lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }


}
