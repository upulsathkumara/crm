/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * @Author : Roshen Dilshan
 * @Document : UserRowMapper
 * @Created on : Jul 20, 2016, 2:20:00 PM
 */
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();
        user.setEmployeeid(rs.getLong("EMPLOYEEID"));
        user.setNameinfull(rs.getString("NAMEINFULL"));
        user.setInitials(rs.getString("INITIALS"));
        user.setPreferredname(rs.getString("PREFERREDNAME"));
        user.setSurname(rs.getString("SURENAME"));
        user.setEpf(rs.getString("EPF"));
        user.setNic(rs.getString("NIC"));
        user.setEmail(rs.getString("EMAIL"));
        user.setContactno01(rs.getString("CONTACTNO01"));
        user.setContactno02(rs.getString("CONTACTNO02"));
        user.setSelhierarchyid(rs.getString("HIERARCHYID"));
        user.setHierarchyid(rs.getInt("HIERARCHYID"));
        user.setUsername(rs.getString("USERNAME"));
        user.setSeluserrole(rs.getString("USERROLE"));
        user.setSelstatus(rs.getString("STATUS"));
        return user;
    }

}
