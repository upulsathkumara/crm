/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.user;

import java.util.Date;

/**
 * @Author : Roshen Dilshan
 * @Document : User
 * @Created on : Sep 15, 2015, 9:35:45 AM
 */
public class User {

    private long employeeid;
    private String nameinfull;
    private String initials;
    private String preferredname;
    private String surname;
    private String epf;
    private String usercode;
    private String nic;
    private String email;
    private String contactno01;
    private String contactno02;
    private int hierarchyid;
    private String selhierarchyid;
    private String username;
    private String password;
    private int userrole;
    private String seluserrole;
    private Integer[] products;
    private String selproducts;
    private Integer[] territories;
    private String selterritories;
    private Integer[] productscategorytype;
    private String selproductscategorytype;
    private Integer[] supervisors;
    private String selsupervisors;
    private String seletedproducts;
    private Date lastlogin;
    private int userattempts;
    private int status;
    private String selstatus;
    private Date createddatetime;
    private Date lastupdateddatetime;
    private String createduser;
    private Integer[] languageskills;
    private Integer[] productsup;
    private Integer[] territoriesup;
    private Integer[] productscategorytypeup;
    private Integer[] supervisorsup;
    private String searchKeyword;

    /**
     * @return the employeeid
     */
    public long getEmployeeid() {
        return employeeid;
    }

    /**
     * @param employeeid the employeeid to set
     */
    public void setEmployeeid(long employeeid) {
        this.employeeid = employeeid;
    }

    /**
     * @return the nameinfull
     */
    public String getNameinfull() {
        return nameinfull;
    }

    /**
     * @param nameinfull the nameinfull to set
     */
    public void setNameinfull(String nameinfull) {
        this.nameinfull = nameinfull;
    }

    /**
     * @return the initials
     */
    public String getInitials() {
        return initials;
    }

    /**
     * @param initials the initials to set
     */
    public void setInitials(String initials) {
        this.initials = initials;
    }

    /**
     * @return the preferredname
     */
    public String getPreferredname() {
        return preferredname;
    }

    /**
     * @param preferredname the preferredname to set
     */
    public void setPreferredname(String preferredname) {
        this.preferredname = preferredname;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the contactno01
     */
    public String getContactno01() {
        return contactno01;
    }

    /**
     * @param contactno01 the contactno01 to set
     */
    public void setContactno01(String contactno01) {
        this.contactno01 = contactno01;
    }

    /**
     * @return the contactno02
     */
    public String getContactno02() {
        return contactno02;
    }

    /**
     * @param contactno02 the contactno02 to set
     */
    public void setContactno02(String contactno02) {
        this.contactno02 = contactno02;
    }

    /**
     * @return the hierarchyid
     */
    public int getHierarchyid() {
        return hierarchyid;
    }

    /**
     * @param hierarchyid the hierarchyid to set
     */
    public void setHierarchyid(int hierarchyid) {
        this.hierarchyid = hierarchyid;
    }

    /**
     * @return the selhierarchyid
     */
    public String getSelhierarchyid() {
        return selhierarchyid;
    }

    /**
     * @param selhierarchyid the selhierarchyid to set
     */
    public void setSelhierarchyid(String selhierarchyid) {
        this.selhierarchyid = selhierarchyid;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param userid the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userrole
     */
    public int getUserrole() {
        return userrole;
    }

    /**
     * @param userrole the userrole to set
     */
    public void setUserrole(int userrole) {
        this.userrole = userrole;
    }

    /**
     * @return the seluserrole
     */
    public String getSeluserrole() {
        return seluserrole;
    }

    /**
     * @param seluserrole the seluserrole to set
     */
    public void setSeluserrole(String seluserrole) {
        this.seluserrole = seluserrole;
    }

    /**
     * @return the products
     */
    public Integer[] getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(Integer[] products) {
        this.products = products;
    }

    /**
     * @return the selproducts
     */
    public String getSelproducts() {
        return selproducts;
    }

    /**
     * @param selproducts the selproducts to set
     */
    public void setSelproducts(String selproducts) {
        this.selproducts = selproducts;
    }

    /**
     * @return the territories
     */
    public Integer[] getTerritories() {
        return territories;
    }

    /**
     * @param territories the territories to set
     */
    public void setTerritories(Integer[] territories) {
        this.territories = territories;
    }

    /**
     * @return the selterritories
     */
    public String getSelterritories() {
        return selterritories;
    }

    /**
     * @param selterritories the selterritories to set
     */
    public void setSelterritories(String selterritories) {
        this.selterritories = selterritories;
    }

    /**
     * @return the productscategorytype
     */
    public Integer[] getProductscategorytype() {
        return productscategorytype;
    }

    /**
     * @param productscategorytype the productscategorytype to set
     */
    public void setProductscategorytype(Integer[] productscategorytype) {
        this.productscategorytype = productscategorytype;
    }

    /**
     * @return the selproductscategorytype
     */
    public String getSelproductscategorytype() {
        return selproductscategorytype;
    }

    /**
     * @param selproductscategorytype the selproductscategorytype to set
     */
    public void setSelproductscategorytype(String selproductscategorytype) {
        this.selproductscategorytype = selproductscategorytype;
    }

    /**
     * @return the supervisors
     */
    public Integer[] getSupervisors() {
        return supervisors;
    }

    /**
     * @param supervisors the supervisors to set
     */
    public void setSupervisors(Integer[] supervisors) {
        this.supervisors = supervisors;
    }

    /**
     * @return the selsupervisors
     */
    public String getSelsupervisors() {
        return selsupervisors;
    }

    /**
     * @param selsupervisors the selsupervisors to set
     */
    public void setSelsupervisors(String selsupervisors) {
        this.selsupervisors = selsupervisors;
    }

    /**
     * @return the seletedproducts
     */
    public String getSeletedproducts() {
        return seletedproducts;
    }

    /**
     * @param seletedproducts the seletedproducts to set
     */
    public void setSeletedproducts(String seletedproducts) {
        this.seletedproducts = seletedproducts;
    }

    /**
     * @return the lastlogin
     */
    public Date getLastlogin() {
        return lastlogin;
    }

    /**
     * @param lastlogin the lastlogin to set
     */
    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

    /**
     * @return the userattempts
     */
    public int getUserattempts() {
        return userattempts;
    }

    /**
     * @param userattempts the userattempts to set
     */
    public void setUserattempts(int userattempts) {
        this.userattempts = userattempts;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the selstatus
     */
    public String getSelstatus() {
        return selstatus;
    }

    /**
     * @param selstatus the selstatus to set
     */
    public void setSelstatus(String selstatus) {
        this.selstatus = selstatus;
    }

    /**
     * @return the createddatetime
     */
    public Date getCreateddatetime() {
        return createddatetime;
    }

    /**
     * @param createddatetime the createddatetime to set
     */
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    /**
     * @return the lastupdateddatetime
     */
    public Date getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    /**
     * @param lastupdateddatetime the lastupdateddatetime to set
     */
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the languageskills
     */
    public Integer[] getLanguageskills() {
        return languageskills;
    }

    /**
     * @param languageskills the languageskills to set
     */
    public void setLanguageskills(Integer[] languageskills) {
        this.languageskills = languageskills;
    }

    /**
     * @return the productsup
     */
    public Integer[] getProductsup() {
        return productsup;
    }

    /**
     * @param productsup the productsup to set
     */
    public void setProductsup(Integer[] productsup) {
        this.productsup = productsup;
    }

    /**
     * @return the territoriesup
     */
    public Integer[] getTerritoriesup() {
        return territoriesup;
    }

    /**
     * @param territoriesup the territoriesup to set
     */
    public void setTerritoriesup(Integer[] territoriesup) {
        this.territoriesup = territoriesup;
    }

    /**
     * @return the productscategorytypeup
     */
    public Integer[] getProductscategorytypeup() {
        return productscategorytypeup;
    }

    /**
     * @param productscategorytypeup the productscategorytypeup to set
     */
    public void setProductscategorytypeup(Integer[] productscategorytypeup) {
        this.productscategorytypeup = productscategorytypeup;
    }

    /**
     * @return the supervisorsup
     */
    public Integer[] getSupervisorsup() {
        return supervisorsup;
    }

    /**
     * @param supervisorsup the supervisorsup to set
     */
    public void setSupervisorsup(Integer[] supervisorsup) {
        this.supervisorsup = supervisorsup;
    }

    /**
     * @return the epf
     */
    public String getEpf() {
        return epf;
    }

    /**
     * @param epf the epf to set
     */
    public void setEpf(String epf) {
        this.epf = epf;
    }

    /**
     * @return the usercode
     */
    public String getUsercode() {
        return usercode;
    }

    /**
     * @param usercode the usercode to set
     */
    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    /**
     * @return the searchKeyword
     */
    public String getSearchKeyword() {
        return searchKeyword;
    }

    /**
     * @param searchKeyword the searchKeyword to set
     */
    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

}
