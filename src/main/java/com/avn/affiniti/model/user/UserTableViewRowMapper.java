/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONObject;
import org.springframework.jdbc.core.RowMapper;

/**
 * @Author : Roshen Dilshan
 * @Document : UserTableViewRowMapper
 * @Created on : Jul 30, 2016, 1:27:53 AM
 */
public class UserTableViewRowMapper implements RowMapper<JSONObject> {

    @Override
    public JSONObject mapRow(ResultSet rs, int rowNum) throws SQLException {
        JSONObject object = new JSONObject();
        object.put("employeeid", rs.getString("EMPLOYEEID"));
//        object.put("username", rs.getString("USERID"));
        object.put("username", rs.getString("USERNAME"));
        object.put("user_role", rs.getString("USERROLE"));
        object.put("preferred_name", rs.getString("PREFERREDNAME"));
        object.put("surname", rs.getString("SURENAME"));
        object.put("status", rs.getString("STATUS"));
        object.put("created_time", rs.getString("CREATEDDATETIME"));
        object.put("lastupdated_time", rs.getString("LASTUPDATEDDATETIME"));
        object.put("created_user", rs.getString("CREATEDUSER"));
        object.put("lastupdated_user", rs.getString("LASTUPDATEDUSER"));
        object.put("invalis_user_attempts", rs.getString("USERATTEMPTS"));
        String reset_action = "--";
        object.put("reset_attempts", reset_action);
        String action = "<div class=\"row\">"
                //                + "<div class=\"col-xs-3\"><a href=\"javascript:void(0);\" onclick=\"resetPassword('" + rs.getString("USERID") + "')\"><i class=\"fa fa-lg fa-fw fa-key\" title=\"Re-Set Password\"></i></a></div>"
                + "<div class=\"col-xs-3\"><a href='%s/user/view?employeeid=" + rs.getString("EMPLOYEEID") + "'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                + "<div class=\"col-xs-3\"><a href='%s/user/update/view?employeeid=" + rs.getString("EMPLOYEEID") + "'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                + "</div>";
        object.put("action", action);
        return object;
    }

}
