/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.dataobject;

/**
 *
 * @author chandima
 */
public class SubsectionDataObject {

    int userrolid;
    int sectionid;

    public int getUserrolid() {
        return userrolid;
    }

    public void setUserrolid(int userrolid) {
        this.userrolid = userrolid;
    }

    public int getSectionid() {
        return sectionid;
    }

    public void setSectionid(int sectionid) {
        this.sectionid = sectionid;
    }

}
