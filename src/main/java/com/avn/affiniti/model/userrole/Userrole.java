/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.userrole;

import java.util.Date;

/**
 *
 * @author chandima
 */
public class Userrole {

    private Integer userroleid;
    private int status;
    private String description;
    private int userroletype;
    private String userrolecode;
    private Date createdatetime;
    private Date lastupdateddatetime;
    private String createduser;
    private int parentuserrole;

    public int getParentuserrole() {
        return parentuserrole;
    }

    public void setParentuserrole(int parentuserrole) {
        this.parentuserrole = parentuserrole;
    }


    public Integer getUserroleid() {
        return userroleid;
    }

    public void setUserroleid(Integer userroleid) {
        this.userroleid = userroleid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserroletype() {
        return userroletype;
    }

    public void setUserroletype(int userroletype) {
        this.userroletype = userroletype;
    }

    public String getUserrolecode() {
        return userrolecode;
    }

    public void setUserrolecode(String userrolecode) {
        this.userrolecode = userrolecode;
    }

    public Date getCreatedatetime() {
        return createdatetime;
    }

    public void setCreatedatetime(Date createdatetime) {
        this.createdatetime = createdatetime;
    }

    public Date getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

}
