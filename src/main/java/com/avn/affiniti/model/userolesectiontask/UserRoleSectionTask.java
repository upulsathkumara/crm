/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.model.userolesectiontask;

/**
 *
 * @author Kaushan Fernando
   @Document userrolesectiontask
   @Created on: Oct 14, 2017, 2:30:23 PM
 */
public class UserRoleSectionTask {
    private String sectionId;
    private String userroleId;
    private String taskId;
    private String createddatetime;
    private String createduser;
    private String description;

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getUserroleId() {
        return userroleId;
    }

    public void setUserroleId(String userroleId) {
        this.userroleId = userroleId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCreateddatetime() {
        return createddatetime;
    }

    public void setCreateddatetime(String createddatetime) {
        this.createddatetime = createddatetime;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }
    
    public String getDescription() {
        return this.description;
    }

}
