/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.userroletype;

import com.avn.affiniti.hibernate.model.Status;
import java.util.Date;

/**
 *
 * @author chandima
 */
public class Userroletype {

    private Integer userroletypeid;
    private Status status;
    private String description;
    private Date createdatetime;
    private Date lastupdateddatetime;
    private String createduser;

    public Integer getUserroletypeid() {
        return userroletypeid;
    }

    public void setUserroletypeid(Integer userroletypeid) {
        this.userroletypeid = userroletypeid;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedatetime() {
        return createdatetime;
    }

    public void setCreatedatetime(Date createdatetime) {
        this.createdatetime = createdatetime;
    }

    public Date getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

}
