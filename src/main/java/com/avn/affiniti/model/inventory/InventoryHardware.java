/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.model.inventory;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryHadware
 * @Created on : Jun 27, 2017, 9:41:17 AM
 */
public class InventoryHardware {
    
    private String hardwareitemid;
    private String category;
    private String partcode;
    private String partname;
    
    private String posrevision;
    private String pospartid;
    private String posmac;
    private String posptid;
    private String posserialnumber;
    private String location;
    
    private String createduser;
    
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date addeddate;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date updateddate;

    /**
     * @return the hardwareitemid
     */
    public String getHardwareitemid() {
        return hardwareitemid;
    }

    /**
     * @param hardwareitemid the hardwareitemid to set
     */
    public void setHardwareitemid(String hardwareitemid) {
        this.hardwareitemid = hardwareitemid;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the partcode
     */
    public String getPartcode() {
        return partcode;
    }

    /**
     * @param partcode the partcode to set
     */
    public void setPartcode(String partcode) {
        this.partcode = partcode;
    }

    /**
     * @return the partname
     */
    public String getPartname() {
        return partname;
    }

    /**
     * @param partname the partname to set
     */
    public void setPartname(String partname) {
        this.partname = partname;
    }

    /**
     * @return the posrevision
     */
    public String getPosrevision() {
        return posrevision;
    }

    /**
     * @param posrevision the posrevision to set
     */
    public void setPosrevision(String posrevision) {
        this.posrevision = posrevision;
    }

    /**
     * @return the pospartid
     */
    public String getPospartid() {
        return pospartid;
    }

    /**
     * @param pospartid the pospartid to set
     */
    public void setPospartid(String pospartid) {
        this.pospartid = pospartid;
    }

    /**
     * @return the posmac
     */
    public String getPosmac() {
        return posmac;
    }

    /**
     * @param posmac the posmac to set
     */
    public void setPosmac(String posmac) {
        this.posmac = posmac;
    }

    /**
     * @return the posptid
     */
    public String getPosptid() {
        return posptid;
    }

    /**
     * @param posptid the posptid to set
     */
    public void setPosptid(String posptid) {
        this.posptid = posptid;
    }

    /**
     * @return the posserialnumber
     */
    public String getPosserialnumber() {
        return posserialnumber;
    }

    /**
     * @param posserialnumber the posserialnumber to set
     */
    public void setPosserialnumber(String posserialnumber) {
        this.posserialnumber = posserialnumber;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the addeddate
     */
    public Date getAddeddate() {
        return addeddate;
    }

    /**
     * @param addeddate the addeddate to set
     */
    public void setAddeddate(Date addeddate) {
        this.addeddate = addeddate;
    }

    /**
     * @return the updateddate
     */
    public Date getUpdateddate() {
        return updateddate;
    }

    /**
     * @param updateddate the updateddate to set
     */
    public void setUpdateddate(Date updateddate) {
        this.updateddate = updateddate;
    }
    
}
