/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.inventory;

import com.avn.affiniti.hibernate.model.Employee;
import java.util.Date;

/**
 *
 * @author chandima
 */
public class Inventoryterminalitem {

    private Integer inventoryterminalitemid;
    private int client;
    private Employee employee;
    private int inventorycategory;
    private int terminalbrand;
    private int terminalmodel;
    private String serialno;
    private String tid;
    private String mid;
    private Date lastupdateddatetime;
    private Date createddatetime;
    private String createduser;
    private int location;

    /**
     * @return the inventoryterminalitemid
     */
    public Integer getInventoryterminalitemid() {
        return inventoryterminalitemid;
    }

    /**
     * @param inventoryterminalitemid the inventoryterminalitemid to set
     */
    public void setInventoryterminalitemid(Integer inventoryterminalitemid) {
        this.inventoryterminalitemid = inventoryterminalitemid;
    }

    /**
     * @return the client
     */
    public int getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(int client) {
        this.client = client;
    }

    /**
     * @return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * @return the inventorycategory
     */
    public int getInventorycategory() {
        return inventorycategory;
    }

    /**
     * @param inventorycategory the inventorycategory to set
     */
    public void setInventorycategory(int inventorycategory) {
        this.inventorycategory = inventorycategory;
    }

    /**
     * @return the terminalbrand
     */
    public int getTerminalbrand() {
        return terminalbrand;
    }

    /**
     * @param terminalbrand the terminalbrand to set
     */
    public void setTerminalbrand(int terminalbrand) {
        this.terminalbrand = terminalbrand;
    }

    /**
     * @return the terminalmodel
     */
    public int getTerminalmodel() {
        return terminalmodel;
    }

    /**
     * @param terminalmodel the terminalmodel to set
     */
    public void setTerminalmodel(int terminalmodel) {
        this.terminalmodel = terminalmodel;
    }

    /**
     * @return the serialno
     */
    public String getSerialno() {
        return serialno;
    }

    /**
     * @param serialno the serialno to set
     */
    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the lastupdateddatetime
     */
    public Date getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    /**
     * @param lastupdateddatetime the lastupdateddatetime to set
     */
    public void setLastupdateddatetime(Date lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    /**
     * @return the createddatetime
     */
    public Date getCreateddatetime() {
        return createddatetime;
    }

    /**
     * @param createddatetime the createddatetime to set
     */
    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the location
     */
    public int getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(int location) {
        this.location = location;
    }
    

    
}
