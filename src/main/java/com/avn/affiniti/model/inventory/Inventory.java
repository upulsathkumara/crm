/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.inventory;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Author : Nuwan Fernando
 * @Document : Inventory
 * @Created on : Jun 6, 2017, 8:54:36 AM
 */
public class Inventory {

//    private String itemcode;
    private String terminalitemid;
    private String brand;
    private String model;
    private String serialnumber;
    private String category;
    private String owner;
    private String client;
    private String tid;
    private String mid;
    private String location;

    @DateTimeFormat(pattern = "yyyy-mm-dd")  //yyyy-mm-dd
    private Date addeddate;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date updateddate;
    private String createduser;

    /**
     * @return the terminalitemid
     */
    public String getTerminalitemid() {
        return terminalitemid;
    }

    /**
     * @param terminalitemid the terminalitemid to set
     */
    public void setTerminalitemid(String terminalitemid) {
        this.terminalitemid = terminalitemid;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the serialnumber
     */
    public String getSerialnumber() {
        return serialnumber;
    }

    /**
     * @param serialnumber the serialnumber to set
     */
    public void setSerialnumber(String serialnumber) {
        this.serialnumber = serialnumber;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the tid
     */
    public String getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the addeddate
     */
    public Date getAddeddate() {
        return addeddate;
    }

    /**
     * @param addeddate the addeddate to set
     */
    public void setAddeddate(Date addeddate) {
        this.addeddate = addeddate;
    }

    /**
     * @return the updateddate
     */
    public Date getUpdateddate() {
        return updateddate;
    }

    /**
     * @param updateddate the updateddate to set
     */
    public void setUpdateddate(Date updateddate) {
        this.updateddate = updateddate;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

}
