/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.model.clientsystemuser;

/**
 * @Author Nadun Chamikara
 * @Document SystemUser
 * @Created on 04/09/2017, 4:33:40 PM
 */
public class ClientSystemUser {
    private String username;
    private String token;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
