/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.model.tempuserrolesubsection;

/**
 *
 * @author chandima
 */
public class TempUserrolesubsection {

    private int userroleid;
    private int sectionid;
    private int subsectionid;

    public int getUserroleid() {
        return userroleid;
    }

    public void setUserroleid(int userroleid) {
        this.userroleid = userroleid;
    }

    public int getSectionid() {
        return sectionid;
    }

    public void setSectionid(int sectionid) {
        this.sectionid = sectionid;
    }

    public int getSubsectionid() {
        return subsectionid;
    }

    public void setSubsectionid(int subsectionid) {
        this.subsectionid = subsectionid;
    }

}
