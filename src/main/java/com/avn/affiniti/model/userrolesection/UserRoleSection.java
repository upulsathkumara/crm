/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.model.userrolesection;

import java.util.Date;

/**
 * @Author Nadun Chamikara
 * @Document Userrolesection
 * @Created on 13/08/2017, 11:57:22 AM
 */
public class UserRoleSection {
    private Integer userRoleId;
    private Integer[] sectionIds;

    public Integer getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Integer[] getSectionIds() {
        return sectionIds;
    }

    public void setSectionIds(Integer[] sectionIds) {
        this.sectionIds = sectionIds;
    }

    
}
