/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.varlist;

/**
 * @Author : Roshen Dilshan
 * @Document : DropdownSqlVarList
 * @Created on : Jul 18, 2016, 10:28:09 AM
 */
public class DropdownSqlVarList {

    public static final String TEST = "SELECT TITLEID AS ID,DESCRIPTION AS DESCRIPTION FROM title";

    public static final String AFFINITI_DROPDOWN_OWNER = "SELECT EMPLOYEEID AS ID, NAMEINFULL AS DESCRIPTION FROM EMPLOYEE ORDER BY NAMEINFULL";

    public static final String AFFINITI_DROPDOWN_CLIENT = "SELECT C.CLIENTID AS ID, C.NAME AS DESCRIPTION FROM client C "
            + "INNER JOIN clientclientcategory CCC ON CCC.`CLIENT` = C.CLIENTID "
            + "INNER JOIN clientcategory CC ON CC.CLIENTCATEGORYID = CCC.CLIENTCATEGORY "
            + "WHERE CC.CLIENTCATEGORYID = %d ORDER BY C.NAME";
    
    public static final String AFFINITI_DROPDOWN_CLIENT_TO_LOCATION = "SELECT C.CLIENTID AS ID, C.NAME AS DESCRIPTION FROM client C "
            + "INNER JOIN clientclientcategory CCC ON CCC.`CLIENT` = C.CLIENTID "
            + "INNER JOIN clientcategory CC ON CC.CLIENTCATEGORYID = CCC.CLIENTCATEGORY "
            + "WHERE C.LOCATION = %d "
            + "AND CC.CLIENTCATEGORYID = %d "
            + "AND C.STATUS = 1 ORDER BY C.NAME ";
    
    public static final String AFFINITI_DROPDOWN_CLIENT_BY_LOCATION = "SELECT C.CLIENTID AS ID, C.NAME AS DESCRIPTION FROM client C "
            + "INNER JOIN clientclientcategory CCC ON CCC.`CLIENT` = C.CLIENTID "
            + "INNER JOIN clientcategory CC ON CC.CLIENTCATEGORYID = CCC.CLIENTCATEGORY "
            + "WHERE CC.CLIENTCATEGORYID = %d "
            +" AND C.LOCATION = %d AND C.STATUS = %d ORDER BY C.NAME";
    
    public static final String AFFINITI_LOCATION_BY_USERNAME = "SELECT EM.LOCATION FROM employee EM "
            +"WHERE EM.EMPLOYEEID = "
            +"(SELECT SU.USERID FROM systemuser SU "
            +"WHERE SU.USERNAME = %s )";
    
    public static final String AFFINITI_DROPDOWN_PRODUCT_BRAND = "SELECT INVENTORYPRODUCTCATEGORYID AS ID, DESCRIPTION FROM inventoryproductcategory ORDER BY DESCRIPTION";

    public static final String AFFINITI_DROPDOWN_INVENTORY_CATEGORY = "SELECT INVENTORYCATEGORYID AS ID, DESCRIPTION FROM inventorycategory ORDER BY DESCRIPTION";

    public static final String AFFINITI_DROPDOWN_INVENTORY_TERMINAL_CATEGORY = "SELECT IC.INVENTORYCATEGORYID AS ID, IC.DESCRIPTION "
            + "FROM inventorycategory IC "
            + "INNER JOIN inventorycategorytype ICT ON IC.INVENTORYCATEGORYTYPE = ICT.INVENTORYCATEGORYTYPEID "
            + "WHERE ICT.DESCRIPTION = 'Terminal Item'";

    public static final String AFFINITI_DROPDOWN_INVENTORY_HARWADRE_CATEGORY = "SELECT IC.INVENTORYCATEGORYID AS ID, IC.DESCRIPTION "
            + "FROM inventorycategory IC "
            + "INNER JOIN inventorycategorytype ICT ON IC.INVENTORYCATEGORYTYPE = ICT.INVENTORYCATEGORYTYPEID "
            + "WHERE ICT.DESCRIPTION = 'Hardware Item'";

//    public static final String AFFINITI_DROPDOWN_STATUS_BY_CATRGORY = "SELECT STATUSID AS ID, "
//            + "       DESCRIPTION "
//            + "FROM status "
//            + "WHERE CATEGORYID = %d "
//            + "ORDER BY DESCRIPTION";
//    public static final String AFFINITI_DROPDOWN_LOYALTY_REDEMPTION_TYPES = "SELECT ID, "
//            + "DESCRIPTION "
//            + "FROM loyaltyschemeredemptiontypes "
//            + "ORDER BY DESCRIPTION";
//    public static final String AFFINITI_DROPDOWN_LOYALTY_TRANSACTION_TYPES = "SELECT ID, "
//            + "DESCRIPTION "
//            + "FROM loyaltyschemetransactiontypes "
//            + "ORDER BY DESCRIPTION";
//    public static final String AFFINITI_DROPDOWN_LOYALTY_COMPANIES = "SELECT ID, "
//            + "DESCRIPTION "
//            + "FROM loyaltyschemecompanies "
//            + "ORDER BY DESCRIPTION";
//    public static String AFFINITI_DROPDOWN_PRODUCT_BY_SCHEME = "SELECT PD.PRODUCTID AS ID, "
//            + "       PD.DESCRIPTION "
//            + "FROM loyaltyschemeproducts LP "
//            + "INNER JOIN product PD ON LP.PRODUCTID = PD.PRODUCTID "
//            + "WHERE LP.LOYALTYSCHEMEID = %d";
//    public static String AFFINITI_DROPDOWN_CUSTOMERSEGMENTS_BY_SCHEME = "SELECT CS.SEGMENTID AS ID, "
//            + "       CS.DESCRIPTION "
//            + "FROM loyaltyschemecustomersegments LCS "
//            + "INNER JOIN customersegment CS ON LCS.SEGMENTID = CS.SEGMENTID "
//            + "WHERE LCS.LOYALTYSCHEMEID = %d";
//    public static String AFFINITI_DROPDOWN_LOYALTY_SCHEMES_BY_MARKETING_CAMPAIGN = "SELECT LS.LOYALTYSCHEMEID AS ID, "
//            + "       LS.LOYALTYCODE AS DESCRIPTION "
//            + "FROM loyaltymarketingschemes LMS "
//            + "INNER JOIN loyaltyscheme LS ON LMS.LOYALTYSCHEMEID = LS.LOYALTYSCHEMEID "
//            + "WHERE LMS.LOYALTYMARKETINGID = %d";
//    public static String AFFINITI_DROPDOWN_CUSTOMERSEGMENTS_BY_MARKETING_CAMPAIGN = "SELECT CS.SEGMENTID AS ID, "
//            + "       CS.DESCRIPTION "
//            + "FROM loyaltymarketingsegments LMS "
//            + "INNER JOIN customersegment CS ON LMS.SEGMENTID = CS.SEGMENTID "
//            + "WHERE LMS.LOYALTYMARKETINGID = %d";
    public static final String AFFINITI_DROPDOWN_ORGANIZATIONHIERARCHY = "SELECT HIERACHYID AS ID, "
            + "       DESCRIPTION "
            + "FROM organizationhierarchy "
            + "ORDER BY SORTID";
    public static final String AFFINITI_DROPDOWN_TERRITORY_BY_HIERARCHY = "SELECT TERRITORYMAPID AS ID, "
            + "       DESCRIPTION "
            + "FROM territorymap "
            + "WHERE TERRITORYID IN "
            + "    (SELECT TERRITORYID "
            + "     FROM territoryhierarchy "
            + "     WHERE HIERACHYID = %d) "
            + "ORDER BY DESCRIPTION";
    public static final String AFFINITI_DROPDOWN_PRODUCT = "SELECT PRODUCTID AS ID, "
            + "       DESCRIPTION "
            + "FROM product "
            + "WHERE STATUS = %d "
            + "ORDER BY SORTID";
    public static final String AFFINITI_DROPDOWN_TERRITORY_BY_HIERARCHY_PRODUCT = "SELECT TERRITORYMAPID AS ID, "
            + "       DESCRIPTION "
            + "FROM territorymap "
            + "WHERE TERRITORYID IN "
            + "    (SELECT TERRITORYID "
            + "     FROM territoryhierarchy "
            + "     WHERE HIERACHYID = %d) "
            + "  AND TERRITORYMAPID IN "
            + "    (SELECT TERRITORYMAPID "
            + "     FROM territorymapproductsupervisor "
            + "     WHERE PRODUCTID IN (%s)) "
            + "ORDER BY DESCRIPTION";
    public static String AFFINITI_DROPDOWN_USERROLE = "SELECT USERROLEID AS ID, "
            + "       DESCRIPTION "
            + "FROM userrole "
            + "ORDER BY DESCRIPTION";
    public static String AFFINITI_DROPDOWN_CLIENT_USERROLES = "SELECT USERROLEID AS ID, "
            + "       DESCRIPTION "
            + "FROM userrole "
            + "WHERE USERROLEID IN (%s)"
            + "ORDER BY DESCRIPTION";
    public static String AFFINITI_DROPDOWN_USERROLE_BY_PARENTUSERROLE = "SELECT USERROLEID AS ID, "
            + "       DESCRIPTION "
            + "FROM userrole "
            + "WHERE PARENTUSERROLE = %d "
            + "ORDER BY DESCRIPTION";
    public static String AFFINITI_DROPDOWN_USER_EMPLOYEE_PRODUCT = "SELECT PD.PRODUCTID AS ID, "
            + "       PD.DESCRIPTION "
            + "FROM employeeproduct EP "
            + "INNER JOIN product PD ON EP.PRODUCTID = PD.PRODUCTID "
            + "WHERE EP.EMPLOYEEID = %d";
    public static String AFFINITI_DROPDOWN_USER_TERRIROTYMAP_EMPLOYEE = "SELECT TM.TERRITORYMAPID AS ID, "
            + "       TM.DESCRIPTION "
            + "FROM territorymapemployee TMPE "
            + "INNER JOIN territorymap TM ON TM.TERRITORYMAPID = TMPE.TERRITORYMAPID "
            + "WHERE TMPE.EMPLOYEEID = %d";
    public static String AFFINITI_DROPDOWN_PRODUCTCATEGORYTYPE_BY_PRODUCT = "SELECT PCT.PRODUCTCATEGORYTYPEID AS ID, PCT.DESCRIPTION "
            + "  FROM productcategories PDC "
            + "       INNER JOIN productcategory PC "
            + "          ON PDC.PRODUCTCATEGORYID = PC.PRODUCTCATEGORYID "
            + "       INNER JOIN productcategorytype PCT "
            + "          ON PC.PRODUCTCATEGORYTYPEID = PCT.PRODUCTCATEGORYTYPEID "
            + " WHERE PDC.PRODUCTID IN (%s) "
            + "GROUP BY PCT.PRODUCTCATEGORYTYPEID, PCT.DESCRIPTION "
            + "ORDER BY PCT.DESCRIPTION";
    public static String AFFINITI_DROPDOWN_SUPERVISOR_BY_HIERARCHYID = "SELECT EM.EMPLOYEEID AS ID, EM.NAMEINFULL AS DESCRIPTION "
            + "  FROM employee EM "
            + " WHERE EM.HIERARCHYID IN (SELECT HIERACHYID "
            + "                            FROM organizationhierarchy "
            + "                           WHERE LEVEL < (SELECT LEVEL "
            + "                                            FROM organizationhierarchy "
            + "                                           WHERE HIERACHYID = %d))";
//    public static String AFFINITI_DROPDOWN_PRODUCTCATEGORYTYPE_BY_EMPLOYEE = "SELECT PCT.PRODUCTCATEGORYTYPEID AS ID, PCT.DESCRIPTION "
//            + "  FROM employeeproductcatgorytype EPCT "
//            + "       INNER JOIN productcategorytype PCT "
//            + "          ON EPCT.PRODUCTCATEGORYTYPEID = PCT.PRODUCTCATEGORYTYPEID "
//            + " WHERE EPCT.EMPLOYEEID = %d "
//            + "GROUP BY PCT.PRODUCTCATEGORYTYPEID, PCT.DESCRIPTION "
//            + "ORDER BY PCT.DESCRIPTION";
    public static String AFFINITI_DROPDOWN_PRODUCTCATEGORYTYPE_BY_EMPLOYEE = "SELECT DISTINCT PCT.PRODUCTCATEGORYTYPEID AS ID , PCT.DESCRIPTION AS DESCRIPTION  FROM employeeproductcatgorytype EP INNER JOIN productcategory PC ON EP.PRODUCTCATEGORYTYPEID=PC.PRODUCTCATEGORYID "
            + "INNER JOIN productcategorytype PCT ON PC.PRODUCTCATEGORYTYPEID=PCT.PRODUCTCATEGORYTYPEID WHERE EP.EMPLOYEEID=%d "
            + "GROUP BY PCT.PRODUCTCATEGORYTYPEID, PCT.DESCRIPTION "
            + "ORDER BY PCT.DESCRIPTION";
    public static String AFFINITI_DROPDOWN_SUPERVISOR_BY_EMPLOYEE = "SELECT E.EMPLOYEEID AS ID, E.NAMEINFULL AS DESCRIPTION "
            + "  FROM employeesupervisor ES "
            + "       INNER JOIN employee E ON ES.SUPERVISOR = E.EMPLOYEEID "
            + " WHERE ES.EMPLOYEEID = %d "
            + "GROUP BY E.EMPLOYEEID , E.NAMEINFULL "
            + "ORDER BY E.NAMEINFULL";
    public static String AFFINITI_DROPDOWN_LANGUAGE_SKILLS = "SELECT LANGUAGEID AS ID , DESCRIPTION AS DESCRIPTION FROM preflanguage ORDER BY SORTID";
    public static String AFFINITI_DROPDOWN_LANGUAGE_SKILLS_BY_EMPLOYEE = "SELECT ES.LANGUAGEID AS ID, "
            + "       PL.DESCRIPTION AS DESCRIPTION "
            + "FROM employeelanguageskills ES "
            + "INNER JOIN preflanguage PL ON ES.LANGUAGEID=PL.LANGUAGEID "
            + "WHERE EMPID=%d";


    /*Added By Roshen Dilshan Silva*/
    public static final String AFFINITI_DROPDOWN_STATUS_BY_CATRGORY = "SELECT S.STATUSID AS ID, S.DESCRIPTION AS DESCRIPTION "
            + "FROM status S "
            + "WHERE S.STATUCATEGORY = %d "
            + "ORDER BY S.SORTID";
    public static final String AFFINITI_DROPDOWN_STATUS_BY_STATUSCODE = "SELECT S.STATUSID AS ID, S.DESCRIPTION AS DESCRIPTION "
            + "FROM status S "
            + "WHERE S.STATUSCODE IN (%s) "
            + "ORDER BY S.SORTID";
    public static final String AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE = "SELECT S.STATUSID AS ID, S.DESCRIPTION AS DESCRIPTION "
            + "FROM status    S "
            + "     INNER JOIN ticketcategorystatus TCS ON S.STATUSID = TCS.STATUS "
            + "     INNER JOIN ticketflow TF "
            + "        ON     TCS.TICKETCATEGORYSTATUSID = TF.TRANTICKETSTATUS "
            + "           AND TF.INITTICKETSTATUS = %d "
            + "     INNER JOIN ticketcategorystatususerrole TCSUR "
            + "        ON     TCS.TICKETCATEGORYSTATUSID = TCSUR.TICKETCATEGORYSTATUS "
            + "           AND TCSUR.USERROLE = (SELECT SU.USERROLE "
            + "                                 FROM systemuser SU "
            + "                                 WHERE SU.USERNAME = '%s')";
    public static final String AFFINITI_DROPDOWN_TICKET_PRIORITY = "SELECT TP.TICKETPRIORITYID AS ID, TP.DESCRIPTION AS DESCRIPTION "
            + "FROM ticketpriority TP "
            + "WHERE TP.STATUS = %d "
            + "ORDER BY TP.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_TICKET_CATEGORY = "SELECT TC.TICKETCATEGORYID AS ID, TC.DESCRIPTION AS DESCRIPTION "
            + "FROM ticketcategory TC "
            + "INNER JOIN ticketcategoryuserrole TCU ON TCU.TICKETCATEGORY = TC.TICKETCATEGORYID "
            + "INNER JOIN systemuser SU ON SU.USERROLE = TCU.USERROLE "
            + "WHERE TC.STATUS = %d AND SU.USERNAME = '%s' "
            + "ORDER BY TC.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_TICKET_CREATE_STATUS = "SELECT S.STATUSID AS ID, S.DESCRIPTION AS DESCRIPTION "
            + "FROM ticketcategorystatus    TCS "
            + "     INNER JOIN status S ON TCS.STATUS = S.STATUSID "
            + "WHERE TCS.TICKETCATEGORY = %d AND S.ISINITIALSTATUS = 1 "
            + "ORDER BY TCS.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_NATUREOFFAULT = "SELECT NOF.NATUREOFFAULTID AS ID, NOF.DESCRIPTION AS DESCRIPTION "
            + "FROM natureoffault NOF "
            + "WHERE NOF.STATUS = %d "
            + "ORDER BY NOF.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_ACTIONTOBETAKEN = "SELECT ATT.ACTIONTOBETAKENID AS ID, ATT.DESCRIPTION AS DESCRIPTION "
            + "FROM actiontobetaken ATT "
            + "WHERE ATT.STATUS = %d "
            + "ORDER BY ATT.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_DISTRICT = "SELECT D.DISTRICTID AS ID, D.DESCRIPTION AS DESCRIPTION "
            + "FROM district D "
            + "WHERE D.LOCATION = %d "
            + "AND D.STATUS = %d "
            + "ORDER BY D.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_REPORTEDBY = "SELECT RB.REPORTEDID AS ID, RB.DESCRIPTION AS DESCRIPTION "
            + "FROM reportedby RB "
            + "WHERE RB.STATUS = %d "
            + "ORDER BY RB.SORTID ASC";
    public static final String AFFINITI_DROPDOWN_ASSIGNEETYPELIST = "SELECT UR.USERROLEID AS ID, UR.DESCRIPTION AS DESCRIPTION "
            + "FROM userrole UR "
            + "WHERE UR.USERROLECODE IN ('IFS', 'EM') AND UR.STATUS = %d "
            + "ORDER BY UR.DESCRIPTION ASC";
    public static final String AFFINITI_DROPDOWN_TERMINALBRAND = "SELECT TB.TERMINALBRANDID AS ID, TB.DESCRIPTION AS DESCRIPTION "
            + "FROM terminalbrand TB "
            + "WHERE TB.STATUS = %d AND TB.LOCATIONID = %d "
            + "ORDER BY TB.SORTID";
    public static final String AFFINITI_DROPDOWN_TERMINALMODEL = "SELECT TM.TERMINALMODELID AS ID, TM.DESCRIPTION AS DESCRIPTION "
            + "FROM terminalmodel TM "
            + "WHERE TM.STATUS = %d "
            + "ORDER BY TM.SORTID";
    public static final String AFFINITI_DROPDOWN_BANK = "SELECT B.BANKID AS ID, B.DESCRIPTION AS DESCRIPTION "
            + "FROM bank B "
            + "WHERE B.STATUS = %d "
            + "ORDER BY B.SORTID";
    public static final String AFFINITI_DROPDOWN_DELEVERYREQUIREMENT = "SELECT DR.DELEVERYREQUIEREMENTID AS ID, DR.DESCRIPTION AS DESCRIPTION "
            + "FROM deleveryrequirement DR "
            + "WHERE DR.STATUS = %d "
            + "ORDER BY DR.SORTID";
    public static final String AFFINITI_DROPDOWN_ESP = "SELECT E.ESPID AS ID, E.DESCRIPTION AS DESCRIPTION "
            + "FROM esp E "
            + "WHERE E.STATUS = %d "
            + "ORDER BY E.SORTID";
    public static final String AFFINITI_DROPDOWN_PROFILETYPE = "SELECT P.PROFILETYPEID AS ID, P.DESCRIPTION AS DESCRIPTION "
            + "FROM profiletype P "
            + "WHERE P.STATUS = %d "
            + "ORDER BY P.SORTID";
    public static final String AFFINITI_DROPDOWN_TICKETATTRIBUTE_STATUS = "SELECT TS.TICKETATTRIBUTESTATUSID AS ID, TS.DESCRIPTION AS DESCRIPTION "
            + "FROM ticketattributestatus TS "
            + "WHERE TS.STATUS = %d "
            + "ORDER BY TS.SORTID";
    public static final String AFFINITI_DROPDOWN_CAUSEOFFAULT = "SELECT COF.CAUSEOFFAULTID AS ID, COF.DESCRIPTION AS DESCRIPTION "
            + "FROM causeoffault COF "
            + "WHERE COF.STATUS = %d "
            + "ORDER BY COF.SORTID";
    public static final String AFFINITI_DROPDOWN_ACTIONTAKEN = "SELECT AT.ACTIONTAKENID AS ID, AT.DESCRIPTION AS DESCRIPTION "
            + "FROM actiontaken AT "
            + "WHERE AT.STATUS = %d "
            + "ORDER BY AT.SORTID";
    public static final String AFFINITI_DROPDOWN_TERMINALMODELSBYBRAND = "SELECT TM.TERMINALMODELID AS ID, TM.DESCRIPTION AS DESCRIPTION "
            + "FROM terminalmodel TM "
            + "WHERE TM.STATUS = %d AND TM.TERMINALBRAND = %d "
            + "ORDER BY TM.SORTID";
    public static final String AFFINITI_DROPDOWN_ACCESSORYTYPE = "SELECT IHI.INVENTORYHARDWAREITEMID AS ID, IHI.PARTNAME AS DESCRIPTION "
            + "FROM inventoryhardwareitem IHI ";
    public static final String AFFINITI_DROPDOWN_TERMINALMODELS = "SELECT TM.TERMINALMODELID AS ID, TM.DESCRIPTION AS DESCRIPTION "
            + "FROM terminalmodel TM "
            + "WHERE TM.STATUS = %d "
            + "ORDER BY TM.SORTID";
    public static final String AFFINITI_DROPDOWN_TICKETINITIALSTATUS = "SELECT S.STATUSID AS ID, S.DESCRIPTION AS DESCRIPTION "
            + "FROM ticketcategorystatususerrole    TCSU "
            + "     INNER JOIN userrole UR ON TCSU.USERROLE = UR.USERROLEID "
            + "     INNER JOIN systemuser SU ON UR.USERROLEID = SU.USERROLE "
            + "     INNER JOIN ticketcategorystatus TCS "
            + "        ON TCSU.TICKETCATEGORYSTATUS = TCS.TICKETCATEGORYSTATUSID "
            + "     INNER JOIN status S ON TCS.STATUS = S.STATUSID "
            + "WHERE TCS.TICKETCATEGORY = %d AND SU.USERNAME = '%s'";
    public static final String AFFINITI_DROPDOWN_DELIVERYREQUIREMENT = "SELECT DR.DELEVERYREQUIEREMENTID AS ID, DR.DESCRIPTION AS DESCRIPTION "
            + "FROM deleveryrequirement DR "
            + "WHERE DR.STATUS = %d "
            + "ORDER BY DR.SORTID";
    public static final String AFFINITI_DROPDOWN_DEPLOYABLESTATUS = "SELECT DS.DEPLOYABLESTATUSID AS ID, DS.DESCRIPTION AS DESCRIPTION "
            + "FROM deployablestatus DS "
            + "WHERE DS.STATUS = %d "
            + "ORDER BY DS.SORTID";
    public static final String AFFINITI_DROPDOWN_TERMINALDELIVERYSTATUS = "SELECT TDS.TERMINALDELIVERYSTATUSID AS ID, TDS.DESCRIPTION AS DESCRIPTION "
            + "FROM teminaldeliverystatus TDS "
            + "WHERE TDS.STATUS = %d "
            + "ORDER BY TDS.SORTID";
    public static final String AFFINITI_DROPDOWN_PARTTYPE = "SELECT IC.INVENTORYCATEGORYID AS ID, "
            + "IC.DESCRIPTION AS DESCRIPTION "
            + "FROM inventorycategory IC "
            + "WHERE IC.INVENTORYCATEGORYID IN (1,3)";
    public static final String AFFINITI_DROPDOWN_PURPOSETYPE = "SELECT PT.PURPOSETYPEID AS ID, PT.DESCRIPTION AS DESCRIPTION "
            + "FROM purposetype PT "
            + "WHERE PT.STATUS = %d";
    public static final String AFFINITI_DROPDOWN_ASSIGNEELIST = "SELECT E.EMPLOYEEID AS ID, E.NAMEINFULL AS DESCRIPTION "
            + "FROM employee E "
            + "INNER JOIN systemuser SU ON SU.USERID = E.EMPLOYEEID "
            + "INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE "
            + "WHERE UR.USERROLECODE IN (%s) AND SU.STATUS = %d";
    public static final String AFFINITI_DROPDOWN_MANUAL_ASSIGNEELIST = "SELECT E.EMPLOYEEID AS ID, E.NAMEINFULL AS DESCRIPTION "
            + "FROM employee E "
            + "INNER JOIN systemuser SU ON SU.USERID = E.EMPLOYEEID "
            + "WHERE SU.USERROLE = (SELECT TKTSU.USERROLE "
            + "                     FROM ticket T "
            + "                     INNER JOIN systemuser TKTSU ON TKTSU.USERID = T.ASSIGNEE "
            + "                     WHERE T.TICKETID = %d) AND SU.USERID <> (SELECT TKT.ASSIGNEE FROM ticket TKT WHERE TKT.TICKETID = %d)";
    public static final String AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE = "SELECT E.EMPLOYEEID AS ID, E.NAMEINFULL AS DESCRIPTION "
            + "FROM ticket T "
            + "INNER JOIN employee E On E.EMPLOYEEID = T.ASSIGNEE "
            + "WHERE T.TICKETID = %d";
    /**
     * ******************** Added by Ashok *********************
     */
    public static final String AFFINITI_DROPDOWN_SECTIONBYID = "SELECT SECTIONID AS ID, DESCRIPTION FROM section WHERE SECTIONID = %d";
    public static final String AFFINITI_DROPDOWN_SECTIONLEVEL = "SELECT DISTINCT SECTIONLEVEL AS ID, SECTIONLEVEL AS DESCRIPTION FROM section ORDER BY SECTIONLEVEL ASC";
    public static final String AFFINITI_DROPDOWN_PARENTSECTION = "SELECT SECTIONID AS ID, DESCRIPTION FROM section WHERE SECTIONLEVEL < %d ORDER BY SORTID ASC";
    public static final String AFFINITI_DROPDOWN_NOTASSIGNSECTIONS = "SELECT SECTIONID AS ID, DESCRIPTION AS DESCRIPTION FROM Section WHERE SECTIONID NOT IN (SELECT distinct SECTIONID FROM Sectiontask)";

    public static final String AFFINITI_DROPDOWN_STATUSBYCATEGORY = "SELECT STATUSID AS ID, DESCRIPTION FROM status WHERE STATUCATEGORY = %d";

    public static final String AFFINITI_DROPDOWN_ALLSECTIONTASKS = "SELECT TASKID AS ID, DESCRIPTION FROM task";
    public static final String AFFINITI_DROPDOWN_ASSIGNEDSECTIONTASKS = "SELECT TASKID AS ID, DESCRIPTION FROM task WHERE TASKID IN (SELECT TASKID FROM sectiontask WHERE SECTIONID = %d)";
    public static final String AFFINITI_DROPDOWN_NOTASSIGNEDSECTIONTASKS = "SELECT TASKID AS ID, DESCRIPTION FROM task WHERE TASKID NOT IN (SELECT TASKID FROM sectiontask WHERE SECTIONID = %d)";

    public static final String AFFINITI_DROPDOWN_AUDITTRACE_AFFECTEDPAGES = "SELECT DISTINCT AFFECTEDPAGE AS ID, AFFECTEDPAGE AS DESCRIPTION FROM audittrace";
    public static final String AFFINITI_DROPDOWN_AUDITTRACE_TASKS = "SELECT DISTINCT TASK AS ID, TASK AS DESCRIPTION FROM audittrace";
    public static final String AFFINITI_DROPDOWN_AUDITTRACE_CREATEDUSERS = "SELECT DISTINCT CREATEDUSER AS ID, CREATEDUSER AS DESCRIPTION FROM audittrace";
    /**
     * ******************** Added by Haritha *********************
     */
    public static String AFFINITI_DROPDOWN_STATUSCATEGORY = "SELECT STATUSCATEGORYID AS ID, "
            + " DESCRIPTION AS DESCRIPTION"
            + " FROM STATUSCATEGORY "
            + " ORDER BY DESCRIPTION";
    public static String AFFINITY_DROPDOWN_PRODUCTCATEGORY = "SELECT PRODUCTCATEGORYID AS ID, "
            + " DESCRIPTION AS DESCRIPTION"
            + " FROM PRODUCTCATEGORY "
            + " ORDER BY DESCRIPTION";
    /**
     * ******************** Added by Chandima *********************
     */
    public static final String AFFINITI_DROPDOWN_SUBSECTION = "SELECT SUBSECTIONID AS ID,DESCRIPTION from "
            + "subsection sb where  sb.SECTIONID = %d "
            + "and sb.SUBSECTIONID not in( "
            + "select u.SUBSECTIONID "
            + "from userrolesubsection u,subsection s "
            + "where u.SUBSECTIONID=s.SUBSECTIONID and u.SECTIONID = %d "
            + "and u.USERROLEID = %d)";
    public static final String AFFINITI_DROPDOWN_ASIGNED_SUBSECTION = "SELECT sb.SUBSECTIONID AS ID , sb.DESCRIPTION AS DESCRIPTION from subsection sb where  sb.SUBSECTIONID in("
            + " select u.SUBSECTIONID"
            + " from userrolesubsection u,subsection s "
            + " where u.SUBSECTIONID=s.SUBSECTIONID and u.SECTIONID = %d and u.USERROLEID = %d)";
    public static String AFFINITI_DROPDOWN_SECTION = "SELECT SECTIONID AS ID, "
            + "       DESCRIPTION "
            + "FROM section "
            + "ORDER BY DESCRIPTION";
    /**
     * ******************** Added by Kaushan *********************
     */
    public static final String AFFINITI_DROPDOWN_SUBSECTIONS_BY_USERROLE_ID_AND_SECTION_ID = "SELECT SS.SUBSECTIONID AS ID,SS.DESCRIPTION AS DESCRIPTION "
            + "FROM subsection SS WHERE SS.SUBSECTIONID IN "
            + "(SELECT DISTINCT URSS.SUBSECTIONID "
            + "FROM userrolesubsection URSS WHERE URSS.USERROLEID = %d "
            + "AND URSS.SECTIONID = %d)";
    public static final String AFFINITI_DROPDOWN_PRODUCTCATEGORY_BY_CLIENTCATEGORY = "SELECT PC.PRODUCTCATEGORYID AS ID,PC.DESCRIPTION AS DESCRIPTION "
            + "FROM productcategory PC WHERE PC.CLIENTTYPE IN (%s)";
    public static final String AFFINITI_DROPDOWN_SUBSECTIONS = "SELECT SS.SUBSECTIONID AS ID,SS.DESCRIPTION AS DESCRIPTION "
            + "FROM subsection SS "
            + "ORDER BY DESCRIPTION";
    public static final String AFFINITI_DROPDOWN_SUBSECTIONS_BY_SECTION_ID = " SELECT SUBSEC.SUBSECTIONID AS ID,SUBSEC.DESCRIPTION AS DESCRIPTION "
            + "FROM subsection SUBSEC "
            + "WHERE SUBSEC.SECTIONID = %d "
            + "GROUP BY SUBSEC.DESCRIPTION";

    public static final String AFFINITI_DROPDOWN_TASKS = "SELECT T.TASKID AS ID,T.DESCRIPTION AS DESCRIPTION "
            + "FROM task T "
            + "ORDER BY DESCRIPTION";
    public static final String AFFINITI_DROPDOWN_PENDING_TASKS_BY_USERROLE_ID_AND_SECTION_ID_AND_SUBSECTION_ID = "SELECT T.TASKID as ID,T.DESCRIPTION as DESCRIPTION FROM "
            + "task T INNER JOIN "
            + "subsectiontask ST on T.TASKID = ST.TASK WHERE ST.SECTION = %d AND ST.SUBSECTION = %d AND ST.TASK "
            + "NOT IN "
            + "(SELECT UT.TASKID FROM "
            + "userroletask UT "
            + "WHERE UT.USERROLESUBSECTIONID = "
            + "(SELECT URSS.USERROLESUBSECTIONID FROM "
            + "userrolesubsection URSS WHERE URSS.USERROLEID = %d "
            + "AND URSS.SECTIONID = %d "
            + "AND URSS.SUBSECTIONID = %d ))";

    public static final String AFFINITI_DROPDOWN_PENDING_SECTIONTASKS_BY_USERROLE_ID_AND_SECTION_ID = "SELECT T.TASKID as ID,T.DESCRIPTION as DESCRIPTION FROM "
            + "task T INNER JOIN "
            + "sectiontask ST on T.TASKID = ST.TASKID WHERE ST.SECTIONID = %d AND ST.TASKID "
            + "NOT IN "
            + "(SELECT T.TASKID FROM task T INNER JOIN "
            + "userrolesectiontask UT on T.TASKID = UT.TASKID "
            + "WHERE UT.USERROLESECTIONID = "
            + "(SELECT URSS.USERROLESECTIONID FROM "
            + "userrolesection URSS WHERE URSS.USERROLEID = %d "
            + "AND URSS.SECTIONID = %d "
            + "))";
    public static final String AFFINITI_DROPDOWN_ASSIGNED_TASKS_BY_SECTION_ID_AND_SUBSECTION_ID = "SELECT T.TASKID as ID,T.DESCRIPTION as DESCRIPTION FROM "
            + "task T WHERE T.TASKID "
            + "NOT IN "
            + "(SELECT SUBT.TASK FROM subsectiontask SUBT "
            + "WHERE SUBT.SECTION = %d "
            + "AND SUBT.SUBSECTION = %d ORDER BY T.DESCRIPTION)";

    public static final String AFFINITI_SUBSECTION_BY_SUBSECTION_ID = "SELECT SUBSEC.SUBSECTIONID AS ID,SUBSEC.DESCRIPTION AS DESCRIPTION FROM subsection SUBSEC WHERE SUBSEC.SUBSECTIONID = %d";
    public static final String AFFINITI_DROPDOWN_ASSIGNED_TASKS_BY_USERROLE_ID_AND_SECTION_ID_AND_SUBSECTION_ID = "SELECT T.TASKID as ID,T.DESCRIPTION as DESCRIPTION FROM "
            + "task T INNER JOIN userroletask UT on T.TASKID = UT.TASKID "
            + "WHERE UT.USERROLESUBSECTIONID = "
            + "(SELECT URSS.USERROLESUBSECTIONID FROM "
            + "userrolesubsection URSS WHERE URSS.USERROLEID = %d "
            + "AND URSS.SECTIONID = %d "
            + "AND URSS.SUBSECTIONID = %d "
            + ")";
    public static final String AFFINITI_DROPDOWN_ASSIGNED_SECTIONTASKS_BY_USERROLE_ID_AND_SECTION_ID = "SELECT T.TASKID as ID,T.DESCRIPTION as DESCRIPTION FROM "
            + "task T INNER JOIN userrolesectiontask UT on T.TASKID = UT.TASKID "
            + "WHERE UT.USERROLESECTIONID = "
            + "(SELECT URSS.USERROLESECTIONID FROM "
            + "userrolesection URSS WHERE URSS.USERROLEID = %d "
            + "AND URSS.SECTIONID = %d "
            + ")";
    public static final String AFFINITI_DROPDOWN_SECTIONS_BY_SECTION_ID = "SELECT S.SECTIONID AS ID,S.DESCRIPTION AS DESCRIPTION "
            + "FROM section S WHERE S.SECTIONID = %d";
    public static final String AFFINITI_DROPDOWN_SUBSECTIONS_BY_SUBSECTION_ID = "SELECT SUBSEC.SUBSECTIONID AS ID,SUBSEC.DESCRIPTION AS DESCRIPTION "
            + "FROM subsection SUBSEC WHERE SUBSEC.SUBSECTIONID = %d";

    public static final String AFFINITI_DROPDOWN_SECTIONS_BY_USERROLE_ID = "SELECT S.SECTIONID AS ID,S.DESCRIPTION AS DESCRIPTION "
            + "FROM section S WHERE S.SECTIONID IN"
            + "(SELECT DISTINCT URSS.SECTIONID "
            + "FROM userrolesubsection URSS "
            + "WHERE URSS.USERROLEID = %d)";
    public static final String AFFINITI_DROPDOWN_USERROLESECTIONS_BY_USERROLE_ID = "SELECT S.SECTIONID AS ID,S.DESCRIPTION AS DESCRIPTION "
            + "FROM section S WHERE S.SECTIONID IN "
            + "(SELECT DISTINCT URS.SECTIONID "
            + "FROM userrolesection URS "
            + "WHERE URS.USERROLEID = %d)";

    public static final String AFFINITI_DROPDOWN_LICENSE = "SELECT LI.LICENSEID AS ID,LI.DESCRIPTION FROM "
            + "license LI ORDER BY DESCRIPTION";
    /**
     * ******************** Added by Nadun *********************
     */
    public static final String AFFINITI_DROPDOWN_ORGANIZATION = "SELECT ORGANIZATIONID AS ID, "
            + " DESCRIPTION "
            + " FROM ORGANIZATION WHERE STATUS = %d"
            + " ORDER BY DESCRIPTION";
    
    public static final String AFFINITI_DROPDOWN_ORGANIZATION_BY_LOCATION = "SELECT ORGANIZATIONID AS ID, "
            + " DESCRIPTION "
            + " FROM ORGANIZATION WHERE STATUS = %d"
            + " AND LOCATION = %d"
            + " ORDER BY DESCRIPTION";
    
    public static final String AFFINITI_DROPDOWN_CLIENTCATEGORY = "SELECT CLIENTCATEGORYID AS ID, "
            + " DESCRIPTION "
            + " FROM CLIENTCATEGORY WHERE STATUS = %d "
            + " ORDER BY DESCRIPTION";
//    public static final String AFFINITI_DROPDOWN_CLIENTCATEGORY_BY_ORGANIZATION = "SELECT CLIENTCATEGORYID AS ID, "
//            + " DESCRIPTION "
//            + " FROM CLIENTCATEGORY WHERE STATUS = %d AND ORGANIZATION = %d"
//            + " ORDER BY DESCRIPTION";
    public static final String AFFINITI_DROPDOWN_PRODUCTCATEGORY = "SELECT PRODUCTCATEGORYID AS ID, "
            + " DESCRIPTION "
            + " FROM PRODUCTCATEGORY"
            + " ORDER BY DESCRIPTION";
    public static final String AFFINITI_DROPDOWN_PRODUCT_BY_PRODUCTCATEGORY = "SELECT PRODUCTID AS ID, "
            + "       DESCRIPTION "
            + "FROM product "
            + "WHERE PRODUCTCATEGORY = %d AND "
            + "STATUS IN (%s) "
            + "ORDER BY DESCRIPTION";
    public static final String AFFINITI_DROPDOWN_PRODUCTCATEGORY_BY_PRODUCT = "SELECT PC.PRODUCTCATEGORYID AS ID, "
            + "       PC.DESCRIPTION "
            + "FROM product P "
            + "INNER JOIN productcategory PC ON PC.PRODUCTCATEGORYID = P.PRODUCTCATEGORY "
            + "WHERE P.PRODUCTID = %d AND "
            + "P.STATUS = %d ";
    public static String AFFINITI_DROPDOWN_USERROLETYPE = " SELECT USERROLETYPEID AS ID,"
            + " DESCRIPTION  FROM userroletype ORDER BY DESCRIPTION ";
    public static final String AFFINITI_DROPDOWN_LOCATION = "SELECT LOCATIONID AS ID, "
            + " LOCATIONDESCRIPTION AS DESCRIPTION "
            + " FROM LOCATION"
            + " ORDER BY LOCATIONDESCRIPTION";
}
