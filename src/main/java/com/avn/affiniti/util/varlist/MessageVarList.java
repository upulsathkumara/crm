/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.varlist;

/**
 * @Author : KAsun Udayanaga
 * @Document : MessageVarList
 * @Created on : Nov 15, 2017, 9:20:06 AM
 */
public class MessageVarList {

    //------------------------ Success Messages ------------------------//
    public static final String SAVED_SUCCESSFULLY = "%s saved successfully.";
    public static final String UPDATED_SUCCESSFULLY = "%s updated successfully.";
    public static final String ASSIGNED_SUCCESSFULLY = "%s assigned successfully.";
    public static final String DELETED_SUCCESSFULLY = "%s deleted successfully.";
    public static final String ALL_REMOVED_SUCCESSFULLY = "All %s removed successfully.";
    public static final String DATA_RETRIEVED_SUCCESSFULLY = "Data retrieved successfully.";
    //-----------------------------------------------------------------//

    //------------------------ Error Messages ------------------------//
    public static final String SAVE_ERROR = "Error occurred while saving %s.";
    public static final String UPDATE_ERROR = "Error occurred while updating %s.";
    public static final String ASSIGN_ERROR = "Error occurred while assigning %s.";
    public static final String DELETE_ERROR = "Error occurred while deleting %s.";
    public static final String DATA_RETRIEVE_ERROR = "Error occurred while retrieving data.";
    public static final String ACCESS_DENIED = "Access Denied";

    //-----------------------------------------------------------------//
}
