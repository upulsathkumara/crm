/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.varlist;

import java.io.File;

/**
 * @Author : Roshen Dilshan Dilshan
 * @Document : CommonVarList
 * @Created on : Jul 30, 2015, 10:18:36 AM
 */
public class CommonVarList {

    //------------------------ Common Values ------------------------//
    public static final String EMPTY_OR_NULL_REPLACE_VALUE = "--";
    public static final String EMPTY_OR_NULL_REPLACE_VALUE_ALL = "-All-";
    public static final String EMPTY_OR_NULL_REPLACE_VALUE_NA = "-N/A-";

    public static final String EMAIL_DOAMIN = "@123.lk";
    //---------------------------------------------------------------//

    //------------------------ Context Param Names ------------------------//
    public static final String CONTEXT_PARAM_SOFTWARE_VERSION = "software_version";
    //---------------------------------------------------------------------//

    //------------------------ Date Formats ------------------------//
    public static final String DATE_FORMAT_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_yyyyMMdd = "yyyyMMdd";
    public static final String DATE_FORMAT_yyyy_MM_dd = "yyyy-MM-dd";
    public static final String TIME_FORMAT_hh_mm_a = "hh:mm a";
    public static final String DATE_FORMAT_yyyy = "yyyy";
    public static final String DATE_FORMAT_dd_MM_yyyy = "dd/MM/yyyy";
    public static final String DATE_FORMAT_yyyy_MM_dd_hh_mm_a = "yyyy-MM-dd hh:mm a";
    public static final String DATE_FORMAT_yyyy_MM_dd_hh_mm_ss_a = "yyyy-MM-dd hh:mm:ss a";
    public static final String DATE_FORMAT_yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_yyyy_MM_dd_HH_mm = "yyyy-MM-dd HH:mm";
    public static final String DATE_FORMAT_yyyy_MM = "yyyy-MM";
    public static final String DATE_FORMAT_dd_MMM_yy_hhmmssSSa = "dd-MMM-yy hh.mm.ss.SS a";
    public static final String DATE_FORMAT_dd_MMM_yy_hhmmssa = "dd-MMM-yy hh.mm.ss a";
    public static final String DATE_FORMAT_dd_MMM_yy_hhmmss = "dd-MMM-yy hh:mm:ss";
    public static final String DATE_FORMAT_dd_MMM_yy_HHmmss = "dd-MMM-yy HH:mm:ss";
    public static final String DATE_FORMAT_dd_MMM_yyyy = "dd-MMM-yyyy";
    public static final String TIME_FORMAT_hh_mm_24 = "HH:mm";
    public static final String TIME_FORMAT_hh_mm_12 = "hh:mm";
    public static final String DATE_FORMAT_YYYY_MM_dd_hhmmssSSa = "yyyy/MM/dd hh:mm:ss:SS a";
    //--------------------------------------------------------------//

    //------------------------ Notification ------------------------//
    public static final String NOTIFICATION_MAIL_FROM = "avonettestmail@gmail.com";
//    public static final String NOTIFICATION_MAIL_FROM = "noreplyaffiniti@theavo.net";
    //--------------------------------------------------------------//

    //------------------------ Datatable Sorting ------------------------//
    public static final String DATA_TABLE_SORTING_ASC = "ASC";
    public static final String DATA_TABLE_SORTING_DESC = "DESC";
    //-------------------------------------------------------------------//

    //------------------------ Datatable Sorting ------------------------//
    public static final int AFFINTI_ISINITIAL_STATUS = 1;

    public static final String AFFINITI_SESSION_USERNAME = "username";

    //------------------------ Message Codes ------------------------//
    public static final String AFFINITI_MESSAGECODE_SUCCESS = "SUCCESS";
    public static final String AFFINITI_MESSAGECODE_ERROR = "ERROR";
    //------------------------ Message Codes ------------------------//

    //------------------------ File upload location -----------------//
    public static final String CONTEXT_PARAM_BULK_TERMINAL_BREAKDOWN = "bulk_terminal_breakdown_file_loaction";
}
