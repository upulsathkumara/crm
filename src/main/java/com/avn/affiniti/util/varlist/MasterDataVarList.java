/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.varlist;

/**
 * @Author : Roshen Dilshan
 * @Document : MasterDataVarList
 * @Created on : Aug 19, 2015, 7:10:05 PM
 */
public class MasterDataVarList {

    //------------------------ Status Categories ------------------------//
    public static final int AFFINITI_CODE_STATUS_ID_ACTIVE = 1;
    public static final int AFFINITI_CODE_STATUS_ID_INPROGRESS = 6;
    /**
     * Status Active
     */
    public static final String AFFINITI_CODE_STATUS_CODE_ACTIVE = "DEFACT";
    public static final int AFFINITI_CODE_STATUS_ID_INACTIVE = 2;
    /**
     * Status Inactive
     */
    public static final String AFFINITI_CODE_STATUS_CODE_INACTIVE = "DEFINACT";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_OPEN = "TKOP";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_CLOSE = "TKCLO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSIGNED = "TKASS";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCEPTED = "TKACC";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_INPROGRESS = "TKIP";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCCA = "TKASSTOCCA";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC = "TKASSTOSCSC";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYTO = "TKACCBYTO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOIFS = "TKASSTOIFS";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_DTTIFS = "TKTDILTERIFS";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOEM = "TKASSTOEM";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYEM = "TKACCBYEM";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYIFS = "TKACCBYIFS";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ISSUED = "AISSUED";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED = "ANISSUED";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_AMCYES = "AMCYES";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_AMCNO = "AMCNO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_YES = "YES";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_NO = "NO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_POSRESPONSE = "POSRESPONSE";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_NEGRESPONSE = "NEGRESPONSE";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_DRAFT = "DRAFT";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_SENTSIGNATURE = "SENTSIGNATURE";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_CONFIRMED = "CONFIRMED";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTACCREPTERM = "TKTACCREPTERM";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_RETFTMC = "TKTRETEXTTERM";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTACCTERM = "TKTACCTERM";
    /**
     * Status accepted by Senior Customer Support Coordinator
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYSCSC = "TKACCBYSCSC";
    /**
     * Status assigned to Technical Officer
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO = "TKASSTOTO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_BKPTERREJ = "TKTBKPTERREJ";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ISSTERMINIINV = "TKISSTERMINIINV";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC = "TKREQBKPTMC";//Request Backup Terminal - TMC
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ISSBKPTMC = "TKISSBKPTMC";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_DEPBKPTER = "TKTDEPBKPTER";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_CLOANDREPTKT = "TKTCLOANDREPTKT";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSREQCSE = "TKTACSSREQCSE"; //Accessories required - CSE
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC = "TKTCOLTERTMC"; // Collect terminal - TMC
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC = "TKTCOLRTFTMC"; // Collect repaired terminal - TMC
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTISSUTERMVO = "TKTISSUTERMVO"; // Issue terminal - VO
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTISSUTERMTO = "TKTISSUTERMTO"; // Issue terminal - TO
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTNWINSTTKT = "TKTNWINSTTKT"; // Create New Installation Ticket
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTDELTERMIRC = "TKTDELTERMIRC"; // Deliver terminals to IRC
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTGENDOIRC = "TKTGENDOIRC"; // Genarate DO - IRC
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTREJMER = "TKTREJMER"; // Rejected by merchant
    /**
     * Status assigned to Customer Support Engineer
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCSE = "TKTASSTOCSE";
    /**
     * Status accepted by Customer Support Engineer
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYCSE = "TKTACCBYCSE";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSASSCSE = "TKTACSSASSCSE"; //Accessories assign - CSE
    public static final int AFFINITI_TICKET_STATUS_ACCESSORIES_REQUIRD_CSE = 50;
    /**
     * Status request parts from Customer Support Engineer
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_REQPART = "TKTREQPART";
    /**
     * Status Issue Repaired terminals
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_IREDTLS = "TKTISSREPTERM";
    /**
     * Status issue part by Customer Support Engineer
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ISSPART = "TKTISSPART";
    /**
     * Status assigned to Vericentre Officer
     */

    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO = "TKASSTOVO";
    public static final String AFFINITI_CODE_STATUS_ID_TKT_ACCBYVO = "TKACCBYVO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYVO = "TKACCBYVO";
    /**
     * Status Product Owner
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOPO = "TKTASSTOPO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ACCBYPO = "TKTACCBYPO";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_RESOLVED = "TKTRESOL";
    /**
     * Status CSE
     */
    public static final String AFFINITI_TICKET_STATUS_CODE_TKT_TKTACSSREQCSE = "TKTACSSREQCSE";
    /**
     * Status EM
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTREQACCEM = "TKTREQACCEM"; //Request accessories - EM
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTISSUACCEM = "TKTISSUACCEM"; //Issue accessories - EM
    /**
     * Status IFS
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM = "TKTREQBACTERMEM"; //Request backupterminal
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTISSBAKTERMEM = "TKTISSBAKTERMEM"; //Issue backupterminal - IFS
    /**
     * Status Epic Sri Lanka
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK = "TKTASSEPICLK"; //Ticket Assigned - EPIC LK
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TKTACCBYEPICLK = "TKTACCBYEPICLK"; //Ticket Accepted - EPIC LK
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_REOPEN = "TKTREOPEN";
    /**
     * Status fix terminal
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_FIXTER = "TKTFIXTER";
    /**
     * Status terminal test failed
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TSTTREFAIL = "TKTTSTTREFAIL";
    /**
     * Status terminal test success
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TSTTRESUCC = "TKTTSTTRESUCC";
    /**
     * Status assign to Terminal Management Coordinator
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC = "TKTASSTOTMC";
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_ADDTMCINV = "TKTADDTMCINV";//Add to TMC Inventory
    /**
     * Status assign to Terminal Handover to TMC
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKT_TERMHANDTMC = "TKTTERMHANDTMC";
    /**
     * Status Accept to Inventory - TMC
     */
    public static final String AFFINITI_CODE_STATUS_CODE_TKTACCINV = "TKTACCINV";

    //------------------------ Status Categories ------------------------//
    public static final int AFFINITI_CODE_STATUS_CATEGORY_DEFAULT = 1;
    public static final int AFFINITI_CODE_STATUS_CATEGORY_TICKET = 2;
    public static final int AFFINITI_CODE_STATUS_CATEGORY_REQUESTBACKUPTERMINAL = 13;
    public static final int AFFINITI_CODE_STATUS_CATEGORY_COLLECTTERMINALFROMTMC = 40;
    public static final int AFFINITI_CODE_STATUS_CATEGORY_COLLECTTERMINAL = 79;
    public static final int AFFINITI_CODE_STATUS_CATEGORY_CLIENTPRODUCT = 10;
    //------------------------ Status Categories ------------------------//

    //------------------------ Terminal Delivery Status -------------------//
    public static final int AFFINITI_CODE_STATUS_TERMINAL_DELIVERY_YES = 1;
    //------------------------ Terminal Delivery Status -------------------//

    //------------------------ Ticket ------------------------//
    public static final int AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE = 0;
    //------------------------ Ticket ------------------------//

    //------------------------ Ticket Categories ------------------------//
    public static final int AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN = 1;
    public static final int AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR = 2;
    public static final int AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION = 3;
    public static final int AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN = 11;
    public static final int AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL = 12;
    public static final int AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION = 13;
    public static final int AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE = 14;
    public static final int AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL = 15;
    public static final int AFFINITI_TICKET_CATEGORY_BULK_TERMINAL_BREAKDOWN = 16;
    public static final int AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING = 17;
    public static final int AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION = 18;
    public static final int AFFINITI_TICKET_CATEGORY_REINTIALIZATION = 19;
    public static final int AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN = 20;
    public static final int AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR = 21;
    public static final int AFFINITI_TICKET_CATEGORY_MAINTANACE_PAYMENT_INVOICE = 22;
    //------------------------ Ticket Categories ------------------------//

    //------------------------ Pagemanagemet ------------------------//
    public static final int AFFINITI_CODE_ONLY_PARENT_SECTION_TRUE = 1;
    public static final int AFFINITI_CODE_ONLY_PARENT_SECTION_FALSE = 0;
    public static final int AFFINITI_CODE_CLICKABLE_TRUE = 1;
    public static final int AFFINITI_CODE_CLICKABLE_FALSE = 0;
    //------------------------ Pagemanagemet ------------------------//

    //------------------------ User Role ------------------------//
    public static final int AFFINITI_CODE_USERROLE_CC = 1;
    public static final int AFFINITI_CODE_USERROLE_SCSC = 4;
    public static final int AFFINITI_CODE_USERROLE_TO = 5;
    public static final int AFFINITI_CODE_USERROLE_TMC = 6;
    public static final int AFFINITI_CODE_USERROLE_VO = 8;
    public static final int AFFINITI_CODE_USERROLE_CLIENT_ADMIN = 9;
    public static final int AFFINITI_CODE_USERROLE_PO = 11;
    public static final int AFFINITI_CODE_USERROLE_CLIENT_USER = 10;
    public static final int AFFINITI_CODE_USERROLE_BSN = 12;
    public static final int AFFINITI_CODE_USERROLE_IFS = 13;
    public static final int AFFINITI_CODE_USERROLE_EM = 14;
    public static final int AFFINITI_CODE_USERROLE_EPIC_MALAYSIA = 16;
    public static final int AFFINITI_CODE_USERROLE_EPIC_SRILANKA = 17;

    public static final String AFFINITI_CODE_USERROLE_CODE_CCA = "CCA";
    public static final String AFFINITI_CODE_USERROLE_CODE_SCSC = "SCSC";
    public static final String AFFINITI_CODE_USERROLE_CODE_TO = "TO";
    public static final String AFFINITI_CODE_USERROLE_CODE_VO = "VO";
    public static final String AFFINITI_CODE_USERROLE_CODE_PO = "PO";
    public static final String AFFINITI_CODE_USERROLE_CODE_TMC = "TMC";
    public static final String AFFINITI_CODE_USERROLE_CODE_CSE = "CSE";
    public static final String AFFINITI_CODE_USERROLE_CODE_IRC = "IRC";
    public static final String AFFINITI_CODE_USERROLE_CODE_EM = "EM";
    public static final String AFFINITI_CODE_USERROLE_CODE_IFS = "IFS";
    public static final String AFFINITI_CODE_USERROLE_CODE_CLIENT_ADMIN = "CA";
    public static final String AFFINITI_CODE_USERROLE_CODE_CLIENT_USER = "CU";
    public static final String AFFINITI_CODE_USERROLE_CODE_EPIC_SRILANKA = "EPICLK";
    public static final String AFFINITI_CODE_USERROLE_CODE_EPIC_MALAYSIA = "EIPCMYS";
    //------------------------ User Role ------------------------//

    //------------------------ Ticket Created By ------------------------//
    public static final int AFFINITI_CODE_TICKET_REPORTEDBY_BANK = 1;
    public static final int AFFINITI_CODE_TICKET_REPORTEDBY_MERCHANT = 2;
    //------------------------ Ticket Created By ------------------------//

    //------------------------ Ticket Category Content ------------------------//
    public static final String AFFINITI_CODE_TICKET_CATEGORY_CONTENT_STATE_CREATE = "C";
    public static final String AFFINITI_CODE_TICKET_CATEGORY_CONTENT_STATE_VIEWEDIT = "VE";
    //------------------------ Ticket Category Content ------------------------//

    //------------------------ Client Product Status ------------------------//
    public static final int AFFINITI_CODE_STATUS_PRODUCT_ACTIVE = 1;
    public static final int AFFINITI_CODE_STATUS_PRODUCT_INACTIVE = 2;

    //------------------------ Client Product Status ------------------------//
    public static final int AFFINITI_CODE_CLIENT_TOKEN_EXPIRE_TIME = 1000 * 60 * 10; // 10 minutes
    public static final int AFFINITI_CODE_STATUS_ID_OPEN = 3;

    //------------------------ Inventory Category ------------------------//
    public static final int AFFINITI_CODE_INVENTORYCATEGORY_INVENTORYCATEGORYID = 6;

    //------------------------ Inventory Category ------------------------//    
    public static final int AFFINITI_CODE_INVENTORYCATEGORYID_NEW = 1;
    public static final int AFFINITI_CODE_INVENTORYCATEGORYID_RECONDITIONED = 3;
    public static final int AFFINITI_CODE_INVENTORYCATEGORYID_REPAIRED = 4;
    public static final int AFFINITI_CODE_INVENTORYCATEGORYID_BACKUP = 5;
    public static final int AFFINITI_CODE_INVENTORYCATEGORYID_BRANDNEW = 7;//
    public static final int AFFINITI_CODE_INVENTORYCATEGORYID_UPGRADE = 8;
    //------------------------ Inventory Category ------------------------//

    //------------------------ Client Category ------------------------//    
    public static final int AFFINITI_CODE_CLIENTCATEGORYID_HARDWARE = 1;

    //------------------------ Add Or Update ------------------------//    
    public static final String AFFINITI_CODE_CREATE = "create";
    public static final String AFFINITI_CODE_UPDATE = "update";

    //------------------------ Is Final Status  ------------------------//    
    public static final int AFFINITI_CODE_ISFINALSTATUS = 1;

    //------------------------ Location  ------------------------//    
    public static final int AFFINITI_CODE_LOCATION_LK = 1;
    public static final int AFFINITI_CODE_LOCATION_MY = 2;

    //------------------------ Ticket Attribute Status  ------------------------//    
    public static final String AFFINITI_CODE_TICKETATTRIBUTE_STATUS_YES = "Yes";
    public static final String AFFINITI_CODE_TICKETATTRIBUTE_STATUS_NO = "No";

    //------------------------ Ticket Categories Update------------------------//
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_BREAKDOWN = "Terminal Brakdown Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_NEW_TERMINAL_INSTALLATION = "New Installation Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_SOFWARE_HARDWARE_BREAKDOWN = "Software Hardware Breakdown Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_MERCHANT_REMOVAL = "Merchant Removal Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_BASE_SOFTWARE_INSTALLATION = "Base Softaware Installation Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_UPGRADE_DOWNGRADWE = "Terminal Upgrade Downgrade Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_CONVERSION = "Terminal Conversion Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_BACKUP_REMOVAL = "Backup Removal Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_SHARING = "Terminal Sharing Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_REINTIALIZATION = "Re initialization Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_MAINTANACE_PAYMENT_INVOICE = "Maintenence Payment Invoice Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_MALAY_TERMINAL_REPAIR = "Malaysian Terminal Repair Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_MALAY_TERMINAL_BREAKDOWN = "Malaysian Terminal Brakdown Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_REPAIR = "Terminal Repair Ticket";

    //------------------------ Ticket Categories Create------------------------//
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_BREAKDOWN = "Terminal Brakdown Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_NEW_TERMINAL_INSTALLATION = "New Installation Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_SOFWARE_HARDWARE_BREAKDOWN = "Software Hardware Breakdown Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_MERCHANT_REMOVAL = "Merchant Removal Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_BASE_SOFTWARE_INSTALLATION = "Base Softaware Installation Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_UPGRADE_DOWNGRADWE = "Terminal Upgrade Downgrade Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_CONVERSION = "Terminal Conversion Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_BACKUP_REMOVAL = "Backup Removal Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_SHARING = "Terminal Sharing Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_REINTIALIZATION = "Re initialization Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_MAINTANACE_PAYMENT_INVOICE = "Maintenence PaymentInvoice Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_MALAY_TERMINAL_REPAIR = "Malaysian Terminal Repair Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_REPAIR = "Terminal Repair Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_BULK_TERMINAL_BRREAKDOWN = "Bulk Terminal Breakdown Ticket";
    public static final String AFFINITI_TICKET_CATEGORY_CREATE_MALAY_TERMINAL_BREAKDOWN = "Terminalal Brakdown Malayasia Ticket";

    //------------------------ Notification Codes ------------------------//
    public static final int AFFINITI_CODE_NOTIFICATION_TEMPLATE_TICKET_INITIATE = 1;

    //------------------------ Reminder Notification ------------------------//
    public static final int AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND = 128;
    public static final int AFFINITI_CODE_REMINDER_NOTIFICATION_REMIND = 129;
    public static final int AFFINITI_CODE_REMINDER_NOTIFICATION_VIEWED = 130;
    public static final int AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED = 131;
    public static final int AFFINITI_CODE_REMINDER_NOTIFICATION_DELETE = 132;
    
    public static final int AFFINITI_TICKET_SECTION_ID = 202;
}
