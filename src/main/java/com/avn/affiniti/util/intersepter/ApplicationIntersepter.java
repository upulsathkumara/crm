package com.avn.affiniti.util.intersepter;

import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.task.TaskDAO;
import com.avn.affiniti.service.common.CommonService;
import com.avn.affiniti.service.user.UserService;
import com.avn.affiniti.util.varlist.CommonVarList;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : ApplicationIntersepter
 * @Created on : Feb 3, 2016, 2:37:46 PM
 */
public class ApplicationIntersepter implements HandlerInterceptor {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private CommonService commonService;

    @Autowired
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o) throws Exception {
        boolean isContinueRequest = true;
        List<Integer> userSectionTaskList = new ArrayList<Integer>();
        String action = "";
        if (hsr.getParameter("action") != null) {
            action = hsr.getParameter("action");
        }

        if (action.equalsIgnoreCase("save")) {
            action = "create";

        } else if (action.equalsIgnoreCase("edit")) {

            action = "update";
        }
        String user = String.valueOf(hsr.getSession().getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        String requestUrl = hsr.getRequestURI();
        String moduleSplittedUrl = "";
        String completeMiddleUrl = "";
        String requestWithoutParameters = requestUrl.split("'?'")[0];

        if (requestUrl.split("/").length > 2
                && !requestUrl.split("/")[2].equalsIgnoreCase("resources")
                && !requestUrl.split("/")[2].equalsIgnoreCase("login")
                && !requestUrl.split("/")[2].equalsIgnoreCase("dashboardone")
                && !requestUrl.split("/")[2].equalsIgnoreCase("reset")
                && (hsr.getQueryString() == null || !hsr.getQueryString().equalsIgnoreCase("auth=logout"))) {

            if (hsr.getSession(false) == null || hsr.getSession(false).getAttribute("pagehierarchy") == null) {
                if (requestUrl.split("/")[2].equalsIgnoreCase("reset")) {
                    hsr1.sendRedirect(hsr.getContextPath() + "/user/register");
                    isContinueRequest = false;
                } else {
                    hsr1.sendRedirect(hsr.getContextPath() + "/?auth=sessiontimeout");
                    isContinueRequest = false;
                }

            } else {
                //Customize request URL
                for (int i = 2; i < requestWithoutParameters.split("/").length; i++) {
                    moduleSplittedUrl = requestWithoutParameters.split("/")[i];

                    completeMiddleUrl = completeMiddleUrl + "/" + moduleSplittedUrl;
                }
                int userRoleId = userService.getUserRoleByUsername(user);

//               if(action == ""){
                String sectionTaskIds[] = commonService.getTaskIdAndSectionIdByURL(completeMiddleUrl).split(":");

                int sectionId = Integer.parseInt(sectionTaskIds[0]);
                int sectionTaskId = Integer.parseInt(sectionTaskIds[1]);
                int subSectionId = 0;
                int taskId = 0;
                int userRolePermissionType = 0;

                if (sectionId == 0 && sectionTaskId == 0) {
                    String subSectionTaskIds[] = commonService.getTaskIdSubSectionIdByURL(completeMiddleUrl).split(":");

                    subSectionId = Integer.parseInt(subSectionTaskIds[0]);
                    taskId = Integer.parseInt(subSectionTaskIds[1]);

                }

                //check whether the request is ajax
                if (this.checkWhetherAjax(hsr)) {

                    //********************************************
                    //check taskid whether 2(create)
                    if (sectionTaskId == 2 || taskId == 2) {
                        if (action.equalsIgnoreCase("create")) {
                            userRolePermissionType = 0;
                            int sectionOrSubSectionId = sectionId;
                            int sectionOrSubSectionTaskId = sectionTaskId;
                            if (taskId == 2) {
                                userRolePermissionType = 1;
                                sectionOrSubSectionId = subSectionId;
                                sectionOrSubSectionTaskId = taskId;
                            }

                            //check whether user have rights or not
                            if (!this.checkRolePermission(userRoleId, sectionOrSubSectionId, sectionOrSubSectionTaskId, userRolePermissionType)) {
                                JSONObject outputForResponse = new JSONObject();
                                JSONArray rows = new JSONArray();

                                //error message for response
                                outputForResponse.put("CODE", "ERROR");
                                outputForResponse.put("MESSAGE", "Access Denied");
                                outputForResponse.put("sEcho", "1");
                                outputForResponse.put("iTotalRecords", 0);
                                outputForResponse.put("iTotalDisplayRecords", 0);
                                outputForResponse.put("aaData", rows);
                                outputForResponse.put("success", false);
                                outputForResponse.put("status", 400);
                                hsr1.setContentType("application/json");
                                hsr1.getWriter().write(outputForResponse.toString());
                                isContinueRequest = false;

                            }
                        } else if (action.equalsIgnoreCase("update")) {
                            sectionTaskId = 4;
                            userRolePermissionType = 0;
                            int sectionOrSubSectionId = sectionId;

                            if (taskId == 2) {
                                userRolePermissionType = 1;
                                sectionOrSubSectionId = subSectionId;

                            }
                            //check whether user have rights or not
                            if (!this.checkRolePermission(userRoleId, sectionOrSubSectionId, sectionTaskId, userRolePermissionType)) {
                                JSONObject outputForResponse = new JSONObject();
                                JSONArray rows = new JSONArray();

                                //error message for response
                                outputForResponse.put("CODE", "ERROR");
                                outputForResponse.put("MESSAGE", "Access Denied");
                                outputForResponse.put("sEcho", "1");
                                outputForResponse.put("iTotalRecords", 0);
                                outputForResponse.put("iTotalDisplayRecords", 0);
                                outputForResponse.put("aaData", rows);
                                outputForResponse.put("success", false);
                                outputForResponse.put("status", 400);
                                hsr1.setContentType("application/json");
                                hsr1.getWriter().write(outputForResponse.toString());
                                isContinueRequest = false;

                            }

                        } else {
                            JSONObject outputForResponse = new JSONObject();
                            JSONArray rows = new JSONArray();

                            //error message for response
                            outputForResponse.put("CODE", "ERROR");
                            outputForResponse.put("MESSAGE", "Access Denied");
                            outputForResponse.put("sEcho", "1");
                            outputForResponse.put("iTotalRecords", 0);
                            outputForResponse.put("iTotalDisplayRecords", 0);
                            outputForResponse.put("aaData", rows);
                            outputForResponse.put("success", false);
                            outputForResponse.put("status", 400);
                            hsr1.setContentType("application/json");
                            hsr1.getWriter().write(outputForResponse.toString());
                            isContinueRequest = false;
                        }

                        //if section taskid or sub section taaskid not equals 2  
                    } else {
                        //check whether the url is available in sectiontask
                        if (sectionId != 0 && sectionTaskId != 0) {

                            userRolePermissionType = 0;
                            //check whether user have rights or not
                            userSectionTaskList = checkRoleTasksForSectionID(userRoleId, sectionId, sectionTaskId, userRolePermissionType);

                            if (!this.checkRolePermission(userRoleId, sectionId, sectionTaskId, userRolePermissionType)) {

                                JSONObject outputForResponse = new JSONObject();
                                JSONArray rows = new JSONArray();

                                //error message for response
                                outputForResponse.put("CODE", "ERROR");
                                outputForResponse.put("MESSAGE", "Access Denied");
                                outputForResponse.put("sEcho", "1");
                                outputForResponse.put("iTotalRecords", 0);
                                outputForResponse.put("iTotalDisplayRecords", 0);
                                outputForResponse.put("aaData", rows);
                                outputForResponse.put("success", false);
                                outputForResponse.put("status", 400);
                                hsr1.setContentType("application/json");
                                hsr1.getWriter().write(outputForResponse.toString());
                                isContinueRequest = false;
                            }

                        }

                        //check whether the url is available in subsectiontask
                        if (subSectionId != 0 && taskId != 0) {
                            userRolePermissionType = 1;
                            userSectionTaskList = checkRoleTasksForSubSectionID(userRoleId, subSectionId, taskId, userRolePermissionType);

                            //check whether user have rights or not
                            if (!this.checkRolePermission(userRoleId, subSectionId, taskId, userRolePermissionType)) {

                                JSONObject outputForResponse = new JSONObject();
                                JSONArray rows = new JSONArray();

                                //error message for response
                                outputForResponse.put("CODE", "ERROR");
                                outputForResponse.put("MESSAGE", "Access Denied");
                                outputForResponse.put("sEcho", "1");
                                outputForResponse.put("iTotalRecords", 0);
                                outputForResponse.put("iTotalDisplayRecords", 0);
                                outputForResponse.put("aaData", rows);
                                outputForResponse.put("success", false);
                                outputForResponse.put("status", 400);
                                hsr1.setContentType("application/json");
                                hsr1.getWriter().write(outputForResponse.toString());
                                isContinueRequest = false;
                            }
                        }

                    }

                    //***********************************************
                } else {

                    //check whether the url is available in sectiontask
                    if (sectionId != 0 && sectionTaskId != 0) {
                        userRolePermissionType = 0;
                        userSectionTaskList = checkRoleTasksForSectionID(userRoleId, sectionId, sectionTaskId, userRolePermissionType);

                        if (!this.checkRolePermission(userRoleId, sectionId, sectionTaskId, userRolePermissionType)) {
                            //redirect to access denied page
                            isContinueRequest = false;

                            hsr1.sendRedirect(hsr.getContextPath() + "/errorpages/error401");

                        } else {

                        }

                    }

                    //check whether the url is available in subsectiontask
                    if (subSectionId != 0 && taskId != 0) {
                        userRolePermissionType = 1;
                        //check whether user have rights or not
                        userSectionTaskList = checkRoleTasksForSubSectionID(userRoleId, subSectionId, taskId, userRolePermissionType);

                        if (!this.checkRolePermission(userRoleId, subSectionId, taskId, userRolePermissionType)) {
                            //redirect to access denied page
                            isContinueRequest = false;

                            hsr1.sendRedirect(hsr.getContextPath() + "/errorpages/error401");

                        }

                    }
                }

            }
        } else if (requestUrl.split("/").length == 2
                && (hsr.getQueryString() == null || !hsr.getQueryString().equalsIgnoreCase("auth=logout"))) {
            if (hsr.getSession(false) != null && hsr.getSession(false).getAttribute("pagehierarchy") != null) {
                hsr1.sendRedirect(hsr.getContextPath() + "/dashboardone");
            }
        }

        hsr.getSession().setAttribute("permissions", this.setPermissionsJSON(userSectionTaskList));
//                                hsr.getSession().setAttribute("sectionTaskList", userSectionTaskList);;
        return isContinueRequest;
    }

    public boolean checkWhetherAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(
                request.getHeader("X-Requested-With"));

    }

    public boolean checkRolePermission(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws SQLException, Exception {

        return commonService.getUserRolePermission(userRoleId, subSectionOrSectionId, taskId, userRolePermissionType);

    }

    //checkRoleTasksForSectionID
    public List checkRoleTasksForSectionID(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws SQLException, Exception {

        return commonService.getTaskListByUserRoleIdAndSectionId(userRoleId, subSectionOrSectionId, taskId, userRolePermissionType);

    }

    //checkRoleTasksForSubSectionID
    public List checkRoleTasksForSubSectionID(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws SQLException, Exception {

        return commonService.getTaskListByUserRoleIdSubSectionId(userRoleId, subSectionOrSectionId, taskId, userRolePermissionType);

    }

//    private Map<String, String> getHeadersInfo(HttpServletRequest request) {
//
//        Map<String, String> map = new HashMap<String, String>();
//
//        Enumeration headerNames = request.getHeaderNames();
//        while (headerNames.hasMoreElements()) {
//            String key = (String) headerNames.nextElement();
//            String value = request.getHeader(key);
//            map.put(key, value);
//        }
//
//        return map;
//    }
    @Override
    public void postHandle(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, ModelAndView mav) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest hsr, HttpServletResponse hsr1, Object o, Exception excptn) throws Exception {
        String requestUrl = hsr.getRequestURI();
        boolean isLogOut = false;
        if (hsr.getQueryString() != null && hsr.getQueryString().equalsIgnoreCase("auth=logout")) {
            isLogOut = true;
            HttpSession session = hsr.getSession();
            session.removeAttribute("pagehierarchy");
            session.removeAttribute("username");
            session.invalidate();
        }
        if (!isLogOut && requestUrl.split("/").length > 2
                && !requestUrl.split("/")[2].equalsIgnoreCase("resources")
                && requestUrl.split("/")[2].equalsIgnoreCase("login")
                && requestUrl.split("/")[2].equalsIgnoreCase("dashboardone")
                && requestUrl.split("/")[2].equalsIgnoreCase("reset")) {
            if (hsr.getSession(false) == null || hsr.getSession(false).getAttribute("pagehierarchy") == null) {
                hsr1.sendRedirect(hsr.getContextPath() + "/?auth=sessiontimeout");
            }
        }
    }

    private JSONObject setPermissionsJSON(List<Integer> userSectionTaskList) {
        JSONObject permissions = new JSONObject();
        permissions.put("view", false);
        permissions.put("create", false);
        permissions.put("search", false);
        permissions.put("update", false);
        permissions.put("delete", false);
        permissions.put("assign", false);

        for (Integer tid : userSectionTaskList) {
            if (tid == 1) {
                permissions.put("view", true);
            } else if (tid == 2) {
                permissions.put("create", true);
            } else if (tid == 3) {
                permissions.put("search", true);
            } else if (tid == 4) {
                permissions.put("update", true);
            } else if (tid == 5) {
                permissions.put("delete", true);
            } else if (tid == 6) {
                permissions.put("assign", true);
            }
        }

        return permissions;
    }

}
