/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.usermessages;

/**
 * @Author : Roshen Dilshan
 * @Document : AffinitiErrorMessages
 * @Created on : Jun 9, 2017, 3:48:32 PM
 */
public enum AffinitiErrorMessages {

    INVALID_LOGIN(100, ""),
    /*Ticket error id's starts with the 300*/
    EMIAL_SEND_FAILED(300, "");

    private final int id;
    private final String msg;

    AffinitiErrorMessages(int id, String msg) {
        this.id = id;
        this.msg = msg;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

}
