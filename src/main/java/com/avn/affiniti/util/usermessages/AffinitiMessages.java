/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.usermessages;

/**
 * @Author : Roshen Dilshan
 * @Document : AffinitiMessages
 * @Created on : Jun 11, 2017, 11:02:00 AM
 */
public enum AffinitiMessages {

    LOGIN_LOGOUT_SUCCESS("You've been logged out successfully."),
    LOGIN_INVALID_USER("Invalid username and password!"),
    LOGIN_INACTIVE_USER("This user account is inactive."),
    LOGIN_USER_NOT_EXIST("User not exist!"),
    LOGIN_SESSION_OUT("Session Timeout!"),
    TICKET_CREATE_SUCCESS("Ticket ID : %d"),
    TICKET_CREATE_ERROR("Ticket create failed."),
    TICKET_UPDATE_SUCCESS("Ticket update"),
    TICKET_UPDATE_SUCCESSFULLY("Ticket updated successfully "),
    TICKET_UPDATE_ERROR("Ticket update"),
    TICKET_ASSIGNEE("Ticket Assignee : %s"),
    ACCESSORY_ASSIGNE_SUCCESS("Accessory Assignee to Ticket ID : %d"),
    ACCESSORY_ASSIGNEE("Terminal Assignee : %s"),
    TERMINAL_ASSIGNE_SUCCESS("Terminal assigned to Ticket ID : %d"),
    TERMINAL_ASSIGNE_ERROR("Terminal assign faild to Ticket ID : %d"),
    TERMINAL_ASSIGNEE("Ticket Assignee : %s"),
    REMINDERNOTIFY_DESC ("You are assigned with Ticket Id %d");
    private final String message;

    AffinitiMessages(String message) {
        this.message = message;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

}
