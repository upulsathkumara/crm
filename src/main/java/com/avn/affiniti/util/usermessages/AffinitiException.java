/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.util.usermessages;

/**
 * @Author : Roshen Dilshan
 * @Document : AffinitiException
 * @Created on : Jun 9, 2017, 3:48:50 PM
 */
public class AffinitiException extends Exception {

    private int errorCode;
    private String errorMsg;

    public AffinitiException(AffinitiErrorMessages code) {
        this.errorMsg = code.getMsg();
        this.errorCode = code.getId();
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

}
