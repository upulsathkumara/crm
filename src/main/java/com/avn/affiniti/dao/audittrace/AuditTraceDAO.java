
package com.avn.affiniti.dao.audittrace;

import com.avn.affiniti.hibernate.model.Audittrace;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author Kaushan Fernando
 * @Document AuditTraceDAO
 * @Created on: Sep 4, 2017, 4:12:38 PM
 */
public interface AuditTraceDAO {

    public void insertAuditTrace(Audittrace auditTrace) throws HibernateException, SQLException;

    public long getTableDataCount(HttpServletRequest request) throws HibernateException, SQLException, Exception;

    public List<JSONObject> getTableData(HttpServletRequest request, int minCount, int start) throws HibernateException, SQLException, Exception;
}
