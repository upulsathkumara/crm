package com.avn.affiniti.dao.audittrace;

import com.avn.affiniti.hibernate.model.Audittrace;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.json.JSONObject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Kaushan Fernando
 * @Document AuditTraceDAOImpl
 * @Created on: Sep 4, 2017, 4:13:01 PM
 */
@Repository("auditTraceDAO")
public class AuditTraceDAOImpl implements AuditTraceDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void insertAuditTrace(Audittrace auditTrace) throws HibernateException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        session.save(auditTrace);
    }

    @Override
    public long getTableDataCount(HttpServletRequest request) throws HibernateException, SQLException, Exception {
        String query = "SELECT COUNT(*) FROM audittrace A WHERE :where";

        String where = this.getWhere(request);
        query = query.replace(":where", where);

        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    @Override
    public List<JSONObject> getTableData(HttpServletRequest request, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT COALESCE(A.AUDITTRACEID, 'N/A') AS AUDITTRACEID, "
                + "COALESCE(A.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + "COALESCE(A.AFECTEDID, 'N/A') AS AFECTEDID, "
                + "COALESCE(A.TASK, 'N/A') AS TASK, "
                + "COALESCE(A.AFFECTEDPAGE, 'N/A') AS AFFECTEDPAGE, "
                + "COALESCE(A.CREATEDATETIME, 'N/A') AS CREATEDATETIME, "
                + "COALESCE(A.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(A.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM audittrace A "
                + "WHERE :where "
                + "ORDER BY A.AUDITTRACEID DESC "
                + "LIMIT ?,?";

        String where = this.getWhere(request);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new AudittraceTableViewRowMapper());
    }

    public class AudittraceTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("AUDITTRACEID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("audittraceid", rs.getInt("AUDITTRACEID"));
            object.put("description", rs.getString("DESCRIPTION"));
            object.put("afectedid", rs.getString("AFECTEDID"));
            object.put("afectedpage", rs.getString("AFFECTEDPAGE"));
            object.put("task", rs.getString("TASK"));
            object.put("responsibleuser", rs.getString("CREATEDUSER"));
            object.put("createddatetime", rs.getTimestamp("CREATEDATETIME"));

            return object;
        }
    }

    private String getWhere(HttpServletRequest request) {

        String from_date = request.getParameter("from_date");
        String to_date = request.getParameter("to_date");
        String affected_page = request.getParameter("affected_page");
        System.out.println(affected_page);
        String task = request.getParameter("task");
        System.out.println(task);
        String updated_user = request.getParameter("updated_user");
        System.out.println(updated_user);

        String where = "1 = 1 ";

        if (!request.getParameter("from_date").equals("")) {
            if (!to_date.equals("")) {
                where = where + "AND A.CREATEDATETIME BETWEEN '" + from_date + " 00:00:00' AND '" + to_date + " 23:59:59' ";
            } else {
                where = where + "AND A.CREATEDATETIME BETWEEN '" + from_date + " 00:00:00' AND '" + from_date + " 23:59:59' ";
            }
        } else {
            if (!to_date.equals("")) {
                where = where + "AND A.CREATEDATETIME BETWEEN '" + to_date + " 00:00:00' AND '" + to_date + " 23:59:59' ";
            }
        }
        if (!affected_page.equals("")) {
            where = where + "AND A.AFFECTEDPAGE = '" + affected_page + "' ";
        }
        if (!task.equals("")) {
            where = where + "AND A.TASK = '" + task + "' ";
        }
        if (!updated_user.equals("")) {
            where = where + "AND A.CREATEDUSER = '" + updated_user + "' ";
        }
        System.out.println(where);
        return where;
    }

}
