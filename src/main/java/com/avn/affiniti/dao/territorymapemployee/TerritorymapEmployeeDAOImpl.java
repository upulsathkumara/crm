/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.territorymapemployee;

import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TerritorymapEmployeeDAOImpl
 * @Created on : Jul 20, 2016, 12:40:28 AM
 */
@Repository("territorymapEmployeeDAO")
public class TerritorymapEmployeeDAOImpl implements TerritorymapEmployeeDAO {

    @Autowired
    private DataSource dataSource;

    @Override
    public void insertTerritorymapEmployee(User user) throws SQLException {
        String query = "INSERT INTO territorymapemployee "
                + "(TERRITORYMAPID, "
                + "EMPLOYEEID, "
                + "CREATEDATETIME, "
                + "LASTUPDATEDDATETIME, "
                + "CREATEDUSER) "
                + "VALUES "
                + "(?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        for (int territory : user.getTerritories()) {
            jdbcTemplate.update(query,
                    territory,
                    user.getEmployeeid(),
                    user.getCreateduser());
        }
    }

    @Override
    public void deleteTerritorymapEmployeeByEmployee(User user) throws SQLException {
        String query = "DELETE FROM territorymapemployee WHERE EMPLOYEEID = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{user.getEmployeeid()});
    }

}
