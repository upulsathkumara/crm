/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.territorymapemployee;

import com.avn.affiniti.model.user.User;
import java.sql.SQLException;

/**
 * @Author : Roshen Dilshan
 * @Document : TerritorymapEmployeeDAO
 * @Created on : Jul 20, 2016, 12:39:51 AM
 */
public interface TerritorymapEmployeeDAO {
    
    public void insertTerritorymapEmployee(User user) throws SQLException;
    
    public void deleteTerritorymapEmployeeByEmployee(User user) throws SQLException;

}
