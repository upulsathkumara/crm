package com.avn.affiniti.dao.organization;

import com.avn.affiniti.hibernate.model.License;
import com.avn.affiniti.hibernate.model.Organization;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author Kaushan Fernando
 * @Document OrganizationDAO
 * @Created on: Sep 20, 2017, 2:55:59 PM
 */
public interface OrganizationDAO {

    public List<JSONObject> getTableData(String organizationDescription, int location, int minCount, int start) throws HibernateException, SQLException, Exception;

    public long getTableDataCount(String organizationDescription, int location) throws HibernateException, SQLException, Exception;

    public JSONObject saveOrganization(Organization organizationToSave, String action) throws HibernateException, SQLException;

    public Organization getOrganizationByOrganizationId(int organizationId) throws HibernateException, SQLException, Exception;

    public void deleteOrganization(Organization organization) throws HibernateException, SQLException, Exception;

    public License getLicense(int id) throws HibernateException;
    
    public Organization getOrganizationId(int id) throws HibernateException;

}
