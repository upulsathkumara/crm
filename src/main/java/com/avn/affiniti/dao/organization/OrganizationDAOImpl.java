package com.avn.affiniti.dao.organization;

import com.avn.affiniti.hibernate.model.License;
import com.avn.affiniti.hibernate.model.Organization;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kaushan Fernando
 * @Document OrganizationDAOImpl
 * @Created on: Sep 20, 2017, 2:56:41 PM
 */
@Repository("organizationDAO")
public class OrganizationDAOImpl implements OrganizationDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<JSONObject> getTableData(String organizationDescription, int location, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT ORG.ORGANIZATIONID AS ID,COALESCE(ORG.ORGANIZATIONCODE, 'N/A') AS ORGANIZATIONCODE, "
                + "COALESCE(ORG.DESCRIPTION, 'N/A') AS DESCRIPTION,COALESCE(ORG.EMAIL, 'N/A') AS EMAIL, "
                + "COALESCE(ORG.CONTACTNO, 'N/A') AS CONTACTNO, "
                + "COALESCE(ORG.DATEJOINED, 'N/A') AS DATEJOINED, "
                + "COALESCE(ORG.ACTIVATIONDATE, 'N/A') AS ACTIVATIONDATE, "
                + "COALESCE(ORG.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(ORG.CREATEDATETIME, 'N/A') AS CREATEDATETIME, "
                + "COALESCE(ORG.CREATEDUSER, 'N/A') AS CREATEDUSER, "
                + "COALESCE(ST.DESCRIPTION, 'N/A') AS STATUSDESCRIPTION, "
                + "COALESCE(LI.DESCRIPTION, 'N/A') AS LICENSEDESCRIPTION "
                + "FROM organization ORG INNER JOIN status ST ON ST.STATUSID = ORG.`STATUS` INNER JOIN "
                + "license LI ON LI.LICENSEID = ORG.LICENSE WHERE :where ORDER BY ORG.LASTUPDATEDDATETIME DESC LIMIT ?,?";

        String where = this.getWhere(organizationDescription, location);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new UserRoleTaskTableViewRowMapper());

    }

    @Override
    public long getTableDataCount(String organizationDescription, int location) throws HibernateException, SQLException, Exception {

        String query = "SELECT COUNT(*) FROM organization ORG WHERE :where";
        String where = this.getWhere(organizationDescription, location);
        query = query.replace(":where", where);
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    private String getWhere(String organizationDescription, int location) {

        String where = "ORG.LOCATION = " + location + " ";
        if (!organizationDescription.equals("")) {
            where = where + "AND ORG.DESCRIPTION LIKE '%" + organizationDescription + "%' ";
        } else {
            where = where + "AND 1 = 1 ";
        }
        return where;
    }

    @Override
    public JSONObject saveOrganization(Organization organizationToSave, String action) throws HibernateException, SQLException {
        JSONObject object = new JSONObject();

        boolean code = true;
        try {
            if (action.equalsIgnoreCase("save")) {
                action = "create";

            } else if (action.equalsIgnoreCase("edit")) {

                action = "update";
            }
            Session session = sessionFactory.getCurrentSession();
            if (action.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
                session.persist(organizationToSave);
            } else if (action.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
                session.merge(organizationToSave);
            } else {
                code = false;
                object.put("message", MessageVarList.ACCESS_DENIED);
            }

        } catch (Exception e) {
            code = false;
            throw e;
        }
        object.put("code", code);
        return object;
    }

    @Override
    public Organization getOrganizationByOrganizationId(int organizationId) throws HibernateException, SQLException, Exception {

        Session session = sessionFactory.getCurrentSession();
        String hql = "FROM Organization "
                + "WHERE organizationid = " + organizationId;
        return (Organization) session.createQuery(hql).uniqueResult();
    }

    @Override
    public void deleteOrganization(Organization organization) throws HibernateException, SQLException, Exception {

//          String hqlToDelete = "DELETE Organization "
//                + "WHERE organizationid = " + organizationId;
        Session session = sessionFactory.getCurrentSession();
        session.delete(organization);
        //session.createQuery(hqlToDelete).executeUpdate();
    }

    public class UserRoleTaskTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();
            int actions = rs.getInt("ID");

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("organizationcode", rs.getString("ORGANIZATIONCODE"));
            object.put("organizationname", rs.getString("DESCRIPTION"));
            object.put("email", rs.getString("EMAIL"));
            object.put("contactno", rs.getString("CONTACTNO"));
            object.put("license", rs.getString("LICENSEDESCRIPTION"));
            object.put("datejoined", rs.getString("DATEJOINED"));
            object.put("activationdate", rs.getString("ACTIVATIONDATE"));
            object.put("status", rs.getString("STATUSDESCRIPTION"));
            object.put("lastupdatedtime", rs.getString("LASTUPDATEDDATETIME"));
            object.put("createdtime", rs.getString("CREATEDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));
            object.put("action", action);

            return object;
        }
    }

    @Override
    public License getLicense(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        License license = session.load(License.class, id);
        Hibernate.initialize(license);
        return license;

    }

    @Override
    public Organization getOrganizationId(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Organization organization = session.load(Organization.class, id);
        Hibernate.initialize(organization);
        return organization;

    }

}
