/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.systemuser;

import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : SystemuserDAO
 * @Created on : Jun 23, 2017, 11:48:26 AM
 */
public interface SystemuserDAO {

    public Systemuser getSytemuserByUsername(String username) throws HibernateException;

    public Systemuser getSystemuserByEmployeeId(int employeeid) throws HibernateException;

    // Added by Nadun
    public void createOrUpdateSystemUser(Systemuser systemuser) throws HibernateException;

//    public void updateSystemUser(Systemuser systemuser) throws HibernateException;
    public boolean isUserExists(String username) throws SQLException;

    public int getHierarchyidIdByUsername(String username) throws SQLException;

//    public int getTableDataCount(User user, boolean isClient) throws SQLException;
    public int getTableDataCount(User user, int userroleid) throws SQLException;

//    public List<JSONObject> getTableData(User user, int start, int minCount, boolean isClient) throws SQLException;
    public List<JSONObject> getTableData(User user, int start, int minCount, int userroleid) throws SQLException;

    public int getUserRoleByUsername(String username) throws SQLException;

    public Systemuser getUserByUsername(String username) throws HibernateException;

    public void chagePassword(String username, String password) throws SQLException;

    public void updateFirstLogin(String username, int firstlogin) throws SQLException;

    public boolean isNotaDuplicateUsername(String username) throws SQLException;

    public boolean isNotaDuplicateUsernameUpdate(String username, String employeeId) throws SQLException;
}
