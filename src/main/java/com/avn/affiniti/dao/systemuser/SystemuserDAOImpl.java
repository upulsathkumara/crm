/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.systemuser;

import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.model.user.User;
import com.avn.affiniti.model.user.UserTableViewRowMapper;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.CommonVarList;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : SystemuserDAOImpl
 * @Created on : Jun 23, 2017, 11:48:52 AM
 */
@Repository("systemuserDAO")
public class SystemuserDAOImpl implements SystemuserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private DataSource dataSource;

    @Override
    public Systemuser getSytemuserByUsername(String username) throws HibernateException {
        return sessionFactory.getCurrentSession().get(Systemuser.class, username);
    }

    @Override
    public Systemuser getSystemuserByEmployeeId(int employeeid) throws HibernateException {
        return (Systemuser) sessionFactory.getCurrentSession().createQuery("FROM Systemuser SU "
                + ""
                + "WHERE SU.employee.employeeid = :employee").setInteger("employee", employeeid).uniqueResult();
    }

    // Added by Nadun
    @Override
    public void createOrUpdateSystemUser(Systemuser systemuser) throws HibernateException {
//        sessionFactory.getCurrentSession().saveOrUpdate(systemuser);
        sessionFactory.getCurrentSession().merge(systemuser);
    }

//    @Override
//    public void updateSystemUser(Systemuser systemuser) throws HibernateException {
//
//        String hql = "update Systemuser set "
//                + "username = :username, "
//                + "userrole = :userrole, "
//                + "usertype = :usertype, "
//                + "organization = :organization, "
//                + "client = :client, "
//                + "organizationhierarchy = :organizationhierarchy, "
//                + "status = :status, "
//                + "lastupdateddatetime = :lastupdateddatetime "
//                + "where employee.employeeid = :employeeid";
//        
//        String hql = "update Systemuser set "
//                + "username = :username, "
//                + "userrole = :userrole, "
//                + "usertype = :usertype, "
//                + "organization = :organization, "
//                + "client = :client, "
//                + "organizationhierarchy = :organizationhierarchy, "
//                + "status = :status, "
//                + "lastupdateddatetime = :lastupdateddatetime "
//                + "where employee.employeeid = :employeeid";
//
//        Query query = sessionFactory.getCurrentSession().createSQLQuery(hql);
//
//
//        query.setParameter("username", systemuser.getUsername());
//        query.setParameter("userrole", systemuser.getUserrole());
//        query.setParameter("usertype", systemuser.getUsertype());
//        query.setParameter("organization", systemuser.getOrganization());
//        query.setParameter("client", systemuser.getClient());
//        query.setParameter("organizationhierarchy", systemuser.getOrganizationhierarchy());
//        query.setParameter("status", systemuser.getStatus());
//        query.setParameter("lastupdateddatetime", new Date());
//        query.setParameter("employeeid", systemuser.getEmployee().getEmployeeid());
//
//        query.executeUpdate();
//    }
    @Override
    public boolean isUserExists(String username) throws SQLException {
        boolean isExists = false;
        String query = "SELECT "
                + "    COUNT(*) AS CNT "
                + "FROM "
                + "    systemuser SU "
                + "WHERE "
                + "    SU.USERNAME = ?";
        int count = new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Integer.class);
        if (count > 0) {
            isExists = true;
        }
        return isExists;
    }

    @Override
    public int getHierarchyidIdByUsername(String username) throws SQLException {
        String query = "SELECT HIERARCHYID FROM employee E INNER JOIN systemuser SU ON E.EMPLOYEEID = SU.USERID "
                + "WHERE SU.USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Integer.class);
    }

    @Override
    public int getTableDataCount(User user, int userroleid) throws SQLException {
        String searchKeyword = user.getSearchKeyword();
        String query = "SELECT COUNT(*) AS CNT "
                + "FROM systemuser SU "
                + "INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE "
                + "INNER JOIN status S ON S.STATUSID = SU.STATUS "
                + "INNER JOIN employee E ON SU.USERID = E.EMPLOYEEID "
                + "WHERE 1 = 1 :where ";
        String where = "";

        where += " AND UR.PARENTUSERROLE = " + userroleid;
//        if (isClient) {
//            where += " AND CLIENTUSER = " + 1;
//        } else {
//            where += " AND CLIENTUSER = " + 0;
//        }

        if (searchKeyword != null && !searchKeyword.trim().isEmpty()) {
            where += " AND SU.USERNAME LIKE '%" + searchKeyword.trim() + "%'"
                    + "OR LOWER(E.PREFERREDNAME) LIKE LOWER('%" + searchKeyword.trim() + "') "
                    + "OR UPPER(E.NIC) LIKE UPPER('%" + searchKeyword.trim() + "') "
                    + "OR E.CONTACTNO01 LIKE '%" + searchKeyword.trim() + "%'";
        }
        query = query.replace(":where", where);
        return new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
    }

    @Override
    public List<JSONObject> getTableData(User user, int start, int minCount, int userroleid) throws SQLException {
        String searchKeyword = user.getSearchKeyword();

        /*SELECT E.EMPLOYEEID, SU.USERNAME,   
         ISNULL(UR.DESCRIPTION, '--') AS USERROLE,   
         ISNULL(S.DESCRIPTION, '--') AS STATUS,   
         ISNULL(CAST(SU.USERATTEMPTS AS VARCHAR), '0') AS USERATTEMPTS,   
         ISNULL(E.PREFERREDNAME, '--') AS PREFERREDNAME,   
         ISNULL(E.SURENAME, '--') AS SURENAME,   
         ISNULL(CONVERT (VARCHAR(26),SU.CREATEDDATETIME,113 ), '--') AS CREATEDDATETIME,   
         ISNULL(CONVERT (VARCHAR(26),SU.LASTUPDATEDDATETIME, 113), '--') AS LASTUPDATEDDATETIME   
         FROM systemuser SU   INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE   INNER JOIN status S ON S.STATUSID = SU.STATUS   INNER JOIN employee E ON SU.USERID = E.EMPLOYEEID   WHERE 1 = 1   ORDER BY SU.USERNAME
         */
        String query = "SELECT E.EMPLOYEEID, "
                + "COALESCE(SU.USERNAME, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS USERNAME,   "
                + "COALESCE(UR.DESCRIPTION, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS USERROLE,   "
                + "COALESCE(S.DESCRIPTION, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS STATUS,   "
                + "COALESCE(SU.USERATTEMPTS, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS USERATTEMPTS,   "
                + "COALESCE(E.PREFERREDNAME, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS PREFERREDNAME,   "
                + "COALESCE(E.SURENAME, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS SURENAME,   "
                + "COALESCE(SU.CREATEDDATETIME, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS CREATEDDATETIME,   "
                + "COALESCE(SU.LASTUPDATEDDATETIME, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS LASTUPDATEDDATETIME,   "
                + "COALESCE(SU.CREATEDUSER, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS CREATEDUSER,   "
                + "COALESCE(SU.LASTUPDATEDUSER, '" + CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE_NA + "') AS LASTUPDATEDUSER   "
                + "FROM systemuser SU   "
                + "INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE   "
                + "INNER JOIN status S ON S.STATUSID = SU.STATUS   "
                + "INNER JOIN employee E ON SU.USERID = E.EMPLOYEEID   "
                + "WHERE 1 = 1  :where "
                + "ORDER BY SU.USERNAME "
                + "LIMIT ?, ? ";
        String where = "";

        where += " AND UR.PARENTUSERROLE = " + userroleid;
//        if (isClient) {
//            where += " AND CLIENTUSER = " + 1;
//        } else {
//            where += " AND CLIENTUSER = " + 0;
//        }

        if (searchKeyword != null && !searchKeyword.trim().isEmpty()) {
            where += " AND SU.USERNAME LIKE '%" + searchKeyword.trim() + "%'"
                    + "OR LOWER(E.PREFERREDNAME) LIKE LOWER('%" + searchKeyword.trim() + "') "
                    + "OR UPPER(E.NIC) LIKE UPPER('%" + searchKeyword.trim() + "') "
                    + "OR E.CONTACTNO01 LIKE '%" + searchKeyword.trim() + "%'";
        }
        query = query.replace(":where", where);
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new UserTableViewRowMapper());
    }

    @Override
    public int getUserRoleByUsername(String username) throws SQLException {
        String query = "SELECT "
                + "       USERROLE "
                + "FROM systemuser  "
                + "WHERE USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Integer.class);
    }

    @Override
    public Systemuser getUserByUsername(String username) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Systemuser WHERE username = :username");
        query.setString("username", username);
        return (Systemuser) query.uniqueResult();
    }

    @Override
    public void chagePassword(String username, String password) throws SQLException {
        String query = "UPDATE systemuser "
                + "SET "
                + "   PASSWORD = ?, "
                + "  LASTUPDATEDDATETIME = CURRENT_TIMESTAMP "
                + "WHERE USERNAME = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{Common.passwordHashed(password), username});
    }

    @Override
    public void updateFirstLogin(String username, int firstlogin) throws SQLException {
        String query = "UPDATE systemuser "
                + "SET "
                + "   FIRSTLOGIN = ?, "
                + "  LASTUPDATEDDATETIME = CURRENT_TIMESTAMP "
                + "WHERE USERNAME = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{firstlogin, username});
    }

    @Override
    public boolean isNotaDuplicateUsername(String username) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM systemuser WHERE LOWER(USERNAME) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username.toLowerCase()}, Integer.class) == 0;
    }

    @Override
    public boolean isNotaDuplicateUsernameUpdate(String username, String employeeId) throws SQLException {
        boolean isNotaDuplicateUsernameUpdate = false;
        String query = "FROM Systemuser E WHERE E.username = :username";
        Systemuser systemuser = (Systemuser) sessionFactory.getCurrentSession().createQuery(query).setString("username", username).uniqueResult();

        if (systemuser == null) {
            isNotaDuplicateUsernameUpdate = true;
        } else {
            if (String.valueOf(systemuser.getEmployee().getEmployeeid()).equals(employeeId)) {
                isNotaDuplicateUsernameUpdate = true;
            } else {
                isNotaDuplicateUsernameUpdate = false;
            }
        }
        return isNotaDuplicateUsernameUpdate;
    }

}
