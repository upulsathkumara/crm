package com.avn.affiniti.dao.subsectiontask;


import com.avn.affiniti.hibernate.model.Subsectiontask;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author Kaushan Fernando
   @Document SubSectionTaskDAO
   @Created on: Sep 25, 2017, 11:21:30 AM
 */
public interface SubSectionTaskDAO {
    
     public List<JSONObject> getTableData(String id, JSONObject object, int minCount, int start) throws HibernateException, SQLException, Exception;

    public long getTableDataCount(String id, JSONObject object) throws HibernateException, SQLException, Exception;

    public JSONObject saveSubSectionTask(Subsectiontask SubSectionTaskToSave,String action) throws HibernateException, SQLException;

    public List<Subsectiontask> getSubSectionTaskBySectionId(int sectionId,int subSectionid) throws HibernateException, SQLException, Exception;

    public void deleteSubSectionTask(int sectionId,int subSectionId,int taskId) throws HibernateException, SQLException, Exception;



}
