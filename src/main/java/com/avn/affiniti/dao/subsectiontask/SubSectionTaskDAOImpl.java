package com.avn.affiniti.dao.subsectiontask;

import com.avn.affiniti.hibernate.model.Subsectiontask;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kaushan Fernando
 * @Document SUbSectionDAOImpl
 * @Created on: Sep 25, 2017, 11:21:55 AM
 */
@Repository("subsectiontaskDAO")
public class SubSectionTaskDAOImpl implements SubSectionTaskDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<JSONObject> getTableData(String id, JSONObject object, int minCount, int start) throws HibernateException, SQLException, Exception {
        String query = "SELECT SUBST.SECTION AS ID,SUBST.SUBSECTION AS SUBSECTION_ID,IFNULL(SUBSEC.DESCRIPTION, 'N/A') AS SUBSECTION_DESCRIPTION, "
                + "IFNULL(SEC.DESCRIPTION, 'N/A') AS SECTION_DESCRIPTION,IFNULL(T.DESCRIPTION, 'N/A') AS TASK_DESCRIPTION, "
                + "IFNULL(SUBST.URL, 'N/A') AS URL,IFNULL(SUBST.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "IFNULL(SUBST.CREATEDDATETIME, 'N/A') AS CREATEDATETIME,IFNULL(SUBST.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM subsectiontask SUBST INNER JOIN section SEC ON SUBST.SECTION = SEC.SECTIONID INNER JOIN "
                + "task T ON SUBST.TASK = T.TASKID INNER JOIN subsection SUBSEC ON SUBST.SUBSECTION = SUBSEC.SUBSECTIONID "
                + "WHERE :where ORDER BY SUBST.LASTUPDATEDDATETIME DESC LIMIT ?,?";

        String where = this.getWhere(id, object);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new SubSectionTaskTableViewRowMapper());

    }

    @Override
    public long getTableDataCount(String id, JSONObject object) throws HibernateException, SQLException, Exception {
        String query = "SELECT COUNT(*) FROM subsectiontask SUBST WHERE :where";

        String where = this.getWhere(id, object);
        query = query.replace(":where", where);

        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    private String getWhere(String id, JSONObject object) {

        String where = "1 = 1 ";
        if (id.equals("") || id.equals("--Select--")) {
            where = "1 = 1 ";
        } else {
            if (!object.getString("sectionid").equalsIgnoreCase("")) {
                where = where + "AND SUBST.SECTION LIKE ('" + object.getString("sectionid") + "') ";
            }
            if (!object.getString("subsectionid").equalsIgnoreCase("")) {
                where = where + "AND SUBST.SUBSECTION LIKE ('" + object.getString("subsectionid") + "') ";
            }
            if (!object.getString("taskid").equalsIgnoreCase("")) {
                where = where + "AND SUBST.TASK LIKE ('" + object.getString("taskid") + "') ";
            }
        }
        return where;
    }

    @Override
    public JSONObject saveSubSectionTask(Subsectiontask SubSectionTaskToSave, String action) throws HibernateException, SQLException {
        JSONObject object = new JSONObject();
        boolean status = true;

        try {
            if (action.equalsIgnoreCase("save")) {
                action = "create";

            } else if (action.equalsIgnoreCase("edit")) {

                action = "update";
            }
            Session session = sessionFactory.getCurrentSession();
            if (action.equalsIgnoreCase("create")) {
                session.persist(SubSectionTaskToSave);
            } else if (action.equalsIgnoreCase("update")) {
                session.merge(SubSectionTaskToSave);
            } else {
                status = false;
                object.put("message", MessageVarList.ACCESS_DENIED);
                object.put("success", false);
            }

        } catch (Exception e) {
            status = false;
            throw e;
        }

        object.put("status", status);

        return object;
    }

    @Override
    public List<Subsectiontask> getSubSectionTaskBySectionId(int sectionId, int subSectionid) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();

        String hql = "FROM Subsectiontask "
                + "WHERE section = " + sectionId
                + " AND subsection = " + subSectionid;

        List<Subsectiontask> list = session.createQuery(hql).list();

        return list;
    }

    @Override
    public void deleteSubSectionTask(int sectionId, int subSectionId, int taskId) throws HibernateException, SQLException, Exception {
        String hqlToDelete = "DELETE Subsectiontask ST "
                + "WHERE ST.section.sectionid = " + sectionId
                + " AND ST.subsection.subsectionid = " + subSectionId
                + " AND ST.task.taskid = " + taskId;
        Session session = sessionFactory.getCurrentSession();
        session.createQuery(hqlToDelete).executeUpdate();
    }

    public class SubSectionTaskTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();
            String actions = rs.getInt("ID") + ":" + rs.getInt("SUBSECTION_ID");

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("section", rs.getString("SECTION_DESCRIPTION"));
            object.put("subsection", rs.getString("SUBSECTION_DESCRIPTION"));
            object.put("task", rs.getString("TASK_DESCRIPTION"));
            object.put("url", rs.getString("URL"));
            object.put("lastupdatedtime", rs.getString("LASTUPDATEDDATETIME"));
            object.put("createdtime", rs.getString("CREATEDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));
            object.put("action", action);

            return object;
        }
    }
}
