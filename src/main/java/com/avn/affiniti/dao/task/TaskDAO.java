package com.avn.affiniti.dao.task;

import com.avn.affiniti.hibernate.model.Task;
import java.sql.SQLException;

/**
 *
 * @author Kaushan Fernando
 * @Document TaskDAO
 * @Created on: Nov 20, 2017, 11:59:58 AM
 */
public interface TaskDAO {

    public String getTaskIdByTaskName(String action) throws SQLException;
    
    public Task getTaskById(int taskid) throws Exception; 
}
