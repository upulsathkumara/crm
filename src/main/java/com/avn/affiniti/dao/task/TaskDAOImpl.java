package com.avn.affiniti.dao.task;

import com.avn.affiniti.hibernate.model.Task;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kaushan Fernando
 * @Document TaskDAOImpl
 * @Created on: Nov 20, 2017, 12:00:20 PM
 */
@Repository("taskDAO")
public class TaskDAOImpl implements TaskDAO {

    @Autowired
    private DataSource dataSource;
    
    @Autowired
    private SessionFactory sessionFactory;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

//    @Override
//    public String getDescriptionByTaskId(int taskId) throws SQLException {
//
//        String query = "SELECT DESCRIPTION FROM task T WHERE T.TASKID = ?";
//
//        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{taskId}, String.class);
//    }
    @Override
    public String getTaskIdByTaskName(String action) throws SQLException {
        String taskId = "0";
        String query = "SELECT T.TASKID as TASKID FROM task T WHERE T.DESCRIPTION = ?";

        taskId = new JdbcTemplate(dataSource).queryForObject(query, new Object[]{action}, String.class);
        
        return taskId;
    }

    @Override
    public Task getTaskById(int taskid) throws Exception {
        return sessionFactory.getCurrentSession().get(Task.class, taskid);
    }
    

}
