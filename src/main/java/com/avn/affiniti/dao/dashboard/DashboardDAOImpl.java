/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.dashboard;

import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.util.Common;
import static com.avn.affiniti.util.Common.getEndingDateTimeOfMonth;
import static com.avn.affiniti.util.Common.getStartingDateTimeOfMonth;
import static com.avn.affiniti.util.Common.getStringFormatDate;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @Author : Office Isuru
 * @Document : DashboardDAOImpl
 * @Date : Aug 23, 2015 12:16:49 PM
 */
@Repository("dashboardDAO")
public class DashboardDAOImpl implements DashboardDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

//    public void setDataSource(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }
    @Override
    public JSONArray getCaseCount(String username, String column) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray jArray = new JSONArray();
        Date startdate;
        Calendar calendar = Calendar.getInstance();
        try {
            List<Date> dateList = new ArrayList<>();
            String sql1 = "SELECT DATE_ADD(CURDATE(), INTERVAL (MOD(DAYOFWEEK(CURDATE())-1, 7)*-1) DAY) AS week_start, DATE_ADD(CURDATE(), INTERVAL ((MOD(DAYOFWEEK(CURDATE())-1, 7)*-1)+6) DAY) AS week_end ";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql1);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                calendar.setTime(resultSet.getTimestamp("week_start"));
            }
            for (int i = 0; i < 7; i++) {
                dateList.add(calendar.getTime());
                calendar.add(Calendar.DATE, +1);

            }

            String sql2 = "SELECT   COUNT(*) AS CSCOUNT, T.CREATEDDATETIME "
                    + "FROM     ticket T "
                    + "WHERE    T.CREATEDDATETIME BETWEEN ? AND ? AND :COLUMN = ? "
                    + "GROUP BY T.CREATEDDATETIME";
            sql2 = sql2.replace(":COLUMN", column);
            for (Date date : dateList) {
                statement = connection.prepareStatement(sql2);
//                System.out.println(getStringFormatDateandTime(CommonVarList.DATE_FORMAT_YYYY_MM_dd_hhmmssSSa, new Timestamp(Common.getStartingTimeofDay(date).getTime())));
//                System.out.println(getStringFormatDateandTime(CommonVarList.DATE_FORMAT_YYYY_MM_dd_hhmmssSSa, new Timestamp(Common.getEndingTimeofDay(date).getTime())));
                statement.setTimestamp(1, new Timestamp(Common.getStartingTimeofDay(date).getTime()));
                statement.setTimestamp(2, new Timestamp(Common.getEndingTimeofDay(date).getTime()));
                statement.setString(3, username);
                boolean isDataSet = false;
                resultSet = statement.executeQuery();
                int cscount = 0;
                JSONObject obj = new JSONObject();
                while (resultSet.next()) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, date));
                    cscount += resultSet.getInt("CSCOUNT");
                    isDataSet = true;
                }
                if (isDataSet) {
                    obj.put("y", cscount);
                    jArray.put(obj);
                }
//                obj.put("y", cscount);
//                jArray.put(obj);
                if (!isDataSet) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, date));
                    obj.put("y", 0);
                    jArray.put(obj);
                }
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return jArray;
    }

    @Override
    public JSONArray getCallCount(String username) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray jArray = new JSONArray();
        Calendar calendar = Calendar.getInstance();
        try {
            List<Date> dateList = new ArrayList<>();
            String sql = "SELECT DATE_ADD(CURDATE(), INTERVAL (MOD(DAYOFWEEK(CURDATE())-1, 7)*-1) DAY) AS week_start, DATE_ADD(CURDATE(), INTERVAL ((MOD(DAYOFWEEK(CURDATE())-1, 7)*-1)+6) DAY) AS week_end ";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                calendar.setTime(resultSet.getTimestamp("week_start"));
            }
            for (int i = 0; i < 7; i++) {
                dateList.add(calendar.getTime());
                calendar.add(Calendar.DATE, +1);

            }
            String sql2 = "SELECT   COUNT(*) AS CLCOUNT, CL.CREATEDDATETIME "
                    + "FROM     calllog CL "
                    + "WHERE    CL.CREATEDDATETIME BETWEEN ? AND ? AND CL.CREATEDUSER = ? "
                    + "GROUP BY CL.CREATEDDATETIME";
            for (Date date : dateList) {
                statement = connection.prepareStatement(sql2);
                statement.setTimestamp(1, new Timestamp(Common.getStartingTimeofDay(date).getTime()));
                statement.setTimestamp(2, new Timestamp(Common.getEndingTimeofDay(date).getTime()));
                statement.setString(3, username);
                boolean isDataSet = false;
                resultSet = statement.executeQuery();
                JSONObject obj = new JSONObject();
                int clcount = 0;
                while (resultSet.next()) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, date));
                    clcount += resultSet.getInt("CLCOUNT");
                    isDataSet = true;
                }
                if (isDataSet) {
                    obj.put("z", clcount);
                    jArray.put(obj);
                }

                if (!isDataSet) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, date));
                    obj.put("z", 0);
                    jArray.put(obj);
                }
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return jArray;
    }

    @Override
    public JSONArray getProduntCount(String username, String column, Date date) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray objList = new JSONArray();
        try {
//            String sql = "select count(P.DESCRIPTION) as Pcount, P.DESCRIPTION as PDESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-mm')as ENTRIES from AVN_CASE C, AVN_PRODUCT P where C.PRODUCTID=P.PRODUCTID and to_char(C.CREATEDDATETIME, 'yyyy-mm') =? :COLUMN= ?  Group by P.DESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-mm')";
            String sql = "SELECT P.DESCRIPTION, COUNT(T.TICKETID)"
                    + "  FROM product P LEFT OUTER JOIN ticket T ON T.PRODUCTID = P.PRODUCTID "
                    + "                   WHERE C.CREATEDDATETIME BETWEEN ? and ?"
                    + "                  GROUP BY PRD.DESCRIPTION, DATE_FORMAT(C.CREATEDDATETIME, '%Y-%m')";
//            sql = sql.replace(":COLUMN", column);
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
//            statement.setString(1, getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, new java.sql.Date(System.currentTimeMillis())));
            statement.setTimestamp(1, new Timestamp(getStartingDateTimeOfMonth(date).getTime()));
            statement.setTimestamp(2, new Timestamp(getEndingDateTimeOfMonth(date).getTime()));
            statement.setString(3, username);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject obj = new JSONObject();
                obj.put("value", resultSet.getInt("Pcount"));
                obj.put("label", resultSet.getString("PDESCRIPTION"));
                objList.put(obj);
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return objList;
    }

//    @Override
//    public JSONArray getDepartmentCount(String username, String column, Date date) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        JSONArray objList = new JSONArray();
//        try {
////            String sql = "select count(D.DEPARTMENTID) as Ccount, D.DESCRIPTION as DDESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-MM')as ENTRIES from AVN_CASE C, AVN_DEPARTMENT D where C.DEPARTMENTID=D.DEPARTMENTID and to_char(C.CREATEDDATETIME, 'yyyy-MM') =? :COLUMN=?   Group by D.DESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-MM')";
//            String sql = "SELECT COUNT( DPM.DEPARTMENTID) AS Ccount, DPM.DESCRIPTION AS  DDESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-MM')as ENTRIES "
//                    + " FROM AVN_CASE C "
//                    + " INNER JOIN AVN_DEPARTMENT DPM "
//                    + " ON C.DEPARTMENTID = DPM.DEPARTMENTID "
//                    + " INNER JOIN AVN_PRODUCT PRD "
//                    + " ON C.PRODUCTID = PRD.PRODUCTID "
//                    + " WHERE C.CREATEDDATETIME BETWEEN ? and ?  :COLUMN=?"
//                    + " GROUP BY DPM.DESCRIPTION,to_char(C.CREATEDDATETIME, 'yyyy-MM')";
//            sql = sql.replace(":COLUMN", column);
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setTimestamp(1, new Timestamp(getStartingDateTimeOfMonth(date).getTime()));
//            statement.setTimestamp(2, new Timestamp(getEndingDateTimeOfMonth(date).getTime()));
////            statement.setString(1, getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, new java.sql.Date(System.currentTimeMillis())));
//            statement.setString(3, username);
//            resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                JSONObject obj = new JSONObject();
//                obj.put("Count", resultSet.getString("Ccount"));
//                obj.put("Des", resultSet.getString("DDESCRIPTION"));
//                obj.put("year", resultSet.getString("ENTRIES"));
//                objList.put(obj);
//            }
//        } catch (SQLException | JSONException exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return objList;
//    }
    @Override
    public JSONArray getProduntCountOrganizationwise(Date date) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray objList = new JSONArray();
        try {
////            String sql = "select count(P.DESCRIPTION) as Pcount, P.DESCRIPTION as PDESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-mm')as ENTRIES from AVN_CASE C, AVN_PRODUCT P where C.PRODUCTID=P.PRODUCTID and to_char(C.CREATEDDATETIME, 'yyyy-mm') =? and C.STATUSID='1' Group by P.DESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-mm')";
//            String sql = "SELECT COUNT(PRD.DESCRIPTION) AS Pcount, "
//                    + "       PRD.DESCRIPTION AS PDESCRIPTION, "
//                    + "       DATE_FORMAT(C.TICKETDATE, '%Y-%m') AS ENTRIES "
//                    + "  FROM ticket C INNER JOIN product PRD ON C.PRODUCTID = PRD.PRODUCTID "
//                    + " WHERE C.TICKETDATE BETWEEN ? AND ? AND PRD.STATUS = ? "
//                    + "GROUP BY PRD.DESCRIPTION, DATE_FORMAT(C.TICKETDATE, '%Y-%m')";

            String sql = "SELECT "
                    + "COUNT(C.TICKETTYPEID) AS Pcount, TY.DESCRIPTION AS PDESCRIPTION,  "
                    + "DATE_FORMAT(C.TICKETDATE, '%Y-%m') AS ENTRIES "
                    + " FROM ticket C INNER JOIN tickettype TY ON C.TICKETTYPEID=TY.CASETYPEID "
                    + "WHERE "
                    + "  C.TICKETDATE BETWEEN ? AND ?  "
                    + "GROUP BY "
                    + "TY.DESCRIPTION,DATE_FORMAT(C.TICKETDATE, '%Y-%m')";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setTimestamp(1, new Timestamp(getStartingDateTimeOfMonth(date).getTime()));
            statement.setTimestamp(2, new Timestamp(getEndingDateTimeOfMonth(date).getTime()));
//            statement.setInt(3, MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject obj = new JSONObject();
                obj.put("value", resultSet.getInt("Pcount"));
                obj.put("label", resultSet.getString("PDESCRIPTION"));
                objList.put(obj);
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return objList;
    }

    @Override
    public JSONArray getDepartmentCountbyOrganizationwise(Date date) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray objList = new JSONArray();
        try {
//            String sql = "select count(D.DEPARTMENTID) as Ccount, D.DESCRIPTION as DDESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-MM')as ENTRIES from AVN_CASE C, AVN_DEPARTMENT D where C.DEPARTMENTID=D.DEPARTMENTID and to_char(C.CREATEDDATETIME, 'yyyy-MM') =? and C.STATUSID='1'  Group by D.DESCRIPTION, to_char(C.CREATEDDATETIME, 'yyyy-MM')";
            String sql = "SELECT P.DESCRIPTION,"
                    + "       (SELECT COUNT(T.TICKETID)"
                    + "          FROM ticket T "
                    + "         WHERE     T.PRODUCTID = P.PRODUCTID "
                    + "               AND T.TICKETDATE BETWEEN ? "
                    + "                                    AND ?) "
                    + "          AS CNT "
                    + "  FROM product P ";
//                    + "WHERE STATUS = " + MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE;

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setTimestamp(1, new Timestamp(getStartingDateTimeOfMonth(date).getTime()));
            statement.setTimestamp(2, new Timestamp(getEndingDateTimeOfMonth(date).getTime()));
//            statement.setString(1, getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, new java.sql.Date(System.currentTimeMillis())));
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject obj = new JSONObject();
                obj.put("Count", resultSet.getString("CNT"));
                obj.put("Des", resultSet.getString("DESCRIPTION"));
//                obj.put("year", resultSet.getString("ENTRIES"));
                objList.put(obj);
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return objList;
    }

    @Override
    public JSONArray getDepartmentList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql2 = "SELECT P.DESCRIPTION FROM product P INNER JOIN tickettypeproduct TY ON TY.PRODUCTID=P.PRODUCTID WHERE TY.TICKETTYPEID=3";
        JSONArray objList = new JSONArray();
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql2);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject obj = new JSONObject();
                obj.put("Departments", resultSet.getString("DESCRIPTION"));
                obj.put("Year", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, new java.sql.Date(System.currentTimeMillis())));
                objList.put(obj);
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return objList;
    }

    @Override
    public long getUserTicketCountByStatus(int status, String username) throws SQLException {
        String query = "SELECT COUNT(T.TICKETID) "
                + "  FROM ticket T INNER JOIN systemuser S ON S.EMPLOYEEID = T.ASSIGNEEID "
                + " WHERE T.STATUS = ? AND S.USERID = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{status, username}, Long.class);
    }

    @Override
    public long getOrgTicketCountByStatus(int status) throws SQLException {
        String query = "SELECT COUNT(T.TICKETID) "
                + "  FROM ticket T "
                + " WHERE T.STATUS = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{status}, Long.class);
    }

    @Override
    public long getUserTicketCountByStatusNotIn(int[] status, String username) throws SQLException {
        String query = "SELECT COUNT(T.TICKETID) "
                + "  FROM ticket T INNER JOIN systemuser S ON S.EMPLOYEEID = T.ASSIGNEEID "
                + " WHERE T.STATUS NOT IN(" + Arrays.toString(status).replace("[", "").replace("]", "") + ") AND S.USERID = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Long.class);
    }

    @Override
    public long getUserTicketCountByStatusIn(int[] status, String username) throws SQLException {
        String query = "SELECT COUNT(T.TICKETID) "
                + "  FROM ticket T INNER JOIN systemuser S ON S.EMPLOYEEID = T.ASSIGNEEID "
                + " WHERE T.STATUS IN(" + Arrays.toString(status).replace("[", "").replace("]", "") + ") AND S.USERID = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Long.class);
    }

    @Override
    public long getOrgTicketCountByStatusNotIn(int[] status) throws SQLException {
        String query = "SELECT COUNT(T.TICKETID) "
                + "  FROM ticket T "
                + " WHERE T.STATUS NOT IN(" + Arrays.toString(status).replace("[", "").replace("]", "") + ")";
        return new JdbcTemplate(dataSource).queryForObject(query, Long.class);
    }

    @Override
    public double getLeadClosedTimeHours(int status, long assignid) throws SQLException {
        String query = "SELECT   round((sum(time_to_sec(timediff(lead.SALECLOSEDDATE, lead.CREATEDDATETIME))   "
                + "       / 3600)/24) /count(*), 2) AS HOURDIFF  "
                + "  FROM lead"
                + " WHERE lead.STATUS = ? AND lead.ASSIGNEEID = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{status, assignid}, double.class);
    }

    @Override
    public long getOrgTicketCountByStatusIn(int[] status) throws SQLException {
        String query = "SELECT COUNT(T.TICKETID) "
                + "  FROM ticket T "
                + " WHERE T.STATUS IN(" + Arrays.toString(status).replace("[", "").replace("]", "") + ")";
        return new JdbcTemplate(dataSource).queryForObject(query, Long.class);
    }

    @Override
    public JSONArray getLeadCloseOverTime(long assignid) throws Exception {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray jArray = new JSONArray();
        Calendar calendar = Calendar.getInstance();
        try {
            List<Date> dateList = new ArrayList<>();
            String sql = "SELECT CURRENT_TIMESTAMP AS CURRENTDATETIME FROM DUAL";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                calendar.setTime(resultSet.getTimestamp("CURRENTDATETIME"));
            }
            for (int i = 0; i < 6; i++) {
                dateList.add(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
            }
            Collections.reverse(dateList);
            String sql2 = "select count(*) AS COUNT, DATE_FORMAT(lead.CREATEDDATETIME, '%Y-%m') FROM lead "
                    + " WHERE lead.STATUS = ? AND lead.ASSIGNEEID = ?  and lead.CREATEDDATETIME BETWEEN ? AND ? "
                    + " group by DATE_FORMAT(lead.CREATEDDATETIME, '%Y-%m')";
            for (Date date : dateList) {
                statement = connection.prepareStatement(sql2);
//                statement.setInt(1, MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED);
                statement.setLong(2, assignid);
                statement.setTimestamp(3, new Timestamp(Common.getStartingDateTimeOfMonth(date).getTime()));
                statement.setTimestamp(4, new Timestamp(Common.getEndingDateTimeOfMonth(date).getTime()));

                boolean isDataSet = false;
                resultSet = statement.executeQuery();
                JSONObject obj = new JSONObject();
                int clcount = 0;
                while (resultSet.next()) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, date));
                    clcount += resultSet.getInt("COUNT");
                    isDataSet = true;
                }
                if (isDataSet) {
                    obj.put("y", clcount);
                    jArray.put(obj);
                }

                if (!isDataSet) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, date));
                    obj.put("y", 0);
                    jArray.put(obj);
                }
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return jArray;
    }

    public JSONArray getLeadLostOverTime(long assignid) throws Exception {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        JSONArray jArray = new JSONArray();
        Calendar calendar = Calendar.getInstance();
        try {
            List<Date> dateList = new ArrayList<>();
            String sql = "SELECT CURRENT_TIMESTAMP AS CURRENTDATETIME FROM DUAL";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                calendar.setTime(resultSet.getTimestamp("CURRENTDATETIME"));
            }
            for (int i = 0; i < 6; i++) {
                dateList.add(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
            }
            String sql2 = "select count(*) AS COUNT, DATE_FORMAT(lead.CREATEDDATETIME, '%Y-%m') FROM lead "
                    + " WHERE lead.STATUS = ? AND lead.ASSIGNEEID = ?  and lead.CREATEDDATETIME BETWEEN ? AND ? "
                    + " group by DATE_FORMAT(lead.CREATEDDATETIME, '%Y-%m')";
            Collections.reverse(dateList);
            for (Date date : dateList) {
                statement = connection.prepareStatement(sql2);
//                statement.setInt(1, MasterDataVarList.CCL_CODE_STATUS_LEAD_LOST);
                statement.setLong(2, assignid);
                statement.setTimestamp(3, new Timestamp(Common.getStartingDateTimeOfMonth(date).getTime()));
                statement.setTimestamp(4, new Timestamp(Common.getEndingDateTimeOfMonth(date).getTime()));

                boolean isDataSet = false;
                resultSet = statement.executeQuery();
                JSONObject obj = new JSONObject();
                int clcount = 0;
                while (resultSet.next()) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, date));
                    clcount += resultSet.getInt("COUNT");
                    isDataSet = true;
                }
                if (isDataSet) {
                    obj.put("z", clcount);
                    jArray.put(obj);
                }

                if (!isDataSet) {
                    obj.put("m", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM, date));
                    obj.put("z", 0);
                    jArray.put(obj);
                }
            }
        } catch (SQLException | JSONException exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return jArray;
    }

    public double getLeadCount(int status, long assignid) throws SQLException {
        String query = " select count(*) FROM lead "
                + " WHERE lead.STATUS = ? AND lead.ASSIGNEEID = ? ";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{status, assignid}, double.class);
    }

    ///gaje
    @Override
    public long getTableDataCount(Ticket parameters, String username) throws HibernateException, SQLException, Exception {

        Session session = sessionFactory.getCurrentSession();

        Query query;

        String hqll = "SELECT COUNT(*) "
                + "FROM Ticket T "
                + "INNER JOIN Systemuser SU ON SU.employee.employeeid = T.employee.employeeid "
                + "INNER JOIN Status S ON S.statusid = T.status.statusid "
                + "WHERE SU.username = :username AND S.isfinalstatus <> :isfinalstatus";
        query = session.createQuery(hqll);

        query.setParameter("username", username);
        query.setParameter("isfinalstatus", MasterDataVarList.AFFINITI_CODE_ISFINALSTATUS);

        return (long) query.uniqueResult();
    }

    @Override
    public long getIdlingTableDataCount(Ticket parameters, String username) throws HibernateException, SQLException, Exception {

        Session session = sessionFactory.getCurrentSession();

        Query query;
        String hql = "SELECT COUNT(*) "
                + "FROM Ticket T "
                + "JOIN Employee E ON E.employeeid = T.employee.employeeid "
                + "JOIN Systemuser SU ON SU.employee.employeeid = E.employeeid "
                + "JOIN Product P ON P.productid = T.product.productid "
                + "WHERE (TIMESTAMPDIFF(HOUR, T.createddatetime, NOW()) >= 8) AND SU.username = :username AND T.status.statusid =:statusid";
        query = session.createQuery(hql);

        query.setParameter("username", username);
        query.setParameter("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_INPROGRESS);
        return (long) query.uniqueResult();

    }

    @Override
    public List<Ticket> getTableData(Ticket parameters, String username, int minCount, int start) throws HibernateException, SQLException, Exception {

        Session session = sessionFactory.getCurrentSession();

        Query query;

//        query = session.createQuery("SELECT S.employee.tickets FROM Systemuser S WHERE S.username=:username");
        query = session.createQuery("FROM Ticket T WHERE T.status.isfinalstatus <> :isfinalstatus AND  T.employee.employeeid = (SELECT S.employee.employeeid FROM Systemuser S WHERE S.username=:username) ORDER BY T.ticketid DESC");
//        query = session.createQuery("FROM Ticket T "
//                + "INNER JOIN Systemuser SU ON SU.employee.employeeid = T.employee.employeeid "
//                + "INNER JOIN Status S ON S.statusid = T.status.statusid "
//                + "WHERE SU.username = :username AND S.isfinalstatus <> :isfinalstatus");
        query.setParameter("username", username);
        query.setParameter("isfinalstatus", MasterDataVarList.AFFINITI_CODE_ISFINALSTATUS);

        query.setMaxResults(minCount);
        query.setFirstResult(start);
        return query.list();
    }

    @Override
    public List<Object> getIdlingTableData(Ticket parameters, String username, int minCount, int start) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();

        Query query;

        String hql1 = "SELECT T.ticketid, T.ticketdate, E.nameinfull, T.subject, P.description, T.lastupdateddatetime, T.createddatetime, T.createduser "
                + "FROM Ticket T "
                + "JOIN Employee E ON E.employeeid = T.employee.employeeid "
                + "JOIN Systemuser SU ON SU.employee.employeeid = E.employeeid "
                + "JOIN Product P ON P.productid = T.product.productid "
                + "WHERE (TIMESTAMPDIFF(HOUR, T.createddatetime, NOW()) >= 8) AND SU.username = :username AND T.status.statusid =:statusid";
//
//        String hql = "SELECT T.ticketid, T.ticketdate, E.nameinfull, T.subject, T.prodcut.description, T.lastupdatedatetime, T.createddatetime, T.createduser "
//                + "FROM Ticket T "
//                + "INNER JOIN Systemuser SU ON SU.employee.employeeid = T.employee.employeeid "
//                + "INNER JOIN Employee E ON E.employeeid = T.employee.employeeid "
//                + "WHERE (TIMESTAMPDIFF(HOUR, T.createddatetime, NOW()) >= 8) AND SU.username = :username AND T.status.statusid =:statusid";
        query = session.createQuery(hql1);
        query.setParameter("username", username);
        query.setParameter("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_INPROGRESS);

        query.setMaxResults(minCount);
        query.setFirstResult(start);
        return query.list();

        //List<Ticket> ticketlist = query.list();
    }

    @Override
    public long getOpenTicketCount(String username) throws HibernateException, SQLException {
        Session session = sessionFactory.getCurrentSession();

        Query query;
        String hql = "SELECT COUNT(*) "
                + "FROM Ticket T "
                + "INNER JOIN Systemuser SU ON SU.employee.employeeid = T.employee.employeeid "
                + "INNER JOIN Status S ON S.statusid = T.status.statusid "
                + "WHERE SU.username = :username AND S.isfinalstatus <> :isfinalstatus";
        query = session.createQuery(hql);

        query.setParameter("username", username);
        query.setParameter("isfinalstatus", MasterDataVarList.AFFINITI_CODE_ISFINALSTATUS);
        return (long) query.uniqueResult();

//////////////////////
        // String hql = "SELECT COUNT(*) FROM Ticket T WHERE T.status.statusid =:statusid";
        // Query query = session.createQuery(hql);
        //query.setParameter("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_OPEN);
        //return (long) query.uniqueResult();
    }

    @Override
    public long getInProgressTicketCount(String username) throws HibernateException, SQLException {
        Session session = sessionFactory.getCurrentSession();

        Query query;
        String hql = "SELECT COUNT(*) "
                + "FROM Ticket T "
                + "INNER JOIN Systemuser SU ON SU.employee.employeeid = T.employee.employeeid "
                + "WHERE SU.username = :username AND T.status.statusid =:statusid";
        query = session.createQuery(hql);

        query.setParameter("username", username);
        query.setParameter("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_INPROGRESS);
        return (long) query.uniqueResult();

    }

    @Override
    public long getOrgInProgressTicketCount(String username) throws HibernateException, SQLException {
        Session session = sessionFactory.getCurrentSession();

        String hql = "SELECT COUNT(*) FROM Ticket T "
                + "INNER JOIN Organization O ON O.organizationid = T.organization.organizationid "
                + "WHERE T.status.statusid = :statusid";
        Query query = session.createQuery(hql);
        query.setParameter("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_INPROGRESS);

        return (long) query.uniqueResult();

    }

    @Override
    public long getIdlingTicketCount(String username) throws HibernateException, SQLException {
        Session session = sessionFactory.getCurrentSession();
        ///
        Query query;
        String hql = "SELECT COUNT(*) "
                + "FROM Ticket T "
                + "JOIN Employee E ON E.employeeid = T.employee.employeeid "
                + "JOIN Systemuser SU ON SU.employee.employeeid = E.employeeid "
                + "JOIN Product P ON P.productid = T.product.productid "
                + "WHERE (TIMESTAMPDIFF(HOUR, T.createddatetime, NOW()) >= 8) AND SU.username = :username AND T.status.statusid =:statusid";
        query = session.createQuery(hql);

        query.setParameter("username", username);
        query.setParameter("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_INPROGRESS);
        return (long) query.uniqueResult();

    }

}
