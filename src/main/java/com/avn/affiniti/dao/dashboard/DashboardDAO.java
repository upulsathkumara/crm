/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.dashboard;

import com.avn.affiniti.hibernate.model.Ticket;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONArray;

/**
 *
 * @author ISURU
 */
public interface DashboardDAO {

    public JSONArray getCaseCount(String username, String column) throws Exception;

    public JSONArray getCallCount(String username) throws Exception;

//    public JSONArray getDepartmentCount(String username, String column, Date date) throws Exception;
    public JSONArray getProduntCount(String username, String column, Date date) throws Exception;

    public JSONArray getProduntCountOrganizationwise(Date date) throws Exception;

    public JSONArray getDepartmentCountbyOrganizationwise(Date date) throws Exception;

//       public JSONArray getDepartmentCountbyOrganizationwise() throws Exception;
    public JSONArray getDepartmentList() throws Exception;

    public long getUserTicketCountByStatus(int status, String username) throws SQLException;

    public long getUserTicketCountByStatusNotIn(int[] status, String username) throws SQLException;

    public long getUserTicketCountByStatusIn(int[] status, String username) throws SQLException;

    public long getOrgTicketCountByStatus(int status) throws SQLException;

    public long getOrgTicketCountByStatusNotIn(int[] status) throws SQLException;

    public long getOrgTicketCountByStatusIn(int[] status) throws SQLException;

    public double getLeadClosedTimeHours(int status, long assignid) throws SQLException;

    public JSONArray getLeadCloseOverTime(long username) throws Exception;

    public JSONArray getLeadLostOverTime(long username) throws Exception;

    public double getLeadCount(int status, long assignid) throws SQLException;

    //gajeee
    public long getTableDataCount(Ticket parameters, String username) throws HibernateException, SQLException, Exception;

    public long getIdlingTableDataCount(Ticket parameters, String username) throws HibernateException, SQLException, Exception;

    public List<Ticket> getTableData(Ticket parameters, String username, int minCount, int start) throws HibernateException, SQLException, Exception;

    public List<Object> getIdlingTableData(Ticket parameters, String username, int minCount, int start) throws HibernateException, SQLException, Exception;

    public long getOpenTicketCount(String username) throws HibernateException, SQLException;

    public long getInProgressTicketCount(String username) throws HibernateException, SQLException;

    public long getIdlingTicketCount(String username) throws HibernateException, SQLException;

    public long getOrgInProgressTicketCount(String username) throws HibernateException, SQLException;

}
