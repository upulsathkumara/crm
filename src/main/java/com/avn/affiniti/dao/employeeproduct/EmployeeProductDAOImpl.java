/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.employeeproduct;

import com.avn.affiniti.hibernate.model.Employeeproduct;
import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : EmployeeProductDAOImpl
 * @Created on : Jul 20, 2016, 12:28:03 AM
 */
@Repository("employeeProductDAO")
public class EmployeeProductDAOImpl implements EmployeeProductDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

//    @Override
//    public void insertEmployeeProduct(User user) throws SQLException {
//        String query = "INSERT INTO employeeproduct "
//                + "(EMPLOYEEID, "
//                + "PRODUCTID, "
//                + "CREATEDATETIME, "
//                + "LASTUPDATEDDATETIME, "
//                + "CREATEDUSER) "
//                + "VALUES "
//                + "(?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
//        for (int product : user.getProducts()) {
//            jdbcTemplate.update(query,
//                    user.getEmployeeid(),
//                    product,
//                    user.getCreateduser());
//        }
//    }
    @Override
    public void insertEmployeeProduct(Employeeproduct employeeproduct) throws HibernateException {
        sessionFactory.getCurrentSession().save(employeeproduct);
    }

//    @Override
//    public void deleteEmployeeProductByEmployee(User user) throws SQLException {
//        String query = "DELETE FROM employeeproduct WHERE EMPLOYEEID = ?";
//        new JdbcTemplate(dataSource).update(query, new Object[]{user.getEmployeeid()});
//    }
    @Override
    public void deleteEmployeeProductByEmployee(int employeeId) throws HibernateException {
        try {
            String query = "DELETE Employeeproduct EP WHERE EP.id.employeeid =:employeeId";
            sessionFactory.getCurrentSession().createQuery(query).setParameter("employeeId", employeeId).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

}
