/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.employeeproduct;

import com.avn.affiniti.hibernate.model.Employeeproduct;
import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : EmployeeProductDAO
 * @Created on : Jul 20, 2016, 12:27:44 AM
 */
public interface EmployeeProductDAO {

//    public void insertEmployeeProduct(User user) throws SQLException;
    public void insertEmployeeProduct(Employeeproduct employeeproduct) throws HibernateException;

//    public void deleteEmployeeProductByEmployee(User user) throws SQLException;
    public void deleteEmployeeProductByEmployee(int employeeId) throws HibernateException;

}
