/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.remindernotification;

//import com.avn.affiniti.model.remindernotification.ReminderNotification;
import com.avn.affiniti.hibernate.model.Remindernotification;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ISURU
 */
@Repository("ReminderNotificationDAO")
public class ReminderNotificationDAOImpl implements ReminderNotificationDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

//    public List<ReminderNotification> getReminderById(String name) throws Exception {
//        List<ReminderNotification> reminderList = null;
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        try {
//            String sql = "SELECT ID,SOURCETABLEID,DESCRIPTION,REMINDERSTATUS,TO_CHAR (REMINDERTIME, 'DD-MON-YYYY HH24:MI:SS')REMINDERTIME,SECTIONID,VIEWEDSTATUS  FROM remindernotification WHERE VIEWEDSTATUS=? AND REMINDERUSER=?";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
//            statement.setString(2, name);
//            resultSet = statement.executeQuery();
//            reminderList = new ArrayList<>();
//            ReminderNotification reminder;
//            while (resultSet.next()) {
//                reminder = new ReminderNotification();
//                reminder.setId(resultSet.getString("ID"));
//                reminder.setSourcetabelId(resultSet.getString("SOURCETABLEID"));
//                reminder.setDescription(resultSet.getString("DESCRIPTION"));
//                reminder.setRemindStatus(resultSet.getString("REMINDERSTATUS"));
//                reminder.setRemindTime(resultSet.getString("REMINDERTIME"));
//                reminder.setViewStatus(resultSet.getString("VIEWEDSTATUS"));
//                reminder.setSection(resultSet.getString("SECTIONID"));
//                reminderList.add(reminder);
//
//            }
//        } catch (SQLException | NumberFormatException exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return reminderList;
//    }
    @Override
    public int getReminderCount(String name, Date d1, Date d2) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        try {
            String sql = "SELECT COUNT(*) CNT "
                    + "FROM remindernotification "
                    + "WHERE (REMINDERSTATUS = ? "
                    + "  AND REMINDERUSER = ? "
                    + "  AND REMINDERTIME BETWEEN ? AND ?) "
                    + "  OR (REMINDERTIME < ? "
                    + "  AND REMINDERSTATUS = ? "
                    + "  AND VIEWEDSTATUS <> ? "
                    + "  AND REMINDERUSER = ?)";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            statement.setString(2, name);
            statement.setTimestamp(3, new Timestamp(Common.getStartingTimeofDay(d1).getTime()));
            statement.setTimestamp(4, Common.getTimestampFromDateTime24(d2));
            statement.setTimestamp(5, Common.getTimestampFromDateTime24(d2));
            statement.setInt(6, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            statement.setInt(7, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            statement.setString(8, name);
//            System.out.println(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt("CNT");
            }
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return count;
    }

    @Override
    public boolean setRemindStatus(String level, String id) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        boolean status = false;
        try {
            connection = dataSource.getConnection();
            String sql = "UPDATE remindernotification SET REMINDERSTATUS=? WHERE ID=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, level);
            statement.setString(2, id);
            statement.executeUpdate();
            status = true;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return status;
    }

    @Override
    public boolean setViewStatus(String level, String id, String sourcetableid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        boolean status = false;
        try {
            connection = dataSource.getConnection();
            String sql = "UPDATE remindernotification SET VIEWEDSTATUS=? WHERE SOURCETABLEID=? AND SOURCETABLEID=? ";
            statement = connection.prepareStatement(sql);
            statement.setString(1, level);
            statement.setString(2, id);
            statement.setString(3, id);
            statement.executeUpdate();
            status = true;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return status;
    }

    @Override
    public boolean setDeleteStatus(String level, String id, String id2) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        boolean status = false;
        try {
            connection = dataSource.getConnection();
            String sql = "UPDATE remindernotification SET VIEWEDSTATUS=? WHERE SOURCETABLEID=? AND SECTIONID=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, level);
            statement.setString(2, id);
            statement.setString(3, id2);
            statement.executeUpdate();
            status = true;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return status;
    }

    @Override
    public List<Remindernotification> getTableData(int minCount, int start, String username, Date d1, Date d2) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Remindernotification> reminderList = null;
        try {
            /**
             * **************************** MsSQL *****************************
             */
            /*String sql = "SELECT  * "
             + "FROM    (SELECT ROW_NUMBER() OVER ( ORDER BY REMINDERTIME DESC) AS RowNum, ID, "
             + "       SOURCETABLEID, "
             + "       DESCRIPTION, "
             + "       REMINDERSTATUS, "
             + "       CONVERT(VARCHAR(19), REMINDERTIME, 100) AS REMINDERTIME, "
             + "       SECTIONID, "
             + "       VIEWEDSTATUS "
             + "FROM remindernotification "
             + "WHERE (REMINDERSTATUS = ? "
             + "  AND REMINDERUSER = ? "
             + "  AND REMINDERTIME < ?) "
             + "  OR (REMINDERTIME < ? "
             + "  AND REMINDERUSER = ? "
             + "  AND VIEWEDSTATUS <> ?) "
             + ") AS TB "
             + "WHERE   RowNum > ? "
             + "    AND RowNum <= ? ";*/

            /**
             * **************************** MySQL *****************************
             */
            String sql = "SELECT ID, "
                    + "          SOURCETABLEID, "
                    + "          DESCRIPTION, "
                    + "          REMINDERSTATUS, "
                    + "          REMINDERTIME AS FORMATTED_REMINDERTIME, "
                    + "          SECTIONID, "
                    + "          VIEWEDSTATUS,"
                    + "          REMINDERTIME "
                    + "   FROM remindernotification "
                    + "   WHERE (REMINDERSTATUS = ? AND REMINDERUSER = ? AND REMINDERTIME < ?) "
                    + "         OR (REMINDERTIME < ? AND REMINDERUSER = ? AND VIEWEDSTATUS <> ?) "
                    + "   ORDER BY REMINDERTIME DESC "
                    + "   LIMIT ?,?";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            statement.setString(2, username);
//            statement.setTimestamp(3, new Timestamp(Common.getStartingTimeofDay(d1).getTime()));
            statement.setTimestamp(3, Common.getTimestampFromDateTime24(d2));
            statement.setTimestamp(4, Common.getTimestampFromDateTime24(d2));
            statement.setString(5, username);
            statement.setInt(6, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_DELETE);
//            statement.setInt(7, MasterDataVarList.CCL_CODE_REMINDER_NOTIFICATION_DELETE);
//            statement.setString(8, username);
            statement.setInt(7, start);
            statement.setInt(8, minCount);
            resultSet = statement.executeQuery();

            reminderList = new ArrayList<>();
            while (resultSet.next()) {
                Remindernotification reminder;
                reminder = new Remindernotification();
                reminder.setId(resultSet.getInt("ID"));
                reminder.setSourcetableid(resultSet.getInt("SOURCETABLEID"));
                reminder.setDescription(resultSet.getString("DESCRIPTION"));
                reminder.setReminderstatus(resultSet.getInt("REMINDERSTATUS"));
                reminder.setRemindertime(resultSet.getTimestamp("FORMATTED_REMINDERTIME"));

                // reminder.setRemindertime(resultSet.getString("FORMATTED_REMINDERTIME"));
                reminder.setViewedstatus(resultSet.getInt("VIEWEDSTATUS"));
                reminder.setSectionid(resultSet.getInt("SECTIONID"));
//                reminder.setId(resultSet.getString("ID"));
//                reminder.setSourcetabelId(resultSet.getString("SOURCETABLEID"));
//                reminder.setDescription(resultSet.getString("DESCRIPTION"));
//                reminder.setRemindStatus(resultSet.getString("REMINDERSTATUS"));
//                reminder.setRemindTime(resultSet.getString("FORMATTED_REMINDERTIME"));
//                reminder.setViewStatus(resultSet.getString("VIEWEDSTATUS"));
//                reminder.setSection(resultSet.getString("SECTIONID"));
                reminderList.add(reminder);
            }
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return reminderList;

    }

    @Override
    public int getAllCount(String name, Date d1, Date d2) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        try {
            String sql = "SELECT COUNT(*) CNT "
                    + "FROM remindernotification "
                    + "WHERE (REMINDERSTATUS = ? "
                    + "  AND REMINDERUSER = ? "
                    + "  AND REMINDERTIME < ?) "
                    + "  OR (REMINDERTIME < ? "
                    + "  AND REMINDERUSER = ? "
                    + "  AND VIEWEDSTATUS <> ?) " /*+ "  OR (VIEWEDSTATUS <> ? "
                     + "  AND REMINDERUSER = ?)"*/;
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_DELETE);
            statement.setString(2, name);
//            statement.setTimestamp(3, new Timestamp(Common.getStartingTimeofDay(d1).getTime()));
            statement.setTimestamp(3, Common.getTimestampFromDateTime24(d2));
            statement.setTimestamp(4, Common.getTimestampFromDateTime24(d2));
            statement.setString(5, name);
            statement.setInt(6, MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_DELETE);
//            statement.setInt(7, MasterDataVarList.CCL_CODE_REMINDER_NOTIFICATION_DELETE);
//            statement.setString(8, name);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt("CNT");
            }
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception exception) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
        return count;
    }

    @Override
    public String loadReminderNEW(HttpServletRequest request, HttpServletResponse response) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
