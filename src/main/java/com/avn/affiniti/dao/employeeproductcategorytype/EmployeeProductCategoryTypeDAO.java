/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.employeeproductcategorytype;

import com.avn.affiniti.model.user.User;
import java.sql.SQLException;

/**
 * @Author : Roshen Dilshan
 * @Document : EmployeeProductCategoryTypeDAO
 * @Created on : Jul 26, 2016, 9:01:50 AM
 */
public interface EmployeeProductCategoryTypeDAO {
    
    public void insertEmployeeProductCategoryType(User user) throws SQLException;
    
    public void deleteEmployeeProductByEmployee(User user) throws SQLException;

}
