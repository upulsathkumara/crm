/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.employeeproductcategorytype;

import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : EmployeeProductCategoryTypeDAOImpl
 * @Created on : Jul 26, 2016, 9:02:23 AM
 */
@Repository("employeeProductCategoryTypeDAO")
public class EmployeeProductCategoryTypeDAOImpl implements EmployeeProductCategoryTypeDAO {

    @Autowired
    private DataSource dataSource;

    @Override
    public void insertEmployeeProductCategoryType(User user) throws SQLException {
        String selectquery = "SELECT DISTINCT (PCS.PRODUCTID) AS PRODUCTS "
                + "  FROM productcategory PC "
                + "       INNER JOIN productcategories PCS "
                + "          ON PC.PRODUCTCATEGORYID = PCS.PRODUCTCATEGORYID "
                + " WHERE PC.PRODUCTCATEGORYTYPEID IN (%s)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<Integer> qulifiedProductList = jdbcTemplate.queryForList(String.format(selectquery, (Arrays.toString(user.getProductscategorytype())).substring(1, (Arrays.toString(user.getProductscategorytype())).length() - 1)), Integer.class);
        String insertquery = "INSERT INTO employeeproductcatgorytype(EMPLOYEEID, "
                + "                                       PRDUCTID, "
                + "                                       PRODUCTCATEGORYTYPEID, "
                + "                                       CREATEDATETIME, "
                + "                                       LASTUPDATEDDATETIME, "
                + "                                       CREATEDUSER) "
                + "VALUES (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
        for (int productid : user.getProducts()) {
            for (int qulifiedProductId : qulifiedProductList) {
                if (productid == qulifiedProductId) {
                    String sql = "SELECT PCS.PRODUCTCATEGORYTYPEID FROM productcategories PC INNER JOIN productcategory PCS ON PC.PRODUCTCATEGORYID=PCS.PRODUCTCATEGORYID WHERE PC.PRODUCTID=%d and PCS.PRODUCTCATEGORYTYPEID IS NOT NULL";
                    List<Integer> productcategoryidList = jdbcTemplate.queryForList(String.format(sql, productid), Integer.class);
                    //product category
                    for (int productCategoryType : user.getProductscategorytype()) {
                        for (int productcategoryID : productcategoryidList) {
                            if (productcategoryID == productCategoryType) {
                                String productcatsql = "SELECT PRODUCTCATEGORYID FROM productcategory WHERE PRODUCTCATEGORYTYPEID=%d";
                                List<Integer> productcategoryList = jdbcTemplate.queryForList(String.format(productcatsql, productCategoryType), Integer.class);
                                for (int productcatid : productcategoryList) {
                                    jdbcTemplate.update(insertquery,
                                            user.getEmployeeid(),
                                            productid,
                                            productcatid,
                                            user.getCreateduser());
                                }
                                break;
                            }
                        }

                    }

                    break;
                }
            }
        }
    }

    @Override
    public void deleteEmployeeProductByEmployee(User user) throws SQLException {
        String query = "DELETE FROM employeeproductcatgorytype WHERE EMPLOYEEID = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{user.getEmployeeid()});
    }

}
