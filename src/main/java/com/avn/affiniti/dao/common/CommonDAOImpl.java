package com.avn.affiniti.dao.common;

import com.avn.affiniti.util.varlist.CommonVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

/**
 *
 * @Author : Roshen Dilshan
 * @Document : CommonDAOImpl
 * @Date : Oct 8, 2015, 1:41:45 PM
 */
@Repository("commonDAO")
public class CommonDAOImpl implements CommonDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Date getCurentTimesStamp() throws SQLException {
        return new JdbcTemplate(dataSource).queryForObject("SELECT CURRENT_TIMESTAMP AS CURRENTDATETIME FROM DUAL", Date.class);
    }

//    @Override
//    public JSONArray getAssignedTaskList(int userroleid) throws Exception {
//        JSONArray objList = new JSONArray();
//        try {
//            String sql = "SELECT USRT.TASKID, "
//                    + "       USRSUS.SECTIONID, "
//                    + "       USRSUS.SUBSECTIONID, "
//                    + "       USRSUS.USERROLEID "
//                    + "FROM userroletask USRT "
//                    + "INNER JOIN userrolesubsection USRSUS ON USRSUS.USERROLESUBSECTIONID = USRT.USERROLESUBSECTIONID "
//                    + "INNER JOIN userrole UR ON USRSUS.USERROLEID = UR.USERROLEID "
//                    + "WHERE UR.USERROLEID = ?";
//            while (resultSet.next()) {
//                JSONObject obj = new JSONObject();
//                obj.put("taskid", resultSet.getString("TASKID"));
//                obj.put("sectionid", resultSet.getString("SECTIONID"));
//                obj.put("subsectionid", resultSet.getString("SUBSECTIONID"));
//                obj.put("userroleid", resultSet.getString("USERROLEID"));
//
//                objList.put(obj);
//            }
//        } catch (SQLException | JSONException ex) {
//            throw ex;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return objList;
//    }
    @Override
    public Map<String, String> getDropdownValueList(String query) throws SQLException, DataAccessException {
        return new JdbcTemplate(dataSource).query(query, new ResultSetExtractor<Map>() {
            @Override
            public Map extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                Map<String, String> list = new LinkedHashMap<>();
                list.put("", "-- Select --");
                while (resultSet.next()) {
                    list.put(resultSet.getString("ID"), resultSet.getString("DESCRIPTION"));
                }
                return list;
            }
        });
    }

    @Override
    public JSONArray getJSONArrayDropdownValueList(String query) throws SQLException {
        return new JdbcTemplate(dataSource).query(query, new ResultSetExtractor<JSONArray>() {
            @Override
            public JSONArray extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                JSONArray array = new JSONArray();
                JSONObject object = new JSONObject();
                object.put("id", "");
                object.put("value", "-- Select --");
                array.put(object);
                while (resultSet.next()) {
                    object = new JSONObject();
                    object.put("id", resultSet.getString("ID"));
                    object.put("value", resultSet.getString("DESCRIPTION"));
                    array.put(object);
                }
                return array;
            }
        });
    }

    @Override
    public boolean getUserRolePermission(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query;
        if (userRolePermissionType == 1) {
            String hql = "SELECT COUNT(*) "
                    + "FROM Userrolesubsection URS "
                    + "INNER JOIN Userroletask URT "
                    + "ON URT.userrolesubsection.userrolesubsectionid = URS.userrolesubsectionid "
                    + "WHERE URS.userrole.userroleid = :userroleid AND URT.task.taskid = :taskid AND URS.subsection.subsectionid = :subsectionid";

            query = session.createQuery(hql);
            query.setInteger("userroleid", userRoleId);
            query.setInteger("subsectionid", subSectionId);
            query.setInteger("taskid", taskId);

        } else {
            String hql = "SELECT COUNT(*) "
                    + "FROM Userrolesection URS "
                    + "INNER JOIN Userrolesectiontask URT "
                    + "ON URT.userrolesection.userrolesectionid = URS.userrolesectionid "
                    + "WHERE URS.userrole.userroleid = :userroleid AND URS.section.sectionid = :sectionid AND URT.id.taskid = :taskid ";

            query = session.createQuery(hql);
            query.setInteger("userroleid", userRoleId);
            query.setInteger("sectionid", subSectionId);
            query.setInteger("taskid", taskId);
        }

        return ((long) query.uniqueResult()) > 0;
    }

    // get task list by userroleid and subsectionid or section id
    @Override
    public List<Integer> getTaskListByUserRoleIdSubSectionId(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query = null;

        String hql = "SELECT URT.task.taskid  "
                + "FROM Userrolesubsection URS "
                + "INNER JOIN Userroletask URT "
                + "ON URT.userrolesubsection.userrolesubsectionid = URS.userrolesubsectionid "
                + "WHERE URS.userrole.userroleid = :userroleid AND URS.subsection.subsectionid = :subsectionid";

        query = session.createQuery(hql);
        query.setInteger("userroleid", userRoleId);
        query.setInteger("subsectionid", subSectionId);

        return query.list();
    }

    @Override
    public List<Integer> getTaskListByUserRoleIdAndSectionId(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query = null;

        String hql = "SELECT URT.task.taskid "
                + "FROM Userrolesection URS "
                + "INNER JOIN Userrolesectiontask URT "
                + "ON URT.userrolesection.userrolesectionid = URS.userrolesectionid "
                + "WHERE URS.userrole.userroleid = :userroleid AND URS.section.sectionid = :sectionid ";

        query = session.createQuery(hql);
        query.setInteger("userroleid", userRoleId);
        query.setInteger("sectionid", subSectionId);

        return query.list();
    }

    @Override
    public boolean checkSectionAndSubSectionStatus(int sectionOrSubSectionId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query;
        int atviceStatus = CommonVarList.AFFINTI_ISINITIAL_STATUS;
        String hql;
        if (userRolePermissionType == 1) {
            hql = "SELECT COUNT(*) "
                    + "FROM Subsection SUB "
                    + "WHERE SUB.subsectionid = :sectionOrSubSectionId "
                    + "AND SUB.status.statusid = :statusid ";

        } else {
            hql = "SELECT COUNT(*) "
                    + "FROM Section S "
                    + "WHERE S.sectionid = :sectionOrSubSectionId "
                    + "AND S.status.statusid = :statusid ";

        }

        query = session.createQuery(hql);
        query.setInteger("statusid", atviceStatus);
        query.setInteger("sectionOrSubSectionId", sectionOrSubSectionId);

        return ((long) query.uniqueResult()) > 0;
    }

    @Override
    public String getSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        String subSectionId = "0";

        String hqlCount = "SELECT count(*) "
                + "FROM Subsectiontask SUBSECTASK "
                + "WHERE SUBSECTASK.url = :url ";

        String hql = "SELECT SUBSECTASK.id.subsection AS SUBSECID "
                + "FROM Subsectiontask SUBSECTASK "
                + "WHERE SUBSECTASK.url = :url ";

//        String hql2 = "SELECT SUBSECTASK.id.task AS TASKID "
//                + "FROM Subsectiontask SUBSECTASK "
//                + "WHERE SUBSECTASK.url = :url ";
        Query query = session.createQuery(hql);
        // Query query2 = session.createQuery(hql2);
        Query query3 = session.createQuery(hqlCount);

        query.setString("url", URL);
        //  query2.setString("url", URL);
        query3.setString("url", URL);

        long countLines = (long) query3.uniqueResult();
        if (countLines != 0) {
            subSectionId = String.valueOf((int) query.uniqueResult());
           // String taskId = String.valueOf((int) query2.uniqueResult());

            // recivedIds = subSectionId + ":" + taskId;
        }

        return subSectionId;
    }

    @Override
    public String getSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        String sectionId = "0";

        String hqlCount = "SELECT count(*) "
                + "FROM Sectiontask "
                + "WHERE url = :url ";

        String hql = "SELECT SECTASK.id.sectionid AS SECID "
                + "FROM Sectiontask SECTASK "
                + "WHERE SECTASK.url = :url ";

//        String hql2 = "SELECT SECTASK.id.taskid AS TASKID "
//                + "FROM Sectiontask SECTASK "
//                + "WHERE SECTASK.url = :url ";
        Query query = session.createQuery(hql);
        //  Query query2 = session.createQuery(hql2);
        Query query3 = session.createQuery(hqlCount);

        query.setString("url", URL);
        //  query2.setString("url", URL);
        query3.setString("url", URL);

        long countLines = (long) query3.uniqueResult();
        if (countLines != 0) {
            sectionId = String.valueOf((int) query.uniqueResult());
            // String taskId = String.valueOf((int) query2.uniqueResult());

//            recivedIds = sectionId + ":" + taskId;
        }

        return sectionId;
    }

    @Override
    public String getTaskIdAndSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        String recivedIds = "0:0";

        String hqlCount = "SELECT count(*) "
                + "FROM Sectiontask "
                + "WHERE url = :url ";

        String hql = "SELECT SECTASK.id.sectionid AS SECID "
                + "FROM Sectiontask SECTASK "
                + "WHERE SECTASK.url = :url ";

        String hql2 = "SELECT SECTASK.id.taskid AS TASKID "
                + "FROM Sectiontask SECTASK "
                + "WHERE SECTASK.url = :url ";

        Query query = session.createQuery(hql);
        Query query2 = session.createQuery(hql2);
        Query query3 = session.createQuery(hqlCount);

        query.setString("url", URL);
        query2.setString("url", URL);
        query3.setString("url", URL);

        long countLines = (long) query3.uniqueResult();
        if (countLines != 0) {
            String sectionId = String.valueOf((int) query.uniqueResult());
            String taskId = String.valueOf((int) query2.uniqueResult());

            recivedIds = sectionId + ":" + taskId;

        }

        return recivedIds;
    }

    @Override
    public String getTaskIdSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        Session session = sessionFactory.getCurrentSession();
        String recivedIds = "0:0";

        String hqlCount = "SELECT count(*) "
                + "FROM Subsectiontask SUBSECTASK "
                + "WHERE SUBSECTASK.url = :url ";

        String hql = "SELECT SUBSECTASK.id.subsection AS SUBSECID "
                + "FROM Subsectiontask SUBSECTASK "
                + "WHERE SUBSECTASK.url = :url ";

        String hql2 = "SELECT SUBSECTASK.id.task AS TASKID "
                + "FROM Subsectiontask SUBSECTASK "
                + "WHERE SUBSECTASK.url = :url ";

        Query query = session.createQuery(hql);
        Query query2 = session.createQuery(hql2);
        Query query3 = session.createQuery(hqlCount);

        query.setString("url", URL);
        query2.setString("url", URL);
        query3.setString("url", URL);

        long countLines = (long) query3.uniqueResult();
        if (countLines != 0) {
            String subSectionId = String.valueOf((int) query.uniqueResult());
            String taskId = String.valueOf((int) query2.uniqueResult());

            recivedIds = subSectionId + ":" + taskId;

        }

        return recivedIds;
    }

}
