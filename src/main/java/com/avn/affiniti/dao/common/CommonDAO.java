package com.avn.affiniti.dao.common;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.springframework.dao.DataAccessException;

/**
 * @Author : Roshen Dilshan
 * @Document : CommonDAO
 * @Created on : Oct 8, 2015, 1:41:36 PM
 */
public interface CommonDAO {

    public Date getCurentTimesStamp() throws SQLException;

    public Map<String, String> getDropdownValueList(String query) throws SQLException, DataAccessException;

    public JSONArray getJSONArrayDropdownValueList(String query) throws SQLException;

    public boolean getUserRolePermission(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception;
    
    public List<Integer> getTaskListByUserRoleIdAndSectionId(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception;
    
    public List<Integer> getTaskListByUserRoleIdSubSectionId(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception;

    public boolean checkSectionAndSubSectionStatus(int sectionOrSubSectionId, int userRolePermissionType) throws HibernateException, SQLException, Exception;

    public String getSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;

    public String getSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;

    public String getTaskIdAndSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;
    
    public String getTaskIdSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;
    
    

}
