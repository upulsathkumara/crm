/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketassignment;

import com.avn.affiniti.hibernate.model.Ticketassignment;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketAssignmentDAO
 * @Created on : Jun 22, 2017, 5:42:11 PM
 */
public interface TicketAssignmentDAO {
    
    public int createOrUpdateTicketAssignment(Ticketassignment ticketassignment) throws HibernateException;

    public Ticketassignment getTicketAssignmentByTktCatUsrrol(int ticketcategory, int userrole) throws HibernateException;

}
