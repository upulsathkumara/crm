/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketassignment;

import com.avn.affiniti.hibernate.model.Ticketassignment;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketAssignmentDAOImpl
 * @Created on : Jun 22, 2017, 5:43:08 PM
 */
@Repository("ticketAssignmentDAO")
public class TicketAssignmentDAOImpl implements TicketAssignmentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int createOrUpdateTicketAssignment(Ticketassignment ticketassignment) throws HibernateException {
        sessionFactory.getCurrentSession().saveOrUpdate(ticketassignment);
        return ticketassignment.getId();
    }

    @Override
    public Ticketassignment getTicketAssignmentByTktCatUsrrol(int ticketcategory, int userrole) throws HibernateException {
        return (Ticketassignment) sessionFactory.getCurrentSession().createQuery(" FROM Ticketassignment TA "
                + "WHERE TA.ticketcategory.ticketcategoryid = :ticketcategory "
                + "AND TA.userrole.userroleid = :userrole")
                .setInteger("ticketcategory", ticketcategory)
                .setInteger("userrole", userrole)
                .uniqueResult();
    }

}
