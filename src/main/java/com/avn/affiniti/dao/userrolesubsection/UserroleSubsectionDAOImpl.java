/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrolesubsection;

/**
 *
 * @author chandima
 */
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.model.dataobject.SubsectionDataObject;
import com.avn.affiniti.model.tempuserrolesubsection.TempUserrolesubsection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository("userroleSubsectionDAO")
public class UserroleSubsectionDAOImpl implements UserroleSubsectionDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Userrolesubsection> getUserrolesubsectionList(int max) throws HibernateException {
        List<Userrolesubsection> userrolesubsections = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Userrolesubsection u ORDER BY u.userrolesubsectionid DESC");
        //  query.setFirstResult(0);
        // query.setMaxResults(max);
        userrolesubsections = query.list();
        //   session.close();
        return userrolesubsections;
    }

    @Override
    public List<Userrolesection> getSectionList(int id) throws HibernateException {
        List<Userrolesection> sections = null;
        //Session session = sessionFactory.openSession();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Userrolesection WHERE userroleid = " + id);
        sections = query.list();
        //  session.close();
        return sections;

    }

    @Override
    public List<Subsection> getSubSectionList(SubsectionDataObject subsectionDataObject) throws HibernateException {
        List<Subsection> subsections = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Subsection WHERE sectionid = " + subsectionDataObject.getSectionid());
        subsections = query.list();
        //  session.close();
        return subsections;
    }

    @Override
    public long getTableDataCount() throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM userrolesubsection";
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        return count;
    }

    @Override
    public List<JSONObject> getTableData(int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT "
                + " COALESCE(SS.SUBSECTIONID, 'N/A') AS USERROLESUBSECTIONID, "
                + " COALESCE(UR.DESCRIPTION, 'N/A') AS USERROLE, "
                + " COALESCE(UR.USERROLEID, 'N/A') AS USERROLEID, "
                + " COALESCE(S.DESCRIPTION, 'N/A') AS SECTION, "
                + " COALESCE(S.SECTIONID, 'N/A') AS SECTIONID, "
                + " COALESCE(SS.DESCRIPTION, 'N/A') AS SUBSECTION, "
                + " COALESCE(US.CREATEDDATETIME, 'N/A') AS CREATEDDATE, "
                + " COALESCE(US.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATE, "
                + " COALESCE(US.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + " FROM userrolesubsection US INNER JOIN section S ON US.SECTIONID = S.SECTIONID "
                + " INNER JOIN subsection SS ON US.SUBSECTIONID = SS.SUBSECTIONID "
                + " INNER JOIN userrole UR ON US.USERROLEID = UR.USERROLEID "
                + " WHERE  1 = 1 "
                + " ORDER BY US.LASTUPDATEDDATETIME DESC"
                + " LIMIT ?,?";

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new UserroleSubSectionTableViewRowMapper());
    }

    public class UserroleSubSectionTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a value1='" + rs.getInt("SECTIONID") + "' value2='" + rs.getInt("USERROLEID") + "'  value='view' id='" + rs.getInt("USERROLESUBSECTIONID") + "'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a value1='" + rs.getInt("SECTIONID") + "' value2='" + rs.getInt("USERROLEID") + "'  value='edit' id='" + rs.getInt("USERROLESUBSECTIONID") + "'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("userroleDes", rs.getString("USERROLE"));
            object.put("sectionDes", rs.getString("SECTION"));
            object.put("subsectionDes", rs.getString("SUBSECTION"));
            object.put("lastupdateddatetime", rs.getString("LASTUPDATEDDATE"));
            object.put("createduser", rs.getString("CREATEDUSER"));
            return object;
        }
    }

    @Override
    public List<Userrolesubsection> getAssignedSubSectionList(SubsectionDataObject subsectionDataObject) throws HibernateException {
        List<Userrolesubsection> userrolesubsections = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Userrolesubsection WHERE userroleid = " + subsectionDataObject.getUserrolid()
                + "AND sectionid = " + subsectionDataObject.getSectionid());
        userrolesubsections = query.list();
        //session.close();
        return userrolesubsections;
    }

    @Override
    public List<Userrole> getUserrols() throws HibernateException, Exception {
        List<Userrole> userrole = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Userrole");
        userrole = query.list();

        return userrole;
    }

    @Override
    public List<Userrolesubsection> getUserrolesubsectionListById(int id) {
        return null;
    }

    @Override
    public List<Userrolesubsection> getUserrolesubsectionListByDescription(String description) {
        return null;
    }

    @Override
    public boolean addUserrolesubsection(Userrolesubsection userrolesubsection) throws HibernateException, Exception {
        boolean result = true;
        String description = "Saved Userrolesubsection Object with the fields : "
                + "Section : "
                + userrolesubsection.getSection().getDescription() + " SubSection :  "
                + userrolesubsection.getSubsection().getDescription() + " Userrole : "
                + userrolesubsection.getUserrole().getDescription() + " ";

        sessionFactory.getCurrentSession().saveOrUpdate(userrolesubsection);
        return result;
    }

    @Override
    public boolean updateUserrolesubsection(int sectionid, int userrolid) throws HibernateException, Exception {
        String hqlToDelete = "DELETE Userrolesubsection "
                + "WHERE userroleid = " + userrolid
                + " AND sectionid = " + sectionid;
        sessionFactory.getCurrentSession().createQuery(hqlToDelete).executeUpdate();
        return true;
    }

    @Override
    public boolean deleteUserrolesubsectionsById(int id) throws HibernateException, Exception {
        String hqlToDelete = "DELETE Userrolesubsection "
                + "WHERE userrolesubsectionid = " + id;
        sessionFactory.getCurrentSession().createQuery(hqlToDelete).executeUpdate();
        return true;
    }

    @Override
    public void deleteUserroleTasks(List<Userrolesubsection> userrolesubsections) throws HibernateException, SQLException, Exception {
        for (Userrolesubsection userrolesubsection : userrolesubsections) {
            if (userrolesubsection != null) {
                String hqlToDelete = "DELETE Userroletask "
                        + "WHERE userrolesubsectionid = " + userrolesubsection.getUserrolesubsectionid();
                sessionFactory.getCurrentSession().createQuery(hqlToDelete).executeUpdate();
            }
        }
    }

    @Override
    public List<Userrolesubsection> getRemovingUserrolesubsectionsByUserroleAndSection(List<TempUserrolesubsection> removingUserrolesubsections) throws HibernateException {
        List<Userrolesubsection> userrolesubsections = new ArrayList<>();

        Session session = sessionFactory.getCurrentSession();
        for (TempUserrolesubsection removingUserrolesubsection : removingUserrolesubsections) {
            Userrolesubsection userrolesubsection = new Userrolesubsection();
            int userroleid = removingUserrolesubsection.getUserroleid();
            int sectionid = removingUserrolesubsection.getSectionid();
            int subsectionid = removingUserrolesubsection.getSubsectionid();
            Query query = session.createQuery("FROM Userrolesubsection WHERE userroleid = " + userroleid
                    + "AND sectionid = " + sectionid + "AND subsectionid = " + subsectionid);
            userrolesubsection = (Userrolesubsection) query.uniqueResult();
            userrolesubsections.add(userrolesubsection);
        }

        // session.close();
        return userrolesubsections;
    }

    @Override
    public List<Userrolesubsection> getUserrolesubsectionsByUserroleAndSection(int userrole, int section) throws HibernateException {
        List<Userrolesubsection> userrolesubsections = null;
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Userrolesubsection WHERE userroleid = " + userrole
                + "AND sectionid = " + section);
        userrolesubsections = query.list();
        //  session.close();
        return userrolesubsections;
    }

    @Override
    public Userrolesubsection getUserroleSubsection(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Userrolesubsection userrolesubsection = session.load(Userrolesubsection.class, id);
        Hibernate.initialize(userrolesubsection);
        return userrolesubsection;

    }

    @Override
    public Task getTask(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Task task = session.load(Task.class, id);
        Hibernate.initialize(task);
        return task;

    }

}
