/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrolesubsection;

/**
 *
 * @author chandima
 */
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.model.dataobject.SubsectionDataObject;
import com.avn.affiniti.model.tempuserrolesubsection.TempUserrolesubsection;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

public interface UserroleSubsectionDAO {

    public List<Userrolesubsection> getUserrolesubsectionList(int max) throws HibernateException;

    public List<Userrolesubsection> getUserrolesubsectionListById(int id);

    public List<Userrolesubsection> getUserrolesubsectionListByDescription(String description);

    public boolean addUserrolesubsection(Userrolesubsection userrolesubsection) throws HibernateException, Exception;

    public List<Userrolesection> getSectionList(int id) throws HibernateException, Exception;

    public List<Subsection> getSubSectionList(SubsectionDataObject subsectionDataObject) throws HibernateException;

    public List<Userrole> getUserrols() throws HibernateException, Exception;

    public List<Userrolesubsection> getAssignedSubSectionList(SubsectionDataObject subsectionDataObject) throws HibernateException;

    public boolean updateUserrolesubsection(int sectionid, int userrolid) throws HibernateException, Exception;

    public long getTableDataCount() throws SQLException;

    public List<JSONObject> getTableData(int minCount, int start) throws HibernateException, SQLException, Exception;

    public void deleteUserroleTasks(List<Userrolesubsection> userrolesubsections) throws HibernateException, SQLException, Exception;

    public List<Userrolesubsection> getUserrolesubsectionsByUserroleAndSection(int userrole, int section) throws HibernateException;

    public List<Userrolesubsection> getRemovingUserrolesubsectionsByUserroleAndSection(List<TempUserrolesubsection> removingUserrolesubsections) throws HibernateException;

    public boolean deleteUserrolesubsectionsById(int id) throws HibernateException, Exception;
    
    public Userrolesubsection getUserroleSubsection(int id) throws HibernateException;
    
     public Task getTask(int id) throws HibernateException;
}
