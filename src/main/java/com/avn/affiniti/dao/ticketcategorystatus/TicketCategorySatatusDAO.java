/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketcategorystatus;

import com.avn.affiniti.hibernate.model.Ticketcategorystatus;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketCategorySatatusDAO
 * @Created on : Jun 14, 2017, 9:44:33 AM
 */
public interface TicketCategorySatatusDAO {

    public Ticketcategorystatus getTicketCategoryStatusByTktTktcategory(int ticketid, int ticketcategory) throws HibernateException;

}
