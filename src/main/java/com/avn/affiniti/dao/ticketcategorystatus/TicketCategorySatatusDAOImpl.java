/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketcategorystatus;

import com.avn.affiniti.hibernate.model.Ticketcategorystatus;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketCategorySatatusDAOImpl
 * @Created on : Jun 14, 2017, 9:44:59 AM
 */
@Repository("ticketCategorySatatusDAO")
public class TicketCategorySatatusDAOImpl implements TicketCategorySatatusDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Ticketcategorystatus getTicketCategoryStatusByTktTktcategory(int ticketid, int ticketcategory) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Ticketcategorystatus TCS "
                + "WHERE TCS.status.statusid = ( "
                + "SELECT T.status.statusid "
                + "FROM Ticket T "
                + "WHERE T.ticketid = :ticketid) "
                + "AND TCS.ticketcategory.ticketcategoryid = :ticketcategory");
        query.setInteger("ticketid", ticketid);
        query.setInteger("ticketcategory", ticketcategory);
        return (Ticketcategorystatus) query.uniqueResult();
    }

}
