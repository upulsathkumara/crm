/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.contact;

import com.avn.affiniti.hibernate.model.Contact;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : ContactDAO
 * @Created on : Jun 12, 2017, 8:19:01 AM
 */
public interface ContactDAO {

    public int insertContact(Contact contact) throws HibernateException;

    public Contact getContactById(int id) throws HibernateException;

    public void updateContact(Contact contact) throws HibernateException;

}
