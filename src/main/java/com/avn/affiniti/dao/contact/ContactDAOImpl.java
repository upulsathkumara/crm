/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.contact;

import com.avn.affiniti.hibernate.model.Contact;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : ContactDAOImpl
 * @Created on : Jun 12, 2017, 8:19:21 AM
 */
@Repository("contactDAO")
public class ContactDAOImpl implements ContactDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int insertContact(Contact contact) throws HibernateException {
        return (Integer) sessionFactory.getCurrentSession().save(contact);
    }

    @Override
    public Contact getContactById(int id) throws HibernateException {
        return (Contact) sessionFactory.getCurrentSession().get(Contact.class, id);
    }

    @Override
    public void updateContact(Contact contact) throws HibernateException {
        sessionFactory.getCurrentSession().update(contact);
    }

}
