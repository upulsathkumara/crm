/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.client;

import com.avn.affiniti.hibernate.model.Client;
import com.avn.affiniti.hibernate.model.Clientcategory;
import com.avn.affiniti.hibernate.model.Clientclientcategory;
import java.util.List;
import org.hibernate.HibernateException;

/**
 * @Author Nadun Chamikara
 * @Document ClientDAO
 * @Created on 18/08/2017, 12:04:56 PM
 */
public interface ClientDAO {

//    public void createOrUpdateClient(Client client) throws HibernateException, Exception;
    public void create(Client client) throws HibernateException, Exception;

    public void update(Client client) throws HibernateException, Exception;

    public long getActiveClientsCount(String name, String organizationId, String clientCategoryId, int location) throws HibernateException, Exception;

    public List<Client> getActiveClients(String name, String organizationId, int location, int start, int minCount) throws HibernateException, Exception;

    public Client getClientById(int clientId) throws HibernateException, Exception;

    public void deleteClient(Client client) throws HibernateException, Exception;
    
    public Clientcategory getClientcategorybyId(int id) throws HibernateException;
}
