/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.client;

import com.avn.affiniti.hibernate.model.Client;
import com.avn.affiniti.hibernate.model.Clientcategory;
import com.avn.affiniti.hibernate.model.Clientclientcategory;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Nadun Chamikara
 * @Document ClientDAOImpl
 * @Created on 18/08/2017, 12:07:28 PM
 */
@Repository("clientDAO")
public class ClientDAOImpl implements ClientDAO {

    @Autowired
    private SessionFactory sessionFactory;

//    @Override
//    public void createOrUpdateClient(Client client) throws HibernateException, Exception {
//        sessionFactory.getCurrentSession().saveOrUpdate(client);
//    }
    @Override
    public void create(Client client) throws HibernateException, Exception {
        sessionFactory.getCurrentSession().persist(client);
    }

    @Override
    public void update(Client client) throws HibernateException, Exception {
        sessionFactory.getCurrentSession().merge(client);
    }

    @Override
    public long getActiveClientsCount(String name, String organizationId, String clientCategoryId, int location) throws HibernateException, Exception {
        String hql = "SELECT COUNT(DISTINCT C.clientid) "
                + "FROM Client C, Clientclientcategory CT "
                + "WHERE C.clientid = CT.client "
                + "AND C.location.locationid = " + location + " "
                + "AND C.status =" + MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE;
        name = name.trim();
        if (!name.equals("")) {
            hql = hql.concat(" AND C.name LIKE '%" + name + "%'");
        }
        if (!organizationId.equals("") && !clientCategoryId.equals("")) {
            int oid = Integer.parseInt(String.valueOf(organizationId));
            int cid = Integer.parseInt(String.valueOf(clientCategoryId));
            hql = hql.concat(" AND C.organization.organizationid = " + oid + " AND CT.clientcategory.clientcategoryid = " + cid);
        } else if (!organizationId.equals("")) {
            int oid = Integer.parseInt(String.valueOf(organizationId));
            hql = hql.concat(" AND C.organization.organizationid = " + oid);
        } else if (!clientCategoryId.equals("")) {
            int cid = Integer.parseInt(String.valueOf(clientCategoryId));
            hql = hql.concat(" AND CT.clientcategory.clientcategoryid = " + cid);
        }

        return (long) sessionFactory.getCurrentSession().createQuery(hql).uniqueResult();
    }

    @Override
    public List<Client> getActiveClients(String name, String organizationId, int location, int start, int minCount) throws HibernateException, Exception {
//        String hql = "FROM Client WHERE status.statusid =" + MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE;
//        Query query;
//        // hql = "FROM Client WHERE ORGANIZATION = :organizationId AND CLIENTCATEGORY = :clientCategoryId";
//        if (!name.equals("")) {
//            hql = hql.concat(" AND NAME LIKE '%" + name + "%'");
//        }
//        if (!organizationId.equals("") && !clientCategoryId.equals("")) {
//            int oid = Integer.parseInt(String.valueOf(organizationId));
//            int cid = Integer.parseInt(String.valueOf(clientCategoryId));
//            hql = hql.concat(" AND organization.organizationid = " + oid + " AND clientcategory.clientcategoryid = " + cid);
//        } else if (!organizationId.equals("")) {
//            int oid = Integer.parseInt(String.valueOf(organizationId));
//            hql = hql.concat(" AND organization.organizationid = " + oid);
//        } else if (!clientCategoryId.equals("")) {
//            int cid = Integer.parseInt(String.valueOf(clientCategoryId));
//            hql = hql.concat(" AND clientcategory.clientcategoryid = " + cid);
//        }
//        hql = hql.concat(" ORDER BY createdatetime DESC");
//        query = sessionFactory.getCurrentSession().createQuery(hql).setFirstResult(start);
//        if (minCount != 0) {
//            query.setMaxResults(minCount);
//        }
//        return query.list();

        String hql = "FROM Client WHERE location.locationid = " + location + " "
                + "AND status.statusid =" + MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE;
        Query query;
        if (!name.equals("")) {
            hql = hql.concat(" AND name LIKE '%" + name + "%'");
        }
        if (!organizationId.equals("")) {
            int oid = Integer.parseInt(String.valueOf(organizationId));
            hql = hql.concat(" AND organization.organizationid = " + oid);
        }
        hql = hql.concat(" ORDER BY createdatetime DESC");
        query = sessionFactory.getCurrentSession().createQuery(hql).setFirstResult(start);
        if (minCount != 0) {
            query.setMaxResults(minCount);
        }
        return query.list();

    }

    @Override
    public Client getClientById(int clientId) throws HibernateException, Exception {
        String hql = "FROM Client WHERE clientid=" + clientId;
        List<Client> temp = sessionFactory.getCurrentSession().createQuery(hql).list();
        return temp.get(0);
    }

    @Override
    public void deleteClient(Client client) throws HibernateException, Exception {
        sessionFactory.getCurrentSession().update(client);
    }

    @Override
    public Clientcategory getClientcategorybyId(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Clientcategory clientcategory = session.load(Clientcategory.class, id);
        Hibernate.initialize(clientcategory);
        return clientcategory;
    }

}
