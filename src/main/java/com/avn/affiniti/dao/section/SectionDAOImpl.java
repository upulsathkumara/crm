package com.avn.affiniti.dao.section;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Isuru
 */
@Repository("sectionDAO")
public class SectionDAOImpl implements SectionDAO {
    
    @Autowired
    private DataSource dataSource;
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public boolean insertOrUpdateSection(Section section, String action) throws HibernateException, SQLException, Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            if (action.equals(MasterDataVarList.AFFINITI_CODE_CREATE)) {
                session.persist(section);
            } else if (action.equals(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
                session.merge(section);
            }
        } catch (HibernateException ex) {
            status = false;
            Loggers.LOGGER_FILE.error(ex);
            throw ex;            
        }
        return status;
    }
    
    @Override
    public Section getSectionBySectionId(String sectionId) throws HibernateException, SQLException, Exception {
        
        Session session = sessionFactory.getCurrentSession();
        Section section = session.get(Section.class, Integer.parseInt(sectionId));
        return section;
    }
    
    @Override
    public List<JSONObject> getTableData(Section parameters, int minCount, int start) throws HibernateException, SQLException, Exception {
        
        String query = "SELECT COALESCE(S.SECTIONID, 'N/A') AS SECTIONID, "
                + "COALESCE(S.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + "COALESCE(S.SECTIONLEVEL, 'N/A') AS SECTIONLEVEL, "
                + "COALESCE((SELECT DESCRIPTION FROM section WHERE SECTIONID = S.PARENTSECTION), 'N/A') AS PARENTSECTION, "
                + "COALESCE(S.ICON, 'N/A') AS ICON, "
                + "COALESCE(S.URL, 'N/A') AS URL, "
                + "COALESCE((SELECT DESCRIPTION FROM status WHERE STATUSID = S.STATUS), 'N/A') AS STATUSDESCRIPTION "
                + "FROM section S "
                + "WHERE :where "
                + "ORDER BY LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";
        
        String where = this.getWhere(parameters);
        query = query.replace(":where", where);
        
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new SectionTableViewRowMapper());
        
    }
    
    @Override
    public long getTableDataCount(Section parameters) throws HibernateException, SQLException, Exception {
        
        String query = "SELECT COUNT(*) FROM section S WHERE :where";
        
        String where = this.getWhere(parameters);
        query = query.replace(":where", where);
        
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        
        return count;
    }
    
    private String getWhere(Section parameters) {
        
        String where;
        
        if (parameters.getSection().getSectionid() != null && !parameters.getSection().getDescription().equals("")) {
            where = "S.SECTIONID LIKE " + parameters.getSection().getSectionid() + " AND S.DESCRIPTION LIKE '%" + parameters.getSection().getDescription() + "%'";
        } else if (parameters.getSection().getSectionid() != null) {
            where = "S.SECTIONID LIKE " + parameters.getSection().getSectionid();
        } else if (!parameters.getSection().getDescription().equals("")) {
            where = "S.DESCRIPTION LIKE '%" + parameters.getSection().getDescription() + "%'";
        } else {
            where = "1 = 1";
        }
        return where;
    }
    
    @Override
    public List<com.avn.affiniti.model.section.Section> getSectionLevelZeroByUserrole(int userrole) throws HibernateException, SQLException, Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<com.avn.affiniti.model.section.Section> list = null;
        try {
            String sql = "SELECT SEC.SECTIONID, SEC.DESCRIPTION, SEC.SECTIONLEVEL, SEC.PARENTSECTION, SEC.ICON, SEC.ONLYPARENT, SEC.URL "
                    + "FROM section SEC "
                    + "INNER JOIN userrolesection USEC ON USEC.SECTIONID = SEC.SECTIONID "
                    + "WHERE USEC.USERROLEID = ? AND PARENTSECTION IS NULL "
                    + "ORDER BY SORTID";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userrole);
            resultSet = statement.executeQuery();
            list = new ArrayList<>();
            com.avn.affiniti.model.section.Section section;
            while (resultSet.next()) {
                section = new com.avn.affiniti.model.section.Section();
                section.setSectionid(resultSet.getString("SECTIONID"));
                section.setSectionDes(resultSet.getString("DESCRIPTION"));
                section.setSectionlevel(resultSet.getString("SECTIONLEVEL"));
                section.setParentsection(resultSet.getString("PARENTSECTION"));
                section.setIcon(resultSet.getString("ICON"));
                section.setOnlyparent(resultSet.getString("ONLYPARENT"));
                section.setUrl(resultSet.getString("URL"));
                list.add(section);
            }
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
        return list;
    }
    
    @Override
    public List<com.avn.affiniti.model.section.Section> getSectionLowLevelByUserrole(int userrole) throws HibernateException, SQLException, Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<com.avn.affiniti.model.section.Section> list = null;
        try {
            String sql = "SELECT SEC.SECTIONID, SEC.DESCRIPTION, SEC.SECTIONLEVEL, SEC.PARENTSECTION, SEC.ICON "
                    + "FROM section SEC "
                    + "INNER JOIN userrolesection USEC ON USEC.SECTIONID = SEC.SECTIONID "
                    + "WHERE USEC.USERROLEID = ?  AND PARENTSECTION IS NOT NULL "
                    + "ORDER BY SORTID";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userrole);
            resultSet = statement.executeQuery();
            list = new ArrayList<>();
            com.avn.affiniti.model.section.Section section;
            while (resultSet.next()) {
                section = new com.avn.affiniti.model.section.Section();
                section.setSectionid(resultSet.getString("SECTIONID"));
                section.setSectionDes(resultSet.getString("DESCRIPTION"));
                section.setSectionlevel(resultSet.getString("SECTIONLEVEL"));
                section.setParentsection(resultSet.getString("PARENTSECTION"));
                section.setIcon(resultSet.getString("ICON"));
                list.add(section);
            }
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
        return list;
    }
    
    public class SectionTableViewRowMapper implements RowMapper<JSONObject> {
        
        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();
            
            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("SECTIONID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("SECTIONID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";
            
            object.put("action", action);
            object.put("sectionId", rs.getInt("SECTIONID"));
            object.put("sectionDes", rs.getString("DESCRIPTION"));
            object.put("sectionlevel", rs.getInt("SECTIONLEVEL"));
            object.put("parentSection", rs.getString("PARENTSECTION"));            
            object.put("sectionIcon", rs.getString("ICON"));
            object.put("url", rs.getString("URL"));
            object.put("statusDes", rs.getString("STATUSDESCRIPTION"));
            
            return object;
        }
    }

//    @Override
//    public Audittrace setAudittrace(Section section, String task) throws Exception {
//
//        String description = "Section "+task+" "
//                + "Section Id : " + section.getSectionid() + ", "
//                + "Task Ids : ,";
//
//        Audittrace audittrace = new Audittrace();
//        audittrace.setDescription(description);
//        audittrace.setAfectedid(sectionTask.getId().getSectionid() + " , " + sectionTask.getId().getTaskid());
//        audittrace.setTask(task);
//        audittrace.setAffectedpage("Sectiontask");
//        audittrace.setCreatedatetime(new Date());
//        audittrace.setLastupdateddatetime(new Date());
//        audittrace.setCreateduser(sectionTask.getCreateduser());
//
//        return audittrace;
//    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
//    @Override
//    public List<Section> getParntSection(String sectionlevel) throws HibernateException, SQLException, Exception {
//        
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<Section> Parentlist = null;
//        try {
//            String sql = "SELECT SECTIONID, DESCRIPTION, SORTID FROM SECTION WHERE SECTIONLEVEL = ? ORDER BY SORTID ASC";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, (Integer.valueOf(sectionlevel)) - 1);
//            resultSet = statement.executeQuery();
//            Parentlist = new ArrayList<>();
//            Section section;
//            while (resultSet.next()) {
//                section = new Section();
//                section.setSectionid(resultSet.getString("SECTIONID"));
//                section.setSectionDes(resultSet.getString("DESCRIPTION"));
//                Parentlist.add(section);
//            }
//        } catch (SQLException | NumberFormatException e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }        
//        
//        List list = null;
//        return list;
//    }
//    @Override
//    public Section getSectionBySectionId(String sectionId) throws HibernateException, SQLException, Exception {
//            query = session.createQuery("SELECT SECTIONID,DESCRIPTION,SECTIONLEVEL,"
//                    + "(SELECT DESCRIPTION FROM Section WHERE SECTIONID = :parent ),"
//                    + "ICON,ONLYPARENT,URL,SORTID,(SELECT DESCRIPTION FROM Status WHERE STATUSID = :statusId) "
//                    + "FROM Section S WHERE SECTIONID = :sid");
//        int id = Integer.parseInt(sectionId);
//        Criteria cr = session.createCriteria(Section.class);
//        cr.add(Restrictions.eq("sectionid", id));
//
//        return (Section) cr.uniqueResult();
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        Section sectionData = null;
//        try {
//            String sql = " SELECT SE.SECTIONID,SE.DESCRIPTION AS SEDES, NVL(SE.PARENTSECTION, '0') AS PARENTSECTION, "
//                    + " SE.SECTIONLEVEL, SE.CREATEDUSER, SE.STATUS, ST.DESCRIPTION AS STDES, SE.SORTID, SE.ICON, SE.ONLYPARENT, SE.URL FROM SECTION SE "
//                    + " INNER JOIN STATUS ST ON SE.STATUS = ST.STATUSID WHERE SE.SECTIONID=?";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, Integer.valueOf(sectionId));
//            resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                sectionData = new Section();
//                sectionData.setSectionid(resultSet.getString("SECTIONID"));
//                sectionData.setSectionDes(resultSet.getString("SEDES"));
//                sectionData.setSectionlevel(resultSet.getString("SECTIONLEVEL"));
//                sectionData.setParentsection(resultSet.getString("PARENTSECTION"));
//                sectionData.setIcon(resultSet.getString("ICON"));
//                sectionData.setStatusid(resultSet.getString("STATUS"));
//                sectionData.setStatusDes(resultSet.getString("STDES"));
//                sectionData.setSortid(resultSet.getString("SORTID"));
//                if (resultSet.getInt("ONLYPARENT") == MasterDataVarList.AFFINITI_CODE_ONLY_PARENT_SECTION_TRUE) {
//                    sectionData.setOnlyparentsection(true);
//                } else {
//                    sectionData.setOnlyparentsection(false);
//                }
//                if (sectionData.getUrl() != null && !sectionData.getUrl().isEmpty()) {
//                    sectionData.setUrl(resultSet.getString("URL"));
//                } else {
//
//                }
//
//            }
//        } catch (SQLException | NumberFormatException e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        
//        return null;        
//    }
//    @Override
//    public List<Section> getTableData(Section parameters, int minCount, int start) throws HibernateException, SQLException, Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<Section> list = null;
//        try {
//            String sql = "SELECT "
//                    + "    * "
//                    + " FROM "
//                    + "    ("
//                    + "    SELECT "
//                    + "        TB.*, "
//                    + "         ROWNUM AS ROWNUMER  "
//                    + "    FROM "
//                    + "        ( SELECT SE.SECTIONID, SE.DESCRIPTION AS SEDES, SE.SECTIONLEVEL, NVL((SELECT DESCRIPTION FROM SECTION WHERE SECTIONID = SE.PARENTSECTION), '--') AS PARENTSECTION, "
//                    + " SE.CREATEDUSER, SE.STATUS, ST.DESCRIPTION AS STDES,SE.SORTID, NVL(SE.ICON,'--')ICON FROM SECTION SE INNER JOIN STATUS ST ON "
//                    + " SE.STATUS = ST.STATUSID :where ORDER BY SE.SECTIONID) TB  "
//                    + "    WHERE "
//                    + "        ROWNUM <= ?  "
//                    + "    ) "
//                    + " WHERE "
//                    + "    ROWNUMER > ? ";
//
//            sql = sql.replace(":where", this.getWhere(parameters));
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, start + minCount);
//            statement.setInt(2, start);
//            resultSet = statement.executeQuery();
//
//            list = new ArrayList<>();
//            Section sectionData;
//            while (resultSet.next()) {
//                sectionData = new Section();
//                sectionData.setSectionid(resultSet.getString("SECTIONID"));
//                sectionData.setSectionDes(resultSet.getString("SEDES"));
//                sectionData.setSectionlevel(resultSet.getString("SECTIONLEVEL"));
//                sectionData.setParentsection(resultSet.getString("PARENTSECTION"));
//                sectionData.setIcon(resultSet.getString("ICON"));
//                sectionData.setStatusid(resultSet.getString("STATUS"));
//                sectionData.setStatusDes(resultSet.getString("STDES"));
//                sectionData.setSortid(resultSet.getString("SORTID"));
//                list.add(sectionData);
//            }
//
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//    }
//    private String getWhere(Section parameters) {
//        String where = "";
//        if (parameters.getSearchoption() != null
//                && !parameters.getSearchoption().isEmpty()
//                && parameters.getSearchoption().equalsIgnoreCase("sectionid")
//                && parameters.getInput() != null
//                && !parameters.getInput().isEmpty()) {
//            where += "where SE.SECTIONID LIKE '%" + parameters.getInput().trim() + "%'";
//        }
//        if (parameters.getSearchoption() != null
//                && !parameters.getSearchoption().isEmpty()
//                && parameters.getSearchoption().equalsIgnoreCase("sectiondes")
//                && parameters.getInput() != null
//                && !parameters.getInput().isEmpty()) {
//            where += "where SE.DESCRIPTION LIKE '%" + parameters.getInput().trim() + "%'";
//        }
//        return where;
//    }
//    @Override
//    public int getTableDataCount(Section parameters) throws HibernateException, SQLException, Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        int count = 0;
//        try {
//            String sql = "SELECT COUNT(*) CNT FROM SECTION SE INNER JOIN STATUS ST ON SE.STATUS = ST.STATUSID :where ";
//            sql = sql.replace(":where", this.getWhere(parameters));
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                count = resultSet.getInt("CNT");
//            }
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//    }
//    @Override
//    public Map<String, String> getSectionDropdownList() throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        Map<String, String> list = null;
//        try {
//            String sql = "SELECT SECTIONID, DESCRIPTION, SORTID FROM SECTION WHERE STATUS = 3 ORDER BY SORTID ASC";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            resultSet = statement.executeQuery();
//            list = new LinkedHashMap<>();
//            list.put("", "-- Select --");
//            while (resultSet.next()) {
//                list.put(resultSet.getString("SECTIONID"), resultSet.getString("DESCRIPTION"));
//            }
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return list;
//    }
//    @Override
//    public Map<String, String> getMultiSectionDropdownList() throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        Map<String, String> list = null;
//        try {
//            String sql = "SELECT SECTIONID, DESCRIPTION, SORTID FROM SECTION WHERE STATUS = 3 ORDER BY SORTID ASC";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            resultSet = statement.executeQuery();
//            list = new LinkedHashMap<>();
//            while (resultSet.next()) {
//                list.put(resultSet.getString("SECTIONID"), resultSet.getString("DESCRIPTION"));
//            }
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return list;
//    }
//    @Override
//    public List<Section> getSectionDropdownListByRoleID(String roleid) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<Section> SectionList = null;
//        try {
//            String sql = "SELECT S.SECTIONID,S.DESCRIPTION, RS.USERROLEID, SORTID FROM SECTION S INNER JOIN USERROLESECTION RS ON S.SECTIONID=RS.SECTIONID WHERE STATUS=3 AND RS.USERROLEID=? ORDER BY SORTID ASC";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, (Integer.valueOf(roleid)));
//            resultSet = statement.executeQuery();
//            SectionList = new ArrayList<>();
//            Section section;
//            while (resultSet.next()) {
//                section = new Section();
//                section.setSectionid(resultSet.getString("SECTIONID"));
//                section.setSectionDes(resultSet.getString("DESCRIPTION"));
//                SectionList.add(section);
//            }
//        } catch (SQLException | NumberFormatException e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return SectionList;
//    }
//    @Override
//    public List<Section> getSectionByUserrole(int userrole) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<Section> list = null;
//        try {
//            String sql = "SELECT SEC.SECTIONID, SEC.DESCRIPTION, SEC.SECTIONLEVEL, SEC.PARENTSECTION, SEC.ICON "
//                    + "FROM SECTION SEC "
//                    + "INNER JOIN USERROLESECTION USEC ON USEC.SECTIONID = SEC.SECTIONID "
//                    + "WHERE USEC.USERROLEID = ?";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, userrole);
//            resultSet = statement.executeQuery();
//            list = new ArrayList<>();
//            Section section;
//            while (resultSet.next()) {
//                section = new Section();
//                section.setSectionid(resultSet.getString("SECTIONID"));
//                section.setSectionDes(resultSet.getString("DESCRIPTION"));
//                section.setSectionlevel(resultSet.getString("SECTIONLEVEL"));
//                section.setParentsection(resultSet.getString("PARENTSECTION"));
//                section.setIcon(resultSet.getString("ICON"));
//                list.add(section);
//            }
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return list;
//    }
//
}
