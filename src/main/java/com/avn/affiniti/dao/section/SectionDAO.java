package com.avn.affiniti.dao.section;

import com.avn.affiniti.hibernate.model.Section;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author Isuru
 */
public interface SectionDAO {

    public boolean insertOrUpdateSection(Section section, String action) throws HibernateException, SQLException, Exception;  

    public Section getSectionBySectionId(String sectionId) throws HibernateException, SQLException, Exception;

    public List<JSONObject> getTableData(Section parameters, int minCount, int start) throws HibernateException, SQLException, Exception;

    public List<com.avn.affiniti.model.section.Section> getSectionLevelZeroByUserrole(int userrole) throws HibernateException, SQLException, Exception;

    public List<com.avn.affiniti.model.section.Section> getSectionLowLevelByUserrole(int userrole) throws HibernateException, SQLException, Exception;

    public long getTableDataCount(Section parameters) throws HibernateException, SQLException, Exception;
    
 
//    public Audittrace setAudittrace(Section section, String task) throws Exception;

//    public Map<String, String> getSectionDropdownList() throws Exception;
//
//    public Map<String, String> getMultiSectionDropdownList() throws Exception;
//
//    public List<Section> getSectionDropdownListByRoleID(String roleid) throws Exception;
//
//    public List<Section> getSectionByUserrole(int userrole) throws Exception;
}
