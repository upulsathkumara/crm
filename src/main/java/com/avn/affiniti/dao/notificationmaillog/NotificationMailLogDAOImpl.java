/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.notificationmaillog;

import com.avn.affiniti.hibernate.model.Notificationmaillog;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author Nadun Chamikara
 * @Document NotificationMailLogDAOImpl
 * @Created on 05/01/2018, 2:10:56 PM
 */
@Repository("notificationMailLogDAO")
public class NotificationMailLogDAOImpl implements NotificationMailLogDAO{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createNotificationMailLog(Notificationmaillog notificationmaillog) throws HibernateException {
        sessionFactory.getCurrentSession().save(notificationmaillog);
    }
    
    
}
