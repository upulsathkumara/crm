/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.notificationmaillog;

import com.avn.affiniti.hibernate.model.Notificationmaillog;
import org.hibernate.HibernateException;

/**
 * @Author Nadun Chamikara
 * @Document NotificationMailLogDAO
 * @Created on 05/01/2018, 2:10:38 PM
 */
public interface NotificationMailLogDAO {
    public void createNotificationMailLog(Notificationmaillog notificationmaillog) throws HibernateException;
}
