/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.user;

import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import java.util.List;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : UserDAO
 * @Created on : Sep 15, 2015, 1:40:24 PM
 */
public interface UserDAO {

    public int getTableDataCount(User user) throws SQLException;

    public List<JSONObject> getTableData(User user, int start, int minCount) throws SQLException;

    public boolean isUserExists(User user) throws SQLException;

    public long insertUser(User user) throws SQLException;

    public void insertUserAccount(User user) throws SQLException;

    public int getUserRoleByUsername(String username) throws SQLException;

    public User getUserByUsername(String username) throws SQLException;

    public User getUserByEmployeeId(long employeeid) throws SQLException;

    public void updateUser(User user) throws SQLException;

    public void updateUserAccount(User user) throws SQLException;

    public boolean isActivieUser(String username) throws SQLException;

    public boolean isUserExist(String username) throws SQLException;

    public boolean isValidUser(String username, String password) throws SQLException;

    public boolean isFirstLogin(String username) throws SQLException;

    public void chagePassword(String username, String password) throws SQLException;

    public void updateFirstLogin(String username, int firstlogin) throws SQLException;

    public boolean isNotaDuplicateEpf(String epf) throws SQLException;

    public boolean isNotaDuplicateNic(String nic) throws SQLException;

    public boolean isNotaDuplicateEmail(String email) throws SQLException;

    public void insertLanguagelanguageSkills(User user) throws SQLException;

    public void deleteLanguagelanguageSkills(long empid) throws SQLException;

    public int getHierarchyidIdByUsername(String username) throws SQLException;

}
