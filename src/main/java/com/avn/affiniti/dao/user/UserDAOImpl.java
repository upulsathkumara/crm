/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.user;

import com.avn.affiniti.model.user.User;
import com.avn.affiniti.model.user.UserRowMapper;
import com.avn.affiniti.model.user.UserTableViewRowMapper;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : UserDAOImpl
 * @Created on : Sep 15, 2015, 1:44:38 PM
 */
@Repository("userDAO")
public class UserDAOImpl implements UserDAO {

    @Autowired
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int getTableDataCount(User user) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT "
                + "FROM systemuser SU "
                + "INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE "
                + "INNER JOIN status S ON S.STATUSID = SU.STATUS "
                + "INNER JOIN employee E ON SU.EMPLOYEEID = E.EMPLOYEEID "
                + "WHERE 1 = 1 :where ";
        String where = "";
        if (user.getUsername()!= null && !user.getUsername().trim().isEmpty()) {
            where = "AND SU.USERNAME LIKE '%" + user.getUsername() + "%'"
                    + "OR LOWER(E.PREFERREDNAME) LIKE LOWER('%" + user.getUsername().trim() + "') "
                    + "OR UPPER(E.NIC) LIKE UPPER('%" + user.getUsername().trim() + "') "
                    + "OR E.CONTACTNO01 LIKE '%" + user.getUsername().trim() + "%'";
        }
        query = query.replace(":where", where);
        return new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
    }

    @Override
    public List<JSONObject> getTableData(User user, int start, int minCount) throws SQLException {
        String query = "SELECT  *   "
                + "FROM    (SELECT ROW_NUMBER() OVER ( ORDER BY SU.CREATEDDATETIME DESC ) AS RowNum, "
                + "E.EMPLOYEEID, "
                + "SU.USERNAME,   "
                + "ISNULL(UR.DESCRIPTION, '--') AS USERROLE,   "
                + "ISNULL(S.DESCRIPTION, '--') AS STATUS,   "
                + "ISNULL(CAST(SU.USERATTEMPTS AS VARCHAR), '0') AS USERATTEMPTS,   "
                + "ISNULL(E.PREFERREDNAME, '--') AS PREFERREDNAME,   "
                + "ISNULL(E.SURENAME, '--') AS SURENAME,   "
                + "ISNULL(CONVERT (VARCHAR(26),SU.CREATEDDATETIME,113 ), '--') AS CREATEDDATETIME,   "
                + "ISNULL(CONVERT (VARCHAR(26),SU.LASTUPDATEDDATETIME, 113), '--') AS LASTUPDATEDDATETIME   "
                + "FROM systemuser SU   "
                + "INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE   "
                + "INNER JOIN status S ON S.STATUSID = SU.STATUS   "
                + "INNER JOIN employee E ON SU.EMPLOYEEID = E.EMPLOYEEID   "
                + "WHERE 1 = 1  :where ) AS TB   "
                + "WHERE   RowNum > ?   "
                + "AND RowNum <= ?   "
                + "ORDER BY RowNum";
        String where = "";
        if (user.getUsername() != null && !user.getUsername().trim().isEmpty()) {
            where = "AND SU.USERNAME LIKE '%" + user.getUsername().trim() + "%'"
                    + "OR LOWER(E.PREFERREDNAME) LIKE LOWER('%" + user.getUsername().trim() + "') "
                    + "OR UPPER(E.NIC) LIKE UPPER('%" + user.getUsername().trim() + "') "
                    + "OR E.CONTACTNO01 LIKE '%" + user.getUsername().trim() + "%'";
        }
        query = query.replace(":where", where);
        return new JdbcTemplate(dataSource).query(query, new Object[]{minCount, start}, new UserTableViewRowMapper());
    }

    @Override
    public boolean isUserExists(User user) throws SQLException {
        boolean isExists = false;
        String query = "SELECT "
                + "    COUNT(*) AS CNT "
                + "FROM "
                + "    systemuser SU "
                + "WHERE "
                + "    SU.USERNAME = ?";
        int count = new JdbcTemplate(dataSource).queryForObject(query, new Object[]{user.getUsername()}, Integer.class);
        if (count > 0) {
            isExists = true;
        }
        return isExists;
    }

    @Override
    public long insertUser(final User user) throws SQLException {
        final String query = "INSERT INTO employee (NAMEINFULL, "
                + "INITIALS, "
                + "PREFERREDNAME, "
                + "SURENAME, "
                + "EMPLOYEECODE, "
                + "EPF, "
                + "NIC, "
                + "EMAIL, "
                + "CONTACTNO01, "
                + "CONTACTNO02, "
                + "HIERARCHYID, "
                + "STATUS, "
                + "CREATEDDATETIME, "
                + "LASTUPDATEDDATETIME, "
                + "CREATEDUSER) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        new JdbcTemplate(dataSource).update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement statement = connection.prepareStatement(query, new String[]{"EMPLOYEEID"});
                statement.setString(1, user.getNameinfull());
                statement.setString(2, user.getInitials());
                statement.setString(3, user.getPreferredname());
                statement.setString(4, user.getSurname());
                statement.setString(5, user.getUsercode());
                statement.setString(6, user.getEpf());
                statement.setString(7, user.getNic().toUpperCase());
                statement.setString(8, user.getEmail().toLowerCase());
                statement.setString(9, user.getContactno01());
                statement.setString(10, user.getContactno02());
                statement.setInt(11, user.getHierarchyid());
                statement.setInt(12, user.getStatus());
                statement.setString(13, user.getCreateduser());
                return statement;
            }
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void insertUserAccount(User user) throws SQLException {
        String query = "INSERT INTO systemuser "
                + "(USERNAME, "
                + "PASSWORD, "
                + "USERROLE, "
                + "EMPLOYEEID, "
                + "STATUS, "
                + "CREATEDDATETIME, "
                + "LASTUPDATEDDATETIME, "
                + "CREATEDUSER) "
                + "VALUES "
                + "(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
        new JdbcTemplate(dataSource).update(query,
                user.getUsername(),
                Common.passwordHashed(user.getPassword()),
                user.getUserrole(),
                user.getEmployeeid(),
                user.getStatus(),
                user.getCreateduser());
    }

    @Override
    public User getUserByUsername(String username) throws SQLException {
        String query = "SELECT EP.EMPLOYEEID, "
                + "       EP.NAMEINFULL, "
                + "       EP.INITIALS, "
                + "       EP.PREFERREDNAME, "
                + "       EP.SURENAME, "
                + "       EP.EPF, "
                + "       EP.NIC, "
                + "       EP.EMAIL, "
                + "       EP.CONTACTNO01, "
                + "       EP.CONTACTNO02, "
                + "       EP.HIERARCHYID, "
                + "       SU.USERNAME, "
                + "       SU.USERROLE, "
                + "       SU.STATUS "
                + "FROM systemuser SU "
                + "INNER JOIN employee EP ON EP.EMPLOYEEID = SU.USERID "
                + "WHERE USERNAME = ?";
        return (User) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, new UserRowMapper());
    }

    @Override
    public User getUserByEmployeeId(long employeeid) throws SQLException {
        String query = "SELECT EP.EMPLOYEEID, "
                + "       EP.NAMEINFULL, "
                + "       EP.INITIALS, "
                + "       EP.PREFERREDNAME, "
                + "       EP.SURENAME, "
                + "       EP.EPF, "
                + "       EP.NIC, "
                + "       EP.EMAIL, "
                + "       EP.CONTACTNO01, "
                + "       EP.CONTACTNO02, "
                + "       EP.HIERARCHYID, "
                + "       SU.USERNAME, "
                + "       SU.USERROLE, "
                + "       SU.STATUS "
                + "FROM employee EP "
                + "INNER JOIN systemuser SU ON EP.EMPLOYEEID = SU.USERID "
                + "WHERE EP.EMPLOYEEID = ?";
        return (User) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{employeeid}, new UserRowMapper());
    }

    @Override
    public int getUserRoleByUsername(String username) throws SQLException {
        String query = "SELECT "
                + "       USERROLE "
                + "FROM systemuser  "
                + "WHERE USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Integer.class);
    }

    @Override
    public void updateUser(User user) throws SQLException {
        String query = "UPDATE employee "
                + "SET "
                + "  NAMEINFULL = ?, "
                + "  INITIALS = ?, "
                + "  PREFERREDNAME = ?, "
                + "  SURENAME = ?, "
                + "  EMPLOYEECODE = ?, "
                + "  EPF = ?, "
                + "  NIC = ?, "
                + "  EMAIL = ?, "
                + "  CONTACTNO01 = ?, "
                + "  CONTACTNO02 = ?, "
                + "  HIERARCHYID = ?, "
                + "  STATUS = ?, "
                + "  LASTUPDATEDDATETIME = CURRENT_TIMESTAMP "
                + "WHERE EMPLOYEEID = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{
            user.getNameinfull(),
            user.getInitials(),
            user.getPreferredname(),
            user.getSurname(),
            user.getUsercode(),
            user.getEpf(),
            user.getNic().toUpperCase(),
            user.getEmail().toLowerCase(),
            user.getContactno01(),
            user.getContactno02(),
            user.getHierarchyid(),
            user.getStatus(),
            user.getEmployeeid()
        });
    }

    @Override
    public void updateUserAccount(User user) throws SQLException {
        String query = "UPDATE systemuser "
                + "SET "
                + "  USERNAME = ?, "
                + "  USERROLE = ?, "
                + "  STATUS = ?, "
                + "  LASTUPDATEDDATETIME = CURRENT_TIMESTAMP "
                + "WHERE EMPLOYEEID = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{
            user.getUsername(),
            user.getUserrole(),
            user.getStatus(),
            user.getEmployeeid()
        });
    }

    @Override
    public boolean isActivieUser(String username) throws SQLException {
        String query = "SELECT COUNT(*) FROM systemuser WHERE USERNAME = ? AND STATUS = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{
            username,
            MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        }, Integer.class) == 1;
    }

    @Override
    public boolean isUserExist(String username) throws SQLException {
        String query = "SELECT COUNT(*) FROM systemuser WHERE USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Integer.class) == 1;
    }

    @Override
    public boolean isValidUser(String username, String password) throws SQLException {
        String query = "SELECT PASSWORD FROM systemuser WHERE USERNAME = ?";
        String pw = new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, String.class);
        return BCrypt.checkpw(password, pw);
    }

    @Override
    public boolean isFirstLogin(String username) throws SQLException {
        String query = "SELECT FIRSTLOGIN FROM systemuser WHERE USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Boolean.class);
    }

    @Override
    public void chagePassword(String username, String password) throws SQLException {
        String query = "UPDATE systemuser "
                + "SET "
                + "   PASSWORD = ?, "
                + "  LASTUPDATEDDATETIME = CURRENT_TIMESTAMP "
                + "WHERE USERNAME = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{Common.passwordHashed(password), username});
    }

    @Override
    public void updateFirstLogin(String username, int firstlogin) throws SQLException {
        String query = "UPDATE systemuser "
                + "SET "
                + "   FIRSTLOGIN = ?, "
                + "  LASTUPDATEDDATETIME = CURRENT_TIMESTAMP "
                + "WHERE USERNAME = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{firstlogin, username});
    }

    @Override
    public boolean isNotaDuplicateEpf(String epf) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM employee WHERE UPPER(EPF) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{epf.toUpperCase()}, Integer.class) == 0;
    }

    @Override
    public boolean isNotaDuplicateNic(String nic) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM employee WHERE UPPER(NIC) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{nic.toUpperCase()}, Integer.class) == 0;
    }

    @Override
    public boolean isNotaDuplicateEmail(String email) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM employee WHERE LOWER(EMAIL) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{email.toLowerCase()}, Integer.class) == 0;
    }

    @Override
    public void insertLanguagelanguageSkills(User user) throws SQLException {
        String query = "INSERT INTO employeelanguageskills (EMPID, LANGUAGEID, CREATEDDATETIME, LASTUPDATEDDATETIME, CREATEDUSER) "
                + "VALUES (?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        for (int languageID : user.getLanguageskills()) {
            jdbcTemplate.update(query,
                    user.getEmployeeid(),
                    languageID,
                    user.getCreateduser());
        }
    }

    @Override
    public void deleteLanguagelanguageSkills(long empid) throws SQLException {
        String query = "DELETE FROM employeelanguageskills WHERE EMPID=? ";
        new JdbcTemplate(dataSource).update(query, new Object[]{empid});
    }

    @Override
    public int getHierarchyidIdByUsername(String username) throws SQLException {
        String query = "SELECT HIERARCHYID FROM employee E INNER JOIN systemuser SU ON E.EMPLOYEEID = SU.USERID "
                + "WHERE SU.USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Integer.class);
    }
}
