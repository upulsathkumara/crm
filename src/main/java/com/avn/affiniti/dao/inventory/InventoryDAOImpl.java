/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.inventory;

import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.hibernate.model.Inventorycategory;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Inventoryproductcategory;
import com.avn.affiniti.hibernate.model.Inventoryterminalhistory;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.model.inventory.Inventory;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryDAOImpl
 * @Created on : Jun 2, 2017, 4:09:01 PM
 */
@Repository("inventoryDAO")
public class InventoryDAOImpl implements InventoryDAO {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private StatusDAO statusDAO;

    @Override
    public void insertTerminal(Inventoryterminalitem terminalitems, String selectFunction) throws HibernateException {
        try {
            //  sessionFactory.getCurrentSession().persist(terminalitems);
            Session session = sessionFactory.getCurrentSession();
            if (selectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
                session.persist(terminalitems);
            } else if (selectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
                session.merge(terminalitems);
            } else {
                // object.put("message", MessageVarList.ACCESS_DENIED);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  " + ex);
        }
    }

    @Override
    public void insertHardwareItem(Inventoryhardwareitem hardwareitem, String setSelectFunction) throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
            session.persist(hardwareitem);
        }

    }

    @Override
    public void insertInventoryterminalhistory(Inventoryterminalhistory inventoryterminalhistory, String setSelectFunction) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
            session.persist(inventoryterminalhistory);
        }
    }

    @Override
    public List<Inventory> getTerminalData() {
        return sessionFactory.getCurrentSession().createQuery("from inventoryterminalitem").list();
    }

//    @Override
//    public JSONArray getItemSearchedData(String categoryId, String contextPath, int start, int length) throws SQLException {
//        Session session = sessionFactory.getCurrentSession();
//
//        String ViewStatus = "";
//        String UpdateStatus = "";
//
//        List<Inventoryhardwareitem> searchedItemList = new ArrayList<>();
//
//        if (!categoryId.isEmpty()) {
//            String hql = "FROM Inventoryhardwareitem IHI "
//                    + "JOIN FETCH IHI.inventorycategory IC "
//                    + "WHERE IC.inventorycategoryid = :categoryId";
//
//            Query query = session.createQuery(hql);
//            query.setParameter("categoryId", Integer.parseInt(categoryId));
//            query.setFirstResult(start);
//            query.setMaxResults(length);
//
//            searchedItemList = query.list();
//        } else {
//            String hql = "FROM Inventoryhardwareitem";
//
//            Query query = session.createQuery(hql);
//            query.setFirstResult(start);
//            query.setMaxResults(length);
//
//            searchedItemList = query.list();
//        }
//
//        long iTotalRecords = 0; // total number of records (unfiltered)
//        long iTotalDisplayRecords = 0;
//
//        JSONArray rows = new JSONArray();
//
//        iTotalRecords = getItemTableCount(categoryId);
//        iTotalDisplayRecords = iTotalRecords;
//        if (iTotalRecords > 0) {
//            for (Inventoryhardwareitem data : searchedItemList) {
//                JSONObject object = new JSONObject();
//
//                if (data.getInventorycategory().getDescription().equals("Reconditioned")) {
//                    object.put("category", data.getInventorycategory().getDescription());
//                    object.put("partname", data.getPartname());
//                    object.put("partcode", data.getPartcode());
//                    object.put("posrevision", data.getPosrevision());
//                    object.put("pospartid", data.getPospartid());
//                    object.put("posmac", data.getPosmac());
//                    object.put("posptid", data.getPosptid());
//                    object.put("createduser", data.getCreateduser());
//                    object.put("createddate", data.getCreateddatetime());
//                    String action = "<div class=\"row\"> "
//                            + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory_hardware/view/item?itemId=" + data.getInventoryhardwareitemid() + "'class=" + ViewStatus + "  ><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
//                            + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory_hardware/edit/item?itemId=" + data.getInventoryhardwareitemid() + "' class=" + UpdateStatus + "><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Update\"></i></a></div>"
//                            + "</div>";
//                    object.put("action", action);
//                } else {
//                    object.put("category", data.getInventorycategory().getDescription());
//                    object.put("partname", data.getPartname());
//                    object.put("partcode", data.getPartcode());
//                    object.put("posrevision", CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE);
//                    object.put("pospartid", CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE);
//                    object.put("posmac", CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE);
//                    object.put("posptid", CommonVarList.EMPTY_OR_NULL_REPLACE_VALUE);
//                    object.put("createduser", data.getCreateduser());
//                    object.put("createddate", data.getCreateddatetime());
//                    String action = "<div class=\"row\"> "
//                            + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory_hardware/view/item?itemId=" + data.getInventoryhardwareitemid() + "'class=" + ViewStatus + "  ><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
//                            + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory_hardware/edit/item?itemId=" + data.getInventoryhardwareitemid() + "' class=" + UpdateStatus + "><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Update\"></i></a></div>"
//                            + "</div>";
//                    object.put("action", action);
//                }
//
//                rows.put(object);
//            }
////                        audit.insertAuditTrace("Case search page ", String.valueOf(MasterDataVarList.CCL_CODE_SEARCH), " Case search ", parameters.getInput(), (String) session.getAttribute("username"));
//        }
//
//        return rows;
//    }
    @Override
    public JSONArray getTerminalSearchedData(String user, String tModelId, String contextPath, int start, int length) throws SQLException {
        Session session = sessionFactory.getCurrentSession();

        String ViewStatus = "";
        String UpdateStatus = "";
        String AssignStatus = "";

        List<Inventoryterminalitem> searchedTerminalList = new ArrayList<>();

        if (!tModelId.isEmpty()) {
            String hql = "FROM Inventoryterminalitem ITI "
                    + "JOIN FETCH ITI.inventoryproductcategory IPC "
                    + "WHERE IPC.description LIKE :tModelId AND ITI.createduser = :user";

            Query query = session.createQuery(hql);
            query.setParameter("tModelId", "%" + tModelId + "%");
            query.setParameter("user", user);
            query.setFirstResult(start);
            query.setMaxResults(length);

            searchedTerminalList = query.list();
        } else {
            String hql = "FROM Inventoryterminalitem ITI "
                    + "WHERE ITI.createduser = :user";

            Query query = session.createQuery(hql);
            query.setParameter("user", user);
            query.setFirstResult(start);
            query.setMaxResults(length);

            searchedTerminalList = query.list();
        }

        long iTotalRecords = 0; // total number of records (unfiltered)
        long iTotalDisplayRecords = 0;

        JSONArray rows = new JSONArray();

        iTotalRecords = getTerminalTableCount(user, tModelId);
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            for (Inventoryterminalitem data : searchedTerminalList) {
                JSONObject object = new JSONObject();
                object.put("brand", data.getTerminalbrand().getDescription());
                object.put("model", data.getTerminalmodel().getDescription());
                object.put("serialNo", data.getSerialno());
                object.put("client", data.getClient().getName());
                object.put("category", data.getInventorycategory().getDescription());
                object.put("owner", data.getEmployee().getPreferredname());
                object.put("createddatetime", data.getCreateddatetime());
                object.put("createduser", data.getCreateduser());
                String action = "<div class=\"row\"> "
                        + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory/view/terminal?terminalId=" + data.getInventoryterminalitemid() + "'class=" + ViewStatus + "  ><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                        + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory/edit/terminal?terminalId=" + data.getInventoryterminalitemid() + "' class=" + UpdateStatus + "><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Update\"></i></a></div>"
                        + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory/assign/terminal?terminalId=" + data.getInventoryterminalitemid() + "' class=" + AssignStatus + "><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Assign\"></i></a></div>"
                        + "</div>";
                object.put("action", action);

                rows.put(object);
            }
//                        audit.insertAuditTrace("Case search page ", String.valueOf(MasterDataVarList.CCL_CODE_SEARCH), " Case search ", parameters.getInput(), (String) session.getAttribute("username"));
        }

        return rows;

    }

//    @Override
//    public Long getItemTableCount(String categoryId) throws SQLException {
//        Session session = sessionFactory.getCurrentSession();
//
//        List tablerecords = new ArrayList();
//
//        if (!categoryId.isEmpty()) {
//            String hql = "FROM Inventoryhardwareitem IHI "
//                    + "JOIN FETCH IHI.inventorycategory IC "
//                    + "WHERE IC.inventorycategoryid = :categoryId";
//            Query query = session.createQuery(hql);
//            query.setParameter("categoryId", Integer.parseInt(categoryId));
//
//            tablerecords = query.list();
//        } else {
//            String hql = "FROM Inventoryhardwareitem";
//            Query query = session.createQuery(hql);
//            tablerecords = query.list();
//        }
//
//        return (long) tablerecords.size();
//
//    }
    //////////////////
    /////////////////////////////////table count////////////////////////
    @Override
    public long getTableDataCount(Inventoryhardwareitem parameters) throws SQLException {

//        Session session = sessionFactory.getCurrentSession();
//        Query query;
//        if (parameters != null) {
//            query = session.createQuery("SELECT COUNT(P.productid) FROM Product P WHERE P.productcategory.productcategoryid =:prodID");
//            query.setInteger("prodID", parameters.getProductcategoryid());
//        } else {
//            query = session.createQuery("SELECT COUNT(*) FROM Product");
//        }
//
//        return (long) query.uniqueResult();
        String query = "SELECT COUNT(*) FROM inventoryhardwareitem I WHERE :where";
        query = query.replace(":where", this.getWhere(parameters));
        return new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

    }

    @Override
    public List<JSONObject> getTableData(Inventoryhardwareitem parameters, int minCount, int start, HttpServletRequest request) throws HibernateException, SQLException, Exception {

//        Session session = sessionFactory.getCurrentSession();
//        Query query;
//
//        if (parameters != null) {
//            System.out.println(parameters.getProductcategoryid());
//            query = session.createQuery("FROM Product P WHERE P.productcategory.productcategoryid =:prodID");
//            query.setParameter("prodID", parameters.getProductcategoryid());            
//        } else {
//
//            query = session.createQuery("FROM Product");
//        }
//
//        query.setMaxResults(minCount);
//        query.setFirstResult(start);
//
//        return query.list();
//    }
        String query = "SELECT "
                + "COALESCE(I.INVENTORYHARDWAREITEMID, 'N/A') AS INVENTORYHARDWAREITEMID, "
                // + "COALESCE(P.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + "COALESCE(IC.DESCRIPTION, 'N/A') AS INVENTORYCATEGORY, "
                // + "COALESCE(S.DESCRIPTION, 'N/A') AS STATUS, "
                + "COALESCE(I.PARTCODE, 'N/A') AS PARTCODE, "
                + "COALESCE(I.PARTNAME, 'N/A') AS PARTNAME, "
                + "COALESCE(I.LOCATION, 'N/A') AS LOCATION, "
                + "COALESCE(I.POSREVISION, 'N/A') AS POSREVISION, "
                + "COALESCE(I.POSPARTID, 'N/A') AS POSPARTID, "
                + "COALESCE(I.POSMAC, 'N/A') AS POSMAC, "
                + "COALESCE(I.POSPTID, 'N/A') AS POSPTID, "
                + "COALESCE(I.POSSERIALNUMBER, 'N/A') AS POSSERIALNUMBER, "
                + "COALESCE(I.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + "COALESCE(I.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(I.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM inventoryhardwareitem I "
                + "INNER JOIN inventorycategory IC ON  I.INVENTORYCATEGORY=IC.INVENTORYCATEGORYID "
                // + "INNER JOIN status S ON  P.STATUS=S.STATUSID "
                + "WHERE :where "
                + "ORDER BY LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhere(parameters));

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new HardwareTableViewRowMapper(request));
//        return new JdbcTemplate(dataSource).query(query, new StatusTableViewRowMapper());
    }

    public class HardwareTableViewRowMapper implements RowMapper<JSONObject> {

        private HttpServletRequest request;

        public HardwareTableViewRowMapper(HttpServletRequest request) {
            // System.out.println("AAAAAAAA : "+request);
            this.request = request;

        }

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/inventory_hardware/view/item?itemId=" + rs.getInt("INVENTORYHARDWAREITEMID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/inventory_hardware/edit/item?itemId=" + rs.getInt("INVENTORYHARDWAREITEMID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";
            object.put("action", action);
            object.put("inventoryhardwareitemid", rs.getInt("INVENTORYHARDWAREITEMID"));
            object.put("category", rs.getString("INVENTORYCATEGORY"));
            object.put("partcode", rs.getString("PARTCODE"));
            object.put("partname", rs.getString("PARTNAME"));
            object.put("location", rs.getString("LOCATION"));
            object.put("posrevision", rs.getString("POSREVISION"));
            object.put("pospartid", rs.getString("POSPARTID"));
            object.put("posmac", rs.getString("POSMAC"));
            object.put("posptid", rs.getString("POSPTID"));
            object.put("posserialnumber", rs.getString("POSSERIALNUMBER"));
            object.put("lastupdatedtime", rs.getString("LASTUPDATEDDATETIME"));
            object.put("createdtime", rs.getString("CREATEDDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }
    }

    private String getWhere(Inventoryhardwareitem parameters) {
        String where = "1 = 1 ";
        if (parameters != null) {
            if (parameters.getInventorycategory() != null && parameters.getInventorycategory().getInventorycategoryid() != 0) {
                where += "AND I.inventorycategory = " + parameters.getInventorycategory().getInventorycategoryid();
            }
            if (parameters.getPartname() != null && !parameters.getPartname().isEmpty()) {
                where += " AND I.partname LIKE '%" + parameters.getPartname() + "%'";
            }

        }
        System.out.println(where);
        return where;
    }

///////////////
    @Override
    public Long getTerminalTableCount(String user, String tModelId) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
//        String tModelId = request.getParameter("brandsearch");

        List tablerecords = new ArrayList();

        if (!tModelId.isEmpty()) {
            String hql = "FROM Inventoryterminalitem ITI "
                    + "JOIN FETCH ITI.inventoryproductcategory IPC "
                    + "WHERE IPC.description LIKE :tModelId AND ITI.createduser = :user";

            Query query = session.createQuery(hql);
            query.setParameter("tModelId", "%" + tModelId + "%");
            query.setParameter("user", user);
            tablerecords = query.list();
        } else {
            String hql = "FROM Inventoryterminalitem ITI "
                    + "WHERE ITI.createduser = :user";

            Query query = session.createQuery(hql);
            query.setParameter("user", user);
            tablerecords = query.list();
        }

        return (long) tablerecords.size();
    }

    @Override
    public Inventoryterminalitem getTerminalById(int id) throws SQLException { //List<Inventory>
        Session session = sessionFactory.getCurrentSession();

        Inventoryterminalitem inventoryTerminal = new Inventoryterminalitem();

        Inventoryproductcategory inventoryproductcategory = new Inventoryproductcategory();

        String hql = "FROM Inventoryterminalitem AS ITI "
                + "WHERE ITI.inventoryterminalitemid = :ITIid";

        Query query = session.createQuery(hql);
        query.setParameter("ITIid", id);

        inventoryTerminal = (Inventoryterminalitem) query.uniqueResult();

        return inventoryTerminal;
    }

    @Override
    public Inventoryhardwareitem getItemById(int id) throws SQLException {
        Session session = sessionFactory.getCurrentSession();

        Inventoryhardwareitem hardwareitem = new Inventoryhardwareitem();

        String hql = "FROM Inventoryhardwareitem AS IHI "
                + "WHERE IHI.inventoryhardwareitemid = :IHIid";

        Query query = session.createQuery(hql);
        query.setParameter("IHIid", id);

        hardwareitem = (Inventoryhardwareitem) query.uniqueResult();

        return hardwareitem;
    }

    @Override
    public void editTerminal(Inventoryterminalitem terminalitem) throws SQLException {
        Session session = sessionFactory.getCurrentSession();

        String hql = "update Inventoryterminalitem ITI"
                + " SET ITI.inventorycategory = :category, ITI.terminalbrand = :brand,"
                + " ITI.terminalmodel = :model, ITI.serialno = :serialno, ITI.client = :client,"
                + " ITI.lastupdateddatetime = :updateddate,"
                + " ITI.tid = :tid, ITI.mid = :mid"
                + " WHERE ITI.inventoryterminalitemid = :id";

        Query query = session.createQuery(hql);

        query.setParameter("category", terminalitem.getInventorycategory());
        query.setParameter("brand", terminalitem.getTerminalbrand());
        query.setParameter("model", terminalitem.getTerminalmodel());
        query.setParameter("serialno", terminalitem.getSerialno());
        query.setParameter("client", terminalitem.getClient());
        query.setParameter("updateddate", terminalitem.getLastupdateddatetime());
        query.setParameter("tid", terminalitem.getTid());
        query.setParameter("mid", terminalitem.getMid());
        query.setParameter("id", terminalitem.getInventoryterminalitemid());

        query.executeUpdate();

    }

    @Override
    public void editHardwareItem(Inventoryhardwareitem hardwareitem, String setSelectFunction) throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
            session.merge(hardwareitem);
        }
    }

    @Override
    public long getTerminalTableDataCount(Inventoryterminalitem parameters, int location) throws SQLException {

        String query = "SELECT COUNT(*) AS CNT FROM inventoryterminalitem IT WHERE :where";
        query = query.replace(":where", this.getWhere(parameters, location));
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    @Override
    public List<JSONObject> getTerminalTableData(Inventoryterminalitem parameters, int location, int minCount, int start, HttpServletRequest request) throws HibernateException, SQLException, Exception {

        String query = "SELECT COALESCE (IT.INVENTORYTERMINALITEMID, 'N/A') AS INVENTORYTERMINALITEMID,"
                + " COALESCE (TB.DESCRIPTION, 'N/A') AS TERMINALBRAND, "
                + " COALESCE(TM.DESCRIPTION, 'N/A') AS TERMINALMODEL, "
                + " COALESCE(IT.SERIALNO, 'N/A') AS SERIALNO, "
                + " COALESCE(CL.NAME, 'N/A') AS CLIENT, "
                + " COALESCE(CT.DESCRIPTION, 'N/A') AS INVENTORYCATEGORY, "
                + " COALESCE(EM.NAMEINFULL, 'N/A') AS OWNER, "
                + " COALESCE(IT.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + " COALESCE(IT.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + " FROM inventoryterminalitem IT LEFT OUTER JOIN terminalmodel TM ON IT.TERMINALMODEL = TM.TERMINALMODELID "
                + " LEFT OUTER JOIN terminalbrand TB ON IT.TERMINALBRAND = TB.TERMINALBRANDID "
                + " LEFT OUTER JOIN client CL ON IT.CLIENT = CL.CLIENTID "
                + " LEFT OUTER JOIN employee EM ON IT.OWNER = EM.EMPLOYEEID "
                + " LEFT OUTER JOIN inventorycategory CT ON IT.INVENTORYCATEGORY = CT.INVENTORYCATEGORYID "
                + " WHERE :where"
                + " ORDER BY IT.LASTUPDATEDDATETIME DESC  "
                + " LIMIT ?,? ";

        String where = this.getWhere(parameters, location);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new InventoryDAOImpl.InventoryterminalItemTableViewRowMapper(request));
    }

    public class InventoryterminalItemTableViewRowMapper implements RowMapper<JSONObject> {

        private HttpServletRequest request;

        public InventoryterminalItemTableViewRowMapper(HttpServletRequest request) {
            this.request = request;
        }

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String ViewStatus = "";
            String UpdateStatus = "";
            String AssignStatus = "";
            String contextPath = request.getContextPath();
            int data = rs.getInt("INVENTORYTERMINALITEMID");
            String action = "<div class=\"row\"> "
                    + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory/view/terminal?terminalId=" + data + "'class=" + ViewStatus + "  ><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory/edit/terminal?terminalId=" + data + "' class=" + UpdateStatus + "><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Update\"></i></a></div>"
                    //                    + "<div class=\"col-xs-3\"><a href='" + contextPath + "/inventory/assign/terminal?terminalId=" + data + "' class=" + AssignStatus + "><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Assign\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("brand", rs.getString("TERMINALBRAND"));
            object.put("model", rs.getString("TERMINALMODEL"));
            object.put("serialNo", rs.getString("SERIALNO"));
            object.put("client", rs.getString("CLIENT"));
            object.put("category", rs.getString("INVENTORYCATEGORY"));
            object.put("owner", rs.getString("OWNER"));
            object.put("createddatetime", rs.getString("CREATEDDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }

    }

    private String getWhere(Inventoryterminalitem parameters, int location) {
        String where = "IT.LOCATION = " + location + " ";
        if (parameters == null) {
            where = where + "AND 1 = 1 ";
        } else if (parameters.getTerminalbrand() != null && parameters.getClient() != null && parameters.getTerminalmodel() != null) {
            where = where + "AND IT.TERMINALBRAND = " + parameters.getTerminalbrand().getTerminalbrandid()
                    + " AND IT.CLIENT = " + parameters.getClient().getClientid()
                    + " AND IT.TERMINALMODEL = " + parameters.getTerminalmodel().getTerminalmodelid();
        } else if (parameters.getClient() != null && parameters.getTerminalbrand() != null) {
            where = where + "AND IT.CLIENT = " + parameters.getClient().getClientid()
                    + " AND IT.TERMINALBRAND = " + parameters.getTerminalbrand().getTerminalbrandid();
        } else if (parameters.getTerminalmodel() != null && parameters.getTerminalbrand() != null) {
            where = where + "AND IT.TERMINALMODEL = " + parameters.getTerminalmodel().getTerminalmodelid()
                    + " AND IT.TERMINALBRAND = " + parameters.getTerminalbrand().getTerminalbrandid();
        } else if (parameters.getTerminalmodel() != null && parameters.getClient() != null) {
            where = where + "AND IT.TERMINALMODEL = " + parameters.getTerminalmodel().getTerminalmodelid()
                    + " AND IT.CLIENT = " + parameters.getClient().getClientid();
        } else if (parameters.getTerminalbrand() != null) {
            where = where + "AND IT.TERMINALBRAND = " + parameters.getTerminalbrand().getTerminalbrandid();
        } else if (parameters.getTerminalmodel() != null) {
            where = where + "AND IT.TERMINALMODEL = " + parameters.getTerminalmodel().getTerminalmodelid();
        } else if (parameters.getClient() != null) {
            where = where + "AND IT.CLIENT = " + parameters.getClient().getClientid();
        }
        return where;
    }

    @Override
    public boolean addTerminaltoTMCInventory(Inventoryterminalitem inventoryterminalitem) throws HibernateException, SQLException {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(inventoryterminalitem);
        } catch (Exception e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public void changeStatusOfTerminalByStatus(Inventoryterminalitem inventoryterminalitem) throws SQLException, HibernateException {
        inventoryterminalitem = this.getTerminalById(inventoryterminalitem.getInventoryterminalitemid());
        inventoryterminalitem.setStatus(inventoryterminalitem.getStatus());
        Session session = sessionFactory.getCurrentSession();
        session.merge(inventoryterminalitem);
    }

    @Override
    public Boolean validateSerialNumber(String serialNumber) throws SQLException {

        String query = "SELECT COUNT(*) "
                + "FROM inventoryterminalitem I "
                + "WHERE I.SERIALNO = ? ";
        int count = new JdbcTemplate(dataSource).queryForObject(query, new Object[]{serialNumber}, Integer.class);

        if (count > 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Inventoryhardwareitem getInventoryCategoryById(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Inventoryhardwareitem inventoryhardwareitem = session.get(Inventoryhardwareitem.class, id);
        Hibernate.initialize(inventoryhardwareitem);
        return inventoryhardwareitem;
    }

    @Override
    public Inventorycategory getInventoryCategoryByCategoryId(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Inventorycategory inventorycategory = session.load(Inventorycategory.class, id);
        Hibernate.initialize(inventorycategory);
        return inventorycategory;
    }
    
    @Override
    public Inventoryterminalitem getInventoryitemBySerialNumber(String serialno) throws HibernateException{
        return (Inventoryterminalitem) sessionFactory.getCurrentSession().createQuery("FROM Inventoryterminalitem I "
                + "WHERE I.serialno = :serialno")
                .setString("serialno", serialno)
                .uniqueResult();
    }
}