/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.inventory;

import com.avn.affiniti.hibernate.model.Inventorycategory;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Inventoryterminalhistory;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.model.inventory.Inventory;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryDAO
 * @Created on : Jun 2, 2017, 4:08:37 PM
 */
public interface InventoryDAO {

    public void insertTerminal(Inventoryterminalitem terminalitems, String selectFunction) throws HibernateException;

    public void insertHardwareItem(Inventoryhardwareitem hardwareitem, String setSelectFunction) throws HibernateException, Exception;

    public void insertInventoryterminalhistory(Inventoryterminalhistory inventoryterminalhistory, String setSelectFunction) throws HibernateException;

    public void editTerminal(Inventoryterminalitem terminalitem) throws SQLException;

    public void editHardwareItem(Inventoryhardwareitem hardwareitem, String setSelectFunction) throws HibernateException, Exception;

    public List<Inventory> getTerminalData();

    public JSONArray getTerminalSearchedData(String user, String tModelId, String contextPath, int start, int length) throws SQLException;

    //public JSONArray getItemSearchedData(String categoryId, String contextPath, int start, int length) throws SQLException;
    public Long getTerminalTableCount(String user, String tModelId) throws SQLException;

    // public Long getItemTableCount(String categoryId) throws SQLException;
    public Inventoryterminalitem getTerminalById(int id) throws SQLException;

    public Inventoryhardwareitem getItemById(int id) throws SQLException;

    public long getTableDataCount(Inventoryhardwareitem parameters) throws SQLException;

    public List<JSONObject> getTableData(Inventoryhardwareitem parameters, int minCount, int start, HttpServletRequest request) throws HibernateException, SQLException, Exception;

    public long getTerminalTableDataCount(Inventoryterminalitem parameters, int location) throws SQLException;

    public List<JSONObject> getTerminalTableData(Inventoryterminalitem parameters, int location, int minCount, int start, HttpServletRequest request) throws HibernateException, SQLException, Exception;

    public boolean addTerminaltoTMCInventory(Inventoryterminalitem inventoryterminalitem) throws HibernateException, SQLException;

    public void changeStatusOfTerminalByStatus(Inventoryterminalitem inventoryterminalitem) throws SQLException, HibernateException;

    public Boolean validateSerialNumber(String serialNumber) throws SQLException;
    
    
    public Inventoryhardwareitem getInventoryCategoryById(int id) throws HibernateException;
    
    public Inventorycategory getInventoryCategoryByCategoryId(int id) throws HibernateException;
    
    public Inventoryterminalitem getInventoryitemBySerialNumber(String serialno) throws HibernateException;


   
}
