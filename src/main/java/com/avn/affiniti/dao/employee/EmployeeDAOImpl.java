/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.employee;

import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author : Roshen Dilshan
 * @Document : EmployeeDAOImpl
 * @Created on : Jun 23, 2017, 5:32:25 PM
 */
@Repository("employeeDAO")
public class EmployeeDAOImpl implements EmployeeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private DataSource dataSource;

    @Override
    public Employee getEmployeeById(int employeeid) throws HibernateException {
        return sessionFactory.getCurrentSession().get(Employee.class, employeeid);
    }

    @Override
    public Employee getFirstAssigneeEmployeeByUserRole(int userrole) throws HibernateException {
        return (Employee) sessionFactory.getCurrentSession().createQuery("FROM Employee E "
                + "WHERE E.employeeid = ("
                + "SELECT MIN(SU.employee.employeeid) "
                + "FROM Systemuser SU "
                + "WHERE SU.userrole.userroleid = :userrole "
                + "AND SU.status.statusid = :statusid "
                + ")")
                .setInteger("userrole", userrole)
                .setInteger("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE)
                .uniqueResult();
    }

    @Override
    public Employee getNextAssigneeEmployeeByUserRole(int userrole, int employee) throws HibernateException {
        return (Employee) sessionFactory.getCurrentSession().createQuery("FROM Employee E "
                + "WHERE E.employeeid = ("
                + "SELECT MIN(SU.employee.employeeid) "
                + "FROM Systemuser SU "
                + "WHERE SU.userrole.userroleid = :userrole "
                + "AND SU.employee.employeeid > :employee "
                + "AND SU.status.statusid = :statusid)")
                .setInteger("userrole", userrole)
                .setInteger("employee", employee)
                .setInteger("statusid", MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE)
                .uniqueResult();
    }

    @Override
    public Employee getEmployeeByUsername(String username) throws HibernateException {
        return (Employee) sessionFactory.getCurrentSession().createQuery("FROM Employee E "
                + "WHERE E.employeeid = ("
                + "SELECT SU.employee.employeeid "
                + "FROM Systemuser SU "
                + "WHERE SU.username = :username)")
                .setString("username", username).uniqueResult();
    }

    @Override
    public String getSystemUserByUsername(String username) throws HibernateException {

        String query = "SELECT UR.USERROLECODE "
                + "FROM systemuser SU "
                + "INNER JOIN employee E ON E.EMPLOYEEID = SU.USERID "
                + "INNER JOIN userrole UR ON UR.USERROLEID = SU.USERROLE "
                + "WHERE SU.username = ?";

        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, String.class);
    }

    @Override
    public String getEmployeeDetailsByUsername(String username) throws HibernateException {

        String query = "SELECT E.nameinfull "
                + "FROM systemuser SU "
                + "INNER JOIN employee E ON E.EMPLOYEEID = SU.USERID "
                + "WHERE SU.username = ?";

        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, String.class);

//        return (String) sessionFactory.getCurrentSession().createQuery("SELECT E.nameinfull "
//                + "FROM Systemuser SU "
//                + "JOIN SU.employee E "
//                + "WHERE SU.username = :username").setString("username", username).uniqueResult();
    }

    @Override
    public void saveOrUpdate(Employee employee) throws HibernateException {
        sessionFactory.getCurrentSession().saveOrUpdate(employee);
    }

    @Override
    public boolean isNotaDuplicateEpf(String epf) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM employee WHERE UPPER(EPF) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{epf.toUpperCase()}, Integer.class) == 0;
    }

    @Override
    public boolean isNotaDuplicateNic(String nic) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM employee WHERE UPPER(NIC) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{nic.toUpperCase()}, Integer.class) == 0;
    }

    @Override
    public boolean isNotaDuplicateEmail(String email) throws SQLException {
        String query = "SELECT COUNT(*) AS CNT FROM employee WHERE LOWER(EMAIL) = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{email.toLowerCase()}, Integer.class) == 0;
    }

    @Override
    public boolean isNotaDuplicateEmailUpdate(String email, String employeeId) throws SQLException {
        boolean isNotaDuplicateEmailUpdate = false;
        String query = "FROM Employee E WHERE E.email = :email";
        Employee employee = (Employee) sessionFactory.getCurrentSession().createQuery(query).setString("email", email).uniqueResult();

        if (employee == null) {
            isNotaDuplicateEmailUpdate = true;
        } else {
            if (String.valueOf(employee.getEmployeeid()).equals(employeeId)) {
                isNotaDuplicateEmailUpdate = true;
            } else {
                isNotaDuplicateEmailUpdate = false;
            }
        }
        return isNotaDuplicateEmailUpdate;
    }

    @Override
    public Employee getEmployeeByClientId(int clientId) throws HibernateException {
        String query = "FROM Employee E WHERE E.client.clientid = :clientid";
        Employee employee = (Employee) sessionFactory.getCurrentSession().createQuery(query).setParameter("clientid", clientId).uniqueResult();
        return employee;
    }

}
