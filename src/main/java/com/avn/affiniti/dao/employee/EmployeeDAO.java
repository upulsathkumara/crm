/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.employee;

import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Systemuser;
import java.sql.SQLException;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : EmployeeDAO
 * @Created on : Jun 23, 2017, 5:32:02 PM
 */
public interface EmployeeDAO {

    public Employee getEmployeeById(int employeeid) throws HibernateException;

    public Employee getFirstAssigneeEmployeeByUserRole(int userrole) throws HibernateException;

    public Employee getNextAssigneeEmployeeByUserRole(int userrole, int employee) throws HibernateException;

    public Employee getEmployeeByUsername(String username) throws HibernateException;
    
    public String getSystemUserByUsername(String username) throws HibernateException;

    public String getEmployeeDetailsByUsername(String username) throws HibernateException;

    public void saveOrUpdate(Employee employee) throws HibernateException;

    public boolean isNotaDuplicateEpf(String epf) throws SQLException;

    public boolean isNotaDuplicateNic(String nic) throws SQLException;

    public boolean isNotaDuplicateEmail(String email) throws SQLException;

    public boolean isNotaDuplicateEmailUpdate(String email, String employeeId) throws SQLException;

    public Employee getEmployeeByClientId(int clientId) throws HibernateException;
}
