/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.tickethistory;

import com.avn.affiniti.hibernate.model.Tickethistory;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketHistoryDAO
 * @Created on : Jun 29, 2017, 11:09:36 AM
 */
public interface TicketHistoryDAO {

    public int createTicketHistory(Tickethistory tickethistory) throws HibernateException;
    
    public int getAssigneeByTicketId(int ticketID, String userrolecode) throws HibernateException;
    
    public Tickethistory getMaxTicketHistoryByTicketStatus(int ticketid, int statusid) throws HibernateException;

}
