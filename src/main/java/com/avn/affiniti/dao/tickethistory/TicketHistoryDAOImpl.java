/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.tickethistory;

import com.avn.affiniti.hibernate.model.Tickethistory;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketHistoryDAOImpl
 * @Created on : Jun 29, 2017, 11:09:52 AM
 */
@Repository("ticketHistoryDAO")
public class TicketHistoryDAOImpl implements TicketHistoryDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int createTicketHistory(Tickethistory tickethistory) throws HibernateException {
        sessionFactory.getCurrentSession().save(tickethistory);
        return tickethistory.getTickethistoryid();
    }

    @Override
    public int getAssigneeByTicketId(int ticketID, String userrolecode) throws HibernateException {
        String hql = "SELECT DISTINCT(TH.employee.employeeid)"
                + "FROM Tickethistory TH "
                + "INNER JOIN Systemuser SU ON SU.employee.employeeid = TH.employee.employeeid "
                + "INNER JOIN Userrole UR ON UR.userroleid = SU.userrole.userroleid "
                + "WHERE TH.ticket.ticketid = :ticketid  AND UR.userrolecode = :userrolecode ";
        
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("ticketid", ticketID);
        query.setParameter("userrolecode", userrolecode);
        
        return (int) query.uniqueResult();
        
    }

    @Override
    public Tickethistory getMaxTicketHistoryByTicketStatus(int ticketid, int statusid) throws HibernateException {
//        Query query = sessionFactory.getCurrentSession().createQuery("FROM Tickethistory TH "
//                + "WHERE TH.tickethistoryid = "
//                + "(SELECT MAX(TH.tickethistoryid)-1 "
//                + "FROM Tickethistory TH "
//                + "WHERE TH.ticket.ticketid = :ticket "
//                + "AND TH.status.statusid = :status) ");
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Tickethistory TH "
                + "WHERE TH.ticket.ticketid = :ticketid1 "
                + "AND TH.tickethistoryid < (SELECT MAX(THH.tickethistoryid) "
                + "FROM Tickethistory THH WHERE THH.ticket.ticketid = :ticketid2 "
                + "AND THH.status.statusid = :statusid) "
                + "ORDER BY TH.tickethistoryid DESC ");
        query.setInteger("ticketid1", ticketid);
        query.setInteger("ticketid2", ticketid);
        query.setInteger("statusid", statusid);
        query.setMaxResults(1);
        return (Tickethistory) query.uniqueResult();
    }
 

}
