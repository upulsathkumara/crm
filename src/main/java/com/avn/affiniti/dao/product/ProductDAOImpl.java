/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.product;

import com.avn.affiniti.hibernate.model.Product;
import com.avn.affiniti.hibernate.model.Productcategory;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author :acer
 * @Document :ProductDAOImpl
 * @Created on:Sep 4, 2017,10:20:58 AM
 */
@Repository("productDAO")
public class ProductDAOImpl implements ProductDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Product> getProductByProductCategory(String productCategoryId) throws SQLException {
        String hql = "FROM Product";
        // FROM Product p ORDER BY p.createddatetime DESC
        List<Product> list = null;
        if (!productCategoryId.equals("")) {
            try {
                int pId = Integer.parseInt(productCategoryId);
                hql = hql.concat(" WHERE PRODUCTCATEGORY = " + pId);
                list = sessionFactory.getCurrentSession().createQuery(hql).list();
            } catch (HibernateException | NumberFormatException ex) {
                System.out.println("******" + ex);
            }
        }

        return list;
    }

    @Override
    public void createProduct(Product product, String setSelectFunction) throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
            session.persist(product);
        } else if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
            session.merge(product);
        } 
    }
    
//    @Override
//    public void updateProduct(Product product) throws HibernateException, Exception {
//        sessionFactory.getCurrentSession().merge(product);
//    }

    @Override
    public Product getProductById(int id) throws HibernateException {
        Product product = new Product();
        try {
            product = sessionFactory.getCurrentSession().get(Product.class, id);
        } catch (HibernateException ex) {
            System.out.println("########" + ex);
        }
        return product;
    }

    /////////////////////////////////table count////////////////////////
    @Override
    public long getTableDataCount(Productcategory parameters) throws SQLException {

//        Session session = sessionFactory.getCurrentSession();
//        Query query;
//        if (parameters != null) {
//            query = session.createQuery("SELECT COUNT(P.productid) FROM Product P WHERE P.productcategory.productcategoryid =:prodID");
//            query.setInteger("prodID", parameters.getProductcategoryid());
//        } else {
//            query = session.createQuery("SELECT COUNT(*) FROM Product");
//        }
//
//        return (long) query.uniqueResult();
        String query = "SELECT COUNT(*) FROM product P WHERE :where";
        query = query.replace(":where", this.getWhere(parameters));
        return new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

    }

    @Override
    public List<JSONObject> getTableData(Productcategory parameters, int minCount, int start) throws HibernateException, SQLException, Exception {

//        Session session = sessionFactory.getCurrentSession();
//        Query query;
//
//        if (parameters != null) {
//            System.out.println(parameters.getProductcategoryid());
//            query = session.createQuery("FROM Product P WHERE P.productcategory.productcategoryid =:prodID");
//            query.setParameter("prodID", parameters.getProductcategoryid());            
//        } else {
//
//            query = session.createQuery("FROM Product");
//        }
//
//        query.setMaxResults(minCount);
//        query.setFirstResult(start);
//
//        return query.list();
//    }
        String query = "SELECT "
                + "COALESCE(P.PRODUCTID, 'N/A') AS PRODUCTID, "
                + "COALESCE(P.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + "COALESCE(PC.DESCRIPTION, 'N/A') AS PRODUCTCATEGORY, "
                + "COALESCE(S.DESCRIPTION, 'N/A') AS STATUS, "
                + "COALESCE(P.SORTID, 'N/A') AS SORTID, "
                + "COALESCE(P.CREATEDATETIME, 'N/A') AS CREATEDATETIME, "
                + "COALESCE(P.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(P.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM product P "
                + "INNER JOIN productcategory PC ON  P.PRODUCTCATEGORY=PC.PRODUCTCATEGORYID "
                + "INNER JOIN status S ON  P.STATUS=S.STATUSID "
                + "WHERE :where "
                + "ORDER BY LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhere(parameters));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new ProductTableViewRowMapper());
//        return new JdbcTemplate(dataSource).query(query, new StatusTableViewRowMapper());
    }
    
    
    public class ProductTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("PRODUCTID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("PRODUCTID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";
            object.put("action", action);
            object.put("productid", rs.getInt("PRODUCTID"));
            object.put("productname", rs.getString("DESCRIPTION"));
            object.put("productcategory", rs.getString("PRODUCTCATEGORY"));
            object.put("status", rs.getString("STATUS"));
            object.put("lastupdatedtime", rs.getString("LASTUPDATEDDATETIME"));
            object.put("createdtime", rs.getString("CREATEDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }
    }

    private String getWhere(Productcategory parameters) {
        String where = "1 = 1 ";
        if (parameters != null) {
            where += "AND P.PRODUCTCATEGORY = " + parameters.getProductcategoryid();
        }
        return where;
    }
    
    @Override
    public Productcategory getProductCategoryByCategoryId(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Productcategory productcategory = session.load(Productcategory.class, id);
        Hibernate.initialize(productcategory);
        return productcategory;
    }
    
    
    

}
