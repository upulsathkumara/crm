 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.product;

import com.avn.affiniti.hibernate.model.Product;
import com.avn.affiniti.hibernate.model.Productcategory;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author acer
 */
public interface ProductDAO {

    public List<Product> getProductByProductCategory(String productCategoryId) throws SQLException;

    public void createProduct(Product product, String setSelectFunction) throws HibernateException, Exception;
    
    //public void updateProduct(Product product) throws HibernateException, Exception;

    
     public Product getProductById(int id) throws HibernateException;
     
     public long getTableDataCount(Productcategory parameters) throws SQLException;
     
     public List<JSONObject> getTableData(Productcategory parameters, int minCount, int start) throws HibernateException, SQLException, Exception;
     
    public Productcategory getProductCategoryByCategoryId(int id) throws HibernateException; 
}
