/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.notificationtemplate;
import com.avn.affiniti.hibernate.model.Notificationtempate;


/**
 * @Author Nadun Chamikara
 * @Document NotificationTemplateDAO
 * @Created on 05/01/2018, 1:15:15 PM
 */
public interface NotificationTemplateDAO {
    public Notificationtempate getNotificationTemplate(int Id) throws Exception;
}
