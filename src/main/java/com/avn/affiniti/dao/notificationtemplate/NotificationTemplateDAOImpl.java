/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.notificationtemplate;

import com.avn.affiniti.hibernate.model.Notificationtempate;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author Nadun Chamikara
 * @Document NotificationTemplateDAOImpl
 * @Created on 05/01/2018, 1:16:00 PM
 */

@Repository("notificationTemplateDAO")
public class NotificationTemplateDAOImpl implements NotificationTemplateDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public Notificationtempate getNotificationTemplate(int Id) throws HibernateException {
        return sessionFactory.getCurrentSession().get(Notificationtempate.class, Id);
    }
    
}
