/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrole;

import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : UserRoleDAOImpl
 * @Created on : Jun 23, 2017, 11:52:43 AM
 */
@Repository("userRoleDAO")
public class UserRoleDAOImpl implements UserRoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private DataSource dataSource;

    @Override
    public Userrole getUserRoleByUsername(String username) throws HibernateException {
        return (Userrole) sessionFactory.getCurrentSession().createQuery("FROM Userrole U "
                + "WHERE U.userroleid = ("
                + "SELECT SU.userrole.userroleid FROM Systemuser SU WHERE SU.username = :username"
                + ")")
                .setString("username", username)
                .uniqueResult();
    }

    @Override
    public Userrole getUserRoleByUserRoleCode(String useroelcode) throws HibernateException {
        return (Userrole) sessionFactory.getCurrentSession().createQuery("FROM Userrole U "
                + "WHERE U.userrolecode = :userrolecode")
                .setString("userrolecode", useroelcode)
                .uniqueResult();
    }

    /**
     * ***********************************--CHANDIMA
     * NIROSHAN--***************************************
     */
    /**
     * **********************************--Get Table Data
     * Count--************************************
     */
    @Override
    public long getTableDataCount(Userrole parameters) throws SQLException {

        String query = "SELECT COUNT(*) AS CNT FROM userrole UR WHERE :where";
        query = query.replace(":where", this.getWhere(parameters));
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    /**
     * **********************************--Get Table
     * Data--************************************
     */
    @Override
    public List<JSONObject> getTableData(Userrole parameters, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = " SELECT  COALESCE(UR.USERROLEID, 'N/A') AS USERROLEID, "
                + " COALESCE(UR.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + " COALESCE(UT.DESCRIPTION, 'N/A') AS USERROLETYPE, "
                + " COALESCE(UR.USERROLECODE, 'N/A') AS USERROLECODE, "
                + " COALESCE(ST.DESCRIPTION, 'N/A') AS STATUS, "
                + " COALESCE(UR.CREATEDATETIME, 'N/A') AS CREATEDATETIME, "
                + " COALESCE(UR.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + " COALESCE(UR.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + " FROM userrole UR INNER JOIN userroletype UT ON UR.USERROLETYPE = UT.USERROLETYPEID "
                + " INNER JOIN status ST ON ST.STATUSID = UR.STATUS "
                + " WHERE :where "
                + " ORDER BY UR.LASTUPDATEDDATETIME DESC "
                + " LIMIT ?,? ";

        String where = this.getWhere(parameters);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new UserroleTableViewRowMapper());
    }

    /**
     * **********************************--Get Table Data
     * Count--************************************
     */
    private String getWhere(Userrole parameters) {

        String where;

        if (parameters == null) {
            where = "1 = 1 ";
        } else if (parameters.getUserroleid() != null) {
            where = "UR.USERROLEID = " + parameters.getUserroleid();
        } else {
            where = "UR.DESCRIPTION LIKE '%" + parameters.getDescription() + "%'";
        }
        return where;
    }

    /**
     * **********************************--Userrole Table View Row
     * Mapper--************************************
     */
    public class UserroleTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("USERROLEID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("USERROLEID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("userroleId", rs.getInt("USERROLEID"));
            object.put("desctiption", rs.getString("DESCRIPTION"));
            object.put("userroletype", rs.getString("USERROLETYPE"));
            object.put("userrolecode", rs.getString("USERROLECODE"));
            object.put("status", rs.getString("STATUS"));
            object.put("createddate", rs.getString("CREATEDATETIME"));
            object.put("lastupdateddate", rs.getString("LASTUPDATEDDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }

    }

    /**
     * ***************************--User role Add and
     * Update--******************************
     */
    @Override
    public boolean addUserrole(Userrole userrole, String selectFunction) throws HibernateException, Exception {
        boolean result = true;
        Session session = sessionFactory.getCurrentSession();
        if (selectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
            session.persist(userrole);
        } else if (selectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
            session.merge(userrole);
        } else {
            result = false;
            // object.put("message", MessageVarList.ACCESS_DENIED);
        }
        //  sessionFactory.getCurrentSession().saveOrUpdate(userrole);
        return result;
    }

    /**
     * ******************************--Get Userrole By
     * ID--********************************
     */
    @Override
    public Userrole getUserroleById(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Userrole userrole = session.load(Userrole.class, id);
        Hibernate.initialize(userrole);
        return userrole;
    }

    @Override
    public long insertUserrole(com.avn.affiniti.model.userrole.Userrole userrole) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        long subsectionID = 0;
        try {
            String sql = "INSERT INTO `userrole`(`DESCRIPTION`, `USERROLECODE`, `USERROLETYPE`, `PARENTUSERROLE`, `STATUS`, `CREATEDATETIME`, `LASTUPDATEDDATETIME`, `CREATEDUSER`) VALUES ()";
            String[] generatedColumns = {"SUBSECTIONID"};
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql, generatedColumns);
            statement.setString(1, userrole.getDescription());
            statement.setString(2, userrole.getUserrolecode());
            statement.setInt(3, userrole.getUserroletype());
            statement.setInt(4, userrole.getParentuserrole());

            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                subsectionID = resultSet.getLong(1);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
        return subsectionID;
    }
    
    
    

}
