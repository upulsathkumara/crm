/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrole;

import com.avn.affiniti.hibernate.model.Userrole;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : UserRoleDAO
 * @Created on : Jun 23, 2017, 11:52:24 AM
 */
public interface UserRoleDAO {

    public Userrole getUserRoleByUsername(String username) throws HibernateException;

    public Userrole getUserRoleByUserRoleCode(String useroelcode) throws HibernateException;

    public long getTableDataCount(Userrole parameters) throws SQLException;

    public List<JSONObject> getTableData(Userrole parameters, int minCount, int start) throws HibernateException, SQLException, Exception;

    public boolean addUserrole(Userrole userrole, String selectFunction) throws HibernateException, Exception;

    public Userrole getUserroleById(int id) throws HibernateException;

    public long insertUserrole(com.avn.affiniti.model.userrole.Userrole userrole) throws Exception;
}
