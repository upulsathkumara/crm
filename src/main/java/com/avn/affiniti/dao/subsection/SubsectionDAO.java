/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.subsection;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Task;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author Isuru
 */
public interface SubsectionDAO {

    public List<Subsection> getSubsectionList(int max) throws HibernateException;

    public List<Section> getSectionList() throws HibernateException, Exception;

    public List<Status> getStatusList() throws HibernateException, Exception;

    public List<Subsection> getSubsectionListById(int id) throws HibernateException, Exception;

    public List<Subsection> getSubsectionListByDescription(String description);

    public boolean addSubsection(Subsection subsection, String setSelectFunction) throws HibernateException, Exception;

    public List<com.avn.affiniti.model.subsection.Subsection> getSubSectionsByUserroleAndSectionID(int userRole, String sectionid) throws Exception;

    public Subsection getSubsectionById(int id);

    public long getTableDataCount(Subsection parameters) throws SQLException;

    public List<JSONObject> getTableData(Subsection parameters, int minCount, int start) throws HibernateException, SQLException, Exception;

    public Subsection getsubSectionBySubSectionId(String subsectionId) throws HibernateException, SQLException, Exception;

    public Task gettaskByTaskId(String taskId) throws HibernateException, SQLException, Exception;

//    public long insertSubSection(Subsection subsection, String username) throws Exception;
//
//    public int getTableDataCount(Subsection parameters) throws Exception;
//
//    public List<Subsection> getTableData(Subsection parameters, int minCount, int start) throws Exception;
//
//    public Subsection getSubectionBySubSectionId(String subsectionId) throws Exception;
//
//    public void updateSubSection(Subsection subsection, String username) throws Exception;
//
//    public List<Subsection> getSubSectionsByUserroleAndSectionID(int userrole, String sectionid) throws Exception;
    }
