/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.subsection;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Isuru
 */
@Repository("subsectionDAO")
public class SubsectionDAOImpl implements SubsectionDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Subsection> getSubsectionList(int max) throws HibernateException {
        List<Subsection> subsectionList = null;
        System.out.println("********************accesed the DAO********************");

        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Subsection SB "
                + "JOIN FETCH SB.status");
        //query.setFirstResult(0);
        //query.setMaxResults(max);
        subsectionList = query.list();
        return subsectionList;
    }

    @Override
    public List<Section> getSectionList() throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Section");
        List<Section> sections = query.list();
        return sections;
    }

    @Override
    public List<Status> getStatusList() throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Status WHERE statusid IN (1,2) ORDER BY statusid");
        List<Status> statuses = query.list();
        return statuses;
    }

//    @Override
//    public List<com.avn.affiniti.model.subsection.Subsection> getSubSectionsByUserroleAndSectionID(int userRole, String sectionid) throws Exception {
//        Session session = sessionFactory.openSession();
//
//        List<com.avn.affiniti.model.subsection.Subsection> subsections;
//        try {
//            String sql
//                    = "FROM Subsection SUS "
//                    + "INNER JOIN FETCH Userrolesubsection URSUS ON URSUS.subsectionid = SUS.subsectionid "
//                    + "WHERE URSUS.userroleid = :userRole "
//                    + "AND URSUS.sectionid = :sectionid "
//                    + "AND SUS.statusid = 3 "
//                    + "ORDER BY SUS.sortid ";
//
//            subsections = new ArrayList<>();
//            com.avn.affiniti.model.subsection.Subsection subsection;
//            Query query = session.createQuery(sql);
//            query.setParameter("userRole", userRole);
//            query.setParameter("sectionid", sectionid);
//            List<Subsection> hbsubsections = query.list();
//
//            for (Subsection hbsubsection : hbsubsections) {
//                subsection = new com.avn.affiniti.model.subsection.Subsection();
//                subsection.setSubsectionDes(hbsubsection.getDescription());
//                subsection.setIcon(hbsubsection.getIcon());
//                subsection.setUrl(hbsubsection.getUrl());
//                subsection.setClickable(String.valueOf(hbsubsection.isClickable()));
//                subsections.add(subsection);
//            }
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
//
//        }
//        return subsections;
//    }
    @Override
    public List<com.avn.affiniti.model.subsection.Subsection> getSubSectionsByUserroleAndSectionID(int userRole, String sectionid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<com.avn.affiniti.model.subsection.Subsection> subsections;
        try {
            String sql = "SELECT "
                    + "IFNULL(SUS.DESCRIPTION, '') AS DESCRIPTION, "
                    + "IFNULL(SUS.ICON, '') AS ICON, "
                    + "IFNULL(SUS.URL, '') AS URL, "
                    + "CLICKABLE "
                    + "FROM subsection SUS "
                    + "INNER JOIN userrolesubsection URSUS ON URSUS.SUBSECTIONID = SUS.SUBSECTIONID "
                    + "WHERE URSUS.USERROLEID = ? "
                    + "AND URSUS.SECTIONID = ? "
                    + "AND SUS.STATUS = ? "
                    + "ORDER BY SUS.SORTID ";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userRole);
            statement.setInt(2, Integer.valueOf(sectionid));
            statement.setInt(3, MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE);
            resultSet = statement.executeQuery();
            subsections = new ArrayList<>();
            com.avn.affiniti.model.subsection.Subsection subsection;
            while (resultSet.next()) {
                subsection = new com.avn.affiniti.model.subsection.Subsection();
                subsection.setSubsectionDes(resultSet.getString("DESCRIPTION"));
                subsection.setIcon(resultSet.getString("ICON"));
                subsection.setUrl(resultSet.getString("URL"));
                subsection.setClickable(resultSet.getString("CLICKABLE"));
                subsections.add(subsection);
            }
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
        return subsections;
    }

    @Override
    public List<Subsection> getSubsectionListById(int id) throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Subsection where SUBSECTIONID = " + id);
        List<Subsection> subsectionList = query.list();
        return subsectionList;
    }

    @Override
    public List<Subsection> getSubsectionListByDescription(String description) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Subsection u where u.description like :userId");
        List<Subsection> userList = query.setParameter("userId", description + "%").list();
        return userList;
    }

    @Override
    public long getTableDataCount(Subsection parameters) throws SQLException {

        String query = "SELECT COUNT(*) AS CNT FROM subsection SS WHERE :where";
        query = query.replace(":where", this.getWhere(parameters));
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    @Override
    public List<JSONObject> getTableData(Subsection parameters, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT "
                + "COALESCE(SS.SUBSECTIONID, 'N/A') AS SUBSECTIONID, "
                + "COALESCE(SS.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + "COALESCE(S.DESCRIPTION, 'N/A') AS SECTION, "
                + "COALESCE(ST.DESCRIPTION, 'N/A') AS STATUS, "
                + "COALESCE(SS.URL, 'N/A') AS URL, "
                + "COALESCE(SS.CLICKABLE, 'N/A') AS CLICKABLE, "
                + "COALESCE(SS.ICON, 'N/A') AS ICON "
                + "FROM subsection SS INNER JOIN section S ON SS.SECTIONID = S.SECTIONID "
                + "INNER JOIN status ST ON ST.STATUSID = SS.STATUS "
                + "WHERE :where "
                + "ORDER BY SS.LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";

        String where = this.getWhere(parameters);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new SubSectionTableViewRowMapper());
    }

    public class SubSectionTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("SUBSECTIONID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("SUBSECTIONID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("subsectionId", rs.getInt("SUBSECTIONID"));
            object.put("subsectionDes", rs.getString("DESCRIPTION"));
            object.put("sectionDes", rs.getString("SECTION"));
            object.put("statusDes", rs.getString("STATUS"));
            object.put("url", rs.getString("URL"));

            if (rs.getString("CLICKABLE") == "0" || rs.getString("CLICKABLE").equalsIgnoreCase("0")) {
                object.put("clickable", "NO");

            } else if (rs.getString("CLICKABLE") == "1" || rs.getString("CLICKABLE").equalsIgnoreCase("1")) {
                object.put("clickable", "YES");
            } else {
                object.put("clickable", "N/A");
            }
            if (rs.getString("ICON") == "" || rs.getString("ICON").equalsIgnoreCase("")) {
                object.put("icon", "N/A");
            } else {
                object.put("icon", rs.getString("ICON"));
            }

            return object;
        }
    }

    private String getWhere(Subsection parameters) {
        String where;
        if (parameters == null) {
            where = "1 = 1 ";
        } else if (parameters.getSection() != null) {
            int sec = parameters.getSection().getSectionid();
            where = "SS.SECTIONID = " + sec;
        } else {
            where = "SS.DESCRIPTION LIKE '%" + parameters.getDescription() + "%'";
        }
        return where;
    }

    @Override
    public boolean addSubsection(Subsection subsection, String setSelectFunction) throws HibernateException, Exception {
        boolean result = true;
        //sessionFactory.getCurrentSession().saveOrUpdate(subsection);
        Session session = sessionFactory.getCurrentSession();
        if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
            session.persist(subsection);
        } else if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
            session.merge(subsection);
        } else {
            result = false;
            // object.put("message", MessageVarList.ACCESS_DENIED);
        }
        return result;
    }

    @Override
    public Subsection getSubsectionById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Subsection subsection = session.load(Subsection.class, id);
        //Hibernate.initialize(subsection);
        return subsection;
    }

//    @Override
//    public long insertSubSection(Subsection subsection, String username) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        long subsectionID = 0;
//        try {
//            String sql = "INSERT INTO AVN_SUBSECTION (DESCRIPTION,SECTIONID, CREATEDDATETIME, LASTUPDATEDDATETIME,CREATEDUSER,STATUS,ICON,SORTID,URL,CLICKABLE) "
//                    + " VALUES (?,?,CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,?,?,?,?,?,?)";
//            String[] generatedColumns = {"SUBSECTIONID"};
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql, generatedColumns);
//            statement.setString(1, subsection.getSubsectionDes());
//            statement.setString(2, subsection.getSectionid());
//            statement.setString(3, username);
//            statement.setInt(4, Integer.valueOf(subsection.getStatusid()));
//            if (subsection.getIcon() != null && !subsection.getIcon().isEmpty()) {
//                statement.setString(5, subsection.getIcon());
//            } else {
//                statement.setNull(5, Types.VARCHAR);
//            }
//            statement.setInt(6, Integer.valueOf(subsection.getSortid()));
//            statement.setString(7, subsection.getUrl());
//            if (subsection.isClickableurl()) {
//                statement.setInt(8, MasterDataVarList.AFFINITI_CODE_CLICKABLE_TRUE);
//            } else {
//                statement.setInt(8, MasterDataVarList.AFFINITI_CODE_CLICKABLE_FALSE);
//            }
//            statement.executeUpdate();
//            resultSet = statement.getGeneratedKeys();
//            while (resultSet.next()) {
//                subsectionID = resultSet.getLong(1);
//            }
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return subsectionID;
//    }
//
//    @Override
//    public int getTableDataCount(Subsection parameters) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        int count = 0;
//        try {
//            String sql = "SELECT COUNT(*) AS CNT "
//                    + "FROM AVN_SUBSECTION SS "
//                    + "INNER JOIN AVN_STATUS ST "
//                    + "ON SS.STATUS=ST.STATUSID "
//                    + "INNER JOIN AVN_SECTION S "
//                    + "ON SS.SECTIONID=S.SECTIONID :where ";
//            sql = sql.replace(":where", this.getWhere(parameters));
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                count = resultSet.getInt("CNT");
//            }
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return count;
//    }
//
//    private String getWhere(Subsection parameters) {
//        String where = "";
//        if (parameters.getSearchoption() != null
//                && !parameters.getSearchoption().isEmpty()
//                && parameters.getSearchoption().equalsIgnoreCase("subsectionId")
//                && parameters.getInput() != null
//                && !parameters.getInput().isEmpty()) {
//            where += "where SS.SUBSECTIONID LIKE '%" + parameters.getInput().trim() + "%'";
//        }
//        if (parameters.getSearchoption() != null
//                && !parameters.getSearchoption().isEmpty()
//                && parameters.getSearchoption().equalsIgnoreCase("subsectionDes")
//                && parameters.getInput() != null
//                && !parameters.getInput().isEmpty()) {
//            where += "where SS.DESCRIPTION LIKE '%" + parameters.getInput().trim() + "%'";
//        }
//        return where;
//    }
//
//    @Override
//    public List<Subsection> getTableData(Subsection parameters, int minCount, int start) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<Subsection> list = null;
//        try {
//            String sql = "SELECT "
//                    + "    * "
//                    + " FROM "
//                    + "    ("
//                    + "    SELECT "
//                    + "        TB.*, "
//                    + "         ROWNUM AS ROWNUMER  "
//                    + "    FROM "
//                    + "        ( SELECT SS.SUBSECTIONID,SS.DESCRIPTION AS SUBDES,SS.SECTIONID,SS.STATUS, ST.DESCRIPTION AS STDES, S.DESCRIPTION AS SDES,  NVL(SS.ICON,'--')AS ICON, SS.SORTID,SS.URL,SS.CLICKABLE FROM AVN_SUBSECTION SS INNER JOIN AVN_STATUS ST ON SS.STATUS=ST.STATUSID INNER JOIN AVN_SECTION S ON SS.SECTIONID=S.SECTIONID :where ORDER BY SS.SUBSECTIONID ASC) TB  "
//                    + "    WHERE "
//                    + "        ROWNUM <= ?  "
//                    + "    ) "
//                    + " WHERE "
//                    + "    ROWNUMER > ? ";
//
//            sql = sql.replace(":where", this.getWhere(parameters));
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, start + minCount);
//            statement.setInt(2, start);
//            resultSet = statement.executeQuery();
//
//            list = new ArrayList<>();
//            Subsection subsectionData;
//            while (resultSet.next()) {
//                subsectionData = new Subsection();
//                subsectionData.setSubsectionId(resultSet.getString("SUBSECTIONID"));
//                subsectionData.setSubsectionDes(resultSet.getString("SUBDES"));
//                subsectionData.setSectionid(resultSet.getString("SECTIONID"));
//                subsectionData.setSectionDes(resultSet.getString("SDES"));
//                subsectionData.setStatusid(resultSet.getString("STATUS"));
//                subsectionData.setStatusDes(resultSet.getString("STDES"));
//                subsectionData.setUrl(resultSet.getString("URL"));
//                subsectionData.setIcon(resultSet.getString("ICON"));
//                subsectionData.setSortid(resultSet.getString("SORTID"));
//                if (resultSet.getString("CLICKABLE").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_CLICKABLE_TRUE))) {
//                    subsectionData.setClickable("YES");
//                } else {
//                    subsectionData.setClickable("NO");
//                }
//                list.add(subsectionData);
//            }
//
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return list;
//    }
//
//    @Override
//    public Subsection getSubectionBySubSectionId(String subsectionId) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        Subsection subsectionData = null;
//        try {
//            String sql = " SELECT SS.SUBSECTIONID,SS.DESCRIPTION AS SUBDES,SS.SECTIONID,SS.STATUS, ST.DESCRIPTION AS STDES, S.DESCRIPTION AS SDES,"
//                    + " SS.ICON,SS.SORTID,SS.URL, SS.CLICKABLE FROM AVN_SUBSECTION SS INNER JOIN AVN_STATUS ST ON SS.STATUS=ST.STATUSID INNER JOIN AVN_SECTION S ON "
//                    + " SS.SECTIONID=S.SECTIONID WHERE SS.SUBSECTIONID=?";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, Integer.valueOf(subsectionId));
//            resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                subsectionData = new Subsection();
//                subsectionData.setSubsectionId(resultSet.getString("SUBSECTIONID"));
//                subsectionData.setSubsectionDes(resultSet.getString("SUBDES"));
//                subsectionData.setSectionid(resultSet.getString("SECTIONID"));
//                subsectionData.setSectionDes(resultSet.getString("SDES"));
//                subsectionData.setStatusid(resultSet.getString("STATUS"));
//                subsectionData.setStatusDes(resultSet.getString("STDES"));
//                subsectionData.setIcon(resultSet.getString("ICON"));
//                subsectionData.setUrl(resultSet.getString("URL"));
//                subsectionData.setSortid(resultSet.getString("SORTID"));
//                if (resultSet.getString("CLICKABLE").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_CLICKABLE_TRUE))) {
//                    subsectionData.setClickableurl(true);
//                } else {
//                    subsectionData.setClickableurl(false);
//                }
//            }
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return subsectionData;
//    }
//
//    @Override
//    public void updateSubSection(Subsection subsection, String username) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        try {
//            String sql = "UPDATE AVN_SUBSECTION SET DESCRIPTION=?,LASTUPDATEDDATETIME=CURRENT_TIMESTAMP,CREATEDUSER=?,STATUS=?,URL=?,ICON=?,SORTID=?,CLICKABLE=? WHERE SUBSECTIONID=?";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, subsection.getSubsectionDes());
//            statement.setString(2, username);
//            statement.setInt(3, Integer.valueOf(subsection.getStatusid()));
//            statement.setString(4, subsection.getUrl());
//            statement.setString(5, subsection.getIcon());
//            statement.setInt(6, Integer.valueOf(subsection.getSortid()));
//               if (subsection.isClickableurl()) {
//                statement.setInt(7, MasterDataVarList.AFFINITI_CODE_CLICKABLE_TRUE);
//            } else {
//                statement.setInt(7, MasterDataVarList.AFFINITI_CODE_CLICKABLE_FALSE);
//            }
//            statement.setString(8, subsection.getSubsectionId());
//          
//            statement.executeUpdate();
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//    }
//
//    @Override
//    public List<Subsection> getSubSectionsByUserroleAndSectionID(int userrole, String sectionid) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<Subsection> subsections;
//        try {
//            String sql = "SELECT "
//                    + "IFNULL(SUS.DESCRIPTION, '') AS DESCRIPTION, "
//                    + "IFNULL(SUS.ICON, '') AS ICON, "
//                    + "IFNULL(SUS.URL, '') AS URL, "
//                    + "CLICKABLE "
//                    + "FROM subsection SUS "
//                    + "INNER JOIN userrolesubsection URSUS ON URSUS.SUBSECTIONID = SUS.SUBSECTIONID "
//                    + "WHERE URSUS.USERROLEID = ? AND URSUS.SECTIONID = ? "
//                    + "ORDER BY SUS.SORTID";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, userrole);
//            statement.setInt(2, Integer.valueOf(sectionid));
//            resultSet = statement.executeQuery();
//            subsections = new ArrayList<>();
//            Subsection subsection;
//            while (resultSet.next()) {
//                subsection = new Subsection();
//                subsection.setSubsectionDes(resultSet.getString("DESCRIPTION"));
//                subsection.setIcon(resultSet.getString("ICON"));
//                subsection.setUrl(resultSet.getString("URL"));
//                subsection.setClickable(resultSet.getString("CLICKABLE"));
//                subsections.add(subsection);
//            }
//        } catch (SQLException sqle) {
//            throw sqle;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return subsections;
//    }
    @Override
    public Subsection getsubSectionBySubSectionId(String subsectionId) throws HibernateException, SQLException, Exception {

        Session session = sessionFactory.getCurrentSession();
        Subsection subsection = session.get(Subsection.class, Integer.parseInt(subsectionId));
        return subsection;
    }

    @Override
    public Task gettaskByTaskId(String taskId) throws HibernateException, SQLException, Exception {

        Session session = sessionFactory.getCurrentSession();
        Task task = session.get(Task.class, Integer.parseInt(taskId));
        return task;
    }
}
