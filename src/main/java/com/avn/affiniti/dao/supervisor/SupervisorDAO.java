/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.supervisor;

import com.avn.affiniti.hibernate.model.Employeesupervisor;
import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : SupervisorDAO
 * @Created on : Jul 26, 2016, 10:31:35 AM
 */
public interface SupervisorDAO {

//    public void insertSupervisor(User user) throws SQLException;
    public void insertSupervisor(Employeesupervisor employeesupervisor) throws HibernateException;

//    public void deleteSupervisor(User user) throws SQLException;
    public void deleteSupervisorsByEmployee(int employeeId) throws HibernateException;

}
