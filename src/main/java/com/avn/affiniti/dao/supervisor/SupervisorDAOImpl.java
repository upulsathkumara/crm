/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.supervisor;

import com.avn.affiniti.hibernate.model.Employeesupervisor;
import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : SupervisorDAOImpl
 * @Created on : Jul 26, 2016, 10:31:53 AM
 */
@Repository("supervisorDAO")
public class SupervisorDAOImpl implements SupervisorDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

//    @Override
//    public void insertSupervisor(User user) throws SQLException {
//        String query = "INSERT INTO employeesupervisor(EMPLOYEEID, "
//                + "                               SUPERVISOR, "
//                + "                               CREATEDATETIME, "
//                + "                               LASTUPDATEDDATETIME, "
//                + "                               CREATEDUSER) "
//                + "VALUES (?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
//        for (int supervisor : user.getSupervisors()) {
//            jdbcTemplate.update(query, user.getEmployeeid(), supervisor, user.getCreateduser());
//        }
//    }
    @Override
    public void insertSupervisor(Employeesupervisor employeesupervisor) throws HibernateException {
        sessionFactory.getCurrentSession().save(employeesupervisor);
    }

//    @Override
//    public void deleteSupervisor(User user) throws SQLException {
//        String query = "DELETE FROM employeesupervisor WHERE EMPLOYEEID = ?";
//        new JdbcTemplate(dataSource).update(query, new Object[]{user.getEmployeeid()});
//    }
    @Override
    public void deleteSupervisorsByEmployee(int employeeId) throws HibernateException {
//        String hql = "DELETE FROM Employeesupervisor WHERE EMPLOYEEID =:employeeId";
        String hql = "DELETE Employeesupervisor ES WHERE ES.id.employeeid =:employeeId";
        sessionFactory.getCurrentSession().createQuery(hql).setParameter("employeeId", employeeId).executeUpdate();
    }

}
