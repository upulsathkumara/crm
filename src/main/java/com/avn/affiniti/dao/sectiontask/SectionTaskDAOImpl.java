package com.avn.affiniti.dao.sectiontask;

import com.avn.affiniti.dao.audittrace.AuditTraceDAOImpl;
import com.avn.affiniti.dao.task.TaskDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Sectiontask;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.util.Loggers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Isuru
 */
@Repository("sectionTaskDAO")
public class SectionTaskDAOImpl implements SectionTaskDAO {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private TaskDAO taskDAO;
    @Autowired
    private AuditTraceDAOImpl auditTraceDAOImpl;

    @Override
    public boolean insertSectionTask(Sectiontask sectiontask) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(sectiontask);
        } catch (HibernateException ex) {
            status = false;
            Loggers.LOGGER_FILE.error(ex);
            throw ex;
        }
        return status;
    }

    @Override
    public boolean deleteSectionTask(Section section) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery("DELETE Sectiontask sk where sk.id.sectionid = :sectionId");
            query.setParameter("sectionId", section.getSectionid());
            query.executeUpdate();
        } catch (HibernateException ex) {
            status = false;
            Loggers.LOGGER_FILE.error(ex);
            throw ex;
        }
        return status;
    }

    @Override
    public long getTableDataCount(Sectiontask parameters) throws Exception {
        String query = "SELECT COUNT(*) FROM sectiontask SK WHERE :where";
        String where = this.getWhere(parameters);
        query = query.replace(":where", where);
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        return count;
    }

    @Override
    public List<JSONObject> getTableData(Sectiontask parameters, int minCount, int start) throws Exception {
        String query = "SELECT "
                + "COALESCE(SK.SECTIONID, 'N/A') AS SECTIONID, "
                + "COALESCE(SK.TASKID, 'N/A') AS TASKID, "
                + "COALESCE((SELECT DESCRIPTION FROM section WHERE SECTIONID = SK.SECTIONID), 'N/A') AS SECTION, "
                + "COALESCE((SELECT DESCRIPTION FROM task WHERE TASKID = SK.TASKID), 'N/A') AS TASK, "
                + "COALESCE(URL, 'N/A') AS URL, "
                + "COALESCE(SK.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(SK.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + "COALESCE(SK.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM sectiontask SK "
                + "WHERE :where "
                + "ORDER BY LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";

        String where = this.getWhere(parameters);
        query = query.replace(":where", where);
        
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new SectiontaskTableViewRowMapper());
    }

    private String getWhere(Sectiontask parameters) {
        
        String where;

        if (parameters.getSection().getSectionid() != null && !parameters.getSection().getDescription().equals("")) {
            where = "(SK.SECTIONID = " + parameters.getSection().getSectionid() + " OR SK.TASKID = " + parameters.getSection().getSectionid() + ") "
                    + "AND (SK.SECTIONID IN (SELECT SECTIONID FROM section WHERE DESCRIPTION LIKE '%" + parameters.getSection().getDescription() + "%') "
                    + "OR SK.TASKID IN (SELECT TASKID FROM task  WHERE DESCRIPTION LIKE '%" + parameters.getTask().getDescription() + "%')) ";
        } else if (parameters.getSection().getSectionid() != null) {
            where = "SK.SECTIONID = " + parameters.getSection().getSectionid() + " OR SK.TASKID = " + parameters.getSection().getSectionid();
        } else if (!parameters.getSection().getDescription().equals("")) {
            where = "SK.SECTIONID IN (SELECT SECTIONID FROM section WHERE DESCRIPTION LIKE '%" + parameters.getSection().getDescription() + "%') "
                    + "OR SK.TASKID IN (SELECT TASKID FROM task  WHERE DESCRIPTION LIKE '%" + parameters.getTask().getDescription() + "%')";
        } else {
            where = "1 = 1";
        }
        return where;
    }

    @Override
    public List<JSONObject> getUrlBySectionId(int SectionId) throws Exception {
        String query = "SELECT "
                + "COALESCE(SK.SECTIONID, 'N/A') AS SECTIONID, "
                + "COALESCE(SK.TASKID, 'N/A') AS TASKID, "
                + "COALESCE((SELECT DESCRIPTION FROM section WHERE SECTIONID = SK.SECTIONID), 'N/A') AS SECTION, "
                + "COALESCE((SELECT DESCRIPTION FROM task WHERE TASKID = SK.TASKID), 'N/A') AS TASK, "
                + "COALESCE(URL, 'N/A') AS URL, "
                + "COALESCE(SK.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(SK.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + "COALESCE(SK.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM sectiontask SK WHERE SECTIONID = " + SectionId;

        return new JdbcTemplate(dataSource).query(query, new SectiontaskTableViewRowMapper());
    }

    public class SectiontaskTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getString("SECTIONID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getString("SECTIONID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("sectionId", rs.getString("SECTIONID"));
            object.put("taskId", rs.getString("TASKID"));
            object.put("sectionDes", rs.getString("SECTION"));
            object.put("taskDes", rs.getString("TASK"));
            object.put("url", rs.getString("URL"));
            object.put("lastupdateddatetime", rs.getTimestamp("LASTUPDATEDDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }
    }

    @Override
    public void insertAudittrace(Sectiontask sectionTask, String action, JSONArray taskList, String username) throws Exception {

        Audittrace audittrace = new Audittrace();
        String description = action + "ing SectionTask ";

        if (action.equals("create") || action.equals("update")) {
            description = description + ", Section Id : " + sectionTask.getId().getSectionid() + " Task Ids :";
            for (int i = 0; taskList.length() > i; i++) {
                Task task = taskDAO.getTaskById(Integer.parseInt(taskList.getJSONObject(i).getString("taskid")));
                description = " " + description + task.getDescription() + ",";
            }
            audittrace.setAfectedid(sectionTask.getId().getSectionid() + " , " + sectionTask.getId().getTaskid());

        } else if (action.equals("view")) {
            description = description + ", Section Id : " + sectionTask.getSection().getSectionid();
        } else if (action.equals("search")) {
            if (!sectionTask.getSection().getDescription().equals("")) {
                description = description + ", Section and Task Description : " + sectionTask.getSection().getDescription();
            }
            if (sectionTask.getSection().getSectionid() != null) {
                description = description + ", Section Id and Task Id : " + sectionTask.getSection().getSectionid();
            }
            if (sectionTask.getSection().getDescription().equals("") && sectionTask.getSection().getSectionid() == null) {
                description = "View all data in Section Task Table";
            }
        }

        audittrace.setDescription(description);
        audittrace.setTask(action);
        audittrace.setCreateduser(username);
        audittrace.setAffectedpage("Sectiontask");
        audittrace.setCreatedatetime(new Date());
        audittrace.setLastupdateddatetime(new Date());
        auditTraceDAOImpl.insertAuditTrace(audittrace);
    }

//    @Override
//    public List<SectionTask> insertSectionTask(SectionTask sectiontask, String username) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<SectionTask> list = null;
//        try {
//            JSONArray array = new JSONArray(sectiontask.getMultitaskarray());
//            String sql = "DELETE FROM AVN_USERROLETASK WHERE USERROLESUBSECTIONID  IN (SELECT  USERROLESUBSECTIONID FROM AVN_USERROLESUBSECTION where SECTIONID=?) AND TASKID NOT IN (" + array.toString().replaceAll("[\\[\\]\"]", "") + ")";
//            connection = dataSource.getConnection();
//            connection.setAutoCommit(false);
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectiontask.getSectionid());
//            statement.execute();
//
//            sql = "DELETE FROM AVN_SECTIONTASK WHERE SECTIONID=?";
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectiontask.getSectionid());
//            statement.execute();
//
//            sql = "INSERT INTO AVN_SECTIONTASK (SECTIONID,TASKID,CREATEDDATETIME,LASTUPDATEDDATETIME,CREATEDUSER) VALUES (?,?,CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,?)";
//            statement = connection.prepareStatement(sql);
//            if (sectiontask.getMultitaskarray() != null && !sectiontask.getMultitaskarray().isEmpty()) {
////                JSONArray array = new JSONArray(sectiontask.getMultitaskarray());
//                list = new ArrayList<>();
//                SectionTask sectiontaskData;
//                for (int i = 0; i < array.length(); i++) {
//                    sectiontaskData = new SectionTask();
//                    String taskid = array.getString(i);
//                    sectiontaskData.setTaskid(taskid);
//                    statement.setInt(1, Integer.valueOf(sectiontask.getSectionid()));
//                    statement.setInt(2, Integer.valueOf(taskid));
//                    statement.setString(3, username);
//                    statement.execute();
//                    list.add(sectiontaskData);
//                }
//            }
//            connection.commit();
//        } catch (Exception e) {
//            if (connection != null) {
//                connection.rollback();
//            }
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                    connection.rollback();
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return list;
//    }
//
//    @Override
//    public int getTableDataCount(SectionTask parameters) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        int count = 0;
//        try {
//            String sql = "SELECT COUNT(*) CNT FROM AVN_SECTIONTASK ST INNER JOIN AVN_TASK TS ON ST.TASKID=TS.TASKID INNER JOIN AVN_SECTION S ON ST.SECTIONID=S.SECTIONID :where ";
//            sql = sql.replace(":where", this.getWhere(parameters));
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                count = resultSet.getInt("CNT");
//            }
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return count;
//    }
//
//    private String getWhere(SectionTask parameters) {
//        String where = "";
//
//        if (parameters.getSectionid() != null
//                && !parameters.getSectionid().equalsIgnoreCase("--")) {
//            where += "AND ST.SECTIONID LIKE '%" + parameters.getSectionid().trim() + "%'";
//        }
//        return where;
//    }
//
//    @Override
//    public List<SectionTask> getTableData(SectionTask parameters, int minCount, int start) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<SectionTask> list = null;
//        try {
//            String sql = "SELECT "
//                    + "    * "
//                    + " FROM "
//                    + "    ("
//                    + "    SELECT "
//                    + "        TB.*, "
//                    + "         ROWNUM AS ROWNUMER  "
//                    + "    FROM "
//                    + "        ( SELECT ST.SECTIONID, S.DESCRIPTION AS SDES, ST.TASKID, TS.DESCRIPTION AS TDES,ST.CREATEDDATETIME,ST.LASTUPDATEDDATETIME,ST.CREATEDUSER FROM AVN_SECTIONTASK ST INNER JOIN AVN_TASK TS ON ST.TASKID=TS.TASKID INNER JOIN AVN_SECTION S ON ST.SECTIONID=S.SECTIONID :where ORDER BY ST.SECTIONID ASC) TB  "
//                    + "    WHERE "
//                    + "        ROWNUM <= ?  "
//                    + "    ) "
//                    + " WHERE "
//                    + "    ROWNUMER > ? ";
//
//            sql = sql.replace(":where", this.getWhere(parameters));
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, start + minCount);
//            statement.setInt(2, start);
//            resultSet = statement.executeQuery();
//
//            list = new ArrayList<>();
//            SectionTask sectiontaskData;
//            while (resultSet.next()) {
//                sectiontaskData = new SectionTask();
//                sectiontaskData.setSectionid(resultSet.getString("SECTIONID"));
//                sectiontaskData.setSectionDes(resultSet.getString("SDES"));
//                sectiontaskData.setTaskid(resultSet.getString("TASKID"));
//                sectiontaskData.setTaskDes(resultSet.getString("TDES"));
//                sectiontaskData.setCreateddatetime(resultSet.getString("CREATEDDATETIME"));
//                sectiontaskData.setLastupdatedtime(resultSet.getString("LASTUPDATEDDATETIME"));
//                sectiontaskData.setCreateduser(resultSet.getString("CREATEDUSER"));
//                list.add(sectiontaskData);
//            }
//
//        } catch (Exception exception) {
//            throw exception;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return list;
//    }
//
//    @Override
//    public Map<String, String> getAssignedTask(String sectionid) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        Map<String, String> list = null;
//        try {
//            String sql = "SELECT  ST.TASKID, TS.DESCRIPTION AS TDES FROM AVN_SECTIONTASK ST INNER JOIN AVN_TASK TS ON ST.TASKID=TS.TASKID WHERE ST.SECTIONID=? ORDER BY ST.SECTIONID ASC";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectionid);
//            resultSet = statement.executeQuery();
//            list = new LinkedHashMap<>();
//            while (resultSet.next()) {
//                list.put(resultSet.getString("TASKID"), resultSet.getString("TDES"));
//            }
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return list;
//    }
//
//    @Override
//    public Map<String, String> getNotAssignedTask(String sectionid) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        Map<String, String> list = null;
//        try {
//            String sql = "SELECT T.TASKID, T.DESCRIPTION AS TDES FROM AVN_TASK T WHERE TASKID NOT IN (SELECT  ST.TASKID FROM AVN_SECTIONTASK ST INNER JOIN AVN_TASK TS ON ST.TASKID=TS.TASKID  WHERE ST.SECTIONID=?)";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectionid);
//            resultSet = statement.executeQuery();
//            list = new LinkedHashMap<>();
//            while (resultSet.next()) {
//                list.put(resultSet.getString("TASKID"), resultSet.getString("TDES"));
//            }
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception exception) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception exception) {
//                }
//            }
//        }
//        return list;
//    }
//
//    @Override
//    public List<SectionTask> updatSectionTask(SectionTask sectiontask, String username) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<SectionTask> list = null;
//        try {
//            JSONArray array = new JSONArray(sectiontask.getMultitaskarray());
//            String sql = "DELETE FROM AVN_USERROLETASK WHERE USERROLESUBSECTIONID  IN (SELECT  USERROLESUBSECTIONID FROM AVN_USERROLESUBSECTION where SECTIONID=?) AND TASKID NOT IN (" + array.toString().replaceAll("[\\[\\]\"]", "") + ")";
//            connection = dataSource.getConnection();
//            connection.setAutoCommit(false);
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectiontask.getSectionid());
//            statement.execute();
//
//            sql = "DELETE FROM AVN_SECTIONTASK WHERE SECTIONID=?";
//
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectiontask.getSectionid());
//            statement.execute();
//
//            sql = "INSERT INTO AVN_SECTIONTASK (SECTIONID,TASKID,CREATEDDATETIME,LASTUPDATEDDATETIME,CREATEDUSER) VALUES (?,?,CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,?)";
//            statement = connection.prepareStatement(sql);
//            if (sectiontask.getMultitaskarray() != null && !sectiontask.getMultitaskarray().isEmpty()) {
//
//                list = new ArrayList<>();
//                SectionTask sectiontaskData;
//                for (int i = 0; i < array.length(); i++) {
//                    sectiontaskData = new SectionTask();
//                    String taskid = array.getString(i);
//                    sectiontaskData.setTaskid(taskid);
//                    statement.setInt(1, Integer.valueOf(sectiontask.getSectionid()));
//                    statement.setInt(2, Integer.valueOf(taskid));
//                    statement.setString(3, username);
//                    statement.execute();
//                    list.add(sectiontaskData);
//                }
//            }
//            connection.commit();
//        } catch (Exception e) {
//            if (connection != null) {
//                connection.rollback();
//            }
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                    connection.rollback();
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return list;
//    }
//
//    public List<SectionTask> getAssignSectionDropdownListByUserRoleID(String sectionid) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<SectionTask> assignTaskList = null;
//        try {
//            String sql = "SELECT  ST.TASKID, TS.DESCRIPTION AS TDES FROM AVN_SECTIONTASK ST INNER JOIN AVN_TASK TS ON ST.TASKID=TS.TASKID WHERE ST.SECTIONID=?";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectionid);
//            resultSet = statement.executeQuery();
//            assignTaskList = new ArrayList<>();
//            SectionTask sectiontask;
//            while (resultSet.next()) {
//                sectiontask = new SectionTask();
//                sectiontask.setTaskid(resultSet.getString("TASKID"));
//                sectiontask.setTaskDes(resultSet.getString("TDES"));
//                assignTaskList.add(sectiontask);
//            }
//        } catch (SQLException | NumberFormatException e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return assignTaskList;
//    }
//
//    public List<SectionTask> getNotAssignSectionDropdownListByUserRoleID(String sectionid) throws Exception {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        List<SectionTask> NotAssignTaskList = null;
//        try {
//            String sql = "SELECT T.TASKID, T.DESCRIPTION AS TDES FROM AVN_TASK T WHERE TASKID NOT IN (SELECT  ST.TASKID FROM AVN_SECTIONTASK ST INNER JOIN AVN_TASK TS ON ST.TASKID=TS.TASKID  WHERE ST.SECTIONID=?)";
//            connection = dataSource.getConnection();
//            statement = connection.prepareStatement(sql);
//            statement.setString(1, sectionid);
//            resultSet = statement.executeQuery();
//            NotAssignTaskList = new ArrayList<>();
//            SectionTask sectiontask;
//            while (resultSet.next()) {
//                sectiontask = new SectionTask();
//                sectiontask.setTaskid(resultSet.getString("TASKID"));
//                sectiontask.setTaskDes(resultSet.getString("TDES"));
//                NotAssignTaskList.add(sectiontask);
//            }
//        } catch (SQLException | NumberFormatException e) {
//            throw e;
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (Exception e) {
//                }
//            }
//            if (statement != null) {
//                try {
//                    statement.close();
//                } catch (Exception e) {
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (Exception e) {
//                }
//            }
//        }
//        return NotAssignTaskList;
//    }
}
