
package com.avn.affiniti.dao.sectiontask;

import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Sectiontask;
import com.avn.affiniti.hibernate.model.Task;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Isuru
 */
public interface SectionTaskDAO {
    
    
    public boolean insertSectionTask(Sectiontask sectiontask) throws Exception;

    public boolean deleteSectionTask(Section section) throws Exception;
    
    public long getTableDataCount(Sectiontask parameters) throws Exception;

    public List<JSONObject> getTableData(Sectiontask parameters, int minCount, int start) throws Exception;
    
    public List<JSONObject> getUrlBySectionId(int SectionId) throws Exception;
    
    public void insertAudittrace(Sectiontask sectionTask, String action, JSONArray tasks, String username) throws Exception;

//    public Map<String, String> getAssignedTask(String sectionid) throws Exception;
//
//    public Map<String, String> getNotAssignedTask(String sectionid) throws Exception;
//          
//    public List<SectionTask> insertSectionTask(SectionTask sectiontask, String username) throws Exception;
//
//    public int getTableDataCount(SectionTask parameters) throws Exception;
//
//    public List<SectionTask> getTableData(SectionTask parameters, int minCount, int start) throws Exception;
//
//    public Map<String, String> getAssignedTask(String sectionid) throws Exception;
//
//    public Map<String, String> getNotAssignedTask(String sectionid) throws Exception;
//
//    public List<SectionTask> updatSectionTask(SectionTask sectiontask, String username) throws Exception;

}
