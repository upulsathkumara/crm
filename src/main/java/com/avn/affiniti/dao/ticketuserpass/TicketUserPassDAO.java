/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.ticketuserpass;

import com.avn.affiniti.hibernate.model.Ticketuserpass;
import org.hibernate.HibernateException;

/**
 * @Author Nadun Chamikara
 * @Document TicketUserPassDAO
 * @Created on 04/01/2018, 8:48:19 AM
 */
public interface TicketUserPassDAO {
    public void createTicketUserPass(Ticketuserpass ticketuserpass) throws HibernateException;
}
