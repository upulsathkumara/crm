/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.ticketuserpass;

import com.avn.affiniti.hibernate.model.Ticketuserpass;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author Nadun Chamikara
 * @Document TicketUserPassDAOImpl
 * @Created on 04/01/2018, 8:50:18 AM
 */
@Repository("ticketUserPassDAO")
public class TicketUserPassDAOImpl implements TicketUserPassDAO{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void createTicketUserPass(Ticketuserpass ticketuserpass) throws HibernateException {
        sessionFactory.getCurrentSession().save(ticketuserpass);
    }
    
}
