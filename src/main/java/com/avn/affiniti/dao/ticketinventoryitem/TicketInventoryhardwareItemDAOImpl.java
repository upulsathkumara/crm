/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketinventoryitem;

import com.avn.affiniti.hibernate.model.Tickethistory;
import com.avn.affiniti.hibernate.model.Ticketinventoryitems;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : KAsun Udayanaga
 * @Document : TicketInventoryhardwareItemDAOImpl
 * @Created on : Oct 23, 2017, 2:56:18 PM
 */
@Repository("ticketInventoryhardwareItemDAO")
public class TicketInventoryhardwareItemDAOImpl implements TicketInventoryhardwareItemDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int createTicketInventoryhardwareItem(Ticketinventoryitems ticketinventoryitem) throws HibernateException {
        sessionFactory.getCurrentSession().save(ticketinventoryitem);
        return ticketinventoryitem.getTicketinventoryitemid();
    }

}
