/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketinventoryitem;

import com.avn.affiniti.hibernate.model.Tickethistory;
import com.avn.affiniti.hibernate.model.Ticketinventoryitems;
import org.hibernate.HibernateException;

/**
 * @Author : KAsun Udayanaga
 * @Document : TicketInventoryhardwareItemDAO
 * @Created on : Oct 23, 2017, 2:56:11 PM
 */
public interface TicketInventoryhardwareItemDAO {

    public int createTicketInventoryhardwareItem(Ticketinventoryitems ticketinventoryitem) throws HibernateException;

}
