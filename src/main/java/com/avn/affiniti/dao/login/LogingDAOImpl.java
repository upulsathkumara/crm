/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.sql.DataSource;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @Author : Office Isuru
 * @Document : LogingDAOImpl
 * @Date : Sep 22, 2015 1:41:31 PM
 */
@Repository("loginDAO")
public class LogingDAOImpl implements LoginDAO {

    @Autowired
    private DataSource dataSource;

    @Override
    public Date getLastLoginDateTime(String username) throws SQLException {
        String query = "SELECT LASTLOGIN FROM systemuser WHERE USERNAME = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{username}, Date.class);
    }

    @Override
    public void updateLastLogin(String username, Date logintime) throws SQLException {
        String query = "UPDATE systemuser SET LASTLOGIN = ? WHERE USERNAME = ?";
        new JdbcTemplate(dataSource).update(query, new Object[]{logintime, username});
    }

    @Override
    public JSONArray getAssignedTaskList(int userroleid) throws SQLException {
        String query = "SELECT ST.TASKID, ST.SECTIONID "
                + "  FROM sectiontask ST "
                + "       INNER JOIN userrolesection URT ON ST.SECTIONID = URT.SECTIONID "
                + " WHERE URT.USERROLEID = ?";
        return new JSONArray(new JdbcTemplate(dataSource).query(query, new Object[]{userroleid}, new AssignedTaskRowMapper()));
    }

    public class AssignedTaskRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int rowNum) throws SQLException {
            JSONObject object = new JSONObject();
            object.put("taskid", rs.getString("TASKID"));
            object.put("sectionid", rs.getString("SECTIONID"));
            return object;
        }
    }
}
