/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.clientproduct;

import com.avn.affiniti.hibernate.model.Clientproduct;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Nadun Chamikara
 * @Document ClientProductDAOImpl
 * @Created on 24/08/2017, 11:51:03 AM
 */
@Repository("clientProductDAO")
public class ClientProductDAOImpl implements ClientProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createOrUpdateClientProduct(Clientproduct clientproduct) throws HibernateException, Exception {
        sessionFactory.getCurrentSession().saveOrUpdate(clientproduct);
    }

    @Override
    public void deleteClientProductsByClientId(int clientId) throws Exception {
        sessionFactory.getCurrentSession().createQuery("DELETE Clientproduct WHERE client=" + clientId).executeUpdate();
    }
}
