/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



package com.avn.affiniti.dao.clientproduct;

import com.avn.affiniti.hibernate.model.Clientproduct;
import org.hibernate.HibernateException;

/**
 * @Author Nadun Chamikara
 * @Document ClientProductDAO
 * @Created on 24/08/2017, 11:49:48 AM
 */



public interface ClientProductDAO {
    public void createOrUpdateClientProduct(Clientproduct clientproduct) throws HibernateException, Exception;
    public void deleteClientProductsByClientId(int clientId) throws Exception;
}
