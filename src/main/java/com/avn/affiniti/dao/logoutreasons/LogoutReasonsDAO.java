/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.logoutreasons;

import java.util.List;

/**
 *
 * @Author : Office Isuru
 * @Document : LogoutreasonsDAO
 * @Date : Sep 28, 2015 4:31:42 PM
 */
public interface LogoutReasonsDAO {
    
     public List<String> getLogOutResons() throws Exception;

}
