/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.status;

import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Statuscategory;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : StatusDAO
 * @Created on : Jun 14, 2017, 8:58:42 AM
 */
public interface StatusDAO {

    public Status getStatsuById(int id) throws HibernateException;

    public Status getInitialStatusByTicketCategory(int ticketcategoryid) throws HibernateException;

    public Status getLatestTicketHistoryStatusByTicket(int ticketid) throws HibernateException;

    public Status getStatusByStatusCode(String statuscode) throws HibernateException;

    public List<Status> getStatusByStatusCategory(String statusCategoryId) throws SQLException;

    public String getStatusByStatusId(int statusId) throws SQLException;
    
    public void createStatus(Status status, String setSelectFunction ) throws HibernateException, Exception;
    
    //public void UpdateStatus(Status status) throws HibernateException, Exception;

    public long getTableDataCount(Statuscategory parameters) throws SQLException;

    public List<JSONObject> getTableData(Statuscategory parameters, int minCount, int start) throws HibernateException, SQLException, Exception;

     public void deleteStatus(int statusid) throws HibernateException, SQLException, Exception;
     
     public Statuscategory getStatuscategory(int id) throws HibernateException; 

     
    // public void deleteStatus(List<Status> status) throws HibernateException, SQLException, Exception;
}
