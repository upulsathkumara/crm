/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.status;

import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Statuscategory;
import com.avn.affiniti.hibernate.model.Ticketcategorystatus;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : StatusDAOImpl
 * @Created on : Jun 14, 2017, 8:58:55 AM
 */
@Repository("statusDAO")
public class StatusDAOImpl implements StatusDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Status> getStatusByStatusCategory(String statusCategoryId) throws SQLException {
        String hql = "FROM Status";
        List<Status> list = null;
        if (!statusCategoryId.equals("")) {
            try {
                int sId = Integer.parseInt(statusCategoryId);
                hql = hql.concat(" WHERE STATUCATEGORY = " + sId);
                list = sessionFactory.getCurrentSession().createQuery(hql).list();
            } catch (HibernateException | NumberFormatException ex) {
                System.out.println("******" + ex);
            }
        }
        return list;
    }

    @Override
    public void createStatus(Status status, String setSelectFunction) throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_CREATE)) {
            session.persist(status);
        } else if (setSelectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
            session.merge(status);
        }
    }

    @Override
    public String getStatusByStatusId(int statusId) throws SQLException {
        String query = "SELECT S.STATUSCODE FROM status S WHERE S.STATUSID = ?";
        return new JdbcTemplate(dataSource).queryForObject(query, new Object[]{statusId}, String.class);
    }

    @Override
    public long getTableDataCount(Statuscategory parameters) throws SQLException {

//        Session session = sessionFactory.getCurrentSession();
//        Query query;
//        if (parameters != null) {
//            query = session.createQuery("SELECT COUNT(P.productid) FROM Product P WHERE P.productcategory.productcategoryid =:prodID");
//            query.setInteger("prodID", parameters.getProductcategoryid());
//        } else {
//            query = session.createQuery("SELECT COUNT(*) FROM Product");
//        }
//
//        return (long) query.uniqueResult();
        String query = "SELECT COUNT(*) FROM status S WHERE :where";
        query = query.replace(":where", this.getWhere(parameters));
        return new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
    }

    @Override
    public List<JSONObject> getTableData(Statuscategory parameters, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT "
                + "COALESCE(S.STATUSID, 'N/A') AS STATUSID, "
                + "COALESCE(S.DESCRIPTION, 'N/A') AS DESCRIPTION, "
                + "COALESCE(S.STATUSCODE, 'N/A') AS STATUSCODE, "
                + "COALESCE(SC.DESCRIPTION, 'N/A') AS STATUSCATEGORY, "
                + "COALESCE(S.ISINITIALSTATUS, 'N/A') AS ISINITIALSTATUS, "
                + "COALESCE(S.ISFINALSTATUS, 'N/A') AS ISFINALSTATUS, "
                + "COALESCE(S.SORTID, 'N/A') AS SORTID, "
                + "COALESCE(S.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + "COALESCE(S.LASTUPDATEDDATETIME, 'N/A') AS LASTUPDATEDDATETIME, "
                + "COALESCE(S.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM status S "
                + "INNER JOIN statuscategory SC ON  S.STATUCATEGORY=SC.STATUSCATEGORYID "
                + "WHERE :where "
                + "ORDER BY LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhere(parameters));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new StatusTableViewRowMapper());

    }

    public class StatusTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("STATUSID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("STATUSID") + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("STATUSID") + "' value='delete'><i class=\"fa fa-lg fa-fw fa-trash\" title=\"Delete\"></i></a></div>"
                    + "</div>";
            object.put("action", action);
            object.put("statusid", rs.getInt("STATUSID"));
            object.put("statusdescription", rs.getString("DESCRIPTION"));
            object.put("statuscategory", rs.getString("STATUSCATEGORY"));
            object.put("sortId", rs.getString("SORTID"));
            object.put("statuscode", rs.getString("STATUSCODE"));
            object.put("lastupdatedtime", rs.getString("LASTUPDATEDDATETIME"));
            object.put("createdtime", rs.getString("CREATEDDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }
    }

    private String getWhere(Statuscategory parameters) {
        String where = "1 = 1 ";
        if (parameters != null) {
            where += "AND S.STATUCATEGORY = " + parameters.getStatuscategoryid();
        }
        return where;
    }

    @Override
    public Status getStatsuById(int id) throws HibernateException {
        Status status = new Status();
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Status S WHERE S.statusid = :statusId");
        query.setParameter("statusId", id);
        try {
            status = (Status) query.uniqueResult();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        return status;
    }

    @Override
    public Status getInitialStatusByTicketCategory(int ticketcategoryid) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Ticketcategorystatus AS TCS JOIN FETCH TCS.status AS S "
                + "WHERE S.isinitialstatus = :isinitialstatus AND TCS.ticketcategory.ticketcategoryid = :ticketcategoryid");
        query.setInteger("isinitialstatus", CommonVarList.AFFINTI_ISINITIAL_STATUS);
        query.setInteger("ticketcategoryid", ticketcategoryid);
        return ((Ticketcategorystatus) query.uniqueResult()).getStatus();
    }

    @Override
    public Status getLatestTicketHistoryStatusByTicket(int ticketid) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Status S WHERE S.statusid = ("
                + "SELECT MAX(TH.status.statusid) "
                + "FROM Tickethistory TH "
                + "INNER JOIN TH.ticket T "
                + "WHERE T.ticketid = :ticketid)");
        query.setInteger("ticketid", ticketid);
        return (Status) query.uniqueResult();
    }

    @Override
    public Status getStatusByStatusCode(String statuscode) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Status S where S.statuscode = :statuscode");
        query.setString("statuscode", statuscode);
        return (Status) query.uniqueResult();
    }

    @Override
    public void deleteStatus(int statusid) throws HibernateException, SQLException, Exception {

//            String hqlToDelete = "DELETE Status "
//                    + "WHERE statusid = " + status.getStatusid();
        Query query = sessionFactory.getCurrentSession().createSQLQuery("DELETE FROM Status WHERE statusid = :statusid");
        query.setParameter("statusid", statusid);
//            sessionFactory.getCurrentSession().createQuery(query).executeUpdate();
        query.executeUpdate();

    }

    @Override
    public Statuscategory getStatuscategory(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Statuscategory statuscategory = session.load(Statuscategory.class, id);
        Hibernate.initialize(statuscategory);
        return statuscategory;

    }

}
