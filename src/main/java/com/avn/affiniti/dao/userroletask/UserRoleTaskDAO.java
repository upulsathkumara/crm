/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userroletask;


import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.hibernate.model.Userroletask;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 * @Author Kaushan Fernando
 * @Document UserRoleTaskDAO
 * @Created on 10/08/2017, 8:50:15 AM
 */
public interface UserRoleTaskDAO {

    public List<Userrolesubsection> getUserRoleSubSections(int userRoleId) throws HibernateException, SQLException;
  
    public List<JSONObject> getTableData(String userRoleId, int minCount, int start) throws HibernateException, SQLException, Exception ;
    
    public long getTableDataCount(String userRoleId) throws HibernateException, SQLException, Exception ;

    public JSONObject saveUserRoleTask(Userroletask userRoleTaskToSave) throws HibernateException, SQLException;

    public int findUserroleSubSectionId(int userRoleId, int sectionId, int subSectionId) throws HibernateException, SQLException;

    public String findTaskDescriptionByTaskId(int taskId) throws SQLException;
            
    public List<Userroletask> getOldUserRleTaskObject(int userRoleSubSectionId) throws HibernateException, SQLException;

    public void deleteUserroletask(int userRoleSubSectionId,int taskId) throws HibernateException, SQLException;
    
    public long checkUserRoleSubSectionTaskAvailability(int userRoleSubSectionId,int taskId) throws HibernateException, SQLException,Exception;

    
}
