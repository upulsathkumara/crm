package com.avn.affiniti.dao.userroletask;

import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.hibernate.model.Userroletask;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author Kaushan Fernando
 * @Document UserRoleSectionDAOImpl
 * @Created on 10/08/2017, 8:50:31 AM
 */
@Repository("userRoleTaskDAO")
public class UserRoleTaskDAOImpl implements UserRoleTaskDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Userrolesubsection> getUserRoleSubSections(int userRoleId) throws HibernateException, SQLException {
        String hql = "FROM Userrolesubsection urs "
                + "WHERE userroleid = " + userRoleId;

        List<Userrolesubsection> list = sessionFactory.getCurrentSession().createQuery(hql).list();

        return list;
    }

    @Override
    public List<JSONObject> getTableData(String userRoleId, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT COALESCE(URT.CREATEDDATETIME , 'N/A') AS CREATED_DATETIME, COALESCE(URT.LASTUPDATEDDATETIME , 'N/A') AS UPDATED_DATETIME , COALESCE(URT.CREATEDUSER , 'N/A') AS CREATED_USER, "
                + "COALESCE(T.DESCRIPTION , 'N/A') AS TASK_DESCRIPTION, "
                + "COALESCE(S.DESCRIPTION , 'N/A') AS SECTION_DESCRIPTION, "
                + "S.SECTIONID AS SECTIONID, "
                + "COALESCE(SUBSEC.DESCRIPTION , 'N/A') AS SUBSECTION_DESCRIPTION, "
                + "SUBSEC.SUBSECTIONID AS SUBSECTIONID, "
                + "USR.USERROLEID AS USERROLEID, "
                + "COALESCE(USR.DESCRIPTION , 'N/A') AS USERROLE_DESCRIPTION "
                + "FROM userroletask URT INNER JOIN  userrolesubsection URSS ON URSS.USERROLESUBSECTIONID = URT.USERROLESUBSECTIONID "
                + "INNER JOIN  task T ON T.TASKID = URT.TASKID "
                + "INNER JOIN  section S ON S.SECTIONID = URSS.SECTIONID "
                + "INNER JOIN  subsection SUBSEC ON  SUBSEC.SUBSECTIONID = URSS.SUBSECTIONID "
                + "INNER JOIN  userrole USR ON  USR.USERROLEID = URSS.USERROLEID "
                + "WHERE :where "
                + "ORDER BY URT.LASTUPDATEDDATETIME DESC LIMIT ?,?";

        String where = this.getWhere(userRoleId);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new UserRoleTaskTableViewRowMapper());

    }

    @Override
    public long getTableDataCount(String userRoleId) throws HibernateException, SQLException, Exception {

        String query = "SELECT COUNT(*) FROM userroletask URT WHERE URT.USERROLESUBSECTIONID IN "
                + "(SELECT URSS.USERROLESUBSECTIONID FROM userrolesubsection URSS WHERE :where)";

        String where = this.getWhere(userRoleId);
        query = query.replace(":where", where);

        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    private String getWhere(String userRoleId) {

        String where;

        if (userRoleId.equals("") || userRoleId.equals("--Select--")) {
            where = "1 = 1";
        } else {
            int userRoleid = Integer.parseInt(userRoleId);
            where = "URSS.USERROLEID LIKE " + userRoleid;

        }

        return where;
    }

    @Override
    public JSONObject saveUserRoleTask(Userroletask userRoleTaskToSave) throws HibernateException, SQLException {
        JSONObject object = new JSONObject();
        boolean status = true;
        try {

            Session session = sessionFactory.getCurrentSession();

            session.saveOrUpdate(userRoleTaskToSave);

        } catch (Exception e) {
            object.put("message", MessageVarList.ACCESS_DENIED);
            status = false;
            throw e;
        }

        object.put("status", status);

        return object;

    }

    @Override
    public long checkUserRoleSubSectionTaskAvailability(int userRoleSubSectionId, int taskId) throws HibernateException, SQLException, Exception {

        String query = "SELECT COUNT(*) FROM userroletask URT WHERE URT.USERROLESUBSECTIONID = " + userRoleSubSectionId
                + " AND URT.TASKID = " + taskId;

        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    @Override
    public int findUserroleSubSectionId(int userRoleId, int sectionId, int subSectionId) throws HibernateException, SQLException {
        int userRoleSubSectionId;

        String hqlToSaveAndUpdate = "SELECT userrolesubsectionid FROM Userrolesubsection "
                + "WHERE userroleid = " + userRoleId
                + " AND sectionid = " + sectionId
                + " AND subsectionid = " + subSectionId;
        Session session = sessionFactory.getCurrentSession();
        userRoleSubSectionId = ((Integer) session.createQuery(hqlToSaveAndUpdate).uniqueResult());

        return userRoleSubSectionId;
    }

    @Override
    public void deleteUserroletask(int userRoleSubSectionId, int taskId) throws HibernateException, SQLException {
        String hqlToDelete = "DELETE Userroletask "
                + "WHERE userrolesubsectionid = " + userRoleSubSectionId
                + " AND taskid = " + taskId;
        Session session = sessionFactory.getCurrentSession();
        session.createQuery(hqlToDelete).executeUpdate();
    }

    @Override
    public List<Userroletask> getOldUserRleTaskObject(int userRoleSubSectionId) throws HibernateException, SQLException {

        String hql = "FROM Userroletask "
                + "WHERE userrolesubsectionid = " + userRoleSubSectionId;

        List<Userroletask> listOfOldObject = sessionFactory.getCurrentSession().createQuery(hql).list();

        return listOfOldObject;

    }

    @Override
    public String findTaskDescriptionByTaskId(int taskId) throws SQLException {
        Session session = sessionFactory.getCurrentSession();
        String taskDescription = "";

        String hql = "SELECT T.description "
                + "FROM Task T "
                + "WHERE T.taskid = :taskid ";

        Query query = session.createQuery(hql);

        query.setInteger("taskid", taskId);

        taskDescription = (String) query.uniqueResult();

        return taskDescription;

    }

    public class UserRoleTaskTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();
            String actions = rs.getInt("USERROLEID") + ":" + rs.getInt("SECTIONID") + ":" + rs.getInt("SUBSECTIONID");

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("section", rs.getString("SECTION_DESCRIPTION"));
            object.put("subsection", rs.getString("SUBSECTION_DESCRIPTION"));
            object.put("userrole", rs.getString("USERROLE_DESCRIPTION"));
            object.put("task", rs.getString("TASK_DESCRIPTION"));
            object.put("lastupdatedtime", rs.getString("UPDATED_DATETIME"));
            object.put("createdtime", rs.getString("CREATED_DATETIME"));
            object.put("createduser", rs.getString("CREATED_USER"));
            object.put("action", action);

            return object;
        }
    }

   

}
