/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.userrolesectiontask;

import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.hibernate.model.Userrolesectiontask;
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.hibernate.model.Userroletask;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author :Kaushan Fernando
 *@Document :UserRoleSectionDAO
 *@Created on:Oct 13, 2017,2:48:47 PM
 */
public interface UserRoleSectionTaskDAO {

    public List<Userrolesection> getUserRoleSections(int userRoleId) throws HibernateException, SQLException;
  
    public List<JSONObject> getTableData(String userRoleId, int minCount, int start) throws HibernateException, SQLException, Exception ;
    
    public long getTableDataCount(String userRoleId) throws HibernateException, SQLException, Exception ;

    public void saveUserRoleSectionTask(Userrolesectiontask userRoleSectionTaskToSave) throws HibernateException, SQLException;

    public int findUserroleSectionId(int userRoleId, int sectionId) throws HibernateException, SQLException;

    public List<Userrolesectiontask> getOldUserRoleTaskObject(int userRoleSectionId) throws HibernateException, SQLException;

    public void deleteUserroletask(int userRoleSubSectionId,int taskId) throws HibernateException, SQLException;

    public long checkUserRoleSectionTaskAvailability(int userRoleSectionId,int taskId) throws HibernateException, SQLException,Exception;
}
