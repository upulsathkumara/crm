/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrolesectiontask;

import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.hibernate.model.Userrolesectiontask;
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.hibernate.model.Userroletask;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author :Kaushan Fernando
 * @Document :UserRoleSectionTaskDAOImpl
 * @Created on:Oct 13, 2017,2:51:54 PM
 */
@Repository("userRoleSectionTaskDAO")
public class UserRoleSectionTaskDAOImpl implements UserRoleSectionTaskDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Userrolesection> getUserRoleSections(int userRoleId) throws HibernateException, SQLException {
        String hql = "FROM Userrolesection urs "
                + "WHERE userroleid = " + userRoleId;

        List<Userrolesection> list = sessionFactory.getCurrentSession().createQuery(hql).list();

        return list;
    }

    @Override
    public List<JSONObject> getTableData(String userRoleId, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT COALESCE(URT.CREATEDDATETIME , 'N/A') AS CREATED_DATETIME, COALESCE(URT.LASTUPDATEDDATETIME , 'N/A') AS UPDATED_DATETIME , COALESCE(URT.CREATEDUSER , 'N/A') AS CREATED_USER, "
                + "COALESCE(T.DESCRIPTION , 'N/A') AS TASK_DESCRIPTION, "
                + "COALESCE(S.DESCRIPTION , 'N/A') AS SECTION_DESCRIPTION, "
                + "S.SECTIONID AS SECTIONID, "
                + "USR.USERROLEID AS USERROLEID, "
                + "COALESCE(USR.DESCRIPTION , 'N/A') AS USERROLE_DESCRIPTION "
                + "FROM userrolesectiontask URT INNER JOIN  userrolesection URSS ON URSS.USERROLESECTIONID = URT.USERROLESECTIONID "
                + "INNER JOIN  task T ON T.TASKID = URT.TASKID "
                + "INNER JOIN  section S ON S.SECTIONID = URSS.SECTIONID "
                + "INNER JOIN  userrole USR ON  USR.USERROLEID = URSS.USERROLEID "
                + "WHERE :where "
                + "ORDER BY URT.LASTUPDATEDDATETIME DESC LIMIT ?,?";

        String where = this.getWhere(userRoleId);
        query = query.replace(":where", where);

        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new UserRoleTaskTableViewRowMapper());

    }

    @Override
    public long getTableDataCount(String userRoleId) throws HibernateException, SQLException, Exception {

        String query = "SELECT COUNT(*) FROM userrolesectiontask URT WHERE URT.USERROLESECTIONID IN "
                + "(SELECT URSS.USERROLESECTIONID FROM userrolesection URSS WHERE :where)";

        String where = this.getWhere(userRoleId);
        query = query.replace(":where", where);

        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    private String getWhere(String userRoleId) {

        String where;

        if (userRoleId.equals("") || userRoleId.equals("--Select--")) {
            where = "1 = 1";
        } else {
            int userRoleid = Integer.parseInt(userRoleId);
            where = "URSS.USERROLEID LIKE " + userRoleid;

        }

        return where;
    }

    @Override
    public void saveUserRoleSectionTask(Userrolesectiontask userRoleSectionTaskToSave) throws HibernateException, SQLException {

        Session session = sessionFactory.getCurrentSession();

        session.saveOrUpdate(userRoleSectionTaskToSave);

    }

    @Override
    public long checkUserRoleSectionTaskAvailability(int userRoleSectionId, int taskId) throws HibernateException, SQLException, Exception {

        String query = "SELECT COUNT(*) FROM userrolesectiontask URT WHERE URT.USERROLESECTIONID = " + userRoleSectionId
                + " AND URT.TASKID = " + taskId;

        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

        return count;
    }

    @Override
    public int findUserroleSectionId(int userRoleId, int sectionId) throws HibernateException, SQLException {
        int userRoleSectionId;

        String hqlToSaveAndUpdate = "SELECT userrolesectionid FROM Userrolesection "
                + "WHERE userroleid = " + userRoleId
                + " AND sectionid = " + sectionId;
        Session session = sessionFactory.getCurrentSession();
        userRoleSectionId = ((Integer) session.createQuery(hqlToSaveAndUpdate).uniqueResult());

        return userRoleSectionId;
    }

    @Override
    public void deleteUserroletask(int userRoleSectionId, int taskId) throws HibernateException, SQLException {
        String hqlToDelete = "DELETE Userrolesectiontask "
                + "WHERE userrolesectionid = " + userRoleSectionId
                + " AND taskid = " + taskId;

        Session session = sessionFactory.getCurrentSession();

        session.createQuery(hqlToDelete).executeUpdate();

    }

    @Override
    public List<Userrolesectiontask> getOldUserRoleTaskObject(int userRoleSectionId) throws HibernateException, SQLException {

        String hql = "FROM Userrolesectiontask "
                + "WHERE userrolesectionid = " + userRoleSectionId;

        List<Userrolesectiontask> listOfOldObject = sessionFactory.getCurrentSession().createQuery(hql).list();

        return listOfOldObject;

    }

    public class UserRoleTaskTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();
            String actions = rs.getInt("USERROLEID") + ":" + rs.getInt("SECTIONID");

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + actions + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";

            object.put("section", rs.getString("SECTION_DESCRIPTION"));
            object.put("userrole", rs.getString("USERROLE_DESCRIPTION"));
            object.put("task", rs.getString("TASK_DESCRIPTION"));
            object.put("lastupdatedtime", rs.getString("UPDATED_DATETIME"));
            object.put("createdtime", rs.getString("CREATED_DATETIME"));
            object.put("createduser", rs.getString("CREATED_USER"));
            object.put("action", action);

            return object;
        }
    }

}
