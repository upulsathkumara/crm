/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrolesection;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userrolesection;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author Nadun Chamikara
 * @Document UserRoleSectionDAOImpl
 * @Created on 10/08/2017, 8:50:31 AM
 */
@Repository("userRoleSectionDAO")
public class UserRoleSectionDAOImpl implements UserRoleSectionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public long getUserRoleSectionsCount(String userRoleId, String sectionId) throws HibernateException, Exception {
        String hql = "SELECT COUNT(*) FROM Userrolesection";

        if (!userRoleId.equals("") && !sectionId.equals("")) {
            int uid = Integer.parseInt(String.valueOf(userRoleId));
            int sid = Integer.parseInt(String.valueOf(sectionId));
            hql = hql.concat(" WHERE userrole.userroleid = " + uid + " AND section.sectionid = " + sid);
        } else if (!userRoleId.equals("")) {
            int uid = Integer.parseInt(String.valueOf(userRoleId));
            hql = hql.concat(" WHERE userrole.userroleid = " + uid);
        } else if (!sectionId.equals("")) {
            int sid = Integer.parseInt(String.valueOf(sectionId));
            hql = hql.concat(" WHERE section.sectionid = " + sid);
        }

        return (long) sessionFactory.getCurrentSession().createQuery(hql).uniqueResult();

    }

    @Override
    public List<Userrolesection> getUserRoleSections(String userRoleId, String sectionId, int start, int minCount) throws HibernateException, Exception {
        String hql = "FROM Userrolesection";

        Query query;
        // hql = "FROM Userrolesection WHERE USERROLEID = :userRoleId AND SECTIONID = :sectionId";

//            if (userRoleId != 0 && sectionId != 0) {
//                hql = hql.concat(" WHERE USERROLEID = " + userRoleId + " AND SECTIONID = " + sectionId);
//            } else if (userRoleId != 0) {
//                hql = hql.concat(" WHERE USERROLEID = " + userRoleId);
//            } else if (sectionId != 0) {
//                hql = hql.concat(" WHERE SECTIONID = " + sectionId);
//            }
        if (!userRoleId.equals("") && !sectionId.equals("")) {
            int uid = Integer.parseInt(String.valueOf(userRoleId));
            int sid = Integer.parseInt(String.valueOf(sectionId));
            hql = hql.concat(" WHERE userrole.userroleid = " + uid + " AND section.sectionid = " + sid);
        } else if (!userRoleId.equals("")) {
            int uid = Integer.parseInt(String.valueOf(userRoleId));
            hql = hql.concat(" WHERE userrole.userroleid = " + uid);
        } else if (!sectionId.equals("")) {
            int sid = Integer.parseInt(String.valueOf(sectionId));
            hql = hql.concat(" WHERE section.sectionid = " + sid);
        }
        hql = hql.concat(" ORDER BY createddatetime DESC");

        query = sessionFactory.getCurrentSession().createQuery(hql).setFirstResult(start);
        if (minCount != 0) {
            query.setMaxResults(minCount);
        }

        return query.list();
    }

    @Override
    public void createUserRoleSection(Userrolesection userrolesection) throws HibernateException, Exception {
        Session session = sessionFactory.getCurrentSession();
        //session.evict(userrolesection);
        session.save(userrolesection);
    }

    @Override
    public void deleteUserRoleSectionsByUserRoleIdAndSectionId(int userRoleId, int sectionId) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("DELETE Userrolesection URS WHERE URS.userrole.userroleid = :userroleid "
                + "AND URS.section.sectionid = :sectionid");
        query.setInteger("userroleid", userRoleId);
        query.setInteger("sectionid", sectionId);
        query.executeUpdate();
    }

    @Override
    public int findUserRoleSectionIdByUserRoleIdAndSectionId(int oldUuserRoleId, int oldSectionId) throws HibernateException, Exception {
        int userRoleSectionId;

        String hql = "SELECT userrolesectionid FROM Userrolesection "
                + "WHERE userroleid = " + oldUuserRoleId
                + " AND sectionid = " + oldSectionId;

        Session session = sessionFactory.getCurrentSession();
        userRoleSectionId = ((Integer) session.createQuery(hql).uniqueResult());

        return userRoleSectionId;
    }

    @Override
    public void deleteUserRoleSectionTaskByUserRoleSectionId(int userRoleSectionId) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("DELETE Userrolesectiontask URST WHERE URST.id.userrolesectionid = :userrolesectionid ");
        query.setInteger("userrolesectionid", userRoleSectionId);

        query.executeUpdate();
    }

    @Override
    public Userrole getUserRoleId(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Userrole userrole = session.load(Userrole.class, id);
        Hibernate.initialize(userrole);
        return userrole;

    }

    @Override
    public Section getSectionId(int id) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        Section section = session.load(Section.class, id);
        Hibernate.initialize(section);
        return section;

    }

}
