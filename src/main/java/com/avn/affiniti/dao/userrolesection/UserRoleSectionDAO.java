/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.userrolesection;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userrolesection;
import java.util.List;
import org.hibernate.HibernateException;

/**
 * @Author Nadun Chamikara
 * @Document UserRoleSectionDAO
 * @Created on 10/08/2017, 8:50:15 AM
 */
public interface UserRoleSectionDAO {

    public long getUserRoleSectionsCount(String userRoleId, String sectionId) throws HibernateException, Exception;

    public List<Userrolesection> getUserRoleSections(String userRoleId, String sectionId, int start, int minCount) throws HibernateException, Exception;

    public void createUserRoleSection(Userrolesection userrolesection) throws HibernateException, Exception;

    public int findUserRoleSectionIdByUserRoleIdAndSectionId(int oldUuserRoleId, int oldSectionId) throws HibernateException, Exception;

    public void deleteUserRoleSectionsByUserRoleIdAndSectionId(int userRoleId, int sectionId) throws HibernateException;

    public void deleteUserRoleSectionTaskByUserRoleSectionId(int userRoleSectionId) throws HibernateException;

    public Userrole getUserRoleId(int id) throws HibernateException;
    
    public Section getSectionId(int id) throws HibernateException;
}
