/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticketcategorycontent;

import com.avn.affiniti.hibernate.model.Ticketcatgorycontent;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketCategoryContentDAOImpl
 * @Created on : Jun 3, 2017, 11:51:43 AM
 */
@Repository("ticketCategoryContentDAO")
public class TicketCategoryContentDAOImpl implements TicketCategoryContentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Ticketcatgorycontent getTicketCategoryContentByTicketCategory(int ticketcategory, String state) throws HibernateException {
        return (Ticketcatgorycontent) sessionFactory.getCurrentSession()
                .createQuery("FROM Ticketcatgorycontent "
                        + "WHERE ticketcategory.ticketcategoryid = :ticketcategory "
                        + "AND state = :state")
                .setInteger("ticketcategory", ticketcategory)
                .setString("state", state)
                .uniqueResult();
    }

}
