/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.dao.ticketcategorycontent;

import com.avn.affiniti.hibernate.model.Ticketcatgorycontent;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketCategoryContentDAO
 * @Created on : Jun 3, 2017, 11:51:24 AM
 */
public interface TicketCategoryContentDAO {
    
    public Ticketcatgorycontent getTicketCategoryContentByTicketCategory(int ticketcategory, String state) throws HibernateException;

}
