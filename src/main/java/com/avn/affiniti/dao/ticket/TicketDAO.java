/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticket;

import com.avn.affiniti.hibernate.model.Bulkterminallot;
import com.avn.affiniti.hibernate.model.Location;
import com.avn.affiniti.hibernate.model.Remindernotification;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.hibernate.model.Tickethistory;
import com.avn.affiniti.model.ticket.BackupRemovalTicket;
import com.avn.affiniti.model.ticket.BaseSoftwareInstallationTicket;
import com.avn.affiniti.model.ticket.MaintainancePaymentInvoiceTicket;
import com.avn.affiniti.model.ticket.MerchantRemovalTicket;
import com.avn.affiniti.model.ticket.NewInstallationTicke;
import com.avn.affiniti.model.ticket.ReinitializationTicket;
import com.avn.affiniti.model.ticket.SoftwareHardwareBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalConversionTicket;
import com.avn.affiniti.model.ticket.TerminalRepairMysTicket;
import com.avn.affiniti.model.ticket.TerminalRepairTicket;
import com.avn.affiniti.model.ticket.TerminalSharingTicket;
import com.avn.affiniti.model.ticket.TerminalUpgradeDowngradeTicket;
import com.avn.affiniti.model.ticket.TicketParam;
import com.avn.affiniti.model.tickethistory.TicketHistory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketDAO
 * @Created on : May 2, 2017, 12:08:24 PM
 */
public interface TicketDAO {

    public int createTicket(Ticket ticket) throws HibernateException;

    public Ticket getTicket(int ticketid) throws HibernateException;

    public Tickethistory getTicketHistoryMaxDataByTicketId(int ticketid) throws SQLException;

    public TerminalBreakdownTicket getTerminalBreakdownTicket(int ticketid) throws SQLException;

    public NewInstallationTicke getNewInstallationTicket(int ticketid) throws SQLException;

    public SoftwareHardwareBreakdownTicket getSoftwareHardwareTicketById(int ticketid) throws SQLException;

    public MerchantRemovalTicket getMerchantRemovalTicketById(int ticketid) throws SQLException;

    public BaseSoftwareInstallationTicket getBaseSoftwareInstallationTicketById(int ticketid) throws SQLException;

    public TerminalUpgradeDowngradeTicket getTerminalUpgradeDowngradeTicketById(int ticketid) throws SQLException;

    public BackupRemovalTicket getBackupRemovalTicketById(int ticketid) throws SQLException;

    public TerminalSharingTicket getTerminalSharingTicketById(int ticketid) throws SQLException;

    public TerminalConversionTicket getTerminalConversionTicketById(int ticketid) throws SQLException;

    public ReinitializationTicket getReinitializationTicketById(int ticketid) throws SQLException;

    public MaintainancePaymentInvoiceTicket getMaintainancePaymentInvoiceTicketById(int ticketid) throws SQLException;

    public TerminalBreakdownTicket getMalaysianTerminalBreakdownTicketById(int ticketid) throws SQLException;

    public TerminalRepairMysTicket getMalaysianTerminalRepairTicketById(int ticketid) throws SQLException;

    public void updateTicket(Ticket ticket) throws HibernateException;

    public void updateTicketHistory(Tickethistory ticketHistory) throws HibernateException;

    public JSONArray getTicketTerminalAssignmentData(String user, String contextpath, int strat, int length) throws SQLException;

    public TerminalRepairTicket getTerminalRepairTicket(int ticketid) throws SQLException;

    public String getAssigneeByTicketId(int ticketid) throws HibernateException, SQLException;

    public int getTableCount(TicketParam ticket) throws SQLException;

    public List<TicketParam> getTableData(TicketParam ticket, int start, int length) throws SQLException;

    public JSONObject insertFileMasterData(Bulkterminallot bulkterminallot) throws SQLException;

    public void insertBulkTerminalBreakdown(Ticket ticket) throws SQLException;

    public long getTicketTableDataCount(TicketParam ticket, Systemuser systemuser) throws HibernateException, SQLException, Exception;

    public List<JSONObject> getTicketTableData(TicketParam ticket, Systemuser systemuser, int minCount, int start) throws SQLException;

    public long getTerminalTableDataCount(String tickeId, String username) throws HibernateException, SQLException, Exception;

    public List<JSONObject> getTerminalTableData(String ticketId, String username, int minCount, int start) throws SQLException;

    public String getStatusCodeByTicketId(int ticktId) throws SQLException;

    public long getTicketTableDataCountByStatus(ArrayList statusId, Systemuser systemuser) throws SQLException;

    public List<JSONObject> getTicketTableDataByStatus(int minCount, int start, ArrayList statusId) throws SQLException;

    public JSONObject getTerminalInfo(String input) throws SQLException;

    public long getTickeHistoryDataCount(TicketParam ticket) throws HibernateException, SQLException, Exception;

    public List<TicketHistory> getTickeHistoryData(TicketParam ticket, int minCount, int start) throws HibernateException, SQLException, Exception;

    public Boolean validateSerialNumber(String serialNumber) throws SQLException;

    public void createReminderNotification(Remindernotification remindernotification) throws SQLException;
}
