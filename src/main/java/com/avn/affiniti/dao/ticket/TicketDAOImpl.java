/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.ticket;

import com.avn.affiniti.dao.employee.EmployeeDAO;
import com.avn.affiniti.dao.inventory.InventoryDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.hibernate.model.Bulkterminallot;
import com.avn.affiniti.hibernate.model.Remindernotification;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.hibernate.model.Tickethistory;
import com.avn.affiniti.model.ticket.BackupRemovalTicket;
import com.avn.affiniti.model.ticket.BaseSoftwareInstallationTicket;
import com.avn.affiniti.model.ticket.MaintainancePaymentInvoiceTicket;
import com.avn.affiniti.model.ticket.MerchantRemovalTicket;
import com.avn.affiniti.model.ticket.NewInstallationTicke;
import com.avn.affiniti.model.ticket.ReinitializationTicket;
import com.avn.affiniti.model.ticket.SoftwareHardwareBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalConversionTicket;
import com.avn.affiniti.model.ticket.TerminalRepairMysTicket;
import com.avn.affiniti.model.ticket.TerminalRepairTicket;
import com.avn.affiniti.model.ticket.TerminalSharingTicket;
import com.avn.affiniti.model.ticket.TerminalUpgradeDowngradeTicket;
import com.avn.affiniti.model.ticket.TicketParam;
import com.avn.affiniti.model.tickethistory.TicketHistory;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketDAOImpl
 * @Created on : May 2, 2017, 12:09:04 PM
 */
@Repository("ticketDAO")
public class TicketDAOImpl implements TicketDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private DataSource msSqldataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private SystemuserDAO systemuserDAO;

    @Autowired
    private InventoryDAO inventoryDAO;

    @Autowired
    private EmployeeDAO employeeDAO;

    @Override
    public int createTicket(Ticket ticket) throws HibernateException {
        sessionFactory.getCurrentSession().save(ticket);
        return ticket.getTicketid();
    }

    @Override
    public Ticket getTicket(int ticketid) throws HibernateException {
        return sessionFactory.getCurrentSession().get(Ticket.class, ticketid);
    }

    @Override
    public TerminalBreakdownTicket getTerminalBreakdownTicket(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(T.PARTNO, '')                   AS PARTNO, "
                + "       IFNULL(T.REVISION, '')                 AS REVISION, "
                + "       IFNULL(T.MAC, '')                      AS MAC, "
                + "       IFNULL(T.PTID, '')                     AS PTID, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(T.CREATEDBY, '')                AS CREATEDBY, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(S.DESCRIPTION, '')              AS STATUS, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (TerminalBreakdownTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalBreakdownTicket.class));
    }

    @Override
    public SoftwareHardwareBreakdownTicket getSoftwareHardwareTicketById(int ticketid) throws SQLException {

        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       T.TERMINALSERIALNO                     AS TERMINALSERIALNO, "
                + "       TB.DESCRIPTION                         AS TERMINALBRNAD, "
                + "       TMO.DESCRIPTION                        AS TERMINALMODEL, "
                + "       T.WARRANTYEXPIRYSTATUS                 AS WARRANTYEXPIRYSTATUS, "
                + "       T.AMCAEXPIRYSTATUS                     AS AMCEXPIRYSTATUS, "
                + "       T.MERCHANTNAME                         AS MERCHANTNAME, "
                + "       T.CONTACTPERSION                       AS CONTACTPERSON, "
                + "       T.CONTACTNO                            AS CONTACTNO, "
                + "       T.LOCATIONADDRESS                      AS LOCATIONADDRESS, "
                + "       T.TECHNICALOFFICERINCHARGE             AS TECHNICALOFFICERINCHARGE, "
                + "       T.REPORTEDBY                           AS REPORTEDBY, "
                + "       IFNULL(T.CREATEDBY, '')                 AS CREATEDBY, "
                + "       IFNULL(T.ADDITIONALCOSTSINVOLVED, '')  AS ADITIONALCOSTS, "
                + "       IFNULL(T.BACKUPDEVICESERIAL, '')       AS BACKUPDEVICESERIAL, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(DI.DESCRIPTION, '')             AS DISTRICT, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "     LEFT OUTER JOIN district DI ON T.DISTRICT = DI.DISTRICTID "
                + "WHERE T.TICKETID = ?";

        return (SoftwareHardwareBreakdownTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(SoftwareHardwareBreakdownTicket.class));

    }

    @Override
    public MerchantRemovalTicket getMerchantRemovalTicketById(int ticketid) throws SQLException {

        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (MerchantRemovalTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(MerchantRemovalTicket.class));

    }

    @Override
    public BaseSoftwareInstallationTicket getBaseSoftwareInstallationTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.BOXNUMBER, '')                AS BOXNO, "
                + "       IFNULL(T.STARTSERIALNO, '')            AS STARTSERIALNO, "
                + "       IFNULL(T.ENDSERIALNO, '')              AS ENDSERIALNO, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (BaseSoftwareInstallationTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(BaseSoftwareInstallationTicket.class));

    }

    @Override
    public TerminalUpgradeDowngradeTicket getTerminalUpgradeDowngradeTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (TerminalUpgradeDowngradeTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalUpgradeDowngradeTicket.class));
    }

    @Override
    public BackupRemovalTicket getBackupRemovalTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(T.CONTACTPERSION, '')             AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                 AS CONTACTNO, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.REMARKS, '')                  AS REMARK, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.CURRENCY, '')                 AS CURRENCY, "
                + "       IFNULL(T.DESCRIPTION, '')              AS DESCRIPTION "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (BackupRemovalTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(BackupRemovalTicket.class));
    }

    @Override
    public TerminalSharingTicket getTerminalSharingTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.VMTID, '')                    AS VMTID, "
                + "       IFNULL(T.VMMID, '')                    AS VMMID, "
                + "       IFNULL(T.REMARKS, '')                  AS REMARK, "
                + "       IFNULL(T.AMEXTID, '')                  AS AMEXTID, "
                + "       IFNULL(T.AMEXMID, '')                  AS AMEXMID, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.CURRENCY, '')                 AS CURRENCY, "
                + "       IFNULL(T.DESCRIPTION, '')              AS DESCRIPTION, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (TerminalSharingTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalSharingTicket.class));
    }

    @Override
    public TerminalConversionTicket getTerminalConversionTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.NEWSOFTWAREVERSION, '')       AS NEWSOFTWARVERSION, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "WHERE T.TICKETID = ?";
        return (TerminalConversionTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalConversionTicket.class));
    }

    @Override
    public ReinitializationTicket getReinitializationTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.NEWSOFTWAREVERSION, '')       AS NEWSOFTWAREVERSION, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (ReinitializationTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(ReinitializationTicket.class));
    }

    @Override
    public MaintainancePaymentInvoiceTicket getMaintainancePaymentInvoiceTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = C.CLIENTCATEGORY "
                + "WHERE T.TICKETID = ?";
        return (MaintainancePaymentInvoiceTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(MaintainancePaymentInvoiceTicket.class));
    }

    @Override
    public TerminalBreakdownTicket getMalaysianTerminalBreakdownTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                //                + "        ON CC.CLIENTCATEGORYID = C.CLIENTCATEGORY "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "WHERE T.TICKETID = ?";
        return (TerminalBreakdownTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalBreakdownTicket.class));
    }

    @Override
    public TerminalRepairMysTicket getMalaysianTerminalRepairTicketById(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                 AS TICKETID, "
                + "IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "IFNULL(T.DAMAGEPARTDESCRIPTION, '')    AS DAMAGEPARTDESCRIPTION, "
                + "IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "IFNULL(T.REPLACEDPART, '')             AS REPLACEDPARTS, "
                + "IFNULL(IC.DESCRIPTION, '')             AS PARTTYPE, "
                + "IFNULL(T.REVISION, '')             	  AS REVISION, "
                + "IFNULL(T.MAC, '')             	  AS MAC, "
                + "IFNULL(T.PTID, '')             	  AS PTID, "
                + "IFNULL(T.COMPLETIONDATETIME, '')       AS COMPLETIONDATETIME, "
                + "IFNULL(T.TOTLETIMESPENT, '')       	  AS TOTALTIMESPENT, "
                + "IFNULL(T.SPECIALREMARK, '')       	  AS SPECIALREMARK, "
                + "IFNULL(T.DELIVERYORDERREF, '')         AS DELIVERYORDERREFERENCE, "
                + "IFNULL(T.RMAREFERANCE, '')         	  AS RMAREFERANCE, "
                + "IFNULL(T.DISPATCHDATE, '')         	  AS DISPATCHDATE, "
                + "IFNULL(T.COURIERSERVICEPROVIDER, '')   AS COURIERSERVICEPROVIDER, "
                + "IFNULL(T.TRACKINGNUMBER, '')   	  AS TACKINGNUMBER, "
                + "IFNULL(T.DESTINATION, '')   		  AS DESTINATION, "
                + "IFNULL(T.CONTACTPERSION, '')   	  AS CONTACTPERSON, "
                + "IFNULL(T.CONTACTNO, '')   		  AS CONTACTNUMBER, "
                + "IFNULL(D.DESCRIPTION, '')   		  AS DISTRICT, "
                + "IFNULL(T.LOCATIONADDRESS, '')   	  AS LOCATIONADDRESS, "
                + "IFNULL(DR.DESCRIPTION, '')             AS DELIVERYREQUIREMENT, "
                + "IFNULL(T.DELEVERYDESTINATION, '')      AS DELIVERYDESTINATION, "
                + "IFNULL(T.INVOICENUMBER, '')      	  AS INVOICENUMBER, "
                + "IFNULL(PT.DESCRIPTION, '')      	  AS PURPOSE, "
                + "IFNULL(T.INVOICEVALUE, '')      	  AS INVOICEVALUE, "
                + "IFNULL(T.INVOICEDATE, '')      	  AS INVOICEDATE, "
                + "IFNULL(T.DESCRIPTION, '')      	  AS DESCRIPTION, "
                + "IFNULL(T.BKPLOCATIONADDRESS, '')       AS BKPLOCATIONADDRESS "
                + "FROM ticket T "
                + "LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "LEFT OUTER JOIN productcategory PC ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "LEFT OUTER JOIN ticketcategory TC ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "LEFT OUTER JOIN terminalbrand TB ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "LEFT OUTER JOIN terminalmodel TMO ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "LEFT OUTER JOIN deleveryrequirement DR ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "LEFT OUTER JOIN natureoffault NOF ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "LEFT OUTER JOIN actiontobetaken ATBT ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "LEFT OUTER JOIN causeoffault COF ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "LEFT OUTER JOIN inventoryterminalitem ITMI ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "LEFT OUTER JOIN terminalbrand BTB ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "LEFT OUTER JOIN terminalmodel BTMO ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "LEFT OUTER JOIN inventorycategory IC ON IC.INVENTORYCATEGORYID = T.PARTTYPE "
                + "LEFT OUTER JOIN purposetype PT ON PT.PURPOSETYPEID = T.PURPOSETYPE "
                + "WHERE T.TICKETID = ?";
        return (TerminalRepairMysTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalRepairMysTicket.class));
    }

    @Override
    public NewInstallationTicke getNewInstallationTicket(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(S.ISFINALSTATUS, '')            AS STATUSID, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALPASSWORD, '')         AS TERMINALPASSWORD, "
                + "       IFNULL(T.REMARKS, '')                  AS REMARKS, "
                + "       IFNULL(T.SHARINGTID, '')               AS SHARINGTID, "
                + "       IFNULL(T.SHARINGMID, '')               AS SHARINGMID, "
                + "       IFNULL(T.NACNO, '')                    AS NACNO, "
                + "       IFNULL(T.SHARINGNACNO, '')             AS SHARINGNACNO, "
                + "       IFNULL(T.ESP, '')                      AS ESPID, "
                + "       IFNULL(E.DESCRIPTION, '')              AS ESP, "
                + "       IFNULL(PT.DESCRIPTION, '')             AS PROFILETYPE, "
                + "       IFNULL(T.RECEIPTTEXT, '')                     AS receipttext, "
                + "       IFNULL(T.APPLICATIONVERSION, '')              AS applicationversion, "
                + "       IFNULL(T.VISAMASTERAMEXCONFIGURATION, '')     AS visamasteramexconfig, "
                + "       IFNULL(T.DCC, '')                             AS dcc, "
                + "       IFNULL(T.CLIENTCONTACTPERSON, '')             AS clientcontactperson, "
                + "       IFNULL(T.CLIENTEMAIL, '')                     AS clientemail, "
                + "       IFNULL(T.CLIENTCONTACTNUMBER, '')             AS clientcontactno, "
                + "       IFNULL(T.MERCHANTCONTACTPERSON, '')           AS merchantcontactperson, "
                + "       IFNULL(T.MERCHANTCONTACTNUMBER, '')           AS merchantcontactno, "
                + "       IFNULL(T.TERMINALSERIALNO, '')                AS terminalserialno, "
                + "       IFNULL(TUP.USER1, '')                 AS USER1, "
                + "       IFNULL(TUP.USERPASS1, '')             AS USERPASS1, "
                + "       IFNULL(TUP.USER2, '')                 AS USER2, "
                + "       IFNULL(TUP.USERPASS2, '')             AS USERPASS2, "
                + "       IFNULL(TUP.USER3, '')                 AS USER3, "
                + "       IFNULL(TUP.USERPASS3, '')             AS USERPASS3, "
                + "       IFNULL(TUP.USER4, '')                 AS USER4, "
                + "       IFNULL(TUP.USERPASS4, '')             AS USERPASS4, "
                + "       IFNULL(TUP.USER5, '')                 AS USER5, "
                + "       IFNULL(TUP.USERPASS5, '')             AS USERPASS5, "
                + "       IFNULL(TUP.USER6, '')                 AS USER6, "
                + "       IFNULL(TUP.USERPASS6, '')             AS USERPASS6, "
                + "       IFNULL(TUP.USER7, '')                 AS USER7, "
                + "       IFNULL(TUP.USERPASS7, '')             AS USERPASS7, "
                + "       IFNULL(TUP.USER8, '')                 AS USER8, "
                + "       IFNULL(TUP.USERPASS8, '')             AS USERPASS8, "
                + "       IFNULL(TUP.USER9, '')                 AS USER9, "
                + "       IFNULL(TUP.USERPASS9, '')             AS USERPASS9, "
                + "       IFNULL(TUP.USER10, '')                 AS USER10, "
                + "       IFNULL(TUP.USERPASS10, '')             AS USERPASS10, "
                + "       IFNULL(TUP.USER11, '')                 AS USER11, "
                + "       IFNULL(TUP.USERPASS11, '')             AS USERPASS11, "
                + "       IFNULL(TUP.USER12, '')                 AS USER12, "
                + "       IFNULL(TUP.USERPASS12, '')             AS USERPASS12, "
                + "       IFNULL(TUP.USER13, '')                 AS USER13, "
                + "       IFNULL(TUP.USERPASS13, '')             AS USERPASS13, "
                + "       IFNULL(TUP.USER14, '')                 AS USER14, "
                + "       IFNULL(TUP.USERPASS14, '')             AS USERPASS14, "
                + "       IFNULL(TUP.USER15, '')                 AS USER15, "
                + "       IFNULL(TUP.USERPASS15, '')             AS USERPASS15, "
                + "       IFNULL(TUP.USER16, '')                 AS USER16, "
                + "       IFNULL(TUP.USERPASS16, '')             AS USERPASS16, "
                + "       IFNULL(TUP.USER17, '')                 AS USER17, "
                + "       IFNULL(TUP.USERPASS17, '')             AS USERPASS17, "
                + "       IFNULL(TUP.USER18, '')                 AS USER18, "
                + "       IFNULL(TUP.USERPASS18, '')             AS USERPASS18, "
                + "       IFNULL(TUP.USER19, '')                 AS USER19, "
                + "       IFNULL(TUP.USERPASS19, '')             AS USERPASS19, "
                + "       IFNULL(TUP.USER20, '')                 AS USER20, "
                + "       IFNULL(TUP.USERPASS20, '')             AS USERPASS20, "
                + "       IFNULL(TAMA.CODE, '')         AS MANUALKEY, "
                + "       IFNULL(TAOF.CODE, '')         AS OFFLINE, "
                + "       IFNULL(TAPR.CODE, '')         AS PREAUTH, "
                + "       IFNULL(TATI.CODE, '')         AS TIP, "
                + "       IFNULL(TACA.CODE, '')         AS CASHADVANCE, "
                + "       IFNULL(TAPA.CODE, '')         AS PANMASK, "
                + "       IFNULL(TAL4.CODE, '')         AS L4, "
                + "       IFNULL(TAEM.CODE, '')         AS EMV, "
                + "       IFNULL(TAFO.CODE, '')         AS FOURDBC, "
                + "       IFNULL(TAVO.CODE, '')         AS VOID_, "
                + "       IFNULL(TASE.CODE, '')         AS SETTLEMENT, "
                + "       IFNULL(TATO.CODE, '')         AS TOBEINSTALLEDBYEPIC, "
                + "       IFNULL(TAPS.CODE, '')         AS PASSWORD, "
                + "       IFNULL(TAAU.CODE, '')         AS AUTOSETTLEMENT, "
                + "       IFNULL(T.SIMNO, '')                  AS SIMNO, "
                + "       IFNULL(DATE(T.DEADLINEDATE), '')     AS DEADLINEDATE, "
                + "       IFNULL(T.REMARKS, '')                AS REMARKS, "
                + "       IFNULL(T.CURRENCY, '')                AS CURRENCY, "
                + "       IFNULL(C.NAME, '')                     AS CLIENT, "
                + "       IFNULL(CC.DESCRIPTION, '')             AS CLIENTTYPE, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(T.PARTNO, '')                   AS PARTNO, "
                + "       IFNULL(T.REVISION, '')                 AS REVISION, "
                + "       IFNULL(T.MAC, '')                      AS MAC, "
                + "       IFNULL(T.PTID, '')                     AS PTID, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(S.DESCRIPTION, '')              AS STATUS, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN client C "
                + "        ON C.CLIENTID = T.CLIENT "
                + "     LEFT OUTER JOIN clientcategory CC "
                + "        ON CC.CLIENTCATEGORYID = T.CLIENTTYPE "
                + "     LEFT OUTER JOIN esp E "
                + "        ON E.ESPID = T.ESP "
                + "     LEFT OUTER JOIN profiletype PT "
                + "        ON PT.PROFILETYPEID = T.PROFILETYPE "
                + "     LEFT OUTER JOIN ticketattributestatus TAMA ON TAMA.TICKETATTRIBUTESTATUSID = T.MANUALKEY "
                + "     LEFT OUTER JOIN ticketattributestatus TAOF ON TAOF.TICKETATTRIBUTESTATUSID = T.OFFLINE "
                + "     LEFT OUTER JOIN ticketattributestatus TAPR ON TAPR.TICKETATTRIBUTESTATUSID = T.PREAUTH "
                + "     LEFT OUTER JOIN ticketattributestatus TATI ON TATI.TICKETATTRIBUTESTATUSID = T.TIP "
                + "     LEFT OUTER JOIN ticketattributestatus TACA ON TACA.TICKETATTRIBUTESTATUSID = T.CASHADVANCE "
                + "     LEFT OUTER JOIN ticketattributestatus TAPA ON TAPA.TICKETATTRIBUTESTATUSID = T.PANMASK "
                + "     LEFT OUTER JOIN ticketattributestatus TAL4 ON TAL4.TICKETATTRIBUTESTATUSID = T.L4 "
                + "     LEFT OUTER JOIN ticketattributestatus TAEM ON TAEM.TICKETATTRIBUTESTATUSID = T.EMV "
                + "     LEFT OUTER JOIN ticketattributestatus TAFO ON TAFO.TICKETATTRIBUTESTATUSID = T.FOURDBC "
                + "     LEFT OUTER JOIN ticketattributestatus TAVO ON TAVO.TICKETATTRIBUTESTATUSID = T.VOID "
                + "     LEFT OUTER JOIN ticketattributestatus TASE ON TASE.TICKETATTRIBUTESTATUSID = T.SETTLEMENT "
                + "     LEFT OUTER JOIN ticketattributestatus TATO ON TATO.TICKETATTRIBUTESTATUSID = T.TOBEINSTALLEDBYEPIC "
                + "     LEFT OUTER JOIN ticketattributestatus TAPS ON TAPS.TICKETATTRIBUTESTATUSID = T.PASSWORD "
                + "     LEFT OUTER JOIN ticketattributestatus TAAU ON TAAU.TICKETATTRIBUTESTATUSID = T.AUTOSETTLEMENT "
                + "     LEFT OUTER JOIN ticketuserpass TUP ON TUP.TICKET = T.TICKETID "
                + "WHERE T.TICKETID = ?";
        return (NewInstallationTicke) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(NewInstallationTicke.class));
    }

    @Override
    public void updateTicket(Ticket ticket) throws HibernateException {
        sessionFactory.getCurrentSession().update(ticket);
    }

    @Override
    public void updateTicketHistory(Tickethistory ticketHistory) throws HibernateException {
        sessionFactory.getCurrentSession().update(ticketHistory);
    }

    @Override
    public JSONArray getTicketTerminalAssignmentData(String user, String contextpath, int strat, int length) throws SQLException {
        List<Ticket> ticketList = new ArrayList<>();

        String hql = "FROM Ticket T "
                + "JOIN FETCH T.status S "
                + "WHERE (S.statusid = 11 OR S.statusid = 13) AND T.createduser = :user"; //should ne 11 and 13

        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("user", user);
        query.setFirstResult(strat);
        query.setMaxResults(length);

        ticketList = query.list();
        long count = ticketList.size();

        long iTotalRecords = 0; // total number of records (unfiltered)
        long iTotalDisplayRecords = 0;

        JSONArray rows = new JSONArray();

        iTotalRecords = count;
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            for (Ticket data : ticketList) {
                JSONObject object = new JSONObject();
                object.put("ticketid", data.getTicketid());
                object.put("tickettype", data.getTicketcategory().getDescription());
                object.put("ticketstatus", data.getStatus().getDescription());
                rows.put(object);
            }
//                        audit.insertAuditTrace("Case search page ", String.valueOf(MasterDataVarList.CCL_CODE_SEARCH), " Case search ", parameters.getInput(), (String) session.getAttribute("username"));
        }
        return rows;

    }

    @Override
    public TerminalRepairTicket getTerminalRepairTicket(int ticketid) throws SQLException {
        String query = "SELECT T.TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')   AS TICKETDATETIME, "
                + "       IFNULL(TP.DESCRIPTION, '') AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')  AS PRODUCT, "
                + "       T.TICKECATEGORY            AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '') AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '') AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '') AS TICKETCATEGORY, "
                + "       IFNULL(RB.DESCRIPTION, '') AS REPORTEDBY, "
                + "       IFNULL(S.DESCRIPTION, '')  AS STATUS, "
                + "       T.STATUS                   AS STATUSID, "
                + "       IFNULL(S.ISFINALSTATUS, '') AS ISFINALSTATUS, "
                + "       T.ISUSERNEGLIGENCE, "
                + "       T.ISLIGHTENING, "
                + "       T.ISCOVEREDUNDERWARRANTY, "
                + "       T.ISUNDERAMC, "
                + "       T.PARENTTICKET, "
                + "       T.DAMAGEPARTDESCRIPTION, "
                + "       T.WARRANTYEXPIRYSTATUS, "
                + "       T.CAUSEOFFAULT, "
                + "       T.ACTIONTAKEN, "
                + "       T.REPLACEDPART, "
                + "       T.PARTTYPE, "
                + "       T.PARTNUMBER, "
                + "       T.PARTNUMBEROFTERMINAL, "
                + "       T.REVISION, "
                + "       T.MAC, "
                + "       T.PTID, "
                + "       T.COMPLETIONDATETIME, "
                + "       T.TOTLETIMESPENT, "
                + "       T.SPECIALREMARK, "
                + "       T.DISPATCHDATE, "
                + "       T.COURIERSERVICEPROVIDER, "
                + "       T.TRACKINGNUMBER,"
                + "       T.DELEVERYDESTINATION, "
                + "       MAX(TH.STATUS) AS THSTATUS, "
                + "       T.TICKETSTAGE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN tickethistory TH ON T.TICKETID = TH.TICKETID "
                + "WHERE T.TICKETID = ?";
        return (TerminalRepairTicket) new JdbcTemplate(dataSource).queryForObject(query, new Object[]{ticketid}, new BeanPropertyRowMapper(TerminalRepairTicket.class));
    }

    @Override
    public String getAssigneeByTicketId(int ticketid) throws HibernateException, SQLException {

        String hql = "SELECT E.nameinfull "
                + "FROM Ticket T "
                + "JOIN T.employee E "
                + "WHERE T.ticketid = :ticketid";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("ticketid", ticketid);

        return (String) query.uniqueResult();
    }

    @Override
    public int getTableCount(TicketParam ticket) throws SQLException {

        String query = "SELECT COUNT(*) "
                + "FROM ticket T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     INNER JOIN systemuser SU "
                + "         ON SU.USERID = T.ASSIGNEE "
                + "WHERE :where ";

        query = query.replace(":where", this.getWhere(ticket));
        return new JdbcTemplate(dataSource).queryForObject(query, Integer.class);

    }

    @Override
    public List<TicketParam> getTableData(TicketParam ticket, int start, int length) throws SQLException {

        List<TicketParam> ticketList;
        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATE, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(T.PARTNO, '')                   AS PARTNO, "
                + "       IFNULL(T.REVISION, '')                 AS REVISION, "
                + "       IFNULL(T.MAC, '')                      AS MAC, "
                + "       IFNULL(T.PTID, '')                     AS PTID, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(S.DESCRIPTION, '')              AS STATUS, "
                + "       IFNULL(S.STATUSCODE, '')               AS STATUSCODE, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     INNER JOIN systemuser SU "
                + "         ON SU.USERID = T.ASSIGNEE "
                + "WHERE :where "
                + "ORDER BY T.TICKETID DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhere(ticket));
        ticketList = (List<TicketParam>) new JdbcTemplate(dataSource).query(query, new Object[]{start, length}, new BeanPropertyRowMapper(TicketParam.class));
        return ticketList;
    }

    private String getWhere(TicketParam parameters) {

        String where = " SU.USERNAME = '" + parameters.getCreateduser() + "' ";;
        if (parameters.getInput() != null && !parameters.getInput().isEmpty()) {
            where += " AND T.TICKETID LIKE '" + parameters.getInput().trim() + "'"
                    + " OR T.TID LIKE '%" + parameters.getInput().trim() + "%'";
        }

        return where;
    }

    @Override
    public JSONObject insertFileMasterData(Bulkterminallot bulkterminallot) throws SQLException {

        JSONObject jsonobject = new JSONObject();

        try {
            sessionFactory.getCurrentSession().save(bulkterminallot);
            bulkterminallot.getLotno();
            jsonobject.put("CODE", "SUCCESS");
        } catch (HibernateException he) {
            he.printStackTrace();
            jsonobject.put("CODE", "ERROR");
            jsonobject.put("MESSAGE", "Failed to upload attachment");
        }

        return jsonobject;

    }

    @Override
    public void insertBulkTerminalBreakdown(Ticket ticket) throws SQLException {
        sessionFactory.getCurrentSession().save(ticket);
    }

    @Override
    public long getTicketTableDataCount(TicketParam ticket, Systemuser systemuser) throws HibernateException, SQLException, Exception {

        long count = 0;
        String userRoleCode = systemuser.getUserrole().getUserrolecode();
        if (userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EM)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_IRC)) {
            String query = "SELECT COUNT(*) FROM ticket T "
                    + "LEFT OUTER JOIN status S "
                    + "ON "
                    + "T.`STATUS` = S.STATUSID "
                    + "WHERE :where ";
//                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC + "', "
//                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC + "', "
//                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM + "') ";

            query = query.replace(":where", this.getWhereTicket(systemuser));
            count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        }
        return count;
    }

    @Override
    public List<JSONObject> getTicketTableData(TicketParam ticket, Systemuser systemuser, int minCount, int start) throws SQLException {

        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, 'N/A')               AS TICKETDATE, "
                + "       IFNULL(TP.DESCRIPTION, 'N/A')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(T.PARTNO, '')                   AS PARTNO, "
                + "       IFNULL(T.REVISION, '')                 AS REVISION, "
                + "       IFNULL(T.MAC, '')                      AS MAC, "
                + "       IFNULL(T.PTID, '')                     AS PTID, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.MERCHANTNAME, 'N/A')          AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.MERCHANTCONTACTNUMBER, 'N/A') AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(S.DESCRIPTION, 'N/A')              AS STATUS, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "     INNER JOIN systemuser SU "
                + "        ON T.ASSIGNEE = SU.USERID "
                + "WHERE :where "
                + "ORDER BY T.TICKETID DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhereTicket(systemuser));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new TicketAssignTableViewRowMapper());
    }

    private String getWhereTicket(Systemuser systemuser) {
        String where = "1 = 1 AND T.ASSIGNEE = " + systemuser.getEmployee().getEmployeeid() + " ";
        if (systemuser.getClient() != null) {
            where = where + "AND T.CLIENT = " + systemuser.getClient().getClientid() + " ";
        }
        where = where + "AND S.STATUSCODE IN ('" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC + "',"
                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC + "',"
                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM + "',"
                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC + "') ";
//        if (systemuser.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC)) {
//            where = where + "AND S.STATUSCODE = " + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC + "' ";
//        } else if (systemuser.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)) {
//            where = where + "AND S.STATUSCODE = " + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC + "' ";
//        } else if (systemuser.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM)) {
//            where = where + "AND S.STATUSCODE = " + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM + "' ";
//        }
        return where;
    }

    @Override
    public Tickethistory getTicketHistoryMaxDataByTicketId(int ticketid) throws SQLException {
        String query = "FROM Tickethistory TH "
                + "WHERE TH.tickethistoryid = (SELECT MAX(TKTH.tickethistoryid) "
                + "FROM Tickethistory TKTH "
                + "WHERE TKTH.ticket.ticketid = :ticketid )";

        return (Tickethistory) sessionFactory.getCurrentSession().createQuery(query).setInteger("ticketid", ticketid).uniqueResult();
    }

    private class TicketAssignTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("TICKETID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-chevron-down\" title=\"View\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("ticketid", rs.getInt("TICKETID"));
            object.put("ticketdate", Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, rs.getDate("TICKETDATE")));
            object.put("ticketpriority", rs.getString("TICKETPRIORITY"));
            object.put("merchantname", rs.getString("MERCHANTNAME"));
            object.put("merchantcontactno", rs.getString("CONTACTNO"));
            object.put("status", rs.getString("STATUS"));

            return object;
        }
    }

    @Override
    public long getTerminalTableDataCount(String ticketId, String username) throws HibernateException, SQLException, Exception {
        String query = "SELECT COUNT(*) "
                + "FROM inventoryterminalitem T "
                + "LEFT OUTER JOIN status S "
                + "ON T.STATUS = S.STATUSID "
                + "WHERE :where ";
        query = query.replace(":where", this.getWhereInventoryCategoryId(Integer.parseInt(ticketId), username));
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        return count;
    }

    @Override
    public List<JSONObject> getTerminalTableData(String ticketId, String username, int minCount, int start) throws SQLException {

        String query = "SELECT COALESCE (T.INVENTORYTERMINALITEMID, 'N/A') AS INVENTORYTERMINALITEMID,"
                + "COALESCE (TB.DESCRIPTION, 'N/A') AS TERMINALBRAND, "
                + "COALESCE(TM.DESCRIPTION, 'N/A') AS TERMINALMODEL, "
                + "COALESCE(T.SERIALNO, 'N/A') AS SERIALNO, "
                + "COALESCE(CL.NAME, 'N/A') AS CLIENT, "
                + "COALESCE(CT.DESCRIPTION, 'N/A') AS INVENTORYCATEGORY, "
                + "COALESCE(EM.NAMEINFULL, 'N/A') AS OWNER, "
                + "COALESCE(T.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + "COALESCE(T.CREATEDUSER, 'N/A') AS CREATEDUSER "
                + "FROM inventoryterminalitem T "
                + "LEFT OUTER JOIN terminalmodel TM ON T.TERMINALMODEL = TM.TERMINALMODELID "
                + "LEFT OUTER JOIN terminalbrand TB ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "LEFT OUTER JOIN client CL ON T.CLIENT = CL.CLIENTID "
                + "LEFT OUTER JOIN employee EM ON T.OWNER = EM.EMPLOYEEID "
                + "LEFT OUTER JOIN inventorycategory CT ON T.INVENTORYCATEGORY = CT.INVENTORYCATEGORYID "
                + "LEFT OUTER JOIN status S "
                + "ON T.STATUS = S.STATUSID "
                + "WHERE :where "
                + "ORDER BY T.LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,? ";

        query = query.replace(":where", this.getWhereInventoryCategoryId(Integer.parseInt(ticketId), username));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new TerminalTableViewRowMapper());
    }

    private String getWhereInventoryCategoryId(int ticketId, String username) {

        Ticket ticket = this.getTicket(ticketId);
        Systemuser systemUser = systemuserDAO.getSytemuserByUsername(username);
        String where = "1 = 1 AND S.STATUSCODE = '" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED + "' ";

        if (systemUser.getEmployee().getLocation().getLocationid().equals(MasterDataVarList.AFFINITI_CODE_LOCATION_LK)) {
            where = where + "AND T.LOCATION = " + MasterDataVarList.AFFINITI_CODE_LOCATION_LK + " ";
        } else {
            where = where + "AND T.LOCATION = " + MasterDataVarList.AFFINITI_CODE_LOCATION_MY + " ";
        }
        if (ticket.getClient() != null) {
            where = where + "AND T.CLIENT = " + ticket.getClient().getClientid() + " ";
        }
        if (ticket.getStatus().getStatusid() == MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_REQUESTBACKUPTERMINAL
                || ticket.getStatus().getStatusid() == MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_COLLECTTERMINAL) {//Request Backup Terminal - TMC
            where = where + "AND T.INVENTORYCATEGORY = " + MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_BACKUP + " ";
        } else if (ticket.getStatus().getStatusid() == MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_COLLECTTERMINALFROMTMC) {//Collect Terminal from TMC
            if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION) {//New Installation
                where = where + "AND T.INVENTORYCATEGORY IN ('" + MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_NEW + "','" + MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_BRANDNEW + "') ";
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE) {//Upgrade Downgrade
                where = where + "AND T.INVENTORYCATEGORY IN ('" + MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_UPGRADE + "') ";
            }
        } else if (ticket.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC)) {//Collect Repaired Terminal from TMC
            where = where + "AND T.INVENTORYCATEGORY = " + MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_REPAIRED + " ";
        }
        return where;
    }

    private class TerminalTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("INVENTORYTERMINALITEMID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-plus\" title=\"View\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("brand", rs.getString("TERMINALBRAND"));
            object.put("model", rs.getString("TERMINALMODEL"));
            object.put("serialNo", rs.getString("SERIALNO"));
            object.put("client", rs.getString("CLIENT"));
            object.put("category", rs.getString("INVENTORYCATEGORY"));
            object.put("owner", rs.getString("OWNER"));
            object.put("createddatetime", rs.getDate("CREATEDDATETIME"));
            object.put("createduser", rs.getString("CREATEDUSER"));

            return object;
        }
    }

    @Override
    public String getStatusCodeByTicketId(int ticktId) throws SQLException {

        String query = "SELECT "
                + "S.STATUSCODE FROM ticket T "
                + "LEFT OUTER JOIN status S "
                + "ON S.STATUSID = T.STATUS "
                + "WHERE T.TICKETID = " + ticktId;
        return new JdbcTemplate(dataSource).queryForObject(query, String.class);
    }

    @Override
    public long getTicketTableDataCountByStatus(ArrayList statusId, Systemuser systemuser) throws SQLException {

        long count = 0;
        String userRoleCode = systemuser.getUserrole().getUserrolecode();
        if (userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EM)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_IFS)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_IRC)) {
            String query = "SELECT COUNT(*) "
                    + "FROM ticket T "
                    + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                    + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                    + "     LEFT OUTER  JOIN productcategory PC "
                    + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                    + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                    + "     LEFT OUTER JOIN ticketcategory TC "
                    + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                    + "     LEFT OUTER JOIN terminalbrand TB "
                    + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                    + "     LEFT OUTER JOIN terminalmodel TMO "
                    + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                    + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                    + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                    + "     LEFT OUTER JOIN deleveryrequirement DR "
                    + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                    + "     LEFT OUTER JOIN natureoffault NOF "
                    + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                    + "     LEFT OUTER JOIN actiontobetaken ATBT "
                    + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                    + "     LEFT OUTER JOIN causeoffault COF "
                    + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                    + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                    + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                    + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                    + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                    + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                    + "     LEFT OUTER JOIN terminalbrand BTB "
                    + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                    + "     LEFT OUTER JOIN terminalmodel BTMO "
                    + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                    + "WHERE :where ";

            query = query.replace(":where", this.getWhereForTicketTableDataByStatus(statusId));
            count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        }
        return count;
    }

    @Override
    public List<JSONObject> getTicketTableDataByStatus(int minCount, int start, ArrayList statusId) throws SQLException {

        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATE, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(T.PARTNO, '')                   AS PARTNO, "
                + "       IFNULL(T.REVISION, '')                 AS REVISION, "
                + "       IFNULL(T.MAC, '')                      AS MAC, "
                + "       IFNULL(T.PTID, '')                     AS PTID, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(S.DESCRIPTION, '')              AS STATUS, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "WHERE :where "
                + "ORDER BY T.TICKETID DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhereForTicketTableDataByStatus(statusId));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new TicketAcceptTableViewRowMapper());
    }

    private String getWhereForTicketTableDataByStatus(ArrayList statusCode) {
        String where = "1 = 1 ";
        if (!statusCode.isEmpty()) {
            String strStatusIdList = Arrays.toString(statusCode.toArray(new String[0])).replaceAll("[\\[\\]\"]", "'").replace(", ", "','");
            where = where + "AND T.STATUS IN (SELECT STATUSID FROM status WHERE STATUSCODE IN (" + strStatusIdList + "))";
        }
        return where;
    }

    private class TicketAcceptTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("TICKETID") + "' value='accept'><i class=\"fa fa-lg fa-fw fa-check\" title=\"accept\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("TICKETID") + "' value='reject'><i class=\"fa fa-lg fa-fw fa-times\" title=\"reject\"></i></a></div>"
                    + "</div>";

            object.put("action", action);

            object.put("ticketid", rs.getInt("TICKETID"));
            object.put("ticketdate", Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, rs.getDate("TICKETDATE")));
            object.put("ticketpriority", rs.getString("TICKETPRIORITY"));
            object.put("merchantname", rs.getString("MERCHANTNAME"));
            object.put("merchantcontactno", rs.getString("CONTACTNO"));
            object.put("status", rs.getString("STATUS"));

            return object;
        }
    }

    @Override
    public JSONObject getTerminalInfo(String input) throws SQLException {

        JSONObject jsonObject = new JSONObject();

        String query_merchant = "SELECT DISTINCT P.VALUE AS MERCHANT "
                + "FROM VeriCenter.VeriCentre.RELATION R "
                + "INNER JOIN VeriCenter.VeriCentre.PARAMETER P ON P.PARTID = R.TERMID AND P.FAMNM = R.FAMNM AND P.APPNM = R.APPNM "
                + "WHERE P.PARTID = ? AND PARNAMELOC IN ('3RH', '1RH1', ', R1HDR01', '3RH1', 'R1HDR1', 'R3HDR1')";

        List<Map<String, Object>> merchantInfo = new JdbcTemplate(msSqldataSource).queryForList(query_merchant, new Object[]{input});
        for (Map merchant : merchantInfo) {
            jsonObject.put("merchantname", merchant.get("MERCHANT"));
        }

        String query_add1 = "SELECT DISTINCT P.VALUE AS ADDRESS1 "
                + "FROM VeriCenter.VeriCentre.RELATION R "
                + "INNER JOIN VeriCenter.VeriCentre.PARAMETER P ON P.PARTID = R.TERMID AND P.FAMNM = R.FAMNM AND P.APPNM = R.APPNM "
                + "WHERE P.PARTID = ? AND PARNAMELOC IN ('4RH', 'R4FDR01', 'R4FDR1', '4RF1', '4RH1')";

        List<Map<String, Object>> addressInfo = new JdbcTemplate(msSqldataSource).queryForList(query_add1, new Object[]{input});
        for (Map address1 : addressInfo) {
            jsonObject.put("addressone", address1.get("ADDRESS1"));
        }

        String query_add2 = " SELECT DISTINCT P.VALUE AS ADDRESS2 "
                + "FROM VeriCenter.VeriCentre.RELATION R "
                + "INNER JOIN VeriCenter.VeriCentre.PARAMETER P ON P.PARTID = R.TERMID AND P.FAMNM = R.FAMNM AND P.APPNM = R.APPNM "
                + "WHERE P.PARTID = ? "
                + "AND PARNAMELOC IN ( '5RH', '6RH1', 'R6HDR01', '5RH1', 'R3HDR1', '4RH', 'R4FDR01') "
                + "AND P.VALUE <> ( SELECT DISTINCT PVAL.VALUE "
                + "                 FROM VeriCenter.VeriCentre.RELATION RVAL "
                + "                 INNER JOIN VeriCenter.VeriCentre.PARAMETER PVAL ON PVAL.PARTID = RVAL.TERMID AND PVAL.FAMNM = RVAL.FAMNM AND PVAL.APPNM = RVAL.APPNM "
                + "                 WHERE PVAL.PARTID = ? AND PVAL.PARNAMELOC IN ('4RH', 'R4FDR01', 'R4FDR1', '4RF1', '4RH1'))";

        List<Map<String, Object>> address2Info = new JdbcTemplate(msSqldataSource).queryForList(query_add2, new Object[]{input, input});
        for (Map address2 : address2Info) {
            jsonObject.put("addresstwo", address2.get("ADDRESS2"));
        }

        String query_merchantid = "SELECT P.VALUE AS MERCHANTID "
                + "FROM VeriCenter.VeriCentre.RELATION R "
                + "INNER JOIN VeriCenter.VeriCentre.PARAMETER P ON P.PARTID = R.TERMID AND P.FAMNM = R.FAMNM AND P.APPNM = R.APPNM "
                + "WHERE P.PARTID = ? AND PARNAMELOC IN ( 'SAMMID1', 'HNBMID', 'BOCMID1', 'SCBMID1', 'HSBMID01', 'BNTHMID')";

        String query = "SELECT  DISTINCT P.PARTID AS TID, "
                + "             R.FAMNM AS TERMINALMODEL "
                + "FROM VeriCenter.VeriCentre.RELATION R "
                + "INNER JOIN VeriCenter.VeriCentre.PARAMETER P ON P.PARTID = R.TERMID AND P.FAMNM = R.FAMNM AND P.APPNM = R.APPNM "
                + "WHERE P.PARTID = ? AND R.LASTPAR = (SELECT MAX(tbl_date.LASTDATE) "
                + "                                    FROM ( SELECT MAX(REL.LASTPAR) AS LASTDATE"
                + "                                           FROM VeriCenter.VeriCentre.RELATION REL "
                + "                                           INNER JOIN VeriCenter.VeriCentre.PARAMETER PARM ON PARM.PARTID = REL.TERMID AND PARM.FAMNM = REL.FAMNM AND PARM.APPNM = REL.APPNM "
                + "                                           WHERE PARM.PARTID = ? "
                + "                                           UNION "
                + "                                           SELECT MAX(RLASTFULL.LASTFULL) AS LASTDATE "
                + "                                           FROM VeriCenter.VeriCentre.RELATION RLASTFULL "
                + "                                           INNER JOIN VeriCenter.VeriCentre.PARAMETER PLASTFULL ON PLASTFULL.PARTID = RLASTFULL.TERMID AND PLASTFULL.FAMNM = RLASTFULL.FAMNM AND PLASTFULL.APPNM = RLASTFULL.APPNM "
                + "                                           WHERE PLASTFULL.PARTID = ?) AS tbl_date)";

        List<Map<String, Object>> terminalInfoList = new JdbcTemplate(msSqldataSource).queryForList(query, new Object[]{input, input, input});
        for (Map teminalInfo : terminalInfoList) {
            jsonObject.put("tid", teminalInfo.get("TID"));
            jsonObject.put("terminalmodel", teminalInfo.get("TERMINALMODEL"));
            jsonObject.put("partid", teminalInfo.get("TID"));
        }
        return jsonObject;
    }

    @Override
    public long getTickeHistoryDataCount(TicketParam ticket) throws HibernateException, SQLException, Exception {

        String query = "SELECT COUNT(*) FROM tickethistory TH WHERE TH.TICKETID = " + ticket.getTicketid() + " ";
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        return count;
    }

    @Override
    public List<TicketHistory> getTickeHistoryData(TicketParam ticket, int minCount, int start) throws HibernateException, SQLException, Exception {

        List<TicketHistory> tickethistorylist;

        String query = "SELECT "
                + "COALESCE(T.PREVIOUSTICKETID, 'N/A') AS PREVIOUSTICKETID, "
                + "COALESCE(TH.CREATEDDATETIME, 'N/A') AS CREATEDDATETIME, "
                + "COALESCE(CF.DESCRIPTION, 'N/A') AS CAUSEOFFAULT, "
                + "COALESCE(AT.DESCRIPTION, 'N/A') AS ACTIONTAKEN, "
                + "COALESCE(E.NAMEINFULL, 'N/A') AS ASSIGNEEID, "
                + "COALESCE(S.DESCRIPTION, 'N/A') AS STATUS, "
                + "COALESCE(TH.STATUSRESIONID, 'N/A') AS STATUSRESIONID, "
                + "COALESCE(TH.RESOLUTIONDESCRIPTION, 'N/A') AS RESOLUTIONDESCRIPTION "
                + "FROM tickethistory TH "
                + "INNER JOIN ticket T ON TH.TICKETID = T.TICKETID "
                + "LEFT OUTER JOIN actiontaken AT ON TH.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "INNER JOIN employee E ON TH.ASSIGNEEID = E.EMPLOYEEID "
                + "LEFT OUTER JOIN causeoffault CF ON TH.CAUSEOFFAULT = CF.CAUSEOFFAULTID "
                + "INNER JOIN status S ON TH.STATUS = S.STATUSID "
                + "WHERE TH.TICKETID = ? "
                + "ORDER BY TH.TICKETHISTORYID DESC "
                + "LIMIT ?,?";

        tickethistorylist = new JdbcTemplate(dataSource).query(
                query, new Object[]{ticket.getTicketid(), start, minCount},
                new RowMapper<TicketHistory>() {
                    @Override
                    public TicketHistory mapRow(ResultSet rs, int rowNum) throws SQLException {
                        TicketHistory tickethistorydata = new TicketHistory();
                        tickethistorydata.setPreviousticketid(rs.getString("PREVIOUSTICKETID"));
                        tickethistorydata.setCreateddatetime(rs.getTimestamp("CREATEDDATETIME"));
                        tickethistorydata.setCauseoffault(rs.getString("CAUSEOFFAULT"));
                        tickethistorydata.setActiontaken(rs.getString("ACTIONTAKEN"));
                        tickethistorydata.setAssignee(rs.getString("ASSIGNEEID"));
                        tickethistorydata.setStatus(rs.getString("STATUS"));
                        tickethistorydata.setStatusresionid(rs.getString("STATUSRESIONID"));
                        tickethistorydata.setResolutiondescription(rs.getString("RESOLUTIONDESCRIPTION"));
                        return tickethistorydata;
                    }
                });

        return tickethistorylist;
    }

    public class TicketHistoryViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            object.put("date", Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd_hh_mm_a, rs.getTimestamp("CREATEDDATETIME")));
            object.put("causeOfFault", rs.getString("CAUSEOFFAULT"));
            object.put("actionTaken", rs.getString("ACTIONTAKEN"));
            object.put("assigneeid", rs.getString("ASSIGNEEID"));
            object.put("status", rs.getString("STATUS"));
            object.put("resolution", rs.getString("RESOLUTIONDESCRIPTION"));
            if (rs.getString("PREVIOUSTICKETID") != null) {
//                String action ="<div class=\"row\">"
//                        +"<div class=\"col-xs-3\"><a target='_blank' href='" + request.getContextPath() + "/accessory " + "'value='ticket_redirect'><i class=\"fa fa-lg fa-fw fa-link\" title=\"ticket_redirect\"></i></a></div>"
//                        +"</div>";
                String action = "<div class=\"row\">"
                        + "<div class=\"col-xs-3\"><a target='_blank' href='" + rs.getString("URL") + "'value='ticket_redirect'><i class=\"fa fa-lg fa-fw fa-link\" title=\"ticket_redirect\"></i></a></div>"
                        + "</div>";
                object.put("action", rs.getString("URL"));
            } else {
                object.put("action", rs.getString("N/A"));
            }

            return object;
        }
    }

    @Override
    public Boolean validateSerialNumber(String serialNumber) throws SQLException {

        String query = "SELECT COUNT(*) "
                + "FROM ticket T "
                + "WHERE T.TERMINALSERIALNO = ? ";
        int count = new JdbcTemplate(dataSource).queryForObject(query, new Object[]{serialNumber}, Integer.class);

        if (count > 0) {
            return false;
        } else {
            return true;
        }
    }

    public void createReminderNotification(Remindernotification remindernotification) throws SQLException {
        sessionFactory.getCurrentSession().save(remindernotification);
    }
}
