/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.accessory;

import com.avn.affiniti.hibernate.model.Inventoryhardwarehistory;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.model.ticket.TicketParam;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author ASHOK
 */
public interface AccessoryAssignDAO {
    
    public long getTicketTableDataCount(Systemuser systemUser) throws HibernateException, SQLException, Exception;
     
    public List<JSONObject> getTicketTableData(Systemuser systemUser, int minCount, int start) throws HibernateException, SQLException, Exception;
    
    public long getAccessoryTableDataCount(Ticket ticket, Systemuser systemuser) throws HibernateException, SQLException, Exception;
     
    public List<JSONObject> getAccessoryTableData(Ticket ticket, Systemuser systemuser, int minCount, int start) throws HibernateException, SQLException, Exception;
    
    public boolean insertInvenotyHardwareHistory(Inventoryhardwarehistory inventoryhardwarehistory) throws HibernateException, SQLException;
}
