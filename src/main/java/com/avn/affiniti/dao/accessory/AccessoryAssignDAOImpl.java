/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.accessory;

import com.avn.affiniti.hibernate.model.Inventoryhardwarehistory;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.model.ticket.TicketParam;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASHOK
 */
@Repository("accessoryAssignDAO")
public class AccessoryAssignDAOImpl implements AccessoryAssignDAO {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public long getTicketTableDataCount(Systemuser systemUser) throws HibernateException, SQLException, Exception {

        long count = 0;
        String userRoleCode = systemUser.getUserrole().getUserrolecode();

        if (userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE)
                //                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_IRC)
                //                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC)
                || userRoleCode.equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EM)) {
            String query = "SELECT COUNT(*) "
                    + "FROM ticket T "
                    + "LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                    + "WHERE :where ";

            query = query.replace(":where", this.getWhereTicket(systemUser));
            count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        }
        return count;
    }

    @Override
    public List<JSONObject> getTicketTableData(Systemuser systemUser, int minCount, int start) throws HibernateException, SQLException, Exception {

        String query = "SELECT T.TICKETID                        AS TICKETID, "
                + "       IFNULL(T.TICKETDATE, '')               AS TICKETDATE, "
                + "       IFNULL(TP.DESCRIPTION, '')             AS TICKETPRIORITY, "
                + "       IFNULL(P.DESCRIPTION, '')              AS PRODUCT, "
                + "       T.TICKECATEGORY                        AS TICKETCATEGORYID, "
                + "       IFNULL(PC.DESCRIPTION, '')             AS PRODUCTCATEGORY, "
                + "       IFNULL(TM.DESCRIPTION, '')             AS TERRITORYMAP, "
                + "       IFNULL(TC.DESCRIPTION, '')             AS TICKETCATEGORY, "
                + "       IFNULL(T.TID, '')                      AS TID, "
                + "       IFNULL(T.MID, '')                      AS MID, "
                + "       IFNULL(T.TERMINALSERIALNO, '')         AS TERMINALSERIAL, "
                + "       IFNULL(TB.DESCRIPTION, '')             AS TERMINALBRAND, "
                + "       IFNULL(TMO.DESCRIPTION, '')            AS TERMINALMODEL, "
                + "       IFNULL(T.WARRANTYEXPIRYSTATUS, '')     AS WARRANTYEXPIRYSTATUS, "
                + "       IFNULL(T.AMCAEXPIRYSTATUS, '')         AS AMCAEXPIRYSTATUS, "
                + "       IFNULL(T.LOTNO, '')                    AS LOTNO, "
                + "       IFNULL(T.PARTNO, '')                   AS PARTNO, "
                + "       IFNULL(T.REVISION, '')                 AS REVISION, "
                + "       IFNULL(T.MAC, '')                      AS MAC, "
                + "       IFNULL(T.PTID, '')                     AS PTID, "
                + "       IFNULL(B.DESCRIPTION, '')              AS BANK, "
                + "       IFNULL(T.MERCHANTNAME, '')             AS MERCHANTNAME, "
                + "       IFNULL(T.CONTACTPERSION, '')           AS CONTACTPERSON, "
                + "       IFNULL(T.CONTACTNO, '')                AS CONTACTNO, "
                + "       IFNULL(D.DESCRIPTION, '')              AS DISTRICT, "
                + "       IFNULL(T.LOCATIONADDRESS, '')          AS LOCATIONADDRESS, "
                + "       IFNULL(DR.DESCRIPTION, '')             AS DELEVERYREQUIREMENT, "
                + "       IFNULL(T.DELEVERYDESTINATION, '')      AS DELEVERYDESTINATION, "
                + "       IFNULL(NOF.DESCRIPTION, '')            AS NATUREOFFAULT, "
                + "       IFNULL(ATBT.DESCRIPTION, '')           AS ACTIONTOBETAKEN, "
                + "       IFNULL(COF.DESCRIPTION, '')            AS CAUSEOFFAULT, "
                + "       IFNULL(AT.DESCRIPTION, '')             AS ACTIONTAKEN, "
                + "       IFNULL(T.TECHNICALOFFICERINCHARGE, '') AS TECHNICALOFFICERINCHARGE, "
                + "       IFNULL(RB.DESCRIPTION, '')             AS REPORTEDBY, "
                + "       IFNULL(T.REPORTEDMERCHANT, '')         AS REPORTEDMERCHANT, "
                + "       IFNULL(S.DESCRIPTION, '')              AS STATUS, "
                + "       IFNULL(ITMI.TID, '')                   AS BTID, "
                + "       IFNULL(ITMI.MID, '')                   AS BMID, "
                + "       IFNULL(ITMI.SERIALNO, '')              AS BTERMINALSERIAL, "
                + "       IFNULL(BTB.DESCRIPTION, '')            AS BTERMINALBRAND, "
                + "       IFNULL(BTMO.DESCRIPTION, '')           AS BTERMINALMODEL, "
                + "       IFNULL(T.DEPLOYMENTDATE, '')           AS DEPLOYMENTDATE, "
                + "       IFNULL(T.HARDWAREITEMQTY, '')          AS  HARDWAREITEMQTY, "
                + "       IFNULL(IH.PARTNAME, '')   AS  PARTNAME "
                + "FROM ticket    T "
                + "     LEFT OUTER JOIN ticketpriority TP ON T.PRIORITY = TP.TICKETPRIORITYID "
                + "     LEFT OUTER JOIN product P ON P.PRODUCTID = T.PRODUCT "
                + "     LEFT OUTER JOIN productcategory PC "
                + "        ON T.PRODUCSUBCATEGORY = PC.PRODUCTCATEGORYID "
                + "     LEFT OUTER JOIN territorymap TM ON T.TERRITORYMAP = TM.TERRITORYMAPID "
                + "     LEFT OUTER JOIN ticketcategory TC "
                + "        ON T.TICKECATEGORY = TC.TICKETCATEGORYID "
                + "     LEFT OUTER JOIN terminalbrand TB "
                + "        ON T.TERMINALBRAND = TB.TERMINALBRANDID "
                + "      LEFT OUTER JOIN inventoryhardwareitem IH "
                + "      ON T.INVENTORYHARDWAREITEMS = IH.INVENTORYHARDWAREITEMID "
                + "     LEFT OUTER JOIN terminalmodel TMO "
                + "        ON T.TERMINALMODEL = TMO.TERMINALMODELID "
                + "     LEFT OUTER JOIN bank B ON T.BANK = B.BANKID "
                + "     LEFT OUTER JOIN district D ON T.DISTRICT = D.DISTRICTID "
                + "     LEFT OUTER JOIN deleveryrequirement DR "
                + "        ON T.DELEVERYREQUIRMENT = DR.DELEVERYREQUIEREMENTID "
                + "     LEFT OUTER JOIN natureoffault NOF "
                + "        ON T.NATUREOFFAULT = NOF.NATUREOFFAULTID "
                + "     LEFT OUTER JOIN actiontobetaken ATBT "
                + "        ON T.ACTIONTOBETAKEN = ATBT.ACTIONTOBETAKENID "
                + "     LEFT OUTER JOIN causeoffault COF "
                + "        ON T.CAUSEOFFAULT = COF.CAUSEOFFAULTID "
                + "     LEFT OUTER JOIN actiontaken AT ON T.ACTIONTAKEN = AT.ACTIONTAKENID "
                + "     LEFT OUTER JOIN reportedby RB ON T.REPORTEDBY = RB.REPORTEDID "
                + "     LEFT OUTER JOIN status S ON T.STATUS = S.STATUSID "
                //                + "     INNER JOIN status S ON T.STATUS = S.STATUSID "
                + "     LEFT OUTER JOIN inventoryterminalitem ITMI "
                + "        ON T.INVENTORYTERMINAL = ITMI.INVENTORYTERMINALITEMID "
                + "     LEFT OUTER JOIN terminalbrand BTB "
                + "        ON ITMI.TERMINALBRAND = BTB.TERMINALBRANDID "
                + "     LEFT OUTER JOIN terminalmodel BTMO "
                + "        ON ITMI.TERMINALBRAND = BTMO.TERMINALMODELID "
                + "WHERE :where "
                + "ORDER BY T.TICKETID DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhereTicket(systemUser));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new TicketTableViewRowMapper());
    }

    private String getWhereTicket(Systemuser systemuser) {

        String where = "1 = 1 AND T.ASSIGNEE = " + systemuser.getEmployee().getEmployeeid() + " ";
        if (systemuser.getClient() != null) {
            where = where + "AND T.CLIENT = " + systemuser.getClient().getClientid() + " ";
        }
        where = where + "AND S.STATUSCODE IN ('" + MasterDataVarList.AFFINITI_TICKET_STATUS_CODE_TKT_TKTACSSREQCSE + "',"
                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQACCEM + "',"
                + "'" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQPART + "') ";
        return where;
    }

    private class TicketTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a href='#' id='" + rs.getInt("TICKETID") + "' value='view'><i class=\"fa fa-lg fa-fw fa-chevron-down\" title=\"View\"></i></a></div>"
                    + "</div>";

            object.put("action", action);
            object.put("ticketid", rs.getInt("TICKETID"));
            object.put("ticketdate", Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, rs.getDate("TICKETDATE")));
            object.put("ticketpriority", rs.getString("TICKETPRIORITY"));
            object.put("merchantname", rs.getString("MERCHANTNAME"));
            object.put("partname", rs.getString("PARTNAME"));
            object.put("quantity", rs.getString("HARDWAREITEMQTY"));
            object.put("merchantcontactno", rs.getString("CONTACTNO"));
            object.put("status", rs.getString("STATUS"));

            return object;
        }
    }

    @Override
    public long getAccessoryTableDataCount(Ticket ticket, Systemuser systemuser) throws HibernateException, SQLException, Exception {
        String query = "SELECT COUNT(*) "
                + "FROM inventoryhardwareitem IH "
                + "INNER JOIN status S "
                + "ON IH.STATUS = S.STATUSID "
                + "WHERE :where ";

        query = query.replace(":where", this.getWhereForAccessory(ticket, systemuser));
        long count = new JdbcTemplate(dataSource).queryForObject(query, Integer.class);
        return count;
    }

    @Override
    public List<JSONObject> getAccessoryTableData(Ticket ticket, Systemuser systemuser, int minCount, int start) throws HibernateException, SQLException, Exception {
        String query = "SELECT "
                + "COALESCE(IH.INVENTORYHARDWAREITEMID, 'N/A') AS INVENTORYHARDWAREITEMID, "
                + "COALESCE(IC.DESCRIPTION, 'N/A') AS INVENTORYCATEGORY, "
                + "COALESCE(IH.PARTCODE, 'N/A') AS PARTCODE, "
                + "COALESCE(IH.PARTNAME, 'N/A') AS PARTNAME, "
                + "COALESCE(IH.POSREVISION, 'N/A') AS POSREVISION, "
                + "COALESCE(IH.POSPTID, 'N/A') AS POSPTID, "
                + "COALESCE(IH.POSSERIALNUMBER, 'N/A') AS POSSERIALNUMBER "
                + "FROM inventoryhardwareitem IH "
                + "LEFT OUTER JOIN inventorycategory IC "
                + "ON IH.INVENTORYCATEGORY = IC.INVENTORYCATEGORYID "
                + "INNER JOIN status S "
                + "ON IH.STATUS = S.STATUSID "
                + "WHERE :where "
                + "ORDER BY IH.LASTUPDATEDDATETIME DESC "
                + "LIMIT ?,?";

        query = query.replace(":where", this.getWhereForAccessory(ticket, systemuser));
        return new JdbcTemplate(dataSource).query(query, new Object[]{start, minCount}, new AccessoryTableViewRowMapper());
    }

    private String getWhereForAccessory(Ticket ticket, Systemuser systemuser) {

        String where = "1 = 1 AND IH.LOCATION = " + systemuser.getEmployee().getLocation().getLocationid() + " ";
        if (ticket.getInventoryhardwareitem() != null) {
            where = where + "AND IH.PARTCODE = (SELECT PARTCODE FROM inventoryhardwareitem WHERE INVENTORYHARDWAREITEMID = " + ticket.getInventoryhardwareitem().getInventoryhardwareitemid() + ") ";
        }
        where = where + " AND IH.INVENTORYCATEGORY IN ('" + String.valueOf(MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_BRANDNEW) + "',"
                + "'" + String.valueOf(MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORYID_RECONDITIONED) + "') "
                + "AND S.STATUSCODE = '" + MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED + "' ";
        return where;
    }

    private class AccessoryTableViewRowMapper implements RowMapper<JSONObject> {

        @Override
        public JSONObject mapRow(ResultSet rs, int i) throws SQLException {
            JSONObject object = new JSONObject();

            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><input id='" + rs.getInt("INVENTORYHARDWAREITEMID") + "' name='accessory' type='checkbox' class='form-check-input' value='" + rs.getInt("INVENTORYHARDWAREITEMID") + "'></div>"
                    + "</div>";

            object.put("action", action);
            object.put("inventoryhardwareitemid", rs.getInt("INVENTORYHARDWAREITEMID"));
            object.put("inventorycategory", rs.getString("INVENTORYCATEGORY"));
            object.put("partcode", rs.getString("PARTCODE"));
            object.put("partname", rs.getString("PARTNAME"));
            object.put("posrevision", rs.getString("POSREVISION"));
            object.put("posptid", rs.getString("POSPTID"));
            object.put("posserialnumber", rs.getString("POSSERIALNUMBER"));

            return object;
        }
    }

    @Override
    public boolean insertInvenotyHardwareHistory(Inventoryhardwarehistory inventoryhardwarehistory) throws HibernateException, SQLException {

        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            session.save(inventoryhardwarehistory);
        } catch (Exception e) {
            status = false;
            throw e;
        }
        return status;
    }

}
