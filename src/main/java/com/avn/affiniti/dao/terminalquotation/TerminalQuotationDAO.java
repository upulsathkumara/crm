/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.terminalquotation;

import com.avn.affiniti.hibernate.model.Terminalquotation;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : TerminalQuotationDAO
 * @Created on : Jul 18, 2017, 12:30:40 AM
 */
public interface TerminalQuotationDAO {

    public int createTerminalQuotation(Terminalquotation terminalquotation) throws HibernateException;

    public void updateTerminalQuotation(Terminalquotation terminalquotation) throws HibernateException;

    public void createOrUpdateTerminalQuotation(Terminalquotation terminalquotation) throws HibernateException;

    public Terminalquotation getLatestTerminalQuotationByTicket(int ticketid) throws HibernateException;

}
