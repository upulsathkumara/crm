/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.terminalquotation;

import com.avn.affiniti.hibernate.model.Terminalquotation;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author : Roshen Dilshan
 * @Document : TerminalQuotationDAOImpl
 * @Created on : Jul 18, 2017, 12:31:35 AM
 */
@Repository("terminalQuotationDAO")
public class TerminalQuotationDAOImpl implements TerminalQuotationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int createTerminalQuotation(Terminalquotation terminalquotation) throws HibernateException {
        sessionFactory.getCurrentSession().save(terminalquotation);
        return terminalquotation.getTerminalquotationid();
    }

    @Override
    public void updateTerminalQuotation(Terminalquotation terminalquotation) throws HibernateException {
        sessionFactory.getCurrentSession().update(terminalquotation);
    }

    @Override
    public void createOrUpdateTerminalQuotation(Terminalquotation terminalquotation) throws HibernateException {
        sessionFactory.getCurrentSession().saveOrUpdate(terminalquotation);
    }

    @Override
    public Terminalquotation getLatestTerminalQuotationByTicket(int ticketid) throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM Terminalquotation TQ "
                + "WHERE TQ.terminalquotationid = ("
                + "SELECT MAX(terminalquotationid) "
                + "FROM Terminalquotation "
                + "WHERE ticket.ticketid = :ticket)");
        query.setInteger("ticket", ticketid);
        return (Terminalquotation) query.uniqueResult();
    }

}
