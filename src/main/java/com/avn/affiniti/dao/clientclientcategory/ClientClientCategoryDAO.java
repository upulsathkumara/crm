/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.clientclientcategory;

import com.avn.affiniti.hibernate.model.Clientclientcategory;
import org.hibernate.HibernateException;

/**
 * @Author Nadun Chamikara
 * @Document ClientClientCategoryDAO
 * @Created on 18/12/2017, 12:07:57 PM
 */
public interface ClientClientCategoryDAO {

    public void createOrUpdateClientClientCategory(Clientclientcategory clientclientcategory) throws HibernateException, Exception;

    public void deleteClientClientCategoriesByClientId(int clientId) throws HibernateException, Exception;

}
