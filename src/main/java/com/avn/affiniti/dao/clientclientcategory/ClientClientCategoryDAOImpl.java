/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.dao.clientclientcategory;

import com.avn.affiniti.hibernate.model.Clientclientcategory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Author Nadun Chamikara
 * @Document ClientClientCategoryDAOImpl
 * @Created on 18/12/2017, 12:08:09 PM
 */
@Repository("clientClientCategoryDAO")
public class ClientClientCategoryDAOImpl implements ClientClientCategoryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createOrUpdateClientClientCategory(Clientclientcategory clientclientcategory) throws HibernateException, Exception {
        sessionFactory.getCurrentSession().saveOrUpdate(clientclientcategory);
    }

    @Override
    public void deleteClientClientCategoriesByClientId(int clientId) throws HibernateException, Exception {
        Query query = sessionFactory.getCurrentSession().createQuery("DELETE Clientclientcategory WHERE client.clientid = :clientid");
        query.setInteger("clientid", clientId);
        query.executeUpdate();
    }

    
}
