/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.accessory;

import com.avn.affiniti.dao.accessory.AccessoryAssignDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.ticket.TicketDAO;
import com.avn.affiniti.hibernate.model.Inventoryhardwarehistory;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASHOK
 */
@Service("accessoryAssignService")
public class AccessoryAssignServiceImpl implements AccessoryAssignService {

    @Autowired
    private AccessoryAssignDAO accessoryAssignDAO;
    @Autowired
    private SystemuserDAO systemuserDAO;
    @Autowired
    private TicketDAO ticketDAO;

    @Override
    @Transactional
    public JSONObject getTicketTableData(HttpServletRequest request, String loginUser) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        Systemuser systemUser = systemuserDAO.getSytemuserByUsername(loginUser);
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)
        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            iTotalRecords = accessoryAssignDAO.getTicketTableDataCount(systemUser);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = accessoryAssignDAO.getTicketTableData(systemUser, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;
    }

    @Override
    @Transactional
    public JSONObject getAccessoryTableData(HttpServletRequest request, String username) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)
        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();        
        JSONArray rows = new JSONArray();
        int ticketId = Integer.parseInt(request.getParameter("ticketId"));
        Ticket ticket = ticketDAO.getTicket(ticketId);
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
            if (Boolean.valueOf(request.getParameter("search"))) {

                iTotalRecords = accessoryAssignDAO.getAccessoryTableDataCount(ticket, systemuser);//Total Records for searched option
                iTotalDisplayRecords = iTotalRecords;
                if (iTotalRecords > 0) {
                    list = accessoryAssignDAO.getAccessoryTableData(ticket, systemuser, param.iDisplayLength, param.iDisplayStart);
                    rows = new JSONArray(list);
                }
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;
    }
}
