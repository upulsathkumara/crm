/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.accessory;

import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Ticket;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author ASHOK
 */
public interface AccessoryAssignService {
    
    public JSONObject getTicketTableData(HttpServletRequest request, String loginUser) throws SQLException, Exception;
    
    public JSONObject getAccessoryTableData(HttpServletRequest request, String username) throws SQLException, Exception;
    
}
