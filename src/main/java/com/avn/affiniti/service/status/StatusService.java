/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.status;

import com.avn.affiniti.hibernate.model.Status;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author acer
 */
public interface StatusService {
    //public JSONObject getStatusByStatusCategory(String statusCategoryId) throws SQLException;

    public JSONObject getStatussByStatusId(int statusId,HttpSession session);

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException;

   // public JSONObject saveStatus(com.avn.affiniti.hibernate.model.Status status_info) throws SQLException, Exception;    

 
    public JSONObject createOrUpdateStatus(Status status, String createdUser, String input, String id, String setSelectFunction) throws Exception;

    public JSONObject getStatusTableData(HttpServletRequest request,String createdUser) throws SQLException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

   // public void deleteStatus(int statusId) throws Exception;
        public void deleteStatus(int status) throws HibernateException, SQLException, Exception;

}
