/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.status;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Statuscategory;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author :acer
 * @Document :StatusServiceImpl
 * @Created on:Aug 15, 2017,3:04:50 PM
 */
@Service("statusService")
public class StatusServiceImpl implements StatusService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private StatusDAO statusDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Override
    @Transactional
    public JSONObject getStatusTableData(HttpServletRequest request, String createdUser) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        String page = "Status";
        String task = "";
        String description = "";
        String affectedId = "";

        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            String parameters = request.getParameter("searchoptionID");//Setting up searched Section Task ID

            if (parameters.equals("") || parameters.equals("--Select--")) {
                task = "Table View";
                description = "Table View all details in Status ";
                affectedId = "";
            } else {
                Statuscategory statuscategory = statusDAO.getStatuscategory(Integer.parseInt(String.valueOf(parameters)));
                // .getProductCategoryByCategoryId(Integer.parseInt(String.valueOf(parameters)));
                task = "Table Search";
                description = "Search Details in Status Table by '" + statuscategory.getDescription() + "' Category";
                affectedId = parameters;

            }
            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

            Statuscategory statuscategory = new Statuscategory();
            if (!(parameters.trim()).isEmpty()) {
                statuscategory.setStatuscategoryid(Integer.parseInt(parameters));
            } else {
                statuscategory = null;
            }

            iTotalRecords = statusDAO.getTableDataCount(statuscategory);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = statusDAO.getTableData(statuscategory, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
//                for (JSONObject status : list) {
//                    String action = "<div class=\"row\">"
//                            + "<div class=\"col-xs-3\"><a href='#' id='" + section.getSectionid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
//                            + "<div class=\"col-xs-3\"><a href='#' id='" + section.getSectionid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
//                            + "</div>";
//                    JSONObject object = new JSONObject();
//                    object.put("action", action);
//                    object.put("sectionId", (section.getSectionid()) != null ? section.getSectionid() : "N/A");
//                    object.put("sectionDes", section.getDescription().equals("") ? "N/A" : section.getDescription());
//                    object.put("sectionlevel", (String.valueOf(section.getSectionlevel())) != null ? section.getSectionlevel() : "N/A");
//                    try {
//                        object.put("parentSection", section.getSection().getDescription());
//                    } catch (NullPointerException e) {
//                        object.put("parentSection", "N/A");
//                    }
//                    object.put("sectionIcon", (section.getIcon()) != null ? section.getIcon() : "N/A");
//                    object.put("statusDes", (section.getStatus().getDescription()) != null ? section.getStatus().getDescription() : "N/A");

//                    rows.put(status);
//                }
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
            throw e;
        }

        return jsonResponse;
    }

//loading data from data base to table
//    @Override
//    public JSONObject getStatusByStatusCategory(String statusCategoryId) throws SQLException {
//
//        JSONArray array = new JSONArray();
//        JSONObject output = new JSONObject();
//
//        try {
//            for (Status s : statusDAO.getStatusByStatusCategory(statusCategoryId)) {
//                JSONObject object = new JSONObject();
//                object.put("actions", s.getStatusid().toString());
//                object.put("statusid", s.getStatusid().toString());
//                object.put("statusdescription", String.valueOf(s.getDescription()));
//                object.put("statuscategory", String.valueOf(s.getStatuscategory().getDescription()));
//                object.put("statuscode", String.valueOf(s.getStatuscode()));
//                object.put("lastupdatedtime", s.getLastupdateddatetime().toString());
//                object.put("createdtime", s.getCreateddatetime().toString());
//                object.put("createduser", s.getCreateduser().toString());
//            //object.put("sortId",s.getSortid());
//
//                array.put(object);
//
//                output.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
//                output.put("success", true);
//                output.put("message", "");
//            }
//        } catch (Exception ex) {
//            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("message", "ERROR");
//        }
//        output.put("data", array);
//        return output;
//    }
    //
    @Override
    @Transactional
    public JSONObject getStatussByStatusId(int statusId,HttpSession session) {
        JSONObject object = new JSONObject();
        JSONObject output = new JSONObject();
        
         String page = "Status";
        String task = "";
        String affectedId = "";
        String description = "";
        //String createdUser = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        output.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("success", true);
        output.put("message", "");

        try {
            Status s = statusDAO.getStatsuById(statusId);

            object.put("id", s.getStatusid().toString());
            object.put("description", s.getDescription().toString());
            object.put("statuscode", s.getStatuscode().toString());
            object.put("statuscategory", s.getStatuscategory().getStatuscategoryid().toString());
            object.put("sortId", s.getSortid());
            object.put("isfinalstatus", s.getIsfinalstatus());
            object.put("isinitialstatus", s.getIsinitialstatus());

            object.put("lastupdatedtime", s.getLastupdateddatetime().toString());
            object.put("createdtime", s.getCreateddatetime().toString());
            object.put("createduser", s.getCreateduser().toString());
            
             task = "View";

//affectedId = String.valueOf(id);
            int status1 = s.getStatuscategory().getStatuscategoryid();
            Statuscategory sc = statusDAO.getStatuscategory(Integer.parseInt(String.valueOf(status1)));
            description = "Viewed  Single Recorde  in Status Table Status ID of '" + s.getStatusid()
                    + " '  Status Categegory of ' " + sc.getDescription() + " ' Status Code of ' " + s.getStatuscode()
                    + " ' " + " ' Sort ID of ' " + s.getSortid()+ " ' Is Initial Status of ' "+s.getIsinitialstatus()+" ' "
                    + " ' Is Final Status of ' "+s.getIsfinalstatus()+ " ' And Description ' "+s.getDescription()+" '";
            // +pc.getDescription() + " ' Product Name of ' " + product.getDescription() + " ' And Status of ' " + status.getDescription() + " '";
            // product.setCreateddatetime(new Date());

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

        } catch (HibernateException ex) {
            System.out.println(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", "Error : Database Error");
        } catch (Exception ex) {
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", "Error : Error");
        }

        output.put("data", object);

        return output;
    }

    //Loading the status category dropdown
    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException {
        model.put("statusList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUSCATEGORY)));

//        model.put("status", commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTION));
    }

    @Override
    @Transactional
    public JSONObject createOrUpdateStatus(Status status, String createdUser, String input, String id, String setSelectFunction) throws Exception {
        String page = "Status";
        String task = "";
        String affectedId = "";
        String description = "";
        //int productId = 0;
        boolean emptyTasks = false;

        if (id != "null") {
            int status1 = status.getStatuscategory().getStatuscategoryid();
            Statuscategory sc = statusDAO.getStatuscategory(Integer.parseInt(String.valueOf(status1)));

            description = "Status table is Updated in Status ID of  '" + id + " ' Status category as ' " + sc.getDescription()
                    + " '  Status code of ' " + status.getStatuscode() + " ' Sort Id of ' " + status.getSortid().toString()
                    + " ' Is initial status of ' " + status.getIsinitialstatus().toString() + " ' Is final status of ' "
                    + status.getIsfinalstatus().toString() + " ' and Description as" + status.getDescription()+" '";
            task = "update";
            affectedId = String.valueOf(input);
            Status tempStatus = statusDAO.getStatsuById(Integer.parseInt(String.valueOf(id)));
            status.setLastupdateddatetime(new Date());
            status.setCreateduser(tempStatus.getCreateduser());
            status.setCreateddatetime(tempStatus.getCreateddatetime());
            statusDAO.createStatus(status, setSelectFunction);
            //tempStatus.
            //status.setLastupdateddatetime(new Date());
            //status.setCreateddatetime(new Date());
            // status.setCreateduser(createdUser);
            //statusDAO.createStatus(status, setSelectFunction);
        } else {
            task = "add";
            int status1 = status.getStatuscategory().getStatuscategoryid();

            Statuscategory sc = statusDAO.getStatuscategory(Integer.parseInt(String.valueOf(status1)));

            description = "Added new recode into Status table. By Status category as ' " + sc.getDescription() + " '  Status code of ' " + status.getStatuscode()
                    + " ' Sort Id of ' " + status.getSortid().toString() + " ' Is initial status of ' "
                    + status.getIsinitialstatus().toString() + " ' Is final status of ' " + status.getIsfinalstatus().toString()
                    + " ' and Description as '" + status.getDescription()+" '";
            //+sc.getDescription();
            affectedId = String.valueOf(input);
            status.setCreateddatetime(new Date());
            status.setCreateduser(createdUser);
            status.setLastupdateddatetime(new Date());
            statusDAO.createStatus(status, setSelectFunction);

        }

//        try {
//        } catch (Exception ex) {
//            Logger.getLogger(StatusServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
        JSONObject message = new JSONObject();
        //message.put("message", "Status saved");
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        return message;

    }

//    @Override
//    @Transactional
//    public JSONObject createOrUpdateStatus(Status status, String createdUser, String input) throws Exception {
//        String page = "status";
//        String task = "";
//        String affectedId = "";
//        String description = "";
//        int productId = 0;
//        boolean emptyTasks = false;
//
//        Integer statuscategoryid = status.getStatusid();
//
//        if (!input.equals("")) {
////        if (productcategoryid != null) {
//            task = "add";
//            description = "Added new recode into Status table by status id of  " + input;
//            affectedId = String.valueOf(input);
//        } else {
//
//            description = "Status table is Updated by status id of  " + input;
//            task = "update";
//            affectedId = String.valueOf(input);
//        }
//
////        try {
//        /////////////////////////////////////////////////
//        // boolean isUpdate = false;
//        //Status s;
//
//      //  if (status.getStatusid().equals("")) { // update
//            //isUpdate = true;
//             //statusDAO.createOrUpdateStatus(status);
//       // } else {
//                       //  statusDAO.createOrUpdateStatus(status);
//
//      //  }
//        
//        
//        
//        
//        
//        /////////////////////////////
//        status.setCreateddatetime(new Date());
//        status.setLastupdateddatetime(new Date());
//        status.setCreateduser(createdUser);
//        statusDAO.createOrUpdateStatus(status);
////        } catch (Exception ex) {
////            Logger.getLogger(StatusServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
////            ex.printStackTrace();
////        }
//        JSONObject message = new JSONObject();
//        //message.put("message", "Status saved");
//        this.insertAuditTrace(page, affectedId, task, description, createdUser);
//
//        return message;
//
//    }
    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    @Transactional

    public void deleteStatus(int status) throws HibernateException, SQLException, Exception {
        statusDAO.deleteStatus(status);
    }

}
