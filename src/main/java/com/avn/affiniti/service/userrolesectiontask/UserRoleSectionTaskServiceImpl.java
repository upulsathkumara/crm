package com.avn.affiniti.service.userrolesectiontask;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.userrolesectiontask.UserRoleSectionTaskDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Userrolesectiontask;
import com.avn.affiniti.hibernate.model.UserrolesectiontaskId;
import com.avn.affiniti.model.userolesectiontask.UserRoleSectionTask;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author :Kaushan Fernando
 * @Document :UserRoleSectionTaskServiceImpl
 * @Created on:Oct 13, 2017,3:00:10 PM
 */
@Service("userRoleSectionTaskService")
public class UserRoleSectionTaskServiceImpl implements UserRoleSectionTaskService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private UserRoleSectionTaskDAO userRoleSectionTaskDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Override
    @Transactional
    public JSONObject getUserRoleSectionsByUserRoleId(int userRoleId) {
        JSONArray data = new JSONArray();
        JSONObject objData = new JSONObject();
        try {

            data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLESECTIONS_BY_USERROLE_ID,
                    userRoleId
            ));
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            objData.put("success", true);
            objData.put("message", "Successfull");
            objData.put("data", data);
        } catch (HibernateException | SQLException sqle) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sections - please try again!");
        } catch (Exception e) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sections - please try again!");
        }

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getUserRoleSubSectionsBySectionId(int sectionId, int userRoleId) {
        JSONArray data = new JSONArray();
        JSONObject objData = new JSONObject();
        try {

            data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUBSECTIONS_BY_USERROLE_ID_AND_SECTION_ID,
                    userRoleId, sectionId
            ));
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            objData.put("success", true);
            objData.put("message", "Successfull");
            objData.put("data", data);
        } catch (HibernateException | SQLException sqle) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sub sections - please try again!");
        } catch (Exception e) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sub sections - please try again!");
        }

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getTaskLIstById(int userRoleId, int sectionId,HttpSession session) {

        JSONArray dataToRemoveSelect;
        JSONArray data2ToRemoveSelect;
        JSONArray data;
        JSONArray data2;
        JSONArray data3;
        JSONObject objData = new JSONObject();

        String page = "User Role Section Task";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        String elementIdToRemove = "";

        try {
            //   remove select element from DropDown JsonArray on assigned tasks
            dataToRemoveSelect = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PENDING_SECTIONTASKS_BY_USERROLE_ID_AND_SECTION_ID,
                    sectionId, userRoleId, sectionId
            ));
            data = Common.removeDropDownElementFromJsonArray(elementIdToRemove, dataToRemoveSelect);

            //   remove select element from DropDown JsonArray on assigned tasks
            data2ToRemoveSelect = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNED_SECTIONTASKS_BY_USERROLE_ID_AND_SECTION_ID,
                    userRoleId, sectionId
            ));

            data2 = Common.removeDropDownElementFromJsonArray(elementIdToRemove, data2ToRemoveSelect);

            data3 = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTIONS_BY_SECTION_ID,
                    sectionId
            ));

            task = "View";

            description = "Viewed  Single Recorde  in User Role Section Table User Role Section ID of '" + sectionId
                    + " '  User Role Id of ' " +userRoleId;

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            objData.put("success", true);
            objData.put("message", "Successfull");
            objData.put("data", data);
            objData.put("data2", data2);
            objData.put("data3", data3);

        } catch (HibernateException | SQLException sqle) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading Tasks - please try again!");
        } catch (Exception e) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading Tasks - please try again!");
        }

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getUserRoleTaskTableData(HttpServletRequest request, String createdUser) throws HibernateException, SQLException, JSONException, Exception {

        //        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
//        String ViewStatus = "active";
//        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        String page = "UserRoleSectionTask";
        String task = "";
        String description = "";
        String affectedId = "";

//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }
//            if (Boolean.valueOf(request.getParameter("search"))) {

        String userRoleId = request.getParameter("userroleid");

        if (userRoleId.equals("") || userRoleId.equals("--Select--")) {
            task = "Table View";
            description = "Table View all details in UserRoleSectionTask ";
            affectedId = "";
        } else {
            task = "Table Search";
            description = "Table Search details in UserRoleSectionTask by userRole Id " + userRoleId;
            affectedId = userRoleId;

        }
        insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        iTotalRecords = userRoleSectionTaskDAO.getTableDataCount(userRoleId);//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = userRoleSectionTaskDAO.getTableData(userRoleId, param.iDisplayLength, param.iDisplayStart);

            rows = new JSONArray(list);

        }

        jsonResponse.put("status", 200);
        jsonResponse.put("success", true);
        jsonResponse.put("message", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        return jsonResponse;
    }

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) {
        try {
            model.put("userRoleList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLE,
                    MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
            )));

            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            model.put("success", true);
            model.put("message", "Successfull");

        } catch (HibernateException | SQLException sqle) {
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Database Error");
        } catch (Exception e) {
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Error");
        }

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject saveUserRoleSectionTask(UserRoleSectionTask[] userRoleSecionTasks, List<Userrolesectiontask> listOfOldObject, String createdUser, String selectFunction) throws HibernateException, SQLException, JSONException, Exception {

        JSONObject object = new JSONObject();

        String page = "UserRoleSectionTask";
        String task = "";
        String affectedId = "";
        String description = "";
        int userRoleSectionId = 0;
        boolean removeTask = true;
        boolean saveOrNot = true;
        for (Userrolesectiontask usrt : listOfOldObject) {

            int oldTaskId = usrt.getTask().getTaskid();
            int oldUserRoleSectionId = usrt.getId().getUserrolesectionid();
            for (UserRoleSectionTask urt : userRoleSecionTasks) {
                UserRoleSectionTask userRoleSectionTask = new UserRoleSectionTask();

                userRoleSectionTask.setTaskId(urt.getTaskId());
                int newTaskId = Integer.parseInt(userRoleSectionTask.getTaskId());

                if (oldTaskId == newTaskId) {

                    removeTask = false;

                }

            }
            if (removeTask) {

                long taskAvailability = userRoleSectionTaskDAO.checkUserRoleSectionTaskAvailability(oldUserRoleSectionId, oldTaskId);
                if (taskAvailability != 0) {
                    userRoleSectionTaskDAO.deleteUserroletask(oldUserRoleSectionId, oldTaskId);
                }

            }
            removeTask = true;

        }
        for (UserRoleSectionTask urt : userRoleSecionTasks) {

            UserRoleSectionTask userRoleSectionTask = new UserRoleSectionTask();

            userRoleSectionTask.setUserroleId(urt.getUserroleId());
            int userRoleId = Integer.parseInt(userRoleSectionTask.getUserroleId());

            userRoleSectionTask.setSectionId(urt.getSectionId());
            int sectionId = Integer.parseInt(userRoleSectionTask.getSectionId());

            userRoleSectionTask.setTaskId(urt.getTaskId());
            int newTaskId = Integer.parseInt(userRoleSectionTask.getTaskId());

            userRoleSectionId = this.findUserroleSectionId(userRoleId, sectionId);
            affectedId = String.valueOf(userRoleSectionId);

            if (newTaskId != 0) {

                for (Userrolesectiontask usrt : listOfOldObject) {
                    int oldTaskId = usrt.getTask().getTaskid();

                    if (newTaskId == oldTaskId) {
                        saveOrNot = false;
                    }

                }
                if (selectFunction.equals("save")) {
                    description = "Assigned tasks to userrolesectiontask by userrolesectionid : " + userRoleSectionId + " and task ids : " + newTaskId;
                    task = "assign";
                    object.put("message", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Tasks"));
                    selectFunction = "doneselection";
                } else if (selectFunction.equals("edit")) {

                    description = "Updated task from User Role Section Task by User Role Section ID : " + userRoleSectionId + " and old task ids : ";

                    for (Userrolesectiontask usrt : listOfOldObject) {

                        int oldTaskId = usrt.getTask().getTaskid();

                        description = description + " " + oldTaskId + " , ";

                    }
                    description = description + " new task ids " + newTaskId;
                    task = "update";
                    object.put("message", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Tasks"));
                    selectFunction = "doneselection";

                } else if (selectFunction.equals("doneselection")) {
                    description = description + " , " + newTaskId;
                }
                if (saveOrNot) {

                    Userrolesectiontask userRoleSectionTaskToSave = new Userrolesectiontask();
                    UserrolesectiontaskId userRoleSectionTaskId = new UserrolesectiontaskId(userRoleSectionId, newTaskId);

                    userRoleSectionTaskToSave.setId(userRoleSectionTaskId);// set userrolesubsectionid and taskId into userroletask
                    userRoleSectionTaskToSave.setCreateddatetime(new Date());
                    userRoleSectionTaskToSave.setLastupdateddatetime(new Date());
                    userRoleSectionTaskToSave.setCreateduser(createdUser);
                    userRoleSectionTaskDAO.saveUserRoleSectionTask(userRoleSectionTaskToSave);

                }
                saveOrNot = true;
            } else {
                description = "removed tasks from userrolesectiontask by userrolesectionid : " + userRoleSectionId + " and old task ids : ";
                for (Userrolesectiontask usrt : listOfOldObject) {

                    int oldTaskId = usrt.getTask().getTaskid();

                    description = description + " " + oldTaskId + " , ";

                }
                selectFunction = "done";
                task = "remove all tasks";
                object.put("message", String.format(MessageVarList.ALL_REMOVED_SUCCESSFULLY, "Tasks"));
            }

        }
        this.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        object.put(
                "status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        object.put(
                "success", true);

        return object;
    }

    @Override
    @Transactional
    public int findUserroleSectionId(int userRoleId, int sectionId) throws SQLException {
        int userRoleSectionId = 0;
        userRoleSectionId = userRoleSectionTaskDAO.findUserroleSectionId(userRoleId, sectionId);

        return userRoleSectionId;

    }

    @Override
    @Transactional
    public List<Userrolesectiontask> getUserRoleSectionTaskList(int userRoleSectionId) throws HibernateException, SQLException {
        List<Userrolesectiontask> listOfOldObject = userRoleSectionTaskDAO.getOldUserRoleTaskObject(userRoleSectionId);
        return listOfOldObject;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
