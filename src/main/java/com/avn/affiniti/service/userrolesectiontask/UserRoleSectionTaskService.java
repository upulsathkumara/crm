/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrolesectiontask;

import com.avn.affiniti.hibernate.model.Userrolesectiontask;
import com.avn.affiniti.model.userolesectiontask.UserRoleSectionTask;
import java.util.Map;
import org.json.JSONObject;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;

/**
 *
 * @author :Kaushan Fernando
 * @Document :UserRoleSectionTaskService
 * @Created on:Oct 13, 2017,2:57:31 PM
 */
public interface UserRoleSectionTaskService {

    public void loadPageComponent(Map<String, Object> model);

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

    public JSONObject getUserRoleSectionsByUserRoleId(int userRoleId);

    public JSONObject getUserRoleSubSectionsBySectionId(int sectionId, int userRoleId);

    public JSONObject getTaskLIstById(int userRoleId, int sectionId,HttpSession session);

    public List<Userrolesectiontask> getUserRoleSectionTaskList(int userRoleSectionId) throws HibernateException, SQLException;

    public int findUserroleSectionId(int userRoleId, int sectionId) throws SQLException;

    public JSONObject getUserRoleTaskTableData(HttpServletRequest request, String createdUser) throws HibernateException, SQLException, JSONException, Exception;

    public JSONObject saveUserRoleSectionTask(UserRoleSectionTask[] userRoleSectionTasks, List<Userrolesectiontask> listOfOldObject, String createdUser, String selectFunction)throws HibernateException, SQLException, JSONException, Exception;

}
