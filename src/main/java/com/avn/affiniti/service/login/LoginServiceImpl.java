package com.avn.affiniti.service.login;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.login.LoginDAO;
import com.avn.affiniti.dao.user.UserDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import java.sql.SQLException;
import java.util.Date;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Roshen Dilshan on 7/11/2016.
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginDAO loginDAO;
    @Autowired
    UserDAO userDAO;
    @Autowired
    AuditTraceDAO auditTraceDAO;

    @Override
    @Transactional
    public Date getLastLoginDateTime(String username) throws Exception {
        return loginDAO.getLastLoginDateTime(username);
    }

    @Override
    @Transactional
    public void updateLastLogin(String username, Date logintime) throws Exception {
        loginDAO.updateLastLogin(username, logintime);
    }

    @Override
    @Transactional
    public JSONArray getAssignedTaskList(int userroleid) throws Exception {
        return loginDAO.getAssignedTaskList(userroleid);
    }

    @Override
    @Transactional
    public boolean isActivieUser(String username) throws SQLException {
        return userDAO.isActivieUser(username);
    }

    @Override
    @Transactional
    public boolean isUserExist(String username) throws SQLException {
        return userDAO.isUserExist(username);
    }

    @Override
    @Transactional
    public boolean isValidUser(String username, String password) throws Exception {
        return userDAO.isValidUser(username, password);
    }

    @Override
    @Transactional
    public boolean isFirstLogin(String username) throws Exception {
        return userDAO.isFirstLogin(username);
    }

    @Override
    @Transactional
    public void firstTimePasswordChage(String username, String password) throws Exception {
        userDAO.chagePassword(username, password);
        userDAO.updateFirstLogin(username, 0);
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
