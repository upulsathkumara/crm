package com.avn.affiniti.service.login;

import java.sql.SQLException;
import java.util.Date;
import org.json.JSONArray;

/**
 * Created by Roshen Dilshan on 7/11/2016.
 */
public interface LoginService {

    public Date getLastLoginDateTime(String username) throws Exception;

    public void updateLastLogin(String username, Date logintime) throws Exception;

    public JSONArray getAssignedTaskList(int userroleid) throws Exception;

    public boolean isActivieUser(String username) throws SQLException;

    public boolean isUserExist(String username) throws SQLException;

    public boolean isValidUser(String username, String password) throws Exception;

    public boolean isFirstLogin(String username) throws Exception;

    public void firstTimePasswordChage(String username, String password) throws Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

}
