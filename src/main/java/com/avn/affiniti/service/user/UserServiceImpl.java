/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.user;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.employee.EmployeeDAO;
import com.avn.affiniti.dao.employeeproduct.EmployeeProductDAO;
import com.avn.affiniti.dao.employeeproductcategorytype.EmployeeProductCategoryTypeDAO;
//import com.avn.affiniti.dao.mail.MailDAO;
import com.avn.affiniti.dao.supervisor.SupervisorDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.territorymapemployee.TerritorymapEmployeeDAO;
import com.avn.affiniti.dao.user.UserDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Employeeproduct;
import com.avn.affiniti.hibernate.model.EmployeeproductId;
import com.avn.affiniti.hibernate.model.Employeesupervisor;
import com.avn.affiniti.hibernate.model.EmployeesupervisorId;
import com.avn.affiniti.hibernate.model.Location;
import com.avn.affiniti.hibernate.model.Organizationhierarchy;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Userrole;
//import com.avn.affiniti.model.mail.Mail;
import com.avn.affiniti.model.user.User;
import com.avn.affiniti.service.mail.Mail;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author : Roshen Dilshan
 * @Document : UserServiceImpl
 * @Created on : Jul 15, 2016, 11:43:20 AM
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    CommonDAO commonDAO;
    @Autowired
    UserDAO userDAO;
    @Autowired
    EmployeeProductDAO employeeProductDAO;
    @Autowired
    TerritorymapEmployeeDAO territorymapEmployeeDAO;
    @Autowired
    EmployeeProductCategoryTypeDAO employeeProductCategoryTypeDAO;
    @Autowired
    SupervisorDAO supervisorDAO;
//    @Autowired
//    MailDAO mailDAO;

    @Autowired
    SystemuserDAO systemuserDAO;

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    AuditTraceDAO auditTraceDAO;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model, HttpSession session) throws SQLException {
        model.put("hierarchylist", commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_ORGANIZATIONHIERARCHY));
        model.put("statuslist", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY, MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT)));
        model.put("productlist", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCT, MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))));

        Map<String, String> userRoleList;

        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
//        int userroleid = systemuserDAO.getSytemuserByUsername(username).getUserrole().getUserroleid();
        int userroleid = this.getUserRoleByUsername(username);
        userRoleList = commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLE_BY_PARENTUSERROLE, userroleid));

        model.put("userrolelist", userRoleList);

    }

    @Override
    @Transactional
    public void loadPageComponentView(Map<String, Object> model, long employeeid) throws SQLException {
        model.put("productlist", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USER_EMPLOYEE_PRODUCT, employeeid))));
//        model.put("territorylist", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USER_TERRIROTYMAP_EMPLOYEE, employeeid))));
//        model.put("productcategorytypelist", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCTCATEGORYTYPE_BY_EMPLOYEE, employeeid))));
        model.put("supervisorlist", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUPERVISOR_BY_EMPLOYEE, employeeid))));
//        model.put("languageskillslist", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_LANGUAGE_SKILLS_BY_EMPLOYEE, employeeid))));
    }

    @Override
    public Map<String, String> getTerritoryListByOrgHierarchy(int orgHierarchy) throws SQLException {
        return commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERRITORY_BY_HIERARCHY, orgHierarchy));
    }

    @Override
    public Map<String, String> getTerritoryListByOrgHierarchyProduct(int orgHierarchy, Integer[] product) throws SQLException {
        return commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERRITORY_BY_HIERARCHY_PRODUCT, orgHierarchy, (Arrays.toString(product)).substring(1, (Arrays.toString(product)).length() - 1)));
    }

    @Override
    public Map<String, String> getProductCategoryTypesByProduct(Integer[] product) throws SQLException {
        return commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCTCATEGORYTYPE_BY_PRODUCT, (Arrays.toString(product)).substring(1, (Arrays.toString(product)).length() - 1)));
    }

    @Override
    public Map<String, String> getSupervisorByHierarchy(int orgHierarchy) throws SQLException {
        return commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUPERVISOR_BY_HIERARCHYID, orgHierarchy));
    }

    @Override
    public boolean isUserExists(User user) throws SQLException {
        return systemuserDAO.isUserExists(user.getUsername());
    }

    @Override
    @Transactional
    public int getTableDataCount(User user, HttpSession session) throws SQLException {
//        return systemuserDAO.getTableDataCount(user, isClient(session));
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        int userroleid = systemuserDAO.getSytemuserByUsername(username).getUserrole().getUserroleid();
        return systemuserDAO.getTableDataCount(user, userroleid);

    }

    @Override
    @Transactional
    public List<JSONObject> getTableData(User user, int start, int minCount, HttpSession session) throws Exception {
        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid("");
        audittrace.setAffectedpage("User search");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        if (user.getSearchKeyword() == null) {
            audittrace.setDescription("Viewed Data in User Table");

        } else {
            audittrace.setDescription("Searched Data in User Table by ' " + user.getSearchKeyword()+" '.");

        }
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("View");
        auditTraceDAO.insertAuditTrace(audittrace);

//        return systemuserDAO.getTableData(user, start, minCount, isClient(session));
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
//        int userroleid = systemuserDAO.getSytemuserByUsername(username).getUserrole().getUserroleid();
        int userroleid = this.getUserRoleByUsername(username);
        return systemuserDAO.getTableData(user, start, minCount, userroleid);

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public long insertUser(User user, HttpServletRequest request, HttpSession session) throws Exception {
//        long employeeid = userDAO.insertUser(user);
        Status status = new Status();
        status.setStatusid(user.getStatus());
        Organizationhierarchy organizationhierarchy = new Organizationhierarchy();
        organizationhierarchy.setHierachyid(user.getHierarchyid());

        Employee employee = new Employee();
        employee.setStatus(status);
        employee.setOrganizationhierarchy(organizationhierarchy);
        employee.setNameinfull(user.getNameinfull());
        employee.setInitials(user.getInitials());
        employee.setPreferredname(user.getPreferredname());
        employee.setSurename(user.getSurname());
//        employee.setEpf(user.getEpf());
        employee.setNic(user.getNic());
        employee.setEmail(user.getEmail());
        employee.setContactno01(user.getContactno01());
        employee.setContactno02(user.getContactno02());
        employee.setCreateddatetime(new Date());
        employee.setLastupdateddatetime(new Date());
        employee.setCreateduser(user.getCreateduser());

        Location location = systemuserDAO.getSytemuserByUsername(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))).getEmployee().getLocation();
        employee.setLocation(location);

        employeeDAO.saveOrUpdate(employee);

        user.setEmployeeid(employee.getEmployeeid());

        Systemuser systemuser = new Systemuser();
        systemuser.setEmployee(employee);
        systemuser.setUsername(String.valueOf(user.getUsername()));

        Userrole userrole = new Userrole();
        userrole.setUserroleid(user.getUserrole());
        systemuser.setUserrole(userrole);
//        systemuser.setLastlogin(null);
        systemuser.setUserattempts(0);
        systemuser.setFirstlogin(true);
//        systemuser.setUsertype(null);
//        systemuser.setOrganization(null);
        systemuser.setOrganizationhierarchy(organizationhierarchy);
        systemuser.setLastupdateddatetime(new Date());
        systemuser.setCreateddatetime(new Date());
        systemuser.setCreateduser(user.getCreateduser());
        systemuser.setTokencreatedtime(new Date());

        if (isClient(session)) {
            systemuser.setClientuser(1);
        } else {
            systemuser.setClientuser(0);
        }

//        String token = Long.toHexString(Double.doubleToLongBits(Math.random()));
//        token = Common.passwordHashed(token);
        String token = Common.getUserPasswordToken();
        String password = Common.passwordHashed(token);
        systemuser.setPassword(password);
        Status systemUserStatus = new Status();
        systemUserStatus.setStatusid(MasterDataVarList.AFFINITI_CODE_STATUS_ID_INACTIVE);
        systemuser.setStatus(systemUserStatus);

        systemuserDAO.createOrUpdateSystemUser(systemuser);

        if (user.getProducts() != null) {
//            employeeProductDAO.insertEmployeeProduct(user);
            for (int productId : user.getProducts()) {
                Employeeproduct employeeproduct = new Employeeproduct();
                EmployeeproductId employeeproductId = new EmployeeproductId(employee.getEmployeeid(), productId);
                employeeproduct.setId(employeeproductId);
                employeeproduct.setCreatedatetime(new Date());
                employeeproduct.setLastupdateddatetime(new Date());
                employeeproduct.setCreateduser(user.getCreateduser());
                employeeProductDAO.insertEmployeeProduct(employeeproduct);
            }
        }
//        if (user.getTerritories() != null) {
//            territorymapEmployeeDAO.insertTerritorymapEmployee(user);
//        }
//        if (user.getProductscategorytype() != null) {
//            employeeProductCategoryTypeDAO.insertEmployeeProductCategoryType(user);
//        }
        if (user.getSupervisors() != null) {
//            supervisorDAO.insertSupervisor(user);
            for (int supervisorId : user.getSupervisors()) {
                Employeesupervisor employeesupervisor = new Employeesupervisor();
                EmployeesupervisorId employeesupervisorId = new EmployeesupervisorId(employee.getEmployeeid(), supervisorId);
                employeesupervisor.setId(employeesupervisorId);
                employeesupervisor.setCreatedatetime(new Date());
                employeesupervisor.setLastupdateddatetime(new Date());
                employeesupervisor.setCreateduser(user.getCreateduser());
                supervisorDAO.insertSupervisor(employeesupervisor);
            }
        }
//        if (user.getLanguageskills() != null) {
//            userDAO.insertLanguagelanguageSkills(user);
//        }

        System.out.println("userrole: " + user.getUserrole());
//        if (user.getUserrole() == MasterDataVarList.AFFINITI_CODE_USERROLE_CLIENT_USER || user.getUserrole() == MasterDataVarList.AFFINITI_CODE_USERROLE_CLIENT_ADMIN) {
        Mail m = new Mail();
        m.setMailSender(mailSender);

        m.sendUserRegistrationMail(request, user.getEmail(), user.getPreferredname(), user.getUsername(), token);

//        }
        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid(employee.getEmployeeid().toString());
        audittrace.setAffectedpage("User create");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(user.getCreateduser()));
        audittrace.setDescription("User Created with EmployeeID= '" + employee.getEmployeeid() + "' - And Name of - " + employee.getNameinfull());
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("Create");
        auditTraceDAO.insertAuditTrace(audittrace);

        return employee.getEmployeeid();
    }

    @Override
    public void sendPasswordMail(User user) throws MailException, MessagingException {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(CommonVarList.NOTIFICATION_MAIL_FROM);
        simpleMailMessage.setTo(user.getEmail());
        simpleMailMessage.setSubject("Affiniti CRM Account Activation");
        String messagecontent = "Hello %s,\n"
                + "\n"
                + "Thank you for choosing Affiniti and we welcome you to the all new CRM platform. \n"
                + "\n"
                //                + "You may use following auto-generated credentials to access Affiniti and as a best practice we would suggest you to change your password at your earliest convenience.\n"
                //                + "\n"
                + "Username: %s\n"
                //                + "Password: %s\n"
                + "\n"
                + "Best Regards,\n"
                + "Affiniti Team";
        simpleMailMessage.setText(String.format(messagecontent,
                (user.getPreferredname().substring(0, 1)).toUpperCase() + (user.getPreferredname().substring(1)).toLowerCase(),
                user.getUsername()
        //                , 
        //                user.getPassword()
        ));
//        Mail mail = new Mail();
//        mail.setSimpleMailMessage(simpleMailMessage);
//        mail.setResources(null);
//        mailDAO.sendMail(mail);
    }

    @Override
    public int getUserRoleByUsername(String username) throws SQLException {
        return systemuserDAO.getUserRoleByUsername(username);
    }

    @Override
    @Transactional
    public void getUserData(long employeeid, Map<String, Object> model, HttpSession session) throws SQLException {
        User user = userDAO.getUserByEmployeeId(employeeid);
        model.put("user", user);

        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid(String.valueOf(employeeid));
        audittrace.setAffectedpage("User view/update page");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setDescription("User viewed Details of ' " + user.getNameinfull() + " 'Employee ID of = ' " + String.valueOf(employeeid) + " '");
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("View");
        auditTraceDAO.insertAuditTrace(audittrace);
    }

    @Override
    public User getUserByUsername(String username) throws HibernateException {
        Systemuser systemuser = systemuserDAO.getUserByUsername(username);
        User user = new User();
        user.setEmployeeid(systemuser.getEmployee().getEmployeeid());
        user.setHierarchyid(systemuser.getOrganizationhierarchy().getHierachyid());
        user.setUserrole(systemuser.getUserrole().getUserroleid());
        user.setStatus(systemuser.getStatus().getStatusid());
        user.setLastlogin(systemuser.getLastlogin());
        user.setUserattempts(systemuser.getUserattempts());

        return user;
    }

    @Override
    @Transactional
    public void updateUser(User user, HttpSession session) throws Exception {
//        userDAO.updateUser(user);
//        userDAO.updateUserAccount(user);
        int employeeId = Integer.parseInt(String.valueOf(user.getEmployeeid()));
        Status status = new Status();
        status.setStatusid(user.getStatus());
        Organizationhierarchy organizationhierarchy = new Organizationhierarchy();
        organizationhierarchy.setHierachyid(user.getHierarchyid());

        Employee employee = employeeDAO.getEmployeeById(employeeId);
        employee.setOrganizationhierarchy(organizationhierarchy);
        employee.setNameinfull(user.getNameinfull());
        employee.setInitials(user.getInitials());
        employee.setPreferredname(user.getPreferredname());
        employee.setSurename(user.getSurname());
//        employee.setEpf(user.getEpf());
        employee.setNic(user.getNic());
        employee.setEmail(user.getEmail());
        employee.setContactno01(user.getContactno01());
        employee.setContactno02(user.getContactno02());
        employee.setLastupdateddatetime(new Date());

        employeeDAO.saveOrUpdate(employee);

        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employeeId);
        systemuser.setEmployee(employee);
        systemuser.setStatus(status);
//        systemuser.setUsername(user.getUsername());

        Userrole userrole = new Userrole();
        userrole.setUserroleid(user.getUserrole());
        systemuser.setUserrole(userrole);
//        systemuser.setLastlogin(null);
//        systemuser.setUsertype(null);
//        systemuser.setOrganization(null);
        systemuser.setOrganizationhierarchy(organizationhierarchy);
        systemuser.setLastupdateddatetime(new Date());
        systemuser.setLastupdateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        systemuserDAO.createOrUpdateSystemUser(systemuser);

//        employeeProductDAO.deleteEmployeeProductByEmployee(user);
        employeeProductDAO.deleteEmployeeProductByEmployee(employee.getEmployeeid());

        if (user.getProducts() != null) {
            //            employeeProductDAO.insertEmployeeProduct(user);
            for (int productId : user.getProducts()) {
                Employeeproduct employeeproduct = new Employeeproduct();
                EmployeeproductId employeeproductId = new EmployeeproductId(employee.getEmployeeid(), productId);
                employeeproduct.setId(employeeproductId);
                employeeproduct.setCreatedatetime(new Date());
                employeeproduct.setLastupdateddatetime(new Date());
                employeeproduct.setCreateduser(user.getCreateduser());
                employeeProductDAO.insertEmployeeProduct(employeeproduct);
            }
        }

//        territorymapEmployeeDAO.deleteTerritorymapEmployeeByEmployee(user);
//        if (user.getTerritories() != null) {
//            territorymapEmployeeDAO.insertTerritorymapEmployee(user);
//        }
//        employeeProductCategoryTypeDAO.deleteEmployeeProductByEmployee(user);
//        if (user.getProductscategorytype() != null) {
//            employeeProductCategoryTypeDAO.insertEmployeeProductCategoryType(user);
//        }
//        supervisorDAO.deleteSupervisor(user);
        supervisorDAO.deleteSupervisorsByEmployee(employee.getEmployeeid());
        if (user.getSupervisors() != null) {
//            supervisorDAO.insertSupervisor(user);
            for (int supervisorId : user.getSupervisors()) {
                Employeesupervisor employeesupervisor = new Employeesupervisor();
                EmployeesupervisorId employeesupervisorId = new EmployeesupervisorId(employee.getEmployeeid(), supervisorId);
                employeesupervisor.setId(employeesupervisorId);
                employeesupervisor.setCreatedatetime(new Date());
                employeesupervisor.setLastupdateddatetime(new Date());
                employeesupervisor.setCreateduser(user.getCreateduser());
                supervisorDAO.insertSupervisor(employeesupervisor);
            }
        }

//        userDAO.deleteLanguagelanguageSkills(user.getEmployeeid());
//        if (user.getLanguageskills() != null) {
//            userDAO.insertLanguagelanguageSkills(user);
//        }
        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid(employee.getEmployeeid().toString());
        audittrace.setAffectedpage("User update");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(user.getCreateduser()));
        audittrace.setDescription("User update data in Employeeid = ' " + employee.getEmployeeid() + " '- And Name of- ' " + employee.getNameinfull() + " '.");
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("Update");
        auditTraceDAO.insertAuditTrace(audittrace);
    }

    @Override
    public Map<String, String> getUserSeletedProductList(long employeeid) throws SQLException {
        return Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USER_EMPLOYEE_PRODUCT, employeeid)));
    }

    @Override
    public Map<String, String> getUserSeletedTerritoryList(long employeeid) throws SQLException {
        return Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USER_TERRIROTYMAP_EMPLOYEE, employeeid)));
    }

    @Override
    public Map<String, String> getUserSeletedProductCategoryTypeList(long employeeid) throws SQLException {
        return Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCTCATEGORYTYPE_BY_EMPLOYEE, employeeid)));
    }

    @Override
    public Map<String, String> getUserSeletedSupervisorList(long employeeid) throws SQLException {
        return Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUPERVISOR_BY_EMPLOYEE, employeeid)));
    }

    @Override
    public boolean isNotaDuplicateEpf(String epf) throws SQLException {
        return employeeDAO.isNotaDuplicateEpf(epf);
    }

    @Override
    public boolean isNotaDuplicateNic(String nic) throws SQLException {
        return employeeDAO.isNotaDuplicateNic(nic);
    }

    @Override
    public boolean isNotaDuplicateEmail(String email) throws Exception {
        return employeeDAO.isNotaDuplicateEmail(email);
    }

    @Override
    @Transactional
    public boolean isNotaDuplicateEmailUpdate(String email, String employeeId) throws Exception {
        return employeeDAO.isNotaDuplicateEmailUpdate(email, employeeId);
    }

    @Override
    @Transactional
    public boolean isNotaDuplicateUsername(String username) throws Exception {
        return systemuserDAO.isNotaDuplicateUsername(username);
    }

    @Override
    @Transactional
    public boolean isNotaDuplicateUsernameUpdate(String username, String employeeId) throws Exception {
        return systemuserDAO.isNotaDuplicateUsernameUpdate(username, employeeId);
    }

    @Override
    @Transactional
    public void passwordReSet(String username, String password) throws SQLException {
        systemuserDAO.chagePassword(username, password);
        systemuserDAO.updateFirstLogin(username, 1);
    }

    @Override
    public void sendPasswordResetMail(User user) throws MailException, MessagingException {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(CommonVarList.NOTIFICATION_MAIL_FROM);
        simpleMailMessage.setTo(user.getEmail());
        simpleMailMessage.setSubject("Affiniti CRM Password Re-Set");
        String messagecontent = "Hello %s,\n"
                + "\n"
                + "We've received a request to reset your password. \n"
                + "\n"
                + "Please use below password to login.\n"
                + "\n"
                + "Password: %s\n"
                + "\n"
                + "Best Regards,\n"
                + "Affiniti Team";
        simpleMailMessage.setText(String.format(messagecontent,
                (user.getPreferredname().substring(0, 1)).toUpperCase() + (user.getPreferredname().substring(1)).toLowerCase(),
                user.getPassword()));
//        Mail mail = new Mail();
//        mail.setSimpleMailMessage(simpleMailMessage);
//        mail.setResources(null);
//        mailDAO.sendMail(mail);
    }

    @Override
    public int getHierarchyidIdByUsername(String username) throws SQLException {
        return systemuserDAO.getHierarchyidIdByUsername(username);
    }

    private boolean isClient(HttpSession session) {
        boolean isClient = false;
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
//        if (systemuserDAO.getSytemuserByUsername(username).getClientuser() == 1) {
        int userroleid = systemuserDAO.getSytemuserByUsername(username).getUserrole().getUserroleid();
        if (userroleid == MasterDataVarList.AFFINITI_CODE_USERROLE_CLIENT_USER || userroleid == MasterDataVarList.AFFINITI_CODE_USERROLE_CLIENT_ADMIN) {
            isClient = true;
        } else {
            isClient = false;
        }

        return isClient;
    }

    @Override
    @Transactional
    public int validateUserRegistrationToken(String username, String token, boolean isValidUser) {
//        try {
//            Systemuser systemuser = systemuserDAO.getSytemuserByUsername(email);
//            return (systemuser.getUsername().equalsIgnoreCase(email) && systemuser.getPassword().equalsIgnoreCase(token));
//        } catch (Exception e) {
//            return false;
//        }

//        try {
//            return userDAO.isValidUser(email, token);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
        /*
         0 => username and doesn't match
         1 => username and token matched
         2 => username and token matched and token time valid 
         */
        int code = 0;

        try {
//            if (userDAO.isValidUser(username, token)) {
            if (isValidUser) {

                code = 1;

                Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);
                if (new Date().getTime() - systemuser.getTokencreatedtime().getTime() < MasterDataVarList.AFFINITI_CODE_CLIENT_TOKEN_EXPIRE_TIME) {
                    code = 2;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);

        }

        return code;
    }

    @Override
    @Transactional
    public boolean completeUserRegistration(String username, String token, String password, boolean isValidUser) throws Exception {

        Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);
//            if (userDAO.isValidUser(email, token)) {
//                systemuser.setPassword(Common.passwordHashed(password));
//                systemuserDAO.createOrUpdateSystemUser(systemuser);
//            }
        if (this.validateUserRegistrationToken(username, token, isValidUser) == 2) {
            systemuser.setPassword(Common.passwordHashed(password));
            Status status = new Status();
            status.setStatusid(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE);
            systemuser.setStatus(status);
            systemuserDAO.createOrUpdateSystemUser(systemuser);

            return true;
        }

        return false;
    }

    @Override
    @Transactional
    public boolean resendRegistrationLink(String username, String token, HttpServletRequest request, boolean isValidUser) throws Exception {
//        if (userDAO.isValidUser(username, token)) {
        if (isValidUser) {

            Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);

//            String newtToken = Long.toHexString(Double.doubleToLongBits(Math.random()));
//            newtToken = Common.passwordHashed(newtToken);
            String newToken = Common.getUserPasswordToken();
            String password = Common.passwordHashed(newToken);
            systemuser.setPassword(password);
            systemuser.setTokencreatedtime(new Date());
            systemuserDAO.createOrUpdateSystemUser(systemuser);

            Mail m = new Mail();
            m.setMailSender(mailSender);
//            String url = "http://" + request.getServerName() + (request.getServerPort() == 0 ? "" : ":" + request.getServerPort()) + request.getContextPath();
//            String message = "<!DOCTYPE html><html><body>"
//                    + "Click <a href='" + url + "/client/register?username=" + username + "&token=" + newtToken + "'>here</a> to register"
//                    + "</body></html>";
//            System.out.println(message);
//            m.sendMail("avonettestmail@gmail.com", systemuser.getEmployee().getEmail(), "Testing123", message);
            m.sendUserRegistrationMail(request, systemuser.getEmployee().getEmail(), systemuser.getEmployee().getPreferredname(), username, newToken);

            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean changePassword(boolean isValidUser, HttpServletRequest request, HttpSession session) throws Exception {
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        String currentPassword = String.valueOf(request.getParameter("cpassword"));
        String newPassword = String.valueOf(request.getParameter("newpassword"));

//        if (userDAO.isValidUser(username, currentPassword)) {
        if (isValidUser) {

            Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);
            systemuser.setPassword(Common.passwordHashed(newPassword));
            systemuser.setLastupdateddatetime(new Date());
            systemuserDAO.createOrUpdateSystemUser(systemuser);

            Audittrace audittrace = new Audittrace();
            audittrace.setAfectedid(systemuser.getEmployee().getEmployeeid().toString());
            audittrace.setAffectedpage("User password change");
            audittrace.setCreatedatetime(new Date());
            audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            audittrace.setDescription("User change password username=" + username);
            audittrace.setLastupdateddatetime(new Date());
            audittrace.setTask("Password change");
            auditTraceDAO.insertAuditTrace(audittrace);

            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean resetPassword(HttpServletRequest request, HttpSession session) throws Exception {
        String username = String.valueOf(request.getParameter("username"));
        Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);

        if (systemuser == null) {
            return false;
        }

        String password = Common.getUserPasswordToken();
        password = password.substring(password.length() - 12, password.length());
        systemuser.setPassword(Common.passwordHashed(password));
        systemuser.setFirstlogin(true);
        systemuserDAO.createOrUpdateSystemUser(systemuser);

        Mail mail = new Mail();
        mail.setMailSender(mailSender);
        mail.sendPasswordResetMail(request, systemuser.getEmployee().getEmail(), username, password);

        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid(systemuser.getEmployee().getEmployeeid().toString());
        audittrace.setAffectedpage("User password reset");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setDescription("User reset password username=" + username);
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("Password reset");
        auditTraceDAO.insertAuditTrace(audittrace);

        return true;
    }

    @Override
    @Transactional
    public JSONArray getUserroleAuthForManualAssign(String username) throws SQLException {
        JSONArray userrolecodeArr = new JSONArray();
        JSONObject userrolecodeObj = new JSONObject();

        String userrolecode = systemuserDAO.getSytemuserByUsername(username).getUserrole().getUserrolecode();

        userrolecodeObj.put("userrole", userrolecode);
        userrolecodeArr.put(userrolecodeObj);

        return userrolecodeArr;
    }

    @Override
    public boolean isValidUser(String username, String password) throws Exception {
        return userDAO.isValidUser(username, password);
    }

}
