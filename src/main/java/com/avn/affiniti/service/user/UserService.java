/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.user;

import com.avn.affiniti.model.user.User;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.mail.MailException;

/**
 * @Author : Roshen Dilshan
 * @Document : UserService
 * @Created on : Jul 15, 2016, 11:43:04 AM
 */
public interface UserService {

    public void loadPageComponent(Map<String, Object> model, HttpSession session) throws Exception;

    public void loadPageComponentView(Map<String, Object> model, long employeeid) throws SQLException;

    public Map<String, String> getTerritoryListByOrgHierarchy(int orgHierarchy) throws SQLException;

    public Map<String, String> getTerritoryListByOrgHierarchyProduct(int orgHierarchy, Integer[] product) throws SQLException;

    public Map<String, String> getProductCategoryTypesByProduct(Integer[] product) throws SQLException;

    public Map<String, String> getSupervisorByHierarchy(int orgHierarchy) throws SQLException;

    public boolean isUserExists(User user) throws SQLException;

    public int getTableDataCount(User user, HttpSession session) throws SQLException;

    public List<JSONObject> getTableData(User user, int start, int minCount, HttpSession session) throws Exception;

    public long insertUser(User user, HttpServletRequest request, HttpSession session) throws Exception;

    public void sendPasswordMail(User user) throws MailException, MessagingException;

    public int getUserRoleByUsername(String username) throws SQLException;

    public void getUserData(long employeeid, Map<String, Object> model, HttpSession session) throws SQLException;

    public User getUserByUsername(String username) throws HibernateException;

    public void updateUser(User user, HttpSession session) throws Exception;

    public Map<String, String> getUserSeletedProductList(long employeeid) throws SQLException;

    public Map<String, String> getUserSeletedTerritoryList(long employeeid) throws SQLException;

    public Map<String, String> getUserSeletedProductCategoryTypeList(long employeeid) throws Exception;

    public Map<String, String> getUserSeletedSupervisorList(long employeeid) throws Exception;

    public boolean isNotaDuplicateEpf(String epf) throws SQLException;

    public boolean isNotaDuplicateNic(String nic) throws SQLException;

    public boolean isNotaDuplicateEmail(String email) throws Exception;

    public boolean isNotaDuplicateEmailUpdate(String email, String employeeId) throws Exception;

    public boolean isNotaDuplicateUsername(String username) throws Exception;

    public boolean isNotaDuplicateUsernameUpdate(String username, String employeeId) throws Exception;

    public void passwordReSet(String username, String password) throws SQLException;

    public void sendPasswordResetMail(User user) throws MailException, MessagingException;

    public int getHierarchyidIdByUsername(String username) throws SQLException;

    public int validateUserRegistrationToken(String username, String token, boolean isValidUser);

    public boolean completeUserRegistration(String username, String token, String password, boolean isValidUser) throws Exception;

    public boolean resendRegistrationLink(String username, String token, HttpServletRequest request, boolean isValidUser) throws Exception;

    public boolean changePassword(boolean isValidUser, HttpServletRequest request, HttpSession session) throws Exception;

    public boolean resetPassword(HttpServletRequest request, HttpSession session) throws Exception;
    
    public JSONArray getUserroleAuthForManualAssign(String username) throws SQLException;

    public boolean isValidUser(String username, String password) throws Exception;
}
