/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * @Author Nadun Chamikara
 * @Document Mail
 * @Created on 01/09/2017, 9:22:26 AM
 */
public class Mail {

    private JavaMailSender mailSender;
    
    private String registrationMailContent = "<!DOCTYPE html>"
            + "<html>"
            + "<body>"
            + "Dear {firstname},"
            + "<br><br>"
            + "Thank you for choosing Affiniti and we welcome you to the all new CRM platform"
            + "<br>"
            + "Please Click <a href='{url}/reset?username={username}&token={token}'>here</a> to reset password and login to the application" 
            + "<br><br>"
            + "Best Regards,"
            + "<br>"
            + "Team AFFINITI"
            + "</body>"
            + "</html>";
    private String registrationMailSubject = "Affiniti - User Registration Password Reset";
    
    private String passwordResetMailContent = "<!DOCTYPE html>"
            + "<html>"
            + "<body>"
            + "Your new password is : "+"{password}"
            + "</body>"
            + "</html>";
    private String passwordResetMailSubject = "Affiniti - Password Reset";

//    private String from = "avonettestmail@gmail.com";

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendUserRegistrationMail(HttpServletRequest request, String to, String firstname, String username, String token) {

        try {
            String url = "http://" + request.getServerName() + (request.getServerPort() == 0 ? "" : ":" + request.getServerPort()) + request.getContextPath();

            MimeMessage message = mailSender.createMimeMessage();
            message.setSubject(registrationMailSubject);
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
//            helper.setFrom(from);
            helper.setTo(to);

            registrationMailContent = registrationMailContent.replace("{firstname}", firstname);
            registrationMailContent = registrationMailContent.replace("{url}", url);
            registrationMailContent = registrationMailContent.replace("{username}", username);
            registrationMailContent = registrationMailContent.replace("{token}", token);

            helper.setText(registrationMailContent, true);
//            System.out.println(registrationMailContent);
            mailSender.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }

    public void sendPasswordResetMail(HttpServletRequest request, String to, String username, String password) {

        try {
            String url = "http://" + request.getServerName() + (request.getServerPort() == 0 ? "" : ":" + request.getServerPort()) + request.getContextPath();

            MimeMessage message = mailSender.createMimeMessage();
            message.setSubject(passwordResetMailSubject);
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
//            helper.setFrom(from);
            helper.setTo(to);

            passwordResetMailContent = passwordResetMailContent.replace("{password}", password);

            helper.setText(passwordResetMailContent, true);
//            System.out.println(passwordResetMailContent);
            mailSender.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }
}
