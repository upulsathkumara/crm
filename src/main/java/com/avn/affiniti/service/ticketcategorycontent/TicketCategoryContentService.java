/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.avn.affiniti.service.ticketcategorycontent;

import com.avn.affiniti.hibernate.model.Ticketcatgorycontent;
import org.hibernate.HibernateException;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketCategoryContentService
 * @Created on : Jun 3, 2017, 12:21:02 PM
 */
public interface TicketCategoryContentService {
    
    public Ticketcatgorycontent getTicketCategoryContentByTicketCategory(int ticketcategory, String state) throws HibernateException;

}
