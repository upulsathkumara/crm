/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.ticketcategorycontent;

import com.avn.affiniti.dao.ticketcategorycontent.TicketCategoryContentDAO;
import com.avn.affiniti.hibernate.model.Ticketcatgorycontent;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketCategoryContentServiceImpl
 * @Created on : Jun 3, 2017, 12:21:19 PM
 */
@Repository("ticketCategoryContentService")
public class TicketCategoryContentServiceImpl implements TicketCategoryContentService {

    @Autowired
    TicketCategoryContentDAO ticketCategoryContentDAO;

    @Override
    @Transactional
    public Ticketcatgorycontent getTicketCategoryContentByTicketCategory(int ticketcategory, String state) throws HibernateException {
        return ticketCategoryContentDAO.getTicketCategoryContentByTicketCategory(ticketcategory, state);
    }

}
