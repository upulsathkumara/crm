package com.avn.affiniti.service.inventory;

import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Ticket;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryService
 * @Created on : Jun 2, 2017, 4:07:32 PM
 */
public interface InventoryService {

    public JSONObject getTicketAssignmentData(HttpServletRequest request, String loginUser) throws SQLException;

    public JSONObject getTerminalAssignmentData(HttpServletRequest request, String username) throws SQLException;

    public Map<String, String> dropdownValueListInventoryCategory();

    public Map<String, String> dropdownValueListProductBrand();

    public Map<String, String> dropdownValueListClient();

    public Map<String, String> dropdownValueListTerminalCategory();

    public Map<String, String> dropdownValueListHardwareCategory();
    
    public Map<String, String> dropdownValueListHardwareLocation();

    public void loadPageComponent(Map<String, Object> model, int location) throws SQLException, HibernateException;

    public void insertTerminal(Inventoryterminalitem terminal) throws SQLException, HibernateException;

    public void insertHardwareItem(Inventoryhardwareitem hardwareitem, String createdUser, String input, String setSelectFunction) throws Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

    public void editTerminal(Inventoryterminalitem terminalitem) throws SQLException;

    public void editHardwareItem(Inventoryhardwareitem hardwareitem, String createduser1, String input, String setSelectFunction) throws SQLException, Exception ;

    public JSONArray getTerminalSearchedData(String user, String tModelId, String contextPath, int start, int length) throws SQLException;

    public JSONArray getTicketTerminalAssignmentData(String user, String contextpath, int strat, int length) throws SQLException;

    public JSONObject getInventoryhardwareByInventoryhardwareId(int inventoryhardwareitemId);

    public JSONObject getInventoryhardwareTableData(HttpServletRequest request, String createdUser) throws SQLException, Exception;

//    public JSONArray getItemSearchedData(String categoryId, String contextPath, int start, int length)throws SQLException;
    public Long getTerminalTableCount(String user, String tModelId) throws SQLException;

    public Inventoryterminalitem getTerminalById(int id) throws SQLException;

    public Inventoryhardwareitem getItemById(int id) throws SQLException;

    public JSONArray getTerminalModelDropdownByBrand(int terminalbrand) throws SQLException;

    public JSONObject getTerminalTableData(HttpServletRequest request, int location) throws SQLException, HibernateException, Exception;

    public void insertTerminal(com.avn.affiniti.model.inventory.Inventoryterminalitem terminal, String selectFunction) throws SQLException, HibernateException;
    
    public JSONObject acceptTicket(HttpServletRequest request, String username) throws SQLException;

    public JSONObject accepttoTMCInventory(int ticketId) throws HibernateException;

    public JSONObject addTerminaltoTMCInventory(Inventoryterminalitem inventoryterminalitem) throws SQLException, HibernateException;

    public Boolean validateSerialNumber(String serialNumber) throws SQLException;
    
    public Employee getEmployeeByUsername(String uername) throws HibernateException;
}
