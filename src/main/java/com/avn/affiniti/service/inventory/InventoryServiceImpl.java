package com.avn.affiniti.service.inventory;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.client.ClientDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.employee.EmployeeDAO;
import com.avn.affiniti.dao.inventory.InventoryDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.ticket.TicketDAO;
import com.avn.affiniti.dao.user.UserDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Client;
import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Inventorycategory;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Location;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Terminalbrand;
import com.avn.affiniti.hibernate.model.Terminalmodel;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.model.ticket.TicketParam;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryServiceImpl
 * @Created on : Jun 2, 2017, 4:08:03 PM
 */
@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private CommonDAO commonDAO;
    @Autowired
    private ClientDAO clientDAO;
    @Autowired
    private InventoryDAO inventoryDAO;
    @Autowired
    private SystemuserDAO systemuserDAO;
    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private AuditTraceDAO auditTraceDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private HttpSession session;
    @Autowired
    private StatusDAO statusDAO;
    @Autowired
    private EmployeeDAO employeeDAO;

    @Override
    @Transactional
    public JSONObject getTicketAssignmentData(HttpServletRequest request, String loginUser) throws SQLException {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        Systemuser systemUser = systemuserDAO.getSytemuserByUsername(loginUser);
        TicketParam ticket = new TicketParam();
        ticket.setAssignee(userDAO.getUserByUsername(loginUser).getEmployeeid());
        ticket.setUsreRoleId(userDAO.getUserRoleByUsername(loginUser));

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            iTotalRecords = ticketDAO.getTicketTableDataCount(ticket, systemUser);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = ticketDAO.getTicketTableData(ticket, systemUser, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;

    }

    @Override
    @Transactional
    public JSONObject getTerminalAssignmentData(HttpServletRequest request, String username) throws SQLException {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
            if (Boolean.valueOf(request.getParameter("search"))) {
                String ticketId = request.getParameter("ticketId");

                iTotalRecords = ticketDAO.getTerminalTableDataCount(ticketId, username);//Total Records for searched option
                iTotalDisplayRecords = iTotalRecords;
                if (iTotalRecords > 0) {
                    list = ticketDAO.getTerminalTableData(ticketId, username, param.iDisplayLength, param.iDisplayStart);
                    rows = new JSONArray(list);
                }
            }
            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;

    }

    /*By Ashok*/
    @Override
    public Map<String, String> dropdownValueListInventoryCategory() {
        Map<String, String> list = new LinkedHashMap<>();

        try {
            list = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_INVENTORY_CATEGORY);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public Map<String, String> dropdownValueListTerminalCategory() {
        Map<String, String> list = new LinkedHashMap<>();

        try {
            list = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_INVENTORY_TERMINAL_CATEGORY);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public Map<String, String> dropdownValueListHardwareCategory() {
        Map<String, String> list = new LinkedHashMap<>();

        try {
            list = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_INVENTORY_HARWADRE_CATEGORY);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public Map<String, String> dropdownValueListHardwareLocation() {
        Map<String, String> list = new LinkedHashMap<>();

        try {
            list = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_LOCATION);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public Map<String, String> dropdownValueListProductBrand() {
        Map<String, String> list = new LinkedHashMap<>();

        try {
            list = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCT_BRAND);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public Map<String, String> dropdownValueListClient() {
        Map<String, String> list = new LinkedHashMap<>();

        try {
            list = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public void loadPageComponent(Map<String, Object> model, int location) throws SQLException, HibernateException {
        model.put("dropdownListCategory", commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_INVENTORY_TERMINAL_CATEGORY));
        model.put("dropdownListClient", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT_TO_LOCATION, location, MasterDataVarList.AFFINITI_CODE_CLIENTCATEGORYID_HARDWARE)));
        model.put("dropdownListBrand",
                commonDAO.getDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                location
                        )
                )
        );
        model.put("dropdownListModel",
                commonDAO.getDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODEL,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
    }

    @Override
    public JSONArray getTerminalModelDropdownByBrand(int terminalbrand) throws SQLException {
        return commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELSBYBRAND,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                        terminalbrand
                )
        );
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void insertTerminal(Inventoryterminalitem terminal) throws SQLException, HibernateException {

//        Inventoryterminalitem invterminalitems = new Inventoryterminalitem();
//        Inventoryproductcategory inventoryproductcategory = new Inventoryproductcategory();
//        Inventorycategory inventorycategory = new Inventorycategory();
//        
//        
//        invterminalitems.setClient(Integer.parseInt(terminal.getClient()));
//        invterminalitems.setCreateduser(terminal.getCreateduser());
//        invterminalitems.setOwner(Integer.parseInt(terminal.getOwner()));
//        
//        inventoryproductcategory.setInventoryproductcategoryid(Integer.parseInt(terminal.getBrand()));
//        inventorycategory.setInventorycategoryid(Integer.parseInt(terminal.getCategory()));
//        
//        invterminalitems.setInventoryproductcategory(inventoryproductcategory);
//        invterminalitems.setModel(terminal.getModel());
//        invterminalitems.setSerialno(terminal.getSerialnumber());
//        invterminalitems.setInventorycategory(inventorycategory);
//        invterminalitems.setLastupdateddatetime(new Date());  //new Date()
//        invterminalitems.setCreateddatetime(new Date());
//        Employee employee = new Employee();
//        employee.setEmployeeid(systemuserDAO.getSytemuserByUsername(terminal.getCreateduser()).getEmployee().getEmployeeid());
//        terminal.setEmployee(employee);
//        terminal.setCreateddatetime(commonDAO.getCurentTimesStamp());
//        terminal.setLastupdateddatetime(commonDAO.getCurentTimesStamp());
//        inventoryDAO.insertTerminal(terminal);
    }

    @Override
    @Transactional
    public void insertHardwareItem(Inventoryhardwareitem hardwareitem, String createdUser, String input, String setSelectFunction) throws Exception {
      
      
        String page = "insert inventry hardware";
        String task = "";
        String affectedId = "";
        String description = "";
        boolean emptyTasks = false;

        if (!input.equals("")) {

            Inventoryhardwareitem ic = inventoryDAO.getInventoryCategoryById(hardwareitem.getInventorycategory().getInventorycategoryid());
            Inventorycategory ic2 = inventoryDAO.getInventoryCategoryByCategoryId(hardwareitem.getInventorycategory().getInventorycategoryid());

            description = ic2.getDescription() + "  Accessory Added ";
            task = "add";
            affectedId = String.valueOf(input);

            Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED);
            hardwareitem.setStatus(status);
            hardwareitem.setCreateduser(createdUser);
            hardwareitem.setLastupdateddatetime(new Date());
            hardwareitem.setCreateddatetime(new Date());
            inventoryDAO.insertHardwareItem(hardwareitem, setSelectFunction);

        } else {
            description = "inventry hardware table is Updated by product id of  " + input;
            task = "update";
            affectedId = String.valueOf(input);

            task = "update";
            description = "Added new recode into Status table by status id of  " + input;
            affectedId = String.valueOf(input);
            hardwareitem.setCreateddatetime(new Date());
            hardwareitem.setCreateduser(createdUser);
            hardwareitem.setLastupdateddatetime(new Date());
            inventoryDAO.insertHardwareItem(hardwareitem, setSelectFunction);

        }

//        try {
//        hardwareitem.setCreateddatetime(new Date());
//        hardwareitem.setLastupdateddatetime(new Date());
//        hardwareitem.setCreateduser(createdUser);
//        // productDAO.createOrUpdateProduct(product);
//        inventoryDAO.insertHardwareItem(hardwareitem);
//        } catch (Exception ex) {
//            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
//        JSONObject message = new JSONObject();
        //message.put("message", "Hardware saved");
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

//        return message;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    @Transactional
    public JSONArray getTerminalSearchedData(String user, String tModelId, String contextPath, int start, int length) throws SQLException {
        return inventoryDAO.getTerminalSearchedData(user, tModelId, contextPath, start, length);
    }

//    @Override
//    @Transactional
//    public JSONArray getItemSearchedData(String categoryId, String contextPath, int start, int length) throws SQLException {
//        return inventoryDAO.getItemSearchedData(categoryId, contextPath, start, length);
//    }
    @Override
    @Transactional
    public Long getTerminalTableCount(String user, String tModelId) throws SQLException {
        return inventoryDAO.getTerminalTableCount(user, tModelId);
    }

    @Override
    @Transactional
    public JSONObject getInventoryhardwareTableData(HttpServletRequest request, String createdUser) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
//        String page = "Inventry Hardware";
//        String task = "";
//        String description = "";
//        String affectedId = "";
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            String inventorycategoryid = request.getParameter("inventorycategory");//Setting up searched Section Task ID
            String partname = request.getParameter("partname");//Setting up searched Section Task ID

//            if (inventorycategoryid.equals("") || inventorycategoryid.equals("--Select--")) {
//                task = "Table View";
//                description = "Table View all details in Inventory Hardware ";
//                affectedId = "";
//            } else {
//                task = "Table Search";
//                description = "Table Search details in Inventory Hardware by Inventory Hardware Id " + inventorycategoryid;
//                affectedId = inventorycategoryid;
//            }
//            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log
            Inventoryhardwareitem inventoryhardwareitem = new Inventoryhardwareitem();

            if (!(inventorycategoryid.trim()).isEmpty() && inventorycategoryid != null) {
                Inventorycategory inventorycategory = new Inventorycategory();
                inventorycategory.setInventorycategoryid(Integer.parseInt(inventorycategoryid));
                inventoryhardwareitem.setInventorycategory(inventorycategory);
            }
            if (!(partname.trim()).isEmpty() && partname != null) {
                inventoryhardwareitem.setPartname(partname);
            }

            if (inventoryhardwareitem.getInventorycategory() == null && inventoryhardwareitem.getPartname() == null) {
                inventoryhardwareitem = null;
            }

            iTotalRecords = inventoryDAO.getTableDataCount(inventoryhardwareitem);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = inventoryDAO.getTableData(inventoryhardwareitem, param.iDisplayLength, param.iDisplayStart, request);
                rows = new JSONArray(list);

            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
            throw e;
        }

        return jsonResponse;
    }

    @Override
    @Transactional
    public JSONObject getInventoryhardwareByInventoryhardwareId(int inventoryhardwareitemId) {
        JSONObject object = new JSONObject();
        JSONObject output = new JSONObject();

        output.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("success", true);
        output.put("message", "");

        try {
            Inventoryhardwareitem i = inventoryDAO.getItemById(inventoryhardwareitemId);

            object.put("id", i.getInventoryhardwareitemid().toString());
            object.put("category", i.getInventorycategory().getInventorycategoryid().toString());

            object.put("partcode", i.getPartcode().toString());
            object.put("partname", i.getPartname().toString());
            object.put("posrevision", i.getPosrevision().toString());
            object.put("pospartid", i.getPospartid().toString());
            object.put("posmac", i.getPosmac().toString());
            object.put("posptid", i.getPosptid().toString());
            object.put("posserialnumber", i.getPosserialnumber().toString());
            //object.put("statusid", String.valueOf(p.getStatus().getStatusid()));
            // object.put("sortid", String.valueOf(p.getSortid()));

            object.put("lastupdatedtime", i.getLastupdateddatetime().toString());
            object.put("createdtime", String.valueOf(i.getCreateddatetime()));
            object.put("createduser", String.valueOf(i.getCreateduser()));

        } catch (HibernateException ex) {
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", "Error : Database Error");
        } catch (Exception ex) {
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", "Error : Error");
        }

        output.put("data", object);

        return output;
    }

    @Override
    @Transactional
    public Inventoryterminalitem getTerminalById(int id) throws SQLException {
        return inventoryDAO.getTerminalById(id);
    }

    @Override
    @Transactional
    public void editTerminal(Inventoryterminalitem terminalitem) throws SQLException {
        terminalitem.setLastupdateddatetime(commonDAO.getCurentTimesStamp());
        inventoryDAO.editTerminal(terminalitem);
    }

    @Override
    @Transactional
    public Inventoryhardwareitem getItemById(int id) throws SQLException {
        
        return inventoryDAO.getItemById(id);
    }

    @Override
    @Transactional
    public void editHardwareItem(Inventoryhardwareitem hardwareitem, String createduser1, String input, String setSelectFunction) throws SQLException, Exception {
        String page = "insert inventry hardware";
        String task = "";
        String affectedId = "";
        String description = "";
        Inventorycategory ic2 = inventoryDAO.getInventoryCategoryByCategoryId(hardwareitem.getInventorycategory().getInventorycategoryid());

        //boolean emptyTasks = false;
        if (input != "null") {
            description = " Accessory  Updated  to " + ic2.getDescription();
            task = "update";
            affectedId = String.valueOf(input);
            hardwareitem.setLastupdateddatetime(new Date());
            hardwareitem.setCreateddatetime(new Date());
            hardwareitem.setCreateduser(createduser1);
            inventoryDAO.editHardwareItem(hardwareitem, setSelectFunction);

        } else {
            task = "add";
            description = "Added new recode into Accessory table by  " + ic2.getDescription();
            affectedId = String.valueOf(input);
            hardwareitem.setCreateddatetime(new Date());
            hardwareitem.setCreateduser(createduser1);
            hardwareitem.setLastupdateddatetime(new Date());
            inventoryDAO.insertHardwareItem(hardwareitem, setSelectFunction);

        }

        ////////////////
        // public  JSONObject insertHardwareItem(Inventoryhardwareitem hardwareitem) {
//        try {
        //hardwareitem.setCreateddatetime(new Date());
        // hardwareitem.setLastupdateddatetime(new Date());
        // hardwareitem.setCreateduser(createduser1);
        // productDAO.createOrUpdateProduct(product);
        // inventoryDAO.editHardwareItem(hardwareitem);
//        } catch (Exception ex) {
//            Logger.getLogger(InventoryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
        JSONObject message = new JSONObject();
        // message1.put("message1", "Hardwear saved");
        insertAuditTrace(page, affectedId, task, description, createduser1); //insert audittrace log
        //this.insertAuditTrace(page, affectedId, task, description, createdUser);

        //return message1;
    }

    @Override
    @Transactional
    public JSONArray getTicketTerminalAssignmentData(String user, String contextpath, int strat, int length) throws SQLException {
        return ticketDAO.getTicketTerminalAssignmentData(user, contextpath, strat, length);
    }

//    @Override
//    @Transactional
//    public JSONArray getItemSearchedData(String categoryId, String contextPath, int start, int length) throws SQLException {
//        return inventoryDAO.getItemSearchedData(categoryId, contextPath, start, length);
//    }
    @Override
    public JSONObject getTerminalTableData(HttpServletRequest request, int location) throws SQLException, HibernateException, Exception {
        String ViewStatus = "active";
        String UpdateStatus = "active";

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }

        Inventoryterminalitem parameters = this.getParameters(request);//Setting up searched Section Task ID

        iTotalRecords = inventoryDAO.getTerminalTableDataCount(parameters, location);//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = inventoryDAO.getTerminalTableData(parameters, location, param.iDisplayLength, param.iDisplayStart, request);
            rows = new JSONArray(list);

        }

        jsonResponse.put("success", true);
        jsonResponse.put("messageService", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

        String page = "Terminal Item View ";
        String affectedId = "Inverntory Terminal View page";
        String task = "View Inverntory Treminal Items";
        String description = "Table Search by Userrole ID : ";

        String createdUser = String.valueOf(session.getAttribute("username"));

        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        return jsonResponse;
    }

    private Inventoryterminalitem getParameters(HttpServletRequest request) throws Exception {

        Inventoryterminalitem inventoryterminalitem = new Inventoryterminalitem();

        String searchValue = request.getParameter("brandsearch");
        String modelSearchValue = request.getParameter("modelsearch");
        String clientSearchValue = request.getParameter("clientsearch");

        if ((searchValue.equals("") || searchValue == null) && (clientSearchValue.equals("") || clientSearchValue == null) && (modelSearchValue.equals("") || modelSearchValue == null)) {
            inventoryterminalitem = null;
        }
        if (searchValue != "" && searchValue != null) {
            //Terminalbrand terminalbrand = terminalbrand = inventoryDAO.getTerminalbrandByDescription(searchValue);
            //inventoryterminalitem.setTerminalbrand(terminalbrand);

            int brandid = Integer.parseInt(searchValue);
            Terminalbrand terminalbrand = new Terminalbrand();
            terminalbrand.setTerminalbrandid(brandid);
            inventoryterminalitem.setTerminalbrand(terminalbrand);
        }
        if (clientSearchValue != "" && clientSearchValue != null) {
            Client client = new Client();
            int cid = Integer.parseInt(clientSearchValue);
            client.setClientid(cid);
            inventoryterminalitem.setClient(client);
        }
        if (modelSearchValue != "" && modelSearchValue != null) {
            Terminalmodel terminalmodel = new Terminalmodel();
            int mid = Integer.parseInt(modelSearchValue);
            terminalmodel.setTerminalmodelid(mid);
            inventoryterminalitem.setTerminalmodel(terminalmodel);
        }

        return inventoryterminalitem;
    }

    @Override
    @Transactional
    public void insertTerminal(com.avn.affiniti.model.inventory.Inventoryterminalitem terminal, String selectFunction) throws SQLException, HibernateException {

        Inventoryterminalitem hbinventoryterminalitem = new Inventoryterminalitem();

        if (terminal.getInventoryterminalitemid() != null) {
            String id = terminal.getInventoryterminalitemid().toString();
            if (id != "") {
                hbinventoryterminalitem.setInventoryterminalitemid(terminal.getInventoryterminalitemid());
            }
        }

        Client client = new Client();
        client.setClientid(terminal.getClient());
        hbinventoryterminalitem.setClient(client);

        Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED);
        hbinventoryterminalitem.setStatus(status);

        Inventorycategory inventorycategory = new Inventorycategory();
        inventorycategory.setInventorycategoryid(terminal.getInventorycategory());
        hbinventoryterminalitem.setInventorycategory(inventorycategory);

        Terminalbrand terminalbrand = new Terminalbrand();
        terminalbrand.setTerminalbrandid(terminal.getTerminalbrand());
        hbinventoryterminalitem.setTerminalbrand(terminalbrand);

        Terminalmodel terminalmodel = new Terminalmodel();
        terminalmodel.setTerminalmodelid(terminal.getTerminalmodel());
        hbinventoryterminalitem.setTerminalmodel(terminalmodel);

        hbinventoryterminalitem.setTid(terminal.getTid());
        hbinventoryterminalitem.setMid(terminal.getMid());
        hbinventoryterminalitem.setSerialno(terminal.getSerialno());

        hbinventoryterminalitem.setCreateddatetime(new Date());
        hbinventoryterminalitem.setLastupdateddatetime(new Date());

        String createduser = (String) session.getAttribute("username");
        hbinventoryterminalitem.setCreateduser(createduser);

        //Setting the location of the logged user to the terminal object
        Location location = systemuserDAO.getSytemuserByUsername(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))).getEmployee().getLocation();
        hbinventoryterminalitem.setLocation(location);

        String page = "Terminal Item Create ";
        String affectedId = "";
        if (hbinventoryterminalitem.getInventoryterminalitemid() != null) {
            affectedId = hbinventoryterminalitem.getInventoryterminalitemid().toString();
        } 
        //String clientId= 0;
////       Client cid = clientDAO.getClientById(clientId);
               //getSectionBySectionId(sectionId);
        String task = "Create Inventory Treminal Item";
        String description = "Terminal Added by  : "
                + " Client : " + hbinventoryterminalitem.getClient().getClientid()
                + " , Ternimal brand :" + hbinventoryterminalitem.getTerminalbrand().getTerminalbrandid()
                + " , TID  :" + hbinventoryterminalitem.getTid()
                + ", MID : " +hbinventoryterminalitem.getMid()
                + " , Terminal model : " + hbinventoryterminalitem.getTerminalmodel().getTerminalmodelid()
                + " , Serial No " + hbinventoryterminalitem.getSerialno()
                + " , Inventory Catagory : " + hbinventoryterminalitem.getInventorycategory().getInventorycategoryid();

        String createdUser = String.valueOf(session.getAttribute("username"));

        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        inventoryDAO.insertTerminal(hbinventoryterminalitem, selectFunction);

    }

    @Override
    @Transactional
    public JSONObject acceptTicket(HttpServletRequest request, String username) throws SQLException {

        /**
         * ************************************************************************************************
         */
        //        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        ArrayList statusCode = new ArrayList();
        statusCode.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TERMHANDTMC);//41 - Terminal handover to TMC
        statusCode.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_RETFTMC);//61 - Return Existing Terminal - TMC
//        statusCode.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ADDTMCINV);//56 - Add to TMC Inventory
        statusCode.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTDELTERMIRC);//85 - Deliver terminals to IRC
        statusCode.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_DTTIFS);//92 - Deliver terminals to IFS      

        Systemuser systemuser = systemuserDAO.getSytemuserByUsername(username);

        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            iTotalRecords = ticketDAO.getTicketTableDataCountByStatus(statusCode, systemuser);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = ticketDAO.getTicketTableDataByStatus(param.iDisplayLength, param.iDisplayStart, statusCode);
                rows = new JSONArray(list);
            }
//            }
            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
//            }
        } catch (Exception e) {
            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;
    }

    @Override
    @Transactional
    public JSONObject accepttoTMCInventory(int ticketId) throws HibernateException {

        JSONObject response = new JSONObject();
        String statusCode = null;
        try {
            Ticket ticket = ticketDAO.getTicket(ticketId);

            if (ticket.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_DTTIFS)) {
                statusCode = MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACCREPTERM;
            } else if (ticket.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTDELTERMIRC)) {
                statusCode = MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACCTERM;
            } else {
                statusCode = MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKTACCINV;
            }
            Status status = statusDAO.getStatusByStatusCode(statusCode);
            ticket.setStatus(status);
            ticketDAO.updateTicket(ticket);

            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED);

            Inventoryterminalitem inventoryterminalitem = null;
            if (ticket.getInventoryterminalitem() != null) {
                inventoryterminalitem = inventoryDAO.getTerminalById(ticket.getInventoryterminalitem().getInventoryterminalitemid());
                inventoryterminalitem.setStatus(status);
                inventoryDAO.insertTerminal(inventoryterminalitem, MasterDataVarList.AFFINITI_CODE_UPDATE);
            }

            response.put("CODE", "SUCCESS");
//            response.put("MESSAGE", "Inventroy Terminal Item of  " + inventoryterminalitem.getClient().getName() + " Accepted to the Inventory Successfully");
            response.put("MESSAGE", "Inventroy Terminal Item Accepted to the Inventory Successfully");
        } catch (HibernateException | SQLException | JSONException e) {
            response.put("CODE", "ERROR");
            response.put("MESSAGE", "Inventroy Terminal Item Not Accepted");
            e.printStackTrace();
        }
        return response;
    }

    @Override
    @Transactional
    public JSONObject addTerminaltoTMCInventory(Inventoryterminalitem inventoryterminalitem) throws SQLException, HibernateException {

        JSONObject response = new JSONObject();
        try {
            boolean status = inventoryDAO.addTerminaltoTMCInventory(inventoryterminalitem);
            if (status) {
                response.put("CODE", "SUCCESS");
            } else {
                response.put("CODE", "ERROR");
            }
        } catch (HibernateException | SQLException | JSONException ex) {
            response.put("CODE", "ERROR");
            ex.printStackTrace();
        }
        return response;
    }

    @Override
    public Boolean validateSerialNumber(String serialNumber) throws SQLException {
        return inventoryDAO.validateSerialNumber(serialNumber);
    }

    @Override
    @Transactional
    public Employee getEmployeeByUsername(String uername) throws HibernateException {
        return employeeDAO.getEmployeeByUsername(uername);
    }

}
