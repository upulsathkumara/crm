package com.avn.affiniti.service.userroletask;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.userrolesubsection.UserroleSubsectionDAO;
import com.avn.affiniti.dao.userroletask.UserRoleTaskDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.hibernate.model.Userrolesectiontask;
import com.avn.affiniti.hibernate.model.UserrolesectiontaskId;
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.hibernate.model.Userroletask;
import com.avn.affiniti.hibernate.model.UserroletaskId;
import com.avn.affiniti.model.userolesectiontask.UserRoleSectionTask;
import com.avn.affiniti.model.userroletask.UserRoleTask;
import com.avn.affiniti.service.userrole.UserroleService;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Kaushan Fernando
 * @Document UserRoleTaskServiceImpl
 * @Created on 10/08/2017, 8:43:54 AM
 */
@Service("userRoleTaskService")
public class UserRoleTaskServiceImpl implements UserRoleTaskService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private UserroleSubsectionDAO userroleSubsectionDAO;

    @Autowired
    private UserRoleTaskDAO userRoleTaskDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Override
    @Transactional
    public JSONObject getUserRoleSectionsByUserRoleId(int userRoleId) {
        JSONArray data = new JSONArray();
        JSONObject objData = new JSONObject();
        try {

            data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTIONS_BY_USERROLE_ID,
                    userRoleId
            ));
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            objData.put("success", true);
            objData.put("message", "Successfull");
            objData.put("data", data);
        } catch (HibernateException | SQLException sqle) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sections - please try again!");
        } catch (Exception e) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sections - please try again!");
        }

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getUserRoleSubSectionsBySectionId(int sectionId, int userRoleId) {
        JSONArray data = new JSONArray();
        JSONObject objData = new JSONObject();
        try {

            data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUBSECTIONS_BY_USERROLE_ID_AND_SECTION_ID,
                    userRoleId, sectionId
            ));
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            objData.put("success", true);
            objData.put("message", "Successfull");
            objData.put("data", data);
        } catch (HibernateException | SQLException sqle) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sub sections - please try again!");
        } catch (Exception e) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading sub sections - please try again!");
        }

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getTaskLIstById(UserRoleTask userRoleTask,int userRoleId, int sectionId, int subSectionId, HttpSession session) {

        JSONArray dataToRemoveSelect;
        JSONArray data2ToRemoveSelect;
        JSONArray data;
        JSONArray data2;
        JSONArray data3;
        JSONArray data4;
        JSONObject objData = new JSONObject();

        String page = "User Role Task ";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        // UserRoleTask userRoleTask = new UserRoleTask();
        String elementIdToRemove = "";

        try {
            //   remove select element from DropDown JsonArray on assigned tasks
            dataToRemoveSelect = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PENDING_TASKS_BY_USERROLE_ID_AND_SECTION_ID_AND_SUBSECTION_ID,
                    sectionId, subSectionId, userRoleId, sectionId, subSectionId
            ));
            data = Common.removeDropDownElementFromJsonArray(elementIdToRemove, dataToRemoveSelect);

            //   remove select element from DropDown JsonArray on assigned tasks
            data2ToRemoveSelect = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNED_TASKS_BY_USERROLE_ID_AND_SECTION_ID_AND_SUBSECTION_ID,
                    userRoleId, sectionId, subSectionId
            ));

            data2 = Common.removeDropDownElementFromJsonArray(elementIdToRemove, data2ToRemoveSelect);

            data3 = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTIONS_BY_SECTION_ID,
                    sectionId
            ));

            data4 = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUBSECTIONS_BY_SUBSECTION_ID,
                    subSectionId
            ));
            task = "View";

            description = "Viewed  Single Recorde  in User Role Task Table User Role Task  ID of '" + userRoleTask.getSectionId()
                    + " ' " + " ' User Role ID   of ' "+userRoleId+ " ' "
                    + " ' Subsection  ID of ' " +subSectionId+ " ' ";

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            objData.put("success", true);
            objData.put("message", "Successfull");
            objData.put("data", data);
            objData.put("data2", data2);
            objData.put("data3", data3);
            objData.put("data4", data4);

        } catch (HibernateException | SQLException sqle) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading Tasks - please try again!");
        } catch (Exception e) {
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading Tasks - please try again!");
        }

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getUserRoleTaskTableData(HttpServletRequest request, String createdUser) throws HibernateException, SQLException, JSONException, Exception {

        //        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
//        String ViewStatus = "active";
//        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        String page = "UserRoleTask";
        String task = "";
        String description = "";
        String affectedId = "";

//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }
//            if (Boolean.valueOf(request.getParameter("search"))) {

        String userRoleId = request.getParameter("userroleid");
//
//            if (userRoleId.equals("") || userRoleId.equals("--Select--")) {
//                task = "Table View";
//                description = "Table View all details in Userroletask ";
//                affectedId = "";
//            } else {
//                JSONArray data = new JSONArray();
//                String userRoleDescription = "";
//
//                data = UserRoleService.getUserroleDescriptionByUserRoleId(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT_USERROLES,
//                        userRoleId
//                ));
//                for (int i = 0; i < data.length(); i++) {
//                    JSONObject jSONObject = data.getJSONObject(i);
//                    if (jSONObject.getString("id").equalsIgnoreCase(userRoleId)) {
//                        userRoleDescription = jSONObject.getString("value");
//                    }
//                }
//                task = "Table Search";
//                description = "Table Search details in Userroletask by user role id " + userRoleId + " user role name " + userRoleDescription;
//                affectedId = userRoleId;
//
//            }
//            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        iTotalRecords = userRoleTaskDAO.getTableDataCount(userRoleId);//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = userRoleTaskDAO.getTableData(userRoleId, param.iDisplayLength, param.iDisplayStart);

            rows = new JSONArray(list);

        }

        jsonResponse.put("status", 200);
        jsonResponse.put("success", true);
        jsonResponse.put("message", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        return jsonResponse;
    }

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) {
        try {
            model.put("userRoleList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLE,
                    MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
            )));

            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            model.put("success", true);
            model.put("message", "Successfull");

        } catch (HibernateException | SQLException sqle) {
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Database Error");
        } catch (Exception e) {
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Error");
        }

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject saveUserRoleTask(UserRoleTask[] userRoleTasks, List<Userroletask> listOfOldObject, String createdUser, String selectFunction) throws HibernateException, SQLException, JSONException, Exception {

        JSONObject object = new JSONObject();

        String page = "User Role Subsection Task";
        String task = "Assign";
        String affectedId = "";
        String description = "";
        int userRoleSubSectionId = 0;
        //Userrolesubsection userrolesubsection = userRoleTaskDAO.getUserroleSubsection(userRoleSubSectionId);
        // Statuscategory sc = statusDAO.getStatuscategory(Integer.parseInt(String.valueOf(status1)));
        boolean removeTask = true;
        boolean saveOrNot = true;
        for (Userroletask usrt : listOfOldObject) {

            int oldTaskId = usrt.getTask().getTaskid();
            int oldUserRoleSubSectionId = usrt.getId().getUserrolesubsectionid();
            for (UserRoleTask urt : userRoleTasks) {
                UserRoleTask userRoleSubSectionTask = new UserRoleTask();

                userRoleSubSectionTask.setTaskId(urt.getTaskId());
                int newTaskId = Integer.parseInt(userRoleSubSectionTask.getTaskId());

                if (oldTaskId == newTaskId) {

                    removeTask = false;

                }

            }
            if (removeTask) {

                long taskAvailability = userRoleTaskDAO.checkUserRoleSubSectionTaskAvailability(oldUserRoleSubSectionId, oldTaskId);
                if (taskAvailability != 0) {
                    userRoleTaskDAO.deleteUserroletask(oldUserRoleSubSectionId, oldTaskId);
                }

            }
            removeTask = true;

        }
        for (UserRoleTask urt : userRoleTasks) {

            UserRoleTask userRoleSubSectionTask = new UserRoleTask();

            userRoleSubSectionTask.setUserroleId(urt.getUserroleId());
            int userRoleId = Integer.parseInt(userRoleSubSectionTask.getUserroleId());

            userRoleSubSectionTask.setSectionId(urt.getSectionId());
            int sectionId = Integer.parseInt(userRoleSubSectionTask.getSectionId());

            userRoleSubSectionTask.setSubsectionId(urt.getSubsectionId());
            int subSectionId = Integer.parseInt(userRoleSubSectionTask.getSubsectionId());

            userRoleSubSectionTask.setTaskId(urt.getTaskId());
            int newTaskId = Integer.parseInt(String.valueOf(userRoleSubSectionTask.getTaskId()));

            userRoleSubSectionId = this.findUserroleSubSectionId(userRoleId, sectionId, subSectionId);
            affectedId = String.valueOf(userRoleSubSectionId);

            if (newTaskId != 0) {

                for (Userroletask usrt : listOfOldObject) {
                    int oldTaskId = usrt.getTask().getTaskid();

                    if (newTaskId == oldTaskId) {
                        saveOrNot = false;
                    }

                }
                Task task1 = userroleSubsectionDAO.getTask(newTaskId);
                if (selectFunction.equals("save")) {

                    description = "Assigned tasks to userroletask by userrolesubsectionid : " + userRoleSubSectionId + " and task of : " + task1.getDescription();
                    // newTaskId + task1.getDescription();
                    task = "assign";
                    object.put("message", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Tasks"));
                    selectFunction = "doneselection";
                } else if (selectFunction.equals("edit")) {

                    description = "Updated task from userroletask by userrolesubsectionid : " + userRoleSubSectionId + " and old task of : ";

                    for (Userroletask usrt : listOfOldObject) {
                        int oldTaskId = usrt.getTask().getTaskid();
                        Task task2 = userroleSubsectionDAO.getTask(oldTaskId);

                        description = description + " " + oldTaskId + " , " + task2.getDescription();

                    }
                    description = description + " new task of " + task1.getDescription();
                    //+ newTaskId +task1.getDescription();
                    task = "update";
                    object.put("message", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Tasks"));
                    selectFunction = "doneselection";

                } else if (selectFunction.equals("doneselection")) {
                    description = description + " , " + newTaskId;
                }
                if (saveOrNot) {

                    Userroletask userRoleSubSectionTaskToSave = new Userroletask();
                    UserroletaskId userRoleSubSectionTaskId = new UserroletaskId(userRoleSubSectionId, newTaskId);

                    userRoleSubSectionTaskToSave.setId(userRoleSubSectionTaskId);// set userrolesubsectionid and taskId into userroletask
                    userRoleSubSectionTaskToSave.setCreateddatetime(new Date());
                    userRoleSubSectionTaskToSave.setLastupdateddatetime(new Date());
                    userRoleSubSectionTaskToSave.setCreateduser(createdUser);
                    userRoleTaskDAO.saveUserRoleTask(userRoleSubSectionTaskToSave);

                }
                saveOrNot = true;
            } else {
                description = "removed tasks from userroletask by userrolesubsectionid : " + userRoleSubSectionId + " and old task ids : ";
                for (Userroletask usrt : listOfOldObject) {

                    int oldTaskId = usrt.getTask().getTaskid();

                    description = description + " " + oldTaskId + " , ";

                }
                selectFunction = "done";
                task = "remove all tasks";
                object.put("message", String.format(MessageVarList.ALL_REMOVED_SUCCESSFULLY, "Tasks"));
            }

        }
        this.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        object.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        object.put("success", true);

        return object;

    }

    @Override
    @Transactional
    public int findUserroleSubSectionId(int userRoleId, int sectionId, int subSectionId) throws SQLException {
        int userRoleSubSectionId = 0;
        userRoleSubSectionId = userRoleTaskDAO.findUserroleSubSectionId(userRoleId, sectionId, subSectionId);
        return userRoleSubSectionId;
    }

    @Override
    @Transactional
    public String findTaskDescriptionByTaskId(int taskId) throws SQLException {
        String taskDescription = "";
        taskDescription = userRoleTaskDAO.findTaskDescriptionByTaskId(taskId);
        return taskDescription;
    }

    @Override
    @Transactional
    public List<Userroletask> getUserRoleTaskList(int userRoleSubSectionId) throws HibernateException, SQLException {
        List<Userroletask> listOfOldObject = userRoleTaskDAO.getOldUserRleTaskObject(userRoleSubSectionId);
        return listOfOldObject;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
