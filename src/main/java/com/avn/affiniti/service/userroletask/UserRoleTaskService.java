package com.avn.affiniti.service.userroletask;

import com.avn.affiniti.hibernate.model.Userroletask;
import com.avn.affiniti.model.userolesectiontask.UserRoleSectionTask;
import java.util.Map;
import org.json.JSONObject;
import com.avn.affiniti.model.userroletask.UserRoleTask;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;

/**
 * @Author Kaushan Fernando
 * @Document UserRoleSectionService
 * @Created on 10/08/2017, 8:43:32 AM
 */
public interface UserRoleTaskService {

    public void loadPageComponent(Map<String, Object> model);

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

    public JSONObject getUserRoleSectionsByUserRoleId(int userRoleId);

    public JSONObject getUserRoleSubSectionsBySectionId(int sectionId, int userRoleId);

    public JSONObject getTaskLIstById(UserRoleTask userRoleTask,int userRoleId, int sectionId, int subSectionId,HttpSession session);
    
    public List<Userroletask> getUserRoleTaskList(int userRoleSubSectionId)throws HibernateException,SQLException;
    
    public int findUserroleSubSectionId(int userRoleId,int sectionId,int subSectionId) throws SQLException;
    
    public String findTaskDescriptionByTaskId(int taskId) throws SQLException;

    public JSONObject getUserRoleTaskTableData(HttpServletRequest request,String createdUser) throws HibernateException, SQLException, JSONException, Exception;

    public JSONObject saveUserRoleTask(UserRoleTask[] userRoleTasks,List<Userroletask> listOfOldObject, String createdUser, String selectFunction) throws HibernateException, SQLException, JSONException, Exception;

}
