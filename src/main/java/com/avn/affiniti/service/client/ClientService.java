/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.client;

import com.avn.affiniti.hibernate.model.Client;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.dao.DataAccessException;

/**
 * @Author Nadun Chamikara
 * @Document ClientService
 * @Created on 17/08/2017, 4:53:55 PM
 */
public interface ClientService {

    public void loadPageComponent(Map<String, Object> model, int location) throws SQLException, HibernateException;

    public JSONArray getProductCategoryByClientType(String clientCategories) throws SQLException, Exception;

    public void createOrUpdateClient(com.avn.affiniti.model.client.Client client, HttpServletRequest request, HttpSession session) throws HibernateException, Exception;

//    public JSONArray getClientCategoryListByOrganizationId(int organizationId) throws SQLException, DataAccessException;
    public long getClientTableDataCount(String name, String organizationId, String clientCategoryId, int location) throws HibernateException, Exception;

    public List<JSONObject> getClientTableData(String name, String organizationId, int location, String clientCategoryId, int start, int minCount, HttpSession session) throws HibernateException, Exception;

    public JSONArray getProductListByProductCategory(int productCategoryId) throws SQLException, DataAccessException, Exception;

    public JSONArray getClientProductStatusList() throws SQLException, DataAccessException, Exception;

    public JSONArray getClientById(int clientId, HttpSession session) throws HibernateException, Exception;

    public void deleteClient(int clientId, HttpSession session) throws HibernateException, Exception;

    public boolean isNotaDuplicateEmail(String email, String clientid) throws Exception;

}
