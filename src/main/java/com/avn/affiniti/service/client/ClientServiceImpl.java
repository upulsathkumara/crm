/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.client;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.client.ClientDAO;
import com.avn.affiniti.dao.clientclientcategory.ClientClientCategoryDAO;
import com.avn.affiniti.dao.clientproduct.ClientProductDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.employee.EmployeeDAO;
import com.avn.affiniti.dao.organization.OrganizationDAO;
import com.avn.affiniti.dao.sectiontask.SectionTaskDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.user.UserDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Client;
import com.avn.affiniti.hibernate.model.Clientcategory;
import com.avn.affiniti.hibernate.model.Clientclientcategory;
import com.avn.affiniti.hibernate.model.ClientclientcategoryId;
import com.avn.affiniti.hibernate.model.Clientproduct;
import com.avn.affiniti.hibernate.model.ClientproductId;
import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Location;
import com.avn.affiniti.hibernate.model.Organization;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.model.clientproduct.ClientProduct;
import com.avn.affiniti.service.mail.Mail;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.Array;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Nadun Chamikara
 * @Document ClientServiceImpl
 * @Created on 17/08/2017, 4:55:01 PM
 */
@Service("clientService")
public class ClientServiceImpl implements ClientService {

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    ClientDAO clientDAO;

    @Autowired
    ClientProductDAO clientProductDAO;
    
    @Autowired
    OrganizationDAO organizationDAO;

    @Autowired
    SystemuserDAO systemuserDAO;

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    AuditTraceDAO auditTraceDAO;

    @Autowired
    SectionTaskDAO sectionTaskDAO;

    @Autowired
    ClientClientCategoryDAO clientClientCategoryDAO;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model, int location) throws SQLException, HibernateException {
        model.put("organizationList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ORGANIZATION_BY_LOCATION,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE, location
        )));

        model.put("statusList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT
        )));

        model.put("productCategoryList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCTCATEGORY
        )));

        model.put("clientCategoryList", Common.removeElementFromMap("", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        ))));

        model.put("clientCategoryListSearch", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        )));

//        model.put("locationList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_LOCATION
//        )));
    }

    @Override
    @Transactional
    public JSONArray getProductCategoryByClientType(String clientCategories) throws SQLException, Exception {
        JSONArray data = new JSONArray();

        String clientCategoriesStr = clientCategories.replace("[", "").replace("]", "");
        data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCTCATEGORY_BY_CLIENTCATEGORY,
                clientCategoriesStr
        ));

        return data;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void createOrUpdateClient(com.avn.affiniti.model.client.Client client, HttpServletRequest request, HttpSession session) throws HibernateException, Exception {
        boolean isUpdate = false;
        Client c;

        if (!client.getClientid().equals("") && request.getParameter("action").equals(MasterDataVarList.AFFINITI_CODE_UPDATE)) { // update
            isUpdate = true;
            c = clientDAO.getClientById(Integer.parseInt(client.getClientid()));
        } else {
            c = new Client();
        }

        Status status = new Status();
        status.setStatusid(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE);

        if (isUpdate) {
            c.setClientid(Integer.parseInt(client.getClientid()));
            clientProductDAO.deleteClientProductsByClientId(c.getClientid());
            status.setStatusid(client.getStatus());
        }
        c.setStatus(status);

//        Clientcategory clientcategory = new Clientcategory();
//        clientcategory.setClientcategoryid(client.getClientcategory());
//        c.setClientcategory(clientcategory);
        Organization organization = new Organization();
        organization.setOrganizationid(client.getOrganization());
        c.setOrganization(organization);

        c.setName(client.getName());
        c.setPrimarycontactperson(client.getPrimarycontactperson());
        c.setEmail(client.getEmail());
        c.setContactno01(client.getContactno01());
        c.setContactno02(client.getContactno02());

        Location location = systemuserDAO.getSytemuserByUsername(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))).getEmployee().getLocation();
        c.setLocation(location);
        c.setAddress(client.getAddress());

        c.setLastupdateddatetime(new Date());

        if (!isUpdate) {
            c.setCreatedatetime(new Date());
            c.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        }

//        clientDAO.createOrUpdateClient(c);
        if (isUpdate) {
            clientDAO.update(c);
        } else {
            clientDAO.create(c);
        }

        // Delete Clientclientcategories
        if (isUpdate) {
            clientClientCategoryDAO.deleteClientClientCategoriesByClientId(c.getClientid());
        }

        // insert Clientclientcategories
        for (String clientCategoryId : client.getClientcategory()) {
            Clientclientcategory clientclientcategory = new Clientclientcategory();
            clientclientcategory.setId(new ClientclientcategoryId(c.getClientid(), Integer.parseInt(clientCategoryId)));
            clientclientcategory.setCreatedatetime(new Date());
            clientclientcategory.setLastupdateddatetime(new Date());
            clientclientcategory.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));

            clientClientCategoryDAO.createOrUpdateClientClientCategory(clientclientcategory);
        }

        for (ClientProduct cp : client.getClientproducts()) {
            Clientproduct clientproduct = new Clientproduct();
            clientproduct.setId(new ClientproductId(c.getClientid(), Integer.parseInt(cp.getProduct())));
            clientproduct.setCreatedatetime(new Date());
            clientproduct.setLastupdateddatetime(new Date());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date purchasedDate = sdf.parse(cp.getPurchaseddate());
            clientproduct.setPurchaseddate(purchasedDate);

            Status s = new Status();
            s.setStatusid(Integer.parseInt(cp.getClientproductstatus()));

            clientproduct.setStatus(s);
            clientproduct.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));

            clientProductDAO.createOrUpdateClientProduct(clientproduct);
        }

        Employee employee;
        if (isUpdate && c.getEmployees().size() > 0) {
            employee = (Employee) c.getEmployees().toArray()[0];
        } else {
            employee = new Employee();
            employee.setCreateddatetime(new Date());
        }
        employee.setClient(c);
        employee.setLastupdateddatetime(new Date());

        if (!isUpdate) {
            employee.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        }

        employee.setStatus(status);
        employee.setLocation(location);
        employee.setNameinfull(client.getName());
        employee.setEmail(client.getEmail());
        employee.setContactno01(client.getContactno01());
        employee.setContactno02(client.getContactno02());

        employeeDAO.saveOrUpdate(employee);

        Systemuser systemuser;

        if (isUpdate && c.getSystemusers().size() > 0) {
            systemuser = systemuserDAO.getSytemuserByUsername(c.getEmail());
        } else {
            systemuser = new Systemuser();
            systemuser.setUsername(client.getUsername());
            Userrole userrole = new Userrole();
            userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_CLIENT_ADMIN);
            systemuser.setUserrole(userrole);
            systemuser.setCreateddatetime(new Date());
            systemuser.setTokencreatedtime(new Date());
        }
        systemuser.setClientuser(1);
        systemuser.setEmployee(employee);
        systemuser.setLastupdateddatetime(new Date());

        String token = "";

        if (!isUpdate) {
            systemuser.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
//            token = Long.toHexString(Double.doubleToLongBits(Math.random()));
//            token = Common.passwordHashed(token);
            token = Common.getUserPasswordToken();
            String password = Common.passwordHashed(token);
            systemuser.setPassword(password);

            Status systemUserStatus = new Status();
            systemUserStatus.setStatusid(MasterDataVarList.AFFINITI_CODE_STATUS_ID_INACTIVE);
            systemuser.setStatus(systemUserStatus);
        } else {
            systemuser.setLastupdateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        }

        systemuserDAO.createOrUpdateSystemUser(systemuser);

        Audittrace audittrace = new Audittrace();
        audittrace.setAffectedpage("Client");
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setCreatedatetime(new Date());
        audittrace.setLastupdateddatetime(new Date());

        if (isUpdate) {
            audittrace.setAfectedid(String.valueOf(c.getClientid()));
            audittrace.setDescription("Client is updated. Client name ' " + c.getName() + " ' And -ID-" + c.getClientid());
            audittrace.setTask("Update");
        } else {
            audittrace.setAfectedid(String.valueOf(c.getClientid()));
            audittrace.setDescription("Client is Created. Client name '  " + c.getName() + " ' And -ID-" + c.getClientid());
            audittrace.setTask("Create");
        }
        auditTraceDAO.insertAuditTrace(audittrace);

        /**
         * ************** MAIL *********************
         */
        if (!isUpdate) {
            Mail m = new Mail();
            m.setMailSender(mailSender);
//            String url = "http://" + request.getServerName() + (request.getServerPort() == 0 ? "" : ":" + request.getServerPort()) + request.getContextPath();
//            String message = "<!DOCTYPE html><html><body>"
//                    + "Click <a href='" + url + "/client/register?username=" + c.getEmail() + "&token=" + token + "'>here</a> to register"
//                    + "</body></html>";
//            System.out.println(message);
//            m.sendMail("avonettestmail@gmail.com", c.getEmail(), "Testing123", message);
            m.sendUserRegistrationMail(request, c.getEmail(), client.getName(), client.getUsername(), token);
        }
    }

//    @Override
//    public JSONArray getClientCategoryListByOrganizationId(int organizationId) throws SQLException, DataAccessException {
//        JSONArray array = new JSONArray();
//        
//        Map<String, String> categories = commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY_BY_ORGANIZATION,
//                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE, organizationId
//        ));
//      
//        
//        for (Map.Entry<String, String> entry : categories.entrySet()) {
//            
//            JSONObject section = new JSONObject();
//            section.put("id", String.valueOf(entry.getKey()));
//            section.put("description", Common.replaceEmptyorNullStringToNA(String.valueOf(entry.getValue())));
//            
//            array.put(section);
//            
//        }
//        
//        return array;
//    }
//    
    @Override
    @Transactional
    public long getClientTableDataCount(String name, String organizationId, String clientCategoryId, int location) throws HibernateException, Exception {
        return clientDAO.getActiveClientsCount(name, organizationId, clientCategoryId, location);
    }

    @Override
    @Transactional
    public List<JSONObject> getClientTableData(String name, String organizationId, int location, String clientCategoryId, int start, int minCount, HttpSession session) throws HibernateException, Exception {
        List<JSONObject> data = new ArrayList<JSONObject>();

        boolean searchByCategory = false;

        if (!clientCategoryId.equals("")) {
            searchByCategory = true;
        }

        for (Client c : clientDAO.getActiveClients(name, organizationId, location, start, minCount)) {

            boolean isSelected = false;

            String categoryDescriptions = "";

            for (Clientclientcategory cat : c.getClientclientcategories()) {

                if (searchByCategory) {
                    if (cat.getClientcategory().getClientcategoryid() == Integer.parseInt(clientCategoryId)) {
                        isSelected = true;
                    }
                } else {
                    isSelected = true;

                }
                categoryDescriptions += (categoryDescriptions.equals("") ? "" : ", ") + cat.getClientcategory().getDescription();
            }

            if (isSelected) {
                JSONObject object = new JSONObject();
                String action = "<div class='row'>"
                        + "<div class='col-md-3'>"
                        + "<li class='fa fa-lg fa-fw fa-eye btn-table-action client-view-icon' data-client-id='" + c.getClientid() + "' title='View'></li>"
                        + "</div>"
                        + "<div class='col-md-3'>"
                        + "<li class='fa fa-lg fa-fw fa-pencil btn-table-action client-edit-icon' data-client-id='" + c.getClientid() + "' title='Edit'></li>"
                        + "</div>"
                        + "<div class='col-md-3'>"
                        + "<li class='fa fa-lg fa-fw fa-trash btn-table-action client-delete-icon' data-client-id='" + c.getClientid() + "' title='Delete'></li>"
                        + "</div>"
                        + "</div>";

                object.put("action", action);
                object.put("name", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getName())));
                object.put("organization", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getOrganization().getDescription())));
//                object.put("clientcategory", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getClientcategory().getDescription())));
                object.put("clientcategory", Common.replaceEmptyorNullStringToNA(categoryDescriptions));
                object.put("location", Common.replaceEmptyorNullStringToNA(String.valueOf((c.getLocation()) == null ? Common.replaceEmptyorNullStringToNA(null) : c.getLocation().getLocationdescription())));
                object.put("status", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getStatus().getDescription())));
                object.put("lastupdateddatetime", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getLastupdateddatetime())));
                object.put("createddatetime", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getCreatedatetime())));
                object.put("createduser", Common.replaceEmptyorNullStringToNA(String.valueOf(c.getCreateduser())));

                data.add(object);
            }
        }
        //Client client = clientDAO.getClientById(clientCategoryId);
        // Clientclientcategory clientclientcategory = clientDAO.getClientcategorybyId(start);

        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid("");
        audittrace.setAffectedpage("Client");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));

        if (clientCategoryId.equals("") || organizationId.equals(" ")) {
            audittrace.setDescription("Client table Viewed ");

        } else if (clientCategoryId != null || organizationId != null) {
       
            Clientcategory clientcategory = clientDAO.getClientcategorybyId(Integer.parseInt(clientCategoryId));
            Organization organization =organizationDAO.getOrganizationId(Integer.parseInt(organizationId));
            audittrace.setDescription("Client table Viewed by Organization of = ' "+organization.getDescription() +  " '  , And clientCategory of = ' " +clientcategory.getDescription()+" '");

        }
        //audittrace.setDescription("Client table load by organizationId=" + organizationId + ", clientCategoryId=" + clientCategoryId);
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("View");

        auditTraceDAO.insertAuditTrace(audittrace);

        return data;
    }

    @Override
    @Transactional
    public JSONArray getProductListByProductCategory(int productCategoryId) throws SQLException, DataAccessException, Exception {
        JSONArray array = new JSONArray();
        int[] statusArr = new int[2];
//            statusArr[0] = MasterDataVarList.AFFINITI_CODE_STATUS_PRODUCT_OPEN;
//            statusArr[1] = MasterDataVarList.AFFINITI_CODE_STATUS_PRODUCT_ASSIGNED;
        statusArr[0] = MasterDataVarList.AFFINITI_CODE_STATUS_PRODUCT_ACTIVE;
        statusArr[1] = MasterDataVarList.AFFINITI_CODE_STATUS_PRODUCT_INACTIVE;
        String statusStr = Arrays.toString(statusArr).replace("[", "").replace("]", "");

        Map<String, String> products = commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCT_BY_PRODUCTCATEGORY,
                productCategoryId, statusStr));

        for (Map.Entry<String, String> entry : products.entrySet()) {

            JSONObject section = new JSONObject();
            section.put("id", String.valueOf(entry.getKey()));
            section.put("description", Common.replaceEmptyorNullStringToNA(String.valueOf(entry.getValue())));

            array.put(section);
        }

        return array;
    }

    // to status service getStatusByCategory
    @Override
    @Transactional
    public JSONArray getClientProductStatusList() throws SQLException, DataAccessException, Exception {
        JSONArray array = new JSONArray();

        Map<String, String> statusList = commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_CLIENTPRODUCT));

        for (Map.Entry<String, String> entry : statusList.entrySet()) {

            JSONObject section = new JSONObject();
            section.put("id", String.valueOf(entry.getKey()));
            section.put("description", Common.replaceEmptyorNullStringToNA(String.valueOf(entry.getValue())));

            array.put(section);

        }
        return array;

    }

    @Override
    @Transactional
    public JSONArray getClientById(int clientId, HttpSession session) throws HibernateException, Exception {
        JSONArray data = new JSONArray();

        Client c = clientDAO.getClientById(clientId);
        JSONObject object = new JSONObject();
        object.put("clientid", String.valueOf(c.getClientid()));
        object.put("name", String.valueOf(c.getName()));

        object.put("primarycontactperson", String.valueOf(c.getPrimarycontactperson()));
        object.put("email", String.valueOf(c.getEmail()));
        object.put("contactno01", String.valueOf(c.getContactno01()));
        object.put("contactno02", String.valueOf(c.getContactno02()));
//        object.put("location", String.valueOf(c.getLocation().getLocationdescription()));
        object.put("location", String.valueOf((c.getLocation()) == null ? null : c.getLocation().getLocationid()));
        object.put("address", String.valueOf(c.getAddress()));

        object.put("organization", String.valueOf(c.getOrganization().getOrganizationid()));
//        object.put("clientcategory", String.valueOf(c.getClientcategory().getClientcategoryid()));

        JSONArray clientCategories = new JSONArray();
        for (Clientclientcategory clientclientcategory : c.getClientclientcategories()) {
            clientCategories.put(clientclientcategory.getClientcategory().getClientcategoryid());
        }
        object.put("clientcategory", clientCategories);
        object.put("status", String.valueOf(c.getStatus().getStatusid()));

        object.put("lastupdatedtime", String.valueOf(c.getLastupdateddatetime()));
        object.put("createdtime", String.valueOf(c.getCreatedatetime()));
        object.put("createduser", String.valueOf(c.getCreateduser()));

        object.put("username", String.valueOf(((Systemuser) ((Employee) c.getEmployees().toArray()[0]).getSystemusers().toArray()[0]).getUsername()));

        JSONArray clientproducts = new JSONArray();
        for (Clientproduct cp : c.getClientproducts()) {
            JSONObject clientproduct = new JSONObject();

            JSONObject product = new JSONObject();
            product.put("id", String.valueOf(cp.getProduct().getProductid()));
            product.put("description", Common.replaceEmptyorNullStringToNA(String.valueOf(cp.getProduct().getDescription())));
            clientproduct.put("product", product);

            JSONObject status = new JSONObject();
            status.put("id", String.valueOf(cp.getStatus().getStatusid()));
            status.put("description", Common.replaceEmptyorNullStringToNA(String.valueOf(cp.getStatus().getDescription())));
            clientproduct.put("status", status);

            clientproduct.put("purchaseddate", String.valueOf(cp.getPurchaseddate()));

            clientproducts.put(clientproduct);
        }

        object.put("clientproducts", clientproducts);

        data.put(object);

        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid(String.valueOf(clientId));
        audittrace.setAffectedpage("Client");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setDescription("Viewed  Single Recorde  in Client Table Client  ID of ' " + clientId+" ' Client Name of ' "
        +c.getName()+" ' Address of ' "+c.getAddress()+" ' Organization of ' "+c.getOrganization().getDescription()+" ' Primary Contact Peason  ' "+c.getPrimarycontactperson()
                +" ' Email of ' "+c.getEmail()+" ' And  Contact Number of ' "+c.getContactno01()+" '");
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("View");

        auditTraceDAO.insertAuditTrace(audittrace);

        return data;
    }

    @Override
    @Transactional
    public void deleteClient(int clientId, HttpSession session) throws HibernateException, Exception {

        Client client = clientDAO.getClientById(clientId);
        Status status = new Status();
        status.setStatusid(MasterDataVarList.AFFINITI_CODE_STATUS_ID_INACTIVE);
        client.setStatus(status);
        //client.getName();
        clientDAO.deleteClient(client);

        Audittrace audittrace = new Audittrace();
        audittrace.setAffectedpage("Client");
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setCreatedatetime(new Date());
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setAfectedid(String.valueOf(clientId));
        audittrace.setDescription("Client is Deleted.  Client ID-'" + clientId + "'. Client Name -'" + client.getName() + "'.");
        audittrace.setTask("Delete");

        auditTraceDAO.insertAuditTrace(audittrace);

    }

    @Override
    @Transactional
    public boolean isNotaDuplicateEmail(String email, String clientid) throws Exception {
        if (clientid.equals("") || clientid == null) {
            // add
            return employeeDAO.isNotaDuplicateEmail(email);
        } else {
            // update
            int employeeId = employeeDAO.getEmployeeByClientId(Integer.parseInt(clientid)).getEmployeeid();
            return employeeDAO.isNotaDuplicateEmailUpdate(email, String.valueOf(employeeId));
        }
    }

}
