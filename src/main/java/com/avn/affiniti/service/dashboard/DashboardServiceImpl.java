/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.dashboard;

//import com.avn.affiniti.dao.activity.ActivityDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.contact.ContactDAO;
import com.avn.affiniti.dao.dashboard.DashboardDAO;
import com.avn.affiniti.hibernate.model.Ticket;
//import com.avn.affiniti.dao.lead.LeadDAO;
//import com.avn.affiniti.dao.target.TargetDAO;
//import com.avn.affiniti.dao.targetperiod.TargetPeriodDAO;
//import com.avn.affiniti.model.lead.LeadChartQueryParameters;
//import com.avn.affiniti.model.target.Target;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author : Roshen Dilshan
 * @Document : DashboardServiceImpl
 * @Created on : May 5, 2016, 9:58:50 AM
 */
@Service("dashboardService")
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    DashboardDAO dashboardDAO;
    // @Autowired
    //TargetPeriodDAO targetPeriodDAO;
    //@Autowired
    //TargetDAO targetDAO;
    //@Autowired
    //LeadDAO leadDAO;
    @Autowired
    CommonDAO commonDAO;
    @Autowired
    ContactDAO contactDAO;
    //@Autowired
    //ActivityDAO activityDAO;

    @Override
    @Transactional
    public JSONArray getAgentTargetGraph(long targetId, int targetPeriodId) throws Exception {
        JSONArray graph = null;
        try {
            //  Target target = targetDAO.getTargetByID(targetId);

            Calendar startCalendar = Calendar.getInstance();
            ///startCalendar.setTime(Common.getStartingTimeofDay(target.getTargetstartdate()));

            Calendar endCalendar = Calendar.getInstance();
            // endCalendar.setTime(Common.getEndingTimeofDay(target.getTargetenddate()));

            int weekOfMonth = startCalendar.get(Calendar.WEEK_OF_MONTH);
            int numberOfDaysInMonth = startCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

            graph = new JSONArray();
            JSONObject graphlines = new JSONObject();
            JSONArray graphpoint = new JSONArray();

            graphpoint.put(Common.getStartingTimeofDay(startCalendar.getTime()).getTime());
            graphpoint.put(BigDecimal.ZERO.setScale(2, RoundingMode.CEILING));
            graphlines.put("label", "Organization Target");
            graphlines.append("data", graphpoint);

            graphpoint = new JSONArray();
            graphpoint.put(Common.getStartingTimeofDay(endCalendar.getTime()).getTime());
            //graphpoint.put(target.getRevenue().setScale(2, RoundingMode.CEILING));
            graphlines.append("data", graphpoint);
            graph.put(graphlines);

            Date range[];
            int i = 1;
            BigDecimal totalAmount = BigDecimal.ZERO;

            graphlines = new JSONObject();
            graphpoint = new JSONArray();

            graphpoint.put(Common.getStartingTimeofDay(startCalendar.getTime()).getTime());
            graphpoint.put(BigDecimal.ZERO.setScale(2, RoundingMode.CEILING));
            graphlines.put("label", "Organization Achievement");
            graphlines.append("data", graphpoint);
            Date today = commonDAO.getCurentTimesStamp();
            while (startCalendar.get(Calendar.WEEK_OF_YEAR) <= endCalendar.get(Calendar.WEEK_OF_YEAR) && i < numberOfDaysInMonth) {
                if (today.getTime() < startCalendar.getTime().getTime()) {
                    break;
                }
                range = new Date[2];
                range[0] = Common.getStartingTimeofDay(startCalendar.getTime());
                while (weekOfMonth == startCalendar.get(Calendar.WEEK_OF_MONTH) && i < numberOfDaysInMonth) {
                    startCalendar.add(Calendar.DATE, 1);
                    i++;
                    if (today.getTime() < startCalendar.getTime().getTime()) {
                        break;
                    }
                }
                if (i == numberOfDaysInMonth) {
                    startCalendar.add(Calendar.DATE, 1);
                }
                weekOfMonth = startCalendar.get(Calendar.WEEK_OF_MONTH);
                range[1] = Common.getStartingTimeofDay(startCalendar.getTime());
                //  LeadChartQueryParameters queryParameters = new LeadChartQueryParameters();
                // queryParameters.setFromdate(range[0]);
                // queryParameters.setTodate(range[1]);
                //queryParameters.setStatusid(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED);
                // queryParameters.setProductid(target.getProductid());

                graphpoint = new JSONArray();
                graphpoint.put(Common.getStartingTimeofDay(startCalendar.getTime()).getTime());
                // BigDecimal amount = leadDAO.getAchievedValue(queryParameters);
                //  if (amount == null) {
                //     amount = BigDecimal.ZERO;
                //  }
                //  totalAmount = totalAmount.add(amount);
                graphpoint.put(totalAmount.setScale(2, RoundingMode.CEILING));
                graphlines.append("data", graphpoint);
            }
            graph.put(graphlines);
        } catch (SQLException sqle) {
            throw sqle;
        }
        return graph;
    }

    ///gajeeeee
    @Override
    @Transactional
    public JSONObject getTicketTableData(HttpServletRequest request, HttpSession session) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<Ticket> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength <= 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            Ticket parameters = this.getParameters(request);//Setting up searched Section Task ID
            String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            iTotalRecords = dashboardDAO.getTableDataCount(parameters, username);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = dashboardDAO.getTableData(parameters, username, param.iDisplayLength, param.iDisplayStart);
                for (Ticket ticket : list) {

                    String action = "<div class=\"row\">"
                            //                            + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + ticket.getTicketid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                            + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + ticket.getTicketid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>";
                           
                    if (ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSREQCSE)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQACCEM)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQPART)) {

                        action += "<div class=\"col-xs-3\"><a target='_blank' href='" + request.getContextPath() + "/accessory " + "'value='accessory_redirect'><i class=\"fa fa-lg fa-fw fa-link\" title=\"accessory_redirect\"></i></a></div>";

                    } else if (ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ADDTMCINV)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TERMHANDTMC)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_DTTIFS)
                            || ticket.getStatus().getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_RETFTMC)) {
                       
                        action += "<div class=\"col-xs-3\"><a target='_blank' href='" + request.getContextPath() + "/inventory/assign/terminal "+"' value='terminal_redirect'><i class=\"fa fa-lg fa-fw fa-link\" title=\"terminal_redirect\"></i></a></div>";

                    }
                    action += "</div>";
                    JSONObject object = new JSONObject();
                    object.put("action", action);
                    object.put("ticketid", (ticket.getTicketid()) != null ? ticket.getTicketid() : "N/A");
                    object.put("assignee", (ticket.getEmployee()) != null ? ticket.getEmployee().getNameinfull() : "N/A");
                    object.put("status", (ticket.getStatus()) != null ? ticket.getStatus().getDescription() : "N/A");
                    object.put("product", (ticket.getProduct()) != null ? ticket.getProduct().getDescription() : "N/A");
                    object.put("ticketcategory", (ticket.getTicketcategory()) != null ? ticket.getTicketcategory().getDescription() : "N/A");
                    object.put("createddatetime", (ticket.getCreateddatetime()) != null ? Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd_hh_mm_a, ticket.getCreateddatetime()) : "N/A");
                    object.put("lastupdatedatetime", (ticket.getLastupdateddatetime()) != null ? Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd_hh_mm_a, ticket.getLastupdateddatetime()) : "N/A");
                    object.put("lastupdateduser", (ticket.getLastupdateduser()) != null ? ticket.getLastupdateduser() : "N/A");

                    // object.put("sectionDes", ticket.getDescription().equals("") ? "N/A" : ticket.getDescription());
                    // object.put("sectionlevel", (String.valueOf(ticket.getSectionlevel())) != null ? ticket.getSectionlevel() : "N/A");
                    // try {
                    //    object.put("parentSection", ticket.getTicket().getDescription());
                    // } catch (NullPointerException e) {
                    //   object.put("parentSection", "N/A");
                    //}
                    // object.put("sectionIcon", (ticket.getIcon()) != null ? ticket.getIcon() : "N/A");
                    // object.put("statusDes", (ticket.getStatus().getDescription()) != null ? ticket.getStatus().getDescription() : "N/A");
                    rows.put(object);
                }
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            e.printStackTrace();

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;
    }

    private Ticket getParameters(HttpServletRequest request) throws Exception {
        Ticket parameters = new Ticket();
        Ticket ticket = new Ticket();

        if (!request.getParameter("searchoptionID").equals("") && request.getParameter("searchoptionID") != null) {
            System.out.println(request.getParameter("searchoptionID"));
            ticket.setTicketid(Integer.parseInt(request.getParameter("searchoptionID")));
        } else {
            ticket.setTicketid(null);
        }
        if (!request.getParameter("searchoptionDES").equals("")) {
            ticket.setDescription("%" + request.getParameter("searchoptionDES") + "%");
        } else {
            ticket.setDescription("");
        }
        parameters.setTicket(ticket);

        return parameters;
    }

    @Override
    @Transactional
    public JSONObject getIdlingTicketTableData(HttpServletRequest request, HttpSession session) throws SQLException, Exception {

        String ViewStatus = "active";
        String UpdateStatus = "active";

        List<Object> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {

            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }

            Ticket parameters = this.getParameters(request);//Setting up searched Section Task ID
            String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            iTotalRecords = dashboardDAO.getIdlingTableDataCount(parameters, username);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = dashboardDAO.getIdlingTableData(parameters, username, param.iDisplayLength, param.iDisplayStart);
                System.out.println("Size: " + list.size());
                Iterator itr = list.iterator();
                while (itr.hasNext()) {
                    Object[] obj = (Object[]) itr.next();
                    String action = "<div class=\"row\">"
                            //                            + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + (String.valueOf(obj[0])) + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                            + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + (String.valueOf(obj[0])) + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                            + "</div>";
                    JSONObject object = new JSONObject();
                    object.put("action", action);
                    object.put("ticketid", Integer.parseInt(String.valueOf(obj[0])));
                    object.put("ticketdate", (String.valueOf(obj[1])) != null ? String.valueOf(obj[1]) : "N/A");
                    object.put("assignee", (String.valueOf(obj[2])) != null ? String.valueOf(obj[2]) : "N/A");
                    object.put("ticketcategory", (String.valueOf(obj[3])) != null ? String.valueOf(obj[3]) : "N/A");
                    object.put("product", (String.valueOf(obj[4])) != null ? String.valueOf(obj[4]) : "N/A");
                    object.put("lastupdatedatetime", (String.valueOf(obj[5])) != null ? String.valueOf(obj[5]) : "N/A");
                    object.put("createddatetime", (String.valueOf(obj[6])) != null ? String.valueOf(obj[6]) : "N/A");
                    object.put("createduser", (String.valueOf(obj[7])) != null ? String.valueOf(obj[7]) : "N/A");

                    rows.put(object);
                }

            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

        } catch (Exception e) {
            e.printStackTrace();

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;

    }

//    @Override
//    @Transactional
//    public com.avn.affiniti.model.customer.Customer getCustomerData(int customerID) throws SQLException {
//
//        List<Object> resultlist = customerDAO.getCustomerDataByCustomerId(customerID);
//        com.avn.affiniti.model.customer.Customer custModel = new com.avn.affiniti.model.customer.Customer();
//        Iterator itr = resultlist.iterator();
//
//        while (itr.hasNext()) {
//            Object[] obj = (Object[]) itr.next();
//            custModel.setCustomerId(Integer.parseInt(String.valueOf(obj[0])));
//            custModel.setCustomer_category_list(String.valueOf(obj[1]));
//            custModel.setCustomer_code(String.valueOf(obj[2]));
//            custModel.setCustomername(String.valueOf(obj[3]));
//            custModel.setTelephone1(String.valueOf(obj[4]));
//            custModel.setTelephone2(String.valueOf(obj[5]));
//            custModel.setMobile(String.valueOf(obj[6]));
//            custModel.setEmail(String.valueOf(obj[7]));
//        }
//
//        List<Address> address = addressDAO.getAddressByCustomerID(customerID);
//        for (Address addressObj : address) {
//
//            custModel.setStreet(addressObj.getStreet());
//            custModel.setBuildingno(addressObj.getBullingno());
//            custModel.setCity(addressObj.getCity());
//
//        }
//
//        return custModel;
//    }
//    @Override
//    @Transactional
//    public JSONObject getBarTargetGarph(long targetId) throws Exception {
//        JSONObject graph = null;
//      //  try {
//            JSONArray targetAmounts = new JSONArray();
//            JSONArray forecastAmounts = new JSONArray();
//            JSONArray achievedAmounts = new JSONArray();
//            JSONArray tick = new JSONArray();
//            JSONArray ticks = new JSONArray();
//           // Target target = targetDAO.getTargetByID(targetId);
//           // LeadChartQueryParameters queryParameters = new LeadChartQueryParameters();
//           // queryParameters.setProductid(target.getProductid());
//            //queryParameters.setStatusid(MasterDataVarList.CCL_CODE_STATUS_LEAD_LOST);
//            //queryParameters.setFromdate(target.getTargetstartdate());
//            //queryParameters.setTodate(target.getTargetenddate());
//            graph = new JSONObject();
//           // BigDecimal targetAmount = target.getRevenue();
//           // if (target.getRevenue() == null) {
//            //    targetAmount = BigDecimal.ZERO;
//            //}
//            JSONArray array = new JSONArray();
//            array.put(0, 0);
//            //array.put(1, targetAmount);
//           // targetAmounts.put(array);
//           // BigDecimal forecastAmount = leadDAO.getForcastValue(queryParameters);
//            //if (forecastAmount == null) {
//           //     forecastAmount = BigDecimal.ZERO;
//           // }
//            array = new JSONArray();
//            array.put(0, 0);
//           // array.put(1, forecastAmount);
//            //forecastAmounts.put(array);
//           // queryParameters.setStatusid(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED);
//          //  BigDecimal achievedAmount = leadDAO.getAchievedValue(queryParameters);
//            //if (achievedAmount == null) {
//               // achievedAmount = BigDecimal.ZERO;
//           // }
//            array = new JSONArray();
//            array.put(0, 0);
//            //array.put(1, achievedAmount);
//          //  achievedAmounts.put(array);
//
//            //tick.put(0);
//          //  tick.put("Organization");
//           // ticks.put(tick);
//          //  graph.put("target", targetAmounts);
//          //  graph.put("forecast", forecastAmounts);
//          //  graph.put("achieved", achievedAmounts);
//          //  graph.put("ticks", ticks);
//       // } catch (Exception sqle) {
//         //   throw sqle;
//      }
//        return graph;
//}
//    @Override
//    @Transactional
//    public JSONObject getUserContactLeadAccountGraph(String username) throws Exception {
//        JSONObject graph = new JSONObject();
//        try {
//            Date[] querter = commonDAO.getCurrentQuerter();
//
//            JSONArray data = new JSONArray();
//            JSONObject dataObject = new JSONObject();
//            JSONArray point = new JSONArray();
//            JSONArray points = new JSONArray();
//            JSONArray tick = new JSONArray();
//            JSONArray ticks = new JSONArray();
//
//            dataObject.put("label", "Contact");
////            dataObject.put("color", "#E74C3C");
//            point.put(contactDAO.getContactCountForUserDateReange(username, querter));
//            point.put(0);
//            points.put(point);
//            dataObject.put("data", points);
//            data.put(dataObject);
//
//            tick.put(0);
//            tick.put("Contact");
//            ticks.put(tick);
//
//            dataObject = new JSONObject();
//            point = new JSONArray();
//            points = new JSONArray();
//            tick = new JSONArray();
//
//            dataObject.put("label", "Lead");
////            dataObject.put("color", "#F1C40F");
//            //point.put(leadDAO.getLeadCountForStatusUserCreatedDateRange(username, MasterDataVarList.CCL_CODE_STATUS_LEAD_INITIAL, querter));
//            point.put(1);
//            points.put(point);
//            dataObject.put("data", points);
//            data.put(dataObject);
//
//            tick.put(1);
//            tick.put("Lead");
//            ticks.put(tick);
//
//            dataObject = new JSONObject();
//            point = new JSONArray();
//            points = new JSONArray();
//            tick = new JSONArray();
//
//            dataObject.put("label", "Account");
////            dataObject.put("color", "#27AE60");
//           // point.put(leadDAO.getLeadCountForStatusUserUpdatedDateRange(username, MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, querter));
//            point.put(2);
//            points.put(point);
//            dataObject.put("data", points);
//            data.put(dataObject);
//
//            tick.put(2);
//            tick.put("Account");
//            ticks.put(tick);
//
//            graph.put("data", data);
//            graph.put("ticks", ticks);
//        } catch (Exception sqle) {
//            throw sqle;
//        }
//        return graph;
//    }
//    @Override
//    @Transactional
//    public JSONObject getUserSuccessFailLeadRatioGraph(String username) throws Exception {
//        JSONObject graph = new JSONObject();
//        try {
//            Date[] querter = commonDAO.getCurrentQuerter();
//
//            JSONObject dataObject = new JSONObject();
//            JSONArray dataObjects = new JSONArray();
//            long totalLeadCount = leadDAO.getLeadContForUserCreatedDateRange(username, querter);
//            //long winningLeadCount = leadDAO.getLeadCountForStatusUserCreatedDateRange(username, MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, querter);
//            //long loosingLeadCount = leadDAO.getLeadCountForStatusUserCreatedDateRange(username, MasterDataVarList.CCL_CODE_STATUS_LEAD_LOST, querter);
//            long pendingLeadCount = totalLeadCount - (winningLeadCount + loosingLeadCount);
//            dataObject.put("label", "Leads Won");
//            try {
//                dataObject.put("data", (winningLeadCount * 100) / totalLeadCount);
//            } catch (ArithmeticException ae) {
//                dataObject.put("data", 0);
//            }
//            dataObject.put("color", "#96AA27");
//            dataObjects.put(dataObject);
//
//            dataObject = new JSONObject();
//            dataObject.put("label", "Leads Pending");
//            try {
//                dataObject.put("data", (pendingLeadCount * 100) / totalLeadCount);
//            } catch (ArithmeticException ae) {
//                dataObject.put("data", 0);
//            }
//            dataObject.put("color", "#F9B433");
//            dataObjects.put(dataObject);
//
//            dataObject = new JSONObject();
//            dataObject.put("label", "Leads Lost");
//            try {
//                dataObject.put("data", (loosingLeadCount * 100) / totalLeadCount);
//            } catch (ArithmeticException ae) {
//                dataObject.put("data", 0);
//            }
//            dataObject.put("color", "#DB5C9B");
//            dataObjects.put(dataObject);
//
//            graph.put("data", dataObjects);
//        } catch (SQLException sqle) {
//            throw sqle;
//        }
//        return graph;
//    }
    // @Override
    // @Transactional
    // public JSONObject getTargetRatioOrganizationGraph(long targetId) throws Exception {
    //   JSONObject graph = null;
    //   try {
    //     Target target = targetDAO.getTargetByID(targetId);
    //     LeadChartQueryParameters parameters = new LeadChartQueryParameters();
    // parameters.setStatusid(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED);
    // parameters.setProductid(target.getProductid());
    // parameters.setFromdate(Common.getStartingTimeofDay(target.getTargetstartdate()));
    // parameters.setTodate(Common.getStartingTimeofDay(target.getTargetenddate()));
    //  BigDecimal achivedAmount = leadDAO.getAchievedValue(parameters);
//            if (achivedAmount == null) {
//                achivedAmount = BigDecimal.ZERO;
//            }
//            BigDecimal ratio = BigDecimal.ZERO;
//            try {
//                BigDecimal prcent = new BigDecimal("100");
//                BigDecimal multiply = achivedAmount.multiply(prcent);
//                ratio = multiply.divide(target.getRevenue(), RoundingMode.HALF_UP);
//            } catch (ArithmeticException ae) {
//            }
//            graph = new JSONObject();
//            graph.put("data", ratio.toString());
//        } catch (Exception sqle) {
//            throw sqle;
//        }
//        return graph;
//    }
//    @Override
//    @Transactional
//    public JSONObject getTargetRatioUserGraph(long targetId, String username) throws Exception {
//        JSONObject graph = null;
//        try {
//            Target target = targetDAO.getTargetByID(targetId);
//            LeadChartQueryParameters parameters = new LeadChartQueryParameters();
//           // parameters.setStatusid(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED);
//            parameters.setProductid(target.getProductid());
//            parameters.setFromdate(Common.getStartingTimeofDay(target.getTargetstartdate()));
//            parameters.setTodate(Common.getStartingTimeofDay(target.getTargetenddate()));
//            parameters.setUsername(username);
//            BigDecimal achivedAmount = leadDAO.getAchievedValueForUser(parameters);
//            if (achivedAmount == null) {
//                achivedAmount = BigDecimal.ZERO;
//            }
//            BigDecimal ratio = BigDecimal.ZERO;
//            try {
//                BigDecimal prcent = new BigDecimal("100");
//                BigDecimal multiply = achivedAmount.multiply(prcent);
//                ratio = multiply.divide(target.getRevenue(), RoundingMode.HALF_UP);
//            } catch (ArithmeticException ae) {
//            }
//            graph = new JSONObject();
//            graph.put("data", ratio.toString());
//        } catch (Exception sqle) {
//            throw sqle;
//        }
//        return graph;
//    }
//    @Override
//    @Transactional
//    public List<Target> getTargetDropDownList(String userName) throws Exception {
//        return targetDAO.getTargetDropDownList(userName);
//    }
//    @Override
//    @Transactional
//    public JSONObject getActivityPerformanceByTargetId(long targetId) throws Exception {
//        JSONObject dataObject = new JSONObject();
//        Target target = targetDAO.getTargetByID(targetId);
////        dataObject.put("activitycount", activityDAO.getActivityCountForActivityId(target.getActivitytpe(),
//             //   target.getTargetstartdate(),
//              //  target.getTargetenddate()));
//        dataObject.put("targetcounts", target.getRevenue().longValue());
//        return dataObject;
//    }
//    @Override
//    @Transactional
//    public JSONObject getActivitiesPerformanceByTargetPeriod(long targetId) throws Exception {
//        JSONObject dataObject = new JSONObject();
//        Target target = targetDAO.getTargetByID(targetId);
//        dataObject.put("calls", activityDAO.getActivityCountForActivityId(MasterDataVarList.AFFINITI_CODE_ACTIVITY_TYPE_CALLS,
//                target.getTargetstartdate(),
//                target.getTargetenddate()));
//        dataObject.put("emails", activityDAO.getActivityCountForActivityId(MasterDataVarList.AFFINITI_CODE_ACTIVITY_TYPE_EMAILS,
//                target.getTargetstartdate(),
//                target.getTargetenddate()));
//        dataObject.put("visits_meetings", activityDAO.getActivityCountForActivityId(MasterDataVarList.AFFINITI_CODE_ACTIVITY_TYPE_VISITS_MEETINGS,
//                target.getTargetstartdate(),
//                target.getTargetenddate()));
//        return dataObject;
//    }
//    @Override
//    @Transactional
//    public JSONObject getUserTicketStatus(String username) throws SQLException {
//        JSONObject jSONObject = new JSONObject();
//        jSONObject.put("opencount", dashboardDAO.getUserTicketCountByStatusIn(new int[]{MasterDataVarList.AFFINITI_CODE_STATUS_OPEN,
//            MasterDataVarList.AFFINITI_CODE_STATUS_REASSIGN, MasterDataVarList.AFFINITI_CODE_STATUS_REOPNE}, username));
//        jSONObject.put("inprogress", dashboardDAO.getUserTicketCountByStatus(MasterDataVarList.AFFINITI_CODE_STATUS_INPROGRESS,
//                username));
//        return jSONObject;
//    }
//    @Override
//    public JSONObject getOrgTicketStatus() throws SQLException {
//        JSONObject jSONObject = new JSONObject();
//        jSONObject.put("opencount", dashboardDAO.getOrgTicketCountByStatusIn(new int[]{MasterDataVarList.AFFINITI_CODE_STATUS_OPEN,
//            MasterDataVarList.AFFINITI_CODE_STATUS_REASSIGN, MasterDataVarList.AFFINITI_CODE_STATUS_REOPNE}));
//        jSONObject.put("inprogress", dashboardDAO.getOrgTicketCountByStatus(MasterDataVarList.AFFINITI_CODE_STATUS_INPROGRESS));
//        return jSONObject;
//    }
//    @Override
//    public JSONObject averageDealClosureTime(long assignid) throws SQLException {
//        JSONObject jSONObject = new JSONObject();
//        try {
//
//            jSONObject.put("avgclosedtime", dashboardDAO.getLeadClosedTimeHours(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, assignid));
//
//            double opencount = dashboardDAO.getLeadCount(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, assignid);
//            double closecount = dashboardDAO.getLeadCount(MasterDataVarList.CCL_CODE_STATUS_LEAD_LOST, assignid);
//
//            double fullcount = opencount + closecount;
//            double ratio = (opencount / fullcount) * 100;
//            jSONObject.put("avgclosedtime", dashboardDAO.getLeadClosedTimeHours(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, assignid));
//            jSONObject.put("avgclosedRatio", ratio);
//
//        } catch (Exception ex) {
//            Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return jSONObject;
//    }
//
//    @Override
//    public JSONArray getLeadCloseOverTime(long assignid) throws Exception {
//        return dashboardDAO.getLeadCloseOverTime(assignid);
//    }
//
//    @Override
//    public JSONArray getLeadLostOverTime(long assignid) throws Exception {
//        return dashboardDAO.getLeadLostOverTime(assignid);
//    }
//    public JSONObject averageDealClosureRation(long assignid) throws SQLException {
//        JSONObject jSONObject = new JSONObject();
//        try {
//
//           double opencount = dashboardDAO.getLeadCount(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, assignid);
//            double closecount = dashboardDAO.getLeadCount(MasterDataVarList.CCL_CODE_STATUS_LEAD_LOST, assignid);
//
//            double fullcount = opencount + closecount;
//
//            double ratio = (opencount / fullcount) * 100;
//
//            jSONObject.put("avgclosedtime", dashboardDAO.getLeadClosedTimeHours(MasterDataVarList.CCL_CODE_STATUS_LEAD_CLOSED, assignid));
//
//        } catch (Exception ex) {
//            Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return jSONObject;
//    }
    //Loading the product category dropdown
//    @Override
//    @Transactional
//    public void loadOpenTicketComponent(Map<String, Object> model) throws SQLException, HibernateException {
//        model.put("productList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITY_DROPDOWN_PRODUCTCATEGORY)));
//
//    }
    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model, HttpSession session) throws SQLException, HibernateException {
        model.put("myOpenTicketCount", dashboardDAO.getOpenTicketCount(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))));
        model.put("myInProgressTicketCount", dashboardDAO.getInProgressTicketCount(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))));
        model.put("IdlingTicketCount", dashboardDAO.getIdlingTicketCount(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))));
        model.put("myOrgInProgressTicketCount", dashboardDAO.getOrgInProgressTicketCount(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))));

    }

}
