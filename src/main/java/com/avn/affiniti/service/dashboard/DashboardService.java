/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.dashboard;

//import com.avn.affiniti.model.target.Target;
import java.lang.annotation.Target;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : DashboardService
 * @Created on : May 5, 2016, 9:57:23 AM
 */
public interface DashboardService {

    public JSONArray getAgentTargetGraph(long targetId, int targetPeriodId) throws Exception;

    public JSONObject getTicketTableData(HttpServletRequest request, HttpSession session) throws SQLException, Exception;

    public JSONObject getIdlingTicketTableData(HttpServletRequest request, HttpSession session) throws SQLException, Exception;
    
   // public void loadOpenTicketComponent(Map<String, Object> model) throws SQLException, HibernateException; 
    //  public JSONObject getBarTargetGarph(long targetId) throws Exception;
    //public JSONObject getUserContactLeadAccountGraph(String username) throws Exception;
    //public JSONObject getUserSuccessFailLeadRatioGraph(String username) throws Exception;
   // public JSONObject getTargetRatioOrganizationGraph(long targetId) throws Exception;
    //public JSONObject getTargetRatioUserGraph(long targetId, String username) throws Exception;
   // public List<Target> getTargetDropDownList(String userName) throws Exception;
   // public JSONObject getActivityPerformanceByTargetId(long targetId) throws Exception;
   // public JSONObject getActivitiesPerformanceByTargetPeriod(long targetId) throws Exception;
  //  public JSONObject getUserTicketStatus(String username) throws SQLException;
    //public JSONObject getOrgTicketStatus() throws SQLException;
   // public JSONObject averageDealClosureTime(long assignid) throws SQLException;
   // public JSONArray getLeadCloseOverTime(long assignid) throws Exception;
  //  public JSONArray getLeadLostOverTime(long assignid) throws Exception;
    //public JSONObject averageDealClosureRation(long assignid) throws SQLException;
    public void loadPageComponent(Map<String, Object> model, HttpSession session) throws SQLException, HibernateException;

    //public void loadPageComponent(Map<String, Object> model, HttpSession session) throws SQLException, HibernateException;

}
