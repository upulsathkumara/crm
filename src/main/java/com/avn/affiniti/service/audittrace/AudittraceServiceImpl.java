package com.avn.affiniti.service.audittrace;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASHOK
 */
@Service
public class AudittraceServiceImpl implements AudittraceService {

    @Autowired
    AuditTraceDAO audittraceDAO;
    
    @Autowired
    CommonDAO commonDAO;

    @Override
    @Transactional
    public JSONObject getSectionTableData(HttpServletRequest request) {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            iTotalRecords = audittraceDAO.getTableDataCount(request);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = audittraceDAO.getTableData(request, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;
    }

    @Override
    public void loadPageComponent(Map<String, Object> model) throws SQLException {        
        
        Map<String, String> affectedPage = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_AUDITTRACE_AFFECTEDPAGES);
        model.put("affectedPageList", affectedPage);
        
        Map<String, String> task = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_AUDITTRACE_TASKS);
        model.put("taskList", task);
        
        Map<String, String> createdUser = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_AUDITTRACE_CREATEDUSERS);
        model.put("createdUserList", createdUser);
    }

}
