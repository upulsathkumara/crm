
package com.avn.affiniti.service.audittrace;

import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;

/**
 *
 * @author ASHOK
 */
public interface AudittraceService {
    public JSONObject getSectionTableData(HttpServletRequest request);
    
    public void loadPageComponent(Map<String, Object> model) throws SQLException;
}
