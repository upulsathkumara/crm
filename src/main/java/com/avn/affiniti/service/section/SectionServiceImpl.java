package com.avn.affiniti.service.section;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.section.SectionDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASHOK
 */
@Service("sectionService")
public class SectionServiceImpl implements SectionService {

    @Autowired
    SectionDAO sectionDAO;

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    StatusDAO statusDAO;

    @Autowired
    AuditTraceDAO auditTraceDAO;

    @Override
    @Transactional//("sessionFactoryTransaction")
    public JSONObject saveSection(com.avn.affiniti.model.section.Section section_info, String createdUser) throws SQLException, Exception {

        JSONObject jsonObj = new JSONObject();

        String page = "Section";
        String task = "";
        String affectedId = "";
        String description = "";

        Section section = new Section();
        Status status = new Status();
        boolean value = false;
        String action = section_info.getAction();

        try {

            if (section_info.getSectionid() != null) {
                section.setSectionid(Integer.parseInt(section_info.getSectionid()));
            }
            section.setDescription(section_info.getSectionDes());
            status.setStatusid(Integer.parseInt(section_info.getStatusid()));
            section.setStatus(status);
            section.setSectionlevel(Integer.parseInt(section_info.getSectionlevel()));

            Section parentSection = new Section();
            if (section.getSectionlevel() != 0) {
                parentSection.setSectionid(Integer.parseInt(section_info.getParentsection()));
            } else {
                parentSection = null;
            }
            section.setSection(parentSection);

            section.setOnlyparent(Boolean.parseBoolean(section_info.getOnlyparent()));
            section.setUrl(section_info.getUrl());
            section.setIcon(section_info.getIcon());
            section.setSortid(Integer.parseInt(section_info.getSortid()));

            section.setCreateddatetime(new Date());
            section.setLastupdateddatetime(new Date());
            section.setCreateduser(section_info.getCreateduser());

            if (section_info.getSectionid() != null) {

                task = "Update";
                Status status1 = statusDAO.getStatsuById(Integer.parseInt(section_info.getStatusid()));

                description = "Updated recode in Section ID of - " + section_info.getSectionid() + " - " + " By Section Description as ' " + section_info.getSectionDes() + " '  Status  of ' " + status1.getDescription()
                        + " ' Level of ' " + section_info.getSectionlevel() + " ' Parent Section  of ' "
                        + section_info.getParentsection() + " ' URL of ' " + section_info.getUrl() + " ' only parent section of ' "
                        + section_info.getOnlyparent() + " ' "
                        + " ' and Icon of '" + section_info.getIcon() + " ' " + " and SortId of ' " + section_info.getSortid() + " '";
                affectedId = section_info.getSectionid();

            } else {
                task = "Add";
                Status status1 = statusDAO.getStatsuById(Integer.parseInt(section_info.getStatusid()));

                description = "Added new recode into Section table. By Section Description as ' " + section_info.getSectionDes() + " '  Status  of ' " + status1.getDescription()
                        + " ' Level of ' " + section_info.getSectionlevel() + " ' Parent Section  of ' "
                        + section_info.getParentsection() + " ' URL of ' " + section_info.getUrl() + " ' only parent section of ' "
                        + section_info.getOnlyparent() + " ' "
                        + " ' and Icon of '" + section_info.getIcon() + " ' " + " and SortId of ' " + section_info.getSortid() + " '";
                //+sc.getDescription();

            }

            value = sectionDAO.insertOrUpdateSection(section, action);

        } catch (HibernateException ex) {
            jsonObj.put("MESSAGE", ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        if (value) {
            jsonObj.put("status", 200);
            jsonObj.put("success", true);
            jsonObj.put("MESSAGE", "Record Saved Successfully...!");
        } else {
            jsonObj.put("status", 500);
            jsonObj.put("success", false);

        }
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        return jsonObj;
    }

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) throws SQLException {
        Map<String, String> status = commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUSBYCATEGORY, MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT));
        model.put("statusList", status);

        Map<String, String> level = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTIONLEVEL);
        model.put("levelList", level);
    }

    @Override
    @Transactional
    public JSONObject getParntSection(String sectionlevel) throws SQLException, Exception {

        JSONObject response = new JSONObject();
        try {
            JSONArray jsonarray = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PARENTSECTION, Integer.parseInt(sectionlevel)));
            jsonarray = Common.removeDropDownElementFromJsonArray("", jsonarray);

            response.put("status", 200);
            response.put("success", true);
            response.put("data", jsonarray);
            response.put("messageService", "Successfull");

        } catch (HibernateException ex) {
            response.put("status", 500);
            response.put("success", false);
            response.put("messageService", ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return response;
    }

    @Override
    @Transactional
    public JSONObject getSectionTableData(HttpServletRequest request, String createdUser) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }String page = "Status";
        String page = "Section";
        String task = "";
        String description = "";
        String affectedId = "";

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            Section parameters = this.getParameters(request);//Setting up searched Section Task ID

            if (parameters.getSection().getSectionid() != null || parameters.getSection().getDescription() != "") {

                task = "Table Search";
                description = "Search Details in Section Table by Section ID of ' " + parameters.getSection().getSectionid() + " ' And Section Description of ' " + parameters.getSection().getDescription() + " '";
                affectedId = "" + parameters.getSection().getSectionid();
            } else {

                task = "Table View";
                description = "Table View all details in Section Table";
                //affectedId = parameters;

            }
            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

            iTotalRecords = sectionDAO.getTableDataCount(parameters);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = sectionDAO.getTableData(parameters, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception ex) {
            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
            Loggers.LOGGER_FILE.error(ex);
        }

        return jsonResponse;
    }

    private Section getParameters(HttpServletRequest request) throws Exception {
        Section parameters = new Section();
        Section section = new Section();

        if (!request.getParameter("searchoptionID").equals("") && request.getParameter("searchoptionID") != null) {
            System.out.println(request.getParameter("searchoptionID"));
            section.setSectionid(Integer.parseInt(request.getParameter("searchoptionID")));
        } else {
            section.setSectionid(null);
        }
        if (!request.getParameter("searchoptionDES").equals("")) {
            section.setDescription(request.getParameter("searchoptionDES"));
        } else {
            section.setDescription("");
        }
        parameters.setSection(section);

        return parameters;
    }

    @Override
    @Transactional
    public JSONObject getSectionBySectionId(String sectionId, HttpSession session) throws SQLException, Exception {

        JSONArray jsonArray = new JSONArray();
        JSONObject details = new JSONObject();
        String page = "Section";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        try {
            Section section = sectionDAO.getSectionBySectionId(sectionId);

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("sectionId", section.getSectionid());
            jsonObj.put("sectionDes", section.getDescription());
            jsonObj.put("sortId", section.getSortid());
            jsonObj.put("sectionlevel", section.getSectionlevel());

            try {
                jsonObj.put("parentSection", section.getSection().getSectionid());
            } catch (NullPointerException ex) {
                jsonObj.put("parentSection", "null");
                Loggers.LOGGER_FILE.error(ex);
            }

            jsonObj.put("sectionIcon", (section.getIcon()) != null ? section.getIcon() : "N/A");
            jsonObj.put("parentOnly", section.isOnlyparent());
            jsonObj.put("statusid", section.getStatus().getStatusid());
            jsonObj.put("url", (section.getUrl()) != null ? section.getUrl() : "N/A");

            task = "View";
                Status status1 = statusDAO.getStatsuById(section.getStatus().getStatusid());


           
            description = "Viewed  Single Recorde  in Section Table Section  ID of '" + section.getSectionid()
                    + " '  Status of ' " + status1.getDescription() + " ' Level of ' " + section.getSectionlevel()
                    + " ' " + " ' Parent Section  of ' " + section.getSection().getSectionid()+ " ' Only Parent of ' " + section.isOnlyparent()+ " ' "
                    + " ' URL of ' " + section.getUrl()+ " ' Icon of ' " + section.getIcon()+ " ' And SortId of ' " + section.getSortid()+ " ' ";


            this.insertAuditTrace(page, affectedId, task, description, createdUser);

            jsonArray.put(jsonObj);

            details.put("status", 200);
            details.put("success", true);
            details.put("data", jsonArray);
            details.put("messageService", "Successfull");

        } catch (HibernateException ex) {
            details.put("status", 500);
            details.put("success", false);
            details.put("messageService", ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return details;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
