package com.avn.affiniti.service.section;

import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author ASHOK
 */
public interface SectionService {

    public JSONObject saveSection(com.avn.affiniti.model.section.Section section_info,String createdUser) throws SQLException, Exception;

    public void loadPageComponent(Map<String, Object> model) throws SQLException;

    public JSONObject getParntSection(String sectionlevel) throws SQLException, Exception;

    public JSONObject getSectionTableData(HttpServletRequest request,String createdUser) throws SQLException, Exception;

    public JSONObject getSectionBySectionId(String sectionId,HttpSession session) throws SQLException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

    }
