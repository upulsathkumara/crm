/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrolesection;

import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.model.userrolesection.UserRoleSection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.DataAccessException;

/**
 * @Author Nadun Chamikara
 * @Document UserRoleSectionService
 * @Created on 10/08/2017, 8:43:32 AM
 */
public interface UserRoleSectionService {

    public long getUserRoleSectionTableDataCount(String userRoleId, String sectionId) throws JSONException, HibernateException, Exception;
    
    public List<JSONObject> getUserRoleSectionTableData(String userRoleId, String sectionId, int start, int minCount, HttpSession session) throws JSONException, HibernateException, Exception;

    public JSONArray getUserRoleSectionsByUserRoleId(String userRoleId, HttpSession session) throws JSONException, HibernateException, Exception;

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException;

    public void createOrUpdateUserRoleSection(UserRoleSection userRoleSection, List<Userrolesection> preUserRoleSections, HttpSession session) throws Exception;

    public JSONArray getSectionsForDropdown() throws SQLException, DataAccessException;

    public List<Userrolesection> getUserRoleSectionsByUserRoleId(String userRoleId) throws Exception;
}
