/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrolesection;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.section.SectionDAO;
import com.avn.affiniti.dao.userrolesection.UserRoleSectionDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.model.userrolesection.UserRoleSection;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Nadun Chamikara
 * @Document UserRoleSectionServiceImpl
 * @Created on 10/08/2017, 8:43:54 AM
 */
@Service("userRoleSectionService")
@Transactional
public class UserRoleSectionServiceImpl implements UserRoleSectionService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private UserRoleSectionDAO userRoleSectionDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Autowired
    private SectionDAO sectionDAO;

    @Override
    public long getUserRoleSectionTableDataCount(String userRoleId, String sectionId) throws JSONException, HibernateException, Exception {
        return userRoleSectionDAO.getUserRoleSectionsCount(userRoleId, sectionId);
    }

    @Override
    public List<JSONObject> getUserRoleSectionTableData(String userRoleId, String sectionId, int start, int minCount, HttpSession session) throws JSONException, HibernateException, Exception {

        List<JSONObject> data = new ArrayList<JSONObject>();

        for (Userrolesection u : userRoleSectionDAO.getUserRoleSections(userRoleId, sectionId, start, minCount)) {
            JSONObject object = new JSONObject();
            String action = "<div class='row'>"
                    + "<div class='col-md-3'>"
                    + "<li class='fa fa-lg fa-fw fa-eye btn-table-action userrolesection-view-icon' data-userrolesection-id='" + u.getUserrole().getUserroleid() + ":" + u.getSection().getSectionid() + "' title='View'></li>"
                    + "</div>"
                    + "<div class='col-md-3'>"
                    + "<li class='fa fa-lg fa-fw fa-pencil btn-table-action userrolesection-edit-icon' data-userrolesection-id='" + u.getUserrole().getUserroleid() + ":" + u.getSection().getSectionid() + "' title='Edit'></li>"
                    + "</div>"
                    + "</div>";

//            object.put("action", u.getId().getUserroleid() + ":" + u.getId().getSectionid());
            object.put("action", action);
            object.put("userrole", u.getUserrole().getDescription().toString());
            object.put("section", u.getSection().getDescription().toString());
            object.put("lastupdatedtime", u.getLastupdateddatetime().toString());
            object.put("createdtime", u.getCreateddatetime().toString());
            object.put("createduser", u.getCreateduser().toString());

            data.add(object);
        }

        Audittrace audittrace = new Audittrace();
        audittrace.setAfectedid("");
        audittrace.setAffectedpage("User role setion");
        audittrace.setCreatedatetime(new Date());
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setDescription("User role section table load by userRoleId=" + userRoleId + ", sectionId=" + sectionId);
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setTask("View");

        auditTraceDAO.insertAuditTrace(audittrace);

        return data;
    }

    @Override
    public JSONArray getUserRoleSectionsByUserRoleId(String userRoleId, HttpSession session) throws JSONException, HibernateException, Exception {
        JSONArray data = new JSONArray();
        for (Userrolesection u : userRoleSectionDAO.getUserRoleSections(userRoleId, "", 0, 0)) {
            JSONObject object = new JSONObject();

//                object.put("userrole", u.getUserrole());
            JSONObject userRole = new JSONObject();
            userRole.put("id", String.valueOf(u.getUserrole().getUserroleid()));
            userRole.put("description", String.valueOf(u.getUserrole().getDescription()));
            object.put("userrole", userRole);

//                object.put("section", u.getSection());
            JSONObject section = new JSONObject();
            section.put("id", String.valueOf(u.getSection().getSectionid()));
            section.put("description", String.valueOf(u.getSection().getDescription()));
            object.put("section", section);

            object.put("lastupdatedtime", Common.replaceEmptyorNullStringToNA(String.valueOf(u.getLastupdateddatetime())));
            object.put("createdtime", Common.replaceEmptyorNullStringToNA(String.valueOf(u.getCreateddatetime())));
            object.put("createduser", Common.replaceEmptyorNullStringToNA(String.valueOf(u.getCreateduser())));

            Audittrace audittrace = new Audittrace();
            audittrace.setAfectedid(String.valueOf(userRoleId));
            audittrace.setAffectedpage("User role setion");
            audittrace.setCreatedatetime(new Date());
            audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            audittrace.setDescription("View user role sections  by userRoleId=" + userRoleId);
            audittrace.setDescription("Viewed  Single Recorde  in User Role Section Table User Role Section ID of '" + u.getUserrole().getUserroleid()
                    + " '  Description of ' " + u.getUserrole().getDescription() + " ' And Section of ' " + u.getSection().getDescription()
                    + " ' ");
            audittrace.setLastupdateddatetime(new Date());
            audittrace.setTask("View");

            auditTraceDAO.insertAuditTrace(audittrace);
            
            data.put(object);
        }

        return data;
    }

    @Override
    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException {
        model.put("userRoleList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLE,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        )));

        model.put("sectionList", commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTION));
    }

    @Override
    public void createOrUpdateUserRoleSection(UserRoleSection userRoleSection, List<Userrolesection> preUserRoleSections, HttpSession session) throws Exception {

        Audittrace audittrace = new Audittrace();

        audittrace.setTask("Assign");
        int id = userRoleSection.getUserRoleId();
        Userrole uid = userRoleSectionDAO.getUserRoleId(id);
        String description = "Assign userolesection UserRole of = ' " + uid.getDescription() + " ' old sections=[";

        for (Userrolesection urs : preUserRoleSections) {

            boolean sectionIdIsToBeRemoved = true;
            int oldUuserRoleId = urs.getUserrole().getUserroleid();
            //  Userrole oldUiD = userRoleSectionDAO.getUserRoleId(oldUuserRoleId);

            Section oldSection = urs.getSection();
//            Section oldSiD = userRoleSectionDAO.getSectionId(oldSectionId);
//            description = description.concat(oldSectionId + ",");
            description = description.concat(oldSection.getDescription() + ",");

            for (Integer newSectionId : userRoleSection.getSectionIds()) {
                if (oldSection.getSectionid() == newSectionId) {
                    sectionIdIsToBeRemoved = false;
                }
            }
            if (sectionIdIsToBeRemoved) {
                int userRoleSectionId = userRoleSectionDAO.findUserRoleSectionIdByUserRoleIdAndSectionId(oldUuserRoleId, oldSection.getSectionid());

                userRoleSectionDAO.deleteUserRoleSectionTaskByUserRoleSectionId(userRoleSectionId);

                userRoleSectionDAO.deleteUserRoleSectionsByUserRoleIdAndSectionId(oldUuserRoleId, oldSection.getSectionid());

            }

        }
        description = description.concat("]");

        description = description.concat(" new sections=[");
        for (Integer sectionId : userRoleSection.getSectionIds()) {
            boolean sectionIdIsToBeSave = true;

            int newSectionId = sectionId;
            for (Userrolesection urs2 : preUserRoleSections) {
                if (newSectionId == urs2.getSection().getSectionid()) {
                    sectionIdIsToBeSave = false;
                }
            }
            if (sectionIdIsToBeSave) {
                Userrolesection u = new Userrolesection();

                Userrole userROleObj = new Userrole();
                userROleObj.setUserroleid(userRoleSection.getUserRoleId());
                u.setUserrole(userROleObj);

                Section sectionObj = new Section();
                sectionObj.setSectionid(sectionId);
                u.setSection(sectionObj);

                u.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
                u.setCreateddatetime(new Date());
                u.setLastupdateddatetime(new Date());
                userRoleSectionDAO.createUserRoleSection(u);

//                description = description.concat(sectionId + ",");
                description = description.concat(sectionDAO.getSectionBySectionId(String.valueOf(sectionId)).getDescription() + ",");

            }

        }
        description = description.concat("]");

        audittrace.setDescription(description);
        audittrace.setAffectedpage("User role section");
        audittrace.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
        audittrace.setCreatedatetime(new Date());
        audittrace.setLastupdateddatetime(new Date());
        audittrace.setAfectedid(String.valueOf(userRoleSection.getUserRoleId()));

        auditTraceDAO.insertAuditTrace(audittrace);

    }

    @Override
    public JSONArray getSectionsForDropdown() throws SQLException, DataAccessException {
        JSONArray array = new JSONArray();

        Map<String, String> sections = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTION);

        for (Map.Entry<String, String> entry : sections.entrySet()) {
            if (!entry.getKey().equals("")) {
                JSONObject section = new JSONObject();
                section.put("id", entry.getKey());
                section.put("description", Common.replaceEmptyorNullStringToNA(String.valueOf(entry.getValue())));

                array.put(section);

            }
        }

        return array;
    }

    @Override
    public List<Userrolesection> getUserRoleSectionsByUserRoleId(String userRoleId) throws Exception {
        return userRoleSectionDAO.getUserRoleSections(userRoleId, "", 0, 0);
    }

}
