package com.avn.affiniti.service.organization;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.organization.OrganizationDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.License;
import com.avn.affiniti.hibernate.model.Location;
import com.avn.affiniti.hibernate.model.Organization;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kaushan Fernando
 * @Document OrganizationServiceImpl
 * @Created on: Sep 20, 2017, 8:58:51 AM
 */
@Service("organizationService")
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private StatusDAO statusDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Autowired
    private OrganizationDAO organizationDAO;

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) throws HibernateException, SQLException, JSONException, Exception {
        model.put("licenselist", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_LICENSE,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        )));

        model.put("statuslist", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT
        )));

        model.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        model.put("success", true);
        model.put("message", "Successfull");
    }

    @Override
    @Transactional
    public JSONObject getOrganizationDetailsTableData(HttpServletRequest request, int location, String createdUser) throws HibernateException, SQLException, JSONException, Exception {

        //        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
//        String ViewStatus = "active";
//        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        String page = "Organization";
        String task = "";
        String description = "";
        String affectedId = "";

//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }
//            if (Boolean.valueOf(request.getParameter("search"))) {

        String organizationId = request.getParameter("organizationid");

        iTotalRecords = organizationDAO.getTableDataCount(organizationId, location);//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = organizationDAO.getTableData(organizationId, location, param.iDisplayLength, param.iDisplayStart);

            rows = new JSONArray(list);

            if (!organizationId.equals("")) {

                task = "Table Search";
                description = "Table Search details in Organization by Organization Name " + organizationId;
                affectedId = organizationId;

//                JSONArray data = new JSONArray();
//                String organizationDescription = "";
//
//                data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT_USERROLES,
//                        organizationId
//                ));
//                for (int i = 0; i < data.length(); i++) {
//                    JSONObject jSONObject = data.getJSONObject(i);
//                    
//                        organizationDescription = jSONObject.getString("value");
//                   
//                }
            } else {
                organizationId = "";
                task = "Table View";
                description = "Table View all details in Organization ";
                affectedId = "";

            }
            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log
        }

        jsonResponse.put("status", 200);
        jsonResponse.put("success", true);
        jsonResponse.put("message", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        return jsonResponse;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject saveOrganization(HttpServletRequest request, HttpSession session, String action, int location) throws HibernateException, SQLException, JSONException, Exception {

        String successMessage = "";

        successMessage = String.format(MessageVarList.SAVED_SUCCESSFULLY, "Organization");
        String organizationInfo = request.getParameter("organizationInfo");
        String licenseInfo = request.getParameter("licenseInfo");
        String statusInfo = request.getParameter("statusInfo");
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        Organization organization = new ObjectMapper().readValue(organizationInfo, Organization.class);
        organization.setStatus((Status) new ObjectMapper().readValue(statusInfo, Status.class));
        organization.setLicense((License) new ObjectMapper().readValue(licenseInfo, License.class));
        Location loc = new Location();
        loc.setLocationid(location);
        organization.setLocation(loc);
        organization.setCreateduser(createdUser);

        //  License license = organizationDAO.getLicense(Integer.parseInt(String.valueOf(License.class)));
        //SettingUp Audit Trace Parameters
        String task = "Add";
        String affectedId = "";
        //Organization OrganizationObj = organizationDAO.getOrganizationByOrganizationId(organization.getOrganizationid());
        Status status = statusDAO.getStatsuById(organization.getStatus().getStatusid());
        License license = organizationDAO.getLicense(organization.getLicense().getLicenseid());
        String description = "Added new Organization and Organization Code :" + organization.getOrganizationcode() + ", "
                + "Organization Name :" + organization.getDescription() + ", "
                + "Contact Number : " + organization.getContactno() + ", "
                + "Email : " + organization.getEmail() + ", "
                + "License Number : " + license.getDescription() + ", "
                + "Status : " + status.getDescription();
        String page = "Organization";

        if (organization.getOrganizationid() != null) {
            Organization oldOrganizationObj = organizationDAO.getOrganizationByOrganizationId(organization.getOrganizationid());
            task = "Update";
            affectedId = organization.getOrganizationid().toString();
            //Inventoryhardwareitem ic = inventoryDAO.getInventoryCategoryById(hardwareitem.getInventorycategory().getInventorycategoryid());

            // ls = organizationDAO.getLicense(location)
            organization.setCreatedatetime(oldOrganizationObj.getCreatedatetime());
            description = "Updated Organization by Organization Code :" + oldOrganizationObj.getOrganizationcode() + " and Organization Name :" + oldOrganizationObj.getDescription() + ", "
                    + "Contact Number changed from : " + oldOrganizationObj.getContactno() + " to " + organization.getContactno() + ", "
                    + "Email changed from : " + oldOrganizationObj.getEmail() + " to " + organization.getEmail() + ", "
                    + "License Number changed from : " + oldOrganizationObj.getLicense().getDescription() + " to " + license.getDescription() + ", "
                    + "Status changed from : " + oldOrganizationObj.getStatus().getDescription() + " to " + status.getDescription();
            successMessage = String.format(MessageVarList.UPDATED_SUCCESSFULLY, "Organization");
        } else {
            organization.setCreatedatetime(new Date());
        }
        organization.setLastupdateddatetime(new Date());
        //End SettingUp Audit Trace Parameters

        JSONObject object = organizationDAO.saveOrganization(organization, action);

        if (object.getBoolean("code")) {
            object.put("message", successMessage);
            object.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            object.put("success", true);
        } else {
            object.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("success", false);
        }

        this.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        return object;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws HibernateException, SQLException, JSONException, Exception {
        Audittrace auditTrace = new Audittrace();

        auditTrace.setAffectedpage(page);
        auditTrace.setTask(task);
        auditTrace.setCreateduser(createdUser);
        auditTrace.setCreatedatetime(new Date());
        auditTrace.setLastupdateddatetime(new Date());
        auditTrace.setAfectedid(affectedId);
        auditTrace.setDescription(description);

        auditTraceDAO.insertAuditTrace(auditTrace);

    }

    @Override
    @Transactional
    public JSONObject getOrganization(int organizationId, HttpSession session) throws HibernateException, SQLException, JSONException, Exception {
        JSONArray jsonArray = new JSONArray();
        JSONObject details = new JSONObject();

        String page = "Organization";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Organization organizationHibModel = organizationDAO.getOrganizationByOrganizationId(organizationId);
        JSONObject jsonObj = new JSONObject();

//            organizationHibModel.setOrganizationcode(org.getOrganizationcode());
//            organizationHibModel.setDescription(org.getDescription());
//            organizationHibModel.setEmail(org.getEmail());
//            organizationHibModel.setContactno(org.getContactno());
//            organizationHibModel.setActivationdate(org.getActivationdate());
//            organizationHibModel.setDatejoined(org.getDatejoined());
//            organizationHibModel.setLicense(org.getLicense());
//            organizationHibModel.setStatus(org.getStatus());
//            String activationDate = formatter.format(organizationHibModel.getActivationdate());
//            String joinedDate = formatter.format(organizationHibModel.getDatejoined());
        jsonObj.put("organizationCode", organizationHibModel.getOrganizationcode());
        jsonObj.put("organizationDescription", organizationHibModel.getDescription());
        jsonObj.put("email", organizationHibModel.getEmail());
        jsonObj.put("contactNo", organizationHibModel.getContactno());
        jsonObj.put("activationDate", formatter.format(organizationHibModel.getActivationdate()));
        jsonObj.put("dateJoined", formatter.format(organizationHibModel.getDatejoined()));
        jsonObj.put("licenseId", organizationHibModel.getLicense().getLicenseid());
        jsonObj.put("licenseDescription", organizationHibModel.getLicense().getDescription());
        jsonObj.put("statusId", organizationHibModel.getStatus().getStatusid());
        jsonObj.put("statusDescription", organizationHibModel.getStatus().getDescription());

        task = "View";

//affectedId = String.valueOf(id);
        Status status = statusDAO.getStatsuById(organizationHibModel.getStatus().getStatusid());
        License license = organizationDAO.getLicense(organizationHibModel.getLicense().getLicenseid());
        description = "Viewed  Single Recorde  in Organization Table organization ID of '" + organizationHibModel.getOrganizationid()
                + " '  Organization Name of ' " + organizationHibModel.getDescription() + " ' Email of ' " + organizationHibModel.getEmail()
                + " ' " + " ' Contacact Number of ' " + organizationHibModel.getContactno() + " ' Activation Date of ' " + organizationHibModel.getActivationdate() + " ' "
                + " ' Joined Date of ' " + organizationHibModel.getDatejoined() + " ' License of ' " + license.getDescription() + " ' And Status of ' " + status.getDescription() + " ' ";
        // +pc.getDescription() + " ' Product Name of ' " + product.getDescription() + " ' And Status of ' " + status.getDescription() + " '";
        // product.setCreateddatetime(new Date());

        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        jsonArray.put(jsonObj);

        details.put("status", 200);
        details.put("success", true);
        details.put("data", jsonArray);
        details.put("messageService", "Successfull");

        return details;
    }

    @Override
    @Transactional
    public Organization getOldOrganizationObj(String organizationId) throws HibernateException, SQLException, Exception {
        int organizationid = Integer.parseInt(organizationId);
        return organizationDAO.getOrganizationByOrganizationId(organizationid);
    }

    @Override
    @Transactional
    public Organization OrganizationLoc(HttpServletRequest request, HttpSession session, int location) throws HibernateException, SQLException, JSONException, Exception {
        //String organizationInfo = request.getParameter("organizationInfo");

        Organization organization = new Organization();
        Location loc = new Location();
        loc.setLocationid(location);
        organization.setLocation(loc);
        
        return organization;

    }

}
