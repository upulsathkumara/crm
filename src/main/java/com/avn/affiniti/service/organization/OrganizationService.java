package com.avn.affiniti.service.organization;

import com.avn.affiniti.hibernate.model.Organization;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Kaushan Fernando
 * @Document OrganizationService
 * @Created on: Sep 20, 2017, 8:58:17 AM
 */
public interface OrganizationService {

    public void loadPageComponent(Map<String, Object> model) throws HibernateException, SQLException, JSONException, Exception;

    public JSONObject getOrganizationDetailsTableData(HttpServletRequest request, int location, String createdUser) throws HibernateException, SQLException, JSONException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws HibernateException, SQLException, JSONException, Exception;

    public JSONObject saveOrganization(HttpServletRequest request, HttpSession session,String action, int location) throws HibernateException, SQLException, JSONException, Exception;

    public JSONObject getOrganization(int organizationId,HttpSession session) throws HibernateException, SQLException, JSONException, Exception;

    public Organization getOldOrganizationObj(String organizationId) throws HibernateException, SQLException, Exception;

    public Organization OrganizationLoc(HttpServletRequest request, HttpSession session, int location) throws HibernateException, SQLException, JSONException, Exception;

}
