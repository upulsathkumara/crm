/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrolesubsection;

/**
 *
 * @author chandima
 */
import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.subsection.SubsectionDAO;
import com.avn.affiniti.dao.userrole.UserRoleDAO;
import com.avn.affiniti.dao.userrolesubsection.UserroleSubsectionDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.model.dataobject.SubsectionDataObject;
import com.avn.affiniti.model.tempuserrolesubsection.TempUserrolesubsection;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userroleSubsectionService")
public class UserroleSubsectionServiceImpl implements UserroleSubsectionService {

    @Autowired
    UserroleSubsectionDAO userroleSubsectionDAO;

    @Autowired
    UserRoleDAO userroleDAO;

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    HttpSession session;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Autowired
    private SubsectionDAO subsectionDAO;

    @Override
    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException {
        model.put("userRoleList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLE,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        )));
    }

    @Override
    public JSONObject getUserroleSubsectionList(HttpServletRequest request, int max) throws Exception {
        JSONArray array = new JSONArray();
        JSONObject response = new JSONObject();

        List<Userrolesubsection> userrolesubsectionList = userroleSubsectionDAO.getUserrolesubsectionList(max);
        for (Userrolesubsection userrolesubsection : userrolesubsectionList) {
            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a value1='" + userrolesubsection.getSection().getSectionid() + "' value2='" + userrolesubsection.getUserrole().getUserroleid() + "'  value='view' id='" + userrolesubsection.getUserrolesubsectionid() + "'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a value1='" + userrolesubsection.getSection().getSectionid() + "' value2='" + userrolesubsection.getUserrole().getUserroleid() + "'  value='edit' id='" + userrolesubsection.getUserrolesubsectionid() + "'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";
            JSONObject object = new JSONObject();
            object.put("userroleDes", userrolesubsection.getUserrole().getDescription());
            object.put("sectionDes", userrolesubsection.getSection().getDescription());
            object.put("subsectionDes", userrolesubsection.getSubsection().getDescription()); //task     
            object.put("createddatetime", userrolesubsection.getCreateddatetime());  // sub section   
            object.put("lastupdateddatetime", userrolesubsection.getLastupdateddatetime()); // user role
            object.put("createduser", userrolesubsection.getCreateduser()); // section
            object.put("action", action); // created user 

 

            array.put(object);
        }
        response.put("status", 200);
        response.put("message", "Response recieved success");
        response.put("success", true);
        response.put("data", array);

        return response;
    }

    @Override
    @Transactional
    public JSONObject getUserroleSubectionTableData(HttpServletRequest request) throws HibernateException, SQLException, Exception {

        String ViewStatus = "active";
        String UpdateStatus = "active";

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }

        iTotalRecords = userroleSubsectionDAO.getTableDataCount();//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = userroleSubsectionDAO.getTableData(param.iDisplayLength, param.iDisplayStart);
            rows = new JSONArray(list);

        }

        jsonResponse.put("status", 200);
        jsonResponse.put("success", true);
        jsonResponse.put("messageService", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

        return jsonResponse;
    }

    @Override
    @Transactional
    public JSONObject getSections(int id) throws HibernateException, Exception {
        JSONArray array = new JSONArray();
        JSONObject response = new JSONObject();
        List<Userrolesection> userrolesections = userroleSubsectionDAO.getSectionList(id);
        for (Userrolesection userRoleSection : userrolesections) {
            JSONObject object = new JSONObject();
            object.put("SECTIONID", userRoleSection.getSection().getSectionid());
            object.put("DESCRIPTION", userRoleSection.getSection().getDescription());
            array.put(object);

        }
        response.put("status", 200);
        response.put("message", "Response recieved success");
        response.put("success", true);
        response.put("data", array);

        return response;
    }

    @Override
    @Transactional
    public JSONArray getSubSections(SubsectionDataObject subsectionDataObject) throws Exception {
        JSONArray array = new JSONArray();
        List<Subsection> subsections = userroleSubsectionDAO.getSubSectionList(subsectionDataObject);
        for (Subsection subsection : subsections) {
            JSONObject object = new JSONObject();
            object.put("SUBSECTIONID", subsection.getSubsectionid());
            object.put("DESCRIPTION", subsection.getDescription());
            array.put(object);
        }
        return array;
    }

    @Override
    @Transactional
    public JSONArray getAssignedSubSections(SubsectionDataObject subsectionDataObject) throws Exception {
        JSONArray array = new JSONArray();
        List<Userrolesubsection> subsections = userroleSubsectionDAO.getAssignedSubSectionList(subsectionDataObject);
        for (Userrolesubsection subsection : subsections) {
            JSONObject object = new JSONObject();
            object.put("SUBSECTIONID", subsection.getSubsection().getSubsectionid());
            object.put("DESCRIPTION", subsection.getSubsection().getDescription());
            array.put(object);
        }

        return array;
    }

    /**
     * **********************************--Start of the new
     * Function--************************************
     */
    @Override
    @Transactional
    public JSONObject getSubsectionDropdownlists(SubsectionDataObject subsectionDataObject) throws Exception {
        JSONArray array11 = new JSONArray();
        JSONArray array12 = new JSONArray();
        JSONArray array1 = new JSONArray();
        JSONArray array2 = new JSONArray();
        JSONObject object = new JSONObject();

        String page = "User Role Sub section";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        String elementIdToRemove = "";

        array11 = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUBSECTION,
                subsectionDataObject.getSectionid(), subsectionDataObject.getSectionid(), subsectionDataObject.getUserrolid()
        ));

        array1 = Common.removeDropDownElementFromJsonArray(elementIdToRemove, array11);

        array12 = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASIGNED_SUBSECTION,
                subsectionDataObject.getSectionid(), subsectionDataObject.getUserrolid()
        ));

        array2 = Common.removeDropDownElementFromJsonArray(elementIdToRemove, array12);
        
                   task = "View";

            description = "Viewed  Single Recorde  in User Role Sub section Table User Role Sub section  ID of '" + subsectionDataObject.getUserrolid()                    
                    + " ' " + " ' Section ID   of ' " + subsectionDataObject.getSectionid() + " ' "
                    + " ' User Role ID of ' " + subsectionDataObject.getUserrolid()+ " ' ";

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

        object.put("subsection", array1);
        object.put("assignedsubsection", array2);
        return object;
    }

    /**
     * **********************************--End of the new
     * Function--************************************
     */
    @Override
    public JSONArray getUserrols() throws Exception {
        JSONArray array = new JSONArray();
        array = commonDAO.getJSONArrayDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLE);
        return array;
    }

    //Adding userrolesubsection 
    @Override
    @Transactional
    public JSONObject addUserroleSubsection(HttpServletRequest request) throws HibernateException, SQLException, JSONException, Exception {
        JSONObject response = new JSONObject();
        int id = 0;
        String sectiondata = request.getParameter("subinfo");
        String Unsectiondata = request.getParameter("Unsubinfo");
        String option = request.getParameter("command");
        String sid = request.getParameter("id");
        String sec = request.getParameter("section");
        String rol = request.getParameter("role");
        if (!sid.equalsIgnoreCase("")) {
            id = Integer.parseInt(sid);
        }

        JSONArray array = new JSONArray(sectiondata);
        JSONArray remarray = new JSONArray(Unsectiondata);
        List<TempUserrolesubsection> removingUserrolesubsections = new ArrayList<>();

        int useroleid = Integer.parseInt(rol);
        int sectionid = Integer.parseInt(sec);
        List<Userrolesubsection> userrolesubsections = userroleSubsectionDAO.getUserrolesubsectionsByUserroleAndSection(useroleid, sectionid);
        for (int i = 0; i < remarray.length(); i++) {
            String sub = remarray.getJSONObject(i).toString();
            TempUserrolesubsection removingUserrolesubsection = new ObjectMapper().readValue(sub, TempUserrolesubsection.class);
            removingUserrolesubsections.add(removingUserrolesubsection);
        }
        List<Userrolesubsection> removedUserrolesubsections = userroleSubsectionDAO.getRemovingUserrolesubsectionsByUserroleAndSection(removingUserrolesubsections);

        userroleSubsectionDAO.deleteUserroleTasks(removedUserrolesubsections);

        for (Userrolesubsection userrolesubsection : removedUserrolesubsections) {
            if (userrolesubsection != null) {
                userroleSubsectionDAO.deleteUserrolesubsectionsById(userrolesubsection.getUserrolesubsectionid());
            }
        }

        int index = 0;
        int subIdArray[] = new int[array.length()];
        List<com.avn.affiniti.model.userrolsubsection.Userrolesubsection> saveUserrolesubsections = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            String sub = array.getJSONObject(i).toString();
            com.avn.affiniti.model.userrolsubsection.Userrolesubsection userrolesubsection = (com.avn.affiniti.model.userrolsubsection.Userrolesubsection) new ObjectMapper().readValue(sub, com.avn.affiniti.model.userrolsubsection.Userrolesubsection.class);
            saveUserrolesubsections.add(userrolesubsection);

        }

        for (Userrolesubsection userrolesubsection : userrolesubsections) {
            for (com.avn.affiniti.model.userrolsubsection.Userrolesubsection checkUserrolesubsection : saveUserrolesubsections) {
                int USbId = userrolesubsection.getSubsection().getSubsectionid();
                int chkUsbId = Integer.parseInt(checkUserrolesubsection.getSubsectionId());
                if (USbId == chkUsbId) {
                    checkUserrolesubsection.setRemoveStatus(1);
                }
            }
        }

        String page = "UserroleSubsections Add";
        String affectedId = "";
        String task = "Adding Userrole Subsections";
        String createdUser = String.valueOf(session.getAttribute("username"));
        String description = "Adding userrole Subsections to ' ";

        for (com.avn.affiniti.model.userrolsubsection.Userrolesubsection userrolesubsection : saveUserrolesubsections) {
            Userrolesubsection hbuserrolesubsection = new Userrolesubsection();
            if (userrolesubsection.getRemoveStatus() != 1) {

                hbuserrolesubsection.setCreateddatetime(new Date());
                hbuserrolesubsection.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
                hbuserrolesubsection.setLastupdateddatetime(new Date());

                //get section from sectionId and set in the hibernate model
                Section section = new Section();
                section.setSectionid(Integer.parseInt(userrolesubsection.getSectionId()));
                hbuserrolesubsection.setSection(section);

                //Setting up the userrole object 
                Userrole userrole = new Userrole();
                userrole.setUserroleid(Integer.parseInt(userrolesubsection.getUserrolid()));
                hbuserrolesubsection.setUserrole(userrole);

                //Setting subsection object
                Subsection subsection = new Subsection();
                subsection.setSubsectionid(Integer.parseInt(userrolesubsection.getSubsectionId()));
                hbuserrolesubsection.setSubsection(subsection);
                userroleSubsectionDAO.addUserrolesubsection(hbuserrolesubsection);

                Userrole ur = userroleDAO.getUserroleById(hbuserrolesubsection.getUserrole().getUserroleid());
                Subsection sb = subsectionDAO.getSubsectionById(hbuserrolesubsection.getSubsection().getSubsectionid());
//                String page = "UserroleSubsections Add";
//                String affectedId = "";
//                String task = "Adding Userrole Subsections";
//                String description = "Adding userrole Subsections to userrole : " + ur.getDescription()
//                        + " and subsection : " + sb.getDescription();
                //////////////////
//                String page = "UserroleSubsections Add";
//                String affectedId = "";
//                String task = "Adding Userrole Subsections";
//                String description = "Adding userrole Subsections to userrole : " + hbuserrolesubsection.getUserrole().getDescription()
//                        + "and subsection : " + hbuserrolesubsection.getSubsection().getDescription();

//                String createdUser = String.valueOf(session.getAttribute("username"));
                description += " to User Role of : ' " + ur.getDescription() + " ' and subsection : ' " + sb.getDescription() + " ' ";
//                this.insertAuditTrace(page, affectedId, task, description, createdUser);
            }

            if (userrolesubsection.getUserrolesubsectionid() != null && userrolesubsection.getUserrolesubsectionid().equals("")) {

                hbuserrolesubsection.setUserrolesubsectionid(userrolesubsection.getUserrolesubsectionid());
            }

            response.put("data", hbuserrolesubsection);
        }
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        response.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Userole Subsection"));
        response.put("success", true);

        return response;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws HibernateException, SQLException {

        Audittrace auditTrace = new Audittrace();

        auditTrace.setAffectedpage(page);
        auditTrace.setTask(task);
        auditTrace.setCreateduser(createdUser);
        auditTrace.setCreatedatetime(new Date());
        auditTrace.setLastupdateddatetime(new Date());
        auditTrace.setAfectedid(affectedId);
        auditTrace.setDescription(description);

        auditTraceDAO.insertAuditTrace(auditTrace);

    }

}
