/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrolesubsection;

/**
 *
 * @author chandima
 */
import com.avn.affiniti.model.dataobject.SubsectionDataObject;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface UserroleSubsectionService {

    public JSONObject getUserroleSubsectionList(HttpServletRequest request, int max) throws Exception;

    public JSONObject getSections(int id) throws HibernateException, Exception;

    public JSONArray getSubSections(SubsectionDataObject subsectionDataObject) throws Exception;

    public JSONArray getUserrols() throws Exception;

    public JSONArray getAssignedSubSections(SubsectionDataObject subsectionDataObject) throws Exception;

    public JSONObject addUserroleSubsection(HttpServletRequest request) throws HibernateException, SQLException, JSONException, Exception;

    public JSONObject getSubsectionDropdownlists(SubsectionDataObject subsectionDataObject) throws Exception;

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException;

    public JSONObject getUserroleSubectionTableData(HttpServletRequest request) throws HibernateException, SQLException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws HibernateException, SQLException;
}
