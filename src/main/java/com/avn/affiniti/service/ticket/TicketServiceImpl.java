/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.ticket;

import com.avn.affiniti.dao.accessory.AccessoryAssignDAO;
import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.employee.EmployeeDAO;
import com.avn.affiniti.dao.inventory.InventoryDAO;
import com.avn.affiniti.dao.notificationmaillog.NotificationMailLogDAO;
import com.avn.affiniti.dao.notificationtemplate.NotificationTemplateDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.terminalquotation.TerminalQuotationDAO;
import com.avn.affiniti.dao.ticket.TicketDAO;
import com.avn.affiniti.dao.ticketassignment.TicketAssignmentDAO;
import com.avn.affiniti.dao.ticketcategorystatus.TicketCategorySatatusDAO;
import com.avn.affiniti.dao.tickethistory.TicketHistoryDAO;
import com.avn.affiniti.dao.ticketinventoryitem.TicketInventoryhardwareItemDAO;
import com.avn.affiniti.dao.ticketuserpass.TicketUserPassDAO;
import com.avn.affiniti.dao.userrole.UserRoleDAO;
import com.avn.affiniti.hibernate.model.Actiontaken;
import com.avn.affiniti.hibernate.model.Actiontobetaken;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Bank;
import com.avn.affiniti.hibernate.model.Bulkterminallot;
import com.avn.affiniti.hibernate.model.Causeoffault;
import com.avn.affiniti.hibernate.model.Client;
import com.avn.affiniti.hibernate.model.Clientcategory;
import com.avn.affiniti.hibernate.model.Deleveryrequirement;
import com.avn.affiniti.hibernate.model.District;
import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Esp;
import com.avn.affiniti.hibernate.model.Inventorycategory;
import com.avn.affiniti.hibernate.model.Inventoryhardwarehistory;
import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.hibernate.model.Inventoryterminalhistory;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Location;
import com.avn.affiniti.hibernate.model.Natureoffault;
import com.avn.affiniti.hibernate.model.Notificationmaillog;
import com.avn.affiniti.hibernate.model.Notificationtempate;
import com.avn.affiniti.hibernate.model.Product;
import com.avn.affiniti.hibernate.model.Productcategory;
import com.avn.affiniti.hibernate.model.Profiletype;
import com.avn.affiniti.hibernate.model.Purposetype;
import com.avn.affiniti.hibernate.model.Remindernotification;
import com.avn.affiniti.hibernate.model.Reportedby;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Terminalbrand;
import com.avn.affiniti.hibernate.model.Terminalmodel;
import com.avn.affiniti.hibernate.model.Terminalquotation;
import com.avn.affiniti.hibernate.model.Territorymap;
import com.avn.affiniti.hibernate.model.Ticketassignment;
import com.avn.affiniti.hibernate.model.Ticketcategory;
import com.avn.affiniti.hibernate.model.Ticketcategorystatus;
import com.avn.affiniti.hibernate.model.Tickethistory;
import com.avn.affiniti.hibernate.model.Ticketinventoryitems;
import com.avn.affiniti.hibernate.model.Ticketpriority;
import com.avn.affiniti.hibernate.model.Ticketuserpass;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.model.ticket.BackupRemovalTicket;
import com.avn.affiniti.model.ticket.BaseSoftwareInstallationTicket;
import com.avn.affiniti.model.ticket.MaintainancePaymentInvoiceTicket;
import com.avn.affiniti.model.ticket.MerchantRemovalTicket;
import com.avn.affiniti.model.ticket.NewInstallationTicke;
import com.avn.affiniti.model.ticket.ReinitializationTicket;
import com.avn.affiniti.model.ticket.SoftwareHardwareBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalConversionTicket;
import com.avn.affiniti.model.ticket.TerminalRepairMysTicket;
import com.avn.affiniti.model.ticket.TerminalRepairTicket;
import com.avn.affiniti.model.ticket.TerminalSharingTicket;
import com.avn.affiniti.model.ticket.TerminalUpgradeDowngradeTicket;
import com.avn.affiniti.model.ticket.Ticket;
import com.avn.affiniti.model.ticket.TicketParam;
import com.avn.affiniti.model.tickethistory.TicketHistory;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.usermessages.AffinitiMessages;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketServiceImpl
 * @Created on : Apr 28, 2017, 3:58:35 PM
 */
@Service("ticketService")
public class TicketServiceImpl implements TicketService {

    @Autowired
    private CommonDAO commonDAO;
    @Autowired
    private StatusDAO statusDAO;
    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private TicketAssignmentDAO ticketAssignmentDAO;
    @Autowired
    private UserRoleDAO userRoleDAO;
    @Autowired
    private SystemuserDAO systemuserDAO;
    @Autowired
    private EmployeeDAO employeeDAO;
    @Autowired
    private TicketHistoryDAO ticketHistoryDAO;
    @Autowired
    private TicketCategorySatatusDAO ticketCategorySatatusDAO;
    @Autowired
    private TerminalQuotationDAO terminalQuotationDAO;
    @Autowired
    private TicketInventoryhardwareItemDAO ticketinventoryitemDAO;
    @Autowired
    private InventoryDAO inventoryDAO;
    @Autowired
    private AccessoryAssignDAO accessoryAssignDAO;
    @Autowired
    private TicketUserPassDAO ticketUserPassDAO;
    @Autowired
    private AuditTraceDAO auditTraceDAO;
    @Autowired
    private NotificationTemplateDAO notificationTemplateDAO;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private NotificationMailLogDAO notificationMailLogDAO;

    @Override
    public void loadPageComponent(Map<String, Object> model, String username) throws SQLException, HibernateException {
        model.put("ticketPriorityList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_PRIORITY,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        )));
        model.put("ticketCategoryList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_CATEGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE, username
        )));
        model.put("productList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCT,
                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
        )));
    }

    @Override
    public JSONObject getTerminalBrakdownCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();

        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
//        object.put("bankList",
//                commonDAO.getJSONArrayDropdownValueList(
//                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_BANK,
//                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
//                        )
//                )
//        );
        object.put("deleveryrequirementList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DELEVERYREQUIREMENT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("natureoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_NATUREOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontobetakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTOBETAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("reportedbyList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_REPORTEDBY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKETINITIALSTATUS,
                                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN,
                                username
                        )
                )
        );

        return object;
    }

    @Override
    @Transactional
    public JSONObject getNewTerminalInstallationCreatePageData(String username, int locationid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
//        object.put("bankList",
//                commonDAO.getJSONArrayDropdownValueList(
//                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_BANK,
//                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
//                        )
//                )
//        );
        object.put("deleveryrequirementList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DELEVERYREQUIREMENT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        object.put("espList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ESP,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        object.put("profileTypeList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PROFILETYPE,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        object.put("ticketAttributeStatusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TICKETATTRIBUTE_STATUS,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        return object;

    }

    @Override
    @Transactional
    public JSONObject getSoftawreHardwareCreatePageData(String username, int locationid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("natureoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_NATUREOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontobetakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTOBETAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("reportedbyList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_REPORTEDBY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        return object;

    }

    @Override
    public JSONObject getMerchantRemovalCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
//        object.put("terminalmodelList", Common.getEmptyDropdownList(new JSONArray()));
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
//        object.put("bankList",
//                commonDAO.getJSONArrayDropdownValueList(
//                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_BANK,
//                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
//                        )
//                )
//        );
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("terminalDeliverdStatusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALDELIVERYSTATUS,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getBaseSoftwareInstallationCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKETINITIALSTATUS,
                                MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION,
                                username
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getTerminalUpgradeDowngradeCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKETINITIALSTATUS,
                                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE,
                                username
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getBackupRemovalCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKETINITIALSTATUS,
                                MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL,
                                username
                        )
                )
        );
        return object;
    }

    @Override
    public JSONObject getBulkTerminalBreakdownCreatePageData(String username) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKETINITIALSTATUS,
                                MasterDataVarList.AFFINITI_TICKET_CATEGORY_BULK_TERMINAL_BREAKDOWN,
                                username
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getTerminalSharingCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getTerminalConversionCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));

        return object;
    }

    @Override
    public JSONObject getReinitializationCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getTermialBreakdownMalayasiaCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("terminalbrandList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALBRAND,
                                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                                        locationid
                                )
                        ))
        );
        object.put("terminalmodelList", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELS,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        ));
//        object.put("bankList",
//                commonDAO.getJSONArrayDropdownValueList(
//                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_BANK,
//                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
//                        )
//                )
//        );
        object.put("clientCategoryList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENTCATEGORY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("deleveryrequirementList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DELEVERYREQUIREMENT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("natureoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_NATUREOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontobetakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTOBETAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("reportedbyList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_REPORTEDBY,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("assigneeTypeList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNEETYPELIST,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

        return object;
    }

    @Override
    public JSONObject getTermialRepairMalayasiaCreatePageData(String username, int locationid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("parttypeList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PARTTYPE,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("districtList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DISTRICT,
                                locationid,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("deliveryrequirementList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DELIVERYREQUIREMENT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("purposeList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PURPOSETYPE,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        return object;
    }

    @Override
    public Map<String, String> getStatusDropDown() throws SQLException {
        return commonDAO.getDropdownValueList(String.format(
                DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_TICKET
        ));
    }

    @Override
    public JSONArray getTerminalModelDropdownByBrand(int terminalbrand) throws SQLException {
        return commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALMODELSBYBRAND,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE,
                        terminalbrand
                )
        );
    }

    @Override
    public JSONArray getClientDropdownByClientCategory(int clientcategory, int location) throws Exception {

        return commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT_BY_LOCATION,
                        clientcategory,
                        location,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        );
    }

    @Override
    public JSONArray getAccessoryType(int status) throws SQLException {
        return commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACCESSORYTYPE
                )
        );
    }

    @Override
    public JSONArray getProductCategory(int product) throws SQLException {
        return commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_PRODUCTCATEGORY_BY_PRODUCT,
                        product,
                        MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                )
        );
    }

    @Override
    public JSONArray getManualAssigneeListById(int ticketId, String username, String manualassigneestatus, int status) throws SQLException {

        JSONArray assigneelist = new JSONArray();
        List<String> userrolelist = new LinkedList<String>();

        if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCCA)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CCA);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOPO)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_PO);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TERMHANDTMC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_RETFTMC)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSREQCSE)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQPART)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
        } else if (statusDAO.getStatusByStatusId(status).equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCSE)) {
            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
        }

        if (!employeeDAO.getSystemUserByUsername(username).equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CLIENT_ADMIN)
                || !employeeDAO.getSystemUserByUsername(username).equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CLIENT_USER)) {
            String[] statusArr = userrolelist.toArray(new String[0]);
            assigneelist = commonDAO.getJSONArrayDropdownValueList(
                    String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNEELIST,
                            Arrays.toString(statusArr).replaceAll("[\\[\\]\"]", "'").replace(", ", "','"),
                            MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                    )
            );
        }
//        else {
//            assigneelist = commonDAO.getJSONArrayDropdownValueList(
//                    String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_MANUAL_ASSIGNEELIST,
//                            ticketId,
//                            ticketId
//                    )
//            );
//        }
        return assigneelist;
    }

    @Override
    public JSONArray getDefaultAssigneeListById(int ticketId) throws SQLException {
        return Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                        ticketId
                ))
        );
    }

    @Override
    public Boolean validateSerialNumber(String serialNumber) throws SQLException {
        return ticketDAO.validateSerialNumber(serialNumber);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void switchAssignee(int ticketId, String username) throws SQLException {

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket ticket = ticketDAO.getTicket(ticketId);
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(ticket.getTicketcategory().getTicketcategoryid());
        Tickethistory tickethistory = new Tickethistory();
        tickethistory.setTicket(ticket);
        tickethistory.setAffecteddate(currentDateTime);
        Status status = new Status();

        if (systemuserDAO.getUserByUsername(username).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
            status.setStatusid(statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO).getStatusid());
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory().getTicketcategoryid(), userrole.getUserroleid());
            Employee assignee = employeeDAO.getEmployeeByUsername(username);
            ticket.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
        }
        tickethistory.setEmployee(ticket.getEmployee());
        ticket.setStatus(status);
        ticket.setLastupdateddatetime(currentDateTime);
        ticket.setLastupdateduser(username);
        ticketDAO.updateTicket(ticket);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(username);
        ticketHistoryDAO.createTicketHistory(tickethistory);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        //  int ticketid;
        /**
         * Audit Trace END*
         */

        int ticketid = 0;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getCreatedby() != null) {
            t.setCreatedby(ticket.getCreatedby());
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        t.setTid(ticket.getTid());
        t.setMid(ticket.getMid());
        t.setTerminalserialno(ticket.getTerminalserialno());
        if (ticket.getTerminalbrnad() != null) {
            Terminalbrand terminalbrand = new Terminalbrand();
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        if (ticket.getTerminalmodel() != null) {
            Terminalmodel terminalmodel = new Terminalmodel();
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        t.setLotno(ticket.getLotno());
        t.setPartno(ticket.getPartno());
        t.setRevision(ticket.getRevision());
        t.setMac(ticket.getMac());
        t.setPtid(ticket.getPtid());
        if (ticket.getBank() != null) {
            Bank bank = new Bank();
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }

        t.setMerchantname(ticket.getMerchantname());
        t.setContactpersion(ticket.getContactperson());
        t.setContactno(ticket.getContactno());
        if (ticket.getDistrict() != null) {
            District district = new District();
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        t.setLocationaddress(ticket.getLocationaddress());
        if (ticket.getDeliveryrequirement() != null) {
            Deleveryrequirement deleveryrequirement = new Deleveryrequirement();
            deleveryrequirement.setDeleveryrequierementid(ticket.getDeliveryrequirement());
            t.setDeleveryrequirement(deleveryrequirement);
        }
        t.setDeleverydestination(ticket.getDeliverydestination());
        if (ticket.getNatureoffault() != null) {
            Natureoffault natureoffault = new Natureoffault();
            natureoffault.setNatureoffaultid(ticket.getNatureoffault());
            t.setNatureoffault(natureoffault);
        }
        if (ticket.getActiontobetaken() != null) {
            Actiontobetaken actiontobetaken = new Actiontobetaken();
            actiontobetaken.setActiontobetakenid(ticket.getActiontobetaken());
            t.setActiontobetaken(actiontobetaken);
        }
        if (ticket.getCauseoffault() != null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            t.setCauseoffault(causeoffault);
            tickethistory.setCauseoffault(causeoffault);
        }
        if (ticket.getActiontaken() != null) {
            Actiontaken actiontaken = new Actiontaken();
            actiontaken.setActiontakenid(ticket.getActiontaken());
            t.setActiontaken(actiontaken);
            tickethistory.setActiontaken(actiontaken);
        }
        t.setTechnicalofficerincharge(ticket.getTechnicalofficerincharge());
        if (ticket.getReportedby() != null) {
            Reportedby reportedby = new Reportedby();
            reportedby.setReportedid(ticket.getReportedby());
            t.setReportedby(reportedby);
        }
        t.setReportedmerchant(ticket.getReportedmerchant());
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Status status = new Status();

        Employee assignee = new Employee();
        Userrole userrole = null;

        if (systemuserDAO.getSytemuserByUsername(ticket.getCreateduser()).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CCA)) {
            if (ticket.getManualassigneestatus().equals("true")) {
                status = statusDAO.getStatsuById(ticket.getStatus());
                if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                } else if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                }
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                t.setEmployee(employee);
                t.setStatus(status);
                t.setCreateduser(ticket.getCreateduser());
                t.setCreateddatetime(new Date());
                t.setLastupdateddatetime(new Date());
                ticketid = ticketDAO.createTicket(t);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(employee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                tickethistory.setTicket(t);
                tickethistory.setStatus(status);
                tickethistory.setEmployee(employee);
                tickethistory.setLastupdateddatetime(currentDateTime);
                tickethistory.setCreateddatetime(currentDateTime);
                tickethistory.setCreateduser(ticket.getCreateduser());
                ticketHistoryDAO.createTicketHistory(tickethistory);
            } else {
                if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                }
            }
        } else {
            if (ticket.getReportedby() == MasterDataVarList.AFFINITI_CODE_TICKET_REPORTEDBY_BANK) {
                userrole = new Userrole();
                //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_CC);
                //status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCCA);
                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                t.setStatus(status);
                tickethistory.setStatus(status);
            } else {
                userrole = new Userrole();
                //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                //status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC);
                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                t.setStatus(status);
                tickethistory.setStatus(status);
            }
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
            assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            //assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);
            ticketid = ticketDAO.createTicket(t);

            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticketid);
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

            tickethistory.setTicket(t);
            tickethistory.setLastupdateddatetime(currentDateTime);
            tickethistory.setCreateddatetime(currentDateTime);
            tickethistory.setCreateduser(ticket.getCreateduser());
            ticketHistoryDAO.createTicketHistory(tickethistory);
        }

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_BREAKDOWN + " is Created.  ID  " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createNewTerminalInstalationTicket(Ticket ticket) throws HibernateException, SQLException, Exception, Exception {

        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int ticketid;
        /**
         * Audit Trace END*
         */

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        Calendar deploymentDate = Calendar.getInstance();
        deploymentDate.setTime(ticket.getTicketdate());
        Calendar nowDeploymentDate = Calendar.getInstance();
        nowDeploymentDate.set(deploymentDate.get(Calendar.YEAR), deploymentDate.get(Calendar.MONTH), deploymentDate.get(Calendar.DATE));

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        t.setTid(ticket.getTid());
        t.setMid(ticket.getMid());

        if (ticket.getSharingtid() != null) {
            t.setSharingtid(ticket.getSharingtid());
        }
        if (ticket.getSharingmid() != null) {
            t.setSharingmid(ticket.getSharingmid());
        }
        if (ticket.getNacno() != null) {
            t.setNacno(ticket.getNacno().toString());
        }
        if (ticket.getSharingnacno() != null) {
            t.setSharingnacno(ticket.getSharingnacno().toString());
        }
        if (ticket.getProfiletype() != null) {
            Profiletype profiletype = new Profiletype();
            profiletype.setProfiletypeid(ticket.getProfiletype());
            t.setProfiletype(profiletype);
        }

        if (ticket.getManualkey() != null) {
            t.setManualkey(ticket.getManualkey());
        }
        if (ticket.getOffline() != null) {
            t.setOffline(ticket.getOffline());
        }
        if (ticket.getPreauth() != null) {
            t.setPreauth(ticket.getPreauth());
        }
        if (ticket.getTip() != null) {
            t.setTip(ticket.getTip());
        }
        if (ticket.getCashadvance() != null) {
            t.setCashadvance(ticket.getCashadvance());
        }
        if (ticket.getPanmask() != null) {
            t.setPanmask(ticket.getPanmask());
        }
        if (ticket.getL4() != null) {
            t.setL4(ticket.getL4());
        }
        if (ticket.getEmv() != null) {
            t.setEmv(ticket.getEmv());
        }
        if (ticket.getFourdbc() != null) {
            t.setFourdbc(ticket.getFourdbc());
        }
        if (ticket.getVoid_() != null) {
            t.setVoid_(ticket.getVoid_());
        }
        if (ticket.getSettlement() != null) {
            t.setSettlement(ticket.getSettlement());
        }
        if (ticket.getTobeinstalledbyepic() != null) {
            t.setTobeinstalledbyepic(ticket.getTobeinstalledbyepic());
        }
        if (ticket.getPassword() != null) {
            t.setPassword(ticket.getPassword());
        }
        if (ticket.getAutosettlement() != null) {
            t.setAutosettlement(ticket.getAutosettlement());
        }
        if (ticket.getSimno() != null) {
            t.setSimno(ticket.getSimno());
        }
        if (ticket.getDeadlinedate() != null) {
            t.setDeadlinedate(ticket.getDeadlinedate());
        }

        if (ticket.getReceipttext() != null) {
            t.setReceipttext(ticket.getReceipttext());
        }
        if (ticket.getApplicationversion() != null) {
            t.setApplicationversion(ticket.getApplicationversion());
        }
        if (ticket.getVisamasteramexconfig() != null) {
            t.setVisamasteramexconfiguration(ticket.getVisamasteramexconfig());
        }
        if (ticket.getDcc() != null) {
            t.setDcc(ticket.getDcc());
        }
        if (ticket.getClientcontactperson() != null) {
            t.setClientcontactperson(ticket.getClientcontactperson());
        }
        if (ticket.getClientemail() != null) {
            t.setClientemail(ticket.getClientemail());
        }
        if (ticket.getClientcontactno() != null) {
            t.setClientcontactnumber(ticket.getClientcontactno());
        }
        if (ticket.getMerchantcontactperson() != null) {
            t.setMerchantcontactperson(ticket.getMerchantcontactperson());
        }
        if (ticket.getMerchantcontactno() != null) {
            t.setMerchantcontactnumber(ticket.getMerchantcontactno());
        }

        t.setTerminalserialno(ticket.getTerminalserialno());
        if (ticket.getTerminalbrnad() != null) {
            Terminalbrand terminalbrand = new Terminalbrand();
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        if (ticket.getTerminalmodel() != null) {
            Terminalmodel terminalmodel = new Terminalmodel();
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }

        if (ticket.getEsp() != null) {
            Esp esp = new Esp();
            esp.setEspid(ticket.getEsp());
            t.setEsp(esp);
        }

        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        t.setLotno(ticket.getLotno());

        if (ticket.getBank() != null) {
            Bank bank = new Bank();
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        t.setContactpersion(ticket.getContactperson());
        t.setContactno(ticket.getContactno());
        t.setMerchantname(ticket.getMerchantname());

        if (ticket.getDistrict() != null) {
            District district = new District();
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        t.setLocationaddress(ticket.getLocationaddress());
        if (ticket.getDeliveryrequirement() != null) {
            Deleveryrequirement deleveryrequirement = new Deleveryrequirement();
            deleveryrequirement.setDeleveryrequierementid(ticket.getDeliveryrequirement());
            t.setDeleveryrequirement(deleveryrequirement);
        }
        t.setDeleverydestination(ticket.getDeliverydestination());
        t.setTechnicalofficerincharge(ticket.getTechnicalofficerincharge());
        t.setDeploymentdate(now.getTime());
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());
        t.setTerminalpassword(ticket.getTerminalpassword());
        t.setRemarks(ticket.getRemarks());

        //Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO);
        Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);

        Userrole userrole = new Userrole();
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_VO);
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        //Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        Ticketuserpass ticketuserpass = new Ticketuserpass();
        ticketuserpass.setTicket(t);
        Status ticketuserpassstatus = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_ACTIVE);
        ticketuserpass.setStatus(ticketuserpassstatus);
        ticketuserpass.setCreateddatetime(currentDateTime);
        ticketuserpass.setLastupdateddatetime(currentDateTime);
        ticketuserpass.setCreateduser(ticket.getCreateduser());
        boolean ticketUserpassNotEmpty = false;

        if (ticket.getUser1() != null) {
            ticketUserpassNotEmpty = true;
            ticketuserpass.setUser1(ticket.getUser1());
            ticketuserpass.setUserpass1(ticket.getUserpass1());
        }
        if (ticket.getUser2() != null) {
            ticketuserpass.setUser2(ticket.getUser2());
            ticketuserpass.setUserpass2(ticket.getUserpass2());
        }
        if (ticket.getUser3() != null) {
            ticketuserpass.setUser3(ticket.getUser3());
            ticketuserpass.setUserpass3(ticket.getUserpass3());
        }
        if (ticket.getUser4() != null) {
            ticketuserpass.setUser4(ticket.getUser4());
            ticketuserpass.setUserpass4(ticket.getUserpass4());
        }
        if (ticket.getUser5() != null) {
            ticketuserpass.setUser5(ticket.getUser5());
            ticketuserpass.setUserpass5(ticket.getUserpass5());
        }
        if (ticket.getUser6() != null) {
            ticketuserpass.setUser6(ticket.getUser6());
            ticketuserpass.setUserpass6(ticket.getUserpass6());
        }
        if (ticket.getUser7() != null) {
            ticketuserpass.setUser7(ticket.getUser7());
            ticketuserpass.setUserpass7(ticket.getUserpass7());
        }
        if (ticket.getUser8() != null) {
            ticketuserpass.setUser8(ticket.getUser8());
            ticketuserpass.setUserpass8(ticket.getUserpass8());
        }
        if (ticket.getUser9() != null) {
            ticketuserpass.setUser9(ticket.getUser9());
            ticketuserpass.setUserpass9(ticket.getUserpass9());
        }
        if (ticket.getUser10() != null) {
            ticketuserpass.setUser10(ticket.getUser10());
            ticketuserpass.setUserpass10(ticket.getUserpass10());
        }
        if (ticket.getUser11() != null) {
            ticketuserpass.setUser11(ticket.getUser11());
            ticketuserpass.setUserpass11(ticket.getUserpass11());
        }
        if (ticket.getUser12() != null) {
            ticketuserpass.setUser12(ticket.getUser12());
            ticketuserpass.setUserpass12(ticket.getUserpass12());
        }
        if (ticket.getUser13() != null) {
            ticketuserpass.setUser13(ticket.getUser13());
            ticketuserpass.setUserpass13(ticket.getUserpass13());
        }
        if (ticket.getUser14() != null) {
            ticketuserpass.setUser14(ticket.getUser14());
            ticketuserpass.setUserpass14(ticket.getUserpass14());
        }
        if (ticket.getUser15() != null) {
            ticketuserpass.setUser15(ticket.getUser15());
            ticketuserpass.setUserpass15(ticket.getUserpass15());
        }
        if (ticket.getUser16() != null) {
            ticketuserpass.setUser16(ticket.getUser16());
            ticketuserpass.setUserpass16(ticket.getUserpass16());
        }
        if (ticket.getUser17() != null) {
            ticketuserpass.setUser17(ticket.getUser17());
            ticketuserpass.setUserpass17(ticket.getUserpass17());
        }
        if (ticket.getUser18() != null) {
            ticketuserpass.setUser18(ticket.getUser18());
            ticketuserpass.setUserpass18(ticket.getUserpass18());
        }
        if (ticket.getUser19() != null) {
            ticketuserpass.setUser19(ticket.getUser19());
            ticketuserpass.setUserpass19(ticket.getUserpass19());
        }
        if (ticket.getUser20() != null) {
            ticketuserpass.setUser20(ticket.getUser20());
            ticketuserpass.setUserpass20(ticket.getUserpass20());
        }

        if (ticketUserpassNotEmpty) {
            ticketUserPassDAO.createTicketUserPass(ticketuserpass);
        }

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_NEW_TERMINAL_INSTALLATION + " is Created.  ID  " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createSoftwareHardwareBreakdownTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int ticketid;
        /**
         * Audit Trace END*
         */

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        if (ticket.getDistrict() != null) {
            District district = new District();
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        if (ticket.getMerchantname() != null) {
            t.setMerchantname(ticket.getMerchantname());
        }
        if (ticket.getContactperson() != null) {
            t.setContactpersion(ticket.getContactperson());
        }
        if (ticket.getContactno() != null) {
            t.setContactno(ticket.getContactno());
        }
        if (ticket.getLocationaddress() != null) {
            t.setLocationaddress(ticket.getLocationaddress());
        }
        if (ticket.getAditionalcosts() != null) {
            t.setAdditionalcostsinvolved(ticket.getAditionalcosts());
        }
        if (ticket.getBackupdeviceserial() != null) {
            t.setBackupdeviceserial(ticket.getBackupdeviceserial());
        }
        if (ticket.getTerminalbrnad() != null) {
            Terminalbrand terminalBd = new Terminalbrand();
            terminalBd.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalBd);
        }
        if (ticket.getTerminalmodel() != null) {
            Terminalmodel terminalModl = new Terminalmodel();
            terminalModl.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalModl);
        }
        if (ticket.getTerminalserialno() != null) {
            t.setTerminalserialno(ticket.getTerminalserialno());
        }
        if (ticket.getWarrantyexpirystatus() != null) {
            t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        }
        if (ticket.getAmcexpirystatus() != null) {
            t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        }
//        if(ticket.getCreatedby() != null){
//            t.setCreateduser(ticket.getCreatedby());
//        }
        if (ticket.getNatureoffault() != null) {
            Natureoffault natureoffault = new Natureoffault();
            natureoffault.setNatureoffaultid(ticket.getNatureoffault());
            t.setNatureoffault(natureoffault);
        }
        if (ticket.getActiontobetaken() != null) {
            Actiontobetaken actiontobetaken = new Actiontobetaken();
            actiontobetaken.setActiontobetakenid(ticket.getActiontobetaken());
            t.setActiontobetaken(actiontobetaken);
        }
        if (ticket.getTechnicalofficerincharge() != null) {
            t.setTechnicalofficerincharge(ticket.getTechnicalofficerincharge());
        }
        if (ticket.getReportedby() != null) {
            Reportedby reportedby = new Reportedby();
            reportedby.setReportedid(ticket.getReportedby());
            t.setReportedby(reportedby);
        }
        if (ticket.getCreatedby() != null) {
            t.setCreatedby(ticket.getCreatedby());
        }
         if (ticket.getCauseoffault()!= null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            t.setCauseoffault(causeoffault);
        }

        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        //Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOPO);
        Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);

        Userrole userrole = new Userrole();
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_PO);
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        //Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_SOFWARE_HARDWARE_BREAKDOWN + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createMerchantRemovalTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int ticketid;
        /**
         * Audit Trace END*
         */

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        Natureoffault natureoffault = new Natureoffault();
        if (ticket.getNatureoffault() != null) {
            natureoffault.setNatureoffaultid(ticket.getNatureoffault());
            t.setNatureoffault(natureoffault);
        }
        Bank bank = new Bank();
        if (ticket.getBank() != null) {
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }

        t.setMerchantname(ticket.getMerchantname());
        t.setTid(ticket.getTid());
        t.setMid(ticket.getMid());
        t.setTerminalserialno(ticket.getTerminalserialno());

        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Status status;
        Userrole userrole = new Userrole();
        //if (ticket.getTerminaldeliverystatus() == MasterDataVarList.AFFINITI_CODE_STATUS_TERMINAL_DELIVERY_YES) {
        //status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC);
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TMC);
        status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        //} else {
        //status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO);
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_VO);
        //status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        //}

        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);

        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        //Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_MERCHANT_REMOVAL + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createBaseSoftwareInstallationTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int ticketid = 0;
        /**
         * Audit Trace END*
         */

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Bank bank = new Bank();
        if (ticket.getBank() != null) {
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }

        t.setLocationaddress(ticket.getLocationaddress());
        t.setBoxnumber(ticket.getBoxnumber());
        t.setStartserialno(ticket.getStartserialno());
        t.setEndserialno(ticket.getEndserialno());

        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Status status = statusDAO.getStatsuById(ticket.getStatus());
        t.setStatus(status);
        tickethistory.setStatus(status);
        Employee assignee = new Employee();
        if (status.getIsfinalstatus() != 1) {

            Userrole userrole = null;
//            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
//                userrole = new Userrole();
////                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
//                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
////                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC);
//                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
//                t.setStatus(status);
//                tickethistory.setStatus(status);
//            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
//                userrole = new Userrole();
////                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
//                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
////                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO);
//                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
//                t.setStatus(status);
//                tickethistory.setStatus(status);
//            }
//            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
////            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
//            Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
//            if (ticketassignment == null) {
//                ticketassignment = new Ticketassignment();
//                ticketassignment.setCreateddatetime(currentDateTime);
//                ticketassignment.setCreateduser(ticket.getCreateduser());
//            }
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//            ticketid = ticketDAO.createTicket(t);
//
//            ticketassignment.setTicketcategory(ticketcategory);
//            ticketassignment.setUserrole(userrole);
//            ticketassignment.setEmployee(assignee);
//            ticketassignment.setLastupdateddatetime(currentDateTime);
//            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
            if (ticket.getManualassigneestatus().equals("true")) {
                if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                } else if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                }
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                assignee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                t.setEmployee(assignee);
                t.setCreateduser(ticket.getCreateduser());
                t.setCreateddatetime(new Date());
                t.setLastupdateddatetime(new Date());
                ticketid = ticketDAO.createTicket(t);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                tickethistory.setTicket(t);
                tickethistory.setEmployee(assignee);
                tickethistory.setLastupdateddatetime(currentDateTime);
                tickethistory.setCreateddatetime(currentDateTime);
                tickethistory.setCreateduser(ticket.getCreateduser());
                ticketHistoryDAO.createTicketHistory(tickethistory);
            } else {
                if (!systemuserDAO.getSytemuserByUsername(ticket.getCreateduser()).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC)) {
                    if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                        userrole = new Userrole();
                        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                        assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                        status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                        if (ticketassignment == null) {
                            ticketassignment = new Ticketassignment();
                            ticketassignment.setCreateddatetime(currentDateTime);
                            ticketassignment.setCreateduser(ticket.getCreateduser());
                        }

                        t.setEmployee(assignee);
                        t.setStatus(status);
                        t.setCreateduser(ticket.getCreateduser());
                        t.setCreateddatetime(new Date());
                        t.setLastupdateddatetime(new Date());
                        ticketid = ticketDAO.createTicket(t);

                        ticketassignment.setTicketcategory(ticketcategory);
                        ticketassignment.setUserrole(userrole);
                        ticketassignment.setEmployee(assignee);
                        ticketassignment.setLastupdateddatetime(currentDateTime);
                        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                        tickethistory.setTicket(t);
                        tickethistory.setEmployee(assignee);
                        tickethistory.setStatus(status);
                        tickethistory.setLastupdateddatetime(currentDateTime);
                        tickethistory.setCreateddatetime(currentDateTime);
                        tickethistory.setCreateduser(ticket.getCreateduser());
                        ticketHistoryDAO.createTicketHistory(tickethistory);
                    } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                        userrole = new Userrole();
                        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                        assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                        status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                        if (ticketassignment == null) {
                            ticketassignment = new Ticketassignment();
                            ticketassignment.setCreateddatetime(currentDateTime);
                            ticketassignment.setCreateduser(ticket.getCreateduser());
                        }

                        t.setEmployee(assignee);
                        t.setStatus(status);
                        t.setCreateduser(ticket.getCreateduser());
                        t.setCreateddatetime(new Date());
                        t.setLastupdateddatetime(new Date());
                        ticketid = ticketDAO.createTicket(t);

                        ticketassignment.setTicketcategory(ticketcategory);
                        ticketassignment.setUserrole(userrole);
                        ticketassignment.setEmployee(assignee);
                        ticketassignment.setLastupdateddatetime(currentDateTime);
                        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                        tickethistory.setTicket(t);
                        tickethistory.setEmployee(assignee);
                        tickethistory.setStatus(status);
                        tickethistory.setLastupdateddatetime(currentDateTime);
                        tickethistory.setCreateddatetime(currentDateTime);
                        tickethistory.setCreateduser(ticket.getCreateduser());
                        ticketHistoryDAO.createTicketHistory(tickethistory);
                    }
                } else {
                    if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                        userrole = new Userrole();
                        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                        assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                        status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC);
                        if (ticketassignment == null) {
                            ticketassignment = new Ticketassignment();
                            ticketassignment.setCreateddatetime(currentDateTime);
                            ticketassignment.setCreateduser(ticket.getCreateduser());
                        }

                        t.setEmployee(assignee);
                        t.setStatus(status);
                        t.setCreateduser(ticket.getCreateduser());
                        t.setCreateddatetime(new Date());
                        t.setLastupdateddatetime(new Date());
                        ticketid = ticketDAO.createTicket(t);

                        ticketassignment.setTicketcategory(ticketcategory);
                        ticketassignment.setUserrole(userrole);
                        ticketassignment.setEmployee(assignee);
                        ticketassignment.setLastupdateddatetime(currentDateTime);
                        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                        tickethistory.setTicket(t);
                        tickethistory.setEmployee(assignee);
                        tickethistory.setStatus(status);
                        tickethistory.setLastupdateddatetime(currentDateTime);
                        tickethistory.setCreateddatetime(currentDateTime);
                        tickethistory.setCreateduser(ticket.getCreateduser());
                        ticketHistoryDAO.createTicketHistory(tickethistory);
                    } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                        userrole = new Userrole();
                        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                        assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                        status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO);
                        if (ticketassignment == null) {
                            ticketassignment = new Ticketassignment();
                            ticketassignment.setCreateddatetime(currentDateTime);
                            ticketassignment.setCreateduser(ticket.getCreateduser());
                        }

                        t.setEmployee(assignee);
                        t.setStatus(status);
                        t.setCreateduser(ticket.getCreateduser());
                        t.setCreateddatetime(new Date());
                        t.setLastupdateddatetime(new Date());
                        ticketid = ticketDAO.createTicket(t);

                        ticketassignment.setTicketcategory(ticketcategory);
                        ticketassignment.setUserrole(userrole);
                        ticketassignment.setEmployee(assignee);
                        ticketassignment.setLastupdateddatetime(currentDateTime);
                        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                        tickethistory.setTicket(t);
                        tickethistory.setEmployee(assignee);
                        tickethistory.setStatus(status);
                        tickethistory.setLastupdateddatetime(currentDateTime);
                        tickethistory.setCreateddatetime(currentDateTime);
                        tickethistory.setCreateduser(ticket.getCreateduser());
                        ticketHistoryDAO.createTicketHistory(tickethistory);
                    }
                }
            }
        } else {
            t.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            ticketid = ticketDAO.createTicket(t);
        }

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);
        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_BASE_SOFTWARE_INSTALLATION + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createTerminalUpgradeDowngradeTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int ticketid = 0;
        /**
         * Audit Trace END*
         */

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        Bank bank = new Bank();
        if (ticket.getBank() != null) {
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }

        t.setMid(ticket.getMid());
        t.setTid(ticket.getTid());
        t.setMerchantname(ticket.getMerchantname());
        t.setLocationaddress(ticket.getLocationaddress());

        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Status status = null;
        Userrole userrole = null;
        Employee assignee = null;
        if (systemuserDAO.getSytemuserByUsername(ticket.getCreateduser()).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CCA)) {
            status = statusDAO.getStatsuById(ticket.getStatus());
        } else {
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        }

        if (ticket.getManualassigneestatus().equals("true")) {
            if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                userrole = new Userrole();
                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
            }
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
            assignee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }

            t.setEmployee(assignee);
            t.setStatus(status);
            t.setCreateduser(ticket.getCreateduser());
            t.setCreateddatetime(new Date());
            t.setLastupdateddatetime(new Date());
            ticketid = ticketDAO.createTicket(t);

            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            tickethistory.setTicket(t);
            tickethistory.setStatus(status);
            tickethistory.setEmployee(assignee);
            tickethistory.setLastupdateddatetime(currentDateTime);
            tickethistory.setCreateddatetime(currentDateTime);
            tickethistory.setCreateduser(ticket.getCreateduser());
            ticketHistoryDAO.createTicketHistory(tickethistory);
        } else {
            if (!systemuserDAO.getSytemuserByUsername(ticket.getCreateduser()).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CCA)) {
                if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                }
            } else {
                if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                }
            }
        }

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);
        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_UPGRADE_DOWNGRADWE + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createBackupRemovalTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int ticketid = 0;
        /**
         * Audit Trace END*
         */

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        District district = new District();
        if (ticket.getDistrict() != null) {
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        if (ticket.getContactperson() != null) {
            t.setContactpersion(ticket.getContactperson());
        }
        if (ticket.getContactno() != null) {
            t.setContactno(ticket.getContactno());
        }
        t.setMid(ticket.getMid());
        t.setTid(ticket.getTid());
        t.setCurrency(ticket.getCurrency());
        t.setRemarks(ticket.getSpecialremark());
        t.setDescription(ticket.getDescription());
        t.setLotno(ticket.getLotno());
        t.setMerchantname(ticket.getMerchantname());
        t.setLocationaddress(ticket.getLocationaddress());
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Status status = statusDAO.getStatsuById(ticket.getStatus());
        Employee assignee = null;

        Userrole userrole = new Userrole();
        if (ticket.getManualassigneestatus().equals("true")) {
            if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                userrole = new Userrole();
                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
            } else if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                userrole = new Userrole();
                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
            }
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
            assignee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }

            t.setEmployee(assignee);
            t.setStatus(status);
            t.setCreateduser(ticket.getCreateduser());
            t.setCreateddatetime(new Date());
            t.setLastupdateddatetime(new Date());
            ticketid = ticketDAO.createTicket(t);

            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            tickethistory.setTicket(t);
            tickethistory.setEmployee(assignee);
            tickethistory.setStatus(status);
            tickethistory.setLastupdateddatetime(currentDateTime);
            tickethistory.setCreateddatetime(currentDateTime);
            tickethistory.setCreateduser(ticket.getCreateduser());
            ticketHistoryDAO.createTicketHistory(tickethistory);
        } else {
            if (!systemuserDAO.getSytemuserByUsername(ticket.getCreateduser()).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC)) {
                if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                }
            } else {
                if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                    userrole = new Userrole();
                    userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                    Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                    assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                    status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO);
                    if (ticketassignment == null) {
                        ticketassignment = new Ticketassignment();
                        ticketassignment.setCreateddatetime(currentDateTime);
                        ticketassignment.setCreateduser(ticket.getCreateduser());
                    }

                    t.setEmployee(assignee);
                    t.setStatus(status);
                    t.setCreateduser(ticket.getCreateduser());
                    t.setCreateddatetime(new Date());
                    t.setLastupdateddatetime(new Date());
                    ticketid = ticketDAO.createTicket(t);

                    ticketassignment.setTicketcategory(ticketcategory);
                    ticketassignment.setUserrole(userrole);
                    ticketassignment.setEmployee(assignee);
                    ticketassignment.setLastupdateddatetime(currentDateTime);
                    ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                    tickethistory.setTicket(t);
                    tickethistory.setEmployee(assignee);
                    tickethistory.setStatus(status);
                    tickethistory.setLastupdateddatetime(currentDateTime);
                    tickethistory.setCreateddatetime(currentDateTime);
                    tickethistory.setCreateduser(ticket.getCreateduser());
                    ticketHistoryDAO.createTicketHistory(tickethistory);
                }
            }
        }
        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);
        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_BACKUP_REMOVAL + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional
    public List<Integer> createBulkTerminalBreakdownTicket(Ticket ticket) throws SQLException, FileNotFoundException, IOException, ParseException, Exception {

        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";

        /**
         * Audit Trace END*
         */
        int rowCount = 0;
        int insertcount = 0;
        int corruptcount = 0;
        int ticketid = 0;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        FileInputStream fileinput = new FileInputStream(ticket.getFilepath());
        XSSFWorkbook workbook = new XSSFWorkbook(fileinput);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        List<Integer> ticketIdList = new ArrayList<Integer>();

        System.out.println("******** BULK TERMINAL BREAKDOWN STARTED UPLOADING ********");

        while (rowIterator.hasNext()) {
            boolean isValid = true;

            Row row = rowIterator.next();
            int rowNum = row.getRowNum();

            if (rowNum > 0) {

                if (containsValue(row)) {

                    rowCount++;
                    Iterator<Cell> cellIterator = row.cellIterator();
                    DataFormatter formatter = new DataFormatter();
                    com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
                    Tickethistory tickethistory = new Tickethistory();
                    Calendar ticketDate = Calendar.getInstance();
                    ticketDate.setTime(ticket.getTicketdate());
                    Calendar now = Calendar.getInstance();
                    now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
                    t.setTicketdate(now.getTime());
                    t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
                    tickethistory.setAffecteddate(now.getTime());

                    while (cellIterator.hasNext()) {

                        Cell cell = cellIterator.next();

                        switch (cell.getCellType()) {
                            case Cell.CELL_TYPE_STRING:
                                if (cell.getColumnIndex() == 0) {
                                    t.setTerminalserialno(cell.getStringCellValue().trim());
                                }
                                if (cell.getColumnIndex() == 1) {
                                    t.setModel(cell.getStringCellValue().trim());
                                }
                                if (cell.getColumnIndex() == 2) {
                                    Natureoffault natureofFault = new Natureoffault();
                                    natureofFault.setNatureoffaultid(Integer.parseInt(cell.getStringCellValue().trim()));
                                    t.setNatureoffault(natureofFault);
                                }
                                if (cell.getColumnIndex() == 3) {
                                    Bank bank = new Bank();
                                    bank.setBankid(Integer.parseInt(cell.getStringCellValue().trim()));
                                    t.setBank(bank);
                                }
                                break;

                            case Cell.CELL_TYPE_NUMERIC:
                                if (cell.getColumnIndex() == 1) {
                                    Terminalmodel terminalmodel = new Terminalmodel();
                                    terminalmodel.setTerminalmodelid(Integer.parseInt(formatter.formatCellValue(cell)));
                                    t.setTerminalmodel(terminalmodel);
                                }
                                if (cell.getColumnIndex() == 2) {
                                    Natureoffault natureofFault = new Natureoffault();
                                    natureofFault.setNatureoffaultid(Integer.parseInt(formatter.formatCellValue(cell)));
                                    t.setNatureoffault(natureofFault);
                                }
                                if (cell.getColumnIndex() == 3) {
                                    Bank bank = new Bank();
                                    bank.setBankid(Integer.parseInt(formatter.formatCellValue(cell)));
                                    t.setBank(bank);
                                }
                                break;
                        }
                    }
                    if (ticket.getTicketpriority() != null) {
                        Ticketpriority ticketPriority = new Ticketpriority();
                        ticketPriority.setTicketpriorityid(ticket.getTicketpriority());
                        t.setTicketpriority(ticketPriority);
                    }
                    if (ticket.getProduct() != null) {
                        Product product = new Product();
                        product.setProductid(ticket.getProduct());
                        t.setProduct(product);
                    }
                    if (ticket.getProductcategory() != null) {
                        Productcategory productcategory = new Productcategory();
                        productcategory.setProductcategoryid(ticket.getProductcategory());
                    }
                    Ticketcategory ticketcategory = new Ticketcategory();
                    if (ticket.getTicketcategory() != null) {
                        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN);
                        t.setTicketcategory(ticketcategory);
                    }

                    Status status = statusDAO.getStatsuById(ticket.getStatus());
                    Userrole userrole = null;
                    if (ticket.getManualassigneestatus().equals("true")) {
                        if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                            userrole = new Userrole();
                            userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                        } else if (status.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                            userrole = new Userrole();
                            userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                        }
                        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                        Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));

                        if (ticketassignment == null) {
                            ticketassignment = new Ticketassignment();
                            ticketassignment.setCreateddatetime(currentDateTime);
                            ticketassignment.setCreateduser(ticket.getCreateduser());
                        }

                        t.setEmployee(employee);
                        t.setStatus(status);
                        t.setCreateduser(ticket.getCreateduser());
                        t.setCreateddatetime(new Date());
                        t.setLastupdateddatetime(new Date());
                        ticketid = ticketDAO.createTicket(t);

                        ticketassignment.setTicketcategory(ticketcategory);
                        ticketassignment.setUserrole(userrole);
                        ticketassignment.setEmployee(employee);
                        ticketassignment.setLastupdateddatetime(currentDateTime);
                        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                        Remindernotification remindernotification = new Remindernotification();
                        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
                        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
                        remindernotification.setReminderuser(systemuser.getUsername());
                        remindernotification.setRemindertime(currentDateTime);
                        remindernotification.setSourcetableid(ticketid);
                        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                        remindernotification.setCreateddatetime(currentDateTime);
                        remindernotification.setLastupdateddatetime(currentDateTime);
                        remindernotification.setCreateduser("SYSTEM");
                        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                        ticketDAO.createReminderNotification(remindernotification);

                        tickethistory.setTicket(t);
                        tickethistory.setStatus(status);
                        tickethistory.setEmployee(employee);
                        tickethistory.setLastupdateddatetime(currentDateTime);
                        tickethistory.setCreateddatetime(currentDateTime);
                        tickethistory.setCreateduser(ticket.getCreateduser());
                        ticketHistoryDAO.createTicketHistory(tickethistory);
                    } else {
                        if (!systemuserDAO.getSytemuserByUsername(ticket.getCreateduser()).getUserrole().getUserrolecode().equals(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC)) {
                            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                                userrole = new Userrole();
                                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                                if (ticketassignment == null) {
                                    ticketassignment = new Ticketassignment();
                                    ticketassignment.setCreateddatetime(currentDateTime);
                                    ticketassignment.setCreateduser(ticket.getCreateduser());
                                }

                                t.setEmployee(assignee);
                                t.setStatus(status);
                                t.setCreateduser(ticket.getCreateduser());
                                t.setCreateddatetime(new Date());
                                t.setLastupdateddatetime(new Date());
                                ticketid = ticketDAO.createTicket(t);

                                ticketassignment.setTicketcategory(ticketcategory);
                                ticketassignment.setUserrole(userrole);
                                ticketassignment.setEmployee(assignee);
                                ticketassignment.setLastupdateddatetime(currentDateTime);
                                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                                Remindernotification remindernotification = new Remindernotification();
                                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
                                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                                remindernotification.setReminderuser(systemuser.getUsername());
                                remindernotification.setRemindertime(currentDateTime);
                                remindernotification.setSourcetableid(ticketid);
                                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                                remindernotification.setCreateddatetime(currentDateTime);
                                remindernotification.setLastupdateddatetime(currentDateTime);
                                remindernotification.setCreateduser("SYSTEM");
                                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                                ticketDAO.createReminderNotification(remindernotification);

                                tickethistory.setTicket(t);
                                tickethistory.setEmployee(assignee);
                                tickethistory.setStatus(status);
                                tickethistory.setLastupdateddatetime(currentDateTime);
                                tickethistory.setCreateddatetime(currentDateTime);
                                tickethistory.setCreateduser(ticket.getCreateduser());
                                ticketHistoryDAO.createTicketHistory(tickethistory);
                            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                                userrole = new Userrole();
                                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
                                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
                                if (ticketassignment == null) {
                                    ticketassignment = new Ticketassignment();
                                    ticketassignment.setCreateddatetime(currentDateTime);
                                    ticketassignment.setCreateduser(ticket.getCreateduser());
                                }

                                t.setEmployee(assignee);
                                t.setStatus(status);
                                t.setCreateduser(ticket.getCreateduser());
                                t.setCreateddatetime(new Date());
                                t.setLastupdateddatetime(new Date());
                                ticketid = ticketDAO.createTicket(t);

                                ticketassignment.setTicketcategory(ticketcategory);
                                ticketassignment.setUserrole(userrole);
                                ticketassignment.setEmployee(assignee);
                                ticketassignment.setLastupdateddatetime(currentDateTime);
                                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                                Remindernotification remindernotification = new Remindernotification();
                                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
                                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                                remindernotification.setReminderuser(systemuser.getUsername());
                                remindernotification.setRemindertime(currentDateTime);
                                remindernotification.setSourcetableid(ticketid);
                                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                                remindernotification.setCreateddatetime(currentDateTime);
                                remindernotification.setLastupdateddatetime(currentDateTime);
                                remindernotification.setCreateduser("SYSTEM");
                                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                                ticketDAO.createReminderNotification(remindernotification);

                                tickethistory.setTicket(t);
                                tickethistory.setEmployee(assignee);
                                tickethistory.setStatus(status);
                                tickethistory.setLastupdateddatetime(currentDateTime);
                                tickethistory.setCreateddatetime(currentDateTime);
                                tickethistory.setCreateduser(ticket.getCreateduser());
                                ticketHistoryDAO.createTicketHistory(tickethistory);
                            }
                        } else {
                            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                                userrole = new Userrole();
                                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_SCSC);
                                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC);
                                if (ticketassignment == null) {
                                    ticketassignment = new Ticketassignment();
                                    ticketassignment.setCreateddatetime(currentDateTime);
                                    ticketassignment.setCreateduser(ticket.getCreateduser());
                                }

                                t.setEmployee(assignee);
                                t.setStatus(status);
                                t.setCreateduser(ticket.getCreateduser());
                                t.setCreateddatetime(new Date());
                                t.setLastupdateddatetime(new Date());
                                ticketid = ticketDAO.createTicket(t);

                                ticketassignment.setTicketcategory(ticketcategory);
                                ticketassignment.setUserrole(userrole);
                                ticketassignment.setEmployee(assignee);
                                ticketassignment.setLastupdateddatetime(currentDateTime);
                                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                                Remindernotification remindernotification = new Remindernotification();
                                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
                                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                                remindernotification.setReminderuser(systemuser.getUsername());
                                remindernotification.setRemindertime(currentDateTime);
                                remindernotification.setSourcetableid(ticketid);
                                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                                remindernotification.setCreateddatetime(currentDateTime);
                                remindernotification.setLastupdateddatetime(currentDateTime);
                                remindernotification.setCreateduser("SYSTEM");
                                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                                ticketDAO.createReminderNotification(remindernotification);

                                tickethistory.setTicket(t);
                                tickethistory.setEmployee(assignee);
                                tickethistory.setStatus(status);
                                tickethistory.setLastupdateddatetime(currentDateTime);
                                tickethistory.setCreateddatetime(currentDateTime);
                                tickethistory.setCreateduser(ticket.getCreateduser());
                                ticketHistoryDAO.createTicketHistory(tickethistory);
                            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                                userrole = new Userrole();
                                userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_TO);
                                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
                                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO);
                                if (ticketassignment == null) {
                                    ticketassignment = new Ticketassignment();
                                    ticketassignment.setCreateddatetime(currentDateTime);
                                    ticketassignment.setCreateduser(ticket.getCreateduser());
                                }

                                t.setEmployee(assignee);
                                t.setStatus(status);
                                t.setCreateduser(ticket.getCreateduser());
                                t.setCreateddatetime(new Date());
                                t.setLastupdateddatetime(new Date());
                                ticketid = ticketDAO.createTicket(t);

                                ticketassignment.setTicketcategory(ticketcategory);
                                ticketassignment.setUserrole(userrole);
                                ticketassignment.setEmployee(assignee);
                                ticketassignment.setLastupdateddatetime(currentDateTime);
                                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                                Remindernotification remindernotification = new Remindernotification();
                                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
                                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                                remindernotification.setReminderuser(systemuser.getUsername());
                                remindernotification.setRemindertime(currentDateTime);
                                remindernotification.setSourcetableid(ticketid);
                                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                                remindernotification.setCreateddatetime(currentDateTime);
                                remindernotification.setLastupdateddatetime(currentDateTime);
                                remindernotification.setCreateduser("SYSTEM");
                                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                                ticketDAO.createReminderNotification(remindernotification);

                                tickethistory.setTicket(t);
                                tickethistory.setEmployee(assignee);
                                tickethistory.setStatus(status);
                                tickethistory.setLastupdateddatetime(currentDateTime);
                                tickethistory.setCreateddatetime(currentDateTime);
                                tickethistory.setCreateduser(ticket.getCreateduser());
                                ticketHistoryDAO.createTicketHistory(tickethistory);
                            }
                        }
                    }

                    if (t.getTicketid() != null) {
                        ticketIdList.add(t.getTicketid());
                    }
                }
            }
        }
        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_BULK_TERMINAL_BRREAKDOWN + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        System.out.println("************* UPLOAD COMPLETED *************");
        //return insertcount;
        return ticketIdList;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createTerminalSharingTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        /**
         * Audit Trace END*
         */
        int ticketid;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        t.setMid(ticket.getMid());
        t.setTid(ticket.getTid());
        t.setVmtid(ticket.getVmtid());
        t.setVmmid(ticket.getVmmid());
        t.setAmextid(ticket.getAmextid());
        t.setAmexmid(ticket.getAmexmid());
        t.setRemarks(ticket.getSpecialremark());

        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        t.setCurrency(ticket.getCurrency());
        t.setDescription(ticket.getDescription());
        t.setLotno(ticket.getLotno());
        Bank bank = new Bank();
        if (ticket.getBank() != null) {
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        t.setMerchantname(ticket.getMerchantname());
        t.setLocationaddress(ticket.getLocationaddress());
        t.setContactpersion(ticket.getContactperson());
        t.setContactno(ticket.getContactno());
        District district = new District();
        if (ticket.getDistrict() != null) {
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }

        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        //Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO);
        Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);

        Userrole userrole = new Userrole();
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_VO);
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        //Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_SHARING + " is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createTerminalConversionTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        /**
         * Audit Trace END*
         */
        int ticketid;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        t.setMid(ticket.getMid());
        t.setTid(ticket.getTid());
        t.setTerminalserialno(ticket.getTerminalserialno());
        t.setNewsoftwareversion(ticket.getNewsoftwareversion());
        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        t.setLotno(ticket.getLotno());
        t.setNewsoftwareversion(ticket.getNewsoftwareversion());
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        //Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO);
        Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);

        Userrole userrole = new Userrole();
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_VO);
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        //Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_TERMINAL_CONVERSION + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createReinitializationTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        /**
         * Audit Trace END*
         */

        int ticketid;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        t.setMid(ticket.getMid());
        t.setTid(ticket.getTid());
        Terminalmodel terminalmodel = new Terminalmodel();
        if (ticket.getTerminalmodel() != null) {
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        Terminalbrand terminalbrand = new Terminalbrand();
        if (ticket.getTerminalbrnad() != null) {
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        t.setLotno(ticket.getLotno());
        t.setTerminalserialno(ticket.getTerminalserialno());
        Bank bank = new Bank();
        if (ticket.getBank() != null) {
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcat = new Clientcategory();
            clientcat.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcat);
        }
        t.setMerchantname(ticket.getMerchantname());
        t.setContactpersion(ticket.getContactperson());
        t.setContactno(ticket.getContactno());
        District district = new District();
        if (ticket.getDistrict() != null) {
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        t.setLocationaddress(ticket.getLocationaddress());
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        //Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO);
        Status status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTASSEPICLK);
        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);

        Userrole userrole = new Userrole();
        //userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_VO);
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EPIC_SRILANKA);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        //Employee assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_REINTIALIZATION + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createTerminalalBrakdownMalayasiaTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        /**
         * Audit Trace END*
         */
        int ticketid;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        if (ticket.getDeploymentdate() != null) {
            Calendar deploymentDate = Calendar.getInstance();
            deploymentDate.setTime(ticket.getDeploymentdate());
            Calendar nowDepDate = Calendar.getInstance();
            nowDepDate.set(deploymentDate.get(Calendar.YEAR), deploymentDate.get(Calendar.MONTH), deploymentDate.get(Calendar.DATE));
            t.setDeploymentdate(nowDepDate.getTime());
        }

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        if (ticket.getTerritory() != null) {
            Territorymap territorymap = new Territorymap();
            territorymap.setTerritorymapid(ticket.getTerritory());
            t.setTerritorymap(territorymap);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        t.setTid(ticket.getTid());
        t.setMid(ticket.getMid());
        t.setTerminalserialno(ticket.getTerminalserialno());

        Inventoryterminalitem inventoryterminalitem = inventoryDAO.getInventoryitemBySerialNumber(ticket.getTerminalserialno());
        t.setInventoryterminalitem(inventoryterminalitem);

        if (ticket.getTerminalbrnad() != null) {
            Terminalbrand terminalbrand = new Terminalbrand();
            terminalbrand.setTerminalbrandid(ticket.getTerminalbrnad());
            t.setTerminalbrand(terminalbrand);
        }
        if (ticket.getTerminalmodel() != null) {
            Terminalmodel terminalmodel = new Terminalmodel();
            terminalmodel.setTerminalmodelid(ticket.getTerminalmodel());
            t.setTerminalmodel(terminalmodel);
        }
        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        t.setLotno(ticket.getLotno());
        t.setPartno(ticket.getPartno());
        t.setRevision(ticket.getRevision());
        t.setMac(ticket.getMac());
        t.setPtid(ticket.getPtid());
        if (ticket.getBank() != null) {
            Bank bank = new Bank();
            bank.setBankid(ticket.getBank());
            t.setBank(bank);
        }
        if (ticket.getClient() != null) {
            Client client = new Client();
            client.setClientid(ticket.getClient());
            t.setClient(client);
        }
        if (ticket.getClientcategory() != null) {
            Clientcategory clientcategory = new Clientcategory();
            clientcategory.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcategory);
        }
        t.setMerchantname(ticket.getMerchantname());
        t.setContactpersion(ticket.getContactperson());
        t.setContactno(ticket.getContactno());
        if (ticket.getDistrict() != null) {
            District district = new District();
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        t.setLocationaddress(ticket.getLocationaddress());
        if (ticket.getDeliveryrequirement() != null) {
            Deleveryrequirement deleveryrequirement = new Deleveryrequirement();
            deleveryrequirement.setDeleveryrequierementid(ticket.getDeliveryrequirement());
            t.setDeleveryrequirement(deleveryrequirement);
        }
        t.setDeleverydestination(ticket.getDeliverydestination());
        if (ticket.getNatureoffault() != null) {
            Natureoffault natureoffault = new Natureoffault();
            natureoffault.setNatureoffaultid(ticket.getNatureoffault());
            t.setNatureoffault(natureoffault);
        }
        if (ticket.getActiontobetaken() != null) {
            Actiontobetaken actiontobetaken = new Actiontobetaken();
            actiontobetaken.setActiontobetakenid(ticket.getActiontobetaken());
            t.setActiontobetaken(actiontobetaken);
        }
        if (ticket.getCauseoffault() != null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            t.setCauseoffault(causeoffault);
            tickethistory.setCauseoffault(causeoffault);
        }
        if (ticket.getActiontaken() != null) {
            Actiontaken actiontaken = new Actiontaken();
            actiontaken.setActiontakenid(ticket.getActiontaken());
            t.setActiontaken(actiontaken);
            tickethistory.setActiontaken(actiontaken);
        }
        t.setTechnicalofficerincharge(ticket.getTechnicalofficerincharge());
        if (ticket.getReportedby() != null) {
            Reportedby reportedby = new Reportedby();
            reportedby.setReportedid(ticket.getReportedby());
            t.setReportedby(reportedby);
        }
        t.setReportedmerchant(ticket.getReportedmerchant());
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Userrole userrole = new Userrole();
        Status status = new Status();
        if (ticket.getAssigneetype() == MasterDataVarList.AFFINITI_CODE_USERROLE_IFS) {
            userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_IFS);
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOIFS);
        } else if (ticket.getAssigneetype() == MasterDataVarList.AFFINITI_CODE_USERROLE_EM) {
            userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EM);
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOEM);
        } else {
            userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_IFS);
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOIFS);
        }
        status.setStatusid(status.getStatusid());
        t.setStatus(status);
        tickethistory.setStatus(status);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_MALAY_TERMINAL_BREAKDOWN + " is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int createTerminalRepairMalayasiaTicket(Ticket ticket) throws HibernateException, SQLException, Exception {
        /**
         * Audit Trace*
         */
        String page = "Ticket Create Page";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        /**
         * Audit Trace END*
         */
        int ticketid;
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket t = new com.avn.affiniti.hibernate.model.Ticket();
        Tickethistory tickethistory = new Tickethistory();
        Calendar ticketDate = Calendar.getInstance();
        ticketDate.setTime(ticket.getTicketdate());
        Calendar now = Calendar.getInstance();
        now.set(ticketDate.get(Calendar.YEAR), ticketDate.get(Calendar.MONTH), ticketDate.get(Calendar.DATE));
        t.setTicketdate(now.getTime());
        t.setNotificationlevel(MasterDataVarList.AFFINITI_CODE_TICKET_NOTIFICATION_LEVEL_DEFAULT_VALUE);
        tickethistory.setAffecteddate(now.getTime());

        Calendar completionDate = Calendar.getInstance();
        completionDate.setTime(ticket.getCompletiondatetime());
        Calendar nowCompletionDate = Calendar.getInstance();
        now.set(completionDate.get(Calendar.YEAR), completionDate.get(Calendar.MONTH), completionDate.get(Calendar.DATE));
        t.setCompletiondatetime(nowCompletionDate.getTime());

        Calendar dispatchDate = Calendar.getInstance();
        dispatchDate.setTime(ticket.getDispatchdate());
        Calendar nowDispatchDate = Calendar.getInstance();
        now.set(dispatchDate.get(Calendar.YEAR), dispatchDate.get(Calendar.MONTH), dispatchDate.get(Calendar.DATE));
        t.setDispatchdate(nowDispatchDate.getTime());

        Calendar invoiceDate = Calendar.getInstance();
        invoiceDate.setTime(ticket.getDispatchdate());
        Calendar nowInvoiceDate = Calendar.getInstance();
        now.set(invoiceDate.get(Calendar.YEAR), invoiceDate.get(Calendar.MONTH), invoiceDate.get(Calendar.DATE));
        t.setInvoicedate(nowInvoiceDate.getTime());

        if (ticket.getTicketpriority() != null) {
            Ticketpriority ticketpriority = new Ticketpriority();
            ticketpriority.setTicketpriorityid(ticket.getTicketpriority());
            t.setTicketpriority(ticketpriority);
        }
        if (ticket.getProduct() != null) {
            Product product = new Product();
            product.setProductid(ticket.getProduct());
            t.setProduct(product);
        }
        if (ticket.getProductcategory() != null) {
            Productcategory productcategory = new Productcategory();
            productcategory.setProductcategoryid(ticket.getProductcategory());
            t.setProductcategory(productcategory);
        }
        Ticketcategory ticketcategory = new Ticketcategory();
        if (ticket.getTicketcategory() != null) {
            ticketcategory.setTicketcategoryid(ticket.getTicketcategory());
            t.setTicketcategory(ticketcategory);
        }
        if (ticket.getDistrict() != null) {
            District district = new District();
            district.setDistrictid(ticket.getDistrict());
            t.setDistrict(district);
        }
        if (ticket.getDeliveryrequirement() != null) {
            Deleveryrequirement deleveryrequirement = new Deleveryrequirement();
            deleveryrequirement.setDeleveryrequierementid(ticket.getDeliveryrequirement());
            t.setDeleveryrequirement(deleveryrequirement);
        }
        t.setDeleverydestination(ticket.getDeliverydestination());
        if (ticket.getCauseoffault() != null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            t.setCauseoffault(causeoffault);
            tickethistory.setCauseoffault(causeoffault);
        }
        if (ticket.getActiontaken() != null) {
            Actiontaken actiontaken = new Actiontaken();
            actiontaken.setActiontakenid(ticket.getActiontaken());
            t.setActiontaken(actiontaken);
        }

        t.setDamagepartdescription(ticket.getDamagepartdescription());
        t.setReplacedpart(ticket.getReplacedpart());
        t.setRevision(ticket.getRevision());
        t.setMac(ticket.getMac());
        t.setPtid(ticket.getPtid());
        t.setTotletimespent(ticket.getTotletimespent());
        t.setSpecialremark(ticket.getSpecialremark());
        t.setCourierserviceprovider(ticket.getCourierserviceprovider());
        t.setTrackingnumber(ticket.getTrackingnumber());
        t.setContactpersion(ticket.getContactperson());
        t.setContactno(ticket.getContactno());
        t.setLocationaddress(ticket.getLocationaddress());
        t.setDeleverydestination(ticket.getDeliverydestination());
        t.setDeliveryorderref(ticket.getDeliveryorderref());
        t.setRmareferance(ticket.getRmareferance());
        t.setDestination(ticket.getDestination());
        t.setInvoicenumber(ticket.getInvoicenumber());
        t.setInvoicevalue(ticket.getInvoicevalue());
        t.setBkplocationaddress(ticket.getBkplocationaddress());
        t.setParttype(ticket.getParttype());
        t.setTerminalserialno(ticket.getTerminalserialno());
        t.setDescription(ticket.getDescription());

        Inventoryterminalitem inventoryterminalitem = inventoryDAO.getInventoryitemBySerialNumber(ticket.getTerminalserialno());
        t.setInventoryterminalitem(inventoryterminalitem);

        if (ticket.getClientcategory() != null) {
            Clientcategory clientcategory = new Clientcategory();  
            clientcategory.setClientcategoryid(ticket.getClientcategory());
            t.setClientcategory(clientcategory);
        }

        if (ticket.getPurposetype() != null) {
        Purposetype purposetype = new Purposetype();
        purposetype.setPurposetypeid(ticket.getPurposetype());
        t.setPurposetype(purposetype);
        }
        t.setLastupdateddatetime(currentDateTime);
        t.setCreateddatetime(currentDateTime);
        t.setCreateduser(ticket.getCreateduser());

        Status status = new Status();
        Userrole userrole = new Userrole();
        userrole.setUserroleid(MasterDataVarList.AFFINITI_CODE_USERROLE_EM);
        status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOEM);
        t.setStatus(status);
        tickethistory.setStatus(status);
        Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(ticket.getTicketcategory(), userrole.getUserroleid());
        Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
        if (ticketassignment == null) {
            ticketassignment = new Ticketassignment();
            ticketassignment.setCreateddatetime(currentDateTime);
            ticketassignment.setCreateduser(ticket.getCreateduser());
        }
        t.setEmployee(assignee);
        tickethistory.setEmployee(assignee);
        ticketid = ticketDAO.createTicket(t);

        ticketassignment.setTicketcategory(ticketcategory);
        ticketassignment.setUserrole(userrole);
        ticketassignment.setEmployee(assignee);
        ticketassignment.setLastupdateddatetime(currentDateTime);
        ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

        Remindernotification remindernotification = new Remindernotification();
        remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticketid));
        Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
        remindernotification.setReminderuser(systemuser.getUsername());
        remindernotification.setRemindertime(currentDateTime);
        remindernotification.setSourcetableid(ticketid);
        remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
        remindernotification.setCreateddatetime(currentDateTime);
        remindernotification.setLastupdateddatetime(currentDateTime);
        remindernotification.setCreateduser("SYSTEM");
        remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
        remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
        ticketDAO.createReminderNotification(remindernotification);

        tickethistory.setTicket(t);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        /**
         * ***********--Insert AuditTrace--*************
         */
        description = MasterDataVarList.AFFINITI_TICKET_CATEGORY_CREATE_MALAY_TERMINAL_REPAIR + "  is Created. ID " + ticketid;
        task = "Create";
        affectedId = String.valueOf(ticketid);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        /**
         * ***********--End of Insert AuditTrace--*************
         */

        return ticketid;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public com.avn.affiniti.hibernate.model.Ticket getTicket(int ticketid) throws HibernateException {
        return ticketDAO.getTicket(ticketid);
    }

    @Override
    public TerminalBreakdownTicket getTerminalBrakdownTicketById(int ticketid) throws SQLException {
        return ticketDAO.getTerminalBreakdownTicket(ticketid);
    }

    @Override
    public NewInstallationTicke getNewInstallationTicketById(int ticketid) throws SQLException {
        return ticketDAO.getNewInstallationTicket(ticketid);
    }

    @Override
    public SoftwareHardwareBreakdownTicket getSoftwareHardwareTicketById(int ticketid) throws SQLException {
        return ticketDAO.getSoftwareHardwareTicketById(ticketid);
    }

    @Override
    public MerchantRemovalTicket getMerchantRemovalTicketById(int ticketid) throws SQLException {
        return ticketDAO.getMerchantRemovalTicketById(ticketid);
    }

    @Override
    public TerminalSharingTicket getTerminalSharingTicketById(int ticketid) throws SQLException {
        return ticketDAO.getTerminalSharingTicketById(ticketid);
    }

    @Override
    public TerminalConversionTicket getTerminalConversionTicketById(int ticketid) throws SQLException {
        return ticketDAO.getTerminalConversionTicketById(ticketid);
    }

    @Override
    public ReinitializationTicket getReinitializationTicketById(int ticketid) throws SQLException {
        return ticketDAO.getReinitializationTicketById(ticketid);
    }

    @Override
    public MaintainancePaymentInvoiceTicket getMaintainancePaymentInvoiceTicketById(int ticketid) throws SQLException {
        return ticketDAO.getMaintainancePaymentInvoiceTicketById(ticketid);
    }

    @Override
    public TerminalBreakdownTicket getMalaysianTerminalBreakdownTicketById(int ticketid) throws SQLException {
        return ticketDAO.getMalaysianTerminalBreakdownTicketById(ticketid);
    }

    @Override
    public TerminalRepairMysTicket getMalaysianTerminalRepairTicketById(int ticketid) throws SQLException {
        return ticketDAO.getMalaysianTerminalRepairTicketById(ticketid);
    }

    @Override
    public BaseSoftwareInstallationTicket getBaseSoftwareInstallationTicketById(int ticketid) throws SQLException {
        return ticketDAO.getBaseSoftwareInstallationTicketById(ticketid);
    }

    @Override
    public TerminalUpgradeDowngradeTicket getTerminalUpgradeDowngradeTicketById(int ticketid) throws SQLException {
        return ticketDAO.getTerminalUpgradeDowngradeTicketById(ticketid);
    }

    @Override
    public BackupRemovalTicket getBackupRemovalTicketById(int ticketid) throws SQLException {
        return ticketDAO.getBackupRemovalTicketById(ticketid);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getNewInstallationTicketUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("deployableStatusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_DEPLOYABLESTATUS,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("terminalDeliverdStatusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TERMINALDELIVERYSTATUS,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );
        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getSoftwareHardwareBreakdownUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );
        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getMerchantRemovalUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getBaseSoftwareInstallationUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getTerminalUpgradeDowngradeUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getBackupRemovalUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getTerminalSharingUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getTerminalConversionUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getReinitializationUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {

        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );

        return object;
    }

    @Override
    public JSONObject getMaintainacePaymentInvoiceUpdatePageData(String username, String status, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();

        if (status.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_YES)) {
            List<String> agreementstatuslist = new LinkedList<>();
            agreementstatuslist.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_DRAFT);
            agreementstatuslist.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_SENTSIGNATURE);
            agreementstatuslist.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_CONFIRMED);
            String[] agreementstatusArr = agreementstatuslist.toArray(new String[0]);
            object.put("amcStatusList",
                    commonDAO.getJSONArrayDropdownValueList(
                            String.format(
                                    DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_STATUSCODE,
                                    Arrays.toString(agreementstatusArr).replaceAll("[\\[\\]\"]", "'").replace(", ", "','")
                            )
                    )
            );

            List<String> statuslist = new LinkedList<>();
            statuslist.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_CLOSE);
            String[] statusArr = statuslist.toArray(new String[0]);
            object.put("statusList",
                    commonDAO.getJSONArrayDropdownValueList(
                            String.format(
                                    DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_STATUSCODE,
                                    Arrays.toString(statusArr).replaceAll("[\\[\\]\"]", "'").replace(", ", "','")
                            )
                    )
            );
        } else {
            List<String> statuslist = new LinkedList<>();
            statuslist.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_POSRESPONSE);
            statuslist.add(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NEGRESPONSE);
            String[] statusArr = statuslist.toArray(new String[0]);
            object.put("statusList",
                    commonDAO.getJSONArrayDropdownValueList(
                            String.format(
                                    DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_STATUSCODE,
                                    Arrays.toString(statusArr).replaceAll("[\\[\\]\"]", "'").replace(", ", "','")
                            )
                    )
            );
        }

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getMalaysiaTerminalBrakdownUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getMalaysiaTerminalRepairUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getTerminalBrakdownUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );
        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );

//        if (userRoleDAO.getUserRoleByUsername(username).getUserrolecode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EPIC_SRILANKA)) {
//            List<String> userrolelist = new LinkedList<String>();
//            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
//            userrolelist.add(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
//            String[] statusArr = userrolelist.toArray(new String[0]);
//            object.put("assigneeList",
//                    commonDAO.getJSONArrayDropdownValueList(
//                            String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNEELIST,
//                                    Arrays.toString(statusArr).replaceAll("[\\[\\]\"]", "'").replace(", ", "','"),
//                                    MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
//                            )
//                    )
//            );
//        } else {
        object.put("assigneeList",
                Common.removeDropDownElementFromJsonArray("", commonDAO.getJSONArrayDropdownValueList(
                                String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_DEFAULT_TKT_ASSIGNEE,
                                        ticketid
                                )
                        ))
        );
//        }

        return object;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public int updateTerminalBrakdownTicket(Ticket ticket) throws HibernateException, SQLException {

        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_BREAKDOWN;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";

        int newTicketId = ticket.getTicketid();
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);
        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)
                    && userRoleDAO.getUserRoleByUsername(ticket.getCreateduser()).getUserrolecode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO)) {
                Employee employee = employeeDAO.getEmployeeByUsername(ticket.getCreateduser());
                t.setEmployee(employee);
                tickethistory.setEmployee(employee);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCCA)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CCA);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSREQCSE)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }

        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_BKPTERREJ)) {
            tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticket.getTicketid(),
                    (statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSBKPTMC)).getStatusid());
            Employee assignee = tickethistory.getEmployee();
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_DEPBKPTER)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
        }
        if (ticket.getCauseoffault() != null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            tickethistory.setCauseoffault(causeoffault);
        }
        if (ticket.getActiontaken() != null) {
            Actiontaken actiontaken = new Actiontaken();
            actiontaken.setActiontakenid(ticket.getActiontaken());
            tickethistory.setActiontaken(actiontaken);
        }
        if (ticket.getInventoryhardwareitem() != null) {
            Inventoryhardwareitem inventoryhardwareitem = new Inventoryhardwareitem();
            inventoryhardwareitem.setInventoryhardwareitemid(ticket.getInventoryhardwareitem());
            t.setInventoryhardwareitem(inventoryhardwareitem);
        }
        if (ticket.getQuantity() != null && !ticket.getQuantity().equals("")) {
            t.setHardwareitemqty(Integer.parseInt(ticket.getQuantity()));
        }
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_CLOANDREPTKT)) {
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCSE);
            com.avn.affiniti.hibernate.model.Ticket subTicket = new com.avn.affiniti.hibernate.model.Ticket();
            tickethistory = new Tickethistory();
            subTicket.setTicket(t);
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            tickethistory.setEmployee(t.getEmployee());

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }

            subTicket.setPreviousticketid(ticket.getTicketid()); //set previous ticket id
            subTicket.setTicketdate(currentDateTime);
            ticketcategory = new Ticketcategory();
            ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR);
            subTicket.setTicketcategory(ticketcategory);
            subTicket.setStatus(status);
            subTicket.setEmployee(assignee);
            subTicket.setLastupdateddatetime(currentDateTime);
            subTicket.setCreateddatetime(currentDateTime);
            subTicket.setCreateduser(ticket.getCreateduser());
            subTicket.setTicketstage(0);
            int subTicketId = ticketDAO.createTicket(subTicket);
            newTicketId = subTicketId;
            subTicket = ticketDAO.getTicket(subTicketId);

            String ticketHistoryDescription = " This terminal repair ticket is related with, Ticket ID :" + ticket.getTicketid();
            tickethistory.setResolutiondescription(ticketHistoryDescription);
            tickethistory.setEmployee(subTicket.getEmployee());
            tickethistory.setTicket(subTicket);
            tickethistory.setStatus(status);
            tickethistory.setLastupdateddatetime(currentDateTime);
            tickethistory.setCreateddatetime(currentDateTime);
            tickethistory.setCreateduser(ticket.getCreateduser());
            ticketHistoryDAO.createTicketHistory(tickethistory);

            Tickethistory parentTicketHistory = ticketDAO.getTicketHistoryMaxDataByTicketId(ticket.getTicketid());
            parentTicketHistory.setEmployee(subTicket.getEmployee());
            ticketDAO.updateTicketHistory(parentTicketHistory);

            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), subTicketId));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

            Status oldstatus = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_CLOSE);
            t.setStatus(oldstatus);
            t.setLastupdateddatetime(currentDateTime);
            t.setLastupdateduser(ticket.getCreateduser());
            ticketDAO.updateTicket(t);
        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTNWINSTTKT)) {
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO);
            com.avn.affiniti.hibernate.model.Ticket subTicket = new com.avn.affiniti.hibernate.model.Ticket();
            tickethistory = new Tickethistory();
            subTicket.setTicket(t);
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            tickethistory.setEmployee(t.getEmployee());

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }

            subTicket.setPreviousticketid(ticket.getTicketid()); // set previous ticket id
            subTicket.setTicketdate(currentDateTime);
            ticketcategory = new Ticketcategory();
            ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION);
            subTicket.setTicketcategory(ticketcategory);
            subTicket.setStatus(status);
            subTicket.setEmployee(assignee);

            if (t.getClientcategory() != null) {
                subTicket.setClientcategory(t.getClientcategory());
            }
            if (t.getClient() != null) {
                subTicket.setClient(t.getClient());
            }

            subTicket.setLastupdateddatetime(currentDateTime);
            subTicket.setCreateddatetime(currentDateTime);
            subTicket.setCreateduser(ticket.getCreateduser());
            int subTicketId = ticketDAO.createTicket(subTicket);
            subTicket = ticketDAO.getTicket(subTicketId);
            newTicketId = subTicketId;

            String ticketHistoryDescription = " This New Terminal Installation ticket is related with, Ticket ID :" + ticket.getTicketid();
            tickethistory.setResolutiondescription(ticketHistoryDescription);
            tickethistory.setTicket(subTicket);
            tickethistory.setStatus(status);
            tickethistory.setLastupdateddatetime(currentDateTime);
            tickethistory.setCreateddatetime(currentDateTime);
            tickethistory.setCreateduser(ticket.getCreateduser());
            ticketHistoryDAO.createTicketHistory(tickethistory);

            Tickethistory parentTicketHistory = ticketDAO.getTicketHistoryMaxDataByTicketId(ticket.getTicketid());
            parentTicketHistory.setEmployee(subTicket.getEmployee());
            ticketDAO.updateTicketHistory(parentTicketHistory);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), subTicketId));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
        }

        description = "Terminal Breakdown Ticket Updated to " + status.getDescription() + ".-ID-" + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
        return newTicketId;

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateNewInstallationTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_NEW_TERMINAL_INSTALLATION;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);
        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);
            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }

        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TERMHANDTMC)) {
            //Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);
        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREJMER)) {
            Inventoryterminalitem invtermitem = new Inventoryterminalitem();
            invtermitem.setInventoryterminalitemid(t.getInventoryterminalitem().getInventoryterminalitemid());
            Status invstatus = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_NOTISSUED);
            invtermitem.setStatus(invstatus);
            inventoryDAO.changeStatusOfTerminalByStatus(invtermitem);
        }

        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "New Installation Ticket  Updated to " + status.getDescription() + ".-ID-  " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateSoftwareHardwareBreakdownTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_SOFWARE_HARDWARE_BREAKDOWN;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());
        tickethistory.setEmployee(t.getEmployee());
        if (ticket.getCauseoffault() != null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            tickethistory.setCauseoffault(causeoffault);
        }
        if (ticket.getActiontaken() != null) {
            Actiontaken actiontaken = new Actiontaken();
            actiontaken.setActiontakenid(ticket.getActiontaken());
            tickethistory.setActiontaken(actiontaken);
        }
        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);
        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOPO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_PO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_RESOLVED)) {
                Employee employee = employeeDAO.getEmployeeByUsername(t.getCreateduser());
                t.setEmployee(employee);
                tickethistory.setEmployee(employee);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REOPEN)) {
                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOPO);
                Employee employee = employeeDAO.getEmployeeById(ticketHistoryDAO.getAssigneeByTicketId(ticket.getTicketid(), MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_PO));
                t.setEmployee(employee);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Software Hardware Breakdown Ticket Updated to" + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateMerchantRemovalTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_MERCHANT_REMOVAL;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Employee assignee;
        Status status = statusDAO.getStatsuById(ticket.getStatus());
        //t.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL, userrole.getUserroleid());
                assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL, userrole.getUserroleid());
                assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL, userrole.getUserroleid());
                assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }
        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ADDTMCINV)) {
            Inventoryterminalitem inventoryterminalitem = new Inventoryterminalitem();
            inventoryterminalitem.setTerminalbrand(t.getTerminalbrand());
            inventoryterminalitem.setTerminalmodel(t.getTerminalmodel());
            inventoryterminalitem.setSerialno(t.getTerminalserialno());
            inventoryterminalitem.setTid(t.getTid());
            inventoryterminalitem.setMid(t.getMid());
            Client client = t.getClient();
            inventoryterminalitem.setClient(client);
            Inventorycategory inventorycategory = new Inventorycategory();
            inventorycategory.setInventorycategoryid(MasterDataVarList.AFFINITI_CODE_INVENTORYCATEGORY_INVENTORYCATEGORYID);
            inventoryterminalitem.setInventorycategory(inventorycategory);
            inventoryterminalitem.setEmployee(t.getEmployee());
            inventoryterminalitem.setCreateddatetime(currentDateTime);
            inventoryterminalitem.setLastupdateddatetime(currentDateTime);
            inventoryDAO.addTerminaltoTMCInventory(inventoryterminalitem);
        }

        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Merchant Removal Ticket is Updated to " + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateBaseSoftawareInstallationTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_BASE_SOFTWARE_INSTALLATION;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Base Softaware Installation Ticket  is Updated " + status.getDescription() + " .-ID-" + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateTerminalUpgradeDowngradeTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_UPGRADE_DOWNGRADWE;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());
        t.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_RETFTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Terminal Upgrade Downgrade Ticket tickect is Updated " + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateBackupRemovalTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_BACKUP_REMOVAL;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TERMHANDTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Backup Removal Ticket  is Updated" + status.getDescription() + " .-ID-" + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateTerminalConversionTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_CONVERSION;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        tickethistory.setEmployee(t.getEmployee());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }

        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Terminal Conversion Ticket  is Updated" + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateTerminalSharingTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_SHARING;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        tickethistory.setEmployee(t.getEmployee());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }

        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Terminal Sharing Ticket  is Updated" + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateReinitializationTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_REINTIALIZATION;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOVO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_VO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }

        t.setStatus(status);
        t.setNewsoftwareversion(ticket.getNewsoftwareversion());
        t.setAmcaexpirystatus(ticket.getAmcexpirystatus());
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Re initialization Ticket tickect is Updated " + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateMaintenencePaymentInvoiceTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_MAINTANACE_PAYMENT_INVOICE;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_POSRESPONSE)) {

        }
        if (!(ticket.getAmcperiod() == null)) {
            t.setAmcperiod(ticket.getAmcperiod());
        }
        if (!(ticket.getAmcpaymentplan() == null)) {
            t.setAmcpaymentplan(ticket.getAmcpaymentplan());
        }
        if (!(ticket.getAgreementstatus() == null)) {
            t.setAgreementstatus(ticket.getAgreementstatus());
        }
        tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));

        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Maintenence Payment Invoice Ticket is Updated " + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateTerminalRepairTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_TERMINAL_REPAIR;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        t.setTicketstage(t.getTicketstage() + 1);
        Status status = statusDAO.getStatsuById(ticket.getStatus());

        if (ticket.getManualassigneestatus().equals("true")) {
            Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(ticket.getAssignee()));
            t.setEmployee(employee);
            tickethistory.setEmployee(employee);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(employee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCSE)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC)) {
                Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_SCSC);
                Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
                Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
                t.setEmployee(assignee);
                tickethistory.setEmployee(assignee);

                if (ticketassignment == null) {
                    ticketassignment = new Ticketassignment();
                    ticketassignment.setCreateddatetime(currentDateTime);
                    ticketassignment.setCreateduser(ticket.getCreateduser());
                }
                ticketassignment.setTicketcategory(ticketcategory);
                ticketassignment.setUserrole(userrole);
                ticketassignment.setEmployee(assignee);
                ticketassignment.setLastupdateddatetime(currentDateTime);
                ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

                Remindernotification remindernotification = new Remindernotification();
                remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
                Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
                remindernotification.setReminderuser(systemuser.getUsername());
                remindernotification.setRemindertime(currentDateTime);
                remindernotification.setSourcetableid(ticket.getTicketid());
                remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
                remindernotification.setCreateddatetime(currentDateTime);
                remindernotification.setLastupdateddatetime(currentDateTime);
                remindernotification.setCreateduser("SYSTEM");
                remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
                remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
                ticketDAO.createReminderNotification(remindernotification);

            } else {
                tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
            }
        }

        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQPART)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        }
//        else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TSTTREFAIL)) {
//            tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticket.getTicketid(),
//                    (statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)).getStatusid());
//            Employee assignee = tickethistory.getEmployee();
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//        }

        if (ticket.getCauseoffault() != null) {
            Causeoffault causeoffault = new Causeoffault();
            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
            t.setCauseoffault(causeoffault);
            tickethistory.setCauseoffault(causeoffault);
        }
        if (ticket.getActiontaken() != null) {
            Actiontaken actiontaken = new Actiontaken();
            actiontaken.setActiontakenid(ticket.getActiontaken());
            t.setActiontaken(actiontaken);
            tickethistory.setActiontaken(actiontaken);
        }

        if (ticket.getInventoryhardwareitem() != null) {
            Inventoryhardwareitem inventoryhardwareitem = new Inventoryhardwareitem();
            inventoryhardwareitem.setInventoryhardwareitemid(ticket.getInventoryhardwareitem());
            t.setInventoryhardwareitem(inventoryhardwareitem);
        }
        if (ticket.getQuantity() != null && !ticket.getQuantity().equals("")) {
            t.setHardwareitemqty(Integer.parseInt(ticket.getQuantity()));
        }

        if (ticket.getIsusernegligence() != null) {
            t.setIsusernegligence(ticket.getIsusernegligence());
        }
        if (ticket.getIslightening() != null) {
            t.setIslightening(ticket.getIslightening());
        }
        if (ticket.getIscoveredunderwarranty() != null) {
            t.setIscoveredunderwarranty(ticket.getIscoveredunderwarranty());
        }
        if (ticket.getIscoveredunderwarranty() != null) {
            t.setIsunderamc(ticket.getIscoveredunderwarranty());
        }
        t.setDamagepartdescription(ticket.getDamagepartdescription());
        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
        t.setReplacedpart(ticket.getReplacedpart());
        t.setParttype(ticket.getParttype());
        t.setPartnumber(ticket.getPartnumber());
        t.setPartnumberofterminal(ticket.getPartnumberofterminal());
        t.setRevision(ticket.getRevision());
        t.setMac(ticket.getMac());
        t.setPtid(ticket.getPtid());

        if (ticket.isQuotation()) {
            Terminalquotation terminalquotation = terminalQuotationDAO.getLatestTerminalQuotationByTicket(ticket.getTicketid());
            if (terminalquotation == null) {
                terminalquotation = new Terminalquotation();
                terminalquotation.setCreateddatetime(currentDateTime);
                terminalquotation.setCreateduser(ticket.getCreateduser());
            }

            if (ticket.getQcauseoffault() != null) {
                Causeoffault causeoffault = new Causeoffault();
                causeoffault.setCauseoffaultid(ticket.getQcauseoffault());
                terminalquotation.setCauseoffault(causeoffault);
            }
            terminalquotation.setTicket(t);
            terminalquotation.setRecipientbank(ticket.getRecipientbank());
            terminalquotation.setTerminalserialno(ticket.getQterminalserialno());
            terminalquotation.setTerminalmodel(ticket.getQterminalmodel());
            terminalquotation.setMerchantlocation(ticket.getMerchantlocation());
            terminalquotation.setReplacementpartno(ticket.getReplacementpartno());
            terminalquotation.setReplacementdescription(ticket.getReplacementdescription());
            terminalquotation.setUnitprice(ticket.getUnitprice());
            terminalquotation.setQuantity(ticket.getQuantity());
            terminalquotation.setTotal(ticket.getTotal());
            terminalquotation.setQuatationvalidityperiod(ticket.getQuatationvalidityperiod());
            terminalquotation.setLastupdateddatetime(currentDateTime);
            terminalQuotationDAO.createOrUpdateTerminalQuotation(terminalquotation);
        }

        t.setCompletiondatetime(ticket.getCompletiondatetime());
        t.setTotletimespent(ticket.getTotletimespent());
        t.setSpecialremark(ticket.getSpecialremark());
        t.setDispatchdate(ticket.getDispatchdate());
        t.setCourierserviceprovider(ticket.getCourierserviceprovider());
        t.setTrackingnumber(ticket.getTrackingnumber());
        t.setDeleverydestination(ticket.getDeliverydestination());
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Terminal Repair Ticket  is Updated " + status.getDescription() + " .-ID-" + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateMalaysianTerminalBreakdownTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_MALAY_TERMINAL_BREAKDOWN;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        Status status = statusDAO.getStatsuById(ticket.getStatus());
        tickethistory.setEmployee(t.getEmployee());
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        if (ticket.getInventoryhardwareitem() != null) {
            Inventoryhardwareitem inventoryhardwareitem = new Inventoryhardwareitem();
            inventoryhardwareitem.setInventoryhardwareitemid(ticket.getInventoryhardwareitem());
            t.setInventoryhardwareitem(inventoryhardwareitem);
        }
        if (ticket.getQuantity() != null && !ticket.getQuantity().equals("")) {
            t.setHardwareitemqty(Integer.parseInt(ticket.getQuantity()));
        }
        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQACCEM)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EM);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EM);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
        }
        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Maintenence Payment Invoice Ticket is Updated " + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void updateMalaysianTerminalRepairTicket(Ticket ticket) throws HibernateException, SQLException {
        String page = MasterDataVarList.AFFINITI_TICKET_CATEGORY_UPDATE_MALAY_TERMINAL_REPAIR;
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = "";
        int newTicketId = ticket.getTicketid();

        Date currentDateTime = commonDAO.getCurentTimesStamp();
        Ticketcategory ticketcategory = new Ticketcategory();
        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN);
        Tickethistory tickethistory = new Tickethistory();
        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
        tickethistory.setTicket(t);
        tickethistory.setAffecteddate(currentDateTime);
        if (!(ticket.getResolutiondescription() == null)) {
            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
        }
        Status status = statusDAO.getStatsuById(ticket.getStatus());
        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTDELTERMIRC)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_IRC);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_DTTIFS)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_IFS);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTGENDOIRC)) {
            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_EM);
            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR, userrole.getUserroleid());
            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
            t.setEmployee(assignee);
            tickethistory.setEmployee(assignee);

            if (ticketassignment == null) {
                ticketassignment = new Ticketassignment();
                ticketassignment.setCreateddatetime(currentDateTime);
                ticketassignment.setCreateduser(ticket.getCreateduser());
            }
            ticketassignment.setTicketcategory(ticketcategory);
            ticketassignment.setUserrole(userrole);
            ticketassignment.setEmployee(assignee);
            ticketassignment.setLastupdateddatetime(currentDateTime);
            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);

            Remindernotification remindernotification = new Remindernotification();
            remindernotification.setDescription(String.format(AffinitiMessages.REMINDERNOTIFY_DESC.getMessage(), ticket.getTicketid()));
            Systemuser systemuser = systemuserDAO.getSystemuserByEmployeeId(assignee.getEmployeeid());
            remindernotification.setReminderuser(systemuser.getUsername());
            remindernotification.setRemindertime(currentDateTime);
            remindernotification.setSourcetableid(ticket.getTicketid());
            remindernotification.setSectionid(MasterDataVarList.AFFINITI_TICKET_SECTION_ID);
            remindernotification.setCreateddatetime(currentDateTime);
            remindernotification.setLastupdateddatetime(currentDateTime);
            remindernotification.setCreateduser("SYSTEM");
            remindernotification.setReminderstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_REMIND);
            remindernotification.setViewedstatus(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_NOT_VIEWED);
            ticketDAO.createReminderNotification(remindernotification);

        } else {
            tickethistory.setEmployee(employeeDAO.getEmployeeByUsername(ticket.getCreateduser()));
        }
        tickethistory.setEmployee(t.getEmployee());
        t.setStatus(status);
        t.setLastupdateddatetime(currentDateTime);
        t.setLastupdateduser(ticket.getCreateduser());
        ticketDAO.updateTicket(t);

        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(ticket.getCreateduser());
        ticketHistoryDAO.createTicketHistory(tickethistory);

        description = "Malaysian TerminalRepair Ticket  is Updated " + status.getDescription() + " .-ID- " + newTicketId;
        task = "update";
        affectedId = String.valueOf(newTicketId);
        createdUser = ticket.getCreateduser();
        this.insertAuditTrace(page, affectedId, task, description, createdUser);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void backupTermianlAssign(int ticketid, int inventoryterminalid, String username) throws HibernateException, SQLException {
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket ticket = ticketDAO.getTicket(ticketid);
        Status status = statusDAO.getStatsuById(ticket.getStatus().getStatusid());
        Employee employee = null;
        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ACCBYTO)) {
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSTERMINIINV);
            employee = ticket.getEmployee();
        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQBACTERMEM)) {
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTISSBAKTERMEM);
            employee = ticket.getEmployee();
        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQBKPTMC)) {
            Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSBKPTMC);
            employee = tickethistory.getEmployee();
        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLRTFTMC)) {
            Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_IREDTLS);
            employee = tickethistory.getEmployee();
        }
        Inventoryterminalitem inventoryterminalitem = inventoryDAO.getTerminalById(inventoryterminalid);
        inventoryterminalitem.setInventoryterminalitemid(inventoryterminalid);
        ticket.setEmployee(employee);
        ticket.setStatus(status);
        ticket.setInventoryterminalitem(inventoryterminalitem);
        ticket.setLastupdateddatetime(currentDateTime);
        ticketDAO.updateTicket(ticket);

        Status IssuedStatus = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSUED);
        inventoryterminalitem.setStatus(IssuedStatus);
        inventoryDAO.changeStatusOfTerminalByStatus(inventoryterminalitem);

        Inventoryterminalhistory inventoryterminalhistory = new Inventoryterminalhistory();
        inventoryterminalhistory.setEmployee(employee);
        inventoryterminalhistory.setInventoryterminalitem(inventoryterminalitem);
        inventoryterminalhistory.setStatus(status);
        inventoryterminalhistory.setTicket(ticket);
        inventoryterminalhistory.setLastupdateddatetime(currentDateTime);
        inventoryterminalhistory.setCreateddatetime(currentDateTime);
        inventoryterminalhistory.setCreateduser(username);
        inventoryDAO.insertInventoryterminalhistory(inventoryterminalhistory, MasterDataVarList.AFFINITI_CODE_CREATE);

        Tickethistory tickethistory = new Tickethistory();
        tickethistory.setTicket(ticket);
        tickethistory.setAffecteddate(currentDateTime);
        tickethistory.setEmployee(employee);
        tickethistory.setInventoryterminalitem(inventoryterminalitem);
        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(username);
        ticketHistoryDAO.createTicketHistory(tickethistory);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void accessoriesAssign(int ticketid, JSONArray terminalhardwareitemlist,
            String username) throws HibernateException, SQLException {
        try {
            Date currentDateTime = commonDAO.getCurentTimesStamp();
            com.avn.affiniti.hibernate.model.Ticket ticket = ticketDAO.getTicket(ticketid);
            Status status = statusDAO.getStatsuById(ticket.getStatus().getStatusid());
            Employee employee = new Employee();
            if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSREQCSE)) {
                Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTACSSASSCSE);
                employee = tickethistory.getEmployee();
            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQACCEM)) {
                Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTISSUACCEM);
                employee = tickethistory.getEmployee();
            } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQPART)) { //test
                Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
                status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSPART);
                employee = tickethistory.getEmployee();
            }

            ticket.setEmployee(employee);
            ticket.setStatus(status);
            ticket.setLastupdateddatetime(currentDateTime);
            ticketDAO.updateTicket(ticket);

            Ticketinventoryitems ticketinventoryitems = new Ticketinventoryitems();
            int count = terminalhardwareitemlist.length();
//            if (ticket.getStatus().getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTREQACCEM)) {//Request accessories - EM
            if (count > ticket.getHardwareitemqty()) {//Assign only required quantity to Ticket
                count = ticket.getHardwareitemqty();
            }
//            }

            ticketinventoryitems.setTicket(ticket);
            ticketinventoryitems.setCreateddatetime(currentDateTime);
            ticketinventoryitems.setLastupdateddatetime(currentDateTime);
            ticketinventoryitems.setCreateduser(username);
            for (int i = 0; i < count; i++) {
//                Inventoryhardwareitem inventoryhardwareitem = new Inventoryhardwareitem();
//                inventoryhardwareitem.setInventoryhardwareitemid(terminalhardwareitemlist.getInt(i));
                Inventoryhardwareitem inventoryhardwareitem = inventoryDAO.getItemById(terminalhardwareitemlist.getInt(i));
                Status statusIssued = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSUED);
                inventoryhardwareitem.setStatus(statusIssued);
                ticketinventoryitems.setInventoryhardwareitem(inventoryhardwareitem);
                ticketinventoryitemDAO.createTicketInventoryhardwareItem(ticketinventoryitems);
                inventoryDAO.editHardwareItem(inventoryhardwareitem, MasterDataVarList.AFFINITI_CODE_UPDATE);
            }

            Tickethistory tickethistory = new Tickethistory();
            tickethistory.setTicket(ticket);
            tickethistory.setAffecteddate(currentDateTime);
            tickethistory.setEmployee(employee);
            tickethistory.setStatus(status);
            tickethistory.setLastupdateddatetime(currentDateTime);
            tickethistory.setCreateddatetime(currentDateTime);
            tickethistory.setCreateduser(username);
            ticketHistoryDAO.createTicketHistory(tickethistory);

            Inventoryhardwarehistory inventoryhardwarehistory = new Inventoryhardwarehistory();
            inventoryhardwarehistory.setTicket(ticket);
            inventoryhardwarehistory.setInventoryterminalitem(ticket.getInventoryterminalitem());
            inventoryhardwarehistory.setStatus(ticket.getStatus());
            inventoryhardwarehistory.setEmployee(ticket.getEmployee());
            inventoryhardwarehistory.setCreateduser(username);
            inventoryhardwarehistory.setCreateddatetime(new Date());
            inventoryhardwarehistory.setLastupdateddatetime(new Date());
            accessoryAssignDAO.insertInvenotyHardwareHistory(inventoryhardwarehistory);
        } catch (SQLException | HibernateException | JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public void newTerminalAssign(int ticketid, int inventoryterminalid, String username) throws HibernateException, SQLException {
        Date currentDateTime = commonDAO.getCurentTimesStamp();
        com.avn.affiniti.hibernate.model.Ticket ticket = ticketDAO.getTicket(ticketid);
        Status status = statusDAO.getStatsuById(ticket.getStatus().getStatusid());
        Employee employee = new Employee();
        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)
                && ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION) {
            Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTISSUTERMVO);
            employee = tickethistory.getEmployee();
        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)
                && ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE) {
            Tickethistory tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticketid, status.getStatusid());
            status = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTISSUTERMTO);
            employee = tickethistory.getEmployee();
        }
        Inventoryterminalitem inventoryterminalitem = inventoryDAO.getTerminalById(inventoryterminalid);
        inventoryterminalitem.setInventoryterminalitemid(inventoryterminalid);
        ticket.setEmployee(employee);
        ticket.setStatus(status);
        ticket.setInventoryterminalitem(inventoryterminalitem);
        ticket.setLastupdateddatetime(currentDateTime);
        ticketDAO.updateTicket(ticket);

        Status IssuedStatus = statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ISSUED);
        inventoryterminalitem.setStatus(IssuedStatus);
        inventoryDAO.changeStatusOfTerminalByStatus(inventoryterminalitem);

        Inventoryterminalhistory inventoryterminalhistory = new Inventoryterminalhistory();
        inventoryterminalhistory.setEmployee(employee);
        inventoryterminalhistory.setInventoryterminalitem(inventoryterminalitem);
        inventoryterminalhistory.setStatus(status);
        inventoryterminalhistory.setTicket(ticket);
        inventoryterminalhistory.setLastupdateddatetime(currentDateTime);
        inventoryterminalhistory.setCreateddatetime(currentDateTime);
        inventoryterminalhistory.setCreateduser(username);
        inventoryDAO.insertInventoryterminalhistory(inventoryterminalhistory, MasterDataVarList.AFFINITI_CODE_CREATE);

        Tickethistory tickethistory = new Tickethistory();
        tickethistory.setTicket(ticket);
        tickethistory.setAffecteddate(currentDateTime);
        tickethistory.setEmployee(employee);
        tickethistory.setInventoryterminalitem(inventoryterminalitem);
        tickethistory.setStatus(status);
        tickethistory.setLastupdateddatetime(currentDateTime);
        tickethistory.setCreateddatetime(currentDateTime);
        tickethistory.setCreateduser(username);
        ticketHistoryDAO.createTicketHistory(tickethistory);
    }

    @Override
    public TerminalRepairTicket getTerminalRepairTicketById(int ticketid) throws SQLException {
        return ticketDAO.getTerminalRepairTicket(ticketid);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject getTerminalRepairUpdatePageData(String username, int ticketid) throws SQLException, HibernateException {
        JSONObject object = new JSONObject();
        Ticketcategorystatus ticketcategorystatus = ticketCategorySatatusDAO.getTicketCategoryStatusByTktTktcategory(ticketid,
                MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR);
        object.put("statusList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(
                                DropdownSqlVarList.AFFINITI_DROPDOWN_TICKET_STATUS_UPDATE,
                                ticketcategorystatus.getTicketcategorystatusid(),
                                username
                        )
                )
        );

        object.put("causeoffaultList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CAUSEOFFAULT,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        object.put("actiontakenList",
                commonDAO.getJSONArrayDropdownValueList(
                        String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ACTIONTAKEN,
                                MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
                        )
                )
        );
        return object;
    }

//    @Override
//    @Transactional("sessionFactoryTransaction")
//    public void updateTerminalRepairTicket(Ticket ticket) throws HibernateException, SQLException {
//        Date currentDateTime = commonDAO.getCurentTimesStamp();
//        Ticketcategory ticketcategory = new Ticketcategory();
//        ticketcategory.setTicketcategoryid(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR);
//        Tickethistory tickethistory = new Tickethistory();
//        com.avn.affiniti.hibernate.model.Ticket t = ticketDAO.getTicket(ticket.getTicketid());
//        tickethistory.setTicket(t);
//        tickethistory.setAffecteddate(currentDateTime);
//        if (!(ticket.getResolutiondescription() == null)) {
//            tickethistory.setResolutiondescription(ticket.getResolutiondescription());
//        }
//        Status status = statusDAO.getStatsuById(ticket.getStatus());
//        if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)) {
//            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TO);
//            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
//            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//
//            if (ticketassignment == null) {
//                ticketassignment = new Ticketassignment();
//                ticketassignment.setCreateddatetime(currentDateTime);
//                ticketassignment.setCreateduser(ticket.getCreateduser());
//            }
//            ticketassignment.setTicketcategory(ticketcategory);
//            ticketassignment.setUserrole(userrole);
//            ticketassignment.setEmployee(assignee);
//            ticketassignment.setLastupdateddatetime(currentDateTime);
//            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
//        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_REQPART)) {
//            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
//            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
//            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//
//            if (ticketassignment == null) {
//                ticketassignment = new Ticketassignment();
//                ticketassignment.setCreateddatetime(currentDateTime);
//                ticketassignment.setCreateduser(ticket.getCreateduser());
//            }
//            ticketassignment.setTicketcategory(ticketcategory);
//            ticketassignment.setUserrole(userrole);
//            ticketassignment.setEmployee(assignee);
//            ticketassignment.setLastupdateddatetime(currentDateTime);
//            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
//        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOCSE)) {
//            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_CSE);
//            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
//            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//
//            if (ticketassignment == null) {
//                ticketassignment = new Ticketassignment();
//                ticketassignment.setCreateddatetime(currentDateTime);
//                ticketassignment.setCreateduser(ticket.getCreateduser());
//            }
//            ticketassignment.setTicketcategory(ticketcategory);
//            ticketassignment.setUserrole(userrole);
//            ticketassignment.setEmployee(assignee);
//            ticketassignment.setLastupdateddatetime(currentDateTime);
//            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
//        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TSTTREFAIL)) {
//            tickethistory = ticketHistoryDAO.getMaxTicketHistoryByTicketStatus(ticket.getTicketid(),
//                    (statusDAO.getStatusByStatusCode(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTO)).getStatusid());
//            Employee assignee = tickethistory.getEmployee();
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//        } else if (status.getStatuscode().equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOTMC)) {
//            Userrole userrole = userRoleDAO.getUserRoleByUserRoleCode(MasterDataVarList.AFFINITI_CODE_USERROLE_CODE_TMC);
//            Ticketassignment ticketassignment = ticketAssignmentDAO.getTicketAssignmentByTktCatUsrrol(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR, userrole.getUserroleid());
//            Employee assignee = this.getRoundRobbinTicketAssignee(userrole, ticketassignment);
//            t.setEmployee(assignee);
//            tickethistory.setEmployee(assignee);
//
//            if (ticketassignment == null) {
//                ticketassignment = new Ticketassignment();
//                ticketassignment.setCreateddatetime(currentDateTime);
//                ticketassignment.setCreateduser(ticket.getCreateduser());
//            }
//            ticketassignment.setTicketcategory(ticketcategory);
//            ticketassignment.setUserrole(userrole);
//            ticketassignment.setEmployee(assignee);
//            ticketassignment.setLastupdateddatetime(currentDateTime);
//            ticketAssignmentDAO.createOrUpdateTicketAssignment(ticketassignment);
//        } else {
//            tickethistory.setEmployee(t.getEmployee());
//        }
//
//        if (ticket.getCauseoffault() != null) {
//            Causeoffault causeoffault = new Causeoffault();
//            causeoffault.setCauseoffaultid(ticket.getCauseoffault());
//            t.setCauseoffault(causeoffault);
//            tickethistory.setCauseoffault(causeoffault);
//        }
//        if (ticket.getActiontaken() != null) {
//            Actiontaken actiontaken = new Actiontaken();
//            actiontaken.setActiontakenid(ticket.getActiontaken());
//            t.setActiontaken(actiontaken);
//            tickethistory.setActiontaken(actiontaken);
//        }
//
//        if (ticket.getIsusernegligence() != null) {
//            t.setIsusernegligence(ticket.getIsusernegligence());
//        }
//        if (ticket.getIslightening() != null) {
//            t.setIslightening(ticket.getIslightening());
//        }
//        if (ticket.getIscoveredunderwarranty() != null) {
//            t.setIscoveredunderwarranty(ticket.getIscoveredunderwarranty());
//        }
//        if (ticket.getIscoveredunderwarranty() != null) {
//            t.setIsunderamc(ticket.getIscoveredunderwarranty());
//        }
//        t.setDamagepartdescription(ticket.getDamagepartdescription());
//        t.setWarrantyexpirystatus(ticket.getWarrantyexpirystatus());
//        t.setReplacedpart(ticket.getReplacedpart());
//        t.setParttype(ticket.getParttype());
//        t.setPartnumber(ticket.getPartnumber());
//        t.setPartnumberofterminal(ticket.getPartnumberofterminal());
//        t.setRevision(ticket.getRevision());
//        t.setMac(ticket.getMac());
//        t.setPtid(ticket.getPtid());
//
//        if (ticket.isQuotation()) {
//            Terminalquotation terminalquotation = terminalQuotationDAO.getLatestTerminalQuotationByTicket(ticket.getTicketid());
//            if (terminalquotation == null) {
//                terminalquotation = new Terminalquotation();
//                terminalquotation.setCreateddatetime(currentDateTime);
//                terminalquotation.setCreateduser(ticket.getCreateduser());
//            }
//
//            if (ticket.getQcauseoffault() != null) {
//                Causeoffault causeoffault = new Causeoffault();
//                causeoffault.setCauseoffaultid(ticket.getQcauseoffault());
//                terminalquotation.setCauseoffault(causeoffault);
//            }
//            terminalquotation.setTicket(t);
//            terminalquotation.setRecipientbank(ticket.getRecipientbank());
//            terminalquotation.setTerminalserialno(ticket.getQterminalserialno());
//            terminalquotation.setTerminalmodel(ticket.getQterminalmodel());
//            terminalquotation.setMerchantlocation(ticket.getMerchantlocation());
//            terminalquotation.setReplacementpartno(ticket.getReplacementpartno());
//            terminalquotation.setReplacementdescription(ticket.getReplacementdescription());
//            terminalquotation.setUnitprice(ticket.getUnitprice());
//            terminalquotation.setQuantity(ticket.getQuantity());
//            terminalquotation.setTotal(ticket.getTotal());
//            terminalquotation.setQuatationvalidityperiod(ticket.getQuatationvalidityperiod());
//            terminalquotation.setLastupdateddatetime(currentDateTime);
//            terminalQuotationDAO.createOrUpdateTerminalQuotation(terminalquotation);
//        }
//
//        t.setCompletiondatetime(ticket.getCompletiondatetime());
//        t.setTotletimespent(ticket.getTotletimespent());
//        t.setSpecialremark(ticket.getSpecialremark());
//        t.setDispatchdate(ticket.getDispatchdate());
//        t.setCourierserviceprovider(ticket.getCourierserviceprovider());
//        t.setTrackingnumber(ticket.getTrackingnumber());
//        t.setDeleverydestination(ticket.getDeliverydestination());
//        t.setStatus(status);
//        t.setLastupdateddatetime(currentDateTime);
//        ticketDAO.updateTicket(t);
//
//        tickethistory.setStatus(status);
//        tickethistory.setLastupdateddatetime(currentDateTime);
//        tickethistory.setCreateddatetime(currentDateTime);
//        tickethistory.setCreateduser(ticket.getCreateduser());
//        ticketHistoryDAO.createTicketHistory(tickethistory);
//    }
    private Employee getRoundRobbinTicketAssignee(Userrole userrole, Ticketassignment ticketassignment) throws HibernateException {
        Employee assignee;
        if (ticketassignment != null) {
            assignee = employeeDAO.getNextAssigneeEmployeeByUserRole(userrole.getUserroleid(), ticketassignment.getEmployee().getEmployeeid());
            if (assignee == null) {
                assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
            }
        } else {
            assignee = employeeDAO.getFirstAssigneeEmployeeByUserRole(userrole.getUserroleid());
        }
        return assignee;
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public String getAssigneeByTicketId(int ticketid) throws HibernateException, SQLException {
        return ticketDAO.getAssigneeByTicketId(ticketid);
    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public String getTicketData(HttpServletRequest request, TicketParam parameters) throws SQLException {
        int userRoleId = systemuserDAO.getUserRoleByUsername(parameters.getCreateduser());
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword
        JSONArray rows = new JSONArray();

        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }

//        if (Boolean.valueOf(request.getParameter("search"))) {
        iTotalRecords = ticketDAO.getTableCount(parameters);
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            List<TicketParam> ticketList = ticketDAO.getTableData(parameters, param.iDisplayStart, param.iDisplayLength);

            for (TicketParam ticket : ticketList) {
                JSONObject jsonobject = new JSONObject();

                jsonobject.put("ticketid", ticket.getTicketid());
                jsonobject.put("ticketdate", Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd_hh_mm_a, ticket.getTicketdate()));
//                object.put("date", (rs.getDate("CREATEDDATETIME")) != null ? Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd_hh_mm_a, rs.getDate("CREATEDDATETIME")) : "N/A");
                jsonobject.put("ticketpriority", ticket.getTicketpriority());
                jsonobject.put("merchantname", ticket.getMerchantname());
                jsonobject.put("merchantcontactno", ticket.getContactno());
                jsonobject.put("status", ticket.getStatus());

                String action;
                if (ticket.getStatuscode().equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_ASSTOSCSC) && userRoleId == MasterDataVarList.AFFINITI_CODE_USERROLE_TO) {
                    action = "<div class=\"row\">"
                            + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + ticket.getTicketid() + "'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                            + "<div class=\"col-xs-3\"><a href=\"#\" id=\"switchassignee\" value = \"switch\" data-id=" + ticket.getTicketid() + " <i class=\"fa fa-lg fa-fw fa-refresh\" title=\"Switch Assignee\"></i></a></div>"
                            + "</div>";
                } else {
                    action = "<div class=\"row\">"
                            + "<div class=\"col-xs-3\"><a href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + ticket.getTicketid() + "'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                            + "</div>";
                }
                jsonobject.put("action", action);
                rows.put(jsonobject);
            }
        }

//        }
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
        jsonResponse.put("aaData", rows);
        return jsonResponse.toString();

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject insertUploadedFileInfo(Ticket ticket) throws SQLException {

        Bulkterminallot bulkterminallot = new Bulkterminallot();
        Ticketcategory ticketCategory = new Ticketcategory();
        ticketCategory.setTicketcategoryid(ticket.getTicketcategory());
        bulkterminallot.setTicketcategory(ticketCategory);
        bulkterminallot.setFilename(ticket.getFile().getOriginalFilename());
        bulkterminallot.setFilepath(ticket.getFilepath());
        bulkterminallot.setCreateduser(ticket.getCreateduser());
        bulkterminallot.setCreateddatetime(new Date());
        bulkterminallot.setLastupdateddatetime(new Date());

        return ticketDAO.insertFileMasterData(bulkterminallot);

    }

    /* Check empty rows in excel file */
    public boolean containsValue(Row row) {

        int fcell = row.getFirstCellNum();// first cell number of excel
        int lcell = row.getLastCellNum(); //last cell number of excel
        int emptycellcount = 0;

        boolean flag = true;

        for (int i = fcell; i < lcell; i++) {
            if (StringUtils.isEmpty(String.valueOf(row.getCell(i))) == true
                    || StringUtils.isWhitespace(String.valueOf(row.getCell(i))) == true
                    || StringUtils.isBlank(String.valueOf(row.getCell(i))) == true
                    || String.valueOf(row.getCell(i)).length() == 0
                    || row.getCell(i) == null) {
                emptycellcount++;
            }
        }

        if (emptycellcount == lcell) {
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }

    @Override
    public JSONObject getTerminalInfo(String input) throws SQLException {
        return ticketDAO.getTerminalInfo(input);
    }

    @Override
    @Transactional
    public JSONObject getTicketHistoryData(HttpServletRequest request) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<TicketHistory> list = null;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        TicketParam ticket = new TicketParam();
        ticket.setTicketid(Integer.parseInt(request.getParameter("ticketid")));
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            iTotalRecords = ticketDAO.getTickeHistoryDataCount(ticket);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = ticketDAO.getTickeHistoryData(ticket, param.iDisplayLength, param.iDisplayStart);
                //  rows = new JSONArray(list);

                for (TicketHistory tickethistory : list) {
                    JSONObject jsonobject = new JSONObject();
                    jsonobject.put("date", Common.getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd_hh_mm_a, tickethistory.getCreateddatetime()));
                    jsonobject.put("causeOfFault", tickethistory.getCauseoffault());
                    jsonobject.put("actionTaken", tickethistory.getActiontaken());
                    jsonobject.put("assigneeid", tickethistory.getAssignee());
                    jsonobject.put("status", tickethistory.getStatus());
                    jsonobject.put("resolution", tickethistory.getResolutiondescription());
                    if (tickethistory.getPreviousticketid().equals("N/A")) {
                        jsonobject.put("action", tickethistory.getPreviousticketid());
                    } else {
                        String action = "<div class=\"row\">"
                                + "<div class=\"col-xs-3\"><a target='_blank' href='" + request.getContextPath() + "/ticket/getticket?ticketid=" + tickethistory.getPreviousticketid() + "'value='parent_tTicket'><i class=\"fa fa-lg fa-fw fa-link\" title=\"Parent Ticket\"></i></a></div>"
                                + "</div>";
                        jsonobject.put("action", action);

                    }
                    rows.put(jsonobject);
                }
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception ex) {
            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
            Loggers.LOGGER_FILE.error(ex);
        }

        return jsonResponse;
    }

    @Override
    public String generateTerminalSharingTicketXML(int ticketId) throws Exception {
        /* XML format
         <share>
         <shno id="1">
         <merchant></merchant>
         <address></address>
         <vmmid></vmmid>
         <vmtid></vmtid>
         <amexmid></amexmid>
         <amextid></amextid>
         <curr></curr>
         <remark></remark>
         </shno>
         </share>
         */

        if (ticketDAO.getStatusCodeByTicketId(ticketId).equals(MasterDataVarList.AFFINITI_CODE_STATUS_ID_TKT_ACCBYVO)) {

            TerminalSharingTicket ticket = ticketDAO.getTerminalSharingTicketById(ticketId);

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // share element
            Document doc = docBuilder.newDocument();
            Element share = doc.createElement("share");
            doc.appendChild(share);

            // shno element
            Element shno = doc.createElement("shno");
            share.appendChild(shno);

            // set attribute to shno element
            Attr attr = doc.createAttribute("id");
            attr.setValue(String.valueOf(ticket.getTicketid()));
            shno.setAttributeNode(attr);

            // merchant element
            Element merchant = doc.createElement("merchant");
            merchant.appendChild(doc.createTextNode(ticket.getMerchantname()));
            shno.appendChild(merchant);

            // address element
            Element address = doc.createElement("address");
            address.appendChild(doc.createTextNode(ticket.getLocationaddress()));
            shno.appendChild(address);

            // vmmid element
            Element vmmid = doc.createElement("vmmid");
            vmmid.appendChild(doc.createTextNode(ticket.getVmmid()));
            shno.appendChild(vmmid);

            // vmtid element
            Element vmtid = doc.createElement("vmtid");
            vmtid.appendChild(doc.createTextNode(ticket.getVmtid()));
            shno.appendChild(vmtid);

            // amexmid element
            Element amexmid = doc.createElement("amexmid");
            amexmid.appendChild(doc.createTextNode(ticket.getAmexmid()));
            shno.appendChild(amexmid);

            // amextid element
            Element amextid = doc.createElement("amextid");
            amextid.appendChild(doc.createTextNode(ticket.getAmextid()));
            shno.appendChild(amextid);

            // curr element
            Element curr = doc.createElement("curr");
            curr.appendChild(doc.createTextNode(ticket.getCurrency()));
            shno.appendChild(curr);

            // remark element
            Element remark = doc.createElement("remark");
            remark.appendChild(doc.createTextNode(ticket.getRemark()));
            shno.appendChild(remark);

            // Output xml to string
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            // pretty format xml
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

            DOMSource source = new DOMSource(doc);

            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            transformer.transform(source, result);
            return writer.toString();
        } else {
            return "";
        }
    }

    @Override
    public String generateNewTerminalInstallationTicketXML(int ticketId) throws Exception {
        if (ticketDAO.getStatusCodeByTicketId(ticketId).equals(MasterDataVarList.AFFINITI_CODE_STATUS_ID_TKT_ACCBYVO)) {

            NewInstallationTicke ticket = ticketDAO.getNewInstallationTicket(ticketId);

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // profile element
            Document doc = docBuilder.newDocument();
            Element profile = doc.createElement("profile");
            doc.appendChild(profile);

            // type element
            Element type = doc.createElement("type");
            type.appendChild(doc.createTextNode(ticket.getProfiletype()));
            profile.appendChild(type);

            // model element
            Element model = doc.createElement("model");
            model.appendChild(doc.createTextNode(ticket.getTerminalmodel()));
            profile.appendChild(model);

            // serialno element
            Element serialno = doc.createElement("serialno");
            serialno.appendChild(doc.createTextNode(ticket.getTerminalserial()));
            profile.appendChild(serialno);

            // merchant_name element
            Element merchant_name = doc.createElement("merchant_name");
            merchant_name.appendChild(doc.createTextNode(ticket.getMerchantname()));
            profile.appendChild(merchant_name);

            // address1 element
            Element address1 = doc.createElement("address1");
            address1.appendChild(doc.createTextNode("XXXXXXX"));
            profile.appendChild(address1);

            // address2 element
            Element address2 = doc.createElement("address2");
            address2.appendChild(doc.createTextNode("XXXXXXX"));
            profile.appendChild(address2);

            // password element
            Element password = doc.createElement("password");
            password.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getPassword())));
            profile.appendChild(password);

            // mnlkey element
            Element mnlkey = doc.createElement("mnlkey");
            mnlkey.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getManualkey())));
            profile.appendChild(mnlkey);

            // offline element
            Element offline = doc.createElement("offline");
            offline.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getOffline())));
            profile.appendChild(offline);

            // preauth element
            Element preauth = doc.createElement("preauth");
            preauth.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getPreauth())));
            profile.appendChild(preauth);

            // tip element
            Element tip = doc.createElement("tip");
            tip.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getTip())));
            profile.appendChild(tip);

            // cashadvance element
            Element cashadvance = doc.createElement("cashadvance");
            cashadvance.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getCashadvance())));
            profile.appendChild(cashadvance);

            // panmask element
            Element panmask = doc.createElement("panmask");
            panmask.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getPanmask())));
            profile.appendChild(panmask);

            // l4 element
            Element l4 = doc.createElement("l4");
            l4.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getL4())));
            profile.appendChild(l4);

            // emv element
            Element emv = doc.createElement("emv");
            emv.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getEmv())));
            profile.appendChild(emv);

            // fourdbc element
            Element fourdbc = doc.createElement("fourdbc");
            fourdbc.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getFourdbc())));
            profile.appendChild(fourdbc);

            // toffline element
            Element toffline = doc.createElement("toffline");
            toffline.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit("XXXXXXX")));
            profile.appendChild(toffline);

            // tvoid element
            Element tvoid = doc.createElement("tvoid");
            tvoid.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getVoid_())));
            profile.appendChild(tvoid);

            // tsettlement element
            Element tsettlement = doc.createElement("tsettlement");
            tsettlement.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getSettlement())));
            profile.appendChild(tsettlement);

            // installbyepic element
            Element installbyepic = doc.createElement("installbyepic");
            installbyepic.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getTobeinstalledbyepic())));
            profile.appendChild(installbyepic);

            // simno element
            Element simno = doc.createElement("simno");
            simno.appendChild(doc.createTextNode(ticket.getSimno()));
            profile.appendChild(simno);

            // cperson element
            Element cperson = doc.createElement("cperson");
            cperson.appendChild(doc.createTextNode(ticket.getContactperson()));
            profile.appendChild(cperson);

            // cno element
            Element cno = doc.createElement("cno");
            cno.appendChild(doc.createTextNode(ticket.getContactno()));
            profile.appendChild(cno);

            // remark element
            Element remark = doc.createElement("remark");
            remark.appendChild(doc.createTextNode(ticket.getRemarks()));
            profile.appendChild(remark);

            // ddate element
            Element ddate = doc.createElement("ddate");
            ddate.appendChild(doc.createTextNode(ticket.getDeadlinedate()));
            profile.appendChild(ddate);

            // autosettlement element
            Element autosettlement = doc.createElement("autosettlement");
            autosettlement.appendChild(doc.createTextNode(Common.replaceTicketAttributeStatusToDigit(ticket.getAutosettlement())));
            profile.appendChild(autosettlement);

            // autostltime element
            Element autostltime = doc.createElement("autostltime");
            autostltime.appendChild(doc.createTextNode("XXXXXXX"));
            profile.appendChild(autostltime);

            // terminalid element
            Element terminalid = doc.createElement("terminalid");
            profile.appendChild(terminalid);

            // tno element
            Element tno = doc.createElement("tno");
            terminalid.appendChild(tno);

            // set attribute to tno element
            Attr attr = doc.createAttribute("id");
            attr.setValue(String.valueOf("XXXXXXXX"));
            tno.setAttributeNode(attr);

            // tid element
            Element tid = doc.createElement("tid");
            tid.appendChild(doc.createTextNode(ticket.getTid()));
            tno.appendChild(tid);

            // mid element
            Element mid = doc.createElement("mid");
            mid.appendChild(doc.createTextNode(ticket.getMid()));
            tno.appendChild(mid);

            // stid element
            Element stid = doc.createElement("stid");
            stid.appendChild(doc.createTextNode(ticket.getSharingtid()));
            tno.appendChild(stid);

            // desc element
            Element desc = doc.createElement("desc");
            desc.appendChild(doc.createTextNode("XXXXXXX"));
            tno.appendChild(desc);

            // curr element
            Element curr = doc.createElement("curr");
            curr.appendChild(doc.createTextNode(ticket.getCurrency()));
            tno.appendChild(curr);

            // esp element
            Element esp = doc.createElement("esp");
            esp.appendChild(doc.createTextNode(ticket.getEsp()));
            tno.appendChild(esp);

            // nacno element
            Element nacno = doc.createElement("nacno");
            nacno.appendChild(doc.createTextNode(String.valueOf(ticket.getNacno())));
            tno.appendChild(nacno);

            // bankpass element
            Element bankpass = doc.createElement("bankpass");
            profile.appendChild(bankpass);

            // userpass element array
            Element[] userpassArr = new Element[20];
            Attr[] userpassIdArr = new Attr[20];
            Element[] userArr = new Element[20];
            Element[] passArr = new Element[20];

            String[] users = this.generateUserArrayFromTicketUserpass(ticket);
            String[] passwords = this.generatePasswordArrayFromTicketUserpass(ticket);

            for (int i = 0; i < 20; i++) {
                // userpass element
                userpassArr[i] = doc.createElement("userpass");
                bankpass.appendChild(userpassArr[i]);

                // set attribute to userpass element
                userpassIdArr[i] = doc.createAttribute("id");
                userpassIdArr[i].setValue(String.valueOf(i + 1));
                userpassArr[i].setAttributeNode(userpassIdArr[i]);

                // user1 element
                userArr[i] = doc.createElement("user1");
                userArr[i].appendChild(doc.createTextNode(String.valueOf(users[i])));
                userpassArr[i].appendChild(userArr[i]);

                // userpass1 element
                passArr[i] = doc.createElement("userpass1");
                passArr[i].appendChild(doc.createTextNode(String.valueOf(passwords[i])));
                userpassArr[i].appendChild(passArr[i]);
            }

            // Output xml to string
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            // pretty format xml
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

            DOMSource source = new DOMSource(doc);

            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            transformer.transform(source, result);
            return writer.toString();
        } else {
            return "";
        }
    }

    private String[] generateUserArrayFromTicketUserpass(NewInstallationTicke ticket) {
        String[] arr = new String[20];
        arr[0] = ticket.getUser1();
        arr[1] = ticket.getUser2();
        arr[2] = ticket.getUser3();
        arr[3] = ticket.getUser4();
        arr[4] = ticket.getUser5();
        arr[5] = ticket.getUser6();
        arr[6] = ticket.getUser7();
        arr[7] = ticket.getUser8();
        arr[8] = ticket.getUser9();
        arr[9] = ticket.getUser10();
        arr[10] = ticket.getUser11();
        arr[11] = ticket.getUser12();
        arr[12] = ticket.getUser13();
        arr[13] = ticket.getUser14();
        arr[14] = ticket.getUser15();
        arr[15] = ticket.getUser16();
        arr[16] = ticket.getUser17();
        arr[17] = ticket.getUser18();
        arr[18] = ticket.getUser19();
        arr[19] = ticket.getUser20();

        return arr;
    }

    private String[] generatePasswordArrayFromTicketUserpass(NewInstallationTicke ticket) {
        String[] arr = new String[20];
        arr[0] = ticket.getUserpass1();
        arr[1] = ticket.getUserpass2();
        arr[2] = ticket.getUserpass3();
        arr[3] = ticket.getUserpass4();
        arr[4] = ticket.getUserpass5();
        arr[5] = ticket.getUserpass6();
        arr[6] = ticket.getUserpass7();
        arr[7] = ticket.getUserpass8();
        arr[8] = ticket.getUserpass9();
        arr[9] = ticket.getUserpass10();
        arr[10] = ticket.getUserpass11();
        arr[11] = ticket.getUserpass12();
        arr[12] = ticket.getUserpass13();
        arr[13] = ticket.getUserpass14();
        arr[14] = ticket.getUserpass15();
        arr[15] = ticket.getUserpass16();
        arr[16] = ticket.getUserpass17();
        arr[17] = ticket.getUserpass18();
        arr[18] = ticket.getUserpass19();
        arr[19] = ticket.getUserpass20();

        return arr;
    }

    @Override
    public JSONObject getStatusCodeByTicketId(int ticketId) throws Exception {
        JSONObject output = new JSONObject();
        String statusCode = ticketDAO.getStatusCodeByTicketId(ticketId);
        output.put("statuscode", statusCode);

        return output;
    }

    @Override
    @Transactional
    public int getLocationByUsername(String username) throws Exception {
        Location location = employeeDAO.getEmployeeByUsername(username).getLocation();
        return (location == null) ? MasterDataVarList.AFFINITI_CODE_LOCATION_LK : location.getLocationid();
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws SQLException, HibernateException {
        Audittrace auditTrace = new Audittrace();
        auditTrace.setAffectedpage(page);
        auditTrace.setTask(task);
        auditTrace.setCreateduser(createdUser);
        auditTrace.setCreatedatetime(new Date());
        auditTrace.setLastupdateddatetime(new Date());
        auditTrace.setAfectedid(affectedId);
        auditTrace.setDescription(description);
        auditTraceDAO.insertAuditTrace(auditTrace);
    }

    @Override
    @Transactional
    public void sendTicketAssignmentNotificationEmail(int ticketid, HttpServletRequest request) throws Exception {

        com.avn.affiniti.hibernate.model.Ticket ticket = ticketDAO.getTicket(ticketid);
        String url = "http://" + request.getServerName() + (request.getServerPort() == 0 ? "" : ":" + request.getServerPort()) + request.getContextPath() + "/ticket/getticket?ticketid={ticketid}";
        Notificationtempate notificationTemplate = notificationTemplateDAO.getNotificationTemplate(MasterDataVarList.AFFINITI_CODE_NOTIFICATION_TEMPLATE_TICKET_INITIATE);

//        String[] ccemaillist = {"cc1@example.com", "cc2@example.com"};
//        String[] bccemaillist = {"bcc1@example.com", "bcc2@example.com"};
        SimpleMailMessage sm = new SimpleMailMessage();

        String assigneeName = ticket.getEmployee().getNameinfull();
        String assigneeEmail = ticket.getEmployee().getEmail();

        //String subject = String.format(notificationTemplate.getSubject(), Common.getFirstCharcterCapital(userCase.getTitle()), customerName, String.valueOf(caseid));
        String subject = String.format(notificationTemplate.getSubject(), String.valueOf(ticket.getTicketid()));

        String mailContent = String.format(notificationTemplate.getReciepent(), assigneeName)
                + notificationTemplate.getIntro()
                + String.format(notificationTemplate.getMailcontent(),
                        String.valueOf(ticket.getTicketid()),
                        Common.replaceEmptyorNullStringToNA((ticket.getTicketpriority() != null) ? ticket.getTicketpriority().getDescription() : ""),
                        Common.replaceEmptyorNullStringToNA((ticket.getTicketcategory() != null) ? ticket.getTicketcategory().getDescription() : ""),
                        Common.replaceEmptyorNullStringToNA((ticket.getProduct() != null) ? ticket.getProduct().getDescription() : ""),
                        Common.replaceEmptyorNullStringToNA((ticket.getProductcategory() != null) ? ticket.getProductcategory().getDescription() : ""),
                        Common.replaceEmptyorNullStringToNA((ticket.getClient() != null) ? ticket.getClient().getName() : ""),
                        Common.replaceEmptyorNullStringToNA(ticket.getLocationaddress()),
                        Common.replaceEmptyorNullStringToNA((ticket.getDistrict() != null) ? ticket.getDistrict().getDescription() : ""),
                        Common.replaceEmptyorNullStringToNA(ticket.getContactpersion()),
                        Common.replaceEmptyorNullStringToNA(ticket.getContactno()),
                        Common.replaceEmptyorNullStringToNA(ticket.getMerchantcontactnumber()),
                        Common.replaceEmptyorNullStringToNA(""),
                        Common.replaceEmptyorNullStringToNA(url.replace("{ticketid}", String.valueOf(ticket.getTicketid())))
                );

        sm.setFrom(CommonVarList.NOTIFICATION_MAIL_FROM);
        sm.setTo(assigneeEmail);
        sm.setSubject(subject);
        sm.setText(mailContent);
        //        sm.setCc(ccemaillist);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(sm.getFrom());

        if (sm.getTo() != null && sm.getTo().length > 0) {
            if (sm.getTo()[0] != null) {
                helper.setTo(sm.getTo());
            }
        }
//        if (sm.getCc() != null && sm.getCc().length > 0) {
//            helper.setCc(sm.getCc());
//        }
//        if (sm.getBcc() != null && sm.getBcc().length > 0) {
//            helper.setBcc(sm.getBcc());
//        }
        helper.setSubject(sm.getSubject());
        helper.setText(sm.getText());

        //            if (resources != null) {
        //                for (FileSystemResource resource : resources) {
        //                    helper.addAttachment(resource.getFilename(), resource);
        //                }
        //            }
        mailSender.send(message);

        Notificationmaillog notificationmaillog = new Notificationmaillog();
        Date currentTime = new Date();
        notificationmaillog.setCreatedatetime(currentTime);
        notificationmaillog.setLastupdateddatetime(currentTime);
        notificationmaillog.setCreateduser(subject);
        notificationmaillog.setSenttime(currentTime);
        notificationmaillog.setRecipientto(assigneeEmail);

//        StringBuilder ccBuilder = new StringBuilder();
//        for (String s : ccemaillist) {
//            ccBuilder.append(s);
//        }
//        String cc = ccBuilder.toString();
//
//        StringBuilder bccBuilder = new StringBuilder();
//        for (String s : bccemaillist) {
//            bccBuilder.append(s);
//        }
//        String bcc = bccBuilder.toString();
//        notificationmaillog.setRecipientcc(cc);
//        notificationmaillog.setRecipientbcc(bcc);
        notificationmaillog.setNotificationtempate(notificationTemplate);

        notificationMailLogDAO.createNotificationMailLog(notificationmaillog);

    }

}
