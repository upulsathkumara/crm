/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.ticket;

import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Merchantacceptancestatus;
import com.avn.affiniti.model.ticket.BackupRemovalTicket;
import com.avn.affiniti.model.ticket.BaseSoftwareInstallationTicket;
import com.avn.affiniti.model.ticket.BulkTerminalBreakdown;
import com.avn.affiniti.model.ticket.MaintainancePaymentInvoiceTicket;
import com.avn.affiniti.model.ticket.MerchantRemovalTicket;
import com.avn.affiniti.model.ticket.NewInstallationTicke;
import com.avn.affiniti.model.ticket.ReinitializationTicket;
import com.avn.affiniti.model.ticket.SoftwareHardwareBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalConversionTicket;
import com.avn.affiniti.model.ticket.TerminalRepairMysTicket;
import com.avn.affiniti.model.ticket.TerminalRepairTicket;
import com.avn.affiniti.model.ticket.TerminalSharingTicket;
import com.avn.affiniti.model.ticket.TerminalUpgradeDowngradeTicket;
import com.avn.affiniti.model.ticket.Ticket;
import com.avn.affiniti.model.ticket.TicketParam;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketService
 * @Created on : Apr 28, 2017, 3:58:13 PM
 */
public interface TicketService {

    public void loadPageComponent(Map<String, Object> model, String username) throws SQLException, HibernateException;

    public Map<String, String> getStatusDropDown() throws SQLException;

    public JSONObject getTerminalBrakdownCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getNewTerminalInstallationCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getSoftawreHardwareCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getMerchantRemovalCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getBaseSoftwareInstallationCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getTerminalUpgradeDowngradeCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getBackupRemovalCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getBulkTerminalBreakdownCreatePageData(String username) throws SQLException, HibernateException;

    public JSONObject getTerminalSharingCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getTerminalConversionCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getReinitializationCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getTermialBreakdownMalayasiaCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONObject getTermialRepairMalayasiaCreatePageData(String username, int locationid) throws SQLException, HibernateException;

    public JSONArray getTerminalModelDropdownByBrand(int terminalbrand) throws SQLException;

    public JSONArray getClientDropdownByClientCategory(int clientcategory, int location) throws Exception;

    public JSONArray getAccessoryType(int status) throws SQLException;

    public JSONArray getProductCategory(int product) throws SQLException;

    public JSONArray getManualAssigneeListById(int ticketId, String username, String manualassigneestatus, int status) throws SQLException;

    public JSONArray getDefaultAssigneeListById(int ticketId) throws SQLException;

    public Boolean validateSerialNumber(String serialNumber) throws SQLException;

    public void switchAssignee(int ticketId, String username) throws SQLException;

    public int createTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createNewTerminalInstalationTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createSoftwareHardwareBreakdownTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createMerchantRemovalTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createBaseSoftwareInstallationTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createTerminalUpgradeDowngradeTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createBackupRemovalTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public List<Integer> createBulkTerminalBreakdownTicket(Ticket ticket) throws SQLException, FileNotFoundException, IOException, ParseException, Exception;

    public int createTerminalSharingTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createTerminalConversionTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createReinitializationTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createTerminalalBrakdownMalayasiaTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public int createTerminalRepairMalayasiaTicket(Ticket ticket) throws HibernateException, SQLException, Exception;

    public com.avn.affiniti.hibernate.model.Ticket getTicket(int ticketid) throws HibernateException;

    public TerminalBreakdownTicket getTerminalBrakdownTicketById(int ticketid) throws SQLException;

    public NewInstallationTicke getNewInstallationTicketById(int ticketid) throws SQLException;

    public SoftwareHardwareBreakdownTicket getSoftwareHardwareTicketById(int ticketid) throws SQLException;

    public MerchantRemovalTicket getMerchantRemovalTicketById(int ticketid) throws SQLException;

    public BaseSoftwareInstallationTicket getBaseSoftwareInstallationTicketById(int ticketid) throws SQLException;

    public TerminalUpgradeDowngradeTicket getTerminalUpgradeDowngradeTicketById(int ticketid) throws SQLException;

    public BackupRemovalTicket getBackupRemovalTicketById(int ticketid) throws SQLException;

    public TerminalSharingTicket getTerminalSharingTicketById(int ticketid) throws SQLException;

    public TerminalConversionTicket getTerminalConversionTicketById(int ticketid) throws SQLException;

    public ReinitializationTicket getReinitializationTicketById(int ticketid) throws SQLException;

    public MaintainancePaymentInvoiceTicket getMaintainancePaymentInvoiceTicketById(int ticketid) throws SQLException;

    public TerminalBreakdownTicket getMalaysianTerminalBreakdownTicketById(int ticketid) throws SQLException;

    public TerminalRepairMysTicket getMalaysianTerminalRepairTicketById(int ticketid) throws SQLException;

    public JSONObject getTerminalBrakdownUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getNewInstallationTicketUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getSoftwareHardwareBreakdownUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getMerchantRemovalUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getBaseSoftwareInstallationUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getTerminalUpgradeDowngradeUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getBackupRemovalUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getTerminalSharingUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getTerminalConversionUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getReinitializationUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getMaintainacePaymentInvoiceUpdatePageData(String username, String status, int ticketid) throws SQLException, HibernateException;

    public JSONObject getMalaysiaTerminalBrakdownUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public JSONObject getMalaysiaTerminalRepairUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public int updateTerminalBrakdownTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateNewInstallationTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateSoftwareHardwareBreakdownTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateMerchantRemovalTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateBaseSoftawareInstallationTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateTerminalUpgradeDowngradeTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateTerminalConversionTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateBackupRemovalTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateTerminalSharingTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateReinitializationTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateMaintenencePaymentInvoiceTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateMalaysianTerminalBreakdownTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateMalaysianTerminalRepairTicket(Ticket ticket) throws HibernateException, SQLException;

    public void updateTerminalRepairTicket(Ticket ticket) throws HibernateException, SQLException;

    public void backupTermianlAssign(int ticketid, int inventoryterminalid, String username) throws HibernateException, SQLException;

    public void accessoriesAssign(int ticketid, JSONArray terminalhardwareitemlist, String username) throws HibernateException, SQLException;

    public void newTerminalAssign(int ticketid, int inventoryterminalid, String username) throws HibernateException, SQLException;

    public TerminalRepairTicket getTerminalRepairTicketById(int ticketid) throws SQLException;

    public JSONObject getTerminalRepairUpdatePageData(String username, int ticketid) throws SQLException, HibernateException;

    public String getAssigneeByTicketId(int ticketid) throws HibernateException, SQLException;

    public String getTicketData(HttpServletRequest request, TicketParam parameters) throws SQLException;

    public JSONObject insertUploadedFileInfo(Ticket ticket) throws SQLException;

    public JSONObject getTerminalInfo(String input) throws SQLException;

    public JSONObject getTicketHistoryData(HttpServletRequest request) throws SQLException, Exception;

    public String generateTerminalSharingTicketXML(int ticketId) throws Exception;

    public String generateNewTerminalInstallationTicketXML(int ticketId) throws Exception;

    public JSONObject getStatusCodeByTicketId(int ticketId) throws Exception;

    public int getLocationByUsername(String username) throws Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws SQLException, HibernateException;

    public void sendTicketAssignmentNotificationEmail(int ticketid, HttpServletRequest request) throws Exception;
}
