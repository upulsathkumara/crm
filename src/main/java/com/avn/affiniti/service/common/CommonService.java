/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.common;

import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONArray;

/**
 *
 * @author Kaushan Fernando
 * @Document CommonService
 * @Created on: Sep 29, 2017, 2:36:00 PM
 */
public interface CommonService {

    public String getSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;

    public String getSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;

    public String getTaskIdAndSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;

    public String getTaskIdSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception;

    public boolean getUserRolePermission(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception;

    public List<Integer> getTaskListByUserRoleIdAndSectionId(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws SQLException, Exception;

    public List<Integer> getTaskListByUserRoleIdSubSectionId(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception;

}
