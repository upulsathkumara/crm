package com.avn.affiniti.service.common;

import com.avn.affiniti.dao.common.CommonDAO;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kaushan Fernando
 * @Document CommonServiceImpl
 * @Created on: Sep 29, 2017, 2:36:30 PM
 */
@Service("commonService")
public class CommonServiceImpl implements CommonService {

    @Autowired
    private CommonDAO commonDAO;

    @Override
    @Transactional
    public String getSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        return commonDAO.getSubSectionIdByURL(URL);
    }

    @Override
    @Transactional
    public boolean getUserRolePermission(int userRoleId, int subSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        boolean rolePermission = false;
        if (commonDAO.checkSectionAndSubSectionStatus(subSectionId, userRolePermissionType)) {
            rolePermission = commonDAO.getUserRolePermission(userRoleId, subSectionId, taskId, userRolePermissionType);
        }
        return rolePermission;
    }

    @Override
    @Transactional
    public String getSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        return commonDAO.getSectionIdByURL(URL);
    }

    @Override
    @Transactional
    public String getTaskIdAndSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        return commonDAO.getTaskIdAndSectionIdByURL(URL);
    }

    @Override
    @Transactional
    public String getTaskIdSubSectionIdByURL(String URL) throws HibernateException, SQLException, Exception {
        return commonDAO.getTaskIdSubSectionIdByURL(URL);
    }

    @Override
    @Transactional
    public List<Integer> getTaskListByUserRoleIdAndSectionId(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        return commonDAO.getTaskListByUserRoleIdAndSectionId(userRoleId, subSectionOrSectionId, taskId, userRolePermissionType);
    }

    @Override
    @Transactional
    public List<Integer> getTaskListByUserRoleIdSubSectionId(int userRoleId, int subSectionOrSectionId, int taskId, int userRolePermissionType) throws HibernateException, SQLException, Exception {
        return commonDAO.getTaskListByUserRoleIdSubSectionId(userRoleId, subSectionOrSectionId, taskId, userRolePermissionType);
    }

}
