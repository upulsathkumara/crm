
package com.avn.affiniti.service.sectiontask;

import com.avn.affiniti.hibernate.model.Section;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author ASHOK
 */
public interface SectionTaskService {
    
    public JSONObject insertSectionTask(HttpServletRequest request, String username) throws HibernateException, SQLException, Exception;
    
    public JSONObject getSectionTaskById(Section section, String username) throws SQLException, Exception;
    
    public JSONObject UpdateSectionTask(HttpServletRequest request, String username) throws SQLException, Exception;
    
    public JSONObject getAllTasks() throws HibernateException, SQLException, Exception;
    
    public JSONObject getSectionTaskTableData(HttpServletRequest request, String username) throws HibernateException, SQLException, Exception;
    
    public void loadPageComponent(Map<String, Object> model) throws SQLException;
}
