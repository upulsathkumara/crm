package com.avn.affiniti.service.sectiontask;

import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.sectiontask.SectionTaskDAO;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Sectiontask;
import com.avn.affiniti.hibernate.model.SectiontaskId;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASHOK
 */
@Service("sectionTaskService")
public class SectionTaskServiceImpl implements SectionTaskService {

    @Autowired
    SectionTaskDAO sectionTaskDAO;

    @Autowired
    CommonDAO commonDAO;

    @Override
    @Transactional
    public JSONObject insertSectionTask(HttpServletRequest request, String username) throws SQLException, Exception {

        JSONObject response = new JSONObject();
        try {
            String requestInfo = request.getParameter("requestInfo");
            String action = request.getParameter("action");
            JSONArray tasks = new JSONArray(requestInfo);
            
            for (int count = 0; tasks.length() > count; count++) {
                
                SectiontaskId sectiontaskId = new SectiontaskId();
                sectiontaskId.setSectionid(Integer.parseInt(tasks.getJSONObject(count).getString("sectionid")));
                sectiontaskId.setTaskid(Integer.parseInt(tasks.getJSONObject(count).getString("taskid")));
                
                Sectiontask sectionTask = new Sectiontask();
                sectionTask.setId(sectiontaskId);
                sectionTask.setUrl(tasks.getJSONObject(count).getString("url"));
                sectionTask.setCreateddatetime(new Date());
                sectionTask.setLastupdateddatetime(new Date());
                sectionTask.setCreateduser(username);

                boolean status = sectionTaskDAO.insertSectionTask(sectionTask);

                if (!status) {
                    sectionTaskDAO.insertAudittrace(sectionTask, action, tasks, username);
                    response.put("status", 500);
                    response.put("success", false);
                    response.put("messageService", "Un-successfull");
                    break;
                }
                response.put("status", 200);
                response.put("success", true);
                response.put("messageService", "Section Tasks Added Successfully");
            }
        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
            response.put("status", 500);
            response.put("success", false);
            response.put("messageService", "Un-successfull");
        }
        return response;
    }

    @Override
    @Transactional
    public JSONObject UpdateSectionTask(HttpServletRequest request, String username) throws SQLException, Exception {

        SectiontaskId sectiontaskId = new SectiontaskId();        
        sectiontaskId.setSectionid(Integer.parseInt(new JSONArray(request.getParameter("requestInfo")).getJSONObject(0).get("sectionid").toString()));
        sectiontaskId.setTaskid(Integer.parseInt(new JSONArray(request.getParameter("requestInfo")).getJSONObject(0).get("taskid").toString()));       
        Section section = new Section();
        section.setSectionid(sectiontaskId.getSectionid());
        boolean status = sectionTaskDAO.deleteSectionTask(section);

        JSONObject response = new JSONObject();
        if (status) {
            response = this.insertSectionTask(request, username);
        }

        return response;
    }

    @Override
    @Transactional
    public JSONObject getSectionTaskById(Section section, String username) throws SQLException, Exception {

        JSONObject response = new JSONObject();
        Sectiontask sectionTask = new Sectiontask();
        sectionTask.setSection(section);
        try {            
            JSONArray sectionInfo = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTIONBYID, section.getSectionid()));
            sectionInfo = Common.removeDropDownElementFromJsonArray("", sectionInfo);
            response.put("sectionInfo", sectionInfo);
            
            JSONArray urlInfo = new JSONArray(sectionTaskDAO.getUrlBySectionId(section.getSectionid()));
            sectionTaskDAO.insertAudittrace(sectionTask, "view", null, username);
            response.put("urlList", urlInfo);
            
            response.put("status", 200);
            response.put("success", true);
            response.put("messageService", "Successfull");

        } catch (Exception ex) {

            response.put("status", 500);
            response.put("success", false);
            response.put("messageService", "Un-successfull");
            Loggers.LOGGER_FILE.error(ex);
        }
        return response;
    }

    @Override
    @Transactional
    public JSONObject getAllTasks() throws HibernateException, SQLException, Exception {

        JSONObject details = new JSONObject();
        try {

            JSONArray rows = commonDAO.getJSONArrayDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_ALLSECTIONTASKS);

            rows = Common.removeDropDownElementFromJsonArray("", rows);

            details.put("status", 200);
            details.put("success", true);
            details.put("data", rows);
            details.put("messageService", "Successfull");

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("messageService", ex);
            details.put("status", 500);
            details.put("success", false);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("messageService", ex);
            details.put("status", 500);
            details.put("success", false);
        }
        return details;
    }

    @Override
    @Transactional
    public JSONObject getSectionTaskTableData(HttpServletRequest request, String username) throws HibernateException, SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            Sectiontask parameters = this.getParameters(request);//Setting up searched Section Task ID

            iTotalRecords = sectionTaskDAO.getTableDataCount(parameters);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = sectionTaskDAO.getTableData(parameters, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
            }

            
            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
            sectionTaskDAO.insertAudittrace(parameters, "search", null, username);

//            }
//            }
        } catch (Exception ex) {            
            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
            Loggers.LOGGER_FILE.error(ex);
        }

        return jsonResponse;
    }

    private Sectiontask getParameters(HttpServletRequest request) throws Exception {
        Sectiontask parameters = new Sectiontask();
        Section section = new Section();
        Task task = new Task();

        if (!request.getParameter("searchoptionID").equals("") && request.getParameter("searchoptionID") != null) {
            section.setSectionid(Integer.parseInt(request.getParameter("searchoptionID")));
            task.setTaskid(Integer.parseInt(request.getParameter("searchoptionID")));
        } else {
            section.setSectionid(null);
            task.setTaskid(null);
        }
        if (!request.getParameter("searchoptionDES").equals("")) {
            section.setDescription(request.getParameter("searchoptionDES"));
            task.setDescription(request.getParameter("searchoptionDES"));
        } else {
            section.setDescription("");
            task.setDescription("");
        }

        parameters.setSection(section);
        parameters.setTask(task);

        return parameters;
    }
    

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) throws SQLException{
        Map<String, String> sectionid = commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_NOTASSIGNSECTIONS);
        model.put("selectIdList", sectionid);
    }
}
