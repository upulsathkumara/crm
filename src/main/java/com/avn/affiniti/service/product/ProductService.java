/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.product;

import com.avn.affiniti.hibernate.model.Product;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author acer
 */
public interface ProductService {

//    public JSONObject getProductByProductCategory(String productCategoryId);

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException;

    public void loadStatusComponent(Map<String, Object> model) throws SQLException, HibernateException;

    public JSONObject createOrUpdateProduct(Product product, String createdUser, String input, String id, String setSelectFunction) throws Exception;

    public JSONObject getProductByProductCategoryId(int productId,HttpSession session);

    public JSONObject getProductTableData(HttpServletRequest request,String createdUser) throws SQLException, Exception;
    
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

}
