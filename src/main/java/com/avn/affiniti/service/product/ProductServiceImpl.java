/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.product;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.product.ProductDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Product;
import com.avn.affiniti.hibernate.model.Productcategory;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author :acer
 * @Document :ProductServiceImpl
 * @Created on:Sep 4, 2017,10:15:35 AM
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private StatusDAO statusDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    //loading data from table
//    @Override
//    public JSONObject getProductByProductCategory(String productCategoryId) {
//
//        JSONArray array = new JSONArray();
//        JSONObject output = new JSONObject();
//
//        
//        try {
//            for (Product p : productDAO.getProductByProductCategory(productCategoryId)) {
//                //System.out.println("product---------------------------------"+p.toString().);
//                JSONObject object = new JSONObject();
//                object.put("actions", p.getProductid().toString());
//                object.put("productid", p.getProductid().toString());
//                object.put("productname", String.valueOf(p.getDescription()));
//                object.put("productcategory", p.getProductcategory().getDescription().toString());
//
////                if (p.getStatus().getDescription() == null) {
////                    object.put("status", "N/A");
////                }
////                else{
////                   object.put("status", String.valueOf(p.getStatus().getDescription())); 
////                }
//                if (p.getStatus()== null){
//                    object.put("status", "N/A"); // created time
//                } else {
//                    object.put("status", String.valueOf(p.getStatus().getDescription())); // created time
//                }
//               //object.put("status", String.valueOf(p.getStatus().getDescription()));
//                //Common.replaceEmptyorNullStringToNA()
//                object.put("lastupdatedtime", String.valueOf(p.getLastupdateddatetime()));
//                object.put("createdtime", String.valueOf(p.getCreatedatetime()));
//                object.put("createduser", String.valueOf(p.getCreateduser()));
//                array.put(object);
//
//                output.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
//                output.put("success", true);
//                output.put("message", "");
//            }
//        } catch (Exception ex) {
//            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("message", "ERROR");
//            ex.printStackTrace();
//        }
//        output.put("data", array);
//        return output;
//    }
    @Override
    @Transactional
    public JSONObject getProductTableData(HttpServletRequest request, String createdUser) throws SQLException, Exception {

//        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
        String ViewStatus = "active";
        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        String page = "Product";
        String task = "";
        String description = "";
        String affectedId = "";

        try {
//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            String parameters = request.getParameter("searchoptionID");//Setting up searched Section Task ID
            if (parameters.equals("") || parameters.equals("--Select--")) {
                task = "Table View";
                description = "Table View all details in Product Table";
                affectedId = "";
            } else {

                Productcategory productcategory = productDAO.getProductCategoryByCategoryId(Integer.parseInt(String.valueOf(parameters)));
                task = "Table Search";
                description = "Search Details in Product Table by '" + productcategory.getDescription() + "' Category";
                affectedId = parameters;

            }
            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

            Productcategory productcategory = new Productcategory();
            if (!(parameters.trim()).isEmpty()) {
                productcategory.setProductcategoryid(Integer.parseInt(parameters));
            } else {
                productcategory = null;
            }

            iTotalRecords = productDAO.getTableDataCount(productcategory);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = productDAO.getTableData(productcategory, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);
//                list = productDAO.getTableData(productcategory, param.iDisplayLength, param.iDisplayStart);
//                for (Product product : list) {
//                    String action = "<div class=\"row\">"
//                            + "<div class=\"col-xs-3\"><a href='#' id='" + product.getProductid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
//                            + "<div class=\"col-xs-3\"><a href='#' id='" + product.getProductid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
//                            + "</div>";
//                    JSONObject object = new JSONObject();
//                    object.put("action", action);
//                    object.put("productid", (product.getProductid()) != null ? product.getProductid() : "N/A");
//                    object.put("productname", product.getDescription().equals("") ? "N/A" : product.getDescription());
//                    System.out.println("ssss" + product.getDescription());
//                    object.put("productcategory", product.getProductcategory().getDescription().equals("") ? "N/A" : product.getProductcategory().getDescription());
//
//                    object.put("status", product.getStatus().getDescription());
//                    object.put("lastupdatedtime", String.valueOf(product.getLastupdateddatetime()));
//                    object.put("createdtime", String.valueOf(product.getCreatedatetime()));
//                    object.put("createduser", String.valueOf(product.getCreateduser()));

//                    object.put("sectionlevel", (String.valueOf(section.getSectionlevel())) != null ? section.getSectionlevel() : "N/A");
//                    try {
//                        object.put("parentSection", section.getSection().getDescription());
//                    } catch (NullPointerException e) {
//                        object.put("parentSection", "N/A");
//                    }
//                    object.put("sectionIcon", (section.getIcon()) != null ? section.getIcon() : "N/A");
//                    object.put("statusDes", (section.getStatus().getDescription()) != null ? section.getStatus().getDescription() : "N/A");
//                    rows.put(object);
//                }
            }

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("messageService", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("messageService", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
            throw e;
        }

        return jsonResponse;
    }

    //Loading the product category dropdown
    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException {
        model.put("productList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITY_DROPDOWN_PRODUCTCATEGORY)));

    }

    //Loading the status  dropdown
    @Override
    @Transactional
    public void loadStatusComponent(Map<String, Object> model) throws SQLException, HibernateException {
        //model.put("productStatusList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITY_DROPDOWN_PRODUCTSTATUSCATEGORY)));
        model.put("productStatusList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY,
                MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT
        )));
    }

    @Override
    @Transactional
    public JSONObject createOrUpdateProduct(Product product, String createdUser, String input, String id, String setSelectFunction) throws Exception {
        String page = "Product";
        String task = "";
        String affectedId = "";
        String description = "";
        boolean emptyTasks = false;

        int product1 = product.getProductcategory().getProductcategoryid();
        Productcategory pc = productDAO.getProductCategoryByCategoryId(Integer.parseInt(String.valueOf(product1)));

        Status status = statusDAO.getStatsuById(product.getStatus().getStatusid());

        // Integer productcategoryid = product.getProductid();
        // System.out.println("input" + input);
        if (id != "null") {

            description = "Product table is Updated.-Product ID-  " + id + "-" + "product category of '" + pc.getDescription() + " ' Product Name of ' " + product.getDescription() + " ' And Status of ' " + status.getDescription() + " '";
            task = "Update";
            affectedId = String.valueOf(id);
            Product tempProduct = productDAO.getProductById(Integer.parseInt(id));
            product.setLastupdateddatetime(new Date());
            product.setCreateduser(tempProduct.getCreateduser());
            product.setCreatedatetime(tempProduct.getCreatedatetime());
            productDAO.createProduct(product, setSelectFunction);

        } else {
            task = "Add";

//affectedId = String.valueOf(id);
            description = "Added new recode into Product table by product category of '" + pc.getDescription() + " ' Product Name of ' " + product.getDescription() + " ' And Status of ' " + status.getDescription() + " '";
            // product.setCreateddatetime(new Date());
            product.setCreatedatetime(new Date());
            product.setCreateduser(createdUser);
            product.setLastupdateddatetime(new Date());
            productDAO.createProduct(product, setSelectFunction);

        }

//        try {
        //  product.setLastupdateddatetime(new Date());
        // product.setCreateduser(createdUser);
        // productDAO.createOrUpdateProduct(product);
//        } catch (Exception ex) {
//            throw new RuntimeException();
//            Logger.getLogger(ProductServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
        JSONObject message = new JSONObject();
//        message.put("message", "Product saved");
        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        return message;

    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    @Transactional
    public JSONObject getProductByProductCategoryId(int productId,HttpSession session) {
        JSONObject object = new JSONObject();
        JSONObject output = new JSONObject();

        String page = "Product";
        String task = "";
        String affectedId = "";
        String description = "";
        //String createdUser = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        output.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("success", true);
        output.put("message", "");

        try {
            Product p = productDAO.getProductById(productId);

            object.put("id", p.getProductid().toString());
            object.put("description", p.getDescription().toString());

            object.put("productcategory", p.getProductcategory().getProductcategoryid().toString());
            object.put("statusid", String.valueOf(p.getStatus().getStatusid()));
            object.put("sortid", String.valueOf(p.getSortid()));

            object.put("lastupdatedtime", p.getLastupdateddatetime().toString());
            object.put("createdtime", String.valueOf(p.getCreatedatetime()));
            object.put("createduser", String.valueOf(p.getCreateduser()));

            task = "View";

//affectedId = String.valueOf(id);
            int product1 = p.getProductcategory().getProductcategoryid();
            Productcategory pc = productDAO.getProductCategoryByCategoryId(Integer.parseInt(String.valueOf(product1)));

            Status status = statusDAO.getStatsuById(p.getStatus().getStatusid());
            description = "Viewed  Single Recorde  in Product Table Product ID of '" + p.getProductid()
                    + " '  Product Name of ' " + p.getDescription() + " ' Product category of ' " + pc.getDescription()
                    + " ' " + " 'And Status of ' " + status.getDescription() + " '";
            // +pc.getDescription() + " ' Product Name of ' " + product.getDescription() + " ' And Status of ' " + status.getDescription() + " '";
            // product.setCreateddatetime(new Date());

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

        } catch (HibernateException ex) {
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", "Error : Database Error");
        } catch (Exception ex) {
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", "Error : Error");
        }

        output.put("data", object);

        return output;
    }
}
