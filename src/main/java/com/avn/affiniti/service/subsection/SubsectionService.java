/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.subsection;

import com.avn.affiniti.hibernate.model.Subsection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author chandima
 */
public interface SubsectionService {

    public JSONObject getSubsectionList(HttpServletRequest request, int max) throws Exception;

    public JSONObject getSections() throws Exception;

    public JSONObject getStatus() throws Exception;

    public JSONArray getSubsectionListById(int id) throws Exception;

    public JSONObject getSubsectionListByDescription(String description) throws Exception;

    public JSONObject addSubsection(com.avn.affiniti.model.subsection.Subsection subsection, String setSelectFunction) throws Exception;

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException;

    public JSONObject getSubsectionById(String parameter) throws Exception;

    public JSONObject updateSubsection(com.avn.affiniti.model.subsection.Subsection subsection, String setSelectFunction);

    public JSONObject getSubectionTableData(HttpServletRequest request) throws HibernateException, SQLException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws SQLException, HibernateException;
}
