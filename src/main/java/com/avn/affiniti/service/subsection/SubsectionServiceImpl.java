/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.subsection;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.section.SectionDAO;
import com.avn.affiniti.dao.subsection.SubsectionDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chandima
 */
@Service("subsectionService")
public class SubsectionServiceImpl implements SubsectionService {

    @Autowired
    SubsectionDAO subsectionDAO;
    @Autowired
    private CommonDAO commonDAO;
    @Autowired
    HttpSession session;
    @Autowired
    private AuditTraceDAO auditTraceDAO;
    @Autowired
    private SectionDAO sectionDAO;

    @Override

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException {
        model.put("sectionList", commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTION));
        model.put("statuslist", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY, MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT)));
    }

    @Override
    @Transactional
    public JSONObject getSections() throws Exception {
        JSONObject response = new JSONObject();
        JSONArray array = commonDAO.getJSONArrayDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTION);
        response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        response.put("message", "Response recieved success");
        response.put("success", true);
        response.put("data", array);
        return response;
    }

    @Override
    @Transactional
    public JSONObject getStatus() throws Exception {
        JSONArray array = new JSONArray();
        JSONObject response = new JSONObject();
        List<Status> statuses = subsectionDAO.getStatusList();
        for (Status status : statuses) {
            JSONObject object = new JSONObject();
            object.put("STATUSID", status.getStatusid());
            object.put("DESCRIPTION", status.getDescription());
            array.put(object);
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("message", "Response recieved success");
            response.put("success", true);
            response.put("data", array);
        }
        return response;
    }

    @Override
    @Transactional
    public JSONObject getSubsectionList(HttpServletRequest request, int max) throws Exception {
        JSONArray array = new JSONArray();
        JSONObject response = new JSONObject();
        List<Subsection> subsections = subsectionDAO.getSubsectionList(max);
        for (Subsection subsection : subsections) {
            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";
            JSONObject object = new JSONObject();
            object.put("subsectionId", subsection.getSubsectionid());
            object.put("subsectionDes", subsection.getDescription());
            object.put("sectionDes", subsection.getSection().getDescription());  // sub section   
            object.put("statusDes", subsection.getStatus().getDescription()); // user role
            object.put("url", subsection.getUrl()); // section
            object.put("clickable", subsection.isClickable());  // last updated time
            if (subsection.getIcon() == null || subsection.getIcon().equals("")) {
                object.put("icon", "N/A"); // created time
            } else {
                object.put("icon", subsection.getIcon()); // created time
            }
            object.put("action", action); // created user    
            array.put(object);

            //Setting the JSON Object
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("message", "Response recieved success");
            response.put("success", true);
            response.put("data", array);
        }
        return response;
    }

    /**
     * **************************************************************************************
     * @param request
     * @return
     * @throws java.sql.SQLException
     */
    @Override
    @Transactional
    public JSONObject getSubectionTableData(HttpServletRequest request) throws HibernateException, SQLException, Exception {

        String ViewStatus = "active";
        String UpdateStatus = "active";

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }

        Subsection parameters = this.getParameters(request);//Setting up searched Section Task ID
        iTotalRecords = subsectionDAO.getTableDataCount(parameters);//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = subsectionDAO.getTableData(parameters, param.iDisplayLength, param.iDisplayStart);
            rows = new JSONArray(list);
        }
        jsonResponse.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        jsonResponse.put("success", true);
        jsonResponse.put("messageService", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

        return jsonResponse;
    }

    private Subsection getParameters(HttpServletRequest request) throws Exception {

        Subsection subsection = new Subsection();
        String searchOption = request.getParameter("searchOption");
        String searchValue = request.getParameter("searchValue");
        String sectionValue = request.getParameter("sectionValue");

        if (searchOption.equals("section")) {
            subsection.setIcon("section");
            subsection.setDescription(searchValue);
        } else if (searchOption.equals("subsectionDes")) {
            subsection.setDescription(searchValue);
        } else if (sectionValue != null && !sectionValue.equals("")) {
            Section section = sectionDAO.getSectionBySectionId(sectionValue);
            subsection.setSection(section);
        } else {
            subsection = null;
        }

        return subsection;
    }

    /**
     * **************************************************************************************
     * @param id
     * @return
     */
    @Override
    @Transactional
    public JSONArray getSubsectionListById(int id) throws Exception {
        JSONArray array = new JSONArray();
        List<Subsection> subsections = subsectionDAO.getSubsectionListById(id);

        String page = "Subsection";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        for (Subsection subsection : subsections) {
//                String action = "<div class=\"row\">"
//                        + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
//                        + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
//                        + "</div>";
            JSONObject object = new JSONObject();
            object.put("subsectionId", subsection.getSubsectionid());
            object.put("subsectionDes", subsection.getDescription()); //task     
            object.put("sectionDes", subsection.getSection().getDescription());  // sub section   
            object.put("statusDes", subsection.getStatus().getDescription()); // user role
            object.put("url", subsection.getUrl()); // section
            object.put("clickable", subsection.isClickable());  // last updated time
            object.put("icon", subsection.getIcon()); // created time
//                object.put("action", action); // created user     

            task = "View";
            //Status status1 = statusDAO.getStatsuById(section.getStatus().getStatusid());

            description = "Viewed  Single Recorde  in Subsection Table Subsection  ID of '" + subsection.getSubsectionid()
                    + " '  Description of ' " + subsection.getDescription() + " ' Status of ' " + subsection.getStatus().getDescription()
                    + " ' " + " ' Icon of ' " + subsection.getIcon() + " ' URL of ' " + subsection.getUrl() + " ' "
                    + " ' Clickable of ' " + subsection.isClickable() + " ' And SortId of ' " + subsection.getSortid() + " ' ";

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

            array.put(object);
        }

        return array;
    }

    @Override
    @Transactional
    public JSONObject getSubsectionListByDescription(String description) throws Exception {
        JSONArray array = new JSONArray();
        JSONObject response = new JSONObject();
        List<Subsection> subsections = subsectionDAO.getSubsectionListByDescription(description);
        for (Subsection subsection : subsections) {
            String action = "<div class=\"row\">"
                    + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                    + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                    + "</div>";
            JSONObject object = new JSONObject();
            object.put("subsectionId", subsection.getSubsectionid());
            object.put("subsectionDes", subsection.getDescription()); //task     
            object.put("sectionDes", subsection.getSection().getDescription());  // sub section   
            object.put("statusDes", subsection.getStatus().getDescription()); // user role
            object.put("url", subsection.getUrl()); // section
            object.put("clickable", subsection.isClickable());  // last updated time
            if (subsection.getIcon() == null || subsection.getIcon() == "") {
                object.put("icon", "N/A"); // created time
            } else {
                object.put("icon", subsection.getIcon()); // created time
            }
            object.put("action", action); // created user     
            array.put(object);
        }
        response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        response.put("message", "Response recieved success");
        response.put("success", true);
        response.put("data", array);

        return response;
    }

    @Override
    @Transactional
    public JSONObject addSubsection(com.avn.affiniti.model.subsection.Subsection subsection, String setSelectFunction) throws Exception {
        Subsection hbSubsection = new Subsection();
        JSONObject response = new JSONObject();

        if (subsection.getSubsectionId() != null && subsection.getSubsectionId() != "") {
            hbSubsection.setSubsectionid(Integer.parseInt(subsection.getSubsectionId()));
        }

        hbSubsection.setClickable(subsection.isClickableurl());
        hbSubsection.setDescription(subsection.getSubsectionDes());
        hbSubsection.setIcon(subsection.getIcon());

        //get section from sectionId and set in the hibernate model
        Section section = new Section();
        section.setSectionid(Integer.parseInt(subsection.getSectionid()));
        hbSubsection.setSection(section);
        hbSubsection.setSortid(Integer.parseInt(subsection.getSortid()));

        //Setting up the status object 
        Status status = new Status();
        status.setStatusid(Integer.parseInt(subsection.getStatusid()));
        hbSubsection.setStatus(status);

        hbSubsection.setUrl(subsection.getUrl());
        hbSubsection.setCreateddatetime(new Date());
        hbSubsection.setLastupdateddatetime(new Date());
        hbSubsection.setCreateduser((String) session.getAttribute("username"));

        if (subsectionDAO.addSubsection(hbSubsection, setSelectFunction)) {
            String page = "Subsection Create";
            String affectedId = "";
            String task = "Create Subsection";
            String description = "Subsection Description "
                    + hbSubsection.getDescription() + "Subsection Create date "
                    + hbSubsection.getCreateddatetime() + "Subsection last updated date "
                    + hbSubsection.getLastupdateddatetime();

            String createdUser = String.valueOf(session.getAttribute("username"));
            this.insertAuditTrace(page, affectedId, task, description, createdUser);
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Subsection"));
            response.put("success", true);
            response.put("data", hbSubsection);
        }

        return response;
    }

    @Override
    @Transactional
    public JSONObject updateSubsection(com.avn.affiniti.model.subsection.Subsection subsection, String setSelectFunction) {
        Subsection hbSubsection = new Subsection();
        JSONObject response = new JSONObject();
        try {
            if (subsection.getSubsectionId() != null && !subsection.getSubsectionId().equals("")) {
                hbSubsection.setSubsectionid(Integer.parseInt(subsection.getSubsectionId()));
            }

            hbSubsection.setClickable(subsection.isClickableurl());
            hbSubsection.setDescription(subsection.getSubsectionDes());
            hbSubsection.setIcon(subsection.getIcon());

            //get section from sectionId and set in the hibernate model
            Section section = new Section();
            section.setSectionid(Integer.parseInt(subsection.getSectionid()));
            hbSubsection.setSection(section);
            hbSubsection.setSortid(Integer.parseInt(subsection.getSortid()));

            //setting status objest
            Status status = new Status();
            status.setStatusid(Integer.parseInt(subsection.getStatusid()));
            hbSubsection.setStatus(status);

            hbSubsection.setUrl(subsection.getUrl());

            hbSubsection.setCreateddatetime(new Date());
            hbSubsection.setLastupdateddatetime(new Date());
            hbSubsection.setCreateduser((String) session.getAttribute("username"));

            if (subsectionDAO.addSubsection(hbSubsection, setSelectFunction)) {
                response.put("status", 200);
                response.put("message", "Response recieved success");
                response.put("success", true);
                response.put("data", hbSubsection);
            }

        } catch (Exception ex) {
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("success", false);
            response.put("message", "Response recieved failure");
            response.put("data", "");
            Loggers.LOGGER_FILE.error(ex);
        }
        return response;
    }

    @Override
    @Transactional
    public JSONObject getSubsectionById(String parameter) throws Exception {
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();

        String page = "Sub section";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        JSONObject response = new JSONObject();
        int id = 0;
        if (!parameter.equals("") && parameter != null) {
            id = Integer.parseInt(parameter);
        }

        Subsection subsection = subsectionDAO.getSubsectionById(id);
//            String action = "<div class=\"row\">"
//                    + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
//                    + "<div class=\"col-xs-3\"><a id='" + subsection.getSubsectionid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
//                    + "</div>";
        object.put("subsectionId", subsection.getSubsectionid());
        object.put("subsectionDes", subsection.getDescription()); //description     
        object.put("sectionId", subsection.getSection().getSectionid());  // sub section id   
        object.put("statusId", subsection.getStatus().getStatusid()); // status
        object.put("statusDes", subsection.getStatus().getDescription()); // status
        object.put("sectionDes", subsection.getSection().getDescription());  // sub section id   
        object.put("statusDes", subsection.getStatus().getDescription()); // status
        object.put("url", subsection.getUrl()); // URL
        object.put("clickable", subsection.isClickable());  // Clickable
        object.put("icon", subsection.getIcon()); // icon
        object.put("sortid", subsection.getSortid());

        task = "View";

        description = "Viewed  Single Recorde  in Subsection Table Subsection  ID of '" + subsection.getSubsectionid()
                + " '  Description of ' " + subsection.getDescription() + " ' Status of ' " + subsection.getStatus().getDescription()
                + " ' " + " ' Clickable   of ' " + subsection.isClickable() +  " ' "
                + " ' URL of ' " + subsection.getUrl() + " ' Icon of ' " + subsection.getIcon() + " ' And SortId of ' " + subsection.getSortid() + " ' ";

        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        array.put(object);

        response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        response.put("message", "Response recieved success");
        response.put("success", true);
        response.put("data", array);

        return response;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws SQLException, HibernateException {
        Audittrace auditTrace = new Audittrace();
        auditTrace.setAffectedpage(page);
        auditTrace.setTask(task);
        auditTrace.setCreateduser(createdUser);
        auditTrace.setCreatedatetime(new Date());
        auditTrace.setLastupdateddatetime(new Date());
        auditTrace.setAfectedid(affectedId);
        auditTrace.setDescription(description);
        auditTraceDAO.insertAuditTrace(auditTrace);
    }

}
