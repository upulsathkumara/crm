package com.avn.affiniti.service.subsectiontask;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.section.SectionDAO;
import com.avn.affiniti.dao.subsection.SubsectionDAO;
import com.avn.affiniti.dao.subsectiontask.SubSectionTaskDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.hibernate.model.Subsection;
import com.avn.affiniti.hibernate.model.Subsectiontask;
import com.avn.affiniti.hibernate.model.SubsectiontaskId;
import com.avn.affiniti.hibernate.model.Task;
import com.avn.affiniti.model.subsectiontask.subsectiontask;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kaushan Fernando
 * @Document SubSectionTaskServiceImpl
 * @Created on: Sep 25, 2017, 11:19:26 AM
 */
@Service("subSectionTaskService")
public class SubSectionTaskServiceImpl implements SubSectionTaskService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private SectionDAO sectionDAO;

    @Autowired
    private SubSectionTaskDAO subSectionTaskDAO;

    @Autowired
    private SubsectionDAO subsectionDAO;

    @Autowired
    private AuditTraceDAO auditTraceDAO;

    @Override
    @Transactional
    public JSONObject getTaskLIstAndSubSectionsBySectionId(int sectionId) throws HibernateException, SQLException, Exception {

        JSONArray data;

        JSONObject objData = new JSONObject();

        data = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUBSECTIONS_BY_SECTION_ID,
                sectionId
        ));

        objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        objData.put("success", true);
        objData.put("message", "Successfull");
        objData.put("data", data);

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getTaskLIstBySectionIdAndSubSectionId(int sectionId, int subSectionId) throws HibernateException, SQLException, Exception {
        JSONArray data;
        JSONArray dataToRemoveSelect;
        String elementIdToRemove = "";

        JSONObject objData = new JSONObject();

        dataToRemoveSelect = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNED_TASKS_BY_SECTION_ID_AND_SUBSECTION_ID,
                sectionId, subSectionId
        ));
        data = Common.removeDropDownElementFromJsonArray(elementIdToRemove, dataToRemoveSelect);

        objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        objData.put("success", true);
        objData.put("message", "Successfull");
        objData.put("data", data);

        return objData;
    }

    @Override
    @Transactional
    public JSONObject getSubSectionTaskTableTableData(HttpServletRequest request, String createdUser) throws HibernateException, SQLException, JSONException, Exception {

        //        JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
//        SectionTask sectiontask = new SectionTask();
//        SectionTask userroletaskview = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_VIEW_SECTIONTASK_SUBSECTION_ID), objList);
//        SectionTask userroletaskedite = checkPrivilage_btn(sectiontask, String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_CCL_CODE_EDIT_SECTIONTASK_SUBSECTION_ID), objList);
//        String ViewStatus = "active";
//        String UpdateStatus = "active";
//        if (userroletaskview.isView_btn()) {
//            ViewStatus = "disabled";
//        } else {
//            ViewStatus = "active";
//        }
//        if (userroletaskedite.isEdit_btn()) {
//            UpdateStatus = "disabled";
//        } else {
//            UpdateStatus = "active";
//        }
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();
        try {
            String page = "SubSectionTask";
            String task = "";
            String description = "";
            String affectedId = "";
            String idType = "";
            String id = "";

//            boolean privilage = commendaoimpl.checkPrivilage(String.valueOf(MasterDataVarList.CCL_CODE_SECTIONTASK_ID), String.valueOf(MasterDataVarList.CCL_CODE_SEARCH_SECTIONTASK_SUBSECTION_ID), MasterDataVarList.CCL_CODE_SEARCH, objList);
//            if (privilage) {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }
//            if (Boolean.valueOf(request.getParameter("search"))) {

            String sectionId = request.getParameter("sectionsearchid");
            String subSectionId = request.getParameter("subsectionsearchid");
            String taskId = request.getParameter("tasklistsearchid");

            //getInventoryCategoryById(hardwareitem.getInventorycategory().getInventorycategoryid());
            JSONObject object = new JSONObject();
            object.put("sectionid", sectionId);
            object.put("subsectionid", subSectionId);
            object.put("taskid", taskId);

            if ((sectionId.equals("") || sectionId.equals("--Select--")) && (subSectionId.equals("") || subSectionId.equals("--Select--")) && (taskId.equals("") || taskId.equals("--Select--"))) {
                task = "Table View";
                description = "Table View all details in subsectiontask ";
                affectedId = "";
                idType = "section";
                id = sectionId;

            } else if (!(sectionId.equals("") || sectionId.equals("--Select--"))) {
                Section sid = sectionDAO.getSectionBySectionId(sectionId);

                task = "Table Search";
                description = "Table Search details in subsectiontask by section of " + sid.getDescription();
                //+ sectionId
                affectedId = sectionId;
                idType = "section";
                id = sectionId;

            } else if (!(subSectionId.equals("") || subSectionId.equals("--Select--"))) {
                Subsection sbid = subsectionDAO.getsubSectionBySubSectionId(subSectionId);

                //  getSectionBySectionId(sectionId);
                task = "Table Search";
                description = "Table Search details in subsectiontask by sub section of " + sbid.getDescription();
                affectedId = subSectionId;
                idType = "subsection";
                id = subSectionId;

            } else if (!(taskId.equals("") || taskId.equals("--Select--"))) {
                Task task1 = subsectionDAO.gettaskByTaskId(taskId);
                task = "Table Search";
                description = "Table Search details in subsectiontask by task of " + task1.getDescription();
                affectedId = taskId;
                idType = "task";
                id = taskId;

            } else {
                Section sid1 = sectionDAO.getSectionBySectionId(sectionId);
                Subsection sbid2 = subsectionDAO.getsubSectionBySubSectionId(subSectionId);
                Task task2 = subsectionDAO.gettaskByTaskId(taskId);
                task = "Table Search";
                description = "Table Search details in section of " + sid1.getDescription() + " : sub section of" + sbid2.getDescription() + " : task Id" + task2.getDescription();
                affectedId = sectionId;
                idType = "section";
                id = sectionId;

            }

            iTotalRecords = subSectionTaskDAO.getTableDataCount(id, object);//Total Records for searched option
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = subSectionTaskDAO.getTableData(id, object, param.iDisplayLength, param.iDisplayStart);
                rows = new JSONArray(list);

            }
            insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

            jsonResponse.put("status", 200);
            jsonResponse.put("success", true);
            jsonResponse.put("message", "Successfull");
            jsonResponse.put("aaData", rows);
            jsonResponse.put("sEcho", sEcho);
            jsonResponse.put("iTotalRecords", iTotalRecords);
            jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

//            }
//            }
        } catch (Exception e) {
            System.out.println("SSSSSS : " + e);

            jsonResponse.put("status", 500);
            jsonResponse.put("success", false);
            jsonResponse.put("message", "Un-successfull");
            jsonResponse.put("sEcho", 0);
            jsonResponse.put("iTotalRecords", 0);
            jsonResponse.put("iTotalDisplayRecords", 0);
        }

        return jsonResponse;
    }

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) {
        try {
            model.put("sectionList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SECTION,
                    MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
            )));
            model.put("subSectionList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_SUBSECTIONS,
                    MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
            )));
            model.put("taskList", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_TASKS,
                    MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE
            )));

            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            model.put("success", true);
            model.put("message", "Successfull");

        } catch (HibernateException | SQLException sqle) {
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Database Error");
        } catch (Exception e) {
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Error");
        }

    }

    @Override
    @Transactional("sessionFactoryTransaction")
    public JSONObject saveSubSectionTask(subsectiontask[] subSectionTask, List<Subsectiontask> listOfOldObject, String createdUser, String action) throws HibernateException, SQLException, JSONException, Exception {
        JSONObject object = new JSONObject();

        String page = "Subsectiontask";
        String task = "";
        String affectedId = "";
        String description = "";

        Date CreatedDate = new Date();
        Date lastUpdatedDate = new Date();

        for (subsectiontask subsectask : subSectionTask) {
            boolean saveURL = true;
            String newUrl = subsectask.getUrl();
            for (Subsectiontask oldSubSectionTask : listOfOldObject) {
                String oldUrl = oldSubSectionTask.getUrl();

                if (oldUrl.equals(newUrl)) {
                    saveURL = false;
                }
            }

            subsectiontask subsectiontaskModel = new subsectiontask();

            subsectiontaskModel.setSectionId(subsectask.getSectionId());
            int sectionId = Integer.parseInt(subsectiontaskModel.getSectionId());

            subsectiontaskModel.setSubSectionId(subsectask.getSubSectionId());
            int subSectionId = Integer.parseInt(subsectiontaskModel.getSubSectionId());

            subsectiontaskModel.setTaskId(subsectask.getTaskId());
            int taskId = Integer.parseInt(subsectiontaskModel.getTaskId());

            subsectiontaskModel.setUrl(subsectask.getUrl());
            String url = subsectiontaskModel.getUrl();

            if (action.equals("save")) {
                //Section sid = sectionDAO.getSectionBySectionId(sectionId);
                description = "Assigned a new task to subsectiontask.";
                //+ " sectionId : " + sectionId + " subSectionId : " + subSectionId;
                task = "assign";

            }

            if (action.equals("edit")) {

                for (Subsectiontask subsectaskOld : listOfOldObject) {

                    CreatedDate = subsectaskOld.getCreateddatetime();

                    lastUpdatedDate = subsectaskOld.getLastupdateddatetime();

                    description = "Updated task from subsectiontask.";
                    //by sectionId : " + sectionId + " subsectionId : " + subSectionId;

                    affectedId = String.valueOf(sectionId);

                    task = "update";

                }

            }
            if (saveURL) {

                Subsectiontask subSectionTaskToSave = new Subsectiontask();

                Section sectionHibModel = new Section();
                sectionHibModel.setSectionid(sectionId);
                subSectionTaskToSave.setSection(sectionHibModel);

                Subsection subSectionHibModel = new Subsection();
                subSectionHibModel.setSubsectionid(subSectionId);
                subSectionTaskToSave.setSubsection(subSectionHibModel);

                Task taskHibModel = new Task();
                taskHibModel.setTaskid(taskId);
                subSectionTaskToSave.setTask(taskHibModel);

                SubsectiontaskId subSecTaskId = new SubsectiontaskId();

                subSecTaskId.setSection(sectionId);
                subSecTaskId.setSubsection(subSectionId);
                subSecTaskId.setTask(taskId);

                subSectionTaskToSave.setId(subSecTaskId);
                subSectionTaskToSave.setUrl(url);
                subSectionTaskToSave.setCreateddatetime(CreatedDate);
                subSectionTaskToSave.setLastupdateddatetime(lastUpdatedDate);
                subSectionTaskToSave.setCreateduser(createdUser);

                object = subSectionTaskDAO.saveSubSectionTask(subSectionTaskToSave, action);

                if (object.getBoolean("status")) {
                    object.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                    object.put("success", true);
                    object.put("message", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Tasks"));
                } else {
                    object.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
                    object.put("success", false);
                }
            }

        }
        if (action.equals("edit")) {
            for (Subsectiontask oldSubSectionTask : listOfOldObject) {
                String oldUrl = oldSubSectionTask.getUrl();
                int sectionId = oldSubSectionTask.getSection().getSectionid();
                int subSectionId = oldSubSectionTask.getSubsection().getSubsectionid();
                int taskId = oldSubSectionTask.getTask().getTaskid();
                boolean deleteURL = true;
                for (subsectiontask subsectask : subSectionTask) {

                    String newUrl = subsectask.getUrl();
                    if (oldUrl.equals(newUrl)) {
                        deleteURL = false;
                    }
                }
                if (deleteURL) {
                    subSectionTaskDAO.deleteSubSectionTask(sectionId, subSectionId, taskId);
                }

            }
        }
        this.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        switch (action) {
            case "add":
                object.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("success", true);
                object.put("message", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Tasks"));
                break;
            case "edit":
                object.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("success", true);
                object.put("message", String.format(MessageVarList.UPDATED_SUCCESSFULLY, "Tasks"));
                break;
        }

        return object;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) throws Exception {

        Audittrace auditTrace = new Audittrace();

        auditTrace.setAffectedpage(page);
        auditTrace.setTask(task);
        auditTrace.setCreateduser(createdUser);
        auditTrace.setCreatedatetime(new Date());
        auditTrace.setLastupdateddatetime(new Date());
        auditTrace.setAfectedid(affectedId);
        auditTrace.setDescription(description);

        auditTraceDAO.insertAuditTrace(auditTrace);

    }

    @Override
    @Transactional
    public List<Subsectiontask> getOldSubSectionTaskObj(String sectionId, String subSectionId) throws HibernateException, SQLException, Exception {
        int sectionid = Integer.parseInt(sectionId);
        int subSectionid = Integer.parseInt(subSectionId);
        List<Subsectiontask> listOfOldObject = subSectionTaskDAO.getSubSectionTaskBySectionId(sectionid, subSectionid);
        return listOfOldObject;
    }

    @Override
    @Transactional
    public JSONObject getTaskLIstBySectionIdAndSubSectionIdOnEditAndView(int sectionId, int subSectionId, HttpSession session) throws HibernateException, SQLException, Exception {

        JSONArray data = new JSONArray();

        String page = "Sub section Task";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        JSONArray data2;
//        JSONArray data3;
        JSONArray dataToRemoveSelect;
//        JSONArray dataToRemoveSelect2;
        String elementIdToRemove = "";

        JSONObject objData = new JSONObject();
        List<Subsectiontask> listOfObject = subSectionTaskDAO.getSubSectionTaskBySectionId(sectionId, subSectionId);

        for (Subsectiontask subsectask : listOfObject) {
            JSONObject object = new JSONObject();
            int taskId = subsectask.getTask().getTaskid();
            String taskDescription = subsectask.getTask().getDescription();
            String URL = subsectask.getUrl();
            if (!(taskDescription.equals("--Select--"))) {
            }
            object.put("taskid", taskId);
            object.put("taskdescription", taskDescription);
            object.put("url", URL);

            task = "View";

            description = "Viewed  Single Recorde  in Subsection Task Table Subsection  ID of '" + subsectask.getSubsection().getSubsectionid()
                    + " '  Section of ' " + subsectask.getSection().getDescription() + " ' Sub Section of ' " + subsectask.getSubsection().getDescription()
                    + " ' " + " ' Task   of ' " + subsectask.getTask().getDescription()+ " ' "
                    + " ' URL of ' " + subsectask.getUrl() + " ' ";

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

            data.put(object);

        }

        dataToRemoveSelect = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_SUBSECTION_BY_SUBSECTION_ID,
                subSectionId
        ));

        data2 = Common.removeDropDownElementFromJsonArray(elementIdToRemove, dataToRemoveSelect);

//        dataToRemoveSelect2 = commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_ASSIGNED_TASKS_BY_SECTION_ID_AND_SUBSECTION_ID,
//                sectionId, subSectionId
//        ));
//        data3 = Common.removeDropDownElementFromJsonArray(elementIdToRemove, dataToRemoveSelect2);
        objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        objData.put("success", true);
        objData.put("message", "Successfull");
        objData.put("data", data);
        objData.put("data2", data2);
//        objData.put("data3", data3);

        return objData;
    }

}
