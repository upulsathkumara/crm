package com.avn.affiniti.service.subsectiontask;

import com.avn.affiniti.hibernate.model.Subsectiontask;
import com.avn.affiniti.model.subsectiontask.subsectiontask;
import java.util.Map;
import org.json.JSONObject;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;

/**
 *
 * @author Kaushan Fernando
 * @Document SubSectionTaskService
 * @Created on: Sep 25, 2017, 11:19:00 AM
 */
public interface SubSectionTaskService {

    public void loadPageComponent(Map<String, Object> model);

    public JSONObject getTaskLIstAndSubSectionsBySectionId(int sectionId) throws HibernateException, SQLException, Exception;

    public JSONObject getTaskLIstBySectionIdAndSubSectionId(int sectionId, int subSectionId) throws HibernateException, SQLException, Exception;

    public JSONObject getTaskLIstBySectionIdAndSubSectionIdOnEditAndView(int sectionId, int subSectionId,HttpSession session) throws HibernateException, SQLException, Exception;

    public JSONObject getSubSectionTaskTableTableData(HttpServletRequest request, String createdUser) throws HibernateException, SQLException, JSONException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser)throws Exception;

    public List<Subsectiontask> getOldSubSectionTaskObj(String sectionId, String subSectionId) throws HibernateException, SQLException, Exception;

    public JSONObject saveSubSectionTask(subsectiontask[] subSectionTask, List<Subsectiontask> listOfOldObject, String createdUser, String action) throws HibernateException, SQLException, JSONException, Exception;
}
