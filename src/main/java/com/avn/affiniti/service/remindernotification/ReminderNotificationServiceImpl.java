/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.remindernotification;

import com.avn.affiniti.controller.login.LoginController;
import com.avn.affiniti.dao.common.CommonDAOImpl;
import com.avn.affiniti.dao.remindernotification.ReminderNotificationDAO;
import com.avn.affiniti.hibernate.model.Remindernotification;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author :Gaje
 * @Document :ReminderNotificationServiceImpl
 * @Created on:Jan 23, 2018,11:21:06 AM
 */
@Service("reminderNotificationService")
public class ReminderNotificationServiceImpl implements ReminderNotificationService {

    @Autowired
    ReminderNotificationDAO remindernotificationDAO;

    @Autowired
    CommonDAOImpl commonDAOImpl;

    @Override
    public void loadPageComponent(Map<String, Object> model, HttpSession session) throws SQLException, HibernateException {
        //model.put("myOpenTicketCount", dashboardDAO.getOpenTicketCount(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME))));

    }

    @Override
    public long getReminderCount(String name, Date d1, Date d2) throws SQLException, Exception {
        long iTotalRecords = 0; // total number of records (unfiltered)
        iTotalRecords = remindernotificationDAO.getReminderCount(name, d1, d2);

        return iTotalRecords;

    }

    @Override
    @Transactional
    public String getReminderNotificationTableData(HttpServletRequest request, HttpSession session, String createdUser) throws SQLException, Exception {
        //String loadReminderNEW(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        Date d1 = commonDAOImpl.getCurentTimesStamp();
        Date d2 = commonDAOImpl.getCurentTimesStamp();
        d2.setTime(d2.getTime() + 10 * 60000);
        int iDisplayStart;
        int iTotalRecords = 0;
        int iDisplayLength = 10;
        int minCount = 0;
        int maxPages = 0;
        String content = request.getParameter("content");
        JSONObject data = new JSONObject(content);
        String name = (String) session.getAttribute("username");
        iTotalRecords = remindernotificationDAO.getAllCount(name, d1, d2);
        maxPages = (iTotalRecords / iDisplayLength);
        if (iTotalRecords % iDisplayLength > 0) {
            maxPages++;
        }
        if ((data.getInt("iDisplayStart") == 0)) {
            iDisplayStart = 0;
        } else {
            iDisplayStart = (10 * data.getInt("iDisplayStart"));
        }

        if (data.getInt("iDisplayLength") == 0) {
            iDisplayLength = 10;
        } else {
            iDisplayLength += (iDisplayLength * data.getInt("iDisplayLength"));
        }
        List< Remindernotification> reminderList = remindernotificationDAO.getTableData(iDisplayLength + iDisplayStart, iDisplayStart, name, d1, d2);
        List<JSONObject> jSONArrayList = new ArrayList<>();
        JSONObject NotifiReminder = new JSONObject();
        try {
            if (reminderList.size() > 0) {
                for (Remindernotification oneitemreminder : reminderList) {
                    JSONObject newNotifiReminder = new JSONObject();
                    newNotifiReminder.put("id", oneitemreminder.getId());
                    newNotifiReminder.put("description", oneitemreminder.getDescription());
                    newNotifiReminder.put("remindTime", oneitemreminder.getRemindertime());
                    newNotifiReminder.put("sourcetabelId", oneitemreminder.getSourcetableid());
                    newNotifiReminder.put("section", oneitemreminder.getSectionid());
                    newNotifiReminder.put("contextpath", request.getContextPath());
                    newNotifiReminder.put("maxPages", maxPages);
                    newNotifiReminder.put("viewState", oneitemreminder.getViewedstatus());
                    NotifiReminder.put("NewNotifiList", newNotifiReminder);

                    jSONArrayList.add(newNotifiReminder);

                }
            } else {

            }

        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return jSONArrayList.toString();
    }

    @Override
    public String getdeletenotification(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        String content = request.getParameter("content");
        JSONObject data = new JSONObject(content);
//        System.out.println("content " + content);
        List<JSONObject> jSONArrayList = new ArrayList<>();
        try {
            remindernotificationDAO.setDeleteStatus(String.valueOf(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_DELETE), data.getString("id"), data.getString("section"));

            Date d1 = commonDAOImpl.getCurentTimesStamp();
            Date d2 = commonDAOImpl.getCurentTimesStamp();
            d2.setTime(d2.getTime() + 10 * 60000);
            int iDisplayStart;
            int iTotalRecords = 0;
            int iDisplayLength = 10;
            int minCount = 0;
            int maxPages = 0;

            String name = (String) session.getAttribute("username");
            iTotalRecords = remindernotificationDAO.getAllCount(name, d1, d2);
            maxPages = (iTotalRecords / iDisplayLength);
            if (iTotalRecords % iDisplayLength > 0) {
                maxPages++;
            }
            if ((data.getInt("iDisplayStart") == 0)) {
                iDisplayStart = 0;
            } else {
                iDisplayStart = (10 * data.getInt("iDisplayStart"));
            }

            if (data.getInt("iDisplayLength") == 0) {
                iDisplayLength = 10;
            } else {
                iDisplayLength += (iDisplayLength * data.getInt("iDisplayLength"));
            }

            List< Remindernotification> reminderList = remindernotificationDAO.getTableData(iDisplayLength, iDisplayStart, name, d1, d2);

            JSONObject NotifiReminder = new JSONObject();

            if (reminderList.size() > 0) {
                for (Remindernotification oneitemreminder : reminderList) {
                    JSONObject newNotifiReminder = new JSONObject();
                    newNotifiReminder.put("id", oneitemreminder.getId());
                    newNotifiReminder.put("description", oneitemreminder.getDescription());
                    newNotifiReminder.put("remindTime", oneitemreminder.getRemindertime());
                    newNotifiReminder.put("sourcetabelId", oneitemreminder.getSourcetableid());
                    newNotifiReminder.put("section", oneitemreminder.getSectionid());
                    newNotifiReminder.put("contextpath", request.getContextPath());
                    newNotifiReminder.put("maxPages", maxPages);
                    newNotifiReminder.put("viewState", oneitemreminder.getViewedstatus());
                    NotifiReminder.put("NewNotifiList", newNotifiReminder);

                    jSONArrayList.add(newNotifiReminder);

                }
            } else {

            }

        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return jSONArrayList.toString();
    }
    
}
