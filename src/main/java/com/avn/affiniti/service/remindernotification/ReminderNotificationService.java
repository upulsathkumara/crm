/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.remindernotification;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONObject;

/**
 *
 * @author Gaje
 */
public interface ReminderNotificationService {
    
        public void loadPageComponent(Map<String, Object> model, HttpSession session) throws SQLException, HibernateException;

        public long getReminderCount(String name, Date d1, Date d2) throws SQLException, Exception;
        
        public String getReminderNotificationTableData(HttpServletRequest request,HttpSession session,String createdUser) throws SQLException, Exception;
   
        public String getdeletenotification(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception;

        

}
