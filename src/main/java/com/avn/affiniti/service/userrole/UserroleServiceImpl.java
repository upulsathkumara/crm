/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrole;

import com.avn.affiniti.dao.audittrace.AuditTraceDAO;
import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.status.StatusDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.userrole.UserRoleDAO;
import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Userrole;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chandima
 */
@Service("userroleService")
public class UserroleServiceImpl implements UserroleService {

    @Autowired
    private CommonDAO commonDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Autowired
    StatusDAO statusDAO;

    @Autowired
    HttpSession session;

    @Autowired
    AuditTraceDAO auditTraceDAO;

    @Autowired
    private SystemuserDAO systemuserDAO;

    @Override
    @Transactional
    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException {
        model.put("statuslist", commonDAO.getDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_STATUS_BY_CATRGORY, MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_DEFAULT)));
        model.put("userroletypelist", commonDAO.getDropdownValueList(DropdownSqlVarList.AFFINITI_DROPDOWN_USERROLETYPE));
    }

    @Override
    @Transactional
    public JSONObject getUserroleTableData(HttpServletRequest request) throws SQLException, HibernateException, Exception {
        //Userrole userrole1 = new Userrole();

        String ViewStatus = "active";
        String UpdateStatus = "active";

        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword

        JSONObject jsonResponse = new JSONObject();
        JSONArray rows = new JSONArray();

        if (param.iDisplayStart < 0) {
            param.iDisplayStart = 0;
        }

        if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
            param.iDisplayLength = 10;
        }

        Userrole parameters = this.getParameters(request);//Setting up searched Section Task ID
        String page = "Userrole Search";
        String affectedId = "";
        String task = "Search/View page";
        String description = "";
        if (parameters != null) {
            description = "Table Search by User Role   ID of ' " + parameters.getUserroleid() + " ' User Role Description of ' " + parameters.getDescription() + " '";
//                + " Decription :" + userrole.getDescription()
//                + " UserroleCode : " + userrole.getUserrolecode()
//                + " Status " + userrole.getStatus()+ parameters.getUserrole().getDescription() +
//                + " Userrole Type : " + userrole.getUserroletype();

        } else {

            description = "View all details in User Role Table";

        }
        String createdUser = String.valueOf(session.getAttribute("username"));

        //this.insertAuditTrace(page, affectedId, task, description, createdUser);
        insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        iTotalRecords = userRoleDAO.getTableDataCount(parameters);//Total Records for searched option
        iTotalDisplayRecords = iTotalRecords;
        if (iTotalRecords > 0) {
            list = userRoleDAO.getTableData(parameters, param.iDisplayLength, param.iDisplayStart);
            rows = new JSONArray(list);

        }

        jsonResponse.put("success", true);
        jsonResponse.put("messageService", "Successfull");
        jsonResponse.put("aaData", rows);
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);

        return jsonResponse;
    }

    @Transactional
    private Userrole getParameters(HttpServletRequest request) throws Exception {

        Userrole userrole = new Userrole();

        String searchOption = request.getParameter("searchOption");
        String searchValue = request.getParameter("searchValue");

        if (searchOption.equals("subsectionId")) {
            boolean chk = true;
            for (char c : searchValue.toCharArray()) {
                if (!Character.isDigit(c)) {
                    chk = false;
                }
            }
            if (chk == true) {
                userrole.setUserroleid(Integer.parseInt(searchValue));
            }

        } else if (searchOption.equals("subsectionDes")) {
            userrole.setDescription(searchValue);
        } else {
            userrole = null;
        }

        return userrole;
    }

    @Override
    @Transactional
    public JSONObject addUserrole(com.avn.affiniti.model.userrole.Userrole userrole, String id, String selectFunction) throws HibernateException, Exception {
        Userrole hbUserrole = new Userrole();
        JSONObject response = new JSONObject();

        if (id != null || id != "") {
            hbUserrole.setUserroleid(userrole.getUserroleid());
        } else {

        }

        hbUserrole.setDescription(userrole.getDescription());
        hbUserrole.setUserroletype(userrole.getUserroletype());
        hbUserrole.setUserrolecode(userrole.getUserrolecode());

        //Setting up the status object 
        Status status = new Status();
        status.setStatusid(userrole.getStatus());
        hbUserrole.setStatus(status);

        if (selectFunction.equalsIgnoreCase(MasterDataVarList.AFFINITI_CODE_UPDATE)) {
            Userrole chkUserrole = userRoleDAO.getUserroleById(Integer.parseInt(id));
            hbUserrole.setCreatedatetime(chkUserrole.getCreatedatetime());
        } else {
            hbUserrole.setCreatedatetime(new Date());
        }

        hbUserrole.setLastupdateddatetime(new Date());
        hbUserrole.setCreateduser((String) session.getAttribute("username"));

        //setting the parent to the userrole object
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        int userroleid = systemuserDAO.getSytemuserByUsername(username).getUserrole().getUserroleid();
        Userrole userrole1 = userRoleDAO.getUserroleById(userroleid);
        if (userrole1.getDescription().equalsIgnoreCase("Epic Sri Lanka")) {
            Userrole userroleobj = new Userrole();
            userroleobj.setUserroleid(userroleid);
            hbUserrole.setUserrole(userroleobj);
        }

        if (userRoleDAO.addUserrole(hbUserrole, selectFunction)) {
            response.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Userole"));
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("success", true);
            response.put("data", hbUserrole);
            String page = "Userrole Create";
            String affectedId = "";
            String task = "View/Create page";
            Status status1 = statusDAO.getStatsuById(userrole.getStatus());

            //  status1 = statusDAO.getStatsuById(userrole.getStatus().getStatusid());
            String description = "Created Userrole With : "
                    + " User Role Decription of : ' " + userrole.getDescription()
                    + " ' UserroleCode of : ' " + userrole.getUserrolecode()
                    + " ' Status of ' " + status1.getDescription()
                    + " ' Userrole Type Of  : " + userrole.getUserroletype() + " ' ";
            String createdUser = String.valueOf(session.getAttribute("username"));

            this.insertAuditTrace(page, affectedId, task, description, createdUser);

        } else {
            response.put("success", false);
            response.put("message", "Response recieved failure");
            response.put("data", "");
        }

        return response;
    }

    @Override
    @Transactional
    public JSONObject getUserroleById(String parameter) throws HibernateException, Exception {
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();

        String page = "User Role";
        String task = "";
        String affectedId = "";
        String description = "";
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        JSONObject response = new JSONObject();
        int id = Integer.parseInt(parameter);
        Userrole userrole = userRoleDAO.getUserroleById(id);
        String action = "<div class=\"row\">"
                + "<div class=\"col-xs-3\"><a id='" + userrole.getUserroleid() + "' value='view'><i class=\"fa fa-lg fa-fw fa-eye\" title=\"View\"></i></a></div>"
                + "<div class=\"col-xs-3\"><a id='" + userrole.getUserroleid() + "' value='edit'><i class=\"fa fa-lg fa-fw fa-pencil\" title=\"Edit\"></i></a></div>"
                + "</div>";
        object.put("userroleId", userrole.getUserroleid());
        object.put("desctiption", userrole.getDescription()); //description     
        object.put("userroletype", userrole.getUserroletype());  // sub section id   
        object.put("userrolecode", userrole.getUserrolecode()); // status
        object.put("status", userrole.getStatus().getDescription()); // status
        object.put("statusid", userrole.getStatus().getStatusid());
        object.put("createddate", userrole.getCreatedatetime());  // sub section id   
        object.put("lastupdateddate", userrole.getLastupdateddatetime()); // status
        object.put("createduser", userrole.getCreateduser()); // URL
        object.put("action", action); // icon

        task = "View";

        description = "Viewed  Single Recorde  in User Role Table User Role  ID of '" + userrole.getUserroleid()
                + " '  Description of ' " + userrole.getDescription() + " ' User Role Type of ' " + userrole.getUserroletype()
                + " ' " + " ' User Role Code  of ' " + userrole.getUserrolecode() + " ' "
                + " ' And Status of ' " + userrole.getStatus().getDescription()+ " ' ";

        this.insertAuditTrace(page, affectedId, task, description, createdUser);

        array.put(object);
        response.put("message", "Response recieved success");
        response.put("success", true);
        response.put("data", array);

        return response;
    }

    @Override
    @Transactional
    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser) {
        try {
            Audittrace auditTrace = new Audittrace();

            auditTrace.setAffectedpage(page);
            auditTrace.setTask(task);
            auditTrace.setCreateduser(createdUser);
            auditTrace.setCreatedatetime(new Date());
            auditTrace.setLastupdateddatetime(new Date());
            auditTrace.setAfectedid(affectedId);
            auditTrace.setDescription(description);

            auditTraceDAO.insertAuditTrace(auditTrace);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    @Transactional
    public JSONArray getUserroleDescriptionByUserRoleId(String userRoleId) throws SQLException {
        return commonDAO.getJSONArrayDropdownValueList(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT_USERROLES,
                userRoleId
        ));
    }

}
