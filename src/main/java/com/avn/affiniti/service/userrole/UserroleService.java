/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.service.userrole;

import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author chandima
 */
public interface UserroleService {

    public void loadPageComponent(Map<String, Object> model) throws SQLException, HibernateException;

    public JSONObject getUserroleTableData(HttpServletRequest request) throws SQLException, HibernateException, Exception;

    public JSONObject addUserrole(com.avn.affiniti.model.userrole.Userrole userrole, String id, String selectFunction) throws HibernateException, Exception;

    public JSONObject getUserroleById(String parameter) throws HibernateException, Exception;

    public void insertAuditTrace(String page, String affectedId, String task, String description, String createdUser);

    public JSONArray getUserroleDescriptionByUserRoleId(String userRoleId) throws SQLException;
}
