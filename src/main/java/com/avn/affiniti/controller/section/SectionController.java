package com.avn.affiniti.controller.section;

import com.avn.affiniti.model.section.Section;
import com.avn.affiniti.service.section.SectionService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ASHOK
 */
@Controller
@RequestMapping("/section")
public class SectionController {

    @Autowired
    SectionService sectionService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String viewSectionPage(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "Section");
            model.put("sectionAdd", new Section());
            sectionService.loadPageComponent(model);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return "section/section";
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public @ResponseBody
    String insertSectoinInfo(HttpServletRequest request, HttpSession session) {
        System.out.println("Test");
        session = request.getSession(true);
        String loginUser = session.getAttribute("username").toString();
        JSONObject details = new JSONObject();

        String sectiondata = request.getParameter("section_info");

        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            Section section = (Section) new ObjectMapper().readValue(sectiondata, Section.class);
            section.setCreateduser(loginUser);
            details = sectionService.saveSection(section, createdUser);

        } catch (SQLException | IOException ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("MESSAGE", "custom message");
            Loggers.LOGGER_FILE.error(ex);

        } catch (Exception ex) {
            details.put("status", 500);
            details.put("success", false);
            details.put("MESSAGE", ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return details.toString();
    }

    @RequestMapping(value = "/loadParentSection", method = RequestMethod.POST)
    public @ResponseBody
    String loadParentSection(HttpServletRequest request) {

        JSONObject details = new JSONObject();

        try {
            String sectionLevel = request.getParameter("section_level");
            details = sectionService.getParntSection(sectionLevel);
        } catch (SQLException ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("messageController", ex);
            Loggers.LOGGER_FILE.error(ex);

        } catch (Exception ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("messageController", ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return details.toString();
    }

    @RequestMapping(value = "/tableLoad", method = RequestMethod.POST)
    public @ResponseBody
    String loadSectionTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            details = sectionService.getSectionTableData(request,createdUser);
        } catch (Exception ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("messageController", ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return details.toString();
    }

    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public @ResponseBody
    String viewSection(HttpServletRequest request,HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            details = sectionService.getSectionBySectionId(request.getParameter("sectionId"),session);
        } catch (Exception ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("messageController", ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return details.toString();
    }
}
