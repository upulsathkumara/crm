/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.userrole;

import com.avn.affiniti.service.userrole.UserroleService;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author chandima
 */
@Controller
@RequestMapping("/userrole")
public class UserroleController {

    @Autowired
    HttpSession session;

    @Autowired
    UserroleService userroleService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String createview(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "User Role Management");
            model.put("UserroleAddform", new com.avn.affiniti.hibernate.model.Userrole());
            userroleService.loadPageComponent(model);
        } catch (Exception ex) {
            Logger.getLogger(UserroleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "userrole/userrole";
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public @ResponseBody
    String insertSubsection(HttpServletRequest request, @RequestParam("action") String selectFunction) {
        String sectiondata = request.getParameter("userrole_info");
        String setSelectFunction = selectFunction;
        try {
            String id = request.getParameter("userrole_id");
            com.avn.affiniti.model.userrole.Userrole userrole = (com.avn.affiniti.model.userrole.Userrole) new ObjectMapper().readValue(sectiondata, com.avn.affiniti.model.userrole.Userrole.class);
            JSONObject response = userroleService.addUserrole(userrole, id, selectFunction);
            return response.toString();
        } catch (Exception ex) {
            Logger.getLogger(UserroleController.class.getName()).log(Level.SEVERE, null, ex);
            return "FAIL";
        }
    }

    @RequestMapping(value = "/loadTable", method = RequestMethod.POST)
    public @ResponseBody
    String loadUserroleTable(HttpServletRequest request) {

        JSONObject response = new JSONObject();
        try {
//            String page = "Userrole Search";
//            String affectedId = "";
//            String task = "View page";
//            String description = "View all details in User Role Table";
//            String createdUser = String.valueOf(session.getAttribute("username"));
//            userroleService.insertAuditTrace(page, affectedId, task, description, createdUser);

            final DataTableRequestParam param = DataTableParamUtility.getParam(request);
            response = userroleService.getUserroleTableData(request);
        } catch (HibernateException ex) {
            Logger.getLogger(UserroleController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            response.put("success", false);
            response.put("messageService", "Un-successfull");
            response.put("sEcho", 0);
            response.put("iTotalRecords", 0);
            response.put("iTotalDisplayRecords", 0);
            Logger.getLogger(UserroleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public @ResponseBody
    String viewUserole(HttpServletRequest request) {
        JSONObject jsonArray = new JSONObject();
        try {
            int id = 0;
            if (request.getParameter("userroleId").equalsIgnoreCase(null) || request.getParameter("userroleId").equalsIgnoreCase("")) {
                id = Integer.parseInt(request.getParameter("userroleId"));
            }

            jsonArray = userroleService.getUserroleById(request.getParameter("userroleId"));
        } catch (Exception ex) {
            System.out.println("Exception in Controller GetSubsection By ID : " + ex);
            Logger.getLogger(UserroleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonArray.toString();
    }

}
