package com.avn.affiniti.controller.inventory;

import com.avn.affiniti.dao.common.CommonDAO;
import com.avn.affiniti.dao.systemuser.SystemuserDAO;
import com.avn.affiniti.dao.ticket.TicketDAO;
import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Inventoryterminalitem;
import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.hibernate.model.Ticket;
import com.avn.affiniti.model.inventory.Inventory;
import com.avn.affiniti.service.inventory.InventoryService;
import com.avn.affiniti.service.ticket.TicketService;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.usermessages.AffinitiMessages;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryController
 * @Created on : Jun 1, 2017, 3:14:30 PM
 */
@Controller
@RequestMapping("/inventory")
public class InventoryTerminalController {

    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private CommonDAO commonDAO;
    @Autowired
    private SystemuserDAO systemuserDAO;

    @RequestMapping(value = "/assign/terminal", method = RequestMethod.GET)
    public String viewTerminalAssign(ModelMap model) {
        try {
            model.put("root", "Inventory");
            model.put("page", "Assign Terminal");

        } catch (Exception exception) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return "inventory/inventoryterminalassign";
    }

    @RequestMapping(value = "/assign/ticket_table", method = RequestMethod.POST)
    @ResponseBody
    public String ticketTable(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();

        session = request.getSession(true);
        String loginUser = session.getAttribute("username").toString();

        try {
            response = inventoryService.getTicketAssignmentData(request, loginUser);
        } catch (Exception e) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return response.toString();
    }

    @RequestMapping(value = "/assign/terminal_table", method = RequestMethod.POST)
    @ResponseBody
    public String terminalTable(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();
        session = request.getSession(true);
        String systemuser = session.getAttribute("username").toString();

        try {
            response = inventoryService.getTerminalAssignmentData(request, systemuser);
        } catch (Exception e) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return response.toString();
    }

    @RequestMapping(value = "/assign/terminalAssign", method = RequestMethod.POST)
    @ResponseBody
    public String assingTerminal(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();
        session = request.getSession(true);

        int ticketid = Integer.parseInt(request.getParameter("ticketId"));
        int inventoryterminalid = Integer.parseInt(request.getParameter("inventoryTerminalId"));
        String username = session.getAttribute("username").toString();
        String ticketCategoryStatusCode = ticketDAO.getStatusCodeByTicketId(ticketid);

        try {
            if (ticketCategoryStatusCode.equals(MasterDataVarList.AFFINITI_CODE_STATUS_CODE_TKT_TKTCOLTERTMC)) {
                ticketService.newTerminalAssign(ticketid, inventoryterminalid, username);
            } else {
                ticketService.backupTermianlAssign(ticketid, inventoryterminalid, username);
            }
            response.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("MESSAGE", String.format(AffinitiMessages.TERMINAL_ASSIGNE_SUCCESS.getMessage(), ticketid)
                    + " | " + String.format(AffinitiMessages.TERMINAL_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketid)));

        } catch (HibernateException | SQLException | JSONException e) {
            response.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            response.put("MESSAGE", String.format(AffinitiMessages.TERMINAL_ASSIGNE_ERROR.getMessage(), ticketid));
            e.printStackTrace();
        }
        return response.toString();
    }

    @RequestMapping(value = "/accept/accept_ticket_table", method = RequestMethod.POST)
    @ResponseBody
    public String acceptTerminalTable(HttpServletRequest request, HttpSession session) throws SQLException {

        session = request.getSession(true);
        String username = session.getAttribute("username").toString();
        JSONObject response = inventoryService.acceptTicket(request, username);
        return response.toString();
    }

    @RequestMapping(value = "/accept/acceptTicket", method = RequestMethod.POST)
    @ResponseBody
    public String acceptTicket(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();
        session = request.getSession(true);
        int acceptTicketid = Integer.parseInt(request.getParameter("acceptTicketId"));
        try {
            response = inventoryService.accepttoTMCInventory(acceptTicketid);
        } catch (HibernateException | JSONException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    @RequestMapping(value = "/insert/terminal", method = RequestMethod.GET)
    public String createviewTerminal(ModelMap model, @ModelAttribute("inventoryTerminalCreate") Inventory inventory, HttpServletRequest request, HttpSession session) {
        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);

            model.put("root", "Inventory");
            model.put("page", "Insert Terminal");

            inventoryService.loadPageComponent(model, employee.getLocation().getLocationid());

        } catch (Exception exception) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return "inventory/inventoryterminalcreate";
    }

    @RequestMapping(value = "/insert_terminal/done", method = RequestMethod.POST)
    public @ResponseBody
    String insertSubsection(HttpServletRequest request, @RequestParam("action") String selectFunction) {
        String sectiondata = request.getParameter("terminal_info");
        JSONObject response = new JSONObject();
        try {
            com.avn.affiniti.model.inventory.Inventoryterminalitem inventoryterminalitem = (com.avn.affiniti.model.inventory.Inventoryterminalitem) new ObjectMapper().readValue(sectiondata, com.avn.affiniti.model.inventory.Inventoryterminalitem.class);
            inventoryService.insertTerminal(inventoryterminalitem, selectFunction);
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Terminal"));
            response.put("success", true);
            return response.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, ex);
            return response.toString();
        }
    }

    @RequestMapping(value = "/getterminalmodels", method = RequestMethod.GET)
    public @ResponseBody
    String getModelList(@RequestParam("terminalbrand") int terminalbrand) {
        JSONArray array = new JSONArray();
        try {
            array = inventoryService.getTerminalModelDropdownByBrand(terminalbrand);
        } catch (Exception e) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/search/terminal", method = RequestMethod.GET)
    public String serchTerminal(ModelMap model, @ModelAttribute("inventoryTerminalCreate") Inventory inventory, HttpServletRequest request, HttpSession session) {

        String tModelId = request.getParameter("brandsearch");
        String client = request.getParameter("clientsearch");
        String modelSearch = request.getParameter("modelsearch");

        String user = (String) session.getAttribute("username");
        //String contextPath = request.getContextPath();
        //String inventorycategoryid = request.getParameter("inventorycategory");//Setting up searched Section Task ID
        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);

            inventoryService.loadPageComponent(model, employee.getLocation().getLocationid());
            model.put("root", "Inventory");
            model.put("page", "Search Terminal");

        } catch (Exception exception) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }

        return "inventory/inventoryterminalsearch";
    }

    @RequestMapping(value = "/searched/terminal", method = RequestMethod.GET)
    @ResponseBody
    public String searchTerminal(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        String tModelId = request.getParameter("brandsearch");
        String client = request.getParameter("clientsearch");
        String model = request.getParameter("modelsearch");

        String user = (String) session.getAttribute("username");
        String contextPath = request.getContextPath();

        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;

        JSONArray rows = new JSONArray();

        try {
            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }

            if (Boolean.valueOf(request.getParameter("search"))) {
                rows = inventoryService.getTerminalSearchedData(user, tModelId, contextPath, param.iDisplayStart, param.iDisplayLength);
            }

        } catch (Exception e) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
        jsonResponse.put("aaData", rows);
        return jsonResponse.toString();
    }

    @RequestMapping(value = "/edit/terminal", method = RequestMethod.GET)
    public String editTerminal(@RequestParam(value = "terminalId", required = false) int terminalId,
            HttpSession session,
            @ModelAttribute("terminaledit") Inventoryterminalitem inventoryTerminal,
            ModelMap model, HttpServletRequest request) throws SQLException {

        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);

            model.put("root", "Inventory");
            model.put("page", "Edit Terminal");

            inventoryService.loadPageComponent(model, employee.getLocation().getLocationid());
            inventoryTerminal = inventoryService.getTerminalById(terminalId);
            model.addAttribute("terminaledit", inventoryTerminal);
            model.addAttribute("terminalId", terminalId);
            model.addAttribute("terminalModel", inventoryTerminal.getTerminalmodel().getTerminalmodelid());
        } catch (Exception exception) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }

        return "inventory/inventoryterminaledit";
    }

    @RequestMapping(value = "/view/terminal", method = RequestMethod.GET)
    public String viewTerminal(@RequestParam(value = "terminalId", required = false) int terminalId,
            HttpSession session,
            @ModelAttribute("terminalview") Inventoryterminalitem inventoryTerminal,
            ModelMap model, HttpServletRequest request) throws SQLException {

        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);

            model.put("root", "Inventory");
            model.put("page", "Veiw Terminal");

            inventoryService.loadPageComponent(model, employee.getLocation().getLocationid());
            inventoryTerminal = inventoryService.getTerminalById(terminalId);

            String page = "Terminal View ";
            String task = "";
            String affectedId = "";
            String description = "";
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            model.addAttribute("terminalview", inventoryTerminal);
            model.addAttribute("terminalModel", inventoryTerminal.getTerminalmodel().getTerminalmodelid());

            task = "View";
                int s =  (Integer.parseInt(String.valueOf(inventoryTerminal.getClient().getClientid())));          
                description = "Viewed  Single Recorde  in Terminal Table Terminal  ID of '" + inventoryTerminal.getInventoryterminalitemid()
                    + " ' " + " ' Client   of ' "+inventoryTerminal.getClient().getName() 
                        + "' Serial No of ' "+inventoryTerminal.getSerialno()+" ' Inventory Catagory of ' " 
                        + inventoryTerminal.getInventorycategory().getDescription()+" ' Terminal Brand of ' "+inventoryTerminal.getTerminalbrand().getDescription()+" ' TID of ' "
                        + inventoryTerminal.getTid()+" ' MID of ' "+inventoryTerminal.getMid()+" ' And Terminal Model ' "+inventoryTerminal.getTerminalmodel().getDescription()
                        +" ' ";
                   

            inventoryService.insertAuditTrace(page, affectedId, task, description, createdUser);
            
        } catch (Exception exception) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }

        return "inventory/inventoryterminalview";
    }

    @RequestMapping(value = "/edit_terminal/done", method = RequestMethod.POST)
    public String editTerminalDone(@RequestBody Inventoryterminalitem inventoryterminal, HttpSession session) {

//        String createduser = (String) session.getAttribute("username");
        try {
            inventoryService.editTerminal(inventoryterminal);
        } catch (SQLException ex) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Success";
    }

    @RequestMapping(value = "/loadtable/terminal", method = RequestMethod.POST)
    public @ResponseBody
    String loadUserroleTable(HttpServletRequest request, HttpSession session) {
        String contextPath = request.getContextPath();
        JSONObject response = new JSONObject();
        session = request.getSession(true);
        String username = session.getAttribute("username").toString();
        Employee employee = inventoryService.getEmployeeByUsername(username);
        String page = "Inventry Terminal";
        String task = "";
        String description = "";
        String affectedId = "";
        String tModelId = request.getParameter("brandsearch");
        String client = request.getParameter("clientsearch");
        String modelSearch = request.getParameter("modelsearch");
        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        String user = (String) session.getAttribute("username");
        try {
            final DataTableRequestParam param = DataTableParamUtility.getParam(request);
            response = inventoryService.getTerminalTableData(request, employee.getLocation().getLocationid());

            if (tModelId.equals("") && client.equals("") && modelSearch.equals("")) {
                task = "Table View";
                description = "Table View all details in Inventory Terminal ";
                affectedId = "";

            } else {
                task = "Table Search";
                description = "Table Search details in Inventory Terminal by Brand ID-' " + tModelId + "' Model ID-' " + modelSearch + "' Cilent ID-' " + client + " '";
                affectedId = tModelId;
            }
            inventoryService.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log

        } catch (HibernateException ex) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            response.put("success", false);
            response.put("messageService", "Un-successfull");
            response.put("sEcho", 0);
            response.put("iTotalRecords", 0);
            response.put("iTotalDisplayRecords", 0);
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/validateSerialNumber", method = RequestMethod.GET)
    public @ResponseBody
    Boolean validateSerialNumber(HttpServletRequest request) {
        boolean isSerialNumberAvailable = false;
        String serialnumber = request.getParameter("serialnumber");
        try {
            isSerialNumberAvailable = inventoryService.validateSerialNumber(serialnumber);
        } catch (Exception ex) {
            Logger.getLogger(InventoryTerminalController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return isSerialNumberAvailable;
    }

}
