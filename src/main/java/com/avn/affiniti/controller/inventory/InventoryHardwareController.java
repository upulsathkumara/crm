/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.inventory;

import com.avn.affiniti.hibernate.model.Inventoryhardwareitem;
import com.avn.affiniti.model.inventory.InventoryHardware;
import com.avn.affiniti.service.inventory.InventoryService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author : Nuwan Fernando
 * @Document : InventoryAccessoryController
 * @Created on : Jun 26, 2017, 10:14:06 AM
 */
@Controller
@RequestMapping("/inventory_hardware")
public class InventoryHardwareController {

    @Autowired
    InventoryService inventoryService;

    @RequestMapping(value = "/insert/item", method = RequestMethod.GET)
    public String createviewItem(ModelMap model, @ModelAttribute("hardwareItemCreate") InventoryHardware hardwareitem) {
        try {
            model.put("root", "Inventory");
            model.put("page", "Insert Hardware Item");

            model.addAttribute("categoryList", inventoryService.dropdownValueListHardwareCategory());
            model.addAttribute("locationList", inventoryService.dropdownValueListHardwareLocation());

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return "inventory/inventoryhardwarecreate";
    }

    @RequestMapping(value = "/insert_item/done", method = RequestMethod.POST)
    public @ResponseBody
    String insertItem(@RequestBody Inventoryhardwareitem hardwareitem, HttpSession session, @RequestParam("action") String selectFunction) {
        String createdUser = (String) session.getAttribute("username");
        hardwareitem.setCreateduser(createdUser);

//        JSONObject object = new JSONObject();
        String input = String.valueOf(hardwareitem.getInventorycategory().getInventorycategoryid());

        // int des = hardwareitem.getInventorycategory().getInventorycategoryid().intValue();
        String setSelectFunction = selectFunction;

//        try {
//            // hardwareitem.setCreateduser(createduser);
//           object = inventoryService.insertHardwareItem(hardwareitem, createdUser, input);
//            
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            object.put("message","Server Error");
//        }
//
////        return "Success";
//        return object.toString();
        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Hardware"));
        output.put("DATA", new JSONArray());

        try {
            inventoryService.insertHardwareItem(hardwareitem, createdUser, input, setSelectFunction);
        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.SAVE_ERROR, "hardware"));
            Logger.getLogger(InventoryHardwareController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.SAVE_ERROR, "hardware"));
            Logger.getLogger(InventoryHardwareController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        }
        return output.toString();

    }

    @RequestMapping(value = "/search/item", method = RequestMethod.GET)
    public String serchTerminal(ModelMap model, @ModelAttribute("hardwareItemSearch") InventoryHardware hardwareitem) {

        try {
            model.put("root", "Inventory");
            model.put("page", "Search Hardware Item");

            model.addAttribute("categoryList", inventoryService.dropdownValueListHardwareCategory());

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return "inventory/inventoryhardwaresearch";
    }

    @RequestMapping(value = "/searched/item", method = RequestMethod.POST)
    public @ResponseBody
    String getHardwareTableData(HttpServletRequest request, HttpSession session) {

        JSONObject output = new JSONObject();

        String page = "Inventry Hardware";
        String task = "";
        String description = "";
        String affectedId = "";

        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            output = inventoryService.getInventoryhardwareTableData(request, createdUser);
            String inventorycategoryid = request.getParameter("inventorycategory");//Setting up searched Section Task ID

            if (inventorycategoryid.equals("") || inventorycategoryid.equals("--Select--")) {
                task = "Table View";
                description = "Table View all details in Inventory Hardware ";
                affectedId = "";
            } else {
                task = "Table Search";
                description = "Table Search details in Inventory Hardware by Inventory Hardware Id " + inventorycategoryid;
                affectedId = inventorycategoryid;
            }
            inventoryService.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log
        } catch (Exception e) {

            Logger.getLogger(InventoryHardwareController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return output.toString();
    }
//    @RequestMapping(value = "/searched/item", method = RequestMethod.GET)
//    @ResponseBody
//    public String searchItem(HttpServletRequest request, HttpServletResponse response, HttpSession session)throws Exception{
//        String categoryId = request.getParameter("category");
//        String contextPath = request.getContextPath();
//        
//        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
//        final String sEcho = param.sEcho;
//        long iTotalRecords = 0; // total number of records (unfiltered)
//
//        long iTotalDisplayRecords = 0;
//        
//        JSONArray rows = new JSONArray();
//        
//        try {
//            if (param.iDisplayStart < 0) {
//                param.iDisplayStart = 0;
//            }
//
//            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
//                param.iDisplayLength = 10;
//            }
//            
//            if (Boolean.valueOf(request.getParameter("search"))) {
//                rows = inventoryService.getItemSearchedData(categoryId, contextPath, param.iDisplayStart, param.iDisplayLength);
//            }
//              
//        } 
//        catch (Exception e) {
//            Logger.getLogger(InventoryController.class.getName()).log(Level.SEVERE, null, e);
//            e.printStackTrace();
//        }
//        
//        JSONObject jsonResponse = new JSONObject();
//        jsonResponse.put("sEcho", sEcho);
//        jsonResponse.put("iTotalRecords", iTotalRecords);
//        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
//        jsonResponse.put("aaData", rows);
//        return jsonResponse.toString();
//    }

    @RequestMapping(value = "/view/item", method = RequestMethod.GET)
    public String viewItem(@RequestParam(value = "itemId", required = false) int itemId,
            HttpSession session,
            @ModelAttribute("itemview") Inventoryhardwareitem hardwareItem,
            ModelMap model) throws SQLException {

        try {
            model.put("root", "Inventory");
            model.put("page", "Veiw Item");

            model.addAttribute("categoryList", inventoryService.dropdownValueListHardwareCategory());
            model.addAttribute("locationList", inventoryService.dropdownValueListHardwareLocation());

            String page = "Accessory View ";
            String task = "";
            String affectedId = "";
            String description = "";
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            hardwareItem = inventoryService.getItemById(itemId);

            task = "View";

            description = "Viewed  Single Recorde  in Accessories Table Accessories  ID of '" + hardwareItem.getInventoryhardwareitemid()
                    + " ' " + " ' Category   of ' " + hardwareItem.getInventorycategory().getDescription() + "' "
                    + " Part Name of ' " + hardwareItem.getPartname() + " ' "+hardwareItem.getPartcode()+" ' Location of ' "+hardwareItem.getLocation().getLocationid()+" ' "
                    +" Revision of ' "+hardwareItem.getPosrevision()+" '  MAC of ' "+hardwareItem.getPosmac()+" ' Part Name of ' "+hardwareItem.getPartname()+" '"
                    +" Serial No of ' "+hardwareItem.getPosserialnumber()+" ' PT_ID of ' "+hardwareItem.getPosptid()+" '";

            inventoryService.insertAuditTrace(page, affectedId, task, description, createdUser);
            
            model.addAttribute("itemview", hardwareItem);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return "inventory/inventoryhardwareview";
    }

    @RequestMapping(value = "/edit/item", method = RequestMethod.GET)
    public String editItem(@RequestParam(value = "itemId", required = false) int itemId,
            HttpSession session,
            @ModelAttribute("itemview") Inventoryhardwareitem hardwareItem,
            ModelMap model) throws SQLException {

        try {
            model.put("root", "Inventory");
            model.put("page", "Veiw Item");

            model.addAttribute("categoryList", inventoryService.dropdownValueListHardwareCategory());
            model.addAttribute("locationList", inventoryService.dropdownValueListHardwareLocation());

            hardwareItem = inventoryService.getItemById(itemId);
            model.addAttribute("itemedit", hardwareItem);
        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return "inventory/inventoryhardwareedit";
    }

    @RequestMapping(value = "/edit_item/done", method = RequestMethod.POST)
    public @ResponseBody
    String editTerminalDone(@RequestBody Inventoryhardwareitem hardwareItem, HttpSession session, @RequestParam("action") String selectFunction) {

        String createduser1 = (String) session.getAttribute("username");
        String input = String.valueOf(hardwareItem.getInventorycategory().getInventorycategoryid());
        //String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        hardwareItem.setCreateduser(createduser1);
        String setSelectFunction = selectFunction;
        //try {

        // inventoryService.editHardwareItem(hardwareItem,createduser1,input);
        // } 
        // catch (SQLException ex) {
        // }
        //return "Success";
        // }
        //////////////////////////////////////
        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("message", String.format(MessageVarList.UPDATED_SUCCESSFULLY, "Hardware"));
        output.put("DATA", new JSONArray());

        try {
            inventoryService.editHardwareItem(hardwareItem, createduser1, input, setSelectFunction);
        } catch (SQLException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.SAVE_ERROR, "Hardware"));
            Logger.getLogger(InventoryHardwareController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.SAVE_ERROR, "Hardware"));
            Logger.getLogger(InventoryHardwareController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        }
        return output.toString();

//    @RequestMapping(value = "/edit_item/done", method = RequestMethod.POST)
//    public @ResponseBody
//    String getInventoryhardwareByInventoryhardwareId(@RequestParam("inventoryhardwareitemid") int inventoryhardwareitemId) {
//        return inventoryService.getInventoryhardwareByInventoryhardwareId(inventoryhardwareitemId).toString();
//        //return new JSONArray().toString();
//    }
    }
}
