/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.client;

import com.avn.affiniti.hibernate.model.Client;
import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.model.clientproduct.ClientProduct;
import com.avn.affiniti.service.client.ClientService;
import com.avn.affiniti.service.inventory.InventoryService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author Nadun Chamikara
 * @Document CustomerController
 * @Created on 17/08/2017, 2:34:12 PM
 */
@Controller
@RequestMapping(value = "client")
public class ClientController {

    @Autowired
    private ClientService clientService;
    @Autowired
    private InventoryService inventoryService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model, HttpServletRequest request, HttpSession session) {
        model.put("page", "Client");
        model.put("clientAddForm", new Client());
        model.put("clientSearchForm", new Client());
        model.put("clientProductAddForm", new ClientProduct());

        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);
            clientService.loadPageComponent(model, employee.getLocation().getLocationid());
        } catch (Exception e) {
            e.printStackTrace();
            Loggers.LOGGER_FILE.error(e);
        }
        return "client/client";
    }

    @RequestMapping(value = "/getproductcategory", method = RequestMethod.POST)
    public @ResponseBody
    String getProductCategoryByClientType(HttpServletRequest request) {

//        int clientType = Integer.parseInt(request.getParameter("clientType"));
        String clientCategories = request.getParameter("clientType");
        JSONObject objData = new JSONObject();
        objData.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        objData.put("SUCCESS", true);
        objData.put("MESSAGE", "");
        objData.put("DATA", new JSONArray());
        try {
            objData.put("DATA", clientService.getProductCategoryByClientType(clientCategories));
        } catch (Exception ex) {
            ex.printStackTrace();
            objData.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("SUCCESS", false);
            objData.put("MESSAGE", "Error : Error while loading product categories - please try again!");
            Loggers.LOGGER_FILE.error(ex);
        }

        return objData.toString();
    }

    @RequestMapping(value = "createorupdateclient", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String createOrUpdateClient(@RequestBody com.avn.affiniti.model.client.Client client, HttpServletRequest request, HttpSession session) {
        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Client"));
        output.put("DATA", new JSONArray());

        try {
            clientService.createOrUpdateClient(client, request, session);
        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.SAVE_ERROR, "client"));
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.SAVE_ERROR, "client"));
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        }
        return output.toString();
    }

//    @RequestMapping(value = "clientcategoriesbyorganization", method = RequestMethod.POST)
//    public @ResponseBody
//    String getClientCategoriesByOrganization(@RequestParam("organizationid") int organizationId) {
//        JSONObject output = new JSONObject();
//        JSONArray array = new JSONArray();
//
//        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
//        output.put("SUCCESS", true);
//        output.put("MESSAGE", "");
//        output.put("DATA", array);
//
//        try {
//            array = clientService.getClientCategoryListByOrganizationId(organizationId);
//            output.put("DATA", array);
//
//        } catch (SQLException ex) {
//            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("SUCCESS", false);
//            output.put("MESSAGE", "Error : Database Error");
//
//            ex.printStackTrace();
//            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (DataAccessException ex) {
//            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("SUCCESS", false);
//            output.put("MESSAGE", "Error : Error");
//
//            ex.printStackTrace();
//            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return output.toString();
//    }
    @RequestMapping(value = "/getclientstotable", method = RequestMethod.GET)
    public @ResponseBody
    String getClientTableData(HttpServletRequest request, HttpSession session) {
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        String username = session.getAttribute("username").toString();
        Employee employee = inventoryService.getEmployeeByUsername(username);

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword
        JSONArray rows = new JSONArray();
        try {

            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }

            String name = request.getParameter("name");
            String organizationId = request.getParameter("organizationid");
            String clientCategoryId = request.getParameter("clientcategoryid");

            iTotalRecords = clientService.getClientTableDataCount(name, organizationId, clientCategoryId, employee.getLocation().getLocationid());
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = clientService.getClientTableData(name, organizationId, employee.getLocation().getLocationid(), clientCategoryId, param.iDisplayStart, param.iDisplayLength, session);

//                for (JSONObject jSONObject : list) {
//                    jSONObject.put("action", String.format(jSONObject.getString("action"), request.getContextPath(), request.getContextPath()));
//                }
                rows = new JSONArray(list);
            }
        } catch (Exception e) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        JSONObject output = new JSONObject();
        output.put("sEcho", sEcho);
        output.put("iTotalRecords", iTotalRecords);
        output.put("iTotalDisplayRecords", iTotalDisplayRecords);
        output.put("aaData", rows);
        return output.toString();

//        JSONArray data = new JSONArray();
//        JSONObject output = new JSONObject();
//
//        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
//        output.put("SUCCESS", true);
//        output.put("MESSAGE", "Data successfully retrieved.");
//        output.put("DATA", data);
//
//        try {
//            data = clientService.getClientTableData(organizationId, clientCategoryId, session);
//            output.put("DATA", data);
//
//        } catch (HibernateException ex) {
//            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("SUCCESS", false);
//            output.put("MESSAGE", "Error : Database Error");
//            ex.printStackTrace();
//            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
//
//        } catch (Exception ex) {
//            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("SUCCESS", false);
//            output.put("MESSAGE", "Error : Error");
//            ex.printStackTrace();
//            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
//
//        }
//        return output.toString();
    }

    @RequestMapping(value = "/getproductlistbycategory", method = RequestMethod.POST)
    public @ResponseBody
    String getProductList(@RequestParam("productcategoryid") int productCategoryId) {
        JSONArray array = new JSONArray();
        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", MessageVarList.DATA_RETRIEVED_SUCCESSFULLY);
        output.put("DATA", array);

        try {
            array = clientService.getProductListByProductCategory(productCategoryId);
            output.put("DATA", array);

        } catch (SQLException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (DataAccessException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return output.toString();
    }

    @RequestMapping(value = "/getclientproductstatuslist", method = RequestMethod.POST)
    public @ResponseBody
    String getClientProductStatusList() {
        JSONArray array = new JSONArray();

        JSONObject output = new JSONObject();
        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", MessageVarList.DATA_RETRIEVED_SUCCESSFULLY);
        output.put("DATA", array);

        try {
            array = clientService.getClientProductStatusList();
            output.put("DATA", array);

        } catch (SQLException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (DataAccessException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return output.toString();
    }

    @RequestMapping(value = "/getclientbyid", method = RequestMethod.POST)
    public @ResponseBody
    String getClientById(@RequestParam("clientid") int clientId, HttpSession session) {
        JSONArray data = new JSONArray();

        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", MessageVarList.DATA_RETRIEVED_SUCCESSFULLY);
        output.put("DATA", data);

        try {
            data = clientService.getClientById(clientId, session);
            output.put("DATA", data);

        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }

    @RequestMapping(value = "/deleteclient", method = RequestMethod.POST)
    public @ResponseBody
    String deleteClient(@RequestParam("clientid") int clientId, HttpSession session) {
        JSONArray data = new JSONArray();
        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", String.format(MessageVarList.DELETED_SUCCESSFULLY, "Client"));
        output.put("DATA", data);

        try {
            clientService.deleteClient(clientId, session);
        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.DELETE_ERROR, "client"));
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.DELETE_ERROR, "client"));
            ex.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }

    @RequestMapping(value = "/isvalidemail", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateEmailUpdate(@RequestParam("email") String email, @RequestParam("clientid") String clientid) {
        boolean isNotaDuplicateUserEmail = false;
        try {
            isNotaDuplicateUserEmail = clientService.isNotaDuplicateEmail(email, clientid);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateUserEmail;
    }
}
