package com.avn.affiniti.controller.userrolesectiontask;

import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.hibernate.model.Userrolesectiontask;
import com.avn.affiniti.model.userolesectiontask.UserRoleSectionTask;
import com.avn.affiniti.service.userrolesectiontask.UserRoleSectionTaskService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author :Kaushan Fernando
 * @Document :UserRoleSectionTaskController
 * @Created on:Oct 13, 2017,2:43:45 PM
 */
@Controller
@RequestMapping(value = "/userrolesectiontask")
public class UserRoleSectionTaskController {

    @Autowired
    private UserRoleSectionTaskService userRoleSectionTaskService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "User Role Section Task");
            model.put("userRoleTaskSearchForm", new Userrolesection());
            model.put("userRoleTaskAddForm", new Userrolesection());

            userRoleSectionTaskService.loadPageComponent(model);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);

        }
        return "userrolesectiontask/userrolesectiontask";
    }

    @RequestMapping(value = "/getuserrolesectiontasktotable", method = RequestMethod.POST)
    public @ResponseBody
    String loadSectionTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            details = userRoleSectionTaskService.getUserRoleTaskTableData(request, createdUser);
        } catch (HibernateException | SQLException | JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", "Un-successfull");
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        }catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", "Un-successfull");
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        }
        return details.toString();
    }

    @RequestMapping(value = "/getuserrolesections", method = RequestMethod.GET)
    public @ResponseBody
    String getUserRoleSectionsByUserRoleId(@RequestParam("userroleid") int userRoleId) {

        JSONObject objData = new JSONObject();

        try {
            objData = userRoleSectionTaskService.getUserRoleSectionsByUserRoleId(userRoleId);
        } catch (Exception ex) {

            Loggers.LOGGER_FILE.error(ex);

        }

        return objData.toString();
    }

    @RequestMapping(value = "/gettasklist", method = RequestMethod.GET)
    public @ResponseBody
    String getTaskLIstById(HttpServletRequest request,HttpSession session) {

        int userRoleId = Integer.parseInt(request.getParameter("userRoleId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));

        JSONObject objData = new JSONObject();

        try {
            objData = userRoleSectionTaskService.getTaskLIstById(userRoleId, sectionId,session);

        } catch (Exception ex) {

            Loggers.LOGGER_FILE.error(ex);

        }

        return objData.toString();
    }

    @RequestMapping(value = "/adduserrolesectiontask", method = RequestMethod.POST)
    public @ResponseBody
    String AddUserRoleTask(@RequestBody UserRoleSectionTask[] userRoleSectionTasks, HttpSession session, @RequestParam("action") String selectFunction, @RequestParam("userroleid") int userRoleId, @RequestParam("sectionid") int sectionId) {
        JSONObject output = new JSONObject();

        try {

            int userRoleSectionId = userRoleSectionTaskService.findUserroleSectionId(userRoleId, sectionId);
            List<Userrolesectiontask> listOfOldObject = userRoleSectionTaskService.getUserRoleSectionTaskList(userRoleSectionId);

            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            output = userRoleSectionTaskService.saveUserRoleSectionTask(userRoleSectionTasks, listOfOldObject, createdUser, selectFunction);

        } catch (HibernateException | SQLException | JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", String.format(MessageVarList.ASSIGN_ERROR, "tasks"));
//            if (selectFunction.equals("save")) {
//                output.put("message", String.format(MessageVarList.SAVE_ERROR, "tasks"));
//
//            } else {
//                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "tasks"));
//            }

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", String.format(MessageVarList.ASSIGN_ERROR, "tasks"));
//            if (selectFunction.equals("save")) {
//                output.put("message", String.format(MessageVarList.SAVE_ERROR, "tasks"));
//
//            } else {
//                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "tasks"));
//            }

        }
        return output.toString();
    }

}
