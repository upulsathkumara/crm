/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.status;

import com.avn.affiniti.hibernate.model.Status;
import com.avn.affiniti.service.status.StatusService;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author :acer
 * @Document :StatusController
 * @Created on:Aug 14, 2017,3:30:44 PM
 */
//@Controller
//@RequestMapping("/status")
//public class StatusController {
//
//    @RequestMapping(value = "/all", method = RequestMethod.GET)
//    public String creatviwe(ModelMap model) {
//        try {
//            model.put("root", "Dashboard");
//            model.put("page", "Status");
//        } catch (Exception exception) {
//            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, exception);
//            exception.printStackTrace();
//        }
//        return "status/status";
//    }
//}
@Controller
@RequestMapping(value = "/status")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("page", "Status");
            model.put("statusSearchForm", new Status());
            model.put("statusAddForm", new Status());

            statusService.loadPageComponent(model);
        } catch (Exception e) {
            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return "status/status";
    }

    @RequestMapping(value = "/getstatus", method = RequestMethod.POST)
    public @ResponseBody

    String getStatusTableData(HttpServletRequest request, HttpSession session) {

        JSONObject output = new JSONObject();
//        String page = "Status";
//        String task = "";
//        String description = "";
//        String affectedId = "";
        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            output = statusService.getStatusTableData(request, createdUser);
                      
//            String parameters = request.getParameter("searchoptionID");//Setting up searched Section Task ID
//
//             if (parameters.equals("") || parameters.equals("--Select--")) {
//                task = "Table View";
//                description = "Table View all details in Status ";
//                affectedId = "";
//            } else {
//                task = "Table Search";
//                description = "Table Search details in Status by status Id " + parameters;
//                affectedId = parameters;
//
//            }
//            statusService.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log
        } catch (Exception e) {

            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return output.toString();
    }

    @RequestMapping(value = "/getustatuss", method = RequestMethod.POST)
    public @ResponseBody
    String getStatussByStatusId(@RequestParam("statusid") int statusId,HttpSession session) {
        return statusService.getStatussByStatusId(statusId,session).toString();
    }

    @RequestMapping(value = "/addorupdatestatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String createOrUpdateStatus(@RequestBody Status status, HttpSession session, HttpServletRequest request, @RequestParam("action") String selectFunction) {
        JSONObject output = new JSONObject();

        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        String test = String.valueOf(status.getStatuscategory().getStatuscategoryid());
        
        String id = String.valueOf(status.getStatusid());
        
        String setSelectFunction = selectFunction;
        //return statusService.createOrUpdateStatus(status, createdUser,test).toString();

        try {
            output = statusService.createOrUpdateStatus(status, createdUser, test,id, setSelectFunction);

            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            output.put("SUCCESS", true);
            output.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Status"));
            output.put("DATA", new JSONArray());
        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.SAVE_ERROR, "Status"));
            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.SAVE_ERROR, "Status"));
            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        return output.toString();

    }

    @RequestMapping(value = "/deletestatus", method = RequestMethod.POST)
    public @ResponseBody
    String deleteStatus(@RequestParam("statusid") int statusid) {
                JSONObject output = new JSONObject();

        
        try {
            statusService.deleteStatus(statusid);
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            output.put("SUCCESS", true);
            output.put("message", String.format(MessageVarList.DELETED_SUCCESSFULLY, "Status"));
            output.put("DATA", new JSONArray());
            } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.DELETE_ERROR, "Status"));
            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("message", String.format(MessageVarList.DELETE_ERROR, "Status"));
            Logger.getLogger(StatusController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

    return output.toString();

    }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }


