package com.avn.affiniti.controller.errorpages;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Kaushan Fernando
   @Document ErrorPagesController
   @Created on: Oct 17, 2017, 3:42:23 PM
 */
@Controller
public class ErrorPagesController {
    
      @RequestMapping(value = "/errorpages/error404", method = RequestMethod.GET)
    public String error404(ModelMap model) {

        return "errorpages/error404";

    }
    
      @RequestMapping(value = "/errorpages/error500", method = RequestMethod.GET)
    public String error500(ModelMap model) {

        return "errorpages/error500";

    }
    
     @RequestMapping(value = "/errorpages/error401", method = RequestMethod.GET)
    public String error401(ModelMap model) {

        return "errorpages/error401";

    }

}
