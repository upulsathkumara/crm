/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.accessory;

import com.avn.affiniti.service.accessory.AccessoryAssignService;
import com.avn.affiniti.service.ticket.TicketService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.usermessages.AffinitiMessages;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ASHOK
 */

@Controller
@RequestMapping("/accessory")
public class AccessoryAssignController {
    
    @Autowired
    AccessoryAssignService accessoryAssignService;
    
    @Autowired
    TicketService ticketService;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String viewAccessoryAssign(ModelMap model) {
        try {
            model.put("root", "Accessory");
            model.put("page", "Assign Accessory");

        } catch (Exception ex) {
            Logger.getLogger(AccessoryAssignController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "accessory/accessoryticketassign";
    }
    
    @RequestMapping(value = "/ticket_table", method = RequestMethod.POST)
    @ResponseBody
    public String ticketTable(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();
        session = request.getSession(true);
        String loginUser = session.getAttribute("username").toString();
        try {
            response = accessoryAssignService.getTicketTableData(request, loginUser);
        } catch (Exception e) {
            Logger.getLogger(AccessoryAssignController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return response.toString();
    }

    @RequestMapping(value = "/accessory_table", method = RequestMethod.POST)
    @ResponseBody
    public String accessoryTable(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();
        session = request.getSession(true);
        String username = session.getAttribute("username").toString();

        try {
            response = accessoryAssignService.getAccessoryTableData(request, username);
        } catch (Exception e) {
            Logger.getLogger(AccessoryAssignController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return response.toString();
    }
    
    @RequestMapping(value = "/accessoryAssign", method = RequestMethod.POST)
    @ResponseBody
    public String assingAccessory(HttpServletRequest request, HttpSession session) throws SQLException {

        JSONObject response = new JSONObject();
        session = request.getSession(true);

        int ticketid = Integer.parseInt(request.getParameter("ticketId"));
        JSONArray inventoryHardwareItemIdlist = new JSONArray(request.getParameter("inventoryHardwareItemId"));
        String username = session.getAttribute("username").toString();
        try {  
            ticketService.accessoriesAssign(ticketid, inventoryHardwareItemIdlist, username);                                               
            response.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            response.put("MESSAGE", String.format(AffinitiMessages.ACCESSORY_ASSIGNE_SUCCESS.getMessage(), ticketid)
                    + " | " + String.format(AffinitiMessages.ACCESSORY_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketid)));

        } catch (HibernateException | JSONException e) {
            response.put("CODE", "ERROR");
            response.put("MESSAGE", "Accessory Successfully Assigned to Ticked ID : " + ticketid);
            e.printStackTrace();
        }
        return response.toString();
    }
    
}