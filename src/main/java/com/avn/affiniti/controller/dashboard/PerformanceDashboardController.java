///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.avn.affiniti.controller.dashboard;
//
//import com.avn.affiniti.dao.common.CommonDAOImpl;
//import com.avn.affiniti.dao.dashboard.DashboardDAOImpl;
//import com.avn.affiniti.daoimpl.audittrace.AuditTraceDAOImpl;
//import com.avn.affiniti.daoimpl.casemgt.CaseDAOImpl;
//import com.avn.affiniti.model.dashboard.DashBordChart;
//import com.avn.affiniti.model.performancedashboard.Performancedashboard;
//import com.avn.affiniti.service.dashboard.DashboardService;
//import com.avn.affiniti.util.varlist.MasterDataVarList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//
///**
// * @Author : Roshen Dilshan
// * @Document : PerformanceDashboardController
// * @Created on : Jun 21, 2016, 12:19:17 PM
// */
//@Controller
//public class PerformanceDashboardController {
//
//    @Autowired
//    CaseDAOImpl casedaoimple;
//    @Autowired
//    DashboardDAOImpl dashboarddaoimpl;
//    @Autowired
//    CommonDAOImpl commondaoimpl;
//    @Autowired
//    DashboardService dashboardService;
//    @Autowired
//    AuditTraceDAOImpl auditTraceDAOImpl;
//
//    @RequestMapping(value = "/performancedashboard", method = RequestMethod.GET)
//    public ModelAndView performanceDashboard(@ModelAttribute("dashboardform") Performancedashboard performancedashboard, ModelMap model, HttpSession session) {
//        String userName = (String) session.getAttribute("username");
//        try {
//            auditTraceDAOImpl.insertAuditTrace(null, null, "Performance Dashboard", "", userName);
//            try {
//                model.addAttribute("targetList", dashboardService.getTargetDropDownList(userName));
//                JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//
//                model.addAttribute("TargetGraph1", getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART), assignchartlist));
//                model.addAttribute("TargetGraph1ID", getWedgetID(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART), assignchartlist));
//                model.addAttribute("TargetGraph1CSS", getWedgetCSS(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART), assignchartlist));
//
//                model.addAttribute("TargetGraph2", getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH), assignchartlist));
//                model.addAttribute("TargetGraph2ID", getWedgetID(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH), assignchartlist));
//                model.addAttribute("TargetGraph2CSS", getWedgetCSS(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH), assignchartlist));
//
//                model.addAttribute("LeadContagGraph", getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD), assignchartlist));
//                model.addAttribute("LeadContagGraphID", getWedgetID(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD), assignchartlist));
//                model.addAttribute("LeadContagGraphCSS", getWedgetCSS(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD), assignchartlist));
//
//                model.addAttribute("LeadRatioGraph", getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist));
//                model.addAttribute("LeadRationID", getWedgetID(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist));
//                model.addAttribute("LeadRationCSS", getWedgetCSS(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist));
//
//            } catch (Exception e) {
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        model.put("MAP", "HP");
//        return new ModelAndView("dashboard/performancedashboard");
//    }
//
//    @RequestMapping(value = "/dashboard/targetGraph", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadTargetGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        long targetId = Long.valueOf(request.getParameter("target"));
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        try {
//           /DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH), assignchartlist);
//            if (dashbordchart.isTragetgraph()) {
//                data = dashboardService.getAgentTargetGraph(targetId, MasterDataVarList.CRM_TARGETPERIOD_MONTHLY).toString();
//            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/targetBarGraph", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadTargetBarGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        long targetId = Long.valueOf(request.getParameter("target"));
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART), assignchartlist);
//            if (dashbordchart.isBarchart()) {
//                data = dashboardService.getBarTargetGarph(targetId).toString();
//            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/contactleadaccount", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadContactLeadAccountGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        try {
//            JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD), assignchartlist);
//            if (dashbordchart.isContacklead()) {
//                data = dashboardService.getUserContactLeadAccountGraph((String) session.getAttribute("username")).toString();
//            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/leadratio", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadRatioGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist);
//        try {
//            if (dashbordchart.isLeadratio()) {
//                data = dashboardService.getUserSuccessFailLeadRatioGraph((String) session.getAttribute("username")).toString();
//            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/organizationratio", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadTargetRatioOrganizationGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        long targetId = Long.valueOf(request.getParameter("target"));
////        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
////        DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist);
//        try {
////            if (dashbordchart.isLeadratio()) {
//            data = dashboardService.getTargetRatioOrganizationGraph(targetId).toString();
////            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/userratio", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadTargetRatioUserGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        String userName = (String) session.getAttribute("username");
//        long targetId = Long.valueOf(request.getParameter("target"));
////        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
////        DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist);
//        try {
////            if (dashbordchart.isLeadratio()) {
//            data = dashboardService.getTargetRatioUserGraph(targetId, userName).toString();
////            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//    
//    @RequestMapping(value = "/dashboard/activitygraph", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadActivityTargetGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
////        String userName = (String) session.getAttribute("username");
//        long targetId = Long.valueOf(request.getParameter("target"));
////        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
////        DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist);
//        try {
////            if (dashbordchart.isLeadratio()) {
//            data = dashboardService.getActivityPerformanceByTargetId(targetId).toString();
////            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//    
//    @RequestMapping(value = "/dashboard/activitiesgraph", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadActivitiesTargetGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
////        String userName = (String) session.getAttribute("username");
//        long targetId = Long.valueOf(request.getParameter("target"));
////        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
////        DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist);
//        try {
////            if (dashbordchart.isLeadratio()) {
//            data = dashboardService.getActivitiesPerformanceByTargetPeriod(targetId).toString();
////            }
//        } catch (Exception e) {
//            Logger.getLogger(PerformanceDashboardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    private DashBordChart getAssignChart(String wedgetid, JSONArray objList) throws Exception {
//        DashBordChart dashbordchart = new DashBordChart();
//        dashbordchart.setCallvscase(false);
//        dashbordchart.setAssigncase(false);
//        dashbordchart.setBarchart(false);
//        dashbordchart.setProductwiseinquiry(false);
//        dashbordchart.setMycall(false);
//        dashbordchart.setMycase(false);
//        dashbordchart.setTragetgraph(false);
//
//        for (int i = 0; i < objList.length(); i++) {
//            JSONObject yObject = objList.getJSONObject(i);
//
//            if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CALL_VS_CASECHART))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setCallvscase(true);
//                }
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_ASSIGN_CASE))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setAssigncase(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setBarchart(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_PRODUCT_WISE_INQUIRY))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setProductwiseinquiry(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_MY_CALL))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setMycall(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_MY_CASE))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setMycase(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setTragetgraph(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setContacklead(true);
//                }
//
//            } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION))) {
//                if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                    dashbordchart.setLeadratio(true);
//                }
//
//            }
//        }
//
//        return dashbordchart;
//    }
//
//    private String getWedgetID(String wedgetid, JSONArray objList) throws Exception {
//        String ID = "";
//        for (int i = 0; i < objList.length(); i++) {
//            JSONObject yObject = objList.getJSONObject(i);
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                ID = yObject.get("sortid").toString();
//                break;
//            }
//        }
//
//        return ID;
//    }
//
//    private String getWedgetCSS(String wedgetid, JSONArray objList) throws Exception {
//        String css = "";
//        for (int i = 0; i < objList.length(); i++) {
//            JSONObject yObject = objList.getJSONObject(i);
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ACTIVE))) {
//                css = yObject.get("css").toString();
//                break;
//            }
//        }
//
//        return css;
//    }
//
//}
