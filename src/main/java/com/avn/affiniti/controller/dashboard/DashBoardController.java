/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.dashboard;

import com.avn.affiniti.controller.login.LoginController;
import com.avn.affiniti.dao.common.CommonDAOImpl;
//import com.avn.affiniti.daoimpl.casemgt.CaseDAOImpl;
import com.avn.affiniti.dao.dashboard.DashboardDAOImpl;
import com.avn.affiniti.dao.employee.EmployeeDAOImpl;
import com.avn.affiniti.dao.systemuser.SystemuserDAOImpl;
import com.avn.affiniti.hibernate.model.Systemuser;
import com.avn.affiniti.service.dashboard.DashboardService;
import com.avn.affiniti.util.Common;
import static com.avn.affiniti.util.varlist.CommonVarList.DATE_FORMAT_yyyy_MM;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ISURU
 */
@Controller
public class DashBoardController {

    // @Autowired
    //CaseDAOImpl casedaoimple;
    @Autowired
    DashboardDAOImpl dashboarddaoimpl;
    @Autowired
    CommonDAOImpl commondaoimpl;
    @Autowired
    DashboardService dashboardService;
    @Autowired
    EmployeeDAOImpl employeeDAOImpl;
    @Autowired
    SystemuserDAOImpl systemuserDAOImpl;

    @RequestMapping(value = "/loadAssigneCaseChart", method = RequestMethod.POST)
    public @ResponseBody
    String loadAssigneCaseChart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        String name = (String) session.getAttribute("username");
        JSONArray objList = (JSONArray) session.getAttribute("assignchartlist");
        JSONArray dateWithXAssignee = new JSONArray();
        JSONArray dateWithYAssignee = new JSONArray();
        List<JSONObject> jSONArray = new ArrayList<>();
        //Case casebeen = new Case();
        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_ASSIGN_CASE), objList);
            //            if (dashbordchart.isAssigncase()) {
            //casebeen = casedaoimple.getUserRoleIdAndEmployeeId(name);
            Systemuser systemuser = systemuserDAOImpl.getSytemuserByUsername(name);
            String employeeId = String.valueOf(systemuser.getEmployee().getEmployeeid());
            dateWithXAssignee = dashboarddaoimpl.getCaseCount(employeeId, "T.ASSIGNEEID");
            dateWithYAssignee = dashboarddaoimpl.getCallCount(name);

            for (int i = 0; i < dateWithXAssignee.length(); i++) {
                boolean isDataSet = false;
                JSONObject xObject = dateWithXAssignee.getJSONObject(i);
                for (int j = 0; j < dateWithYAssignee.length(); j++) {
                    JSONObject yObject = dateWithYAssignee.getJSONObject(j);
                    if (xObject.getString("m").equalsIgnoreCase(yObject.getString("m"))) {
                        JSONObject content = new JSONObject();
                        content.put("z", yObject.getInt("z"));
                        content.put("x", xObject.getString("m"));
                        content.put("y", xObject.getInt("y"));
                        jSONArray.add(content);
                        isDataSet = true;
                        break;
                    }
                }
                if (!isDataSet) {
                    JSONObject content = new JSONObject();
                    content.put("y", xObject.getString("y"));
                    content.put("x", xObject.getString("m"));
                    content.put("z", 0);
                    jSONArray.add(content);
                }

            }
//            model.put("objList", jSONArray);
//            }
        } catch (Exception e) {
            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, e);
        }
        return jSONArray.toString();
    }

    @RequestMapping(value = "/loadCaseVsCaseChart", method = RequestMethod.POST)
    public @ResponseBody
    String loadCaseVsCaseChart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        //-----------------------------------CallVs Case Charat-----------------------------------------------------
        JSONArray dateWithX = new JSONArray();
        JSONArray dateWithY = new JSONArray();
        //Case casebeen = new Case();
        List<JSONObject> jSONArray = new ArrayList<>();
        String name = (String) session.getAttribute("username");
        JSONArray objList = (JSONArray) session.getAttribute("assignchartlist");
        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CALL_VS_CASECHART), objList);
//            if (dashbordchart.isCallvscase()) {
            //casebeen = casedaoimple.getUserRoleIdAndEmployeeId(name);
            Systemuser su = systemuserDAOImpl.getSytemuserByUsername(name);
            String employeeId = String.valueOf(su.getEmployee().getEmployeeid());
            dateWithX = dashboarddaoimpl.getCaseCount(name, "T.CREATEDUSER");
            dateWithY = dashboarddaoimpl.getCallCount(name);
            for (int i = 0; i < dateWithX.length(); i++) {
                boolean isDataSet = false;
                JSONObject xObject = dateWithX.getJSONObject(i);
                for (int j = 0; j < dateWithY.length(); j++) {
                    JSONObject yObject = dateWithY.getJSONObject(j);
                    if (xObject.getString("m").equalsIgnoreCase(yObject.getString("m"))) {
                        JSONObject content = new JSONObject();
                        content.put("z", yObject.getInt("z"));
                        content.put("x", xObject.getString("m"));
                        content.put("y", xObject.getInt("y"));
                        jSONArray.add(content);
                        isDataSet = true;
                        break;
                    }
                }
                if (!isDataSet) {
                    JSONObject content = new JSONObject();
                    content.put("y", xObject.getString("y"));
                    content.put("x", xObject.getString("m"));
                    content.put("z", 0);
                    jSONArray.add(content);
                }
            }

        } catch (Exception e) {
            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, e);
        }
        return jSONArray.toString();
    }

    /**
     * **Added by Gaje****
     */
    @RequestMapping(value = "/tableLoad", method = RequestMethod.POST)
    public @ResponseBody
    String loadTicketTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            details = dashboardService.getTicketTableData(request, session);
        } catch (Exception ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("messageController", ex);
        }
        return details.toString();
    }

    @RequestMapping(value = "/tableLoadidling", method = RequestMethod.POST)
    public @ResponseBody
    String loadIdlingTicketTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            details = dashboardService.getIdlingTicketTableData(request, session);
        } catch (Exception ex) {

            details.put("status", 500);
            details.put("success", false);
            details.put("messageController", ex);
        }
        return details.toString();
    }

//    @RequestMapping(value = "/loadUserWisePieChart", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadUserWisePieChart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        List<JSONObject> jSONArray2 = new ArrayList<>();
//        String name = (String) session.getAttribute("username");
//        Case casebeen = new Case();
//        JSONArray objList = new JSONArray();
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_PRODUCT_WISE_INQUIRY), assignchartlist);
//            if (dashbordchart.isProductwiseinquiry()) {
//                casebeen = casedaoimple.getUserRoleIdAndEmployeeId(name);
//                String employeeId = String.valueOf(casebeen.getEmployeeId());
//                objList = dashboarddaoimpl.getProduntCount(employeeId, "and C.ASSIGNEEID1", commondaoimpl.getCurentTimesStamp());
//
//                int total = 0;
//                for (int i = 0; i < objList.length(); i++) {
//                    JSONObject xObject = objList.getJSONObject(i);
//                    int count = xObject.getInt("value");
//                    total += count;
//                }
//                for (int j = 0; j < objList.length(); j++) {
//                    JSONObject yObject = objList.getJSONObject(j);
//                    double pcount = yObject.getDouble("value");
//                    double precentage = (pcount / total) * 100;
//                    JSONObject content = new JSONObject();
//                    content.put("data", Math.round(precentage * 100.0) / 100.0);
//                    content.put("label", yObject.getString("label"));
//                    jSONArray2.add(content);
//
//                }
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return jSONArray2.toString();
//    }
    @RequestMapping(value = "/loadOrganizationWisePieChart", method = RequestMethod.POST)
    public @ResponseBody
    String loadOrganizationWisePieChart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        List<JSONObject> jSONArray2 = new ArrayList<>();
        String name = (String) session.getAttribute("username");
        JSONArray objList = new JSONArray();
        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
        try {
            //DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_PRODUCT_WISE_INQUIRY), assignchartlist);
            //if (dashbordchart.isProductwiseinquiry()) {
            objList = dashboarddaoimpl.getProduntCountOrganizationwise(commondaoimpl.getCurentTimesStamp());
            int total = 0;
            for (int i = 0; i < objList.length(); i++) {
                JSONObject xObject = objList.getJSONObject(i);
                int count = xObject.getInt("value");
                total += count;
            }
            for (int j = 0; j < objList.length(); j++) {
                JSONObject yObject = objList.getJSONObject(j);
                double pcount = yObject.getDouble("value");
                double precentage = (pcount / total) * 100;
                JSONObject content = new JSONObject();
                content.put("data", Math.round(precentage * 100.0) / 100.0);
                content.put("label", yObject.getString("label"));
                jSONArray2.add(content);

            }

        } catch (Exception ex) {
            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jSONArray2.toString();
    }

//    @RequestMapping(value = "/loadUserWiseBarchart", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadUserWiseBarchart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String name = (String) session.getAttribute("username");
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        Case casebeen = new Case();
//        JSONObject fulllist = new JSONObject();
//        JSONArray Dcount = new JSONArray();
//        JSONArray Departmentlist = new JSONArray();
//        List<JSONObject> jSONArray4 = new ArrayList<>();;
//        List<String> departments = new ArrayList<>();
//        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_PRODUCT_WISE_INQUIRY), assignchartlist);
//            if (dashbordchart.isProductwiseinquiry()) {
//                casebeen = casedaoimple.getUserRoleIdAndEmployeeId(name);
//                String employeeId = String.valueOf(casebeen.getEmployeeId());
////                Dcount = dashboarddaoimpl.getDepartmentCount(employeeId, "and C.ASSIGNEEID1", commondaoimpl.getCurentTimesStamp());
//                Departmentlist = dashboarddaoimpl.getDepartmentList();
//                JSONObject details = new JSONObject();
//                for (int i = 0; i < Departmentlist.length(); i++) {
//                    JSONObject dlist = Departmentlist.getJSONObject(i);
//                    if (i == 0) {
//                        details.put("x", dlist.get("Year"));
//                    }
//                    boolean isdataset = false;
//                    for (int j = 0; j < Dcount.length(); j++) {
//                        JSONObject ddata = Dcount.getJSONObject(j);
//                        if (dlist.getString("Departments").equalsIgnoreCase(ddata.getString("Des"))) {
//                            if (Integer.parseInt(ddata.getString("Count")) != 0) {
//                                details.put("" + dlist.getString("Departments") + "", "" + ddata.getString("Count") + "");
//                                departments.add("'" + dlist.getString("Departments") + "'");
//                                isdataset = true;
//                            }
//                        }
//                    }
//                }
//                jSONArray4.add(details);
//                fulllist.put("Dcontent", jSONArray4);
//                fulllist.put("Department", departments);
//            }
//        } catch (Exception e) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return fulllist.toString();
//    }
    @RequestMapping(value = "/loadOrganizationWiseBarchart", method = RequestMethod.POST)
    public @ResponseBody
    String loadOrganizationWiseBarchart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        String name = (String) session.getAttribute("username");
        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
        // Case casebeen = new Case();
        JSONArray objList = new JSONArray();
        JSONArray Dpcount = new JSONArray();
        JSONArray Departmentlist = new JSONArray();
        JSONObject fulllist = new JSONObject();
        List<JSONObject> jSONArray5 = new ArrayList<>();
        List<String> departmentsO = new ArrayList<>();
        try {
            int height = 125;
            // DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_PRODUCT_WISE_INQUIRY), assignchartlist);
            //if (dashbordchart.isProductwiseinquiry()) {
            Departmentlist = dashboarddaoimpl.getDepartmentList();
            Dpcount = dashboarddaoimpl.getDepartmentCountbyOrganizationwise(commondaoimpl.getCurentTimesStamp());

            JSONObject DOdetails = new JSONObject();
            long dividevalue = 1;
            for (int i = 0; i < Departmentlist.length(); i++) {
                JSONObject dlistO = Departmentlist.getJSONObject(i);
                if (i == 0) {
                    DOdetails.put("x", Common.getStringFormatDate(DATE_FORMAT_yyyy_MM, commondaoimpl.getCurentTimesStamp()));
                }
                boolean isdataset = false;
                for (int j = 0; j < Dpcount.length(); j++) {
                    JSONObject dodata = Dpcount.getJSONObject(j);
                    if (dlistO.getString("Departments").equalsIgnoreCase(dodata.getString("Des"))) {
                        JSONObject details = new JSONObject();
                        long maxcount = dodata.getLong("Count");
                        double percentage = 0;

                        if (j == 0 && maxcount != 0) {
                            percentage = (maxcount / maxcount) * 100;
                            dividevalue = maxcount;
                        } else {
                            percentage = (((double) dodata.getLong("Count")) / dividevalue) * 100;
                        }
                        JSONArray array = new JSONArray();
                        details.put("Dep", dlistO.getString("Departments"));
                        details.put("Count", percentage);
                        details.put("ACount", dodata.getLong("Count"));
                        jSONArray5.add(details);
                        isdataset = true;
                        fulllist.put("DpcontentOrg", jSONArray5);
//                            }
                    }

                }
            }

        } catch (Exception e) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
        }
        return fulllist.toString();
    }

//    @RequestMapping(value = "/dashboard/targetGraph", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadTargetGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH), assignchartlist);
//            if (dashbordchart.isTragetgraph()) {
//                data = dashboardService.getAgentTargetGraph(MasterDataVarList.CRM_TARGETPERIOD_MONTHLY).toString();
//                dashboardService.getBarTargetGarph(1);
//            }
//        } catch (Exception e) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/targetBarGraph", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadTargetBarGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        try {
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART), assignchartlist);
//            if (dashbordchart.isBarchart()) {
//                data = dashboardService.getBarTargetGarph(1).toString();
//            }
//        } catch (Exception e) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/contactleadaccount", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadContactLeadAccountGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        try {
//            JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//            DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD), assignchartlist);
//            if (dashbordchart.isContacklead()) {
//                data = dashboardService.getUserContactLeadAccountGraph((String) session.getAttribute("username")).toString();
//            }
//
//        } catch (Exception e) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//
//    @RequestMapping(value = "/dashboard/leadratio", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadRatioGraph(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
//        String data = "";
//        JSONArray assignchartlist = (JSONArray) session.getAttribute("assignchartlist");
//        DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION), assignchartlist);
//        try {
//            if (dashbordchart.isLeadratio()) {
//                data = dashboardService.getUserSuccessFailLeadRatioGraph((String) session.getAttribute("username")).toString();
//            }
//        } catch (Exception e) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
    //dan comment kare
//    private DashBordChart getAssignChart(String wedgetid, JSONArray objList) throws Exception {
//        DashBordChart dashbordchart = new DashBordChart();
//        dashbordchart.setCallvscase(false);
//        dashbordchart.setAssigncase(false);
//        dashbordchart.setBarchart(false);
//        dashbordchart.setProductwiseinquiry(false);
//        dashbordchart.setMycall(false);
//        dashbordchart.setMycase(false);
//        dashbordchart.setTragetgraph(false);
//
//        for (int i = 0; i < objList.length(); i++) {
//            JSONObject yObject = objList.getJSONObject(i);
//
//            // if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CALL_VS_CASECHART))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setCallvscase(true);
//            }
//            // else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_ASSIGN_CASE))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setAssigncase(true);
//            }
//
//            //} else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_BAR_CHART))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setBarchart(true);
//            }
//
//            //} else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_PRODUCT_WISE_INQUIRY))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setProductwiseinquiry(true);
//            }
//
//            // } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_MY_CALL))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setMycall(true);
//            }
//
//            //} else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_MY_CASE))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setMycase(true);
//            }
//
//            // } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_TARGET_GRAPH))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setTragetgraph(true);
//            }
//
//            // } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CONTACKLEAD))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setContacklead(true);
//            }
//
//            // } else if (yObject.getString("wedgetid").contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_LEADRATION))) {
//            if (yObject.getString("wedgetid").contentEquals(wedgetid) && yObject.getString("statusid").contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_ID_ACTIVE))) {
//                dashbordchart.setLeadratio(true);
//            }
//
//        }
//
//        return dashbordchart;
//    }
//    @RequestMapping(value = "/dashboard/userticketstatus", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadUserTicketStatus(HttpSession session) throws Exception {
//        String data = "";
//        try {
//            data = dashboardService.getUserTicketStatus(String.valueOf(session.getAttribute("username"))).toString();
//        } catch (Exception e) {
//            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//    @RequestMapping(value = "/dashboard/orgticketstatus", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadOrganizationTicketStaus(HttpSession session) throws Exception {
//        String data = "";
//        try {
//            data = dashboardService.getOrgTicketStatus().toString();
//        } catch (Exception e) {
//            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data;
//    }
//    @RequestMapping(value = "/dashboard/averagedealclosuretime", method = RequestMethod.POST)
//    public @ResponseBody
//    String loadAverageDealClosureTime(HttpSession session) throws Exception {
//        JSONObject data = null;
//        try {
//
//            data = dashboardService.averageDealClosureTime(employeeDAOImpl.getEmployeeByUsername(String.valueOf(session.getAttribute("username"))).getEmployeeid());
//        } catch (Exception e) {
//            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return data.toString();
//    }
    @RequestMapping(value = "/loadLeadLostAndClosChart", method = RequestMethod.POST)
    public @ResponseBody
    String loadLeadLostAndClosChart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        JSONArray dateWithX = new JSONArray();
        JSONArray dateWithY = new JSONArray();
        //  Case casebeen = new Case();
        List<JSONObject> jSONArray = new ArrayList<>();
        String name = (String) session.getAttribute("username");
        JSONArray objList = (JSONArray) session.getAttribute("assignchartlist");
        try {
            //DashBordChart dashbordchart = getAssignChart(String.valueOf(MasterDataVarList.CCL_CODE_DASHBORD_CALL_VS_CASECHART), objList);
            // if (dashbordchart.isCallvscase()) {
            //casebeen = casedaoimple.getUserRoleIdAndEmployeeId(name);
            //String employeeId = String.valueOf(casebeen.getEmployeeId());
            // dateWithX = dashboardService.getLeadCloseOverTime(employeeDAOImpl.getEmployeeByUsername(String.valueOf(session.getAttribute("username"))).getEmployeeid());
            dateWithY = dashboarddaoimpl.getLeadLostOverTime(employeeDAOImpl.getEmployeeByUsername(String.valueOf(session.getAttribute("username"))).getEmployeeid());
            for (int i = 0; i < dateWithX.length(); i++) {
                boolean isDataSet = false;
                JSONObject xObject = dateWithX.getJSONObject(i);
                for (int j = 0; j < dateWithY.length(); j++) {
                    JSONObject yObject = dateWithY.getJSONObject(j);
                    if (xObject.getString("m").equalsIgnoreCase(yObject.getString("m"))) {
                        JSONObject content = new JSONObject();
                        content.put("z", yObject.getInt("z"));
                        content.put("x", xObject.getString("m"));
                        content.put("y", xObject.getInt("y"));
                        jSONArray.add(content);
                        isDataSet = true;
                        break;
                    }
                }
                if (!isDataSet) {
                    JSONObject content = new JSONObject();
                    content.put("y", xObject.getString("y"));
                    content.put("x", xObject.getString("m"));
                    content.put("z", 0);
                    jSONArray.add(content);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DashBoardController.class.getName()).log(Level.SEVERE, null, e);
        }

        return jSONArray.toString();
    }

    @RequestMapping(value = "/dashboard/averagedealclosration", method = RequestMethod.POST)
    public @ResponseBody
    String loadAverageDealClosureRation(HttpSession session) throws Exception {
        JSONObject data = null;
        try {
         // data = dashboardService.averageDealClosureRation(employeeDAOImpl.getEmployeeByUsername(String.valueOf(session.getAttribute("username"))).getEmployeeid());   

        } catch (Exception e) {
            Logger.getLogger(DashBoardController.class
                    .getName()).log(Level.SEVERE, null, e);
        }

        return data.toString();
    }
}
