package com.avn.affiniti.controller.subsectiontask;

import com.avn.affiniti.hibernate.model.Subsectiontask;
import com.avn.affiniti.model.subsectiontask.subsectiontask;
import com.avn.affiniti.service.subsectiontask.SubSectionTaskService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Kaushan Fernando
 * @Document SubSectionTask
 * @Created on: Sep 25, 2017, 10:27:17 AM
 */
@Controller
@RequestMapping(value = "/subsectiontask")
public class SubSectionTaskController {

    @Autowired
    private SubSectionTaskService subSectionTaskService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "Subsection Task");
            model.put("subSectionTaskSearchForm", new Subsectiontask());
            model.put("subSectionTaskAddForm", new Subsectiontask());

            subSectionTaskService.loadPageComponent(model);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);

        }
        return "subsectiontask/subsectiontask";
    }

    @RequestMapping(value = "/getsubsectionsandtasksbysectionid", method = RequestMethod.POST)
    public @ResponseBody
    String loadSubSectionTaskTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            /**
             * ********--AuditTrace--*********
             */
            String page = "SubSectionTask";
            String task = "";
            String description = "";
            String affectedId = "";
            String idType = "";
            String id = "";
            
            String sectionId = request.getParameter("sectionsearchid");
            String subSectionId = request.getParameter("subsectionsearchid");
            String taskId = request.getParameter("tasklistsearchid");

            if ((sectionId.equals("") || sectionId.equals("--Select--")) && (subSectionId.equals("") || subSectionId.equals("--Select--")) && (taskId.equals("") || taskId.equals("--Select--"))) {
                task = "Table View";
                description = "Table View all details in subsectiontask ";
                affectedId = "";
                idType = "section";
                id = sectionId;

            } else if (!(sectionId.equals("") || sectionId.equals("--Select--"))) {
                task = "Table Search";
                description = "Table Search details in subsectiontask by section Id " + sectionId;
                affectedId = sectionId;
                idType = "section";
                id = sectionId;

            } else if (!(subSectionId.equals("") || subSectionId.equals("--Select--"))) {
                task = "Table Search";
                description = "Table Search details in subsectiontask by sub section Id " + subSectionId;
                affectedId = subSectionId;
                idType = "subsection";
                id = subSectionId;

            } else if (!(taskId.equals("") || taskId.equals("--Select--"))) {
                task = "Table Search";
                description = "Table Search details in subsectiontask by task Id " + taskId;
                affectedId = taskId;
                idType = "task";
                id = taskId;

            } else {
                task = "Table Search";
                description = "Table Search details in section Id " + sectionId + " : sub section Id" + subSectionId + " : task Id" + taskId;
                affectedId = sectionId;
                idType = "section";
                id = sectionId;

            }
            subSectionTaskService.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log
            /**
             * *******--End Of Audit Trace--**********
             */

            details = subSectionTaskService.getSubSectionTaskTableTableData(request, createdUser);
        } catch (JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", ex);
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", ex);
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", ex);
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        }
        return details.toString();
    }

    @RequestMapping(value = "/getsubsectionsandtasksbysectionid", method = RequestMethod.GET)
    public @ResponseBody
    String getTaskLIstAndSubSectionsBySectionId(HttpServletRequest request) {

        int sectionId = Integer.parseInt(request.getParameter("sectionId"));

        JSONObject objData = new JSONObject();

        try {
            objData = subSectionTaskService.getTaskLIstAndSubSectionsBySectionId(sectionId);

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading - please try again!");
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading - please try again!");
        }

        return objData.toString();
    }

    @RequestMapping(value = "/gettasksbysectionidandsubsectionidoneditandview", method = RequestMethod.GET)
    public @ResponseBody
    String getTaskLIstBySectionIdAndSubSectionIdOnEditAndView(HttpServletRequest request,HttpSession session) {

        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        int subSectionId = Integer.parseInt(request.getParameter("subsectionId"));

        JSONObject objData = new JSONObject();

        try {
            objData = subSectionTaskService.getTaskLIstBySectionIdAndSubSectionIdOnEditAndView(sectionId, subSectionId,session);

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading - please try again!");
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading - please try again!");
        }

        return objData.toString();
    }

    @RequestMapping(value = "/gettasksbysectionidandsubsectionid", method = RequestMethod.GET)
    public @ResponseBody
    String getTaskLIstBySectionIdAndSubSectionId(HttpServletRequest request) {

        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        int subSectionId = Integer.parseInt(request.getParameter("subsectionId"));

        JSONObject objData = new JSONObject();

        try {
            objData = subSectionTaskService.getTaskLIstBySectionIdAndSubSectionId(sectionId, subSectionId);

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading - please try again!");
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading - please try again!");
        }

        return objData.toString();
    }

    @RequestMapping(value = "/addsubsectiontask", method = RequestMethod.POST)
    public @ResponseBody
    String addOrganization(@RequestBody subsectiontask[] subSectionTask, HttpSession session, @RequestParam("action") String action, @RequestParam("sectionId") String sectionId, @RequestParam("subSectionId") String subSectionId) {
        JSONObject output = new JSONObject();

        try {
            List<Subsectiontask> listOfOldObject = null;

            listOfOldObject = subSectionTaskService.getOldSubSectionTaskObj(sectionId, subSectionId);

            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            output = subSectionTaskService.saveSubSectionTask(subSectionTask, listOfOldObject, createdUser, action);

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);

            if (action.equals("save")) {
                output.put("message", String.format(MessageVarList.SAVE_ERROR, "tasks"));

            } else {
                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "tasks"));
            }

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);

            if (action.equals("save")) {
                output.put("message", String.format(MessageVarList.SAVE_ERROR, "tasks"));

            } else {
                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "tasks"));
            }

        }
        return output.toString();
    }

}
