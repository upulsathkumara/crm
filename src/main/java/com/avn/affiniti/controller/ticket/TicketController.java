/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.ticket;

import com.avn.affiniti.hibernate.model.Ticketcatgorycontent;
import com.avn.affiniti.model.ticket.BackupRemovalTicket;
import com.avn.affiniti.model.ticket.BaseSoftwareInstallationTicket;
import com.avn.affiniti.model.ticket.MaintainancePaymentInvoiceTicket;
import com.avn.affiniti.model.ticket.MerchantRemovalTicket;
import com.avn.affiniti.model.ticket.NewInstallationTicke;
import com.avn.affiniti.model.ticket.ReinitializationTicket;
import com.avn.affiniti.model.ticket.SoftwareHardwareBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalBreakdownTicket;
import com.avn.affiniti.model.ticket.TerminalConversionTicket;
import com.avn.affiniti.model.ticket.TerminalRepairMysTicket;
import com.avn.affiniti.model.ticket.TerminalRepairTicket;
import com.avn.affiniti.model.ticket.TerminalSharingTicket;
import com.avn.affiniti.model.ticket.TerminalUpgradeDowngradeTicket;
import com.avn.affiniti.model.ticket.Ticket;
import com.avn.affiniti.model.ticket.TicketParam;
import com.avn.affiniti.service.ticket.TicketService;
import com.avn.affiniti.service.ticketcategorycontent.TicketCategoryContentService;
import com.avn.affiniti.service.user.UserService;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.usermessages.AffinitiMessages;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * @Author : Roshen Dilshan
 * @Document : TicketController
 * @Created on : Apr 27, 2017, 9:20:10 AM
 */
@Controller
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    TicketCategoryContentService ticketCategoryContentService;
    @Autowired
    TicketService ticketService;
    @Autowired
    UserService userService;
    @Autowired
    ServletContext context;

    private final int maxFileSizeInKb = 5120;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createview(ModelMap model, HttpSession session) {
        try {
            model.put("root", "Ticket");
            model.put("page", "Create");
            model.put("ticketForm", new Ticket());
            String userName = (String) session.getAttribute("username");
            ticketService.loadPageComponent(model, userName);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return "ticket/tickecreate";
    }

    @RequestMapping(value = "/getdynamiccontent", method = RequestMethod.GET)
    public @ResponseBody
    String getTicketDynamicContent(HttpServletRequest request, @RequestParam("ticketcategory") int ticketcategory, @RequestParam("state") String state) {
        Ticketcatgorycontent ticketcatgorycontent = ticketCategoryContentService.getTicketCategoryContentByTicketCategory(ticketcategory, state);
        return ticketcatgorycontent.getContent() + ticketcatgorycontent.getScript().replace(":app", request.getContextPath());
    }

    @RequestMapping(value = "/getdynamicdata", method = RequestMethod.GET)
    public @ResponseBody
    String getTicketDynamicData(@RequestParam("ticketcategory") int ticketcategory, HttpSession session) {
        JSONObject object = new JSONObject();
        try {
            String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            int locationid = ticketService.getLocationByUsername(username);

            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN) {
                object = ticketService.getTerminalBrakdownCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION) {
                object = ticketService.getNewTerminalInstallationCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN) {
                object = ticketService.getSoftawreHardwareCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL) {
                object = ticketService.getMerchantRemovalCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION) {
                object = ticketService.getBaseSoftwareInstallationCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE) {
                object = ticketService.getTerminalUpgradeDowngradeCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL) {
                object = ticketService.getBackupRemovalCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BULK_TERMINAL_BREAKDOWN) {
                object = ticketService.getBulkTerminalBreakdownCreatePageData(username);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING) {
                object = ticketService.getTerminalSharingCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION) {
                object = ticketService.getTerminalConversionCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION) {
                object = ticketService.getReinitializationCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN) {
                object = ticketService.getTermialBreakdownMalayasiaCreatePageData(username, locationid);
            }
            if (ticketcategory == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR) {
                object = ticketService.getTermialRepairMalayasiaCreatePageData(username, locationid);
            }
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/getterminalmodels", method = RequestMethod.GET)
    public @ResponseBody
    String getUserStatusList(@RequestParam("terminalbrand") int terminalbrand) {
        JSONArray array = new JSONArray();
        try {
            array = ticketService.getTerminalModelDropdownByBrand(terminalbrand);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/getclientdropdowndetails", method = RequestMethod.GET)
    public @ResponseBody
    String getClientDropdownByClientCategory(@RequestParam("clientcategory") int clientcategory, HttpSession session) {
        JSONArray array = new JSONArray();
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

        try {
            int location = ticketService.getLocationByUsername(username);
            array = ticketService.getClientDropdownByClientCategory(clientcategory, location);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/getaccessorytype", method = RequestMethod.GET)
    public @ResponseBody
    String getAccessoryType(@RequestParam("status") int status) {
        JSONArray array = new JSONArray();
        try {
            array = ticketService.getAccessoryType(status);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/getproductcategory", method = RequestMethod.GET)
    public @ResponseBody
    String getProductCategory(@RequestParam("product") int product) {
        JSONArray array = new JSONArray();
        try {
            array = ticketService.getProductCategory(product);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/getManualAssignee", method = RequestMethod.GET)
    public @ResponseBody
    String getManualAssigneeListByTicketId(@RequestParam("ticketid") int ticketid,
            @RequestParam("manualassignstatus") String manualassignstatus,
            @RequestParam("status") int status,
            HttpSession session) {
        JSONArray array = new JSONArray();
        try {
            array = ticketService.getManualAssigneeListById(ticketid, String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)), manualassignstatus, status);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/getUserroleAuthForManualAssign", method = RequestMethod.GET)
    public @ResponseBody
    String getUserroleAuthForManualAssign(HttpSession session) {
        String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        JSONArray array = new JSONArray();
        try {
            array = userService.getUserroleAuthForManualAssign(username);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/getDefaultAssignee", method = RequestMethod.GET)
    public @ResponseBody
    String getDefaultAssigneeListByTicketId(@RequestParam("ticketid") int ticketid) {
        JSONArray array = new JSONArray();
        try {
            array = ticketService.getDefaultAssigneeListById(ticketid);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/validateSerialNumber", method = RequestMethod.GET)
    public @ResponseBody
    Boolean validateSerialNumber(@RequestParam("serialnumber") String serialnumber) {
        boolean isSerialNumberAvailable = false;
        try {
            isSerialNumberAvailable = ticketService.validateSerialNumber(serialnumber);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return isSerialNumberAvailable;
    }

    @RequestMapping(value = "/switchAssignee", method = RequestMethod.POST)
    public @ResponseBody
    String switchAssignee(@RequestParam("ticketid") int ticketid, HttpSession session) {
        JSONObject object = new JSONObject();
        try {
            ticketService.switchAssignee(ticketid, String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            object.put("message", "Ticket Reassigned");
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", "Error occured while ticket assignment");
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createbdtkt", method = RequestMethod.POST)
    public @ResponseBody
    String createTerminalBreakDownTicket(@RequestBody Ticket ticket, HttpSession session) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createTicket(ticket);
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createnitkt", method = RequestMethod.POST)
    public @ResponseBody
    String createNewInstallationTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createNewTerminalInstalationTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createswhwbreakdowntkt", method = RequestMethod.POST)
    public @ResponseBody
    String createSoftwareHardwareBreakdownTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createSoftwareHardwareBreakdownTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createmrtkt", method = RequestMethod.POST)
    public @ResponseBody
    String createMerchantRemovalTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createMerchantRemovalTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createbsi", method = RequestMethod.POST)
    public @ResponseBody
    String createBaseSoftwareInstallationTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createBaseSoftwareInstallationTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createtudtkt", method = RequestMethod.POST)
    public @ResponseBody
    String createTerminalUpgradeDowngradeTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createTerminalUpgradeDowngradeTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createbrt", method = RequestMethod.POST)
    public @ResponseBody
    String createBackupRemovalTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createBackupRemovalTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createtstkt", method = RequestMethod.POST)
    public @ResponseBody
    String createTerminalSharingTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createTerminalSharingTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createtctkt", method = RequestMethod.POST)
    public @ResponseBody
    String createTerminalConversionTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createTerminalConversionTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createritkt", method = RequestMethod.POST)
    public @ResponseBody
    String createReinitializationTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createReinitializationTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createbdmystkt", method = RequestMethod.POST)
    public @ResponseBody
    String createTerminalBrakdownMalayasiaTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createTerminalalBrakdownMalayasiaTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/createtrmystkt", method = RequestMethod.POST)
    public @ResponseBody
    String createTerminalRepairMalayasiaTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            int ticketId = ticketService.createTerminalRepairMalayasiaTicket(ticket);
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
            object.put("message", String.format(AffinitiMessages.TICKET_CREATE_SUCCESS.getMessage(), ticketId)
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_CREATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/getTerminaldata", method = RequestMethod.GET)
    public @ResponseBody
    String getTerminalInfo(HttpServletRequest request) {
        JSONObject object = null;
        try {
            object = ticketService.getTerminalInfo(request.getParameter("input"));
        } catch (SQLException ex) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return object.toString();
    }

    /*Start Ticket Update*/
    @RequestMapping(value = "/getticket", method = RequestMethod.GET)
    public String getTicket(ModelMap model, @RequestParam("ticketid") int ticketid) {
        try {
            model.put("root", "Ticket");
            model.put("page", "View / Edit");
            if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN) {
                model.put("ticketForm", ticketService.getTerminalBrakdownTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR) {
                model.put("ticketForm", ticketService.getTerminalRepairTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION) {
                model.put("ticketForm", ticketService.getNewInstallationTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN) {
                model.put("ticketForm", ticketService.getSoftwareHardwareTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL) {
                model.put("ticketForm", ticketService.getMerchantRemovalTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION) {
                model.put("ticketForm", ticketService.getBaseSoftwareInstallationTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE) {
                model.put("ticketForm", ticketService.getTerminalUpgradeDowngradeTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL) {
                model.put("ticketForm", ticketService.getBackupRemovalTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING) {
                model.put("ticketForm", ticketService.getTerminalSharingTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION) {
                model.put("ticketForm", ticketService.getTerminalConversionTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION) {
                model.put("ticketForm", ticketService.getReinitializationTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MAINTANACE_PAYMENT_INVOICE) {
                model.put("ticketForm", ticketService.getMaintainancePaymentInvoiceTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN) {
                model.put("ticketForm", ticketService.getMalaysianTerminalBreakdownTicketById(ticketid));
            } else if (ticketService.getTicket(ticketid).getTicketcategory().getTicketcategoryid()
                    == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR) {
                model.put("ticketForm", ticketService.getMalaysianTerminalRepairTicketById(ticketid));
            } else {
                model.put("ticketForm", new TerminalBreakdownTicket());
            }
        } catch (HibernateException | SQLException exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return "ticket/ticketviewedit";
    }

    @RequestMapping(value = "/getterminalbreakdownticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalBreakdownTicket getTerminalBreakdownTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalBreakdownTicket terminalBreakdownTicket = new TerminalBreakdownTicket();
        try {
            terminalBreakdownTicket = ticketService.getTerminalBrakdownTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalBreakdownTicket;
    }

    @RequestMapping(value = "/getNewInstallationticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    NewInstallationTicke getNewInstallationTicketDetail(@RequestParam("ticketid") int ticketid) {
        NewInstallationTicke newInstallationTicket = new NewInstallationTicke();
        try {
            newInstallationTicket = ticketService.getNewInstallationTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return newInstallationTicket;
    }

    @RequestMapping(value = "/getSoftwareHardwareBreakdownticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    SoftwareHardwareBreakdownTicket getSoftwareHardwareBreakdownTicketDetail(@RequestParam("ticketid") int ticketid) {
        SoftwareHardwareBreakdownTicket softwareHardwareBreakdownTicket = new SoftwareHardwareBreakdownTicket();
        try {
            softwareHardwareBreakdownTicket = ticketService.getSoftwareHardwareTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return softwareHardwareBreakdownTicket;
    }

    @RequestMapping(value = "/getBaseSoftwareInstallationticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    BaseSoftwareInstallationTicket getBaseSoftwareInstallationTicketDetail(@RequestParam("ticketid") int ticketid) {
        BaseSoftwareInstallationTicket baseSoftwareInstallationTicket = new BaseSoftwareInstallationTicket();
        try {
            baseSoftwareInstallationTicket = ticketService.getBaseSoftwareInstallationTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return baseSoftwareInstallationTicket;
    }

    @RequestMapping(value = "/getTerminalUpgradeDowngradeticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalUpgradeDowngradeTicket getTerminalUpgradeDowngradeTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalUpgradeDowngradeTicket terminalUpgradeDowngradeTicket = new TerminalUpgradeDowngradeTicket();
        try {
            terminalUpgradeDowngradeTicket = ticketService.getTerminalUpgradeDowngradeTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalUpgradeDowngradeTicket;
    }

    @RequestMapping(value = "/getBackupRemovalticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    BackupRemovalTicket getBackupRemovalTicketDetail(@RequestParam("ticketid") int ticketid) {
        BackupRemovalTicket backupRemovalTicket = new BackupRemovalTicket();
        try {
            backupRemovalTicket = ticketService.getBackupRemovalTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return backupRemovalTicket;
    }

    @RequestMapping(value = "/getmerchantremovalticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    MerchantRemovalTicket getMerchantRemovalTicketDetail(@RequestParam("ticketid") int ticketid) {
        MerchantRemovalTicket merchantRemovalTicket = new MerchantRemovalTicket();
        try {
            merchantRemovalTicket = ticketService.getMerchantRemovalTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return merchantRemovalTicket;
    }

    @RequestMapping(value = "/getterminalsharingticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalSharingTicket getTerminalSharingTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalSharingTicket terminalSharingTicket = new TerminalSharingTicket();
        try {
            terminalSharingTicket = ticketService.getTerminalSharingTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalSharingTicket;
    }

    @RequestMapping(value = "/getterminalconversionticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalConversionTicket getTerminalConversionTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalConversionTicket terminalconversionticket = new TerminalConversionTicket();
        try {
            terminalconversionticket = ticketService.getTerminalConversionTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalconversionticket;
    }

    @RequestMapping(value = "/getreinitializationticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    ReinitializationTicket getReinitializationTicketDetail(@RequestParam("ticketid") int ticketid) {
        ReinitializationTicket reinitializationticket = new ReinitializationTicket();
        try {
            reinitializationticket = ticketService.getReinitializationTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return reinitializationticket;
    }

    @RequestMapping(value = "/getmaintainacepaymentinvoice", method = RequestMethod.GET)
    public @ResponseBody
    MaintainancePaymentInvoiceTicket getMaintainacePaymentInvoiceTicketDetail(@RequestParam("ticketid") int ticketid) {
        MaintainancePaymentInvoiceTicket maintainancepaymentinvoiceticket = new MaintainancePaymentInvoiceTicket();
        try {
            maintainancepaymentinvoiceticket = ticketService.getMaintainancePaymentInvoiceTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return maintainancepaymentinvoiceticket;
    }

    @RequestMapping(value = "/getmalayterminalbreakdownticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalBreakdownTicket getMalaysianTerminalBreakdownTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalBreakdownTicket terminalbreakdownticket = new TerminalBreakdownTicket();
        try {
            terminalbreakdownticket = ticketService.getMalaysianTerminalBreakdownTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalbreakdownticket;
    }

    @RequestMapping(value = "/getmalayterminalrepairticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalRepairMysTicket getMalaysianTerminalRepairTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalRepairMysTicket terminalrepairmayticket = new TerminalRepairMysTicket();
        try {
            terminalrepairmayticket = ticketService.getMalaysianTerminalRepairTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalrepairmayticket;
    }

    @RequestMapping(value = "/update/getdynamicdata", method = RequestMethod.GET)
    public @ResponseBody
    String getUpdateTicketDynamicData(@RequestParam("ticketid") int ticketid, HttpSession session) {
        JSONObject object = new JSONObject();
        try {
            com.avn.affiniti.hibernate.model.Ticket ticket = ticketService.getTicket(ticketid);
            if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN) {
                object = ticketService.getTerminalBrakdownUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_REPAIR) {
                object = ticketService.getTerminalRepairUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_NEW_TERMINAL_INSTALLATION) {
                object = ticketService.getNewInstallationTicketUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_SOFTWARE_HARDWARE_BREAKDOWN) {
                object = ticketService.getSoftwareHardwareBreakdownUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MERCHANT_REMOVAL) {
                object = ticketService.getMerchantRemovalUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BASE_SOFTWARE_INSTALLATION) {
                object = ticketService.getBaseSoftwareInstallationUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_UPGRADE_DOWNGRADWE) {
                object = ticketService.getTerminalUpgradeDowngradeUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_BACKUP_REMOVAL) {
                object = ticketService.getBackupRemovalUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_SHARING) {
                object = ticketService.getTerminalSharingUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_CONVERSION) {
                object = ticketService.getTerminalConversionUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_REINTIALIZATION) {
                object = ticketService.getReinitializationUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_BREAKDOWN) {
                object = ticketService.getMalaysiaTerminalBrakdownUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            } else if (ticket.getTicketcategory().getTicketcategoryid() == MasterDataVarList.AFFINITI_TICKET_CATEGORY_MALAY_TERMINAL_REPAIR) {
                object = ticketService.getMalaysiaTerminalRepairUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                        ticketid);
            }
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/update/maintainancepayment", method = RequestMethod.GET)
    public @ResponseBody
    String getmaintainancePaymentUpdatePageData(@RequestParam("ticketid") int ticketid,
            @RequestParam("amcstatus") String amcstatus,
            HttpSession session) {
        JSONObject object = new JSONObject();

        try {
            object = ticketService.getMaintainacePaymentInvoiceUpdatePageData(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)),
                    amcstatus, ticketid);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatebdtkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateTerminalBreakdownTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            int newTicketId = ticketService.updateTerminalBrakdownTicket(ticket);
            int ticketId = ticket.getTicketid();
            if (newTicketId != ticketId) {
                ticketId = newTicketId;
            }
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 5 || statusId == 9 || statusId == 16 || statusId == 17 || statusId == 107) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticketId)));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatereptkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateTerminalRepairTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateTerminalRepairTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 9 || statusId == 11 || statusId == 14 || statusId == 21 || statusId == 25 || statusId == 27 || statusId == 28 || statusId == 29 || statusId == 32 || statusId == 33 || statusId == 34 || statusId == 163 || statusId == 30 || statusId == 123 || statusId == 124) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatenitkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateNewInstallationTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateNewInstallationTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 9 || statusId == 42 || statusId == 44 || statusId == 32 || statusId == 107 || statusId == 36) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updateswhwbtkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateSoftwareHardwareBreakdownTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateSoftwareHardwareBreakdownTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 49 || statusId == 64 || statusId == 107) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatemrtkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateMerchatRemovalTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateMerchantRemovalTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 32 || statusId == 36 || statusId == 56 || statusId == 57 || statusId == 107) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatebsitkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateBaseInstallationTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateBaseSoftawareInstallationTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 32 || statusId == 121 || statusId == 63) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatetudtkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateTerminalUpgradeDowngradeTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateTerminalUpgradeDowngradeTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 59 || statusId == 60 || statusId == 42 || statusId == 107) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatebrt", method = RequestMethod.POST)
    public @ResponseBody
    String updateBackupRemovalTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateBackupRemovalTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 9 || statusId == 75 || statusId == 42 || statusId == 93 || statusId == 119) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatetstkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateTerminalSharingTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateTerminalSharingTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 36 || statusId == 14 || statusId == 95 || statusId == 107) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatetctkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateTerminalConversionTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateTerminalConversionTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 36 || statusId == 14 || statusId == 95 || statusId == 107 || statusId == 99 || statusId == 98 || statusId == 96 || statusId == 97) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updateritkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateReinitializationTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateReinitializationTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 11 || statusId == 14 || statusId == 36 || statusId == 107 || statusId == 95 || statusId == 100 || statusId == 101 || statusId == 102) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatemalaybdtkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateMalaysianTerminalBreakdownTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateMalaysianTerminalBreakdownTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 71 || statusId == 14 || statusId == 75 || statusId == 77 || statusId == 78 || statusId == 80 || statusId == 81) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatemalaytrtkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateMalaysianTerminalRepairTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateMalaysianTerminalRepairTicket(ticket);
            int ticketId = ticket.getTicketid();
            int statusId = ticket.getStatus();
            if (statusId == 72 || statusId == 73 || statusId == 14 || statusId == 82 || statusId == 83 || statusId == 84 || statusId == 86 || statusId == 87 || statusId == 89 || statusId == 90 || statusId == 91 || statusId == 93) {
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESSFULLY.getMessage());
            } else {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception e) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                    e.printStackTrace();
                }
                object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                        + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/updatempitkt", method = RequestMethod.POST)
    public @ResponseBody
    String updateMaintenencePaymentInvoiceTicket(@RequestBody Ticket ticket, HttpSession session, HttpServletRequest request) {
        JSONObject object = new JSONObject();
        try {
            ticket.setCreateduser(String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME)));
            ticketService.updateMaintenencePaymentInvoiceTicket(ticket);
            int ticketId = ticket.getTicketid();
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
            object.put("message", AffinitiMessages.TICKET_UPDATE_SUCCESS.getMessage()
                    + " | " + String.format(AffinitiMessages.TICKET_ASSIGNEE.getMessage(), ticketService.getAssigneeByTicketId(ticket.getTicketid())));
            try {
                ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
            } catch (Exception e) {
                Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
                e.printStackTrace();
            }
        } catch (Exception e) {
            object.put("code", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            object.put("message", AffinitiMessages.TICKET_UPDATE_ERROR.getMessage());
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/getterminalrepairticketdetail", method = RequestMethod.GET)
    public @ResponseBody
    TerminalRepairTicket getTerminalRepairTicketDetail(@RequestParam("ticketid") int ticketid) {
        TerminalRepairTicket terminalRepairTicket = new TerminalRepairTicket();
        try {
            terminalRepairTicket = ticketService.getTerminalRepairTicketById(ticketid);
        } catch (Exception exception) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return terminalRepairTicket;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String pageView(Model model) {
        return "ticket/ticketsearch";
    }

    @RequestMapping(value = "/searched", method = RequestMethod.GET)
    public @ResponseBody
    String getTableData(HttpServletRequest request, HttpSession session) throws Exception {

        String response = "";
        String user = (String) session.getAttribute("username");
        String searchContent = request.getParameter("searchoptionID");
//        TicketParam parameters = (TicketParam) new ObjectMapper().readValue(searchContent, TicketParam.class);
        TicketParam parameters = new TicketParam();
        parameters.setInput(searchContent);
        parameters.setCreateduser(user);

        try {
            response = ticketService.getTicketData(request, parameters);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
        }

        return response;
    }

    @RequestMapping(value = "/uploadexcelfile", method = RequestMethod.POST)
    public @ResponseBody
    String getExcelFile(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws SQLException, ParseException {
        JSONObject jsonobject = new JSONObject();
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("files");

        Ticket ticket = new Ticket();

        ticket.setFile(multipartFile);
        ticket.setTicketdate(Common.getDateFromString(CommonVarList.DATE_FORMAT_yyyy_MM_dd, request.getParameter("ticketdate")));
        ticket.setTicketcategory(MasterDataVarList.AFFINITI_TICKET_CATEGORY_TERMINAL_BREAKDOWN);
        ticket.setTicketpriority(Integer.parseInt(request.getParameter("ticketpriority")));
        ticket.setStatus(Integer.parseInt(request.getParameter("status")));
        ticket.setCreateduser((String) session.getAttribute("username"));
        ticket.setManualassigneestatus((String) request.getParameter("manualassigneestatus"));
        ticket.setAssignee((String) request.getParameter("assignee"));

        jsonobject = this.uploadExcelFile(CommonVarList.CONTEXT_PARAM_BULK_TERMINAL_BREAKDOWN, ticket, request);

        return jsonobject.toString();
    }

    private JSONObject uploadExcelFile(String path, Ticket ticket, HttpServletRequest request) {
        JSONObject jsonobject = new JSONObject();
        String absolutePath = new File("/tmp").getAbsolutePath();
        OutputStream outputStream = null;
        File nfile;
        File case_attach_file = null;
        boolean filesizok = true;

        try {
            String uploadFileLocation;
            if (!(ticket.getFile().getSize() / 1024 <= maxFileSizeInKb)) {
                filesizok = false;
            }

            if (filesizok) {

                uploadFileLocation = absolutePath + File.separator + context.getInitParameter(path);
                nfile = new File(uploadFileLocation);

                if (!nfile.exists()) {
                    nfile.mkdirs();
                } else {
                    File[] dfiles = nfile.listFiles();
                    if (null != dfiles) {
                        for (int i = 0; i < dfiles.length; i++) {
                            dfiles[i].delete();
                        }
                    }
                }

                uploadFileLocation += File.separator + ticket.getFile().getOriginalFilename();
                case_attach_file = new File(uploadFileLocation);
                ticket.setFilepath(uploadFileLocation);

                if (case_attach_file != null) {
                    outputStream = new FileOutputStream(case_attach_file);
                    outputStream.write(ticket.getFile().getBytes());
                    outputStream.close();
                }

                ticketService.insertUploadedFileInfo(ticket);
                this.processExcelFile(ticket, request);
                jsonobject.put("CODE", "SUCCESS");
                jsonobject.put("MESSAGE", "File uploaded successfully");

            } else {
                jsonobject.put("CODE", "ERROR");
                jsonobject.put("MESSAGE", "Please upload maximum of 10mb Signature file.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            jsonobject.put("CODE", "ERROR");
            jsonobject.put("MESSAGE", "Failed to upload attachment");
        }

        return jsonobject;

    }

    private JSONObject processExcelFile(Ticket ticket, HttpServletRequest request) {
        JSONObject jsonobject = new JSONObject();
        String filepath = "";

        try {
//            int insertcount = ticketService.createBulkTerminalBreakdownTicket(ticket);
            List<Integer> ticketIdList = ticketService.createBulkTerminalBreakdownTicket(ticket);
            for (Integer ticketId : ticketIdList) {
                try {
                    ticketService.sendTicketAssignmentNotificationEmail(ticketId, request);
                } catch (Exception ex) {
                    Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, ex);
                    ex.printStackTrace();
                }
            }
            jsonobject.put("CODE", "SUCCESS");
//            jsonobject.put("INSERTCOUNT", insertcount);
            jsonobject.put("INSERTCOUNT", ticketIdList.size());
            deleteFileFromDirectroy(filepath);
//        } catch (IOException | SQLException | JSONException | ParseException ex) {
        } catch (Exception ex) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            jsonobject.put("CODE", "ERROR");
            jsonobject.put("MESSAGE", "Failed to upload attachment");
        }

        return jsonobject;
    }

    /*Delete uploaded file from the directory*/
    private void deleteFileFromDirectroy(String filepath) {

        File file = new File(filepath);

        if (file.exists()) {
            file.delete();
        }
    }

    @RequestMapping(value = "/ticketHistoryData", method = RequestMethod.POST)
    public @ResponseBody
    String getTicketHistoryData(HttpServletRequest request) throws Exception {

        JSONObject response = new JSONObject();
        try {
            response = ticketService.getTicketHistoryData(request);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
        }
        return response.toString();
    }

    @RequestMapping(value = "/statuscodebyticket", method = RequestMethod.GET)
    public @ResponseBody
    String getStatusByTicket(@RequestParam("ticketid") int ticketid, HttpSession session) {
        JSONObject object = new JSONObject();

        try {
            object = ticketService.getStatusCodeByTicketId(ticketid);
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return object.toString();
    }

    @RequestMapping(value = "/getterminalsharingticketxml", method = RequestMethod.GET, produces = "application/xml")
    public @ResponseBody
    String getTerminalSharingTicketXML(HttpServletRequest request, HttpSession session) {
        String xml = "";
        try {
            xml = ticketService.generateTerminalSharingTicketXML(Integer.parseInt(String.valueOf(request.getParameter("ticketid"))));

        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return xml;
    }

    @RequestMapping(value = "/getnewterminalinstallationticketxml", method = RequestMethod.GET, produces = "application/xml")
    public @ResponseBody
    String getNewTerminalInstallationTicketXML(HttpServletRequest request, HttpSession session) {
        String xml = "";
        try {
            xml = ticketService.generateNewTerminalInstallationTicketXML(Integer.parseInt(String.valueOf(request.getParameter("ticketid"))));
        } catch (Exception e) {
            Logger.getLogger(TicketController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return xml;
    }
}
