
package com.avn.affiniti.controller.audittrace;

import com.avn.affiniti.hibernate.model.Audittrace;
import com.avn.affiniti.service.audittrace.AudittraceService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ASHOK
 */
@Controller
@RequestMapping("/audittrace")
public class AudittraceController {    
    
    @Autowired
    AudittraceService audittraceService;
    
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String viewSectionPage(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "Audit Trace");
            model.put("audittraceSearch", new Audittrace());
            audittraceService.loadPageComponent(model);
            
        } catch (Exception exception) {
            Logger.getLogger(AudittraceController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return "audittrace/audittrace";
    }
    
    
    @RequestMapping(value = "/tableLoad", method = RequestMethod.POST)
    public @ResponseBody String loadSectionTable(HttpServletRequest request) {

        JSONObject responce = new JSONObject();
        try {
            responce = audittraceService.getSectionTableData(request);
        } catch (Exception ex) {
            
            responce.put("status", 500);
            responce.put("success", false);
            responce.put("messageController", ex);
        }
        return responce.toString();
    }
    
    
}
