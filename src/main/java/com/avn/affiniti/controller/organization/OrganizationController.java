package com.avn.affiniti.controller.organization;

import com.avn.affiniti.hibernate.model.Employee;
import com.avn.affiniti.hibernate.model.Organization;
import com.avn.affiniti.service.inventory.InventoryService;
import com.avn.affiniti.service.organization.OrganizationService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Kaushan Fernando
 * @Document OrganizationController
 * @Created on: Sep 19, 2017, 9:32:42 PM
 */
@Controller
@RequestMapping(value = "/organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;
    @Autowired
    private InventoryService inventoryService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "Organization");
            model.put("organizationAddForm", new Organization());
            model.put("organizationSearchForm", new Organization());

            organizationService.loadPageComponent(model);
        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Database Error");
        } catch (JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : JSON Error");
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            model.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            model.put("success", false);
            model.put("message", "Error : Error");
        }
        return "organization/organization";
    }

    @RequestMapping(value = "/getorganizationdetailstotable", method = RequestMethod.POST)
    public @ResponseBody
    String loadSectionTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            details = organizationService.getOrganizationDetailsTableData(request,employee.getLocation().getLocationid(), createdUser);
        } catch (JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", ex);
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", ex);
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", ex);
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        }
        return details.toString();
    }

    @RequestMapping(value = "/addorganization", method = RequestMethod.POST)
    public @ResponseBody
    String addOrganization(HttpServletRequest request, HttpSession session, @RequestParam("action") String action) {
        JSONObject output = new JSONObject();
        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);            
            output = organizationService.saveOrganization(request, session, action, employee.getLocation().getLocationid());

        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            if (action.equals("save")) {
                output.put("message", String.format(MessageVarList.SAVE_ERROR, "organization"));

            } else {
                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "organization"));
            }

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            if (action.equals("save")) {
                output.put("message", String.format(MessageVarList.SAVE_ERROR, "organization"));

            } else {
                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "organization"));
            }
        }
        return output.toString();
    }

    @RequestMapping(value = "/getOrganiztion", method = RequestMethod.GET)
    public @ResponseBody
    String getOrganizationsByOrganizationId(HttpServletRequest request,HttpSession session) {

        int organizationId = Integer.parseInt(request.getParameter("organizationId"));

        JSONObject objData = new JSONObject();

        try {
            objData = organizationService.getOrganization(organizationId,session);

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading organization details - please try again!");

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            objData.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            objData.put("success", false);
            objData.put("message", "Error : Error while loading organization details - please try again!");

        }

        return objData.toString();
    }
    
    @RequestMapping(value = "/addlocation", method = RequestMethod.GET)
    public @ResponseBody
    String OrganizationLoc(HttpServletRequest request, HttpSession session) {
        Organization output = new Organization();
        try {
            session = request.getSession(true);
            String username = session.getAttribute("username").toString();
            Employee employee = inventoryService.getEmployeeByUsername(username);            
            output = organizationService.OrganizationLoc(request, session,employee.getLocation().getLocationid());

        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
           // output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
           // output.put("success", false);
            

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
           ex.printStackTrace();
           // output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
           // output.put("success", false);
           
        }
        return output.toString();
    }

}
