package com.avn.affiniti.controller.userroletask;

import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.hibernate.model.Userroletask;
import com.avn.affiniti.service.userroletask.UserRoleTaskService;
import com.avn.affiniti.model.userroletask.UserRoleTask;
import com.avn.affiniti.service.userrole.UserroleService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.DropdownSqlVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author Kaushan Fernando
 * @Document UserRoleTaskController
 * @Created on 09/08/2017, 2:24:04 PM
 */
@Controller
@RequestMapping(value = "/userroletask")
public class UserRoleTaskController {

    @Autowired
    private UserRoleTaskService userRoleTaskService;

    @Autowired
    private UserroleService UserRoleService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "User Role Subsection Task");
            model.put("userRoleTaskSearchForm", new Userrolesubsection());
            model.put("userRoleTaskAddForm", new Userrolesubsection());

            userRoleTaskService.loadPageComponent(model);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);

        }
        return "userroletask/userroletask";
    }

    @RequestMapping(value = "/getuserrroletasktotable", method = RequestMethod.POST)
    public @ResponseBody
    String loadSectionTable(HttpServletRequest request, HttpSession session) {

        JSONObject details = new JSONObject();

        try {

            /**
             * **********--Audit Trace Part--**************
             */
            String page = "User Role Sub Section Task";
            String task = "View";
            String description = "";
            String affectedId = "";
            String userRoleId = request.getParameter("userroleid");

            if (userRoleId.equals("") || userRoleId.equals("--Select--")) {
                task = "Table View";
                description = "View all details in UserroleSubsection Task Table ";
                affectedId = "";
            } else {
                JSONArray data = new JSONArray();
                String userRoleDescription = "";

                data = UserRoleService.getUserroleDescriptionByUserRoleId(String.format(DropdownSqlVarList.AFFINITI_DROPDOWN_CLIENT_USERROLES,
                        userRoleId
                ));
                for (int i = 0; i < data.length(); i++) {
                    JSONObject jSONObject = data.getJSONObject(i);
                    if (jSONObject.getString("id").equalsIgnoreCase(userRoleId)) {
                        userRoleDescription = jSONObject.getString("value");
                    }
                }
                
                task = "Table Search";
                description = "Table Search details in Userroletask by user role id " + userRoleId + " user role name " + userRoleDescription;
                affectedId = userRoleId;

            }
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            details = userRoleTaskService.getUserRoleTaskTableData(request, createdUser);
            userRoleTaskService.insertAuditTrace(page, affectedId, task, description, createdUser); //insert audittrace log
            /**
             * ********--Audit Trace End--**********
             */

        } catch (HibernateException | SQLException | JSONException ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", "Un-successfull");
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            details.put("status", 500);
            details.put("success", false);
            details.put("message", "Un-successfull");
            details.put("sEcho", 0);
            details.put("iTotalRecords", 0);
            details.put("iTotalDisplayRecords", 0);
        }
        return details.toString();
    }

    @RequestMapping(value = "/getuserrolesections", method = RequestMethod.GET)
    public @ResponseBody
    String getUserRoleSectionsByUserRoleId(@RequestParam("userroleid") int userRoleId) {

        JSONObject objData = new JSONObject();

        try {
            objData = userRoleTaskService.getUserRoleSectionsByUserRoleId(userRoleId);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);

        }

        return objData.toString();
    }

    @RequestMapping(value = "/getuserrolesubsections", method = RequestMethod.GET)
    public @ResponseBody
    String getUserRoleSubSectionsBySectionId(HttpServletRequest request) {

        int userRoleId = Integer.parseInt(request.getParameter("userroleId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));

        JSONObject objData = new JSONObject();

        try {
            objData = userRoleTaskService.getUserRoleSubSectionsBySectionId(sectionId, userRoleId);

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);

        }

        return objData.toString();
    }

    @RequestMapping(value = "/gettasklist", method = RequestMethod.GET)
    public @ResponseBody
    String getTaskLIstById(HttpServletRequest request,HttpSession session,UserRoleTask userRoleTask) {

        int userRoleId = Integer.parseInt(request.getParameter("userRoleId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        int subSectionId = Integer.parseInt(request.getParameter("subSectionId"));

        JSONObject objData = new JSONObject();

        try {
            objData = userRoleTaskService.getTaskLIstById(userRoleTask,userRoleId, sectionId, subSectionId,session);

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);

        }

        return objData.toString();
    }

    @RequestMapping(value = "/adduserroletask", method = RequestMethod.POST)
    public @ResponseBody
    String AddUserRoleTask(@RequestBody UserRoleTask[] userRoleTasks, HttpSession session, @RequestParam("selectfunction") String selectFunction, @RequestParam("userroleid") int userRoleId, @RequestParam("sectionid") int sectionId, @RequestParam("subsectionid") int subSectionId) {
        JSONObject output = new JSONObject();

        try {

            int userRoleSubsectionId = userRoleTaskService.findUserroleSubSectionId(userRoleId, sectionId, subSectionId);
            List<Userroletask> listOfOldObject = userRoleTaskService.getUserRoleTaskList(userRoleSubsectionId);

            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));

            output = userRoleTaskService.saveUserRoleTask(userRoleTasks, listOfOldObject, createdUser, selectFunction);

        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", String.format(MessageVarList.ASSIGN_ERROR, "tasks"));
//            if (selectFunction.equals("save")) {
//                output.put("message", String.format(MessageVarList.ASSIGN_ERROR, "tasks"));
//
//            } else {
//                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "tasks"));
//            }

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            output.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("success", false);
            output.put("message", String.format(MessageVarList.ASSIGN_ERROR, "tasks"));
//            if (selectFunction.equals("save")) {
//                output.put("message", String.format(MessageVarList.ASSIGN_ERROR, "tasks"));
//            } else {
//                output.put("message", String.format(MessageVarList.UPDATE_ERROR, "tasks"));
//            }

        }
        return output.toString();
    }

}
