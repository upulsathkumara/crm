/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.user;

//import com.avn.affiniti.daoimpl.userrole.UserRoleDAOImpl;
import com.avn.affiniti.model.clientsystemuser.ClientSystemUser;
import com.avn.affiniti.model.user.User;
import com.avn.affiniti.service.user.UserService;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author : Roshen Dilshan
 * @Document : UserController
 * @Created on : Sep 14, 2015, 3:18:40 PM
 */
@Controller
public class UserController {

    @Autowired
    ServletContext context;
//    @Autowired
//    UserDAOImpl userDAOImpl;
//    @Autowired
//    UserRoleDAOImpl userRoleDAOImpl;
//    @Autowired
//    StatusDAOImpl statusDAOImpl;
//    @Autowired
//    AuditTraceDAOImpl audittraceDaoImpl;
//    @Autowired
//    BranchDAOImpl branchDAOImpl;
//    @Autowired
//    EmployeeDAOImpl employeeDAOImpl;
//    @Autowired
//    EmployeeCategoryDAOImpl employeeCategoryDAOImpl;
//    @Autowired
//    CommonDAOImpl commonDAOImpl;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String pageView(Map<String, Object> model, HttpSession session) {
        User user = new User();
        model.put("user", user);
        try {
//            audittraceDaoImpl.insertAuditTrace("User Search Page View ", String.valueOf(MasterDataVarList.CCL_CODE_VIEW), " view ", "", (String) session.getAttribute("username"));
        } catch (Exception exception) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, exception);
            Loggers.LOGGER_FILE.error(exception);
        }
        return "user/usersearch";
    }

    @RequestMapping(value = "/user/search/tabledata", method = RequestMethod.GET)
    public @ResponseBody
    String getTableData(HttpServletRequest request, HttpSession session) throws Exception {
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        int iTotalRecords = 0; // total number of records (unfiltered)

        int iTotalDisplayRecords = 0;//value will be set when code filters data by keyword
        JSONArray rows = new JSONArray();
        try {

            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }

            if (Boolean.valueOf(request.getParameter("search"))) {
                User user = this.getParameters(request);

                iTotalRecords = userService.getTableDataCount(user, session);
                iTotalDisplayRecords = iTotalRecords;
                if (iTotalRecords > 0) {
                    list = userService.getTableData(user, param.iDisplayStart, param.iDisplayLength, session);
                    for (JSONObject jSONObject : list) {
                        jSONObject.put("action", String.format(jSONObject.getString("action"), request.getContextPath(), request.getContextPath()));
                    }
                    rows = new JSONArray(list);
                }
//                audittraceDaoImpl.insertAuditTrace("User Search Page ", String.valueOf(MasterDataVarList.CCL_CODE_SEARCH), "User Searched, Parameter: " + (request.getParameter("username") == "" ? "ALL" : request.getParameter("username")), request.getParameter("username"), (String) session.getAttribute("username"));
            }
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("sEcho", sEcho);
        jsonResponse.put("iTotalRecords", iTotalRecords);
        jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
        jsonResponse.put("aaData", rows);
        return jsonResponse.toString();
    }

    @RequestMapping(value = "/user/create/view", method = RequestMethod.GET)
    public String createViewUser(@ModelAttribute("user") User user, Map<String, Object> model, HttpSession session) {
        try {
            userService.loadPageComponent(model, session);
//            user.setSelstatus(String.valueOf(MasterDataVarList.AFFINITI_CODE_STATUS_INACTIVE));
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
            Loggers.LOGGER_FILE.error(e);
        }
        return "user/usercreate";
    }

    @RequestMapping(value = "/user/getterritoriesh", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getTerritoriesByHierarichy(@RequestParam("hierarichy") int hierarichy) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
            list = userService.getTerritoryListByOrgHierarchy(hierarichy);
            Common.removeElementFromMap("", list);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/getterritorieshp", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getTerritoriesByHierarichyBranch(@RequestParam("hierarichy") int hierarichy, @RequestParam("product[]") Integer[] product) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
//            list = userService.getTerritoryListByOrgHierarchyProduct(hierarichy, product);
            Common.removeElementFromMap("", list);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/getproductcategorytypes", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getProductCategoryTypesByProduct(@RequestParam("product[]") Integer[] product) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
//            list = userService.getProductCategoryTypesByProduct(product);
            Common.removeElementFromMap("", list);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/getsupervisor", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getSupervisorByHierarchy(@RequestParam("hierarichy") int hierarichy) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
            list = userService.getSupervisorByHierarchy(hierarichy);
            Common.removeElementFromMap("", list);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.POST)
    public @ResponseBody
    String createUser(@RequestBody User user, HttpServletRequest request, HttpSession session) {
        JSONObject object = new JSONObject();
        long employeeid = 0;
        try {
            if (!userService.isUserExists(user)) {
                user.setCreateduser(String.valueOf(session.getAttribute("username")));
//                user.setStatus(MasterDataVarList.AFFINITI_CODE_STATUS_INACTIVE);
                employeeid = userService.insertUser(user, request, session);
                object.put("code", "SUCCESS");
//                object.put("message", "Employee created, Emplyee ID : <strong>" + employeeid + "</strong>");
                object.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "User"));

//                audittraceDaoImpl.insertAuditTrace("User Create Page ", String.valueOf(MasterDataVarList.CCL_CODE_CREATE), "New User Created, Employee ID: " + employeeid + "", request.getParameter("username"), (String) session.getAttribute("username"));
//                userService.sendPasswordMail(request, user);
            } else {
                object.put("code", "ERROR");
                object.put("message", "User already exists");
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            object.put("code", "ERROR");
            object.put("message", String.format(MessageVarList.SAVE_ERROR, "user"));
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, sqle);
            Loggers.LOGGER_FILE.error(sqle);
        } catch (MailException exception) {
            exception.printStackTrace();
            object.put("code", "ERROR");
            object.put("message", "User created, Emplyee ID : <strong>" + employeeid + "</strong>,<br/>Mail sending failed. Please resend the password mail manually");
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, exception);
            Loggers.LOGGER_FILE.error(exception);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return object.toString();
    }

//    @RequestMapping(value = "/user/isemployeeexists", method = RequestMethod.POST)
//    public @ResponseBody
//    String isEmployeeExists(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        String username = request.getParameter("username");
//        String endpoint = context.getInitParameter(CommonVarList.CONTEXT_PARAM_SERVICE_ADMIN_MANAGEMENT_READ_DSLK);
//        int isnotvalid = 3;
//        JSONObject jSONObject = new JSONObject();
//        jSONObject.put("CODE", 3);
//        try {
//            User newuser = new User();
//            newuser.setUserid(username);
//            if (!userDAOImpl.isUserExists(newuser)) {
//                UserDetail userDetails = new CCLServiceClient().getUserDetails(endpoint, username);
//                if (userDetails.getResponse() != null) {
//                    if (employeeDAOImpl.isEmployeeExists(userDetails.getResponse().getEPFNO())) {
//                        isnotvalid = 1;
//                        jSONObject.put("CODE", 1);
//                        jSONObject.put("EMAIL", employeeDAOImpl.getEmployeeEmailByEPF(userDetails.getResponse().getEPFNO()));
//                    } else {
//                        isnotvalid = 0;
//                        jSONObject.put("CODE", 0);
//                        jSONObject.put("DOAMIN", CommonVarList.EMAIL_DOAMIN);
//                    }
//                }
//            } else {
//                isnotvalid = 4;
//                jSONObject.put("CODE", 4);
//            }
//
//        } catch (Exception e) {
//            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return jSONObject.toString();
//    }
//    @RequestMapping(value = "/user/userreset", method = RequestMethod.POST)
//    public @ResponseBody
//    String reSetUserAttempts(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        String username = request.getParameter("username");
//        JSONObject jSONObject = new JSONObject();
//        try {
////            userDAOImpl.reSetUserAttempts(username);
//            jSONObject.put("CODE", "SUCCESS");
//            jSONObject.put("MESSAGE", "User Attempts Re-Set Successfully.");
//        } catch (Exception e) {
//            jSONObject.put("CODE", "ERROR");
//            jSONObject.put("MESSAGE", "Error Occurred While Re-Set User Attempts Customer.");
//            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
//        }
//        return jSONObject.toString();
//    }
    @RequestMapping(value = "/user/userresetpassword", method = RequestMethod.POST)
    public @ResponseBody
    String passwordReSet(@RequestParam("username") String username, HttpSession session) {
        JSONObject jSONObject = new JSONObject();
        try {
            String password = Long.toHexString(Double.doubleToLongBits(Math.random()));
            userService.passwordReSet(username, password);
            User user = userService.getUserByUsername(username);
            user.setPassword(password);
            jSONObject.put("code", "SUCCESS");
            jSONObject.put("message", "User Password Re-Set");
//            audittraceDaoImpl.insertAuditTrace("Password Reset", "Reset", "Password Reset, Username: " + username + "", user.getUserid(), (String) session.getAttribute("username"));
            userService.sendPasswordResetMail(user);
        } catch (SQLException sqle) {
            jSONObject.put("code", "ERROR");
            jSONObject.put("message", "Error Occurred While Re-Setting");
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, sqle);
            Loggers.LOGGER_FILE.error(sqle);
        } catch (MailException | MessagingException messagingException) {
            jSONObject.put("code", "ERROR");
            jSONObject.put("message", "User Password Re-Set,<br/>Mail sending failed. Please resend the password mail manually");
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, messagingException);
            Loggers.LOGGER_FILE.error(messagingException);
        } catch (Exception ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        return jSONObject.toString();
    }

    @RequestMapping(value = "/user/view", method = RequestMethod.GET)
    public String viewUser(@RequestParam("employeeid") long employeeid, Map<String, Object> model, HttpSession session) {
        try {
            userService.loadPageComponent(model, session);
            userService.getUserData(employeeid, model, session);
            userService.loadPageComponentView(model, ((User) model.get("user")).getEmployeeid());
//            audittraceDaoImpl.insertAuditTrace("User View Page ", String.valueOf(MasterDataVarList.CCL_CODE_VIEW), " User viewed, Employee ID: " + employeeid + "", String.valueOf(employeeid), (String) session.getAttribute("username"));
        } catch (Exception e) {
            model.put("errorMsg", "Failed to view user.");
            model.put("user", new User());
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return "user/userview";
    }

    @RequestMapping(value = "/user/view/isvalidnic", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateNic(@RequestParam("nic") String nic) {
        boolean isNotaDuplicateNic = false;
        try {
            isNotaDuplicateNic = userService.isNotaDuplicateNic(nic);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateNic;
    }

    @RequestMapping(value = "/user/view/isvalidemail", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateEmail(@RequestParam("email") String email) {
        boolean isNotaDuplicateEmail = false;
        try {
            isNotaDuplicateEmail = userService.isNotaDuplicateEmail(email);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateEmail;
    }

    @RequestMapping(value = "/user/view/isvalideepf", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateEPF(@RequestParam("epf") String epf) {
        boolean isNotaDuplicateEPF = false;
        try {
            isNotaDuplicateEPF = userService.isNotaDuplicateEpf(epf);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateEPF;
    }

    @RequestMapping(value = "/user/view/isvaliduserid", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateUserId(@RequestParam("username") String username) {
        boolean isNotaDuplicateUserid = false;
        try {
            isNotaDuplicateUserid = userService.isNotaDuplicateUsername(username);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateUserid;
    }

    @RequestMapping(value = "/user/view/isvaliduseridupdate", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateUserIdUpdate(@RequestParam("username") String username, @RequestParam("employeeid") String employeeid) {
        boolean isNotaDuplicateUserid = false;
        try {
            isNotaDuplicateUserid = userService.isNotaDuplicateUsernameUpdate(username, employeeid);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateUserid;
    }

    @RequestMapping(value = "/user/view/isvalidemailupdate", method = RequestMethod.GET)
    public @ResponseBody
    boolean isNotaDuplicateEmailUpdate(@RequestParam("email") String email, @RequestParam("employeeid") String employeeid) {
        boolean isNotaDuplicateUserid = false;
        try {
            isNotaDuplicateUserid = userService.isNotaDuplicateEmailUpdate(email, employeeid);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return isNotaDuplicateUserid;
    }

    @RequestMapping(value = "/user/update/view", method = RequestMethod.GET)
    public String viewUpdateUser(@RequestParam("employeeid") long employeeid, Map<String, Object> model, HttpSession session) {
        try {
            userService.loadPageComponent(model, session);
            userService.getUserData(employeeid, model, session);
            User user = (User) model.get("user");
            Integer[] products = Common.getSetToIntegerArray(userService.getUserSeletedProductList(user.getEmployeeid()).keySet());
//            if (products != null && products.length > 0) {
////                model.put("productsCategoryTypelIst", Common.removeElementFromMap("", userService.getProductCategoryTypesByProduct(products)));
//                model.put("territoryList", Common.removeElementFromMap("", userService.getTerritoryListByOrgHierarchyProduct(user.getHierarchyid(), products)));
//            } else {
//                model.put("territoryList", Common.removeElementFromMap("", userService.getTerritoryListByOrgHierarchy(user.getHierarchyid())));
//            }
            model.put("supervisorsList", Common.removeElementFromMap("", userService.getSupervisorByHierarchy(user.getHierarchyid())));
            user.setProductsup(products);
//            user.setProductscategorytypeup(Common.getSetToIntegerArray(userService.getUserSeletedProductCategoryTypeList(user.getEmployeeid()).keySet()));
            user.setTerritoriesup(Common.getSetToIntegerArray(userService.getUserSeletedTerritoryList(user.getEmployeeid()).keySet()));
            user.setSupervisorsup(Common.getSetToIntegerArray(userService.getUserSeletedSupervisorList(user.getEmployeeid()).keySet()));
//            audittraceDaoImpl.insertAuditTrace("User Update Page ", String.valueOf(MasterDataVarList.CCL_CODE_VIEW), "User Viewed, Employee ID: " + employeeid + "", String.valueOf(employeeid), (String) session.getAttribute("username"));
        } catch (Exception e) {
            model.put("errorMsg", "Failed to view user.");
            model.put("user", new User());
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return "user/userupdate";
    }

    @RequestMapping(value = "/user/update/userproduct", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getUserSeletedProductList(@RequestParam("employeeid") long employeeid) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
            list = userService.getUserSeletedProductList(employeeid);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/update/userterritory", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getUserSeletedTerritoryList(@RequestParam("employeeid") long employeeid) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
            list = userService.getUserSeletedTerritoryList(employeeid);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/update/userproductcategorytype", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getUserSeletedProductCategoryTypeList(@RequestParam("employeeid") long employeeid) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
            list = userService.getUserSeletedProductCategoryTypeList(employeeid);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/update/usersupervisor", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> getUserSeletedSupervisorList(@RequestParam("employeeid") long employeeid) {
        Map<String, String> list = new LinkedHashMap<>();
        try {
            list = userService.getUserSeletedSupervisorList(employeeid);
        } catch (Exception e) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        return list;
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.POST)
    public @ResponseBody
    String updateUser(@RequestBody User user, HttpSession session) {
        JSONObject object = new JSONObject();
        try {
            user.setCreateduser(String.valueOf(session.getAttribute("username")));
            userService.updateUser(user, session);
            object.put("CODE", "SUCCESS");
            object.put("MESSAGE", String.format(MessageVarList.UPDATED_SUCCESSFULLY, "User"));
//            audittraceDaoImpl.insertAuditTrace("User update page", String.valueOf(MasterDataVarList.CCL_CODE_UPDATE), "User updated, Username: " + user.getUserid() + "", user.getUserid(), (String) session.getAttribute("username"));
        } catch (Exception e) {
            object.put("CODE", "ERROR");
            object.put("MESSAGE", String.format(MessageVarList.UPDATE_ERROR, "user"));
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
            Loggers.LOGGER_FILE.error(e);
        }
        return object.toString();
    }

    private User getParameters(HttpServletRequest request) throws Exception {
        User user = new User();
//        if (request.getParameter("username") != null && !request.getParameter("username").trim().isEmpty()) {
////            user.setUserid(request.getParameter("username"));         
//        }

        if (request.getParameter("searchKeyword") != null && !request.getParameter("searchKeyword").trim().isEmpty()) {
            user.setSearchKeyword(request.getParameter("searchKeyword"));
        }
        return user;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String createRegisterView(@RequestParam("username") String username, @RequestParam("token") String token, ModelMap model) {
//        if (clientService.validateClientRegistrationToken(email, token)) {
//            model.put("clientEmail", email);
//            model.put("clientToken", token);
//            return "client/register";
//        }
//        return "errorpages/error404";
        int code = 0;
        try {
            boolean isValidUser = userService.isValidUser(username, token);
            code = userService.validateUserRegistrationToken(username, token, isValidUser);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }
        if (code == 1) {
            model.put("username", username);
            model.put("userToken", token);
            model.put("tokenExpired", true);
            return "user/register";
        } else if (code == 2) {
            model.put("username", username);
            model.put("userToken", token);
            model.put("tokenExpired", false);
            return "user/register";
        } else {
            return "errorpages/error404";
        }

    }

    @RequestMapping(value = "/reset/completeregistration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String completeRegistration(@RequestBody ClientSystemUser user) {
        JSONArray data = new JSONArray();

        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
        output.put("SUCCESS", false);
        output.put("MESSAGE", "Something wrong! Please refesh the page.");
        output.put("DATA", data);

        try {
            boolean isValidUser = userService.isValidUser(user.getUsername(), user.getToken());
            if (userService.completeUserRegistration(user.getUsername(), user.getToken(), user.getPassword(), isValidUser)) {
                output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                output.put("SUCCESS", true);
                output.put("MESSAGE", "Registration complete!.");
            }

        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", "Error : Error");
            ex.printStackTrace();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }

    @RequestMapping(value = "/reset/resendlink", method = RequestMethod.POST)
    public @ResponseBody
    String resendLink(@RequestParam("username") String username, @RequestParam("token") String token, HttpServletRequest request) {
        JSONArray data = new JSONArray();

        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
        output.put("SUCCESS", false);
        output.put("MESSAGE", "Something wrong! Please refesh the page.");
        output.put("DATA", data);

        try {

            boolean isValidUser = userService.isValidUser(username, token);
            if (userService.resendRegistrationLink(username, token, request, isValidUser)) {
                output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                output.put("SUCCESS", true);
                output.put("MESSAGE", "Email sent. Please check your email!.");
            }

        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", "Error : Error");
            ex.printStackTrace();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }

    @RequestMapping(value = "/user/passwordchange/view", method = RequestMethod.GET)
    public String createPasswordChangeView() {
        return "user/passwordchange";
    }

    @RequestMapping(value = "/user/passwordchange", method = RequestMethod.POST)
    public @ResponseBody
    String changePassword(HttpServletRequest request, HttpSession session) {
        JSONArray data = new JSONArray();

        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
        output.put("SUCCESS", false);
        output.put("MESSAGE", "Something wrong! Please refesh the page.");
        output.put("DATA", data);

        try {
            String username = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            String currentPassword = String.valueOf(request.getParameter("cpassword"));
            boolean isValidUser = userService.isValidUser(username, currentPassword);
            if (userService.changePassword(isValidUser, request, session)) {
                output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                output.put("SUCCESS", true);
                output.put("MESSAGE", "Password successfully changed");
            } else {
                output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
                output.put("SUCCESS", false);
                output.put("MESSAGE", "Current password is wrong");
            }

        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", "Error : Error");
            ex.printStackTrace();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }

    @RequestMapping(value = "/user/passwordreset/view", method = RequestMethod.GET)
    public String createPasswordResetView() {
        return "user/passwordreset";
    }

    @RequestMapping(value = "/user/passwordreset", method = RequestMethod.POST)
    public @ResponseBody
    String resetPassword(HttpServletRequest request, HttpSession session) {
        JSONArray data = new JSONArray();

        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
        output.put("SUCCESS", false);
        output.put("MESSAGE", "Something wrong! Please refesh the page.");
        output.put("DATA", data);

        try {
            if (userService.resetPassword(request, session)) {
                output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
                output.put("SUCCESS", true);
                output.put("MESSAGE", "Password reset email sent.");
            } else {
                output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
                output.put("SUCCESS", false);
                output.put("MESSAGE", "Username does not exits.");
            }

        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", "Error : Error");
            ex.printStackTrace();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }
}
