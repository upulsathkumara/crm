/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.subsection;

import com.avn.affiniti.dao.subsection.SubsectionDAO;
import com.avn.affiniti.model.subsection.Subsection;
import com.avn.affiniti.service.subsection.SubsectionService;
import com.avn.affiniti.util.Loggers;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author chandima
 */
@Controller
@RequestMapping("/subsection")
public class SubSectionController {

    @Autowired
    SubsectionService subsectionService;

    @Autowired
    SubsectionDAO subsectionDAO;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createview(ModelMap model) {
        try {
            model.put("root", "SubSection");
            model.put("page", "Subsection Add");
            model.put("SubsectionAddform", new com.avn.affiniti.hibernate.model.Subsection());
            subsectionService.loadPageComponent(model);
        } catch (Exception ex) {
            Logger.getLogger(SubSectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "subsection/subsection";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public @ResponseBody
    String searchview(HttpServletRequest request) {
        try {
            int max = Integer.parseInt(request.getParameter("max"));
            JSONObject array = subsectionService.getSubsectionList(request, max);
            return array.toString();
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            return "Fail";
        }
    }

    @RequestMapping(value = "/loadTable", method = RequestMethod.POST)
    public @ResponseBody
    String loadSectionTasksTable(HttpServletRequest request) {
        JSONObject response = new JSONObject();
        try {
            String page = "Subsection Table View";
            String affectedId = "";
            String task = "View";
            String description = "Subsection Table view";
            String createdUser = "";
            subsectionService.insertAuditTrace(page, affectedId, task, description, createdUser);
            response = subsectionService.getSubectionTableData(request);
        } catch (HibernateException ex) {
            Loggers.LOGGER_FILE.error(ex);
            Logger.getLogger(SubSectionController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            response.put("success", false);
            response.put("messageService", "Un-successfull");
            response.put("sEcho", 0);
            response.put("iTotalRecords", 0);
            response.put("iTotalDisplayRecords", 0);
            Loggers.LOGGER_FILE.error(ex);
            Logger.getLogger(SubSectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/searchbyid", method = RequestMethod.GET)
    public @ResponseBody
    String searchbyid(HttpServletRequest request) {
        JSONObject array = new JSONObject();
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            array = subsectionService.getSubsectionById(request.getParameter("id"));
        } catch (Exception ex) {
            Logger.getLogger(SubSectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array.toString();
    }

    @RequestMapping(value = "/searchbydescription", method = RequestMethod.GET)
    public @ResponseBody
    String searchbydescription(HttpServletRequest request) {
        JSONObject array = new JSONObject();
        try {
            String a = request.getParameter("description");
            array = subsectionService.getSubsectionListByDescription(request.getParameter("description"));
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return array.toString();
    }

    @RequestMapping(value = "/loadsections", method = RequestMethod.GET)
    public @ResponseBody
    String loadSelect() {
        JSONObject array = new JSONObject();
        try {
            array = subsectionService.getSections();
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return array.toString();
    }

    @RequestMapping(value = "/loadstatus", method = RequestMethod.GET)
    public @ResponseBody
    String loadStatus() {
        JSONObject array = new JSONObject();
        try {
            array = subsectionService.getStatus();
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return array.toString();
    }

    //Adding new Subsection 
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public @ResponseBody
    String insertSubsection(HttpServletRequest request, @RequestParam("action") String selectFunction) {
        String sectiondata = request.getParameter("subsection_info");
        String setSelectFunction = selectFunction;
        try {
            Subsection subsection = (Subsection) new ObjectMapper().readValue(sectiondata, Subsection.class);
            JSONObject response = subsectionService.addSubsection(subsection, setSelectFunction);
            return response.toString();
        } catch (Exception ex) {
            Logger.getLogger(SubSectionController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
            return "FAIL";
        }
    }

    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public @ResponseBody
    String viewSection(HttpServletRequest request) {
        JSONObject jsonArray = new JSONObject();
        try {
            String id = request.getParameter("subsectionId");
//            String page = "Subsection View By ID";
//            String affectedId = id;
//            String task = "View Subsection";
//            String description = "Subsection View By ID " + id;
//            String createdUser = "";
//            subsectionService.insertAuditTrace(page, affectedId, task, description, createdUser);
            jsonArray = subsectionService.getSubsectionById(id);
        } catch (Exception ex) {
            Logger.getLogger(SubSectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonArray.toString();
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updateSubsection(ModelMap model) {
        model.put("root", "SubSection");
        model.put("page", "Update Subsection");
        return "subsection/updatesubsection";
    }

}
