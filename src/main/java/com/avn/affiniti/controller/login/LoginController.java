package com.avn.affiniti.controller.login;

import com.avn.affiniti.dao.common.CommonDAOImpl;
import com.avn.affiniti.dao.employee.EmployeeDAO;
import com.avn.affiniti.dao.logoutreasons.LogoutReasonsDAO;
import com.avn.affiniti.dao.section.SectionDAOImpl;
import com.avn.affiniti.dao.subsection.SubsectionDAOImpl;
import com.avn.affiniti.model.section.Section;
import com.avn.affiniti.model.user.User;
import com.avn.affiniti.service.dashboard.DashboardService;
import com.avn.affiniti.service.login.LoginService;
import com.avn.affiniti.service.user.UserService;
import com.avn.affiniti.util.Common;
import com.avn.affiniti.util.usermessages.AffinitiMessages;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @Autowired
    ServletContext context;
    @Autowired
    CommonDAOImpl commonDAOImpl;
    @Autowired
    LoginService loginService;
    @Autowired
    UserService userService;
    @Autowired
    SectionDAOImpl sectionDAOImpl;
    @Autowired
    SubsectionDAOImpl subsectionDAOImpl;
    @Autowired
    LogoutReasonsDAO logoutReasonDAO;
    @Autowired
    EmployeeDAO employeeDAO;
    @Autowired
    DashboardService dashboardService;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String logout(@RequestParam(value = "auth", required = false) String auth, ModelMap model, HttpSession session) {
        String version = context.getInitParameter("software_version");
        model.addAttribute("version", version);
        String page = "LogOut";
        String task = "logout";
        String affectedId = "";
        String description = "";
        String username = (String) session.getAttribute("username");
        String createdUser = username;

        try {
            if ((auth != null) && (auth.equalsIgnoreCase("logout"))) {
                description = "' " + username + " ' is Logged out from the System.";

                loginService.insertAuditTrace(page, affectedId, task, description, createdUser);
                model.addAttribute("msg", AffinitiMessages.LOGIN_LOGOUT_SUCCESS.getMessage());
            } else if ((auth != null) && (auth.equalsIgnoreCase("sessiontimeout"))) {
                model.addAttribute("info", AffinitiMessages.LOGIN_SESSION_OUT.getMessage());
            }
        } catch (Exception exception) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, exception);
            exception.printStackTrace();
        }
        return "index";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute("loginForm") User user, ModelMap model, HttpSession session) {

        String userName = user.getUsername();
        String createdUser = userName;
        String page = "Login";
        String task = "";
        String affectedId = "";
        String description = "";
        ModelAndView modelAndView = null;
        String version = context.getInitParameter("software_version");
        try {
            if (loginService.isUserExist(userName)) {
                if (!loginService.isActivieUser(user.getUsername())) {
                    model.addAttribute("error", AffinitiMessages.LOGIN_INACTIVE_USER.getMessage());
                } else if (loginService.isValidUser(user.getUsername(), user.getPassword())) {
                    session.setAttribute("username", user.getUsername());
                    description = "' " + userName + " ' is Logged into the System.";
                    task = "login";
                    loginService.insertAuditTrace(page, affectedId, task, description, createdUser);

                    return new ModelAndView("redirect:/dashboardone");
                } else {
                    description = "' " + userName + " ' is Tryied to Log into the System.Invalid Username or Password.";
                    task = "login";
                    loginService.insertAuditTrace(page, affectedId, task, description, createdUser);
                    model.addAttribute("error", AffinitiMessages.LOGIN_INVALID_USER.getMessage());
                }
//                model.addAttribute("error", AffinitiMessages.LOGIN_INVALID_USER.getMessage());
                model.addAttribute("version", version);
                modelAndView = new ModelAndView("index");
            } else {
                description = "' " + userName + " ' is Tryied to Log into the System.User is not Exist";
                task = "login";
                loginService.insertAuditTrace(page, affectedId, task, description, createdUser);
                model.addAttribute("error", AffinitiMessages.LOGIN_USER_NOT_EXIST.getMessage());
                model.addAttribute("version", version);
                modelAndView = new ModelAndView("index");
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/login/getlogoutresons", method = RequestMethod.GET)
    public @ResponseBody
    List<String> loadLogoutResons(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            return logoutReasonDAO.getLogOutResons();
        } catch (Exception e) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    @RequestMapping(value = "/logout/logoutreson", method = RequestMethod.POST)
    public @ResponseBody
    boolean getlogoutReson(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        String logoutreson = request.getParameter("logoutreson");
        try {
//            auditTraceDAOImpl.insertAuditTrace(null, "Logout", logoutreson, null, (String) session.getAttribute("username"));
            return true;
        } catch (Exception e) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }

    }

    @RequestMapping(value = "/login/activuser", method = RequestMethod.GET)
    public @ResponseBody
    String getmodule(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        String username = (String) session.getAttribute("username");
        return employeeDAO.getEmployeeDetailsByUsername(username);
    }

    @RequestMapping(value = {"/dashboardone"}, method = RequestMethod.GET)
    public ModelAndView dashboardone(ModelMap model, HttpSession session) {
        String userName = (String) session.getAttribute("username");
        model.put("root", "");
        model.put("page", "Dashboard");
        try {
            if (session.getAttribute("pagehierarchy") == null) {
                int userRole = userService.getUserRoleByUsername(userName);
                List<Section> levelZeroSections = sectionDAOImpl.getSectionLevelZeroByUserrole(userRole);
                List<Section> lowLevelSections = sectionDAOImpl.getSectionLowLevelByUserrole(userRole);

                for (Iterator i$ = lowLevelSections.iterator(); i$.hasNext();) {
                    Section lowLevelSection = (Section) i$.next();
                    int parentSectionID = Integer.valueOf(lowLevelSection.getParentsection()).intValue();
                    for (Section levelZeroSection : levelZeroSections) {
                        int sectionID = Integer.valueOf(levelZeroSection.getSectionid()).intValue();
                        if (parentSectionID == sectionID) {
                            if (levelZeroSection.getChildsections() == null) {
                                List<Section> list = new ArrayList();
                                list.add(lowLevelSection);
                                levelZeroSection.setChildsections(list);
                            } else {
                                levelZeroSection.getChildsections().add(lowLevelSection);
                            }
                        }
                    }
                }

                for (Section section : levelZeroSections) {
                    setSubsetionsForChildSections(userRole, section.getChildsections());
                    section.setSubsections(subsectionDAOImpl.getSubSectionsByUserroleAndSectionID(userRole, section.getSectionid()));
                }

                session.setAttribute("pagehierarchy", levelZeroSections);
            }

            String lastlogin = null;
            try {
                if (session.getAttribute("lastlogin") == null) {
                    loginService.updateLastLogin(userName, commonDAOImpl.getCurentTimesStamp());
                    lastlogin = Common.getStringFormatDate("dd-MMM-yy hh.mm.ss a", loginService.getLastLoginDateTime(userName));
                    session.setAttribute("lastlogin", "Last Login " + lastlogin);
                    int userroleide = userService.getUserRoleByUsername(userName);
                    int hierarchyid = userService.getHierarchyidIdByUsername(userName);
                    session.setAttribute("userroleuid", Integer.valueOf(userroleide));
                    session.setAttribute("hierarchyid", Integer.valueOf(hierarchyid));

                    JSONArray objList = (JSONArray) session.getAttribute("userroletasklits");
                }
            } catch (NullPointerException e) {
                session.setAttribute("lastlogin", "First Time Login ");
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
            } catch (Exception e) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
            }

            dashboardService.loadPageComponent(model, session);

        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ModelAndView("dashboard");
    }

    private void setSubsetionsForChildSections(int userRole, List<Section> childSections) throws Exception {
        if (childSections != null) {
            for (Section childSection : childSections) {
                childSection.setSubsections(subsectionDAOImpl.getSubSectionsByUserroleAndSectionID(userRole, childSection.getSectionid()));
                setSubsetionsForChildSections(userRole, childSection.getChildsections());
            }
        }
    }
}
