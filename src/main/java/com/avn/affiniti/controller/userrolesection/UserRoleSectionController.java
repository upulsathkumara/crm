/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.userrolesection;

import com.avn.affiniti.hibernate.model.Userrolesection;
import com.avn.affiniti.model.userrolesection.UserRoleSection;
import com.avn.affiniti.service.userrolesection.UserRoleSectionService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.datatable.DataTableParamUtility;
import com.avn.affiniti.util.datatable.DataTableRequestParam;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author Nadun Chamikara
 * @Document UserRoleSectionController
 * @Created on 09/08/2017, 2:24:04 PM
 */
@Controller
@RequestMapping(value = "/userrolesection")
public class UserRoleSectionController {

    @Autowired
    private UserRoleSectionService userRoleSectionService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("userRoleSectionSearchForm", new Userrolesection());
            model.put("userRoleSectionAddForm", new Userrolesection());

            userRoleSectionService.loadPageComponent(model);
        } catch (Exception e) {
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
            Loggers.LOGGER_FILE.error(e);
        }
        return "userrolesection/userrolesection";
    }

    /*
     * Get UserRoleSections by userRoleId and sectionId
     */
    @RequestMapping(value = "/getuserrolesectionstotable", method = RequestMethod.GET)
    public @ResponseBody
    String getUserRoleSectionsTableData(HttpServletRequest request, HttpSession session) {
        List<JSONObject> list;
        final DataTableRequestParam param = DataTableParamUtility.getParam(request);
        final String sEcho = param.sEcho;
        long iTotalRecords = 0; // total number of records (unfiltered)

        long iTotalDisplayRecords = 0;//value will be set when code filters data by keyword
        JSONArray rows = new JSONArray();
        try {

            if (param.iDisplayStart < 0) {
                param.iDisplayStart = 0;
            }

            if (param.iDisplayLength < 10 || param.iDisplayLength > 100) {
                param.iDisplayLength = 10;
            }

            String userRoleId = request.getParameter("userRoleId");
            String sectionId = request.getParameter("sectionId");
            
            iTotalRecords = userRoleSectionService.getUserRoleSectionTableDataCount(userRoleId, sectionId);
            iTotalDisplayRecords = iTotalRecords;
            if (iTotalRecords > 0) {
                list = userRoleSectionService.getUserRoleSectionTableData(userRoleId, sectionId, param.iDisplayStart, param.iDisplayLength, session);
                        
//                for (JSONObject jSONObject : list) {
//                    jSONObject.put("action", String.format(jSONObject.getString("action"), request.getContextPath(), request.getContextPath()));
//                }
                rows = new JSONArray(list);
            }
        } catch (Exception e) {
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, e);
            Loggers.LOGGER_FILE.error(e);
        }
        JSONObject output = new JSONObject();
        output.put("sEcho", sEcho);
        output.put("iTotalRecords", iTotalRecords);
        output.put("iTotalDisplayRecords", iTotalDisplayRecords);
        output.put("aaData", rows);
        return output.toString();

//        JSONObject output = new JSONObject();
//        JSONArray data = new JSONArray();
//
//        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
//        output.put("SUCCESS", true);
//        output.put("MESSAGE", "Data successfully retrieved.");
//        output.put("aaData", data);
//
//        try {
//            data = userRoleSectionService.getUserRoleSectionTableData(userRoleId, sectionId, session);
//            output.put("aaData", data);
//        } catch (HibernateException ex) {
//            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("SUCCESS", false);
//            output.put("MESSAGE", "Error : Database Error");
//            ex.printStackTrace();
//            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, ex);
//
//        } catch (Exception ex) {
//            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
//            output.put("SUCCESS", false);
//            output.put("MESSAGE", "Error : Error");
//            ex.printStackTrace();
//            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return output.toString();
    }

    @RequestMapping(value = "/getuserrolesections", method = RequestMethod.POST)
    public @ResponseBody
    String getUserRoleSectionsByUserRoleId(@RequestParam("userroleid") String userRoleId, HttpSession session) {
        JSONArray data = new JSONArray();
        JSONObject output = new JSONObject();

        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", MessageVarList.DATA_RETRIEVED_SUCCESSFULLY);
        output.put("DATA", data);

        try {
            data = userRoleSectionService.getUserRoleSectionsByUserRoleId(userRoleId, session);
            output.put("DATA", data);

        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }

    @RequestMapping(value = "/addorupdateuserrolesection", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String createOrUpdateUserRoleSection(@RequestBody UserRoleSection userRoleSection, HttpSession session) {
        JSONObject output = new JSONObject();
        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", String.format(MessageVarList.ASSIGNED_SUCCESSFULLY, "Sections"));
        output.put("DATA", new JSONArray());
        try {
            List<Userrolesection> preUserRoleSections = userRoleSectionService.getUserRoleSectionsByUserRoleId(String.valueOf(userRoleSection.getUserRoleId()));
            userRoleSectionService.createOrUpdateUserRoleSection(userRoleSection, preUserRoleSections, session);
        } catch (Exception e) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.ASSIGN_ERROR, "user role section"));
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
            Loggers.LOGGER_FILE.error(e);
        }
        return output.toString();
    }

    @RequestMapping(value = "/getsectionsfordropdown", method = RequestMethod.POST)
    public @ResponseBody
    String getSectionsForDropdown() {
        JSONArray array = new JSONArray();
        JSONObject output = new JSONObject();
        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("MESSAGE", MessageVarList.DATA_RETRIEVED_SUCCESSFULLY);
        output.put("DATA", array);

        try {
            array = userRoleSectionService.getSectionsForDropdown();
            output.put("DATA", array);

        } catch (SQLException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        } catch (DataAccessException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", MessageVarList.DATA_RETRIEVE_ERROR);
            ex.printStackTrace();
            Logger.getLogger(UserRoleSectionController.class.getName()).log(Level.SEVERE, null, ex);
            Loggers.LOGGER_FILE.error(ex);
        }

        return output.toString();
    }
}
