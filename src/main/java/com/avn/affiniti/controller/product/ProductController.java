/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.product;

import com.avn.affiniti.hibernate.model.Product;
import com.avn.affiniti.service.product.ProductService;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author :acer
 * @Document :ProductController
 * @Created on:Sep 4, 2017,8:11:49 AM
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createView(ModelMap model) {
        try {
            model.put("page", "Product");
            model.put("productAddForm", new Product());
            model.put("productSearchForm", new Product());

            productService.loadPageComponent(model);
            productService.loadStatusComponent(model);
        } catch (Exception e) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return "product/product";
    }

    @RequestMapping(value = "/getproduct", method = RequestMethod.POST)
    public @ResponseBody
    String getProductTableData(HttpServletRequest request, HttpSession session) {

        JSONObject output = new JSONObject();
//        String page = "Product";
//        String task = "";
//        String description = "";
//        String affectedId = "";
        try {
            String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
            output = productService.getProductTableData(request, createdUser);
            
//              String parameters = request.getParameter("searchoptionID");//Setting up searched Section Task ID
//            if (parameters.equals("") || parameters.equals("--Select--")) {
//                task = "Table View";
//                description = "Table View all details in Product ";
//                affectedId = "";
//            } else {
//                task = "Table Search";
//                description = "Table Search details in Status by product Id " + parameters;
//                affectedId = parameters;
//
//            }
            //productService.insertAuditTrace(page, affectedId, task, description, createdUser);
            
        } catch (Exception e) {

            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return output.toString();
    }

    //////////////////////////////////////////////
    @RequestMapping(value = "/addorupdateproduct", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String createOrUpdateProduct(@RequestBody Product product, HttpSession session, HttpServletRequest request, @RequestParam("action") String selectFunction) {
        JSONObject output = new JSONObject();


        String createdUser = String.valueOf(session.getAttribute(CommonVarList.AFFINITI_SESSION_USERNAME));
        //String test = request.getParameter("productcategoryid");
        //System.out.println(product.getProductcategory().getProductcategoryid());
        String id = String.valueOf(product.getProductid());
        String test = String.valueOf(product.getProductcategory().getProductcategoryid());
        //output = productService.createOrUpdateProduct(product,createdUser,test);
        //output.put("message", "Product saved");
         String setSelectFunction = selectFunction;

        try {
            output = productService.createOrUpdateProduct(product, createdUser,test, id, setSelectFunction);
            
        output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_SUCCESS);
        output.put("SUCCESS", true);
        output.put("message", String.format(MessageVarList.SAVED_SUCCESSFULLY, "Product"));
        output.put("DATA", new JSONArray());
        } catch (HibernateException ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.SAVE_ERROR, "Product"));
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (Exception ex) {
            output.put("CODE", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            output.put("SUCCESS", false);
            output.put("MESSAGE", String.format(MessageVarList.SAVE_ERROR, "Product"));
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        return output.toString();

    }

    @RequestMapping(value = "/getuproductt", method = RequestMethod.POST)
    public @ResponseBody
    String getProductByProductCategoryId(@RequestParam("productid") int productId,HttpSession session) {
        return productService.getProductByProductCategoryId(productId,session).toString();
    }

}
