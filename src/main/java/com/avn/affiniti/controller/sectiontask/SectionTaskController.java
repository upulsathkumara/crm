package com.avn.affiniti.controller.sectiontask;

import com.avn.affiniti.hibernate.model.Section;
import com.avn.affiniti.service.sectiontask.SectionTaskService;
import com.avn.affiniti.util.Loggers;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ASHOK
 */
@Controller
@RequestMapping("/sectiontask")
public class SectionTaskController {

    @Autowired
    SectionTaskService sectionTaskService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String viewSectionTaskPage(ModelMap model) {
        try {
            model.put("root", "Settings");
            model.put("page", "Section Task");
            model.put("sectionTaskAdd", new Section());
            sectionTaskService.loadPageComponent(model);
        } catch (SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
        }

        return "sectionTask/sectionTask";
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public @ResponseBody
    String insertSectionTask(HttpServletRequest request, HttpSession session) {

        JSONObject response = new JSONObject();
        try {
            response = sectionTaskService.insertSectionTask(request, (String)session.getAttribute("username"));
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody
    String updateSectionTask(HttpServletRequest request, HttpSession session) {

        JSONObject response = new JSONObject();
        session = request.getSession(true);
        try {
            response = sectionTaskService.UpdateSectionTask(request, (String)session.getAttribute("username"));
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public @ResponseBody
    String viewSectionTask(HttpServletRequest request, HttpSession session) {

        JSONObject response = new JSONObject();
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        Section section = new Section();
        section.setSectionid(sectionId);
        session = request.getSession(true);
        try {
            response = sectionTaskService.getSectionTaskById(section, (String)session.getAttribute("username"));
            response.put("selectedSectionId", sectionId);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/loadTask", method = RequestMethod.POST)
    public @ResponseBody
    String loadTasks() {

        JSONObject details = new JSONObject();
        try {
            details = sectionTaskService.getAllTasks();
        } catch (SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return details.toString();
    }

    @RequestMapping(value = "/loadTable", method = RequestMethod.POST)
    public @ResponseBody
    String loadSectionTasksTable(HttpServletRequest request, HttpSession session) {

        JSONObject response = new JSONObject();
        session = request.getSession(true);
        try {
            response = sectionTaskService.getSectionTaskTableData(request, (String)session.getAttribute("username"));
        } catch (HibernateException | SQLException ex) {
            Loggers.LOGGER_FILE.error(ex);
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public @ResponseBody
    String test(HttpServletRequest request) {

        JSONObject response = new JSONObject();
        try {
            String x = request.getParameter("test_info");
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return response.toString();
    }

}
