/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.remindernotification;

import com.avn.affiniti.controller.login.LoginController;
import com.avn.affiniti.controller.section.SectionController;
import com.avn.affiniti.dao.audittrace.AuditTraceDAOImpl;
//import com.avn.affiniti.daoimpl.callcenter.CallCenterDAOImpl;
import com.avn.affiniti.dao.common.CommonDAOImpl;
import com.avn.affiniti.dao.employee.EmployeeDAOImpl;
import com.avn.affiniti.dao.product.ProductDAOImpl;
//import com.avn.affiniti.dao.productcategory.ProductCategoryDAOImpl;
import com.avn.affiniti.dao.remindernotification.ReminderNotificationDAOImpl;
import com.avn.affiniti.dao.status.StatusDAOImpl;
import com.avn.affiniti.hibernate.model.Remindernotification;
import com.avn.affiniti.service.remindernotification.ReminderNotificationService;
import com.avn.affiniti.util.DateTime;
import com.avn.affiniti.util.varlist.CommonVarList;
//import com.avn.affiniti.daoimpl.account.AccountDAOImpl;
//import com.avn.affiniti.daoimpl.audittrace.AuditTraceDAOImpl;
//import com.avn.affiniti.daoimpl.branch.BranchDAOImpl;
//import com.avn.affiniti.daoimpl.calldirection.CallDirectionDAOImpl;
//import com.avn.affiniti.daoimpl.casemgt.CaseDAOImpl;
//import com.avn.affiniti.daoimpl.casepriority.CasePriorityDAOImpl;
//import com.avn.affiniti.daoimpl.casetype.CaseTypeDAOImpl;
//import com.avn.affiniti.daoimpl.department.DepartmentDAOImpl;
//import com.avn.affiniti.daoimpl.dependent.DependentDAOImpl;
//import com.avn.affiniti.daoimpl.education.EducationDAOImpl;
//import com.avn.affiniti.daoimpl.educationlevel.EducationLevelDAOImpl;
//import com.avn.affiniti.daoimpl.employee.EmployeeDAOImpl;
//import com.avn.affiniti.daoimpl.followupaction.FollowUpActionDAOImpl;
//import com.avn.affiniti.daoimpl.language.LanguageDAOImpl;
//import com.avn.affiniti.daoimpl.notification.NotificationDAOImpl;
//import com.avn.affiniti.daoimpl.notificationtemplate.NotifictionTemplateDAOImpl;
//import com.avn.affiniti.daoimpl.opdesignation.OPDesignationDAOImpl;
//import com.avn.affiniti.daoimpl.oplevel.OPLevelDAOImpl;
//import com.avn.affiniti.daoimpl.opprofession.OPProfessionDAOImpl;
//import com.avn.affiniti.daoimpl.organizationalparameters.OrganizationalParametersDAOImpl;
//import com.avn.affiniti.daoimpl.product.ProductDAOImpl;
//import com.avn.affiniti.daoimpl.religon.ReligonDAOImpl;
//import com.avn.affiniti.daoimpl.remindernotification.ReminderNotificationDAOImpl;
//import com.avn.affiniti.daoimpl.roundrobin.RoundRobinDAOImpl;
//import com.avn.affiniti.daoimpl.salesPipline.SalesPiplineDAOImpl;
//import com.avn.affiniti.daoimpl.secretquestion.SecretQuestionDAOImpl;
//import com.avn.affiniti.daoimpl.status.StatusDAOImpl;
//import com.avn.affiniti.daoimpl.title.TitleDAOImpl;
//import com.avn.affiniti.model.remindernotification.ReminderNotification;
import com.avn.affiniti.util.varlist.MasterDataVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ISURU
 */
@Controller
public class ReminderNotificationController {

    @Autowired
    ReminderNotificationDAOImpl remindernotificationdaoimpl;
    @Autowired
    CommonDAOImpl commonDAOImpl;

    @Autowired
    ReminderNotificationService remindernotificationservice;
//    @Autowired
//    CallCenterDAOImpl callCenterDaoImpl;
//    @Autowired
//    AuditTraceDAOImpl audit;
//    @Autowired
//    CallDirectionDAOImpl callDirectionDaoImpl;
//    @Autowired
//    FollowUpActionDAOImpl followUpActionDaoImpl;
//    @Autowired
//    LanguageDAOImpl languagesDaoImpl;
//    @Autowired
//    CaseTypeDAOImpl caseTypeDAOImpl;
    @Autowired
    ProductDAOImpl productDAOImpl;

//    @Autowired
//    CasePriorityDAOImpl casePriorityDAOImpl;
//    @Autowired
//    DepartmentDAOImpl departmentDAOImpl;
//    @Autowired
//    BranchDAOImpl branchdaoimpl;
    @Autowired
    StatusDAOImpl statusDAOImpl;

    @Autowired
    ReminderNotificationService ReminderNotificationService;

    //@Autowired
//    TitleDAOImpl titleDAOImpl;
//    @Autowired
//    AccountDAOImpl accountDAOImpl;
//    @Autowired
//    EducationDAOImpl educationDAOImpl;
//    @Autowired
//    OPDesignationDAOImpl oPDesignationDAOImpl;
//    @Autowired
//    OPLevelDAOImpl oPLevelDAOImpl;
//    @Autowired
//    OPProfessionDAOImpl oPProfessionDAOImpl;
//    @Autowired
//    EducationLevelDAOImpl educationLevelDAOImpl;
//    @Autowired
//    DependentDAOImpl dependentDAOImpl;
//    @Autowired
//    BranchDAOImpl branchDAOImpl;
//    @Autowired
//    ReligonDAOImpl religonDAOImpl;
//    @Autowired
//    SecretQuestionDAOImpl secretQuestionDAOImpl;
//    @Autowired
//    ServletContext context;
//    @Autowired
//    CommonDAOImpl commendaoimpl;
//    @Autowired
//    CaseDAOImpl caseDaoImpl;
//    @Autowired
//    SalesPiplineDAOImpl salesPiplineDAOImpl;
//    @Autowired
//    ProductCategoryDAOImpl productcategoryDAOImpl;
//    @Autowired
//    RoundRobinDAOImpl roundrobindaoimpl;
//    @Autowired
//    EmployeeDAOImpl employeeDAOImpl;
//    @Autowired
//    NotificationDAOImpl notificationDAOImpl;
//    @Autowired
//    OrganizationalParametersDAOImpl organizationalParametersDAOImpl;
//    @Autowired
//    NotifictionTemplateDAOImpl notifictionTemplateDAOImpl;
//    @RequestMapping(value = "Notification", method = RequestMethod.GET)
//    public String caseView(@ModelAttribute("callview") CallCenter data,
//            @RequestParam(value = "Id", required = false) String sourceid,
//            @RequestParam(value = "section", required = false) String section,
//            ModelMap model, HttpSession session) {
//        try {
//            String name = (String) session.getAttribute("username");
//            Case caseDetails = null;
//            CallCenter objcallcenter = new CallCenter();
//            if (section.contentEquals(String.valueOf(MasterDataVarList.AFFINITI_CODE_CALLCENTER_SECTION_ID))) {
//                Case isticket = caseDaoImpl.getCaseByCaseId(sourceid, MasterDataVarList.AFFINITI_CODE_NOTIFICATION_COMPONENT_CALL);
//                if (isticket != null) {
//                    this.setCreateViewComponenetsTicket(model);
//                    Case usercase = new Case();
//                    usercase = caseDaoImpl.getCaseByCaseId(sourceid, MasterDataVarList.CCL_CODE_NOTIFICATION_COMPONENT_CALL);
//                    List<Case> urllist = caseDaoImpl.getListUrl(usercase.getCaseId());
//                    model.addAttribute("downloadid", usercase.getCaseId());
//                    model.put("urllist", urllist);
////                    CallCenter objcallcenter = new CallCenter();
//                    if (usercase.getCaseCallLogId() != null) {
//                        objcallcenter = callCenterDaoImpl.getCallByCallLogId(Integer.valueOf(usercase.getCaseCallLogId()));
//                        model.put("Date", getStringFormatDate(DATE_FORMAT_yyyy_MM_dd_hh_mm_a, objcallcenter.getStartDate()));
//                        model.addAttribute("callduration", objcallcenter.getCallduration());
//                        if (objcallcenter.getCallbackDate() != null) {
//                            model.put("callbackDate", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, objcallcenter.getCallbackDate()));
//                            model.addAttribute("callbackTime", DateTime.getTimeString(getTimestampFromDateAndTime(objcallcenter.getCallbackDate())));
//
//                        }
//                        if (objcallcenter.getIscustomer().contentEquals("1")) {
//                            CallCenter objcustomerdetails = callCenterDaoImpl.getCustomertByContactid(objcallcenter.getCustomercode());
//                            objcallcenter.setTitle(objcustomerdetails.getTitle());
//                            objcallcenter.setTiteldes(objcustomerdetails.getTitle());
//                            objcallcenter.setLast_name(objcustomerdetails.getLast_name());
//                            objcallcenter.setName_in_full(objcustomerdetails.getName_in_full());
//                            if (objcustomerdetails.getTelephone() == null || objcustomerdetails.getTelephone().equalsIgnoreCase("null")) {
//                                objcallcenter.setTelephone("");
//                            } else {
//                                objcallcenter.setTelephone(objcustomerdetails.getTelephone());
//                            }
//
//                            if (objcustomerdetails.getNic() == null || objcustomerdetails.getNic().equalsIgnoreCase("null")) {
//                                objcallcenter.setNic("");
//                            } else {
//                                objcallcenter.setNic(objcustomerdetails.getNic());
//                            }
//
//                            if (objcustomerdetails.getEmail() == null || objcustomerdetails.getEmail().equalsIgnoreCase("null")) {
//                                objcallcenter.setEmail("");
//                            } else {
//                                objcallcenter.setEmail(objcustomerdetails.getEmail());
//                            }
//
//                        }
//                        usercase.setObjcallcenter(objcallcenter);
//                    } else {
//                        objcallcenter = callCenterDaoImpl.getContactByContactid(Integer.valueOf(usercase.getContactid()));
//                        if (objcallcenter.getIscustomer().contentEquals("1")) {
//                            CallCenter objcustomerdetails = callCenterDaoImpl.getCustomertByContactid(objcallcenter.getCustomercode());
//                            objcallcenter.setTitle(objcustomerdetails.getTitle());
//                            objcallcenter.setTiteldes(objcustomerdetails.getTitle());
//                            objcallcenter.setLast_name(objcustomerdetails.getLast_name());
//                            objcallcenter.setName_in_full(objcustomerdetails.getName_in_full());
//                            if (objcustomerdetails.getTelephone() == null || objcustomerdetails.getTelephone().equalsIgnoreCase("null")) {
//                                objcallcenter.setTelephone("");
//                            } else {
//                                objcallcenter.setTelephone(objcustomerdetails.getTelephone());
//                            }
//
//                            if (objcustomerdetails.getNic() == null || objcustomerdetails.getNic().equalsIgnoreCase("null")) {
//                                objcallcenter.setNic("");
//                            } else {
//                                objcallcenter.setNic(objcustomerdetails.getNic());
//                            }
//
//                            if (objcustomerdetails.getEmail() == null || objcustomerdetails.getEmail().equalsIgnoreCase("null")) {
//                                objcallcenter.setEmail("");
//                            } else {
//                                objcallcenter.setEmail(objcustomerdetails.getEmail());
//                            }
//
//                        }
//                        usercase.setObjcallcenter(objcallcenter);
//                    }
//                    if (usercase.getCampainid() != null) {
//                        model.addAttribute("chanelList", commonDAOImpl.getDropdownValueList("SELECT ID AS ID,CH.DESCRIPTION AS DESCRIPTION  FROM campaignchannel C INNER JOIN channel CH ON C.CHANNELID=CH.CHANNELID WHERE CAMPAIGNID ='" + usercase.getCampainid() + "'"));
//                    }
//
//                    model.addAttribute("Objcallcenter", usercase);
////               
//                    model.addAttribute("usercase", usercase);
//                    model.put("caseHistoryList", caseDaoImpl.getCaseHistory(usercase.getCaseId(), name));
//
////                    remindernotificationdaoimpl.setViewStatus("35", sourceid, section);
//                    return "case/ticketview";
//                } else {
//                    this.setCreateViewComponenets(model);
//                    objcallcenter = callCenterDaoImpl.getCallByCallLogId(Integer.valueOf(sourceid));
//                    if (objcallcenter.getIscustomer().contentEquals("1")) {
//                        CallCenter objcustomerdetails = callCenterDaoImpl.getCustomertByContactid(objcallcenter.getCustomercode());
//                        objcallcenter.setTitle(objcustomerdetails.getTitle());
//                        objcallcenter.setTiteldes(objcustomerdetails.getTitle());
//                        objcallcenter.setLast_name(objcustomerdetails.getLast_name());
//                        objcallcenter.setName_in_full(objcustomerdetails.getName_in_full());
//                        if (objcustomerdetails.getTelephone() == null || objcustomerdetails.getTelephone().equalsIgnoreCase("null")) {
//                            objcallcenter.setTelephone("");
//                        } else {
//                            objcallcenter.setTelephone(objcustomerdetails.getTelephone());
//                        }
//
//                        if (objcustomerdetails.getNic() == null || objcustomerdetails.getNic().equalsIgnoreCase("null")) {
//                            objcallcenter.setNic("");
//                        } else {
//                            objcallcenter.setNic(objcustomerdetails.getNic());
//                        }
//
//                        if (objcustomerdetails.getEmail() == null || objcustomerdetails.getEmail().equalsIgnoreCase("null")) {
//                            objcallcenter.setEmail("");
//                        } else {
//                            objcallcenter.setEmail(objcustomerdetails.getEmail());
//                        }
//
//                    }
//                    model.addAttribute("objcallcenter", objcallcenter);
//                    remindernotificationdaoimpl.setViewStatus("35", sourceid, section);
//                    model.addAttribute("objcallcenter", objcallcenter);
//                    model.put("Date", getStringFormatDate(DATE_FORMAT_yyyy_MM_dd_hh_mm_a, objcallcenter.getStartDate()));
//                    model.addAttribute("callduration", objcallcenter.getCallduration());
//                    if (objcallcenter.getCallbackDate() != null) {
//                        model.put("callbackDate", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, objcallcenter.getCallbackDate()));
//                        model.addAttribute("callbackcallduration", DateTime.getTimeString(getTimestampFromDateAndTime(objcallcenter.getCallbackTime())));
//                    }
//
//                }
//                return "callcenter/callview";
//            }
//            if (section.contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_CASE_SECTION_ID))) {
//                this.setCreateViewComponenetsTicket(model);
//
//                Case usercase = new Case();
//
//                usercase = caseDaoImpl.getCaseByCaseId(sourceid, MasterDataVarList.CCL_CODE_NOTIFICATION_COMPONENT_CASE);
//                List<Case> urllist = caseDaoImpl.getListUrl(usercase.getCaseId());
//                model.addAttribute("downloadid", usercase.getCaseId());
//                model.put("urllist", urllist);
////                CallCenter objcallcenter = new CallCenter();
//                if (usercase.getCaseCallLogId() != null) {
//                    objcallcenter = callCenterDaoImpl.getCallByCallLogId(Integer.valueOf(usercase.getCaseCallLogId()));
//                    model.put("Date", getStringFormatDate(DATE_FORMAT_yyyy_MM_dd_hh_mm_a, objcallcenter.getStartDate()));
//                    model.addAttribute("callduration", objcallcenter.getCallduration());
//                    if (objcallcenter.getCallbackDate() != null) {
//                        model.put("callbackDate", getStringFormatDate(CommonVarList.DATE_FORMAT_yyyy_MM_dd, objcallcenter.getCallbackDate()));
//                        model.addAttribute("callbackTime", DateTime.getTimeString(getTimestampFromDateAndTime(objcallcenter.getCallbackDate())));
//
//                    }
//                    if (objcallcenter.getIscustomer().contentEquals("1")) {
//                        CallCenter objcustomerdetails = callCenterDaoImpl.getCustomertByContactid(objcallcenter.getCustomercode());
//                        objcallcenter.setTitle(objcustomerdetails.getTitle());
//                        objcallcenter.setTiteldes(objcustomerdetails.getTitle());
//                        objcallcenter.setLast_name(objcustomerdetails.getLast_name());
//                        objcallcenter.setName_in_full(objcustomerdetails.getName_in_full());
//                        if (objcustomerdetails.getTelephone() == null || objcustomerdetails.getTelephone().equalsIgnoreCase("null")) {
//                            objcallcenter.setTelephone("");
//                        } else {
//                            objcallcenter.setTelephone(objcustomerdetails.getTelephone());
//                        }
//
//                        if (objcustomerdetails.getNic() == null || objcustomerdetails.getNic().equalsIgnoreCase("null")) {
//                            objcallcenter.setNic("");
//                        } else {
//                            objcallcenter.setNic(objcustomerdetails.getNic());
//                        }
//
//                        if (objcustomerdetails.getEmail() == null || objcustomerdetails.getEmail().equalsIgnoreCase("null")) {
//                            objcallcenter.setEmail("");
//                        } else {
//                            objcallcenter.setEmail(objcustomerdetails.getEmail());
//                        }
//
//                    }
//                    usercase.setObjcallcenter(objcallcenter);
//                } else {
//                    objcallcenter = callCenterDaoImpl.getContactByContactid(Integer.valueOf(usercase.getContactid()));
//
//                    if (objcallcenter.getIscustomer().contentEquals("1")) {
//                        CallCenter objcustomerdetails = callCenterDaoImpl.getCustomertByContactid(objcallcenter.getCustomercode());
//                        objcallcenter.setTitle(objcustomerdetails.getTitle());
//                        objcallcenter.setTiteldes(objcustomerdetails.getTitle());
//                        objcallcenter.setLast_name(objcustomerdetails.getLast_name());
//                        objcallcenter.setName_in_full(objcustomerdetails.getName_in_full());
//                        if (objcustomerdetails.getTelephone() == null || objcustomerdetails.getTelephone().equalsIgnoreCase("null")) {
//                            objcallcenter.setTelephone("");
//                        } else {
//                            objcallcenter.setTelephone(objcustomerdetails.getTelephone());
//                        }
//
//                        if (objcustomerdetails.getNic() == null || objcustomerdetails.getNic().equalsIgnoreCase("null")) {
//                            objcallcenter.setNic("");
//                        } else {
//                            objcallcenter.setNic(objcustomerdetails.getNic());
//                        }
//
//                        if (objcustomerdetails.getEmail() == null || objcustomerdetails.getEmail().equalsIgnoreCase("null")) {
//                            objcallcenter.setEmail("");
//                        } else {
//                            objcallcenter.setEmail(objcustomerdetails.getEmail());
//                        }
//                        usercase.setObjcallcenter(objcallcenter);
//
//                    }
//                }
//                if (usercase.getCampainid() != null) {
//                    model.addAttribute("chanelList", commonDAOImpl.getDropdownValueList("SELECT ID AS ID,CH.DESCRIPTION AS DESCRIPTION  FROM campaignchannel C INNER JOIN channel CH ON C.CHANNELID=CH.CHANNELID WHERE CAMPAIGNID ='" + usercase.getCampainid() + "'"));
//                }
//
//                model.addAttribute("Objcallcenter", usercase);
////               
//                model.addAttribute("usercase", usercase);
//                model.put("caseHistoryList", caseDaoImpl.getCaseHistory(sourceid, name));
//
//                remindernotificationdaoimpl.setViewStatus("35", sourceid, section);
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(ReminderNotificationController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        model.put("MAP", "CCSP");
//        if (section.contentEquals(String.valueOf(MasterDataVarList.CCL_CODE_CALLCENTER_SECTION_ID))) {
//
//        }
//        return "case/ticketview";
//    }
    
    
    @RequestMapping(value = "/getCount", method = RequestMethod.POST)
    public @ResponseBody
    String getCount(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        JSONObject NotifiReminder = new JSONObject();
        String name = (String) session.getAttribute("username");
        try {
            Date d1 = commonDAOImpl.getCurentTimesStamp();
            Date d2 = commonDAOImpl.getCurentTimesStamp();
            d2.setTime(d2.getTime() + 10 * 60000);
//            System.out.println("Date" + d2);
//            System.out.println(getStringFormatDate(CommonVarList.DATE_FORMAT_dd_MMM_yy_hhmmssSSa, d1));
//            System.out.println(getStringFormatDate(CommonVarList.DATE_FORMAT_dd_MMM_yy_hhmmssSSa, d2));

            int count = remindernotificationdaoimpl.getReminderCount(name, d1, d2);

            NotifiReminder.put("count", count);
//            System.out.println("COUNT " + count);
        } catch (Exception ex) {
            Logger.getLogger(SectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return NotifiReminder.toString();
    }

    @RequestMapping(value = "/updateNotifiState", method = RequestMethod.POST)
    public @ResponseBody
    String getupdateNotifiState(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String content = request.getParameter("content");
//        System.out.println("content " + content);
        JSONObject NotifiReminder = new JSONObject(content);
//        System.out.println(NotifiReminder.getJSONArray("array"));
        for (int i = 0; i < NotifiReminder.getJSONArray("array").length(); i++) {
            remindernotificationdaoimpl.setRemindStatus(String.valueOf(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_REMIND), String.valueOf(NotifiReminder.getJSONArray("array").get(i)));
        }
        try {

        } catch (Exception ex) {
            Logger.getLogger(SectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "sucess";
    }

    @RequestMapping(value = "/Deletenotification", method = RequestMethod.GET)
    public @ResponseBody
    String deletenotification(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        String output = new String();
        String page = "Status";
        String task = "";
        String description = "";
        String affectedId = "";
        try {

            output = ReminderNotificationService.getdeletenotification(request, response, session);

        } catch (Exception e) {

            e.printStackTrace();
        }
        return output;

//        String content = request.getParameter("content");
//        JSONObject data = new JSONObject(content);
////        System.out.println("content " + content);
//        List<JSONObject> jSONArrayList = new ArrayList<>();
//        try {
//            remindernotificationdaoimpl.setDeleteStatus(String.valueOf(MasterDataVarList.AFFINITI_CODE_REMINDER_NOTIFICATION_DELETE), data.getString("id"), data.getString("section"));
//
//            Date d1 = commonDAOImpl.getCurentTimesStamp();
//            Date d2 = commonDAOImpl.getCurentTimesStamp();
//            d2.setTime(d2.getTime() + 10 * 60000);
//            int iDisplayStart;
//            int iTotalRecords = 0;
//            int iDisplayLength = 10;
//            int minCount = 0;
//            int maxPages = 0;
//
//            String name = (String) session.getAttribute("username");
//            iTotalRecords = remindernotificationdaoimpl.getAllCount(name, d1, d2);
//            maxPages = (iTotalRecords / iDisplayLength);
//            if (iTotalRecords % iDisplayLength > 0) {
//                maxPages++;
//            }
//            if ((data.getInt("iDisplayStart") == 0)) {
//                iDisplayStart = 0;
//            } else {
//                iDisplayStart = (10 * data.getInt("iDisplayStart"));
//            }
//
//            if (data.getInt("iDisplayLength") == 0) {
//                iDisplayLength = 10;
//            } else {
//                iDisplayLength += (iDisplayLength * data.getInt("iDisplayLength"));
//            }
//
//            List< Remindernotification> reminderList = remindernotificationdaoimpl.getTableData(iDisplayLength, iDisplayStart, name, d1, d2);
//
//            JSONObject NotifiReminder = new JSONObject();
//
//            if (reminderList.size() > 0) {
//                for (Remindernotification oneitemreminder : reminderList) {
//                    JSONObject newNotifiReminder = new JSONObject();
//                    newNotifiReminder.put("id", oneitemreminder.getId());
//                    newNotifiReminder.put("description", oneitemreminder.getDescription());
//                    newNotifiReminder.put("remindTime", oneitemreminder.getRemindertime());
//                    newNotifiReminder.put("sourcetabelId", oneitemreminder.getSourcetableid());
//                    newNotifiReminder.put("section", oneitemreminder.getSectionid());
//                    newNotifiReminder.put("contextpath", request.getContextPath());
//                    newNotifiReminder.put("maxPages", maxPages);
//                    newNotifiReminder.put("viewState", oneitemreminder.getViewedstatus());
//                    NotifiReminder.put("NewNotifiList", newNotifiReminder);
//
//                    jSONArrayList.add(newNotifiReminder);
//
//                }
//            } else {
//
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return jSONArrayList.toString();
    }

    @RequestMapping(value = "/loadReminder2", method = RequestMethod.POST)
    public @ResponseBody
    String loadReminderNEW(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

        String output = new String();
        String page = "Status";
        String task = "";
        String description = "";
        String affectedId = "";
        try {

            output = ReminderNotificationService.getReminderNotificationTableData(request, session, affectedId);

        } catch (Exception e) {

            e.printStackTrace();
        }
        return output;
    }

//        Date d1 = commonDAOImpl.getCurentTimesStamp();
//        Date d2 = commonDAOImpl.getCurentTimesStamp();
//        d2.setTime(d2.getTime() + 10 * 60000);
//        int iDisplayStart;
//        int iTotalRecords = 0;
//        int iDisplayLength = 10;
//        int minCount = 0;
//        int maxPages = 0;
//        String content = request.getParameter("content");
//        JSONObject data = new JSONObject(content);
//        String name = (String) session.getAttribute("username");
//        iTotalRecords = remindernotificationdaoimpl.getAllCount(name, d1, d2);
//        maxPages = (iTotalRecords / iDisplayLength);
//        if (iTotalRecords % iDisplayLength > 0) {
//            maxPages++;
//        }
//        if ((data.getInt("iDisplayStart") == 0)) {
//            iDisplayStart = 0;
//        } else {
//            iDisplayStart = (10 * data.getInt("iDisplayStart"));
//        }
//
//        if (data.getInt("iDisplayLength") == 0) {
//            iDisplayLength = 10;
//        } else {
//            iDisplayLength += (iDisplayLength * data.getInt("iDisplayLength"));
//        }
//
//        List< Remindernotification> reminderList = remindernotificationdaoimpl.getTableData(iDisplayLength + iDisplayStart, iDisplayStart, name, d1, d2);
//        List<JSONObject> jSONArrayList = new ArrayList<>();
//        JSONObject NotifiReminder = new JSONObject();
//        try {
//            if (reminderList.size() > 0) {
//                for (Remindernotification oneitemreminder : reminderList) {
//                    JSONObject newNotifiReminder = new JSONObject();
//                    newNotifiReminder.put("id", oneitemreminder.getId());
//                    newNotifiReminder.put("description", oneitemreminder.getDescription());
//                    newNotifiReminder.put("remindTime", oneitemreminder.getRemindertime());
//                    newNotifiReminder.put("sourcetabelId", oneitemreminder.getSourcetableid());
//                    newNotifiReminder.put("section", oneitemreminder.getSectionid());
//                    newNotifiReminder.put("contextpath", request.getContextPath());
//                    newNotifiReminder.put("maxPages", maxPages);
//                    newNotifiReminder.put("viewState", oneitemreminder.getViewedstatus());
//                    NotifiReminder.put("NewNotifiList", newNotifiReminder);
//
//                    jSONArrayList.add(newNotifiReminder);
//
//                }
//            } else {
//
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return jSONArrayList.toString();
    //}
//    private void setCreateViewComponenets(ModelMap model) throws Exception {
//        model.addAttribute("caseTypeList", caseTypeDAOImpl.getCaseTypeDropdownList());
//        model.addAttribute("productList", productDAOImpl.getProductDropdownList());
//        model.addAttribute("casePriorityList", casePriorityDAOImpl.getCasePriorityDropdownList());
//        model.addAttribute("callDirectoryList", callDirectionDaoImpl.getCallDirectionDropdownList());
//        model.addAttribute("fuaList", followUpActionDaoImpl.getFollowUpActionDropdownList());
//        model.addAttribute("p_language", languagesDaoImpl.getLanguagesList());
//        model.addAttribute("statusList", statusDAOImpl.getStatusDropdownList(MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_CALLLOG));
//        model.put("titleList", titleDAOImpl.getTitleDropdownList());
//        model.addAttribute("branchList", branchdaoimpl.getTerritoryMapBranchDropdownList());
//        model.addAttribute("businesshistory", commendaoimpl.getDropdownValueList("SELECT BUSINESSHISTORYID AS ID, DESCRIPTION AS DESCRIPTION  FROM businesshistory"));
//        model.addAttribute("naturofbustiness", commendaoimpl.getDropdownValueList("SELECT  BUSINESSNATUREID AS ID, DESCRIPTION AS DESCRIPTION  FROM businessnaturs"));
//        model.addAttribute("incomeamout", commendaoimpl.getDropdownValueList("SELECT  INCOMEAMOUNTID AS ID, DESCRIPTION AS DESCRIPTION  FROM incomeamount"));
//        model.addAttribute("productcategory", commonDAOImpl.getDropdownValueList("SELECT PRODUCTCATEGORYTYPEID AS ID, DESCRIPTION  AS DESCRIPTION FROM  productcategorytype WHERE PRODUCTCATEGORYTYPEID < 4"));
//    }
//
//    private void setCreateViewComponenetsTicket(ModelMap model) throws Exception {
//        model.addAttribute("caseTypeList", caseTypeDAOImpl.getCaseTypeDropdownList());
//        model.addAttribute("casePriorityList", casePriorityDAOImpl.getCasePriorityDropdownList());
//        model.addAttribute("productList", productDAOImpl.getProductDropdownList());
//        model.addAttribute("statusList", statusDAOImpl.getStatusDropdownList(MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_CASE));
//        model.addAttribute("branchList", branchdaoimpl.getTerritoryMapBranchDropdownList());
//        model.addAttribute("titleList", titleDAOImpl.getTitleDropdownList());
//        model.addAttribute("businesshistory", commonDAOImpl.getDropdownValueList("SELECT BUSINESSHISTORYID AS ID, DESCRIPTION AS DESCRIPTION  FROM businesshistory"));
//        model.addAttribute("naturofbustiness", commonDAOImpl.getDropdownValueList("SELECT  BUSINESSNATUREID AS ID, DESCRIPTION AS DESCRIPTION  FROM businessnaturs"));
//        model.addAttribute("incomeamout", commonDAOImpl.getDropdownValueList("SELECT  INCOMEAMOUNTID AS ID, DESCRIPTION AS DESCRIPTION  FROM incomeamount"));
//        model.addAttribute("productcategory", commonDAOImpl.getDropdownValueList("SELECT PRODUCTCATEGORYTYPEID AS ID, DESCRIPTION  AS DESCRIPTION FROM  productcategorytype WHERE PRODUCTCATEGORYTYPEID < 4"));
//        model.addAttribute("campaingList", commonDAOImpl.getDropdownValueList("SELECT CAMPAIGNID as ID, DESCRIPTION as DESCRIPTION  FROM campaign"));
//
//        model.addAttribute("caseTypeList", caseTypeDAOImpl.getCaseTypeDropdownList());
//        model.addAttribute("productList", productDAOImpl.getProductDropdownList());
//        model.addAttribute("casePriorityList", casePriorityDAOImpl.getCasePriorityDropdownList());
//        model.addAttribute("callDirectoryList", callDirectionDaoImpl.getCallDirectionDropdownList());
//        model.addAttribute("fuaList", followUpActionDaoImpl.getFollowUpActionDropdownList());
//        model.addAttribute("p_language", languagesDaoImpl.getLanguagesList());
//        model.addAttribute("statusCalllogList", statusDAOImpl.getStatusDropdownList(MasterDataVarList.AFFINITI_CODE_STATUS_CATEGORY_CALLLOG));
//        model.put("titleList", titleDAOImpl.getTitleDropdownList());
//        model.addAttribute("branchList", branchdaoimpl.getTerritoryMapBranchDropdownList());
//        model.addAttribute("businesshistory", commonDAOImpl.getDropdownValueList("SELECT BUSINESSHISTORYID AS ID, DESCRIPTION AS DESCRIPTION  FROM businesshistory"));
//        model.addAttribute("naturofbustiness", commonDAOImpl.getDropdownValueList("SELECT  BUSINESSNATUREID AS ID, DESCRIPTION AS DESCRIPTION  FROM businessnaturs"));
//        model.addAttribute("incomeamout", commonDAOImpl.getDropdownValueList("SELECT  INCOMEAMOUNTID AS ID, DESCRIPTION AS DESCRIPTION  FROM incomeamount"));
//        model.addAttribute("productcategory", commonDAOImpl.getDropdownValueList("SELECT PRODUCTCATEGORYTYPEID AS ID, DESCRIPTION  AS DESCRIPTION FROM  productcategorytype WHERE PRODUCTCATEGORYTYPEID < 4"));
//        model.addAttribute("campaingList", commonDAOImpl.getDropdownValueList("SELECT CAMPAIGNID as ID, DESCRIPTION as DESCRIPTION  FROM campaign"));
//
//    }
    public void xmlCronTask2() {

//        System.out.println("this is test shedule");
    }
}
