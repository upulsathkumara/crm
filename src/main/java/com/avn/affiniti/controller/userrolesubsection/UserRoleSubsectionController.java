/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avn.affiniti.controller.userrolesubsection;

/**
 *
 * @author chandima
 */
import com.avn.affiniti.hibernate.model.Userrolesubsection;
import com.avn.affiniti.model.dataobject.SubsectionDataObject;
import com.avn.affiniti.service.userrolesubsection.UserroleSubsectionService;
import com.avn.affiniti.util.Loggers;
import com.avn.affiniti.util.varlist.CommonVarList;
import com.avn.affiniti.util.varlist.MessageVarList;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/userrolesubsection")
public class UserRoleSubsectionController {

    @Autowired
    UserroleSubsectionService userroleSubsectionService;

    @Autowired
    HttpSession session;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String createview(ModelMap model) {
        try {
            model.put("root", "User Role SubSection");
            model.put("page", "");
            model.put("userRoleSubsectionAddForm", new Userrolesubsection());
            userroleSubsectionService.loadPageComponent(model);
        } catch (Exception ex) {
        }
        return "userrolesubsection/userrolesubsection";
    }

    @RequestMapping(value = "/loadTable", method = RequestMethod.POST)
    public @ResponseBody
    String loadUSerroleSubSectionTasksTable(HttpServletRequest request) {

        JSONObject response = new JSONObject();
        try {
            response = userroleSubsectionService.getUserroleSubectionTableData(request);
            String page = "Userrole Subsection View";
            String affectedId = "";
            String task = "View";
            String description = "View All Details in Userrole Subsection Table ";

            String createdUser = String.valueOf(session.getAttribute("username"));
            userroleSubsectionService.insertAuditTrace(page, affectedId, task, description, createdUser);
        } catch (Exception ex) {
            response.put("success", false);
            response.put("messageService", "Un-successfull");
            response.put("sEcho", 0);
            response.put("iTotalRecords", 0);
            response.put("iTotalDisplayRecords", 0);
            Logger.getLogger(UserRoleSubsectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response.toString();
    }

    @RequestMapping(value = "/loadsections", method = RequestMethod.GET)
    public @ResponseBody
    String loadSelect(HttpServletRequest request
    ) {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            JSONObject array = userroleSubsectionService.getSections(id);
            return array.toString();
        } catch (Exception ex) {
            return "";
        }
    }

    @RequestMapping(value = "/loadsubsections", method = RequestMethod.GET)
    public @ResponseBody
    String loadSubsectionList(HttpServletRequest request
    ) {
        JSONArray array = null;
        try {
            String sectiondata = request.getParameter("subsectiondataobject");
            SubsectionDataObject subsectionDataObject = (SubsectionDataObject) new ObjectMapper().readValue(sectiondata, SubsectionDataObject.class);
            array = userroleSubsectionService.getSubSections(subsectionDataObject);
        } catch (Exception ex) {
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
        }
        return array.toString();
    }

    @RequestMapping(value = "/viewandupdate", method = RequestMethod.GET)
    public @ResponseBody
    String viewAndUpdateSubsectionList(HttpServletRequest request
    ) {
        JSONArray array = new JSONArray();
        try {
            String sectiondata = request.getParameter("subsectiondataobject");
            SubsectionDataObject subsectionDataObject = (SubsectionDataObject) new ObjectMapper().readValue(sectiondata, SubsectionDataObject.class);
            array = userroleSubsectionService.getSubSections(subsectionDataObject);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return array.toString();
    }

    @RequestMapping(value = "/loadassignedsubsections", method = RequestMethod.GET)
    public @ResponseBody
    String loadAssignedSubsectionList(HttpServletRequest request
    ) {
        JSONArray array = null;
        try {
            String sectiondata = request.getParameter("subsectiondataobject");
            SubsectionDataObject subsectionDataObject = (SubsectionDataObject) new ObjectMapper().readValue(sectiondata, SubsectionDataObject.class);
            array = userroleSubsectionService.getAssignedSubSections(subsectionDataObject);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserRoleSubsectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array.toString();
    }

    @RequestMapping(value = "/loadSubsectionDropdownlists", method = RequestMethod.GET)
    public @ResponseBody
    String loadSubsectionDropdownlists(HttpServletRequest request
    ) {
        JSONObject array = null;
        try {
            String sectiondata = request.getParameter("subsectiondataobject");
            SubsectionDataObject subsectionDataObject = (SubsectionDataObject) new ObjectMapper().readValue(sectiondata, SubsectionDataObject.class);
            array = userroleSubsectionService.getSubsectionDropdownlists(subsectionDataObject);
        } catch (Exception ex) {
            ex.printStackTrace();
            Loggers.LOGGER_FILE.error(ex);
            Logger.getLogger(UserRoleSubsectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return array.toString();
    }

    @RequestMapping(value = "/loaduserrols", method = RequestMethod.GET)
    public @ResponseBody
    String loadUserrols() {
        JSONArray array = new JSONArray();
        try {
            array = userroleSubsectionService.getUserrols();
        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
        }
        return array.toString();
    }

    //Adding new Subsection 
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public @ResponseBody
    String insertSubsection(HttpServletRequest request, @RequestParam("action") String selectFunction
    ) {
        JSONObject response = new JSONObject();
        try {
            String res = request.getParameter("subinfo");
            String uNres = request.getParameter("Unsubinfo");
            response = userroleSubsectionService.addUserroleSubsection(request);

        } catch (Exception ex) {
            Loggers.LOGGER_FILE.error(ex);
            response.put("data", "");
            response.put("status", CommonVarList.AFFINITI_MESSAGECODE_ERROR);
            response.put("success", false);
            response.put("message", String.format(MessageVarList.ASSIGN_ERROR, "Subsections"));
        }
        return response.toString();
    }

}
